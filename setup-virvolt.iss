#define MyAppVer GetFileVersion(".\Dialogie.exe") ; define variable
#define izAppName "DialogIE op�ration Vir'Volt"
#define izCompany "IZUBA �nergies/ADEME";

[Setup]
AppId={{56C10D20-9C1B-42A5-A789-CC2757E091B3}
AppName={#izAppName}
AppVerName={#izAppName} {#MyAppVer}
AppPublisher={#izCompany}
AppPublisherURL=http://www.izuba.fr
AppSupportURL=http://www.dialogie.info
AppUpdatesURL=http://www.dialogie.info
DefaultDirName={pf}\{#izAppName}
DefaultGroupName={#izAppName}
;LicenseFile=_files\licence.rtf
;InfoBeforeFile=_files\info.rtf
OutputBaseFilename=setup-VirVolt.{#MyAppVer}
Compression=lzma/ultra
SolidCompression=true
UsePreviousAppDir=true
OutputDir=_setup
VersionInfoVersion={#MyAppVer}
VersionInfoCompany={#izCompany}
VersionInfoDescription={#izAppName}
VersionInfoCopyright=� 2014 {#izCompany}
ChangesAssociations=true
AppCopyright={#izCompany}
ShowLanguageDialog=yes
AllowNoIcons=true
InternalCompressLevel=ultra
WizardImageFile=compiler:wizmodernimage-is.bmp
WizardSmallImageFile=compiler:wizmodernsmallimage-is.bmp


[Languages]
Name: fre; MessagesFile: compiler:Languages\French.isl

[Tasks]
Name: desktopicon; Description: {cm:CreateDesktopIcon}; GroupDescription: {cm:AdditionalIcons}; 

[Files]
Source: Dialogie.exe; DestDir: {app}; Flags: ignoreversion
;Source: Interface 3D\I3D.exe; DestDir: {app}\Interface 3D; Flags: ignoreversion

Source: data\*; DestDir: {commonappdata}\{#izAppName}\; Flags: ignoreversion recursesubdirs 
;Source: Interface 3D\data\*; DestDir: {commonappdata}\{#izAppName}\I3D\; Flags: ignoreversion recursesubdirs uninsremovereadonly
;Source: _files\data\Dialogie-DPE data Habitat 20120410a.xls; DestDir: {commonappdata}\{#izAppName}\; DestName: "Dialogie habitat.xls"
;Source: _files\data\Dialogie prix �nergie 20130101.xls; DestDir: {commonappdata}\{#izAppName}\; DestName: "Dialogie prix �nergie.xls" 
Source: _files\img\dialogie3-file.ico; DestDir: {commonappdata}\{#izAppName}\
Source: _files\report\*; DestDir: {commondocs}\{#izAppName}\Mod�les Rapport\; Flags: ignoreversion recursesubdirs uninsneveruninstall

[Icons]
Name: {group}\{#izAppName}; Filename: {app}\Dialogie.exe; WorkingDir: {app}; IconIndex: 0
Name: {commondesktop}\{#izAppName}; Filename: {app}\Dialogie.exe; WorkingDir: {app}; Tasks: desktopicon; 
Name: {group}\{cm:UninstallProgram, Dialogie Vir'Volt}; Filename: {uninstallexe}
Name: {group}\{cm:ProgramOnTheWeb, Vir'Volt}; Filename: "https://sites.google.com/site/alesaintbrieuc/virvolt"

;Name: {group}\Donn�es habitat; Filename: {commonappdata}\{#izAppName}\Dialogie habitat.xls
;Name: {group}\Donn�es prix des �nergies; Filename: {commonappdata}\{#izAppName}\Dialogie prix �nergie.xls


[Run]
Filename: {app}\Dialogie.exe; Description: {cm:LaunchProgram,Dialogie Vir'Volt}; Flags: nowait postinstall skipifsilent

[Registry]
Root: HKCR; SubKey: .eie2; ValueType: string; ValueData: Projet {#izAppName}; Flags: uninsdeletekey
Root: HKCR; SubKey: Projet {#izAppName}; ValueType: string; ValueData: Fichier de projet DialogIE v2; Flags: uninsdeletekey
Root: HKCR; SubKey: Projet {#izAppName}\Shell\Open\Command; ValueType: string; ValueData: """{app}\Dialogie.exe"" ""%1"""; Flags: uninsdeletevalue
Root: HKCR; Subkey: Projet {#izAppName}\DefaultIcon; ValueType: string; ValueData: {commonappdata}\{#izAppName}\dialogie3-file.ico,-1; Flags: uninsdeletevalue

[Dirs]
Name: {userdocs}\{#izAppName}\Projets; Flags: uninsneveruninstall; Tasks: ; Languages: 
Name: {userdocs}\{#izAppName}\Biblioth�ques; Flags: uninsneveruninstall; Tasks: ; Languages: 
Name: {userdocs}\{#izAppName}\Biblioth�ques\Fichiers Meteo; Flags: uninsneveruninstall; Tasks: ; Languages:
