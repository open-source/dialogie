<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
	<xsl:decimal-format name="fr" decimal-separator="," grouping-separator=" " NaN="--" />
	<xsl:output method="html" media-type="text/html" omit-xml-declaration="yes" indent="yes" encoding="iso8859-15" doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN" doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"/>


	<xsl:strip-space elements="*"/>


	<xsl:variable name="debug" select="project/@debug != ''"/>
<xsl:variable name="base-uri" select="base-uri(.)"/>
<xsl:variable name="document-uri" select="document-uri(.)"/>
<xsl:variable name="filename" select="(tokenize($document-uri,'/'))[last()]"/>

	<xsl:template match="/">
		<html>
			<head>
				<title>
					<xsl:text disable-output-escaping="yes">Projet DialogIE</xsl:text>(<xsl:call-template name="copy-izuba"/>)</title>
				<meta name="Copyright" CONTENT="IZUBA energies"/>
				<meta name="Rev" CONTENT="contact@izuba.fr"/>

<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"/>
<!-- Latest compiled JavaScript -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>


				<style type="text/css">
body, a[name] { padding-top: 70px; }

table{
width:100%;
margin:2px;
padding:2px;
border-collapse:collapse;
}



th{
padding:6px;
text-align:left;
background-color:rgba(212,244,255,0.5);
font-weight:bold;
font-size:115%;
border-top:solid 4px #888888;
border-left:solid 4px #888888;
}


td{
padding:2px;
}
td.hidden{
background-color:rgba(255, 127, 127,0.5) !important;
}
td.hidden-rule{
background-color:rgba(127, 255, 127,0.5) !important;
}

td.indent{
padding-left:2px;
border-left:solid 4px rgba(127,127,127,0.5);
}

td.data-label{
	width: 70% !important;
background-color:#eeeeee;
border-bottom:solid 1px #dddddd;
font-weight:bold;
}
td.value{
width: 30% !important;
text-align:right;
background-color:#f8f8f8;
border-bottom:solid 1px #eeeeee;
}



td.array-value{
font-family:courier, "courier new", monospace;
font-size:14px;
text-align:right;
background-color:#f8f8f8;
border:solid 1px #dddddd;
}

th.array-value{
text-align:center;
font-weight:bold;
border:solid 2px #ffffff;
}



.unit{
font-style:italic;
}

</style>
				<script type="text/javascript">
function showhide(event)
{
var detailZone = document.getElementById(event);
//alert('yoooo ' + detailZone.style.display);
if (detailZone.style.display == 'none')
{
detailZone.style.display = '';
}
else
{
detailZone.style.display = 'none';
}
}
</script>

			</head>
			<body>

				<nav class="navbar navbar-inverse navbar-fixed-top">
					<div class="container-fluid">
						<div class="navbar-header">
							 <a class="navbar-brand" href="#">
									Projet <xsl:value-of select="$filename"/>
								</a>
						</div>
						<ul class="nav navbar-nav">
							<xsl:apply-templates select="projet-dialogie/variantes/*" mode="tdm"/>
						</ul>
					</div>
				</nav>

				<table class="table">
					<xsl:apply-templates select="projet-dialogie" mode="data"/>
				</table>
				<xsl:call-template name="copy-izuba"/>
			</body>
		</html>
	</xsl:template>


<!--gestion de la table des matières-->
	<xsl:template match="variante" mode="tdm">
		<li class="dropdown">
			<a class="dropdown-toggle" data-toggle="dropdown" href="#"><xsl:value-of select="id"/><span class="caret"></span></a>
			<ul class="dropdown-menu">
				<li>
					<a><xsl:attribute name="href">#variante-<xsl:value-of select="id"/></xsl:attribute>
						Variante
					</a>
				</li>
				<li>
					<a><xsl:attribute name="href">#var-hypo-<xsl:value-of select="id"/></xsl:attribute>
						Hypothèses
					</a>
				</li>
				<li>
					<a><xsl:attribute name="href">#var-result-<xsl:value-of select="id"/></xsl:attribute>
						Résultats
					</a>
				</li>
			</ul>
		</li>
	</xsl:template>


<!--gestion des données-->
	<xsl:template match="projet-dialogie" mode="data">
		<xsl:variable name="jid" select="generate-id(current())"/>
		<tr>
			<a name="#projet-general"/>
			<th onclick="showhide('{$jid}')">
			Projet
			</th>
		</tr>
		<tr id="{$jid}">
			<td>
				<table>
					<xsl:apply-templates select="*[name() != 'variantes' and not(*) and text() != '']" mode="data"/>
					<xsl:apply-templates select="*[name() != 'variantes'and (*)]" mode="data-sub"/>
				</table>
			</td>
		</tr>
		<tr>
			<td>
				<xsl:apply-templates select="variantes/*" mode="data"/>
			</td>
		</tr>
	</xsl:template>


	<xsl:template match="variante" mode="data">
		<table class="table">
			<tr>
				<a name="variante-{id}"/>
				<th onclick="showhide('variante-{id}')">
			Variante <xsl:value-of select="id"/>
					<xsl:if test="position() = number(/projet-dialogie/reference/text()) + 1">
						&#160;<span class="badge">référence</span>
					</xsl:if>
				</th>
			</tr>
			<tr id="variante-{id}">
				<td class="indent">
					<table>
						<a name="var-hypo-{id}"/>
						<tr>
							<th onclick="showhide('var-hypo-{id}')">Hypothèses</th>
						</tr>
						<tr id="var-hypo-{id}">
							<td class="indent">
								<table>
									<xsl:apply-templates select="*[name() != 'resultat' and not(*) and text() != '']" mode="data"/>
									<xsl:apply-templates select="*[name() != 'resultat'and (*)]" mode="data-sub"/>
								</table>
							</td>
						</tr>
					</table>
					<table>
						<a name="var-result-{id}"/>
						<tr>
							<th onclick="showhide('var-result-{id}')">Résultats</th>
						</tr>
						<tr id="var-result-{id}">
							<td class="indent">
								<table>
									<xsl:apply-templates select="resultat" mode="data-sub"/>
								</table>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</xsl:template>


	<xsl:template match="*" mode="data-sub">
		<xsl:variable name="jid" select="generate-id(current())"/>
		<xsl:variable name="data-array" select="*[not(*) and text() != '' and @x and @y]"/>
		<tr>
			<th colspan="2" onclick="showhide('{$jid}')">
				<xsl:value-of select="name()"/>
				<xsl:apply-templates select="@*" mode="data-attr"/>
			</th>
		</tr>
		<tr id="{$jid}">
			<td class="indent" colspan="2">
				<table>
					<!--les éléments qui ont une valeur, pas d'enfant et qui ne sont pas des tableaux-->
					<xsl:apply-templates select="*[not(*) and text() != '' and not(@x) and not(@y)]" mode="data"/>
					<!--les éléments qui ont une valeur, pas d'enfant et qui sont des tableaux-->
					<xsl:if test="$data-array">
<!--les en-têtes de colonne-->
							<tr>
								<th  class="array-value"> </th>
									<xsl:for-each select="$data-array[@y = $data-array[1]/@y]">
										<xsl:sort select="number(current()/@x)"/>
										<th  class="array-value"><xsl:value-of select="current()/@x"/></th>
									</xsl:for-each>
							</tr>
<!--les valeurs de tableau-->
							<xsl:for-each-group select="$data-array" group-by="@y">
								<xsl:sort select="number(current-grouping-key())"/>
								<tr>
									<th  class="array-value"><xsl:value-of select="current-grouping-key()"/></th>
								<xsl:for-each select="current-group()">
									<xsl:sort select="number(@x)"/>
										<xsl:apply-templates select="current()" mode="data-array"/>
								</xsl:for-each>
								</tr>
							</xsl:for-each-group>
					</xsl:if>
					<!--les éléments qui ont des enfants-->
					<xsl:apply-templates select="*[*]" mode="data-sub"/>
				</table>
			</td>
		</tr>
	</xsl:template>

	<xsl:template match="*" mode="data">
		<xsl:if test="text() != '*'">
			<tr>
				<td class="data-label">
					<xsl:value-of select="name()"/>
					<xsl:apply-templates select="@*" mode="data-attr"/>
				</td>
				<td class="value">
					<xsl:if test="name() != 'saisie-3D'">
						<xsl:apply-templates select="." mode="data-value"/>
					</xsl:if>
				</td>
			</tr>
		</xsl:if>
	</xsl:template>


	<xsl:template match="*" mode="data-array">
		<td class="array-value">
			<xsl:apply-templates select="." mode="data-value"/>
		</td>
	</xsl:template>


	<xsl:template match="*" mode="data-value">
	<xsl:variable name="int-part" select="number(floor(.)*(. >= 0) + ceiling(.) * not(. >= 0))"/>
		<xsl:choose>
			<xsl:when test="$int-part = text()">
				<xsl:value-of select='text()' />
			</xsl:when>
			<xsl:when test="number(text()) = text()">
				<xsl:value-of select='format-number(number(text()), "0.000")' />
			</xsl:when>
			<xsl:when test="text() != '*'">
				<xsl:value-of select="text()"/>
			</xsl:when>
			<xsl:otherwise>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>



	<xsl:template match="@*" mode="data-attr">
		&#160;<span class="badge">
			<xsl:value-of select="name()"/>=<xsl:value-of select="current()"/>
		</span>
	</xsl:template>



	<xsl:template name="copy-izuba">
		<xsl:text disable-output-escaping="yes">&amp;copy; IZUBA &amp;eacute;nergies</xsl:text>
	</xsl:template>



</xsl:stylesheet>
