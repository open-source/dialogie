
<xsl:stylesheet version="1.0" xmlns="urn:schemas-microsoft-com:office:spreadsheet" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:user="urn:my-scripts" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet">


	<xsl:decimal-format name="fr" decimal-separator="," grouping-separator=" " NaN="--" infinity="-" />
<!--	<xsl:output method="xml" encoding="utf-8" indent="yes" />-->
	<xsl:output method="xml" encoding="iso-8859-15" indent="yes" />


	<xsl:strip-space elements="*"/>


	<xsl:variable name="debug" select="project/@debug != ''"/>
	<xsl:variable name="base-uri" select="base-uri(.)"/>
	<xsl:variable name="document-uri" select="document-uri(.)"/>
	<xsl:variable name="filename" select="(tokenize($document-uri,'/'))[last()]"/>

	<xsl:variable name="col-width">15</xsl:variable>
	<xsl:variable name="col-label">3</xsl:variable>
	<xsl:variable name="col-value">3</xsl:variable>


	<xsl:template match="/">
		<Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet" xmlns:html="http://www.w3.org/TR/REC-html40">

			<DocumentProperties xmlns="urn:schemas-microsoft-com:office:office">
				<Author>IZUBA énergies</Author>
				<LastAuthor>IZUBA énergies</LastAuthor>
<!--				<Created>2013-09-02T14:14:06Z</Created>-->
				<Company>IZUBA énergies</Company>
				<Version>11.6568</Version>
			</DocumentProperties>

			<xsl:call-template name="add-styles"/>

			<ExcelWorkbook xmlns="urn:schemas-microsoft-com:office:excel"></ExcelWorkbook>

			<xsl:apply-templates select="projet-dialogie" mode="data"/>
		</Workbook>
	</xsl:template>




<!--gestion des données-->
	<xsl:template match="projet-dialogie" mode="data">
		<Worksheet>
			<xsl:attribute name="ss:Name">
				<xsl:call-template name="get-worksheet-title">
					<xsl:with-param name="title" select="$filename"/>
				</xsl:call-template>
			</xsl:attribute>
			<Table x:FullColumns="1" x:FullRows="1">
				<xsl:call-template name="columns-definition"/>


				<xsl:call-template name="display-label">
					<xsl:with-param name="label" select="$filename"/>
					<xsl:with-param name="label-style">izMainTitle</xsl:with-param>
				</xsl:call-template>
				<xsl:call-template name="display-label">
					<xsl:with-param name="label">Projet</xsl:with-param>
					<xsl:with-param name="label-style">izTitleDesc</xsl:with-param>
				</xsl:call-template>




				<xsl:apply-templates select="*[name() != 'variantes' and not(*) and text() != '']" mode="data">
					<xsl:with-param name="level" select="number(1)"/>
				</xsl:apply-templates>
				<xsl:apply-templates select="*[name() != 'variantes'and (*)]" mode="data-sub">
					<xsl:with-param name="level" select="number(1)"/>
				</xsl:apply-templates>
			</Table>

			<xsl:call-template name="sheet-options">
				<xsl:with-param name="is-project">true</xsl:with-param>
				<xsl:with-param name="is-reference">false</xsl:with-param>
			</xsl:call-template>

		</Worksheet>
<!--gestion des feuilles de chaque variante-->
		<xsl:apply-templates select="variantes/*" mode="data"/>
	</xsl:template>


	<xsl:template match="variante" mode="data">
		<xsl:param name="level">1</xsl:param>
		<Worksheet>
			<xsl:attribute name="ss:Name">
				<xsl:call-template name="get-worksheet-title">
					<xsl:with-param name="title" select="id"/>
				</xsl:call-template>
			</xsl:attribute>
			<Table x:FullColumns="1" x:FullRows="1">
				<xsl:call-template name="columns-definition"/>

				<xsl:call-template name="display-label">
					<xsl:with-param name="label" select="id"/>
					<xsl:with-param name="label-style">izMainTitle</xsl:with-param>
				</xsl:call-template>
				<xsl:call-template name="display-label">
					<xsl:with-param name="label" select="description"/>
					<xsl:with-param name="label-style">izTitleDesc</xsl:with-param>
				</xsl:call-template>
				<xsl:call-template name="display-label">
					<xsl:with-param name="label">Hypothèses</xsl:with-param>
					<xsl:with-param name="label-style">izSubTitle</xsl:with-param>
				</xsl:call-template>

				<xsl:apply-templates select="*[name() != 'resultat' and not(*) and text() != '']" mode="data">
					<xsl:with-param name="level" select="number($level)"/>
				</xsl:apply-templates>
				<xsl:apply-templates select="*[name() != 'resultat'and (*)]" mode="data-sub">
					<xsl:with-param name="level" select="number($level)"/>
				</xsl:apply-templates>

				<xsl:call-template name="display-label">
					<xsl:with-param name="label">Résultats</xsl:with-param>
					<xsl:with-param name="label-style">izSubTitle</xsl:with-param>
				</xsl:call-template>

				<xsl:apply-templates select="resultat" mode="data-sub">
					<xsl:with-param name="level" select="number($level) + 1"/>
				</xsl:apply-templates>

			</Table>
			<xsl:call-template name="sheet-options">
				<xsl:with-param name="is-reference" select="position() = number(/projet-dialogie/reference/text()) + 1"/>
			</xsl:call-template>
		</Worksheet>
	</xsl:template>


	<xsl:template match="*" mode="data-sub">
		<xsl:param name="level" select="number(0)"/>
		<xsl:variable name="data-array" select="*[not(*) and text() != '' and @x and @y]"/>
		<xsl:variable name="label-width">
			<xsl:choose>
				<xsl:when test="@* and number($level) = 0"><xsl:value-of select="$col-width - 2"/></xsl:when>
				<xsl:when test="@* and number($level) != 0"><xsl:value-of select="$col-width - 3"/></xsl:when>
				<xsl:when test="not(@*) and number($level) != 0"><xsl:value-of select="$col-width - 1"/></xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="$col-width"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>

		<Row ss:AutoFitHeight="1">
			<xsl:call-template name="display-cell">
				<xsl:with-param name="level" select="$level"/>
				<xsl:with-param name="label" select="name()"/>
				<xsl:with-param name="label-style">izSubHeader</xsl:with-param>
				<xsl:with-param name="label-width" select="$label-width"/>
			</xsl:call-template>
			<xsl:apply-templates select="@*" mode="data-attr"/>
		</Row>
<!--
			<td class="indent" colspan="2">
				<table>
-->
					<!--les éléments qui ont une valeur, pas d'enfant et qui ne sont pas des tableaux-->
		<xsl:apply-templates select="*[not(*) and text() != '' and not(@x) and not(@y)]" mode="data">
				<xsl:with-param name="level" select="number($level) + 1"/>
		</xsl:apply-templates>
					<!--les éléments qui ont une valeur, pas d'enfant et qui sont des tableaux-->
		<xsl:if test="$data-array">
<!--les en-têtes de colonne-->
			<Row ss:AutoFitHeight="1">
				<xsl:call-template name="display-cell">
					<xsl:with-param name="label"/>
					<xsl:with-param name="label-width">1</xsl:with-param>
				</xsl:call-template>
				<xsl:call-template name="display-cell">
					<xsl:with-param name="label"/>
					<xsl:with-param name="label-style">izArrayHeaderLeft</xsl:with-param>
					<xsl:with-param name="label-width">1</xsl:with-param>
				</xsl:call-template>

				<xsl:for-each select="$data-array[@y = $data-array[1]/@y]">
					<xsl:sort select="number(current()/@x)"/>
					<Cell ss:StyleID="izArrayHeader">
						<Data ss:Type="String">
							<xsl:value-of select="current()/@x"/>
						</Data>
					</Cell>
				</xsl:for-each>
			</Row>
<!--les valeurs de tableau-->
			<xsl:for-each-group select="$data-array" group-by="@y">
				<xsl:sort select="number(current-grouping-key())"/>
				<Row ss:AutoFitHeight="1">
					<xsl:call-template name="display-cell">
						<xsl:with-param name="label"/>
						<xsl:with-param name="label-width">1</xsl:with-param>
					</xsl:call-template>
					<xsl:call-template name="display-cell">
						<xsl:with-param name="label" select="current-grouping-key()"/>
						<xsl:with-param name="label-style">izArrayHeaderLeft</xsl:with-param>
						<xsl:with-param name="label-width">1</xsl:with-param>
					</xsl:call-template>

					<xsl:for-each select="current-group()">
						<xsl:sort select="number(@x)"/>
						<xsl:call-template name="display-value">
							<xsl:with-param name="value" select="current()/text()"/>
							<xsl:with-param name="value-width"/>
						</xsl:call-template>
					</xsl:for-each>

				</Row>
			</xsl:for-each-group>
		</xsl:if>
					<!--les éléments qui ont des enfants-->
		<xsl:apply-templates select="*[*]" mode="data-sub">
				<xsl:with-param name="level" select="number($level) + 1"/>
		</xsl:apply-templates>
<!--
				</table>
			</td>
		</tr>
-->
	</xsl:template>


	<xsl:template match="*" mode="data">
		<xsl:param name="level" select="number(0)"/>
		<xsl:variable name="label-width">
			<xsl:choose>
				<xsl:when test="@* and number($level) = 0"><xsl:value-of select="$col-label - 2"/></xsl:when>
				<xsl:when test="@* and number($level) != 0"><xsl:value-of select="$col-label - 3"/></xsl:when>
				<xsl:when test="not(@*) and number($level) != 0"><xsl:value-of select="$col-label - 1"/></xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="$col-label"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>

		<Row ss:AutoFitHeight="1">
			<xsl:call-template name="display-cell">
				<xsl:with-param name="level" select="$level"/>
				<xsl:with-param name="label" select="name()"/>
				<xsl:with-param name="label-style">izLabel</xsl:with-param>
				<xsl:with-param name="label-width" select="$label-width"/>
			</xsl:call-template>

			<xsl:apply-templates select="@*" mode="data-attr"/>
			<xsl:if test="name() != 'saisie-3D'">
				<xsl:call-template name="display-value">
					<xsl:with-param name="value" select="./text()"/>
				</xsl:call-template>
			</xsl:if>
		</Row>
	</xsl:template>



	<xsl:template name="display-value">
		<xsl:param name="value"/>
		<xsl:param name="value-width" select="$col-value"/>
		<xsl:variable name="int-part" select="number(floor($value)*($value >= 0) + ceiling($value) * not($value >= 0))"/>
		<Cell>
			<xsl:if test="number($value-width) > 0">
				<xsl:attribute name="ss:MergeAcross">
					<xsl:value-of select="$value-width"/>
				</xsl:attribute>
			</xsl:if>
			<xsl:choose>
				<xsl:when test="$int-part = $value">
					<xsl:attribute name="ss:StyleID">Integer</xsl:attribute>
					<Data ss:Type="Number">
						<xsl:value-of select='format-number(number($value), "0")' />
					</Data>
				</xsl:when>
				<xsl:when test="number(text()) = text()">
					<xsl:attribute name="ss:StyleID">Num</xsl:attribute>
					<Data ss:Type="Number">
						<xsl:value-of select='format-number(number($value), "0.000")' />
					</Data>
				</xsl:when>
				<xsl:when test="$value != '*'">
					<xsl:attribute name="ss:StyleID">Num</xsl:attribute>
					<Data ss:Type="String">
						<xsl:value-of select="$value"/>
					</Data>
				</xsl:when>
				<xsl:otherwise>
					<xsl:attribute name="ss:StyleID">Num</xsl:attribute>
					<Data ss:Type="String"></Data>
				</xsl:otherwise>
			</xsl:choose>
		</Cell>
	</xsl:template>


	<xsl:template name="display-label">
		<xsl:param name="level" select="number(0)"/>
		<xsl:param name="label"/>
		<xsl:param name="label-style">Default</xsl:param>
		<xsl:param name="label-width">
			<xsl:value-of select="$col-width"/>
		</xsl:param>

		<Row ss:AutoFitHeight="1">
			<xsl:call-template name="display-cell">
				<xsl:with-param name="level" select="$level"/>
				<xsl:with-param name="label" select="$label"/>
				<xsl:with-param name="label-style" select="$label-style"/>
				<xsl:with-param name="label-width" select="$label-width"/>
			</xsl:call-template>
		</Row>
	</xsl:template>


	<xsl:template name="display-cell">
		<xsl:param name="level" select="number(0)"/>
		<xsl:param name="label"/>
		<xsl:param name="label-style">Default</xsl:param>
		<xsl:param name="label-width">
			<xsl:value-of select="$col-label"/>
		</xsl:param>

	<xsl:if test="$level and number($level) != 0">
		<Cell ss:StyleID='izDeepness'>
			<Data ss:Type="Number">
				<xsl:value-of select='format-number(number($level), "0")' />
			</Data>
		</Cell>
	</xsl:if>
		<Cell>
			<xsl:if test="$label-width != ''">
				<xsl:attribute name="ss:MergeAcross">
					<xsl:value-of select="$label-width"/>
				</xsl:attribute>
			</xsl:if>
			<xsl:if test="$label-style != ''">
				<xsl:attribute name="ss:StyleID">
					<xsl:value-of select="$label-style"/>
				</xsl:attribute>
			</xsl:if>
			<Data ss:Type="String">
				<xsl:value-of select="$label"/>
			</Data>
		</Cell>
	</xsl:template>


	<xsl:template match="@*" mode="data-attr">
		<Cell ss:StyleID="izAttLabel">
			<Data ss:Type="String">
				<xsl:value-of select="name()"/>
			</Data>
		</Cell>
		<Cell ss:StyleID="izAttValue">
			<Data ss:Type="String">
				<xsl:value-of select="current()"/>
			</Data>
		</Cell>
	</xsl:template>


	<xsl:template name="columns-definition">
<!--	profondeur dans l'arborescence-->
		<Column ss:AutoFitWidth="0" ss:Width="20"/>
<!--	libellé de l'élement-->
		<Column ss:AutoFitWidth="0" ss:Width="200"/>
<!--	nom d'attribut -->
		<Column ss:AutoFitWidth="0" ss:Width="40"/>
<!--	valeur d'attribut -->
		<Column ss:AutoFitWidth="0" ss:Width="20"/>
<!--		valeur de l'élément-->
<!--à fixer avec la largeur de colonne par défaut-->
		<Column ss:AutoFitWidth="0" ss:Width="60"/>
		<Column ss:AutoFitWidth="0" ss:Width="60"/>
		<Column ss:AutoFitWidth="0" ss:Width="60"/>
		<Column ss:AutoFitWidth="0" ss:Width="60"/>
		<Column ss:AutoFitWidth="0" ss:Width="60"/>
		<Column ss:AutoFitWidth="0" ss:Width="60"/>
		<Column ss:AutoFitWidth="0" ss:Width="60"/>
		<Column ss:AutoFitWidth="0" ss:Width="60"/>
		<Column ss:AutoFitWidth="0" ss:Width="60"/>
		<Column ss:AutoFitWidth="0" ss:Width="60"/>
		<Column ss:AutoFitWidth="0" ss:Width="40"/>
		<Column ss:AutoFitWidth="0" ss:Width="20"/>
	</xsl:template>


	<xsl:template name="add-styles">
		<Styles>
			<Style ss:ID="Default" ss:Name="Normal">
				<Font ss:Bold="0" ss:FontName="Calibri" x:Family="Swiss" ss:Size="9" ss:Color="#000000" />
				<Borders />
				<Alignment ss:Horizontal="Left" ss:Vertical="Top" ss:WrapText="1" />
				<Interior ss:Color="#FFFFFF" ss:Pattern="Solid" />
				<NumberFormat ss:Format="General" />
			</Style>
			<Style ss:ID="Default2">
				<Font ss:Bold="0" ss:FontName="Calibri" x:Family="Swiss" ss:Size="9" ss:Color="#000000" />
				<Borders>
					<Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1" ss:Color="#EEEBE9"/>
					<Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1" ss:Color="#EEEBE9"/>
					<Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1" ss:Color="#EEEBE9"/>
					<Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1" ss:Color="#EEEBE9"/>
				</Borders>
				<Alignment ss:Horizontal="Left" ss:Vertical="Top" ss:WrapText="1" />
				<Interior ss:Color="#FFFFFF" ss:Pattern="Solid" />
				<NumberFormat ss:Format="General" />
			</Style>
			<Style ss:ID="LHead" ss:Parent="Default2">
				<Font ss:Bold="1" ss:FontName="Calibri" x:Family="Swiss" ss:Size="9" ss:Color="#FFFFFF" />
				<Interior ss:Color="#0083BE" ss:Pattern="Solid" />
			</Style>
			<Style ss:ID="RHead" ss:Parent="Default2">
				<Font ss:Bold="1" ss:FontName="Calibri" x:Family="Swiss" ss:Size="9" ss:Color="#FFFFFF" />
				<Alignment ss:Horizontal="Right" ss:Vertical="Top" ss:WrapText="1" />
				<Interior ss:Color="#0083BE" ss:Pattern="Solid" />
			</Style>
			<Style ss:ID="Num" ss:Parent="Default2">
				<Alignment ss:Horizontal="Right" ss:Vertical="Top" />
				<NumberFormat ss:Format="_(* #,##0.000_);_(* [Red]-#,##0.000_);_(* &quot;-&quot;??_);_(@_)" />
			</Style>
			<Style ss:ID="NumLow" ss:Parent="Num">
				<NumberFormat ss:Format="_(* [BLUE]0.0000E-000_);_(* [Magenta]-0.0000E-000_);_(* &quot;-&quot;??_);_(@_)" />
			</Style>
			<Style ss:ID="Integer" ss:Parent="Default2">
				<Alignment ss:Horizontal="Right" ss:Vertical="Top" />
				<NumberFormat ss:Format="_(* #,##0_);_(* [Red]-#,##0_);_(* &quot;-&quot;??_);_(@_)" />
			</Style>
			<Style ss:ID="Date" ss:Parent="Default2">
				<NumberFormat ss:Format="dd\-mmm\-yyyy" />
			</Style>
			<Style ss:ID="NumBold" ss:Parent="Num">
				<Font ss:Bold="1" ss:FontName="Calibri" x:Family="Swiss" ss:Size="9" ss:Color="#000000" />
			</Style>
			<Style ss:ID="DefBold" ss:Parent="Default2">
				<Font ss:Bold="1" ss:FontName="Calibri" x:Family="Swiss" ss:Size="9" ss:Color="#000000" />
			</Style>
<!-- styles de présentation pure -->
			<Style ss:ID="izMainTitle" ss:Parent="Default">
				<Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1" />
				<Font ss:Bold="1" ss:FontName="Calibri" x:Family="Swiss" ss:Size="20" ss:Color="#FFFFFF"/>
				<Interior ss:Color="#4285F4" ss:Pattern="Solid" />
			</Style>

			<Style ss:ID="izTitleDesc" ss:Parent="izMainTitle">
				<Font ss:Bold="0" ss:Italic="1" ss:Size="14" ss:Color="#FFFFFF"/>
			</Style>

			<Style ss:ID="izSubTitle" ss:Parent="izMainTitle">
				<Font ss:Size="16" ss:Color="#FFFFFF"/>
				<Interior ss:Color="#EA4335" ss:Pattern="Solid" />
				<Borders>
					<Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2" ss:Color="#AAAAAA"/>
				</Borders>
			</Style>


			<Style ss:ID="izSubHeader" ss:Parent="Default">
				<Font ss:Bold="1" ss:FontName="Calibri" x:Family="Swiss" ss:Size="14" ss:Color="#FFFFFF" />
				<Interior ss:Color="#F8C42E" ss:Pattern="Solid" />
			</Style>



			<Style ss:ID="izArrayHeader" ss:Parent="Default2">
				<Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1" />
				<Font ss:Bold="1" ss:FontName="Calibri" x:Family="Swiss" ss:Size="9"/>
				<Interior ss:Color="#CCCCCC" ss:Pattern="Solid" />
				<Borders>
					<Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1" ss:Color="#FFFFFF"/>
					<Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1" ss:Color="#FFFFFF"/>
					<Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1" ss:Color="#FFFFFF"/>
					<Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1" ss:Color="#FFFFFF"/>
				</Borders>
			</Style>

			<Style ss:ID="izArrayHeaderLeft" ss:Parent="izArrayHeader">
				<Alignment ss:Horizontal="Right" ss:Vertical="Center" ss:WrapText="1" />
			</Style>


			<Style ss:ID="izDeepness" ss:Parent="Default2">
				<Alignment ss:Horizontal="Left" ss:Vertical="Center"/>
				<Font ss:Bold="1" ss:FontName="Calibri" x:Family="Swiss" ss:Size="8" ss:Color="#888888" />
				<Interior ss:Color="#EEEEEE" ss:Pattern="Solid" />
				<Borders>
					<Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1" ss:Color="#E3DEDB"/>
				</Borders>
			</Style>


			<Style ss:ID="izLabel" ss:Parent="Default">
				<Font ss:Bold="1" ss:FontName="Calibri" x:Family="Swiss" ss:Size="10" ss:Italic="0"/>
				<Interior ss:Color="#F1F1F1" ss:Pattern="Solid" />
			</Style>


			<Style ss:ID="izAttLabel" ss:Parent="Default">
				<Font ss:Size="8" ss:Color="#FFFFFF"/>
				<Alignment ss:Horizontal="Left" ss:Vertical="Center" ss:WrapText="0" />
				<Interior ss:Color="#3FAB5B" ss:Pattern="Solid" />
			</Style>

			<Style ss:ID="izAttValue" ss:Parent="izAttLabel">
				<Alignment ss:Horizontal="Right" ss:Vertical="Center" ss:WrapText="0" />
				<Font ss:Bold="0" ss:Italic="1" ss:FontName="Calibri" x:Family="Swiss" ss:Size="8" ss:Color="#FFFFFF" />
			</Style>

		</Styles>
	</xsl:template>

	<xsl:template name="sheet-options">
		<xsl:param name="is-reference">false</xsl:param>
		<xsl:param name="is-project">false</xsl:param>
		<WorksheetOptions xmlns="urn:schemas-microsoft-com:office:excel">
			<PageSetup>
				<Header x:Margin="0.25" x:Data="&amp;R Date: &amp;D Time: &amp;T"/>
				<Footer x:Margin="0.25" x:Data="&amp;L Private and Confidential &amp;R Page &amp;P of &amp;N"/>
				<Layout x:Orientation="Landscape"/>
				<PageMargins x:Bottom="0.5" x:Left="0.5" x:Right="0.5" x:Top="0.5"/>
			</PageSetup>
			<Print>
				<ValidPrinterInfo/>
				<VerticalResolution>0</VerticalResolution>
			</Print>
			<Selected/>
			<FreezePanes/>
<!--
			<xsl:choose>
				<xsl:when test="$is-project = 'true'">
					<SplitHorizontal>2</SplitHorizontal>
					<TopRowBottomPane>2</TopRowBottomPane>
					<SplitVertical>4</SplitVertical>
					<LeftColumnRightPane>4</LeftColumnRightPane>
				</xsl:when>
				<xsl:otherwise>
					<SplitHorizontal>3</SplitHorizontal>
					<TopRowBottomPane>3</TopRowBottomPane>
					<SplitVertical>4</SplitVertical>
					<LeftColumnRightPane>4</LeftColumnRightPane>
				</xsl:otherwise>
			</xsl:choose>
-->
			<SplitHorizontal>2</SplitHorizontal>
			<TopRowBottomPane>2</TopRowBottomPane>
			<SplitVertical>4</SplitVertical>
			<LeftColumnRightPane>4</LeftColumnRightPane>

			<ActivePane>2</ActivePane>
			<DoNotDisplayGridlines/>
			<xsl:if test="$is-reference ='true'">
				<TabColorIndex>5</TabColorIndex>
			</xsl:if>
		</WorksheetOptions>
	</xsl:template>


	<xsl:template name="get-worksheet-title">
		<xsl:param name="title"></xsl:param>
		<xsl:variable name="ws-title">
			<xsl:value-of select="translate($title, '/\*[]?:', '--.()..')"/>
		</xsl:variable>
		<!--worksheet name fait 31 car max donc 28 à cause des ... que j'ajoute-->
		<xsl:choose>
			<xsl:when test="string-length($ws-title) &gt; 28">
				<xsl:value-of select="substring($ws-title, 1, 28)"/>...</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$ws-title"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>



</xsl:stylesheet>
