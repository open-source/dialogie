unit IzHashtable;
interface

uses classes, System.Generics.Collections;

type
	TIzHashtable = class(TStringlist)
	protected
		function GetObjByName(name:string):TObject;
		procedure SetObjByName(name:string; value:TObject);
	public
		// "Objects" the property name is taken so...
		property ObjByName[Name: String]: TObject read GetObjByName write SetObjByName;
		property Sorted default True;//we want this stringlist sorted at creation
		procedure freeContent();
    destructor Destroy();reintroduce;override;
end;

type
	TIzDictionary<T1 ; T2 : class> = class(TDictionary<T1, T2>)
	public
		procedure freeContent();
end;

type
	TIzList<T : class> = class(TList<T>)
	public
		procedure freeContent();
end;

implementation

uses
  System.SysUtils;

{ TObjStringlist }

	function TIzHashtable.GetObjByName(name: string): TObject;
	var
		i:integer;
	begin
		result := nil;
		i := IndexOf(name);

		if i = -1 then
			exit
		else
			result := Objects[i];
	end;


	procedure TIzHashtable.SetObjByName(name: string; value: TObject);
	//si name est d�j� pr�sent, on remplace, sinon on ajoute
	var
		i: integer;
	begin

		i := IndexOf(name);
		if i = -1 then
		begin
			self.AddObject(name, value);
			exit;
		end;
		self.Objects[i] := value;
	end;


	procedure TIzHashtable.freeContent();
		var
		nb : integer;
		cpt : integer;
		obj : TObject;
	begin
		nb := Count - 1;
		for cpt := nb downto 0 do
		begin
			obj := objects[cpt];
			if (obj <> nil) then
      begin
        objects[cpt] := nil;
        try
          obj.free;
        finally
        end;
      end;
		end;
  	clear;
  end;

destructor TIzHashtable.Destroy();
begin
  freeContent;
  inherited Destroy();
end;

//******************************************************************************
// TIzDictionary<T1,T2>
//******************************************************************************
procedure TIzDictionary<T1, T2>.freeContent();
var
  item : T2;
begin
  for item in Values do
  begin
    try
      item.Free;
    finally
    end;
  end;
  clear();
end;


//******************************************************************************
// TIzList<T>
//******************************************************************************
procedure TIzList<T>.freeContent();
var
  item : T;
begin
  for item in self do
  begin
    try
      item.Free;
    finally
    end;
  end;
  clear();
end;

end.

