unit objetMeteo;

interface

Uses sysutils,
  VclTee.Chart,
  Graphics,classes,dialogs,Grids,comctrls;

Const FMOIS : array[1..13] of Integer = (0,31*24,59*24,90*24,120*24,151*24,
                                         181*24,212*24,243*24,273*24,304*24,
                                         334*24,365*24);

Const Duree : Array[1..12] of Integer = (31,28,31,30,31,30,31,31,30,31,30,31);

Type
    TLigne = Class
        uid : string;
        Temperature : Integer;
        GlobalHorizontal : Integer;
        DiffusHorizontal : Integer;
        DirectNormal : Integer;
        DureeEnsoleillement : Integer;
        HumiditeRelative : Integer;
        Nebulosite : Integer;
        CoeffEnsoleillement : Double;
        VitesseVent : Integer;
        Mois,jour,Heure : Integer;
        GlobalTheorique : Integer;
        DiffusTheorique : Integer;
        TemperatureTemp : Integer;
        Constructor Create;
        Procedure Affecte(LigneTexte : String; ListeErreur : TStringList;
                 NumLigne : Integer);
    End;

    TFichier = Class
          Lignes : Array[1..8760] of TLigne;
          NomFichier : String;
          Modifie : Boolean;
          Constructor Create;
          Destructor Destroy; override;
          Procedure LoadFromfile(NomFichier : String);
          Procedure SaveTofile(NomFichier : String);
          Procedure VideCourbe(Graph : TChart);
          Procedure CalculeMoisJoursHeures;
          Procedure ControleMoisJoursHeures(Noeuds : TTreeNodes);
          Procedure ControleIndispensable(Noeuds : TTreeNodes);
          Procedure ControleValidite(Noeuds : TTreeNodes);
          Procedure RempliGrille(Grille : TStringGrid);
          Procedure Analyser(Arbre : TTreeView);
          Procedure AffecteIdent(Ident : String);
          Procedure Recopie(Deb, fin, Dest : Integer);
          Procedure CalculeTemperature;

    End;

implementation

uses
  System.UITypes,
  VCLTee.TeEngine,
  bibliometeo,
  IzUtilitaires;

Constructor TLigne.Create;
Begin
     Inherited Create;
     uid := '   ';
     Temperature := -999;
     GlobalHorizontal := -999;
     GlobalTheorique := -999;
     DiffusTheorique := -999;
     DiffusHorizontal := -999;
     DirectNormal := -999;
     DureeEnsoleillement := -999;
     HumiditeRelative := -999;
     Nebulosite := -999;
     CoeffEnsoleillement := -999;
     HumiditeRelative := -999;
     VitesseVent := -999;
     Mois := -999;
     jour := -999;
     Heure := -999;
End;

Constructor TFichier.Create;
Var
   i : Integer;
Begin
     inherited Create;
     NomFichier := '';
     For i := 1 to 8760 do
        Lignes[i] := TLigne.Create;
     Modifie := True;
End;

destructor TFichier.Destroy;
Var
   i : Integer;
begin
     For i := 1 to 8760 do
        Lignes[i].Free;
     inherited Destroy;
end;

Procedure TFichier.Recopie(Deb, fin, Dest : Integer);
Var
   i : Integer;
Begin
     For i := Fin downto Deb do
     Begin
          With Lignes[i] do
            if Dest+(i-deb) <= 8760 then
            Begin
               Lignes[Dest+(i-deb)].uid := Lignes[i].uid;
               Lignes[Dest+(i-deb)].temperature := Lignes[i].temperature;
               Lignes[Dest+(i-deb)].globalhorizontal := Lignes[i].globalhorizontal;
               Lignes[Dest+(i-deb)].globalTheorique := Lignes[i].globalTheorique;
               Lignes[Dest+(i-deb)].DiffusTheorique := Lignes[i].DiffusTheorique;
               Lignes[Dest+(i-deb)].diffushorizontal := Lignes[i].diffushorizontal;
               Lignes[Dest+(i-deb)].DirectNormal := Lignes[i].DirectNormal;
               Lignes[Dest+(i-deb)].DureeEnsoleillement := Lignes[i].DureeEnsoleillement;
               Lignes[Dest+(i-deb)].HumiditeRelative := Lignes[i].HumiditeRelative;
               Lignes[Dest+(i-deb)].Nebulosite := Lignes[i].Nebulosite;
               Lignes[Dest+(i-deb)].CoeffEnsoleillement := Lignes[i].CoeffEnsoleillement;
               Lignes[Dest+(i-deb)].VitesseVent := Lignes[i].VitesseVent;
               Lignes[Dest+(i-deb)].mois := mois;
               Lignes[Dest+(i-deb)].jour := jour;
               Lignes[Dest+(i-deb)].heure := heure;
            End;
     End;
End;

Procedure TFichier.AffecteIdent(Ident : String);
Var
   i : Integer;
Begin
     For i := 1 to 8760 do
     Begin
          Lignes[i].uid := Ident;
     End;
End;

Procedure TFichier.Analyser(Arbre : TTreeView);
Var
   Noeud : TTreeNode;
Begin
     Arbre.items.Clear;
     Arbre.items.AddChild(Nil,'Erreurs fatales');
     ControleIndispensable(Arbre.items);
     Arbre.items[0].text := 'Erreurs fatales ('+IntToStr(Arbre.items[0].count)
       +')';
     Arbre.items.AddChild(Nil,'Avertissements');
     ControleMoisJoursHeures(Arbre.items);
     Noeud := Arbre.items[0].getNextSibling;
     Noeud.text := 'Avertissements ('+IntToStr(Noeud.count)
       +')';
     Arbre.items.AddChild(Nil,'Remarques');
     ControleValidite(Arbre.items);
     Noeud := Noeud.getNextSibling;
     Noeud.text := 'Remarques ('+IntToStr(Noeud.count)
       +')';
     if Arbre.items.count > 1000 then
     Begin
          MessageDLG('Trop d''erreurs, l''analyse a �t� interrompue.',
            mtwarning,[mbOk],0);
     End;
End;

Procedure TFichier.ControleValidite(Noeuds : TTreeNodes);
Var
	 i : Integer;
	 Noeud : TTreeNode;
	 TempPrec : Integer;
	 TempIdent : String;
Begin
		 Noeud := Noeuds.GetFirstNode;
		 Noeud := Noeud.GetNextSibling;
		 Noeud := Noeud.GetNextSibling;
		 TempPrec := Lignes[1].temperature;
		 TempIdent := Lignes[1].uid;
		 For i := 1 to 8760 do
		 Begin
					if Noeuds.Count > 1000 then
						 Exit;
					if (Lignes[i].temperature < -300) or
						 (Lignes[i].temperature > 500)  then
						 Noeuds.addChild(Noeud,'Temperature hors limite � la ligne '
								 +IntToStr(i));
					if Abs(TempPrec-Lignes[i].Temperature) > 70 then
						 Noeuds.addChild(Noeud,'Changement brutal de temperature � la ligne '
								 +IntToStr(i));
					TempPrec := Lignes[i].Temperature;
					if (Lignes[i].GlobalHorizontal < 0) or
						 (Lignes[i].GlobalHorizontal > 500)  then
						 Noeuds.addChild(Noeud,'GlobalHorizontal hors limite � la ligne '
								 +IntToStr(i));
					if (Lignes[i].DiffusHorizontal < 0) or
						 (Lignes[i].DiffusHorizontal > 500)  then
						 Noeuds.addChild(Noeud,'DiffusHorizontal hors limite � la ligne '
								 +IntToStr(i));
					if TempIdent <> Lignes[i].uid then
						 Noeuds.addChild(Noeud,'Changement d''identificateur � la ligne '
								 +IntToStr(i));
					TempIdent := Lignes[i].uid;
		 End;
End;

Procedure TFichier.ControleIndispensable(Noeuds : TTreeNodes);
Var
	 i : Integer;
	 Noeud : TTreeNode;
	 ch : String;
Begin
		 Noeud := Noeuds.GetFirstNode;

		 For i := 1 to 8760 do
		 Begin
					if Noeuds.Count > 1000 then
						 Exit;
					ch := IntToStr(i);
					if Pos(' ',Lignes[i].uid) <> 0 then
						 Noeuds.addChild(Noeud,'L''identificateur doit avoir 3 caract�res � la ligne '
								 +ch);
					if (Lignes[i].temperature = -999) then
						 Noeuds.addChild(Noeud,'Temperature incorrecte � la ligne '
								 +ch);
					if (Lignes[i].GlobalHorizontal = -999) then
						 Noeuds.addChild(Noeud,'Global horizontal incorrect � la ligne '
								 +ch);
					if (Lignes[i].DiffusHorizontal = -999) then
             Noeuds.addChild(Noeud,'Diffus horizontal incorrect � la ligne '
                 +ch);
     End;
End;

Procedure TFichier.RempliGrille(Grille : TStringGrid);
Var
   i : Integer;
   Ligne : TLigne;
Begin
     Grille.RowCount := 8761;
     For i := 1 to 8760 do
     Begin
          Ligne := TLigne(Lignes[i]);
          Grille.Cells[0,i] := Ligne.uid;
          Grille.Cells[1,i] := IntToStr(Ligne.Temperature);
          Grille.Cells[2,i] := IntToStr(Ligne.GlobalHorizontal);
          Grille.Cells[3,i] := IntToStr(Ligne.DiffusHorizontal);
          Grille.Cells[4,i] := IntToStr(Ligne.DirectNormal);
          Grille.Cells[5,i] := IntToStr(Ligne.DureeEnsoleillement);
          Grille.Cells[6,i] := IntToStr(Ligne.HumiditeRelative);
          Grille.Cells[7,i] := IntToStr(Ligne.Nebulosite);
          Grille.Cells[8,i] := FloatToStr(Ligne.CoeffEnsoleillement);
          Grille.Cells[9,i] := IntToStr(Ligne.VitesseVent);
          Grille.Cells[10,i] := IntToStr(Ligne.Mois);
          Grille.Cells[11,i] := IntToStr(Ligne.jour);
          Grille.Cells[12,i] := IntToStr(Ligne.Heure);
     End;
     Modifie := False;
End;

Procedure TFichier.ControleMoisJoursHeures(Noeuds : TTreeNodes);
Var
   i : Integer;
   m,j,h : Integer;
   ListeErreur : TStringList;
   Noeud : TTreeNode;
Begin
     Noeud := Noeuds.GetFirstNode;
     Noeud := Noeud.getnextSibling;

     ListeErreur := TStringList.Create;
     m := 1;
     j := 1;
     h := 1;
     For i := 1 to 8760 do
     Begin
          if Noeuds.Count > 1000 then
             Exit;
          if (Lignes[i].heure <> h) then
             Noeuds.addChild(Noeud,'Heure incorrecte � la ligne '
                 +IntToStr(i));
          if (Lignes[i].jour <> j) then
             Noeuds.addChild(Noeud,'Jour incorrect � la ligne '
                 +IntToStr(i));
          if (Lignes[i].mois <> m) then
             Noeuds.addChild(Noeud,'Mois incorrect � la ligne '
                 +IntToStr(i));
          inc(h);
          if h > 24 then
          begin
               inc(j);
               h := 1;
          End;
          if j > Duree[m] then
          begin
               inc(m);
               j := 1;
          end;
     End;
     ListeErreur.Free;
End;

Procedure TFichier.CalculeMoisJoursHeures;
Var
   i : Integer;
   m,j,h : Integer;
Begin
     m := 1;
     j := 1;
     h := 1;
     For i := 1 to 8760 do
     Begin
          Lignes[i].heure := h;
          Lignes[i].jour := j;
          Lignes[i].mois := m;
          inc(h);
          if h > 24 then
          begin
               inc(j);
               h := 1;
          End;
          if j > Duree[m] then
          begin
               inc(m);
               j := 1;
          end;
     End;
End;

Procedure TFichier.SaveTofile(NomFichier : String);
Var
   Fich : File of SmallInt;
   Indice : Integer;
//   LigneTexte : string;
   //Chaine : String;
Begin
     AssignFile(Fich,NomFichier);
     Rewrite(Fich);
     For indice := 1 to 8760 do
          Write(Fich,Lignes[indice].Temperature);
     CloseFile(Fich);
End;

Procedure TFichier.LoadFromfile(NomFichier : String);
Var
   Fich : TextFile;
   Indice : Integer;
   LigneTexte : String;
   ListeErreur : TStringList;
	 oldFMode : byte;
Begin
		 ListeErreur := TStringList.Create;
		 AssignFile(Fich,NomFichier);
		 oldFMode := fileMode;
		 fileMode := fmOpenRead;
		 Reset(Fich);
		 Indice := 1;
		 While not eof(Fich) and (Indice <= 8760) do
     Begin
          ReadLn(Fich,LigneTexte);
          Try
             Lignes[indice].Affecte(LigneTexte,ListeErreur,Indice)
          Except
             On Exception do ShowMessage(IntToStr(Indice));
          End;
          Inc(Indice);
     End;
		 CloseFile(Fich);
		 FileMode := oldFMode;
     ListeErreur.Free;
     Modifie := True;
End;

Procedure TLigne.Affecte(LigneTexte : String; ListeErreur : TStringList; NumLigne : Integer);
Var
   s : String;
   Erreur : Integer;
Begin
     s := Copy(LigneTexte,1,3);
     uid := s;
     Delete(LigneTexte,1,3);

     s := Copy(LigneTexte,1,4);
     Val(s,Temperature,Erreur);
     if Erreur <> 0 then
     begin
        ListeErreur.Add('Erreur sur la valeur temp�rature de la ligne '
          +IntTOStr(NumLigne));
        Temperature := 0;
     end;
     Delete(LigneTexte,1,4);

     s := Copy(LigneTexte,1,4);
     Val(s,GlobalHorizontal,Erreur);
     if Erreur <> 0 then
     begin
        ListeErreur.Add('Erreur sur la valeur Global Horizontal de la ligne '
          +IntTOStr(NumLigne));
        GlobalHorizontal := -999;
     end;
     Delete(LigneTexte,1,4);

     s := Copy(LigneTexte,1,4);
     Val(s,DiffusHorizontal,Erreur);
     if Erreur <> 0 then
     begin
        ListeErreur.Add('Erreur sur la valeur Diffus Horizontal de la ligne '
          +IntTOStr(NumLigne));
        DiffusHorizontal := -999;
     end;
     Delete(LigneTexte,1,4);

     s := Copy(LigneTexte,1,4);
     Val(s,DirectNormal,Erreur);
     if Erreur <> 0 then
     begin
        DirectNormal := -999;
     end;
     Delete(LigneTexte,1,4);

     s := Copy(LigneTexte,1,4);
     Val(s,DureeEnsoleillement,Erreur);
     if Erreur <> 0 then
     begin
        DureeEnsoleillement := -999;
     end;
     Delete(LigneTexte,1,4);

     s := Copy(LigneTexte,1,3);
     Val(s,HumiditeRelative,Erreur);
     if Erreur <> 0 then
     begin
        //ListeErreur.Add('Erreur sur la valeur Humidite Relative de la ligne '
        //  +IntTOStr(NumLigne));
        HumiditeRelative := -999;
     end;
     Delete(LigneTexte,1,3);

     s := Copy(LigneTexte,1,3);
     Val(s,VitesseVent,Erreur);
     if Erreur <> 0 then
     begin
        //ListeErreur.Add('Erreur sur la valeur Vitesse Vent de la ligne '
        //  +IntTOStr(NumLigne));
        VitesseVent := -999;
     end;
     Delete(LigneTexte,1,3);

     s := Copy(LigneTexte,1,2);
     Val(s,mois,Erreur);
     if Erreur <> 0 then
     begin
        ListeErreur.Add('Erreur sur la valeur mois de la ligne '
          +IntTOStr(NumLigne));
        mois := -999;
     end;
     Delete(LigneTexte,1,2);

     s := Copy(LigneTexte,1,2);
     Val(s,jour,Erreur);
     if Erreur <> 0 then
     begin
        ListeErreur.Add('Erreur sur la valeur jour de la ligne '
          +IntTOStr(NumLigne));
        jour := -999;
     end;
     Delete(LigneTexte,1,2);

     s := Copy(LigneTexte,1,2);
     Val(s,heure,Erreur);
     if Erreur <> 0 then
     begin
        ListeErreur.Add('Erreur sur la valeur heure de la ligne '
          +IntTOStr(NumLigne));
        heure := -999;
     end;
     Delete(LigneTexte,1,2);

End;

Procedure TFichier.VideCourbe(Graph : TChart);
Var
   i : Integer;
Begin
     For i := Graph.SeriesCount-1 downto 0 do
         Graph.Series[i].free;

End;

Procedure TFichier.CalculeTemperature;
Var
   i,j,k : Integer;
   Moys,Min,Max,MoyMin,MoyMax : Array[1..12] of Double;
   Indice, Indice2 : Integer;
   ADistribuer : double;
   Variation : Double;
   Coeff : Double;
   indice3,Indice4,Indice5,Indice6,Indice7 : Integer;
   indice20,Indice21 : Integer;
   Posit : Integer;
   ok : Boolean;
   PosMin,PosMax,Mini,Maxi,Erreur : Integer;
   Moyenne : Integer;
   Moyd : Double;
   Correction : Double;
   NbIteration : Integer;
Begin
	posmin := 0;
	posmax := 0;

     With FormBiblioMeteo do
     Begin
          Ok := True;
          for i := 1 to 12 do
          begin
            Val(convertPoint(StringGridMoy.Cells[3,i]),Moys[i],Erreur);
            Ok := Ok and (Erreur = 0);
            Moys[i] := Moys[i]*10;
            if not Ok then
            Begin
              MessageDLG('Temp�rature moyenne incorrecte pour le mois ' + intToStr(i),
              mtwarning,[mbOk],0);
              Exit;
            End;

            Val(convertPoint(StringGridMoy.Cells[1,i]),MoyMin[i],Erreur);
            Ok := Ok and (Erreur = 0);
            MoyMin[i] := MoyMin[i]*10;
            if not Ok then
            Begin
               MessageDLG('Moyenne des minimales incorrecte pour le mois ' + intToStr(i),
                mtwarning,[mbOk],0);
               Exit;
            End;

            Val(convertPoint(StringGridMoy.Cells[2,i]),MoyMax[i],Erreur);
            Ok := Ok and (Erreur = 0);
            MoyMax[i] := MoyMax[i]*10;
            if not Ok then
            Begin
               MessageDLG('Moyennes des maximales incorrecte pour le mois ' + intToStr(i),
                mtwarning,[mbOk],0);
               Exit;
            End;
            Min[i] := ((MoyMin[i]+273)/1.0193)-273;
            Max[i] := ((MoyMax[i]+273)/0.979)-273;
          end;



     End;
     Indice := 1;
     For i := 1 to 12 do
     Begin
          Indice2 := Indice+2;
          Indice3 := Indice;
          Indice4 := Indice;
          Indice5 := Indice;
          Indice6 := Indice;
          Indice20 := Indice;
          //Rempli avec la moyenne
          For j := 1 to Duree[i] do
               For k := 1 to 24 do
               Begin
                   Lignes[Indice].Temperature := Round(Moys[i]);
                   inc(indice);
               End;
          // Applique les variations
//          ADistribuer := 0;
          For j := 3 to Duree[i]*24 do
          Begin
              //if (Lignes[indice2].CoeffEnsoleillement >=0) then
               //  Coeff := Lignes[indice2].GlobalHorizontal;
              // Partie d�perdition vers le ciel en fonction de l'ensoleillement
              //Variation := -(Lignes[indice2-1].Temperature/100)*Sqr(Coeff+0.3);

              //Ancienne version
              //Variation := -(Lignes[indice2-1].Temperature/100);//*Sqr(Coeff+1);
              //Nouvelle version

              // vieille m�thode

              //Variation := -((Lignes[indice2-1].Temperature+2730)/1000);//*Sqr(Coeff+1);
              //Variation := Variation + Lignes[indice2-1].GlobalHorizontal/(3000
              //   +FormMeteo.TrackBar1.Position*1000)*
              //     (400-Lignes[indice2-1].Temperature);
              //Variation := Variation + Lignes[indice2-1].GlobalHorizontal/(3000
              //   +2*1000)*
              //     (400-Lignes[indice2-1].Temperature);
             // Variation := Variation*3/(1+abs(Lignes[indice2-1].Temperature-Moys[i])/10000);


              //Nouvelle m�thode}
              Variation := -((Lignes[indice2-1].Temperature+2730)/1000);//*Sqr(Coeff+1);
              Variation := Variation + Lignes[indice2-1].GlobalHorizontal/(3000
                 +2*1000)*
                   (400-Lignes[indice2-1].Temperature);
              Variation := Variation*3/(1+abs(Lignes[indice2-1].Temperature-Moys[i])/10000);

              //Variation := Variation-Lignes[indice2-1].temperature/500;
              //Variation := Variation + Coeff/10;

              //Variation := Variation*1.1;


              //Variation := -( Lignes[indice2-1].Temperature/100)*Sqr(Coeff+0.3);

              // Partie �chauffement
              //Variation := Variation + Lignes[indice2-1].GlobalHorizontal/10000*
              //     (400- Lignes[indice2-1].Temperature);


              //Ancienne version
              //Variation := Variation * 2;

              Lignes[indice2].temperature := Lignes[indice2-1].temperature
                   + Round(Variation);
              //if Lignes[indice2].temperature < Min[i] then
              //    Lignes[indice2].temperature := Round(Min[i]);
              //if Lignes[indice2].temperature > Max[i] then
              //    Lignes[indice2].temperature := Round(Max[i]);
              //if Lignes[indice2].temperature < Min[i] then
              //    Lignes[indice2].temperature := Lignes[indice2].temperature+10;
              //if Lignes[indice2].temperature > Max[i] then
              //    Lignes[indice2].temperature := Lignes[indice2].temperature-10;
              inc(indice2);
          End;
          // Rectifie Moy Min Max
          Ok := False;
          NbIteration := 0;
          Repeat
                Indice21 := Indice20;
                Moyenne := 0;
                Mini := MaxLongint;
                For j := 1 to Duree[i]*24 do
                Begin
                     if Lignes[indice21].Temperature < Mini then
                             Mini := Lignes[indice21].Temperature;
                     if (indice21 mod 24) = 0 then
                     Begin
                          Moyenne := Moyenne + Mini;
                          Mini := Maxlongint;
                     End;
                     inc(indice21);
                End;
                Moyd := Moyenne / Duree[i];

                Correction := Moyd-MoyMin[i];
                Indice21 := Indice20;
                For j := 1 to Duree[i]*24 do
                Begin
                   Lignes[indice21].Temperature := Round(
                            Lignes[indice21].Temperature - Correction);
                   inc(indice21);
                End;
                Indice21 := Indice20;
                Moyenne := 0;
                Mini := -MaxLongint;
                For j := 1 to Duree[i]*24 do
                Begin
                     if Lignes[indice21].Temperature > Mini then
                             Mini := Lignes[indice21].Temperature;
                     if (indice21 mod 24) = 0 then
                     Begin
                          Moyenne := Moyenne + Mini;
                          Mini := -Maxlongint;
                     End;
                     inc(indice21);
                End;
                Moyd := Moyenne / Duree[i];
                if Abs(Moyd-MoyMax[i]) > 5 then
                Begin
                     Correction := (Moyd+500)/(MoyMax[i]+500);
                     Indice21 := Indice20;
                     For j := 1 to Duree[i]*24 do
                     Begin
                         Lignes[indice21].Temperature := Round(
                            (Lignes[indice21].Temperature+500)/Correction)-500;
                         inc(indice21);
                     End;
                End
                else
                  Ok := True;
                inc(NbIteration);
          Until Ok or (NbIteration > 100);

          // Rectifie Min Max
          Maxi := -Maxlongint;
          Mini := MaxLongint;
          For j := 1 to Duree[i] do
               For k := 1 to 24 do
               Begin
                   if Lignes[indice5].Temperature >= Max[i] then
                      Lignes[indice5].Temperature := Round(Max[i])-1;
                   if Lignes[indice5].Temperature <= Min[i] then
                      Lignes[indice5].Temperature := Round(Min[i])+1;

                   if Lignes[indice5].Temperature > Maxi then
                   Begin
                        PosMax := Indice5;
                        Maxi := Lignes[indice5].Temperature;
                   End;
                   if Lignes[indice5].Temperature < Mini then
                   Begin
                        PosMin := Indice5;
                        Mini := Lignes[indice5].Temperature;
                   End;
                   inc(indice5);
               End;

          Lignes[PosMin].Temperature := Round(Min[i]);
          Lignes[PosMax].Temperature := Round(Max[i]);

          // Rectifie la moyenne
          ADistribuer := 0;
          For j := 1 to Duree[i] do
               For k := 1 to 24 do
               Begin
                   ADistribuer := ADistribuer +
                      Lignes[Indice3].Temperature;
                   inc(indice3);
               End;
          ADistribuer := (Moys[i]*Duree[i]*24)-ADistribuer;
          if ADistribuer < 0 then
             For k := Round(ADistribuer) to 0 do
             Begin
                 NbIteration := 0;
                 Repeat
                       Posit := Random(Duree[i]*24-1);
                       Ok := (Lignes[Indice4+Posit].Temperature > Min[i])
                         and (Lignes[Indice4+Posit].Temperature < Max[i]);
                       if Ok then
                          Lignes[Indice4+Posit].Temperature :=
                             Lignes[Indice4+Posit].Temperature - 1;
                       inc(NbIteration);
                 until Ok or (NbIteration > 1000000);
             End;
          if ADistribuer > 0 then
             For k := 0 to Round(ADistribuer) do
             Begin
                 NbIteration := 0;
                 Repeat
                       Posit := Random(Duree[i]*24-1);
                       Ok := (Lignes[Indice4+Posit].Temperature < Max[i])
                         and (Lignes[Indice4+Posit].Temperature > Min[i]);
                       if Ok then
                          Lignes[Indice4+Posit].Temperature :=
                             Lignes[Indice4+Posit].Temperature + 1;
                       inc(NbIteration);
                 until Ok or (NbIteration > 1000);
             End;
          //Lissage
          for k := 1 to 1 do
          Begin
            Indice7 := Indice6;
            For j := 1 to Duree[i]*24 do
            begin
              Lignes[indice7].TemperatureTemp := Lignes[indice7].Temperature;
              inc(indice7);
            End;
            Indice7 := Indice6;
            For j := 2 to Duree[i]*24-1 do
            Begin
            if (indice7+1 <> PosMin) and (indice7+1 <> PosMax) then
               Lignes[Indice7+1].TemperatureTemp := (Lignes[Indice7].Temperature*2+
                 Lignes[Indice7+1].Temperature+Lignes[Indice7+2].Temperature*2) div 5;
               inc(indice7);
            End;
            Indice7 := Indice6;
            For j := 1 to Duree[i]*24 do
            begin
              Lignes[indice7].Temperature := Lignes[indice7].TemperatureTemp;
              inc(indice7);
            End;
          end;
          // Rectifie la moyenne
          indice7 := Indice6;
          ADistribuer := 0;
          For j := 1 to Duree[i] do
               For k := 1 to 24 do
               Begin
                   ADistribuer := ADistribuer +
                      Lignes[Indice7].Temperature;
                   inc(indice7);
               End;
          indice7 := Indice6;
          ADistribuer := (Moys[i]*Duree[i]*24)-ADistribuer;
          if ADistribuer < 0 then
             For k := Round(ADistribuer) to 0 do
             Begin
                 Repeat
                       Posit := Random(Duree[i]*24-1);
                       Ok := (Lignes[Indice7+Posit].Temperature > Min[i])
                         and (Lignes[Indice7+Posit].Temperature < Max[i]);
                       if Ok then
                          Lignes[Indice7+Posit].Temperature :=
                             Lignes[Indice7+Posit].Temperature - 1;
                 until Ok;
             End;
          if ADistribuer > 0 then
             For k := 0 to Round(ADistribuer) do
             Begin
                 Repeat
                       Posit := Random(Duree[i]*24-1);
                       Ok := (Lignes[Indice7+Posit].Temperature < Max[i])
                         and (Lignes[Indice7+Posit].Temperature > Min[i]);
                       if Ok then
                          Lignes[Indice7+Posit].Temperature :=
                             Lignes[Indice7+Posit].Temperature + 1;
                 until Ok;
             End;    
     End;
End;


end.
