unit CalculMeteo;

interface

uses
  Math,
  ObjetMeteo,
  dialogs,
  sysutils;


type
  TListeDecl = array [1 .. 13] of double;

  TGefSol = class
    DiffusM, DirectM, Diffus, Direct : double;
    Mois : Integer;
    HeureSolaire : double;
    HeureLegale : double;
    RayGlobal, RayGlobalM : array [1 .. 12] of double;
    LongueurJour : array [1 .. 12] of double;

    constructor Create;

    procedure InitCoeff(Lattitude, Inclinaison, Orientation : double; Matrice : TFichier);
    destructor Destroy; override;
    function CalculSoleil(jour : Integer; Lattitude, HeureSolaire, Inclinaison, Orientation : double) : double;
    procedure Calcul(Lattitude, Inclinaison, Orientation, FActeurSolaire : double; Matrice : TFichier);
    function sindec(jour : Integer) : double;
    function cosdec(sdec : double) : double;
    function quantieme(Mois, jour : Integer) : Integer;
    procedure jourdec(Mois : Integer; var sdec : double; var cdec : double; var jour : Integer);
    function coef_calc_sol(J : Integer) : double;
    function hcoucher(lat : double; sd, cd : double) : double;
    procedure CalculO(Lattitude, Inclinaison, Orientation, FActeurSolaire : double);
    procedure CalculSoleilO(Mois : Integer; Lattitude, HeureSolaire, Inclinaison, Orientation, FActeurSolaire : double);
  end;

implementation

uses
  IzConstantes;

constructor TGefSol.Create;
begin
  inherited Create;
end;

destructor TGefSol.Destroy;
begin
  inherited Destroy;
end;

function TGefSol.hcoucher(lat : double; sd, cd : double) : double;
var
  heure, calc : double;
begin
  heure := -sin(lat) * sd / (cd * cos(lat));
  if (abs(heure) < 1) then
    if (heure > -0.000001) and (heure < 0.00001) then
      hcoucher := 90
    else
    begin
      calc := 180 / pi * arctan(sqrt(1 - heure * heure) / heure);
      hcoucher := calc;
      if heure < 0 then
        hcoucher := abs(abs(calc) - 180);
    end
  else
    hcoucher := 180;
end;

procedure TGefSol.jourdec(Mois : Integer; var sdec : double; var cdec : double; var jour : Integer);
begin
  case Mois of
    1 :
      begin
        sdec := -0.3554503037;
        cdec := 0.9346951811;
        jour := 31;
      end;
    2 :
      begin
        sdec := -0.2254129449;
        cdec := 0.9742633136;
        jour := 28;
      end;
    3 :
      begin
        sdec := -0.03616066633;
        cdec := 0.9993459892;
        jour := 31;
      end;
    4 :
      begin
        sdec := 0.1655955223;
        cdec := 0.9861937553;
        jour := 30;
      end;
    5 :
      begin
        sdec := 0.3202909478;
        cdec := 0.9473192222;
        jour := 31;
      end;
    6 :
      begin
        sdec := 0.3913454909;
        cdec := 0.9202438301;
        jour := 30;
      end;
    7 :
      begin
        sdec := 0.3628123433;
        cdec := 0.9318622235;
        jour := 31;
      end;
    8 :
      begin
        sdec := 0.2398337171;
        cdec := 0.9708139823;
        jour := 31;
      end;
    9 :
      begin
        sdec := 0.05487979723;
        cdec := 0.9984929684;
        jour := 30;
      end;
    10 :
      begin
        sdec := -0.1474526115;
        cdec := 0.9890691216;
        jour := 31;
      end;
    11 :
      begin
        sdec := -0.3121365824;
        cdec := 0.9500372382;
        jour := 30;
      end;
    12 :
      begin
        sdec := -0.3903761351;
        cdec := 0.9206554584;
        jour := 31;
      end;
  end;
end;


function TGefSol.quantieme(Mois, jour : Integer) : Integer;
begin
  quantieme := jour;
  case Mois of
    1 :
      quantieme := jour;
    2 :
      quantieme := jour + 31;
    3 :
      quantieme := jour + 59;
    4 :
      quantieme := jour + 90;
    5 :
      quantieme := jour + 120;
    6 :
      quantieme := jour + 151;
    7 :
      quantieme := jour + 181;
    8 :
      quantieme := jour + 212;
    9 :
      quantieme := jour + 243;
    10 :
      quantieme := jour + 273;
    11 :
      quantieme := jour + 304;
    12 :
      quantieme := jour + 334;
  end;
end;


function TGefSol.sindec(jour : Integer) : double; { ATTENTION: toutes les fonctions trigo  et }
begin { toutes les fonctions inv  utilisent les rad!! }
  if jour <> 81 then
    sindec := 0.4 * sin(2 * pi / 365 * (jour - 80))
  else
    sindec := 0;
end;

function TGefSol.cosdec(sdec : double) : double;
begin
  cosdec := sqrt(1 - sdec * sdec);
end;

function TGefSol.coef_calc_sol(J : Integer) : double;
begin
  coef_calc_sol := 1 + 0.034 * cos(0.986 * (J - 3) * pi / 180);
end;

function TGefSol.CalculSoleil(jour : Integer; Lattitude, HeureSolaire,
  Inclinaison, Orientation : double) : double;
var
  SinDecl, cosDecl, SinLat, CosLat, Eclairement, CosB : double;
  Hauteur, Azimuth, CoeffDir, CoeffDiff : double;
  DiffusHorizontal, FluxDiffus, CoeffCalc, calc : double;
  Albedo : double;
begin
  Albedo := 0.2;
  SinDecl := sindec(jour);
  cosDecl := cosdec(SinDecl);

  CoeffCalc := 1;
  SinLat := sin(Lattitude / 180 * pi);
  CosLat := cos(Lattitude / 180 * pi);
  Hauteur := SinLat * SinDecl + CosLat * cosDecl * cos(HeureSolaire / 180 * pi);
  Hauteur := ArcSin(Hauteur);

  if (Hauteur < 1.570796) or (Hauteur > 1.570797) then
  begin
    Azimuth := (SinLat * cosDecl * cos(HeureSolaire * pi / 180)
      - CosLat * SinDecl) / cos(Hauteur);
    if Azimuth > 1 then
      Azimuth := 1;
    if Azimuth < -1 then
      Azimuth := -1;
    if (Azimuth <> 0) then
    begin
      calc := arctan(sqrt(1 - Azimuth * Azimuth) / Azimuth);
      if (Azimuth < 0) then
        calc := abs(calc) - pi;
      if HeureSolaire < 0 then
        Azimuth := -1 * abs(calc)
      else
        Azimuth := abs(calc)
    end;
  end
  else
    Azimuth := 0;

     // Ciel Normal
  if Hauteur > 0 then
    Eclairement := 1192 * exp(-0.23 / (sin(Hauteur)))
  else
    Eclairement := 0;

  CosB := cos(Hauteur) * sin(Inclinaison / 180 * pi) * cos(Azimuth - (Orientation / 180 * pi))
    + sin(Hauteur) * cos(Inclinaison / 180 * pi);

  CoeffDir := 0;
  CoeffDiff := 0;
  Direct := 0;
  Diffus := 0;
  if (CosB > 0) and (Hauteur > 0) then
  begin
    Direct := Direct + Eclairement * CosB * CoeffCalc;
    DirectM := DirectM + (1 - CoeffDir) * Eclairement * CosB * (1 - CoeffDir) * CoeffCalc
  end;
  if (Hauteur > 0) then
  begin
    FluxDiffus := 62.5 * (1 + cos(Inclinaison / 180 * pi))
      * exp(0.4 * ln(sin(Hauteur))) +
      (Albedo * 1080 * (1 - cos(Inclinaison / 180 * pi))
      * exp(1.22 * ln(sin(Hauteur))));

    DiffusHorizontal := 125 * Power(sin(Hauteur), 0.4);

        // Ciel Normal
    Diffus := Diffus + FluxDiffus * CoeffCalc;
    DiffusM := DiffusM + FluxDiffus * (1 - CoeffDiff) * CoeffCalc;
    Diffus := DiffusHorizontal;
  end
  else
  begin
    Direct := 0;
    Diffus := 0;
  end;
  CalculSoleil := Hauteur;
end;

procedure TGefSol.Calcul;
var
  i, J : Integer;
  Chaine : string;
  sd, cd : double;
  HeureCouche : double;
  Total : Integer;
  Mois, JTemp : Integer;
  nbHeures : array [1 .. 12] of Integer;
  Hauteur : double;
begin
  for i := 1 to 12 do
  begin
    RayGlobal[i] := 0;
    RayGlobalM[i] := 0;
  end;
     // Mois := 1;
  Direct := 0;
  Diffus := 0;
  DirectM := 0;
  DiffusM := 0;
  HeureLegale := 0;

     // Indice := 0; // MENSUEL

  sd := sindec(1);
  cd := cosdec(sd);
  HeureSolaire := -hcoucher(Lattitude / 180 * pi, sd, cd);
  HeureCouche := -HeureSolaire;
  HeureSolaire := -180;
     // NbHeure := 0;
  Mois := 1;
  JTemp := 1;
  for i := 1 to 12 do
    nbHeures[i] := 0;
  for i := 1 to 365 do
  begin
    for J := 1 to 24 do
    begin
      Hauteur := CalculSoleil(i, Lattitude, HeureSolaire, Inclinaison / 180 * pi, Orientation);
      if (HeureSolaire >= -HeureCouche) and
        (HeureSolaire <= HeureCouche) then
      begin
        Matrice.Lignes[(i - 1) * 24 + J].GlobalTheorique :=
          Round(Diffus + Direct * 36 / 100 * 0.76);
        Matrice.Lignes[(i - 1) * 24 + J].DiffusTheorique :=
          Round(Diffus * 36 / 100);
        if (Matrice.Lignes[(i - 1) * 24 + J].GlobalHorizontal / sin(Hauteur)) > 233 then
          inc(nbHeures[Mois]);
      end
      else
      begin
        Matrice.Lignes[(i - 1) * 24 + J].GlobalTheorique := 0;
        Matrice.Lignes[(i - 1) * 24 + J].DiffusTheorique := 0;
      end;
      Direct := 0;
      Diffus := 0;
      HeureSolaire := HeureSolaire + 15;
      if J = 24 then
      begin
        sd := sindec(i);
        cd := cosdec(sd);
        HeureSolaire := -hcoucher(Lattitude / 180 * pi, sd, cd);
        HeureCouche := -HeureSolaire;
        HeureSolaire := -180;
      end;
    end;
    inc(JTemp);
    if JTemp = DureeMois[Mois] + 1 then
    begin
      inc(Mois);
      JTemp := 1;
    end;
  end;
  Chaine := '';
  Total := 0;
  for i := 1 to 12 do
  begin
    Chaine := Chaine + IntToStr(nbHeures[i]) + chr(13);
    Total := Total + nbHeures[i];
  end;
  Chaine := Chaine + IntToStr(Total);
  HeureSolaire := -180;
end;

procedure TGefSol.InitCoeff;
var
  i, J : Integer;
  sd, cd : double;
  HeureCouche : double;
  Mois, JTemp : Integer;
  nbHeures : array [1 .. 12] of Integer;
begin
  for i := 1 to 12 do
  begin
    RayGlobal[i] := 0;
    RayGlobalM[i] := 0;
  end;
  Direct := 0;
  Diffus := 0;
  DirectM := 0;
  DiffusM := 0;
  HeureLegale := 0;

  sd := sindec(1);
  cd := cosdec(sd);
  HeureSolaire := -hcoucher(Lattitude / 180 * pi, sd, cd);
  HeureCouche := -HeureSolaire;
  HeureSolaire := -180;
     // NbHeure := 0;
  Mois := 1;
  JTemp := 1;
  for i := 1 to 12 do
    nbHeures[i] := 0;
  for i := 1 to 365 do
  begin
    for J := 1 to 24 do
    begin
      if (HeureSolaire >= -HeureCouche) and
        (HeureSolaire <= HeureCouche) then
      begin
        Matrice.Lignes[(i - 1) * 24 + J].CoeffEnsoleillement := 0;
      end
      else
      begin
        Matrice.Lignes[(i - 1) * 24 + J].CoeffEnsoleillement := -1;
      end;
      HeureSolaire := HeureSolaire + 15;
      if J = 24 then
      begin
        sd := sindec(i);
        cd := cosdec(sd);
        HeureSolaire := -hcoucher(Lattitude / 180 * pi, sd, cd);
        HeureCouche := -HeureSolaire;
        HeureSolaire := -180;
                    // inc(jour);
      end;
    end;
    inc(JTemp);
    if JTemp = DureeMois[Mois] + 1 then
    begin
      inc(Mois);
      JTemp := 1;
    end;
  end;
  HeureSolaire := -180;
end;

procedure TGefSol.CalculO;
var
  Indice, jour, i : Integer;
  Fich : TextFile;
// Chaine : String;
  sd, cd : double;
  HeureCouche : double;
begin
  for i := 1 to 12 do
  begin
    RayGlobal[i] := 0;
    RayGlobalM[i] := 0;
  end;
  Mois := 1;
  Direct := 0;
  Diffus := 0;
  DirectM := 0;
  DiffusM := 0;
  HeureLegale := 0;

  Indice := 0; // MENSUEL

  jourdec(Mois, sd, cd, jour);

     // Test
  HeureSolaire := -180 + hcoucher(Lattitude / 180 * pi, sd, cd);

  HeureCouche := -HeureSolaire;

  HeureSolaire := -180;
  if Indice = 0 then
    while Mois <> 13 do
    begin
      LongueurJour[Mois] := 24 - (2 * abs(HeureCouche) / 15);
          // LongueurJour[Mois] := (2*Abs(HeureCouche)/15);

      CalculSoleilO(Mois, Lattitude, HeureSolaire, Inclinaison / 180 * pi, Orientation, FActeurSolaire);
      HeureSolaire := HeureSolaire + 15;
      if HeureSolaire >= 180 then
      begin
        RayGlobal[Mois] := RayGlobal[Mois] + (Direct + Diffus);
        RayGlobalM[Mois] := RayGlobalM[Mois] + (DirectM + DiffusM);

        Direct := 0;
        Diffus := 0;
        DirectM := 0;
        DiffusM := 0;
        inc(Mois);
        jourdec(Mois, sd, cd, jour);
        HeureSolaire := -180 + hcoucher(Lattitude / 180 * pi, sd, cd);
        HeureCouche := -HeureSolaire;
        HeureSolaire := -180;
      end;
    end;
  if Indice <> 0 then
    CloseFile(Fich);
  Mois := 1;
  HeureSolaire := -180;
end;

procedure TGefSol.CalculSoleilO(Mois : Integer; Lattitude, HeureSolaire,
  Inclinaison, Orientation, FActeurSolaire : double);
var
  SinDecl, cosDecl, SinLat, CosLat, Eclairement, CosB : double;
  CNorm : double;
  Hauteur, Azimuth : double;
// CoeffDir,CoeffDiff : Double;
  FluxDiffus, CoeffCalc, calc : double;
  Albedo : double;
  jour : Integer;
  FSReel : double;
begin
  Albedo := 0.2;
  CNorm := 0.02792567874; { 1.6*pi/180 } // ciel Normal
  jourdec(Mois, SinDecl, cosDecl, jour);
  CoeffCalc := 1;
  SinLat := sin(Lattitude / 180 * pi);
  CosLat := cos(Lattitude / 180 * pi);
  Hauteur := SinLat * SinDecl + CosLat * cosDecl * cos(HeureSolaire / 180 * pi);
  Hauteur := ArcSin(Hauteur);

  if (Hauteur < 1.570796) or (Hauteur > 1.570797) then
  begin
    Azimuth := (SinLat * cosDecl * cos(HeureSolaire * pi / 180)
      - CosLat * SinDecl) / cos(Hauteur);
    if Azimuth > 1 then
      Azimuth := 1;
    if Azimuth < -1 then
      Azimuth := -1;
    calc := arctan(sqrt(1 - Azimuth * Azimuth) / Azimuth);
    if (Azimuth < 0) then
      calc := abs(calc) - pi;
    if HeureSolaire < 0 then
      Azimuth := -1 * abs(calc)
    else
      Azimuth := abs(calc)
  end
  else
    Azimuth := 0;

     // Ciel Normal
  if Hauteur > 0 then
    Eclairement := 1230 * exp(-1 / (3.8 * sin(Hauteur + CNorm)))
  else
    Eclairement := 0;

  CosB := cos(Hauteur) * sin(Inclinaison) * cos(Azimuth - (Orientation / 180 * pi))
    + sin(Hauteur) * cos(Inclinaison);

  if (CosB > 0) and (Hauteur > 0) then
  begin
    if FActeurSolaire <> -1 then
    begin
      if CosB > 0.8 then
        FSReel := FActeurSolaire
      else
        FSReel := FActeurSolaire * (2.5 * CosB * (1 - (CosB / 1.6)));
    end
    else
      FSReel := 1;

    Direct := Direct + Eclairement * CosB * CoeffCalc * FSReel;
    DirectM := DirectM + Eclairement * CosB * CoeffCalc * FSReel;

  end;
  if (Hauteur > 0) then
  begin
    if FActeurSolaire <> -1 then
    begin
      FSReel := 0.9 * FActeurSolaire;
    end
    else
      FSReel := 1;
    if Hauteur > 0 then
      FluxDiffus := 62.5 * (1 + cos(Inclinaison))
        * exp(0.4 * ln(sin(Hauteur))) +
        (Albedo * 540 * (1 - cos(Inclinaison))
        * exp(1.22 * ln(sin(Hauteur))))
    else
      FluxDiffus := 0;

    Diffus := Diffus + FluxDiffus * FSReel;
    DiffusM := DiffusM + FluxDiffus * FSReel;
  end;
end;


end.
