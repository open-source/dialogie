unit creerSauvegarde;

interface

uses
  Classes, SysUtils;

const
  TagVersion = 'version';

type
  TCreationSauvegarde = class(TStringList)
  private
  public
    function OuvreTag(Chaine: string): integer;
    function FermeTag(Chaine: string): integer;
    procedure Add(Bool: boolean); reintroduce; overload;
    procedure Add(Reel: double); reintroduce; overload;
    procedure Add(Entier: integer); reintroduce; overload;
    procedure AddVersion(Numversion : Integer);
  end;

implementation

uses
   IzUtilitaires;



procedure TCreationSauvegarde.AddVersion(Numversion : Integer);
begin
  OuvreTag(TagVersion);
  Add(NumVersion);
  FermeTag(TagVersion);
end;

function TCreationSauvegarde.OuvreTag(Chaine: string): integer;
begin
  Result := Add('<' + Chaine + '>');
end;

function TCreationSauvegarde.FermeTag(Chaine: string): integer;
begin
  Result := Add('</' + Chaine + '>');
end;

procedure TCreationSauvegarde.Add(Bool: boolean);
begin
  if Bool then
    add('1')
  else
    add('0');
end;

procedure TCreationSauvegarde.Add(Reel: double);
begin
  add(getFullString(reel));
end;

procedure TCreationSauvegarde.Add(Entier: integer);
begin
  add(IntToStr(Entier));
end;

end.

