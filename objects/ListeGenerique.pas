unit ListeGenerique;

interface

uses
  Generics.Collections;

type

  TListeGenerique<T: class> = class(TObjectList<T>)
    procedure ClearAndFreeItems;
    procedure Assign(Source: TListeGenerique<T>);
  end;

implementation

procedure TListeGenerique<T>.ClearAndFreeItems;
var
  i: integer;
begin
  if not OwnsObjects then
    for i := Count - 1 downto 0 do
      T(items[i]).Free;
  Clear;
end;

procedure TListeGenerique<T>.Assign(Source: TListeGenerique<T>);
var
  i: integer;
begin
  Clear;
  Capacity := Source.Capacity;
  for i := 0 to Source.Count - 1 do
    Add(Source[i]);
end;

end.
