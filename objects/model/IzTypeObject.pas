unit IzTypeObject;

interface
	uses
		contnrs, Classes, IzObjectUtil;

	type TTypeObject = class(TObject)
		private
			innerList : TObjectList;

			function getCount() : integer;

		public
			constructor Create();virtual;
			destructor Destroy();override;

			function getObject(rank : integer) : TIzPercentObject;
			function addObject(equipement  : TIzPercentObject) : integer;
			function removeObject(equipement : TIzPercentObject) : integer;
			function remove(rank : integer) : integer; //overload;
			function indexOf(anObject : TIzPercentObject) : integer; //overload;
			procedure removeAll();
			function getLabels() : TStringList;

			property nb : integer read getCount;
	end;

implementation

uses
  System.Types,
  System.SysUtils;

	constructor TTypeObject.Create();
	begin
		inherited create();
		innerList := TObjectList.Create(true);
	end;

	destructor TTypeObject.Destroy();
	begin
		innerList.Free;
		inherited destroy();
	end;

	function TTypeObject.getObject(rank : integer) : TIzPercentObject;
	begin
		getObject := nil;
		if (rank < 0) or (rank >= nb)  then
			exit;
		getObject := TIzPercentObject(innerList.Items[rank]);
	end;


	function TTypeObject.addObject(equipement  : TIzPercentObject) : integer;
	var rank : integer;
	begin
		if (equipement = nil) then
		begin
			addObject := -1;
			exit;
		end;

		rank := innerList.IndexOf(equipement);
		if (rank <> -1) then
		begin
			addObject := rank;
		end
		else
		begin
			addObject := innerList.Add(equipement);
		end;
	end;


	function TTypeObject.removeObject(equipement : TIzPercentObject) : integer;
	var rank : integer;
	begin
		if (equipement = nil) then
		begin
			removeObject := -1;
			exit;
		end;

		rank := innerList.IndexOf(equipement);
		if (rank = -1) then
		begin
			removeObject := -1;
			exit;
		end
		else
			removeObject := innerList.remove(equipement);
	end;

	function TTypeObject.remove(rank : integer) : integer;
	begin
		if (rank < 0) or (rank >= innerList.Count ) then
		begin
			remove := -1;
			exit;
		end;

		remove := removeObject(TIzPercentObject(innerList.Items[rank]));
	end;

	procedure TTypeObject.removeAll();
	begin
		innerList.Clear;
	end;

	function TTypeObject.getCount() : integer;
	begin
		getCount := 0;
		if (innerList <> nil) then
			getCount := innerList.Count;

	end;

	function TTypeObject.indexOf(anObject : TIzPercentObject) : integer;
	begin
		result := innerList.IndexOf(anObject);
	end;

	function TTypeObject.getLabels() : TStringList;
	//� l'appelant de lib�rer par la suite
	var
		ret : TStringList;
		i: integer;
	begin
		ret := TStringList.Create;
		if (getCount = 0) then
		begin
			result := ret;
			exit
		end;

		for i := 0 to innerList.Count - 1 do
		begin
			ret.Add(intToStr(i+1) + ' - ' + getObject(i).getLabel);
		end;
		result := ret;
	end;

end.
