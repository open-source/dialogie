unit IzObjectPersist;

interface

uses
  Classes,
  IzObject,
  Xml.XMLIntf,

  GestionSauvegarde,
  CreerSauvegarde;

type
  TObjectPersist = class(TIzObject)

  protected
    procedure innerLoadList(liste : TStringList); overload; virtual;
    procedure innerLoadList(liste : TSauvegarde); overload; virtual;
    procedure innerSaveList(liste : TStringList); overload; virtual;
    procedure innerSaveList(liste : TCreationSauvegarde); overload; virtual;
    procedure innerLoadXml(node : IXMLNode); virtual;
    procedure innerSaveXml(node : IXMLNode); virtual;

  public
    procedure loadList(liste : TStringList); overload;
    procedure loadList(liste : TSauvegarde); overload;
    procedure saveList(liste : TStringList);
    procedure loadXml(node : IXMLNode);
    procedure loadParentXml(parentNode : IXMLNode);
    procedure saveXml(parentNode : IXMLNode);

    constructor Create(); reintroduce; override;
  end;


type
  TObjectPersistClass = class of TObjectPersist;


implementation

uses
  SysUtils,
  IzUtilitaires;


constructor TObjectPersist.Create();
begin
  inherited Create();
end;


procedure TObjectPersist.loadList(liste : TStringList);
var
  hasTag : boolean;
  sTag : string;
begin
  hasTag := isTag(liste, innerTagName);
  if not(hasTag) and (innerTagName <> ClassName) then
  begin
    hasTag := isTag(liste, ClassName);
    if hasTag then
      sTag := className;
  end
  else
  begin
    sTag := innerTagName;
  end;
  if hasTag then
    removeFirst(liste);

  innerLoadList(liste);

  if hasTag then
  begin
    if not(isEndTag(liste, sTag)) then
    begin
      raise Exception.Create('start tag "' + innerTagName + '" with no end tag.');
      exit;
    end;
    removeFirst(liste); // pour le tag de fin
  end;

end;


procedure TObjectPersist.loadList(liste : TSauvegarde);
var
  hasTag : boolean;
  sTag : string;
  s : string;
begin
  s := liste.GetLigneSansBouger;
  hasTag := isTag(s, innerTagName);
  if not(hasTag) and (innerTagName <> ClassName) then
  begin
    hasTag := isTag(s, ClassName);
    if hasTag then
      sTag := className;
  end
  else
  begin
    sTag := innerTagName;
  end;

  if hasTag then
    liste.getLigne;

  innerLoadList(liste);

  if hasTag then
  begin
    if not(isEndTag(liste.GetLigneSansBouger, sTag)) then
      exit;
    liste.getLigne(); // pour le tag de fin
  end;

end;


procedure TObjectPersist.loadParentXml(parentNode: IXMLNode);
var
  node : IXmlNode;
begin
  node := parentNode.ChildNodes.FindNode(innerTagName);
  if node = nil then
    exit;
  loadXml(node);
end;

procedure TObjectPersist.saveList(liste : TStringList);
begin
  liste.add('<' + innerTagName + '>');
  innerSaveList(liste);
  liste.add('</' + innerTagName + '>');
end;


procedure TObjectPersist.loadXml(node : IXMLNode);
begin
  if node.NodeName <> innerTagName then
  begin
    exit;
  end;
  innerLoadXml(node);
end;


procedure TObjectPersist.saveXml(parentNode : IXMLNode);
var
  node : IXMLNode;
begin
  if (parentNode = nil) then
  begin
    exit;
  end;
{$IFDEF DEBUG}
  if innerTagName = className then
    raise Exception.Create(className + ' innerTagName is className');
{$ENDIF}

  node := parentNode.AddChild(innerTagName);
  if (node = nil) then
  begin
    exit;
  end;
  innerSaveXml(node);
end;


procedure TObjectPersist.innerLoadXml(node : IXMLNode);
begin
{$IFDEF DEBUG}
  raise Exception.Create(className + '.innerLoadXml NYI');
{$ENDIF}
end;


procedure TObjectPersist.innerSaveXml(node : IXMLNode);
begin
{$IFDEF DEBUG}
  raise Exception.Create(className + '.innerSaveXml NYI');
{$ENDIF}
end;


procedure TObjectPersist.innerLoadList(liste : TStringList);
begin

end;


procedure TObjectPersist.innerLoadList(liste : TSauvegarde);
begin

end;

procedure TObjectPersist.innerSaveList(liste : TStringList);
begin

end;


procedure TObjectPersist.innerSaveList(liste : TCreationSauvegarde);
begin

end;

end.
