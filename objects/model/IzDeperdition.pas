unit IzDeperdition;

interface

uses
	IzObjectPersist, IzHashtable, Classes, Xml.XMLIntf;

	type TDeperditionDetail = class(TObjectPersist)
		protected
			Procedure innerSaveList(Liste : TStringList);override;
			procedure innerLoadList(Liste : TStringList);override;

      procedure innerLoadXml(node : IXMLNode); override;
      procedure innerSaveXml(node : IXMLNode); override;
		public
			libelle : string;
			valeur : double;

			Constructor Create;override;
	end;

type
	TDeperdition = class(TObjectPersist)
  private
		protected
			Procedure innerSaveList(Liste : TStringList);override;
			procedure innerLoadList(Liste : TStringList);override;

      procedure innerLoadXml(node : IXMLNode); override;
      procedure innerSaveXml(node : IXMLNode); override;

			function getPerteVitrage() : double;
			function getPertePorte() : double;
			function getPerteToiture() : double;
			function getPerteMur() : double;
			function getPerteSol() : double;
			function getPerteVentilation() : double;
			function getPertePermeabilite() : double;

		public
			apportEquipement : double;
			apportOccupant: double;
			apportSolaire: double;
			apportUtile: double;
			apportTotal: double;
			apportPerdu: double;

			besoinChauffNet: double;
			consoChauffage: double;
			besoinEcsNet: double;
			consoEcs: double;

			deperdition: double;
			perteAux: double;
			perteChauff: double;
			perteEcs: double;

			coeffPerteVentilation: double;
			coeffPertePermeabilite: double;
			coeffPerteToiture: double;
			coeffPerteVitrage: double;
			coeffPerteMur: double;
			coeffPertePorte: double;
			coeffPerteSol: double;

			coeffGvPerte: double;

			pertePontThermique: double;

			chaleurTotale: double;

			coeffDeperdition : double;

			detailPerteVitrage : TIzHashtable;
			detailPerteParoi : TIzHashtable;
			detailPertePorte : TIzHashtable;

			Constructor Create;override;
			destructor Destroy;override;

      procedure reset;


			function getCoeffProcess() : double;virtual;

			property perteVitrage : double read getPerteVitrage;
			property pertePorte : double read getPertePorte;
			property perteToiture : double read getPerteToiture;
			property perteMur : double read getPerteMur;
			property perteSol : double read getPerteSol;
			property perteVentilation : double read getPerteVentilation;
			property pertePermeabilite : double read getPertePermeabilite;
	end;


	//classe pour obtenir les déperditions en pourcentage
	type
		TDeperditionPercent = class(TDeperdition)
			public
				function getCoeffProcess() : double;override;
				procedure transform(const dep : TDeperdition);
		end;


	//classe pour obtenir les déperditions en Watt
	type
		TDeperditionWatt = class(TDeperditionPercent)
			public
				deltaT : double;
				procedure transform(const dep : TDeperdition);
		end;


implementation

	uses SysUtils, IzUtilitaires, XmlUtil;


(*
classe
TDeperditionDetail
*)
	Constructor TDeperditionDetail.Create;
	begin
		inherited create;
		innerTagName := 'detail-deperdition';

		libelle := '';
		valeur := 0;
	end;


	procedure TDeperditionDetail.innerSaveList(Liste : TStringList);
	begin
		liste.add(setTag('libelle', libelle));
		liste.add(setTag('valeur', valeur));
	end;

	procedure TDeperditionDetail.innerLoadList(Liste : TStringList);
	begin
		libelle := getTagString(Liste, 'libelle');
		valeur := getTagDouble(Liste, 'valeur');
  end;

	procedure TDeperditionDetail.innerSaveXml(node : IXmlNode);
	begin
		TXmlUtil.setTag(node, 'libelle', libelle);
		TXmlUtil.setTag(node, 'valeur', valeur);
	end;

	procedure TDeperditionDetail.innerLoadXml(node : IXmlNode);
	begin
		libelle := TXmlUtil.getTagString(node, 'libelle');
		valeur := TXmlUtil.getTagDouble(node, 'valeur');
  end;




(*
classe
TDeperdition
*)
	constructor TDeperdition.Create;
	begin
		inherited Create;
		innerTagName := 'deperdition';

		apportEquipement := 0;
		apportOccupant := 0;
		apportSolaire := 0;
		apportUtile := 0;
		apportTotal := 0;
		apportPerdu := 0;

		besoinChauffNet := 0;
		consoChauffage := 0;
		besoinEcsNet := 0;
		consoEcs := 0;

		deperdition := 0;
		perteAux := 0;
		perteChauff := 0;
		perteEcs := 0;
		coeffPerteVentilation := 0;
		coeffPertePermeabilite := 0;

		coeffPerteToiture := 0;
		coeffPerteVitrage := 0;
		coeffPerteMur := 0;
		coeffPertePorte := 0;
		coeffPerteSol := 0;

    coeffGvPerte := 0;

		pertePontThermique := 0;

		chaleurTotale := 0;

		coeffDeperdition := 0;

		detailPerteVitrage := TIzHashtable.Create;
		detailPerteParoi := TIzHashtable.Create;
		detailPertePorte := TIzHashtable.Create;
	end;

	destructor TDeperdition.Destroy;
	begin
		freeAndNil(detailPerteVitrage);
		freeAndNil(detailPerteParoi);
		freeAndNil(detailPertePorte);
		inherited destroy;
	end;

  procedure TDeperdition.reset();
  begin
    coeffPerteMur := 0;
    coeffPerteVitrage := 0;
    coeffPertePorte := 0;
    coeffPerteToiture := 0;
    coeffPerteSol := 0;
    pertePontThermique := 0;
    detailPerteVitrage.freeContent;
    detailPerteParoi.freeContent;
    detailPertePorte.freeContent;
  end;


	Procedure TDeperdition.innerSaveList(Liste : TStringList);
	var
		cpt : integer;
		detail : TDeperditionDetail;
	begin
		liste.add(setTag('apport-equipement', apportEquipement));
		liste.add(setTag('apport-occupant', apportOccupant));
		liste.add(setTag('apport-solaire', apportSolaire));
		liste.add(setTag('apport-utile', apportUtile));
		liste.add(setTag('apport-total', apportTotal));
		liste.add(setTag('apport-perdu', apportPerdu));

		liste.add(setTag('chauffage-besoin-net', besoinChauffNet));
		liste.add(setTag('chauffage-consommation', consoChauffage));
		liste.add(setTag('chauffage-perte', perteChauff));

		liste.add(setTag('ecs-besoin-net', besoinEcsNet));
		liste.add(setTag('ecs-consommation', consoEcs));
		liste.add(setTag('ecs-perte', perteEcs));

		liste.add(setTag('chaleur-total', chaleurTotale));

		liste.add(setTag('perte-aux', perteAux));
		
		liste.add(setTag('coeff-perte-ventilation', coeffPerteVentilation));
		liste.add(setTag('coeff-perte-permeabilite', coeffPertePermeabilite));

		liste.add(setTag('coeff-perte-vitrage', coeffPerteVitrage));
		liste.add(setTag('coeff-perte-porte', coeffPertePorte));

		liste.add(setTag('coeff-perte-toiture', coeffPerteToiture));
		liste.add(setTag('coeff-perte-mur', coeffPerteMur));
		liste.add(setTag('coeff-perte-sol', coeffPerteSol));

		liste.add(setTag('coeff-GV-perte', coeffGvPerte));

		liste.add(setTag('perte-pont-thermique', pertePontThermique));
		liste.add(setTag('deperdition-besoin', deperdition));

		liste.add(setTag('deperdition-coeff', coeffDeperdition));

		liste.add('<perte-vitrage-detail>');
		liste.Add(setTag('nb', detailPerteVitrage.Count));
		for cpt := 0 to detailPerteVitrage.Count - 1 do
		begin
			detail := TDeperditionDetail(detailPerteVitrage.objects[cpt]);
			detail.saveList(liste);
		end;
		liste.add('</perte-vitrage-detail>');

		liste.add('<perte-paroi-detail>');
		liste.Add(setTag('nb', detailPerteParoi.Count));
		for cpt := 0 to detailPerteParoi.Count - 1 do
		begin
			detail := TDeperditionDetail(detailPerteParoi.objects[cpt]);
			detail.saveList(liste);
		end;
		liste.add('</perte-paroi-detail>');

		liste.add('<perte-porte-detail>');
		liste.Add(setTag('nb', detailPertePorte.Count));
		for cpt := 0 to detailPertePorte.Count - 1 do
		begin
			detail := TDeperditionDetail(detailPertePorte.objects[cpt]);
			detail.saveList(liste);
		end;
		liste.add('</perte-porte-detail>');
	end;


	procedure TDeperdition.innerLoadList(Liste : TStringList);
	var
		cpt : integer;
		nb : integer;
		detail : TDeperditionDetail;
	begin
		apportEquipement := getTagDouble(Liste, 'apport-equipement');
		apportOccupant := getTagDouble(Liste, 'apport-occupant');
		apportSolaire := getTagDouble(Liste, 'apport-solaire');
		apportUtile := getTagDouble(Liste, 'apport-utile');
		apportTotal := getTagDouble(Liste, 'apport-total');
		apportPerdu := getTagDouble(Liste, 'apport-perdu');

		besoinChauffNet := getTagDouble(Liste, 'chauffage-besoin-net');
		consoChauffage := getTagDouble(Liste, 'chauffage-consommation');
		perteChauff := getTagDouble(Liste, 'chauffage-perte');

		besoinEcsNet := getTagDouble(Liste, 'ecs-besoin-net');
		consoEcs := getTagDouble(Liste, 'ecs-consommation');
		perteEcs := getTagDouble(Liste, 'ecs-perte');

		chaleurTotale := getTagDouble(Liste, 'chaleur-total');

		perteAux := getTagDouble(Liste, 'perte-aux');

		coeffPerteVentilation := getTagDouble(Liste, 'coeff-perte-ventilation');

    if isFullTag(liste, 'coeff-perte-permeabilite') then
		  coeffPertePermeabilite := getTagDouble(Liste, 'coeff-perte-permeabilite')
    else
      coeffPertePermeabilite := 0;

		coeffPerteVitrage := getTagDouble(Liste, 'coeff-perte-vitrage');
		coeffPertePorte := getTagDouble(Liste, 'coeff-perte-porte');

		coeffPerteToiture := getTagDouble(Liste, 'coeff-perte-toiture');
		coeffPerteMur := getTagDouble(Liste, 'coeff-perte-mur');
		coeffPerteSol := getTagDouble(Liste, 'coeff-perte-sol');

		coeffGvPerte := getTagDouble(Liste, 'coeff-GV-perte');

		pertePontThermique := getTagDouble(Liste, 'perte-pont-thermique');
		deperdition := getTagDouble(Liste, 'deperdition-besoin');

		coeffDeperdition := getTagDouble(Liste, 'deperdition-coeff');

		removeFirst(liste);//<perte-vitrage-detail>
		nb := getTagInt(Liste, 'nb');
		detailPerteVitrage.freeContent();
		for cpt := 1 to nb do
		begin
			detail := TDeperditionDetail.Create;
			detail.loadList(liste);
			detailPerteVitrage.ObjByName[detail.libelle] := detail;
		end;
		removeFirst(Liste);//</perte-vitrage-detail>

		removeFirst(liste);//<perte-paroi-detail>
		nb := getTagInt(Liste, 'nb');
		detailPerteParoi.freeContent;
		for cpt := 1 to nb do
		begin
			detail := TDeperditionDetail.Create;
			detail.loadList(liste);
			detailPerteParoi.ObjByName[detail.libelle] := detail;
		end;
		removeFirst(liste);//</perte-paroi-detail>

		removeFirst(liste);//<perte-porte-detail>
		nb := getTagInt(Liste, 'nb');
		detailPertePorte.freeContent;
		for cpt := 1 to nb do
		begin
			detail := TDeperditionDetail.Create;
			detail.loadList(liste);
			detailPertePorte.ObjByName[detail.libelle] := detail;
		end;
		removeFirst(liste);//</perte-porte-detail>
	end;



	Procedure TDeperdition.innerSaveXml(node  : IXmlNode);
	var
		cpt : integer;
		detail : TDeperditionDetail;

    child : IXmlNode;
	begin
		TXmlUtil.setTag(node, 'apport-equipement', apportEquipement);
		TXmlUtil.setTag(node, 'apport-occupant', apportOccupant);
		TXmlUtil.setTag(node, 'apport-solaire', apportSolaire);
		TXmlUtil.setTag(node, 'apport-utile', apportUtile);
		TXmlUtil.setTag(node, 'apport-total', apportTotal);
		TXmlUtil.setTag(node, 'apport-perdu', apportPerdu);

		TXmlUtil.setTag(node, 'chauffage-besoin-net', besoinChauffNet);
		TXmlUtil.setTag(node, 'chauffage-consommation', consoChauffage);
		TXmlUtil.setTag(node, 'chauffage-perte', perteChauff);

		TXmlUtil.setTag(node, 'ecs-besoin-net', besoinEcsNet);
		TXmlUtil.setTag(node, 'ecs-consommation', consoEcs);
		TXmlUtil.setTag(node, 'ecs-perte', perteEcs);

		TXmlUtil.setTag(node, 'chaleur-total', chaleurTotale);

		TXmlUtil.setTag(node, 'perte-aux', perteAux);

		TXmlUtil.setTag(node, 'coeff-perte-ventilation', coeffPerteVentilation);
		TXmlUtil.setTag(node, 'coeff-perte-permeabilite', coeffPertePermeabilite);

		TXmlUtil.setTag(node, 'coeff-perte-vitrage', coeffPerteVitrage);
		TXmlUtil.setTag(node, 'coeff-perte-porte', coeffPertePorte);

		TXmlUtil.setTag(node, 'coeff-perte-toiture', coeffPerteToiture);
		TXmlUtil.setTag(node, 'coeff-perte-mur', coeffPerteMur);
		TXmlUtil.setTag(node, 'coeff-perte-sol', coeffPerteSol);

		TXmlUtil.setTag(node, 'coeff-GV-perte', coeffGvPerte);

		TXmlUtil.setTag(node, 'perte-pont-thermique', pertePontThermique);
		TXmlUtil.setTag(node, 'deperdition-besoin', deperdition);

		TXmlUtil.setTag(node, 'deperdition-coeff', coeffDeperdition);

    child := TXmlUtil.addChild(node, 'perte-vitrage-detail');
		for cpt := 0 to detailPerteVitrage.Count - 1 do
		begin
			detail := TDeperditionDetail(detailPerteVitrage.objects[cpt]);
			detail.saveXml(child);
		end;

    child := TXmlUtil.addChild(node, 'perte-paroi-detail');
		for cpt := 0 to detailPerteParoi.Count - 1 do
		begin
			detail := TDeperditionDetail(detailPerteParoi.objects[cpt]);
			detail.saveXml(child);
		end;

    child := TXmlUtil.addChild(node, 'perte-porte-detail');
		for cpt := 0 to detailPertePorte.Count - 1 do
		begin
			detail := TDeperditionDetail(detailPertePorte.objects[cpt]);
			detail.saveXml(child);
		end;
	end;


	procedure TDeperdition.innerLoadXml(node : IXmlNode);
	var
		cpt : integer;
		detail : TDeperditionDetail;

    child : IXmlNode;
	begin
		apportEquipement := TXmlUtil.getTagDouble(node, 'apport-equipement');
		apportOccupant := TXmlUtil.getTagDouble(node, 'apport-occupant');
		apportSolaire := TXmlUtil.getTagDouble(node, 'apport-solaire');
		apportUtile := TXmlUtil.getTagDouble(node, 'apport-utile');
		apportTotal := TXmlUtil.getTagDouble(node, 'apport-total');
		apportPerdu := TXmlUtil.getTagDouble(node, 'apport-perdu');

		besoinChauffNet := TXmlUtil.getTagDouble(node, 'chauffage-besoin-net');
		consoChauffage := TXmlUtil.getTagDouble(node, 'chauffage-consommation');
		perteChauff := TXmlUtil.getTagDouble(node, 'chauffage-perte');

		besoinEcsNet := TXmlUtil.getTagDouble(node, 'ecs-besoin-net');
		consoEcs := TXmlUtil.getTagDouble(node, 'ecs-consommation');
		perteEcs := TXmlUtil.getTagDouble(node, 'ecs-perte');

		chaleurTotale := TXmlUtil.getTagDouble(node, 'chaleur-total');

		perteAux := TXmlUtil.getTagDouble(node, 'perte-aux');

		coeffPerteVentilation := TXmlUtil.getTagDouble(node, 'coeff-perte-ventilation');
		coeffPertePermeabilite := TXmlUtil.getTagDouble(node, 'coeff-perte-permeabilite');

		coeffPerteVitrage := TXmlUtil.getTagDouble(node, 'coeff-perte-vitrage');
		coeffPertePorte := TXmlUtil.getTagDouble(node, 'coeff-perte-porte');

		coeffPerteToiture := TXmlUtil.getTagDouble(node, 'coeff-perte-toiture');
		coeffPerteMur := TXmlUtil.getTagDouble(node, 'coeff-perte-mur');
		coeffPerteSol := TXmlUtil.getTagDouble(node, 'coeff-perte-sol');

		coeffGvPerte := TXmlUtil.getTagDouble(node, 'coeff-GV-perte');

		pertePontThermique := TXmlUtil.getTagDouble(node, 'perte-pont-thermique');
		deperdition := TXmlUtil.getTagDouble(node, 'deperdition-besoin');

		coeffDeperdition := TXmlUtil.getTagDouble(node, 'deperdition-coeff');

		child := TXmlUtil.getChild(node, 'perte-vitrage-detail');
		detailPerteVitrage.freeContent();
		for cpt := 0 to child.ChildNodes.Count - 1 do
		begin
			detail := TDeperditionDetail.Create;
			detail.loadXml(child.ChildNodes[cpt]);
			detailPerteVitrage.ObjByName[detail.libelle] := detail;
		end;

		child := TXmlUtil.getChild(node, 'perte-paroi-detail');
		detailPerteParoi.freeContent;
		for cpt := 0 to child.ChildNodes.Count - 1 do
		begin
			detail := TDeperditionDetail.Create;
			detail.loadXml(child.ChildNodes[cpt]);
			detailPerteParoi.ObjByName[detail.libelle] := detail;
		end;

		child := TXmlUtil.getChild(node, 'perte-porte-detail');
		detailPertePorte.freeContent;
		for cpt := 0 to child.ChildNodes.Count - 1 do
		begin
			detail := TDeperditionDetail.Create;
			detail.loadXml(child.ChildNodes[cpt]);
			detailPertePorte.ObjByName[detail.libelle] := detail;
		end;
	end;

	function TDeperdition.getCoeffProcess() : double;
	begin
		if coeffGvPerte = 0 then
		begin
			result := 0;
			exit;
		end;
		result := chaleurTotale/coeffGvPerte;
	end;

	function TDeperdition.getPerteVitrage() : double;
	begin
		result := coeffPerteVitrage * getCoeffProcess();
	end;

	function TDeperdition.getPertePorte() : double;
	begin
		result := coeffPertePorte * getCoeffProcess();
	end;

	function TDeperdition.getPerteToiture() : double;
	begin
		result := coeffPerteToiture * getCoeffProcess();
	end;

	function TDeperdition.getPerteMur() : double;
	begin
		result := coeffPerteMur * getCoeffProcess();
	end;

	function TDeperdition.getPerteSol() : double;
	begin
		result := coeffPerteSol * getCoeffProcess();
	end;

	function TDeperdition.getPerteVentilation() : double;
	begin
		result := coeffPerteVentilation * getCoeffProcess();
	end;

	function TDeperdition.getPertePermeabilite() : double;
	begin
		result := coeffPertePermeabilite * getCoeffProcess();
	end;




(*
classe
TDeperditionPercent
*)
	function TDeperditionPercent.getCoeffProcess() : double;
	begin
		result := 1;
	end;


	procedure TDeperditionPercent.transform(const dep : TDeperdition);
	var
		cpt : integer;
		nb : integer;
		detail : TDeperditionDetail;
		det : TDeperditionDetail;
	begin
		apportTotal := 100;

		if(dep <> nil) and (dep.apportTotal <> 0) then
		begin
			apportEquipement := dep.apportEquipement / dep.apportTotal * 100;
			apportOccupant := dep.apportOccupant / dep.apportTotal * 100;
			apportSolaire := dep.apportSolaire / dep.apportTotal * 100;
			apportUtile := dep.apportUtile / dep.apportTotal * 100;
			apportPerdu := dep.apportPerdu / dep.apportTotal * 100;
		end
		else
		begin
			apportEquipement := 0;
			apportOccupant := 0;
			apportSolaire := 0;
			apportUtile := 0;
			apportPerdu := 0;
		end;
(*
		besoinChauffNet := besoinChauffNet;
		consoChauffage := consoChauffage;
		perteChauff := perteChauff;

		besoinEcsNet := besoinEcsNet;
		consoEcs := consoEcs;
		perteEcs := perteEcs;


		perteAux := perteAux;
*)
		chaleurTotale := 100;
		coeffGvPerte := dep.coeffGvPerte;
		coeffDeperdition := dep.coeffDeperdition;

		if (dep <> nil) and (dep.chaleurTotale <> 0) then
		begin
			coeffPerteVentilation := dep.perteVentilation / dep.chaleurTotale * 100;
			coeffPertePermeabilite := dep.pertePermeabilite / dep.chaleurTotale * 100;

			coeffPerteVitrage := dep.perteVitrage / dep.chaleurTotale * 100;
			coeffPertePorte := dep.pertePorte / dep.chaleurTotale * 100;

			coeffPerteToiture := dep.perteToiture / dep.chaleurTotale * 100;
			coeffPerteMur := dep.perteMur / dep.chaleurTotale * 100;
			coeffPerteSol := dep.perteSol / dep.chaleurTotale * 100;

			pertePontThermique := dep.pertePontThermique / dep.chaleurTotale * 100;
		end
		else
		begin
			coeffPerteVentilation := 0;
      coeffPertePermeabilite := 0;

			coeffPerteVitrage := 0;
			coeffPertePorte := 0;

			coeffPerteToiture := 0;
			coeffPerteMur := 0;
			coeffPerteSol := 0;

			pertePontThermique := 0;
		end;
(*
		deperdition := deperdition;
*)
    detailPerteVitrage.freeContent;
    detailPerteParoi.freeContent;
    detailPertePorte.freeContent;
    if (dep <> nil) then
    begin
      nb := dep.detailPerteVitrage.Count;
      for cpt := 0 to nb -1 do
      begin
        detail := TDeperditionDetail(dep.detailPerteVitrage.Objects[cpt]);
        det := TDeperditionDetail.Create;
        det.libelle := detail.libelle;
        det.valeur := detail.valeur / dep.chaleurTotale * 100;
        detailPerteVitrage.ObjByName[det.libelle] := det;
      end;

      nb := dep.detailPerteParoi.count;
      for cpt := 0 to nb - 1 do
      begin
        detail := TDeperditionDetail(dep.detailPerteParoi.Objects[cpt]);
        det := TDeperditionDetail.Create;
        det.libelle := detail.libelle;
        det.valeur := detail.valeur / dep.chaleurTotale * 100;
        detailPerteParoi.ObjByName[det.libelle] := det;
      end;

      nb := dep.detailPertePorte.Count;
      for cpt := 0 to nb - 1 do
      begin
        detail := TDeperditionDetail(dep.detailPertePorte.Objects[cpt]);
        det := TDeperditionDetail.Create;
        det.libelle := detail.libelle;
        det.valeur := detail.valeur / dep.chaleurTotale * 100;
        detailPertePorte.ObjByName[det.libelle] := det;
      end;
    end;
	end;




(*
classe
TDeperditionWatt
*)
	procedure TDeperditionWatt.transform(const dep : TDeperdition);
	var
		cpt : integer;
		nb : integer;
		detail : TDeperditionDetail;

		dbl : double;
	begin

		if not(dep is TDeperditionPercent) then
		begin
			inherited transform(dep);
		end;
		dbl := coeffGvPerte * deltaT / 100;

(*
		apportTotal := apportTotal * dbl;
		apportEquipement := apportEquipement * dbl;
		apportOccupant := apportOccupant * dbl;
		apportSolaire := apportSolaire * dbl;
		apportUtile := apportUtile * dbl;
		apportPerdu := apportPerdu * dbl;

		besoinChauffNet := besoinChauffNet * dbl;
		consoChauffage := consoChauffage * dbl;
		perteChauff := perteChauff * dbl;

		besoinEcsNet := besoinEcsNet * dbl;
		consoEcs := consoEcs * dbl;
		perteEcs := perteEcs * dbl;


		perteAux := perteAux * dbl;
*)

		apportTotal := 0;
		apportEquipement := 0;
		apportOccupant := 0;
		apportSolaire := 0;
		apportUtile := 0;
		apportPerdu := 0;

		besoinChauffNet := 0;
		consoChauffage := 0;
		perteChauff := 0;

		besoinEcsNet := 0;
		consoEcs := 0;
		perteEcs := 0;

		perteAux := 0;


		chaleurTotale := chaleurTotale * dbl;
		coeffGvPerte := coeffGvPerte * dbl;
		coeffDeperdition := coeffDeperdition * dbl;

		coeffPerteVentilation := coeffPerteVentilation * dbl;
		coeffPertePermeabilite := coeffPertePermeabilite * dbl;

		coeffPerteVitrage := coeffPerteVitrage * dbl;
		coeffPertePorte := coeffPertePorte * dbl;

		coeffPerteToiture := coeffPerteToiture * dbl;
		coeffPerteMur := coeffPerteMur * dbl;
		coeffPerteSol := coeffPerteSol * dbl;


		pertePontThermique := pertePontThermique * dbl;
		deperdition := deperdition * dbl;

		nb := detailPerteVitrage.Count;
		for cpt := 0 to nb -1 do
		begin
			detail := TDeperditionDetail(detailPerteVitrage.Objects[cpt]);
			detail.valeur := detail.valeur * dbl;
		end;

		nb := detailPerteParoi.count;
		for cpt := 0 to nb - 1 do
		begin
			detail := TDeperditionDetail(detailPerteParoi.Objects[cpt]);
			detail.valeur := detail.valeur * dbl;
		end;

		nb := detailPertePorte.Count;
		for cpt := 0 to nb - 1 do
		begin
			detail := TDeperditionDetail(detailPertePorte.Objects[cpt]);
			detail.valeur := detail.valeur * dbl;
		end;
	end;

end.
