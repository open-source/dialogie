unit IzTypeObjectPersist;

interface

uses
  contnrs,
  IzObject,
  Classes,
  IzObjectPersist,
  XMLIntf,
  GestionSauvegarde,
  CreerSauvegarde;

type
  TTypeObjectPersist = class(TObjectPersist)
  private
    innerContentClass: TObjectPersistClass;
    innerList:         TObjectList;

    function getCount(): integer; inline;

  protected
    procedure innerLoadList(liste: TSauvegarde); override;
    procedure innerLoadList(liste: TStringList); override;
    procedure innerSaveList(liste: TStringList); override;

    procedure innerLoadXml(node: IXMLNode); override;
    procedure innerSaveXml(node: IXMLNode); override;

  public
    constructor Create(); overload;override;
    constructor Create(ownsItems : boolean); overload;
    destructor Destroy(); reintroduce; override;

    function getObject(rank: integer): TObjectPersist;
    function addObject(item: TObjectPersist): integer;
    function removeObject(item: TObjectPersist): integer;
    function remove(rank: integer): integer; //overload;
    function indexOf(anObject: TObjectPersist): integer; //overload;
    procedure freeAll();virtual;
    procedure removeAll();

    function getLabels(): TStringList;virtual;

    property nb: integer Read getCount;
    property contentClass: TObjectPersistClass Write innerContentClass;
  end;



implementation

uses
  System.Types,
  SysUtils,
  IzUtilitaires;



(******************************************************************
TTypeObjectPersist
******************************************************************)

constructor TTypeObjectPersist.Create();
begin
  Create(True);
end;


constructor TTypeObjectPersist.Create(ownsItems : boolean);
begin
  inherited Create();
  innerContentClass := TObjectPersist;
  innerList := TObjectList.Create(ownsItems);
end;


destructor TTypeObjectPersist.Destroy();
begin
  innerList.Free();
  inherited Destroy();
end;


function TTypeObjectPersist.getObject(rank: integer): TObjectPersist;
begin
  result := nil;
  if (rank < 0) or (rank >= nb) then
    exit;
  result := TObjectPersist(innerList.Items[rank]);
end;


function TTypeObjectPersist.addObject(item: TObjectPersist): integer;
var
  rank: integer;
begin
  if (item = nil) then
  begin
    result := -1;
    exit;
  end;

  //contr�le sur la classe de l'objet ajout�
  //innerContentClass n'est en principe jamais � nil
  //    if (innerContentClass <> nil) and not(item is innerContentClass) then
  if not (item is innerContentClass) then
  begin
    result := -2;
    exit;
  end;

  rank := innerList.IndexOf(item);
  if (rank <> -1) then
  begin
    result := rank;
  end
  else
  begin
    result := innerList.Add(item);
  end;
end;


function TTypeObjectPersist.removeObject(item: TObjectPersist): integer;
var
  rank: integer;
begin
  if (item = nil) then
  begin
    result := -1;
    exit;
  end;

  rank := innerList.IndexOf(item);
  if (rank = -1) then
  begin
    result := -1;
    exit;
  end
  else
    result := innerList.remove(item);
end;


function TTypeObjectPersist.remove(rank: integer): integer;
begin
  if (rank < 0) or (rank >= innerList.Count) then
  begin
    result := -1;
    exit;
  end;

  result := removeObject(TObjectPersist(innerList.Items[rank]));
end;


procedure TTypeObjectPersist.removeAll();
begin
  if (innerList <> nil) then
    innerList.Clear;
end;


procedure TTypeObjectPersist.freeAll();
var
  cpt : integer;
  tot : integer;
  o : TObject;
begin
  if (innerList = nil) then
    exit;

  tot := innerList.Count - 1;
  for cpt := tot downto 0 do
  begin
    o := innerList.Items[cpt];
    innerList.Remove(o);
    if (o is TTypeObjectPersist) then
      TTypeObjectPersist(o).freeAll;
    if not(innerList.OwnsObjects) then
    begin
      o.Free;
    end;
//    innerList.Items[cpt].Free;
//    innerList.Items[cpt] := nil;
  end;
//  innerList.Clear;
end;


function TTypeObjectPersist.getCount(): integer;
begin
  Result := 0;
  if (innerList <> nil) then
    Result := innerList.Count;
end;


function TTypeObjectPersist.indexOf(anObject: TObjectPersist): integer;
begin
  Result := innerList.IndexOf(anObject);
end;


function TTypeObjectPersist.getLabels(): TStringList;
  //� l'appelant de lib�rer par la suite
var
  ret: TStringList;
  i: integer;
begin
  ret := TStringList.Create;
  if (getCount = 0) then
    exit(ret);

  for i := 0 to innerList.Count - 1 do
  begin
    ret.Add(IntToStr(i + 1) + ' - ' + getObject(i).getLabel);
  end;
  Result := ret;
end;



procedure TTypeObjectPersist.innerLoadList(liste: TSauvegarde);
var
  ob: TObjectPersist;
  i:  integer;
  nb: integer;
begin
  nb := getTagInt(liste.GetLigne, 'nb');

  innerList.Clear;
  for i := 1 to nb do
  begin
    ob := innerContentClass.Create;
    ob.loadList(liste);
    innerList.Add(ob);
  end;
end;



procedure TTypeObjectPersist.innerLoadList(liste: TStringList);
var
  ob: TObjectPersist;
  i:  integer;
  nb: integer;
begin
  nb := getTagInt(liste, 'nb');

  innerList.Clear;
  for i := 1 to nb do
  begin
    ob := innerContentClass.Create;
    ob.loadList(liste);
    innerList.Add(ob);
  end;
end;


procedure TTypeObjectPersist.innerSaveList(liste: TStringList);
var
  ob: TObject;
  i:  integer;
begin
  liste.Add(setTag('nb', innerList.Count));
  for i := 0 to innerList.Count - 1 do
  begin
    ob := innerList[i];
    if (ob is TObjectPersist) then
    begin
      TObjectPersist(ob).saveList(liste);
    end;
  end;
end;


procedure TTypeObjectPersist.innerLoadXml(node: IXMLNode);
var
  ob: TObjectPersist;
  childNode: IXMLNode;
  i:  integer;
begin
  removeAll();
  if not (node.HasChildNodes) then
    exit;
  for i := 0 to node.childNodes.Count - 1 do
  begin
    childNode := node.ChildNodes[i];
    ob := innerContentClass.Create;
    if childNode.NodeName <> ob.tagName then
    begin
      freeAndNil(ob);
      continue;
    end;
    ob.loadXml(childNode);
    innerList.Add(ob);
  end;
end;


procedure TTypeObjectPersist.innerSaveXml(node: IXMLNode);
var
  ob: TObject;
  i:  integer;
begin
  for i := 0 to innerList.Count - 1 do
  begin
    ob := innerList[i];
    if (ob is TObjectPersist) then
    begin
      TObjectPersist(ob).saveXml(node);
    end;
  end;
end;


end.
