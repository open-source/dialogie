unit IzObjectUtil;


interface

uses
  IzObjectPersist,
  Classes,
  IzTypeObjectPersist,
  GestionSauvegarde,
  CreerSauvegarde,
  Xml.XMLIntf;


type
  TApportInterneObject = class(TObjectPersist)
  protected
    procedure innerLoadList(Liste : TStringList); override;
    procedure innerLoadList(Liste : TSauvegarde); override;
    procedure innerSaveList(Liste : TStringList); override;

    procedure innerLoadXml(node : IXmlNode); override;
    procedure innerSaveXml(node : IXmlNode); override;
  public
    replaced : boolean;
    apportInterne : boolean;
    constructor Create(); reintroduce; override;
  end;


// type TIzPercentObject = class(TIzObject)
// changement de l'arbre d'h�ritage pour pouvoir prendre en charge l'apport interne
type
  TIzPercentObject = class(TApportInterneObject)

  private
    innerPercent : double;
    procedure setPerCent(percent : double);

  protected
    procedure innerLoadList(Liste : TSauvegarde); override;
    procedure innerLoadList(Liste : TStringList); override;
    procedure innerSaveList(Liste : TStringList); override;

    procedure innerLoadXml(node : IXmlNode); override;
    procedure innerSaveXml(node : IXmlNode); override;
  public

    constructor Create(); override;
    property pourCent : double read innerPercent write setPerCent;
  end;

type
  TIzPercentObjectClass = class of TIzPercentObject;


type
  TTypePercentObject = class(TTypeObjectPersist)
  public
    constructor Create(); override;

    function getPercentObject(rank : integer) : TIzPercentObject;
  end;


implementation

uses
  System.SysUtils,
  IzUtilitaires,
  XmlUtil;

(* *************************
TApportInterneObject
************************* *)
constructor TApportInterneObject.Create();
begin
  inherited Create();
  apportInterne := true;
end;


procedure TApportInterneObject.innerLoadList(Liste : TStringList);
begin
  inherited innerLoadList(Liste);
  if isFullTag(Liste, 'apport-interne') then
  begin
    apportInterne := getTagBool(Liste, 'apport-interne');
  end;

end;

procedure TApportInterneObject.innerLoadList(Liste : TSauvegarde);
begin
  inherited innerLoadList(Liste);
  if isFullTag(Liste.GetLigneSansBouger, 'apport-interne') then
  begin
    apportInterne := getTagBool(Liste.GetLigne, 'apport-interne');
  end;

end;


procedure TApportInterneObject.innerSaveList(Liste : TStringList);
begin
  inherited innerSaveList(Liste);
  Liste.add(setTag('apport-interne', apportInterne));
end;

procedure TApportInterneObject.innerLoadXml(node : IXmlNode);
begin
  apportInterne := TXmlUtil.getTagBool(node, 'apport-interne');
end;


procedure TApportInterneObject.innerSaveXml(node : IXmlNode);
begin
  TXmlUtil.setTag(node, 'apport-interne', apportInterne);
end;


(* *************************
TIzPercentObject
************************* *)
constructor TIzPercentObject.Create();
begin
  inherited Create();

		// on dit que tous les �quipements ne particiepent pas
		// aux apports internes par d�faut
		// i.e. ils ne sont pas en zone chauff�e
  apportInterne := false;
  replaced := false;

  innerPercent := 100;
end;


procedure TIzPercentObject.setPerCent(percent : double);
begin
  if (percent <= 100) and (percent >= 0) then
    innerPercent := percent;
end;


procedure TIzPercentObject.innerLoadList(Liste : TStringList);
begin
  replaced := false;
  inherited innerLoadList(Liste);
  if isFullTag(Liste, 'percent') then
  begin
    innerPercent := getTagDouble(Liste, 'percent');
  end;
end;


procedure TIzPercentObject.innerLoadList(Liste : TSauvegarde);
begin
  inherited innerLoadList(Liste);
  if isFullTag(Liste.GetLigneSansBouger, 'percent') then
  begin
    innerPercent := getTagDouble(Liste.GetLigne, 'percent');
  end;
end;


procedure TIzPercentObject.innerSaveList(Liste : TStringList);
begin
  inherited innerSaveList(Liste);
  Liste.add(setTag('percent', innerPercent));
end;


procedure TIzPercentObject.innerLoadXml(node : IXmlNode);
begin
  inherited innerLoadXml(node);
  innerPercent := TXmlUtil.getTagDouble(node, 'percent');
end;


procedure TIzPercentObject.innerSaveXml(node : IXmlNode);
begin
  inherited innerSaveXml(node);
  TXmlUtil.setTag(node, 'percent', innerPercent);
end;


(* *************************
TTypePercentObject
************************* *)

constructor TTypePercentObject.Create();
begin
  inherited Create();
  contentClass := TIzPercentObject;
end;


function TTypePercentObject.getPercentObject(rank : integer) : TIzPercentObject;
begin
  result := TIzPercentObject(getObject(rank));
end;


end.
