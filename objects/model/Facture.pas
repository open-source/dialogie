unit facture;

interface

uses
  Controls,
  classes,
  Sysutils,
  IzObjectPersist,
  IzTypeObjectPersist, Xml.XMLIntf;

type
  TFacture = class(TObjectPersist)
  protected
    procedure innerSaveList(Liste : TStringList); override;
    procedure innerLoadList(stringList : TStringList); override;

    procedure innerLoadXml(node : IXMLNode); override;
    procedure innerSaveXml(node : IXMLNode); override;
  public
    Debut, Fin : Tdate;
    Periode : Integer;
    Quantite : Double;
    Variete : Integer;
    Rendement, Valeur : Double;
    ValeurkWh : Double;
    constructor Create; reintroduce; override;

    procedure loadFromList(Liste : TStringList);
  end;


type
  TTypeFacture = class(TTypeObjectPersist)
  protected
    procedure innerSaveList(Liste : TStringList); override;
    procedure innerLoadList(Liste : TStringList); override;

    procedure innerLoadXml(node : IXMLNode); override;
    procedure innerSaveXml(node : IXMLNode); override;
  public
    PeriodeTotale : Integer;
    QuantiteTotale,
      QuantiteAnnuelle,
      PrixTotal : Double;
    prixTotalAnnuel,
      PrixMoyen : Double;
    kWhTotal : Double;

    constructor Create; reintroduce; override;

    procedure addFacture(facture : TFacture);
    function getFacture(rank : Integer) : TFacture;
    procedure process; virtual;

  end;


type
  TTypeFactureElec = class(TTypeFacture)
  public
    constructor Create; reintroduce; override;
    procedure process; override;
  end;


type
  TTypeFactureFuel = class(TTypeFacture)
  public
    constructor Create; reintroduce; override;
    procedure process; override;
  end;


type
  TTypeFactureGaz = class(TTypeFacture)
  public
    constructor Create; reintroduce; override;
    procedure process; override;
  end;


type
  TTypeFactureBois = class(TTypeFacture)
  public
    constructor Create; reintroduce; override;
    procedure process; override;
  end;


type
  TFactures = class(TObjectPersist)
  protected

    procedure innerSaveList(Liste : TStringList); override;
    procedure innerLoadList(Liste : TStringList); override;

    procedure innerLoadXml(node : IXMLNode); override;
    procedure innerSaveXml(node : IXMLNode); override;
  public
    listeElec : TTypeFactureElec;
    listeFuel : TTypeFactureFuel;
    listeGaz : TTypeFactureGaz;
    listeBois : TTypeFactureBois;

    constructor Create; reintroduce; override;
    destructor Destroy; override;

    procedure loadFromList(Liste : TStringList);

    procedure process();

  end;

implementation

uses
  Contnrs,
  IzUtilitaires, XmlUtil;


(* ************************
TFacture
************************ *)
constructor TFacture.Create;
var
  Y, M, D : Word;
  yy : Integer;
begin
  inherited Create;
  DecodeDate(Date, Y, M, D);
  yy := Y;
  innerTagName := 'facture';
  Debut := StrToDate('01/01/' + getString(yy));
  Fin := StrToDate('31/12/' + getString(yy));
  Periode := Round(Fin - Debut);
  Quantite := 0;
  Variete := 0;
  Rendement := 1;
  Valeur := 0;
  ValeurkWh := 0;
end;


procedure TFacture.innerSaveList(Liste : TStringList);
begin
  inherited innerSaveList(Liste);
  Liste.add(setTagDate('date-debut', Debut));
  Liste.add(setTagDate('date-fin', Fin));
  Liste.add(setTag('periode', Periode));
  Liste.add(setTag('qte', Quantite));
  Liste.add(setTag('variete', Variete));
  Liste.add(setTag('rendement', Rendement));
  Liste.add(setTag('valeur', Valeur));
  Liste.add(setTag('kwh', ValeurkWh));
end;


procedure TFacture.innerLoadXml(node : IXmlNode);
begin
  Debut := TXmlUtil.getTagDate(node, 'date-debut');
  Fin := TXmlUtil.getTagDate(node, 'date-fin');
  Periode := TXmlUtil.getTagInt(node, 'periode');
  Quantite := TXmlUtil.getTagDouble(node, 'qte');
  Variete := TXmlUtil.getTagInt(node, 'variete');
  Rendement := TXmlUtil.getTagDouble(node, 'rendement');
  Valeur := TXmlUtil.getTagDouble(node, 'valeur');
  ValeurkWh := TXmlUtil.getTagDouble(node, 'kwh');
end;


procedure TFacture.innerSaveXml(node : IXmlNode);
begin
  TXmlUtil.setTagDate(node, 'date-debut', Debut);
  TXmlUtil.setTagDate(node, 'date-fin', Fin);
  TXmlUtil.setTag(node, 'periode', Periode);
  TXmlUtil.setTag(node, 'qte', Quantite);
  TXmlUtil.setTag(node, 'variete', Variete);
  TXmlUtil.setTag(node, 'rendement', Rendement);
  TXmlUtil.setTag(node, 'valeur', Valeur);
  TXmlUtil.setTag(node, 'kwh', ValeurkWh);
end;


procedure TFacture.innerLoadList(stringList : TStringList);
begin
  inherited innerLoadList(stringList);
  Debut := getTagDate(removeFirst(stringList), 'date-debut');
  Fin := getTagDate(removeFirst(stringList), 'date-fin');
  Periode := getTagInt(removeFirst(stringList), 'periode');
  Quantite := getTagDouble(removeFirst(stringList), 'qte');
  Variete := getTagInt(removeFirst(stringList), 'variete');
  Rendement := getTagDouble(removeFirst(stringList), 'rendement');
  Valeur := getTagDouble(removeFirst(stringList), 'valeur');
  ValeurkWh := getTagDouble(removeFirst(stringList), 'kwh');
end;


procedure TFacture.loadFromList(Liste : TStringList);
begin
  Debut := getDate(removeFirst(Liste));
  Fin := getDate(removeFirst(Liste));
  Periode := getInt(removeFirst(Liste));
  Quantite := getDouble(removeFirst(Liste));
  Variete := getInt(removeFirst(Liste));
  Rendement := getDouble(removeFirst(Liste));
  Valeur := getDouble(removeFirst(Liste));
  ValeurkWh := getDouble(removeFirst(Liste));
end;


(* ************************
TTypeFacture
************************ *)
constructor TTypeFacture.Create;
begin
  inherited Create();
  contentClass := TFacture;
  innerTagName := 'type-facture';
  PeriodeTotale := 0;
  QuantiteTotale := 0;
  QuantiteAnnuelle := 0;
  PrixTotal := 0;
  prixTotalAnnuel := 0;
  PrixMoyen := 0;
  kWhTotal := 0;
end;

procedure TTypeFacture.innerSaveList(Liste : TStringList);
begin
  inherited innerSaveList(Liste);
  Liste.add(setTag('periode-totale', PeriodeTotale));
  Liste.add(setTag('qte-totale', QuantiteTotale));
  Liste.add(setTag('qte-annuelle', QuantiteAnnuelle));
  Liste.add(setTag('prix-total', PrixTotal));
  Liste.add(setTag('prix-total-annuel', prixTotalAnnuel));
  Liste.add(setTag('prix-moyen', PrixMoyen));
  Liste.add(setTag('kwh-total', kWhTotal));
end;


procedure TTypeFacture.innerLoadList(Liste : TStringList);
begin
  inherited innerLoadList(Liste);
  PeriodeTotale := getTagInt(Liste, 'periode-totale');
  QuantiteTotale := getTagDouble(Liste, 'qte-totale');
  QuantiteAnnuelle := getTagDouble(Liste, 'qte-annuelle');
  PrixTotal := getTagDouble(Liste, 'prix-total');
  prixTotalAnnuel := getTagDouble(Liste, 'prix-total-annuel');
  PrixMoyen := getTagDouble(Liste, 'prix-moyen');
  kWhTotal := getTagDouble(Liste, 'kwh-total');
end;

procedure TTypeFacture.innerSaveXml(node : IXmlNode);
begin
  inherited innerSaveXml(node);
  TXmlUtil.setTag(node, 'periode-totale', PeriodeTotale);
  TXmlUtil.setTag(node, 'qte-totale', QuantiteTotale);
  TXmlUtil.setTag(node, 'qte-annuelle', QuantiteAnnuelle);
  TXmlUtil.setTag(node, 'prix-total', PrixTotal);
  TXmlUtil.setTag(node, 'prix-total-annuel', prixTotalAnnuel);
  TXmlUtil.setTag(node, 'prix-moyen', PrixMoyen);
  TXmlUtil.setTag(node, 'kwh-total', kWhTotal);
end;


procedure TTypeFacture.innerLoadXml(node : IXmlNode);
begin
  inherited innerLoadXml(node);
  PeriodeTotale := TXmlUtil.getTagInt(node, 'periode-totale');
  QuantiteTotale := TXmlUtil.getTagDouble(node, 'qte-totale');
  QuantiteAnnuelle := TXmlUtil.getTagDouble(node, 'qte-annuelle');
  PrixTotal := TXmlUtil.getTagDouble(node, 'prix-total');
  prixTotalAnnuel := TXmlUtil.getTagDouble(node, 'prix-total-annuel');
  PrixMoyen := TXmlUtil.getTagDouble(node, 'prix-moyen');
  kWhTotal := TXmlUtil.getTagDouble(node, 'kwh-total');
end;


procedure TTypeFacture.addFacture(facture : TFacture);
begin
  addObject(facture);
  process;
end;

function TTypeFacture.getFacture(rank : Integer) : TFacture;
begin
  result := TFacture(getObject(rank));
end;

procedure TTypeFacture.process;
begin
  PeriodeTotale := 0;
  QuantiteTotale := 0;
  QuantiteAnnuelle := 0;
  PrixTotal := 0;
  prixTotalAnnuel := 0;
  PrixMoyen := 0;
  kWhTotal := 0;
end;


(* ************************
TTypeFactureElec
************************ *)
constructor TTypeFactureElec.Create;
begin
  inherited Create();
  innerTagName := 'factures-elec';
end;

procedure TTypeFactureElec.process;
var
  i : Integer;
  facture : TFacture;
begin
  inherited process;
  for i := 0 to nb - 1 do
  begin
    facture := getFacture(i);

    facture.Periode := Round(facture.Fin - facture.Debut);
    PeriodeTotale := PeriodeTotale + facture.Periode;
    QuantiteTotale := QuantiteTotale + facture.Quantite;
    PrixTotal := PrixTotal + facture.Valeur;
    if facture.Valeur <> 0 then
      facture.ValeurkWh := facture.Valeur / facture.Quantite
    else
      facture.ValeurkWh := 0;
  end;
  if QuantiteTotale <> 0 then
    PrixMoyen := PrixTotal / QuantiteTotale;
  kWhTotal := QuantiteTotale;
  if PeriodeTotale <> 0 then
  begin
    QuantiteAnnuelle := QuantiteTotale / PeriodeTotale * 365;
    prixTotalAnnuel := PrixTotal / PeriodeTotale * 365;
  end
end;


(* ************************
TTypeFactureFuel
************************ *)
constructor TTypeFactureFuel.Create;
begin
  inherited Create();
  innerTagName := 'factures-fuel';
end;

procedure TTypeFactureFuel.process;
var
  i : Integer;
  FC : TFacture;
begin
  inherited process;
  for i := 0 to nb - 1 do
  begin
    FC := getFacture(i);
    FC.Periode := Round(FC.Fin - FC.Debut);
    PeriodeTotale := PeriodeTotale + FC.Periode;
    QuantiteTotale := QuantiteTotale + FC.Quantite;
    PrixTotal := PrixTotal + FC.Valeur;
					{ Case FC.Variete of
						-1,0 : FC.rendement := 10;
					End; }
    FC.Rendement := 9.6;
    if FC.Valeur <> 0 then
      FC.ValeurkWh := FC.Valeur / (FC.Quantite * FC.Rendement)
    else
      FC.ValeurkWh := 0;
  end;
  if QuantiteTotale <> 0 then
    PrixMoyen := PrixTotal / QuantiteTotale;
  kWhTotal := QuantiteTotale * 9.6;
  if PeriodeTotale <> 0 then
  begin
    QuantiteAnnuelle := QuantiteTotale / PeriodeTotale * 365;
    prixTotalAnnuel := PrixTotal / PeriodeTotale * 365;
  end;
end;


(* ************************
TTypeFactureGaz
************************ *)
constructor TTypeFactureGaz.Create;
begin
  inherited Create();
  innerTagName := 'factures-gaz';
end;

procedure TTypeFactureGaz.process;
var
  i : Integer;
  FC : TFacture;
begin
  inherited process;
  for i := 0 to nb - 1 do
  begin
    FC := getFacture(i);
    FC.Periode := Round(FC.Fin - FC.Debut);
    PeriodeTotale := PeriodeTotale + FC.Periode;
    QuantiteTotale := QuantiteTotale + FC.Quantite;
    PrixTotal := PrixTotal + FC.Valeur;
    FC.Rendement := 1;
    if FC.Valeur <> 0 then
      FC.ValeurkWh := FC.Valeur / (FC.Quantite * FC.Rendement)
    else
      FC.ValeurkWh := 0;
  end;
  if QuantiteTotale <> 0 then
    PrixMoyen := PrixTotal / QuantiteTotale;
  kWhTotal := QuantiteTotale;

  if PeriodeTotale <> 0 then
  begin
    QuantiteAnnuelle := QuantiteTotale / PeriodeTotale * 365;
    prixTotalAnnuel := PrixTotal / PeriodeTotale * 365;
  end;
end;


(* ************************
TTypeFactureBois
************************ *)
constructor TTypeFactureBois.Create;
begin
  inherited Create();
  innerTagName := 'factures-bois';
end;


procedure TTypeFactureBois.process;
var
  i : Integer;
  FC : TFacture;
begin
  inherited process;
  for i := 0 to nb - 1 do
  begin
    FC := getFacture(i);
    FC.Periode := Round(FC.Fin - FC.Debut);
    PeriodeTotale := PeriodeTotale + FC.Periode;
    QuantiteTotale := QuantiteTotale + FC.Quantite;
    PrixTotal := PrixTotal + FC.Valeur;

{$MESSAGE warn 'placer en biblio ou cst'}

    case FC.Variete of
      - 1, 0 :
        FC.Rendement := 1200; // rien et h�tre
      1 :
        FC.Rendement := 1400; // ch�ne
    end;
    if FC.Valeur <> 0 then
      FC.ValeurkWh := FC.Valeur / (FC.Quantite * FC.Rendement)
    else
      FC.ValeurkWh := 0;
    kWhTotal := kWhTotal + (FC.Quantite * FC.Rendement);
  end;
  if QuantiteTotale <> 0 then
    PrixMoyen := PrixTotal / QuantiteTotale;
  if PeriodeTotale <> 0 then
  begin
    QuantiteAnnuelle := QuantiteTotale / PeriodeTotale * 365;
    prixTotalAnnuel := PrixTotal / PeriodeTotale * 365;
  end;
end;


(* ************************
TFactures
************************ *)
constructor TFactures.Create;
begin
  inherited Create;
  innerTagName := 'factures';
  listeElec := TTypeFactureElec.Create;
  listeFuel := TTypeFactureFuel.Create;
  listeGaz := TTypeFactureGaz.Create;
  listeBois := TTypeFactureBois.Create;
end;


destructor TFactures.Destroy;
begin
  listeElec.Free;
  listeFuel.Free;
  listeGaz.Free;
  listeBois.Free;
  inherited Destroy;
end;

procedure TFactures.innerSaveList(Liste : TStringList);
begin
  listeElec.saveList(Liste);
  listeFuel.saveList(Liste);
  listeGaz.saveList(Liste);
  listeBois.saveList(Liste);
end;

procedure TFactures.innerLoadList(Liste : TStringList);
begin
  listeElec.loadList(Liste);
  listeFuel.loadList(Liste);
  listeGaz.loadList(Liste);
  listeBois.loadList(Liste);
end;

procedure TFactures.innerSaveXml(node : IXmlNode);
begin
  listeElec.saveXml(node);
  listeFuel.saveXml(node);
  listeGaz.saveXml(node);
  listeBois.saveXml(node);
end;

procedure TFactures.innerLoadXml(node : IXmlNode);
begin
  listeElec.loadParentXml(node);
  listeFuel.loadParentXml(node);
  listeGaz.loadParentXml(node);
  listeBois.loadParentXml(node);
end;

procedure TFactures.process();
begin
  listeElec.process;
  listeFuel.process;
  listeGaz.process;
  listeBois.process;
end;


procedure TFactures.loadFromList(Liste : TStringList);
var
  i : Integer;
  Compte : Integer;
  facture : TFacture;
begin
  listeElec.removeAll;
  Compte := getInt(removeFirst(Liste));

  for i := 0 to Compte - 1 do
  begin
    facture := TFacture.Create;
    facture.loadFromList(Liste);
    listeElec.addObject(facture);
  end;

  listeFuel.removeAll;
  Compte := getInt(removeFirst(Liste));

  for i := 0 to Compte - 1 do
  begin
    facture := TFacture.Create;
    facture.loadFromList(Liste);
    listeFuel.addObject(facture);
  end;

  listeGaz.removeAll;
  Compte := getInt(removeFirst(Liste));

  for i := 0 to Compte - 1 do
  begin
    facture := TFacture.Create;
    facture.loadFromList(Liste);
    listeGaz.addObject(facture);
  end;

  listeBois.removeAll;
  Compte := getInt(removeFirst(Liste));

  for i := 0 to Compte - 1 do
  begin
    facture := TFacture.Create;
    facture.loadFromList(Liste);
    listeBois.addObject(facture);
  end;

  listeElec.PeriodeTotale := getInt(removeFirst(Liste));

  listeElec.QuantiteTotale := getDouble(removeFirst(Liste));

  listeElec.QuantiteAnnuelle := getDouble(removeFirst(Liste));

  listeElec.PrixTotal := getDouble(removeFirst(Liste));

  listeElec.prixTotalAnnuel := getDouble(removeFirst(Liste));

  listeElec.PrixMoyen := getDouble(removeFirst(Liste));

  listeElec.kWhTotal := getDouble(removeFirst(Liste));


  listeGaz.PeriodeTotale := getInt(removeFirst(Liste));

  listeGaz.QuantiteTotale := getDouble(removeFirst(Liste));

  listeGaz.QuantiteAnnuelle := getDouble(removeFirst(Liste));

  listeGaz.PrixTotal := getDouble(removeFirst(Liste));

  listeGaz.prixTotalAnnuel := getDouble(removeFirst(Liste));

  listeGaz.PrixMoyen := getDouble(removeFirst(Liste));

  listeGaz.kWhTotal := getDouble(removeFirst(Liste));


  listeBois.PeriodeTotale := getInt(removeFirst(Liste));

  listeBois.QuantiteTotale := getDouble(removeFirst(Liste));

  listeBois.QuantiteAnnuelle := getDouble(removeFirst(Liste));

  listeBois.PrixTotal := getDouble(removeFirst(Liste));

  listeBois.prixTotalAnnuel := getDouble(removeFirst(Liste));

  listeBois.PrixMoyen := getDouble(removeFirst(Liste));

  listeBois.kWhTotal := getDouble(removeFirst(Liste));


  listeFuel.PeriodeTotale := getInt(removeFirst(Liste));

  listeFuel.QuantiteTotale := getDouble(removeFirst(Liste));

  listeFuel.QuantiteAnnuelle := getDouble(removeFirst(Liste));

  listeFuel.PrixTotal := getDouble(removeFirst(Liste));

  listeFuel.prixTotalAnnuel := getDouble(removeFirst(Liste));

  listeFuel.PrixMoyen := getDouble(removeFirst(Liste));

  listeFuel.kWhTotal := getDouble(removeFirst(Liste));

end;


end.
