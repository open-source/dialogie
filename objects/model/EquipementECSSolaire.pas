unit EquipementECSSolaire;


interface

uses
  Classes,
  SysUtils,
  MenuEcsSolaire,
  IzObjectPersist,
  Xml.XMLIntf;

type

  TEquipementECSSolaire = class(TObjectPersist)
  protected
    procedure innerSaveList(Liste: TStringList); override;
    procedure innerLoadList(Liste: TStringList); override;

    procedure innerLoadXml(node : IXMLNode); override;
    procedure innerSaveXml(node : IXMLNode); override;
  public
    // Saisie utilisateur
    Actif:       boolean;
    Inclinaison, orientation: double;
    Situation:   double;
    Systeme:     integer;
    Surface:     double;
    Tyype:       integer;
    TemperatureEau, Volume: double;
    Isolation:   integer;
    Emplacement: integer;

    //Tables
    menuMonobloc: TBiblioEcsSolaire;
    menuSepare:   TBiblioEcsSolaire;

    menuBallon:          TBiblioEcssBallon;
    menuCaracteristique: TBiblioEcssCarac;
    menuCapteur:         TBiblioEcssCapteur;

    constructor Create; reintroduce;
    destructor Destroy; override;
    function TailleBallon(NbOccup: double): double;
  end;

implementation

uses
  IzUtilitaires,
  UtilitairesDialogie,
  XmlUtil;


procedure TEquipementECSSolaire.innerSaveList(Liste: TStringList);
begin
  liste.Add(setTag('actif', actif));
  liste.Add(setTag('inclinaison', Inclinaison));
  liste.Add(setTag('orientation', orientation));
  liste.Add(setTag('situation', situation));
  liste.Add(setTag('systeme', systeme));
  liste.Add(setTag('surface', surface));
  liste.Add(setTag('type', tyype));
  liste.Add(setTag('temperature-eau', TemperatureEau));
  liste.Add(setTag('volume', Volume));
  liste.Add(setTag('isolation', isolation));
  liste.Add(setTag('emplacement', Emplacement));
end;


procedure TEquipementECSSolaire.innerLoadList(Liste: TStringList);
begin
  Actif  := getTagBool(Liste, 'actif');
  Inclinaison := getTagDouble(Liste, 'inclinaison');
  orientation := getTagDouble(Liste, 'orientation');
  Situation := getTagDouble(Liste, 'situation');
  Systeme := getTagInt(Liste, 'systeme');
  Surface := getTagDouble(Liste, 'surface');
  Tyype  := getTagInt(Liste, 'type');
  TemperatureEau := getTagDouble(Liste, 'temperature-eau');
  Volume := getTagDouble(Liste, 'volume');
  Isolation := getTagInt(Liste, 'isolation');
  Emplacement := getTagInt(Liste, 'emplacement');
end;

procedure TEquipementECSSolaire.innerSaveXml(node : IXmlNode);
begin
  TXmlUtil.setTag(node, 'actif', actif);
  TXmlUtil.setTag(node, 'inclinaison', Inclinaison);
  TXmlUtil.setTag(node, 'orientation', orientation);
  TXmlUtil.setTag(node, 'situation', situation);
  TXmlUtil.setTag(node, 'systeme', systeme);
  TXmlUtil.setTag(node, 'surface', surface);
  TXmlUtil.setTag(node, 'type', tyype);
  TXmlUtil.setTag(node, 'temperature-eau', TemperatureEau);
  TXmlUtil.setTag(node, 'volume', Volume);
  TXmlUtil.setTag(node, 'isolation', isolation);
  TXmlUtil.setTag(node, 'emplacement', Emplacement);
end;


procedure TEquipementECSSolaire.innerLoadXml(node : IXmlNode);
begin
  Actif  := TXmlUtil.getTagBool(node, 'actif');
  Inclinaison := TXmlUtil.getTagDouble(node, 'inclinaison');
  orientation := TXmlUtil.getTagDouble(node, 'orientation');
  Situation := TXmlUtil.getTagDouble(node, 'situation');
  Systeme := TXmlUtil.getTagInt(node, 'systeme');
  Surface := TXmlUtil.getTagDouble(node, 'surface');
  Tyype  := TXmlUtil.getTagInt(node, 'type');
  TemperatureEau := TXmlUtil.getTagDouble(node, 'temperature-eau');
  Volume := TXmlUtil.getTagDouble(node, 'volume');
  Isolation := TXmlUtil.getTagInt(node, 'isolation');
  Emplacement := TXmlUtil.getTagInt(node, 'emplacement');
end;


function TEquipementECSSolaire.TailleBallon(NbOccup: double): double;
var
  Ent: integer;
begin
  Ent := Round(NbOccup);
  if Ent < 1 then
    ent := 1;
  if ent > menuBallon.Nb then
    ent := menuBallon.Nb;
  TailleBallon := menuBallon.table[ent];
end;


constructor TEquipementECSSolaire.Create;
begin
  inherited Create;
  innerTagName := 'equip-ecs-solaire';
  Actif := False;
  Inclinaison := 0;
  orientation := 0;
  Situation := 0;
  Systeme := 0;
  Surface := 0;
  Tyype := 0;
  TemperatureEau := 0;
  Isolation := 0;
  Emplacement := 0;
  menuMonobloc := TBiblioEcsSolaire.Create(dialogieLibPath + 'menumonobloc.txt');
  menuSepare := TBiblioEcsSolaire.Create(dialogieLibPath + 'menusepare.txt');
  menuBallon := TBiblioEcssBallon.Create(dialogieLibPath + 'ballons.txt');
  menuCaracteristique := TBiblioEcssCarac.Create(dialogieLibPath + 'caracteristiques.txt');
  menuCapteur := TBiblioEcssCapteur.Create(dialogieLibPath + 'typecapteurs.txt');
end;


destructor TEquipementECSSolaire.Destroy;
begin
  menuMonobloc.Free;
  menuSepare.Free;

  menuBallon.Free;
  menuCaracteristique.Free;
  menuCapteur.Free;

  inherited Destroy;
end;

end.
