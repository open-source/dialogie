unit Occupation;

interface

uses
  Classes,
  SysUtils,
  IzObjectPersist,
  Xml.XMLIntf;

type
  TPresence = array [1 .. 24] of integer;
  TMoisOcc = array [1 .. 12] of integer;
  Tpres = array [1 .. 12] of double;

  TOccupation = class(TObjectPersist)
  protected
    procedure innerSaveList(Liste : TStringList); override;
    procedure innerLoadList(Liste : TStringList); override;

    procedure innerLoadXml(node : IXMLNode); override;
    procedure innerSaveXml(node : IXMLNode); override;
  public
    NombreOccupants : Tpres;
    Occupation : integer;
    Presence : TPresence;
    Annuelle : boolean;
    Mois : TMoisOcc;
    NbPresence : integer;
    TablePresence : array of TPresence;
    ListeNomPresence : TStringList;
    destructor Destroy; override;
    constructor Create; override;
    procedure CalculePresence;
    function NbOccupants : double;

    function getPourcentOccupationAnnuelle() : double;
  end;

implementation

uses
  IzConstantes,
  IzUtilitaires,

  IzConstantesConso,
  UtilitairesDialogie, XmlUtil;


function TOccupation.NbOccupants : double;
var
  i : integer;
  Total : double;
begin
  Total := 0;
  for i := 1 to 12 do
  begin
    Total := Total + NombreOccupants[i];
  end;
  NbOccupants := Total / 12;
end;


constructor TOccupation.Create;
var
  i, j : integer;
  Liste : TStringList;
  Ligne : string;
begin
  inherited Create;
  innerTagName := 'occupation';
  Occupation := 0;
  Annuelle := True;
  for i := 1 to 12 do
  begin
    Mois[i] := 90;
    NombreOccupants[i] := 4;
  end;
  Liste := TStringList.Create;
  Liste.LoadFromFile(dialogieLibPath + FichierPresence);
  SetLength(TablePresence, Liste.Count);
  ListeNomPresence := TStringList.Create;
  NbPresence := Liste.Count;
  for i := 0 to Liste.Count - 1 do
  begin
    Ligne := Liste[i];
    ListeNomPresence.Add(getItemCharSep(Ligne, ','));
    for j := 1 to 24 do
      TablePresence[i, j] := getInt(getItemCharSep(Ligne, ','));
  end;
  Liste.Free;
end;


destructor TOccupation.Destroy;
begin
  SetLength(TablePresence, 0);
  FreeAndNil(ListeNomPresence);
  inherited Destroy;
end;


procedure TOccupation.innerSaveList(Liste : TStringList);
var
  i : integer;
begin

  Liste.Add(setTag('occupation', Occupation));
  Liste.Add(setTag('annuelle', Annuelle));
  for i := 1 to 12 do
  begin
    Liste.Add('<mois id=''' + IntToStr(i) + '''>');
    Liste.Add(setTag('num-mois', Mois[i]));
    Liste.Add(setTag('nb-occupant', NombreOccupants[i]));
    Liste.Add('</mois>');
  end;
end;


procedure TOccupation.innerLoadList(Liste : TStringList);
var
  i : integer;
begin
  Occupation := getTagInt(Liste, 'occupation');
  CalculePresence;
  Annuelle := getTagBool(Liste, 'annuelle');
  for i := 1 to 12 do
  begin
    removeFirst(Liste); // <mois>
    Mois[i] := getTagInt(Liste, 'num-mois');
    NombreOccupants[i] := getTagDouble(Liste, 'nb-occupant');
    removeFirst(Liste); // </mois>
  end;
end;


procedure TOccupation.innerSaveXml(node : IXmlNode);
var
  i : integer;
  child : IXmlNode;
  child2 : IXmlNode;
begin

  TXmlUtil.setTag(node, 'occupation', Occupation);
  TXmlUtil.setTag(node, 'annuelle', Annuelle);
  child := TXmlUtil.addChild(node, 'annee');
  for i := 1 to 12 do
  begin
    child2 := TXmlUtil.addChild(child, 'mois');
    TXmlUtil.setAtt(child2, 'id', i);
    TXmlUtil.setTag(child2, 'num-mois', Mois[i]);
    TXmlUtil.setTag(child2, 'nb-occupant', NombreOccupants[i]);
  end;
end;


procedure TOccupation.innerLoadXml(node : IXmlNode);
var
  i : integer;
  child : IXmlNode;
  child2 : IXmlNode;
  id : integer;
begin
  Occupation := TXmlUtil.getTagInt(node, 'occupation');
  CalculePresence;
  Annuelle := TXmlUtil.getTagBool(node, 'annuelle');
  child := TXmlUtil.getChild(node, 'annee');
  for i := 0 to child.ChildNodes.Count - 1 do
  begin
    child2 := child.ChildNodes[i];
    id := TXmlUtil.getAttInt(child2, 'id');
    Mois[id] := TXmlUtil.getTagInt(child2, 'num-mois');
    NombreOccupants[id] := TXmlUtil.getTagDouble(child2, 'nb-occupant');
  end;
end;


procedure TOccupation.CalculePresence;
begin
  Presence := TablePresence[Occupation];
end;


function TOccupation.getPourcentOccupationAnnuelle() : double;
var
  i : integer;
  dbl : double;
begin
  dbl := 0;
  for i := 1 to 12 do
    dbl := dbl + Mois[i] * DureeMois[i];
  Result := dbl / 100;
end;


end.
