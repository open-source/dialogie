unit IzObject;

interface

//uses
//	IzBiblioData;

	type TIzString = class(TObject)
		public
			value : string;
	end;

	type TIzObject = class(TObject)
		protected
			innerTagName : string;
      procedure setTagName(s : string);

		public
			constructor Create();overload;virtual;
			destructor Destroy();override;

			function getLabel() : string;virtual;
			property tagName : string read innerTagName write setTagName;
	end;

	type TIzObjectClass = class of TIzObject;


implementation

(**************************
TIzObject
**************************)
	constructor TIzObject.Create();
	begin
		inherited create();
		innerTagName := ClassName;
	end;

	destructor TIzObject.Destroy();
	begin
		inherited destroy();
	end;

	function TIzObject.getLabel() : string;
	begin
    result := tagName;
	end;

procedure TIzObject.setTagName(s: string);
begin
  if s = '' then
    s := ClassName;
  innerTagName := s;
end;

end.
