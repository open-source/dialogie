unit Generalites;

interface

uses
  Classes,
  SysUtils,
  IzUtilitaires,
  UtilitairesDialogie,
  IzObjectPersist,
  Xml.XMLIntf;


type
  TGeneralite = class(TObjectPersist)
  protected
    procedure innerSaveList(Liste : TStringList); override;
    procedure innerLoadList(Liste : TStringList); override;

    procedure innerLoadXml(node : IXMLNode); override;
    procedure innerSaveXml(node : IXMLNode); override;
  public
    TypeBatiment : TTypeBatiment;

    TypeEnergieCH : string;
    TypeEnergieEC : string;
    TypeEnergieCU : string;

    renovationFaible : boolean;
    classeInertie : TInertieClass;

    constructor Create; reintroduce;
  end;

implementation

uses
  XmlUtil;

constructor TGeneralite.Create;
begin
  inherited Create;
  innerTagName := 'generalites';
  TypeBatiment := BATIMENT_Maison;
  TypeEnergieCH := tableEnergiesDialogie[ENERGIE_ELECTRICITE];
  TypeEnergieEC := tableEnergiesDialogie[ENERGIE_ELECTRICITE];
  TypeEnergieCU := tableEnergiesDialogie[ENERGIE_ELECTRICITE];
  renovationFaible := false;
  classeInertie := icMediumHeavy;
end;


procedure TGeneralite.innerSaveList(Liste : TStringList);
begin
  Liste.Add(setTag('type-batiment', integer(TypeBatiment)));
  Liste.Add(setTag('energie-chauffage', TypeEnergieCH));
  Liste.Add(setTag('energie-ecs', TypeEnergieEC));
  Liste.Add(setTag('energie-cuisson', TypeEnergieCU));
  Liste.Add(setTag('renovation-faible', renovationFaible));
  Liste.Add(setTag('classe-inertie', integer(classeInertie)));
end;


procedure TGeneralite.innerLoadList(Liste : TStringList);
begin
  TypeBatiment := TTypeBatiment(getTagInt(Liste, 'type-batiment'));
  TypeEnergieCH := getTagString(Liste, 'energie-chauffage');
  TypeEnergieEC := getTagString(Liste, 'energie-ecs');
  TypeEnergieCU := getTagString(Liste, 'energie-cuisson');
  if isFullTag(liste, 'renovation-faible') then
    renovationFaible := getTagBool(Liste, 'renovation-faible')
  else
    renovationFaible := false;
  if isFullTag(liste, 'classe-inertie') then
    classeInertie := TInertieClass(getTagInt(Liste, 'classe-inertie'))
  else
    classeInertie := icMediumHeavy;
end;


procedure TGeneralite.innerSaveXml(node : IXmlNode);
begin
  TXmlUtil.setTag(node, 'type-batiment', integer(TypeBatiment));
  TXmlUtil.setTag(node, 'energie-chauffage', TypeEnergieCH);
  TXmlUtil.setTag(node, 'energie-ecs', TypeEnergieEC);
  TXmlUtil.setTag(node, 'energie-cuisson', TypeEnergieCU);
  TXmlUtil.setTag(node, 'renovation-faible', renovationFaible);
  TXmlUtil.setTag(node, 'classe-inertie', integer(classeInertie));
end;


procedure TGeneralite.innerLoadXml(node : IXmlNode);
begin
  TypeBatiment := TTypeBatiment(TXmlUtil.getTagInt(node, 'type-batiment'));
  TypeEnergieCH := TXmlUtil.getTagString(node, 'energie-chauffage');
  TypeEnergieEC := TXmlUtil.getTagString(node, 'energie-ecs');
  TypeEnergieCU := TXmlUtil.getTagString(node, 'energie-cuisson');
  renovationFaible := TXmlUtil.getTagBool(node, 'renovation-faible');
  classeInertie := TInertieClass(TXmlUtil.getTagInt(node, 'classe-inertie'));
end;

end.
