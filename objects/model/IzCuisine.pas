unit IzCuisine;

interface

uses
  IzObjectPersist, Classes, IzTypeObjectPersist, IzBiblioDialogie,
  IzUtilitaires, Occupation, IzConstantes, UtilitairesDialogie, IzObjectUtil,
  IzConstantesConso, Xml.XMLIntf;

type
  TIzEqCuisine = class(TApportInterneObject)
  protected
    procedure innerSaveList(Liste: TStringList); override;
    procedure innerLoadList(Liste: TStringList); override;

    procedure innerLoadXml(node : IXMLNode); override;
    procedure innerSaveXml(node : IXMLNode); override;
  public
    libelle:      string;
    energie:      string;
    utilisation:  string;
    consommation: double;


    constructor Create(); reintroduce; override;
  end;



type
  TIzTypeCuisine = class(TTypeObjectPersist)
  protected
    procedure innerSaveList(Liste: TStringList); override;
    procedure innerLoadList(Liste: TStringList); override;

    procedure innerLoadXml(node : IXMLNode); override;
    procedure innerSaveXml(node : IXMLNode); override;

    function calculConsoDefaut(var Table: TMensuel; surfHab: double; TypeHab: TTypeBatiment; numEnergie: integer; Occup: TMoisOcc): double;
    function consoCuisinePondere(cuisine: TIzEqCuisine): double;
    function consoCuisineAnnuel(cuisine: TIzEqCuisine; Occup: TMoisOcc): double;
    function consoCuisineMois(cuisine: TIzEqCuisine; Occup: TMoisOcc; numMois: integer): double;
  public

    typeCalcul: TTypeCalcul;

    function detailsCuisine(cuisine: TizEqCuisine; moisOccupation: TMoisOcc): string; overload;
    function detailsCuisine(Poste: integer; moisOccupation: TMoisOcc): string; overload;

    procedure CalculConso(var tableConso: TMensuel; var totalConso: double; var tableConsoApport: TValeursMensuelles; var totalConsoApport: double; var tableConsoHorsApport: TValeursMensuelles; var totalConsoHorsApport: double; surfHab: double; TypeHab: TTypeBatiment; TypeEnergie: integer; Occup: TMoisOcc);

    constructor Create(); reintroduce; virtual;
    destructor Destroy(); override;
    function getCuisine(rank: integer): TIzEqCuisine;
  end;


implementation


uses Contnrs, SysUtils, IzFormLog, XmlUtil;




(*
    class TIzEqCuisine
*)
constructor TIzEqCuisine.Create();
begin
  inherited Create();
  innerTagName := 'cuisson';
  libelle := '';
  energie := '';
  utilisation := '';

end;


procedure TIzEqCuisine.innerSaveList(Liste: TStringList);
begin
  inherited innerSaveList(liste);
  Liste.Add(setTag('type', libelle));
  Liste.Add(setTag('utilisation', utilisation));
  Liste.Add(setTag('energie', energie));
  Liste.Add(setTag('consommation', consommation));
end;


procedure TIzEqCuisine.innerLoadList(Liste: TStringList);
begin
  inherited innerLoadList(liste);
  libelle := getTagString(Liste, 'type');
  utilisation := getTagString(Liste, 'utilisation');
  energie := getTagString(Liste, 'energie');
  consommation := getTagDouble(Liste, 'consommation');
end;

procedure TIzEqCuisine.innerSaveXml(node : IXmlNode);
begin
  inherited innerSaveXml(node);
  TXmlUtil.setTag(node, 'type', libelle);
  TXmlUtil.setTag(node, 'utilisation', utilisation);
  TXmlUtil.setTag(node, 'energie', energie);
  TXmlUtil.setTag(node, 'consommation', consommation);
end;


procedure TIzEqCuisine.innerLoadXml(node : IXmlNode);
begin
  inherited innerLoadXml(node);
  libelle := TXmlUtil.getTagString(node, 'type');
  utilisation := TXmlUtil.getTagString(node, 'utilisation');
  energie := TXmlUtil.getTagString(node, 'energie');
  consommation := TXmlUtil.getTagDouble(node, 'consommation');
end;


(*
    class TIzCuisine
*)
constructor TIzTypeCuisine.Create();
begin
  inherited Create;

  innerTagName := 'cuissons';
  contentClass := TIzEqCuisine;
  typeCalcul := CALCUL_Defaut;
end;


destructor TIzTypeCuisine.Destroy();
begin
  inherited Destroy();
end;


procedure TIzTypeCuisine.innerSaveList(Liste: TStringList);
begin
  Liste.Add(setTag('type-calcul', integer(typeCalcul)));
  inherited innerSaveList(liste);
end;


procedure TIzTypeCuisine.innerLoadList(Liste: TStringList);
begin
  typeCalcul := TTypeCalcul(getTagInt(liste, 'type-calcul'));
  inherited innerLoadList(liste);
end;


procedure TIzTypeCuisine.innerSaveXml(node : IXmlNode);
begin
  TXmlUtil.setTag(node, 'type-calcul', integer(typeCalcul));
  inherited innerSaveXml(node);
end;


procedure TIzTypeCuisine.innerLoadXml(node : IXmlNode);
begin
  typeCalcul := TTypeCalcul(TXmlUtil.getTagInt(node, 'type-calcul'));
  inherited innerLoadXml(node);
end;


function TIzTypeCuisine.getCuisine(rank: integer): TIzEqCuisine;
begin
  Result := TIzEqCuisine(getObject(rank));
end;


function TIzTypeCuisine.DetailsCuisine(cuisine: TIzEqCuisine; moisOccupation: TMoisOcc): string;
begin
  Result := '0';
  if (typeCalcul <> CALCUL_Description) or (cuisine = nil) then
    exit;

  cuisine.consommation := consoCuisineAnnuel(cuisine, moisOccupation);
  Result := getString(cuisine.consommation, 0);
end;


function TIzTypeCuisine.DetailsCuisine(Poste: integer; moisOccupation: TMoisOcc): string;
var
  cuis: TIzEqCuisine;
  //    chaine : string;
begin
  cuis := getCuisine(poste);

  Result := detailsCuisine(cuis, moisOccupation);
end;


function TIzTypeCuisine.calculConsoDefaut(var Table: TMensuel; surfHab: double; TypeHab: TTypeBatiment; numEnergie: integer; Occup: TMoisOcc): double;
var
  conso: double;
  i: integer;
  idEnergie: TEnergieIdentificateur;
begin
  idEnergie := TEnergieIdentificateur(numEnergie);
  for i := 1 to 12 do
  begin
    if (idEnergie = ENERGIE_ELECTRICITE) then
    begin
      if (TypeHab = BATIMENT_Maison) then
      begin
        if SurfHab > 100 then
          Table[i, 0] := (4 * SurfHab + 450) * (Occup[i] * DureeMois[i] / 100) / 365
        else
          Table[i, 0] := (6 * SurfHab + 250) * (Occup[i] * DureeMois[i] / 100) / 365;
      end
      else if (TypeHab = BATIMENT_Appartement) then
      begin
        if SurfHab > 90 then
          Table[i, 0] := (4 * SurfHab + 196) * (Occup[i] * DureeMois[i] / 100) / 365
        else
          Table[i, 0] := (5 * SurfHab + 9) * (Occup[i] * DureeMois[i] / 100) / 365;
      end;
    end
    else//gaz nat, butane et propane
    begin
      if (TypeHab = BATIMENT_Maison) then
      begin
        if SurfHab > 100 then
          Table[i, 0] := (4 * SurfHab + 500) * (Occup[i] * DureeMois[i] / 100) / 365
        else
          Table[i, 0] := (6 * SurfHab + 300) * (Occup[i] * DureeMois[i] / 100) / 365;
      end
      else
      if (TypeHab = BATIMENT_Appartement) then
      begin
        if SurfHab > 90 then
          Table[i, 0] := (5 * SurfHab + 221) * (Occup[i] * DureeMois[i] / 100) / 365
        else
          Table[i, 0] := (7 * SurfHab + 10) * (Occup[i] * DureeMois[i] / 100) / 365;
      end;
    end;
(*
    if (idEnergie = ENERGIE_ELECTRICITE) and (TypeHab = BATIMENT_Maison) then
    begin
      if SurfHab > 100 then
        Table[i, 0] := (4 * SurfHab + 450) * (Occup[i] * DureeMois[i] / 100) / 365
      else
        Table[i, 0] := (6 * SurfHab + 250) * (Occup[i] * DureeMois[i] / 100) / 365;
    end
    else if (idEnergie = ENERGIE_GAZ_NATUREL) and (TypeHab = BATIMENT_Maison) then
    begin
      if SurfHab > 100 then
        Table[i, 0] := (4 * SurfHab + 500) * (Occup[i] * DureeMois[i] / 100) / 365
      else
        Table[i, 0] := (6 * SurfHab + 300) * (Occup[i] * DureeMois[i] / 100) / 365;
    end
    else if (idEnergie = ENERGIE_ELECTRICITE) and (TypeHab = BATIMENT_Appartement) then
    begin
      if SurfHab > 90 then
        Table[i, 0] := (4 * SurfHab + 196) * (Occup[i] * DureeMois[i] / 100) / 365
      else
        Table[i, 0] := (5 * SurfHab + 9) * (Occup[i] * DureeMois[i] / 100) / 365;
    end
    else if (idEnergie = ENERGIE_GAZ_NATUREL) and (TypeHab = BATIMENT_Appartement) then
    begin
      if SurfHab > 90 then
        Table[i, 0] := (5 * SurfHab + 221) * (Occup[i] * DureeMois[i] / 100) / 365
      else
        Table[i, 0] := (7 * SurfHab + 10) * (Occup[i] * DureeMois[i] / 100) / 365;
    end;
*)
    case idEnergie of
      ENERGIE_ELECTRICITE:
        Table[i, 1] := Table[i, 0];
      ENERGIE_GAZ_NATUREL:
        Table[i, 2] := Table[i, 0];
      ENERGIE_BUTANE:
        Table[i, 4] := Table[i, 0];
      ENERGIE_PROPANE:
        Table[i, 3] := Table[i, 0];
    end;
    //    if (idEnergie = ENERGIE_ELECTRICITE) then
    //      Table[i, 1] := Table[i, 0]
    //    else
    //      Table[i, 2] := Table[i, 0];
  end;
  Conso := 0;
  for i := 1 to 12 do
    Conso := Conso + Table[i, 0];
  Result := Conso;
end;


function TIzTypeCuisine.consoCuisinePondere(cuisine: TIzEqCuisine): double;
var
  coeff: double;
  consoBrute: double;
begin
  consoBrute := TBiblioDialogie.getLib().biblioCuisine.getConsommation(
    cuisine.libelle, cuisine.energie);
  coeff  := TUtilisationCoeffs.getCoeff(cuisine.utilisation);
  Result := consoBrute * coeff;
end;


function TIzTypeCuisine.consoCuisineAnnuel(cuisine: TIzEqCuisine; Occup: TMoisOcc): double;
var
  consoAn: double;
  i: integer;
begin
  consoAn := 0;
  for i := 1 to 12 do
  begin
    consoAn := consoAn + consoCuisineMois(cuisine, occup, i);
  end;
  Result := consoAn;
end;


function TIzTypeCuisine.consoCuisineMois(cuisine: TIzEqCuisine; Occup: TMoisOcc; numMois: integer): double;
begin
  Result := consoCuisinePondere(cuisine) * DureeMois[numMois] * Occup[numMois] / 36500;
  // /365/100
end;


procedure TIzTypeCuisine.CalculConso(var tableConso: TMensuel; var totalConso: double; var tableConsoApport: TValeursMensuelles; var totalConsoApport: double; var tableConsoHorsApport: TValeursMensuelles; var totalConsoHorsApport: double; surfHab: double; TypeHab: TTypeBatiment; TypeEnergie: integer; Occup: TMoisOcc);
var
  i, j: integer;
  //    Conso : double;
  //    coeff : Double;
  cuis: TIzEqCuisine;
  numEnergie: integer;
  dbl:  double;
begin

  formLog.addLog('cuisine.calculConso');
  //    coeff := 0;
  totalConso := 0;
  totalCOnsoApport := 0;
  totalConsoHorsApport := 0;


  for i := 1 to 12 do
  begin
    tableConsoApport.content[i] := 0;
    tableConsoHorsApport.content[i] := 0;
    for j := 0 to 8 do
      tableConso[i, j] := 0;
  end;


  if typeCalcul = CALCUL_Aucun then
  begin
    exit;//on renvoie tout vide
  end
  else if typeCalcul = CALCUL_Defaut then
  begin
    totalConso := calculConsoDefaut(tableConso, surfHab, typeHab, typeEnergie, occup);
    for i := 1 to 12 do
      tableConsoApport.content[i] := tableConso[i, 0];
    totalConsoApport := totalConso;
  end
  else if typeCalcul = CALCUL_Description then
  begin
    for i := 1 to 12 do
    begin
      //        coeff := Occup[i] * DureeMois[i] /36500;
      for j := 0 to nb - 1 do
      begin
        cuis := getCuisine(j);
        dbl  := consoCuisineMois(cuis, occup, i);//calcul pondere
        //          conso := dbl * coeff;

        //          tableConso[i,0] := tableConso[i,0] + conso;
        tableConso[i, 0] := tableConso[i, 0] + dbl;
        if (cuis.apportInterne) then
          //            tableConsoApport[i] := tableConsoApport[i] + conso
          tableConsoApport.content[i] := tableConsoApport.content[i] + dbl
        else
          //            tableConsoHorsApport[i] := tableConsoHorsApport[i] + conso;
          tableConsoHorsApport.content[i] := tableConsoHorsApport.content[i] + dbl;

        numEnergie := integer(getEnergieDialogie(cuis.energie));
        if numEnergie <> integer(ENERGIE_INCONNUE) then
          tableConso[i, numEnergie] := tableConso[i, numEnergie] + dbl;
      end;
      totalConso := totalConso + tableConso[i, 0];
      totalConsoApport := totalConsoApport + tableConsoApport.content[i];
      totalConsoHorsApport := totalConsoHorsApport + tableConsoHorsApport.content[i];
    end;
  end;
end;

end.
