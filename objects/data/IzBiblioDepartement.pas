unit IzBiblioDepartement;

interface

uses
  Classes, IzBiblioData;

	type TBiblioDepartementItem = record
		numero : integer;
		uid : string;
		nom : string;
		indFChPercent : double;
		indFEcsAncien : double;
		indFEcsRecent : double;
		collFEcsAncien : double;
		collFEcsRecent : double;
		collFEcsSolarairePercent : double;
		DhRef : double;
		PRef  : double;
		C3 : double;
		C4 : double;
		zoneEte : string;
		zoneHiver : string;
		TempExtBase : double;
		eVal : double;
		altitudeIdMini : integer;
		altitudeIdMaxi : integer;
		Nref : double;
	end;


	type TBiblioDepartement = class(TBiblioData)
		private
			table : Array of TBiblioDepartementItem;
		public
			constructor Create(aFileName : string); override;

			function getNoms() : TStrings;
			function getNumeros() : TStrings;

			function getItem(nom : string) : TBiblioDepartementItem;
			function getItemFromNumero(numero : string) : TBiblioDepartementItem;
	end;

	var
		DEPARTEMENT_AUCUN : TBiblioDepartementItem;
implementation
	uses IzUtilitaires, SysUtils;

	constructor TBiblioDepartement.Create(aFileName : string);
	Var
	 Temp : TStringList;
	 i : Integer;
	 Ligne : String;
	Begin
		inherited create(aFileName);
		Temp := getStringList();
		SetLength(table, nb);
		For i := 0 to Temp.Count - 1 do
		Begin
			ligne := Temp[i];

			table[i].numero := getInt(getItemTab(ligne));
			table[i].uid := getItemTab(ligne);
			table[i].nom := getItemTab(ligne);
			table[i].indFChPercent := getDouble(getItemTab(ligne));

			table[i].indFEcsAncien := getDouble(getItemTab(ligne));
			table[i].indFEcsRecent := getDouble(getItemTab(ligne));
			table[i].collFEcsAncien := getDouble(getItemTab(ligne));
			table[i].collFEcsRecent := getDouble(getItemTab(ligne));
			table[i].collFEcsSolarairePercent := getDouble(getItemTab(ligne));
			table[i].DhRef := getDouble(getItemTab(ligne));
			table[i].PRef  := getDouble(getItemTab(ligne));
			table[i].C3 := getDouble(getItemTab(ligne));
			table[i].C4 := getDouble(getItemTab(ligne));
			table[i].zoneEte := getItemTab(ligne);
			table[i].zoneHiver := getItemTab(ligne);
			table[i].TempExtBase := getDouble(getItemTab(ligne));
			table[i].eVal := getDouble(getItemTab(ligne));
			table[i].altitudeIdMini := getInt(getItemTab(ligne));
			table[i].altitudeIdMaxi := getInt(getItemTab(ligne));
			table[i].Nref := getDouble(getItemTab(ligne));

		End;
		Temp.free;
	End;


	function TBiblioDepartement.getNoms() : TStrings;
	var
		i: Integer;
		libelles : TStringList;
	begin
		libelles := createComboList;
		for i := 0 to nb - 1 do
		begin
			libelles.Add(table[i].nom);
		end;
		result := libelles;
	end;


	function TBiblioDepartement.getNumeros() : TStrings;
	var
		i: Integer;
		libelles : TStringList;
	begin
		libelles := createComboList;
		for i := 0 to nb - 1 do
		begin
			libelles.Add(table[i].uid);
		end;
		result := libelles;
	end;


	function TBiblioDepartement.getItem(nom : string) : TBiblioDepartementItem;
	Var
	 i : Integer;
	 item : TBiblioDepartementItem;
	Begin

		result := DEPARTEMENT_AUCUN;
		For i := 0 to nb - 1 do
		Begin
			item := table[i];
			if (item.nom = nom) then
			begin
				result := item;
				exit;
			end;
		End;
	End;

	function TBiblioDepartement.getItemFromNumero(numero : string) : TBiblioDepartementItem;
	Var
	 i : Integer;
	 item : TBiblioDepartementItem;
	Begin

		result := DEPARTEMENT_AUCUN;
		For i := 0 to nb - 1 do
		Begin
			item := table[i];
			if (item.uid = numero) then
			begin
				result := item;
				exit;
			end;
		End;
	End;


	initialization
		DEPARTEMENT_AUCUN.numero := -1;
		DEPARTEMENT_AUCUN.uid := '';
		DEPARTEMENT_AUCUN.nom := '';
		DEPARTEMENT_AUCUN.indFChPercent := -1;
		DEPARTEMENT_AUCUN.indFEcsAncien := -1;
		DEPARTEMENT_AUCUN.indFEcsRecent := -1;
		DEPARTEMENT_AUCUN.collFEcsAncien := -1;
		DEPARTEMENT_AUCUN.collFEcsSolarairePercent := -1;
		DEPARTEMENT_AUCUN.DhRef := -1;
		DEPARTEMENT_AUCUN.PRef := -1;
		DEPARTEMENT_AUCUN.C3 := -1;
		DEPARTEMENT_AUCUN.C4 := -1;
		DEPARTEMENT_AUCUN.zoneEte := '';
		DEPARTEMENT_AUCUN.zoneHiver := '';
		DEPARTEMENT_AUCUN.TempExtBase := -1;
		DEPARTEMENT_AUCUN.eVal := -1;
		DEPARTEMENT_AUCUN.altitudeIdMini := -1;
		DEPARTEMENT_AUCUN.altitudeIdMaxi := -1;
		DEPARTEMENT_AUCUN.Nref := -1;


end.
