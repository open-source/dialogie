unit IzBiblioCollectifPosition;
//BiblioCollectifPosition
interface

uses
	IzBiblioData, Classes, UtilitairesDialogie;

	type TBiblioCollectifPositionItem = record
		positionappart : TTypePositionAppartement;
		libelle : string;
		lpbe : double;
		lpbi : double;
		ltp : double;
		lpib : double;
		lpih : double;
		ltte : double;
		ltti : double;
		ltc : double;
		lrf : double;
		sCombles : double;
		sTerrasse : double;
		sSol : double;
    pontTh : double;
	end;

	type TBiblioCollectifPosition = class(TBiblioData)
		private
			table : Array of TBiblioCollectifPositionItem;
		public
			constructor Create(aFileName : string); override;
			function getItem(libelle : string) : TBiblioCollectifPositionItem;overload;
			function getItem(position : TTypePositionAppartement) : TBiblioCollectifPositionItem;overload;
	end;

implementation

	uses
    System.SysUtils,
    IzUtilitaires;

	Constructor TBiblioCollectifPosition.Create(aFileName : string);
	Var
	 Temp : TStringList;
	 i : Integer;
	 Ligne : String;
	Begin
		inherited create(aFileName);
		Temp := getStringList();
		SetLength(table, nb);
		For i := 0 to Temp.Count - 1 do
		Begin
			ligne := Temp[i];
			table[i].positionappart := TTypePositionAppartement(getInt(getItemTab(ligne)));
			table[i].libelle := getItemTab(ligne);
			table[i].lpbe := getDouble(getItemTab(ligne));
			table[i].lpbi := getDouble(getItemTab(ligne));
			table[i].ltp := getDouble(getItemTab(ligne));
			table[i].lpib := getDouble(getItemTab(ligne));
			table[i].lpih := getDouble(getItemTab(ligne));
			table[i].ltte := getDouble(getItemTab(ligne));
			table[i].ltti := getDouble(getItemTab(ligne));
			table[i].ltc := getDouble(getItemTab(ligne));
			table[i].lrf := getDouble(getItemTab(ligne));
			table[i].sCombles := getDouble(getItemTab(ligne));
			table[i].sTerrasse := getDouble(getItemTab(ligne));
			table[i].sSol := getDouble(getItemTab(ligne));
			table[i].pontTh := getDouble(getItemTab(ligne));
      if table[i].pontTh = 0 then
        table[i].pontTh := 1;//0 impossible alors valeur de 1 par d�faut
		End;
		Temp.free;
	End;


	function TBiblioCollectifPosition.getItem(libelle : string) : TBiblioCollectifPositionItem;
	var
		i: integer;
	begin

		result := table[0];
		for i := 0 to nb - 1 do
		begin
			if table[i].libelle  = libelle then
			begin
				result := table[i];
				exit;
			end;
		end;
	end;



	function TBiblioCollectifPosition.getItem(position : TTypePositionAppartement) : TBiblioCollectifPositionItem;
	var
		i: integer;
	begin

		result := table[0];
		for i := 0 to nb - 1 do
		begin
			if table[i].positionappart  = position then
			begin
				result := table[i];
				exit;
			end;
		end;
	end;

end.
