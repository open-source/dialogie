unit MenuBureau;

interface

uses
	IzBiblioData;

	type
		TBiblioBureauItem = record

			typeAppareil,
			modele,
			utilisation,
			puissance,
			nbHeureHebdo,
			nbJourUtilAnnee,
			consoAnnuelle : string;
		end;



	type TBiblioBureau = class(TBiblioData)
		public
			table : Array of TBiblioBureauItem;
			constructor Create(aFileName : string); override;

	end;

implementation

uses
	Classes, IzUtilitaires;

	Constructor TBiblioBureau.Create(aFileName : string);
	Var
	 Temp : TStringList;
	 i : Integer;
	 Ligne : String;
	 chaine : string;
	Begin
		inherited create(aFileName);
		Temp := getStringList();
		SetLength(table, nb+1);
		For i := 1 to Temp.Count do
		Begin
			ligne := Temp[i-1];

			table[i].typeAppareil := getItemTab(ligne);
			table[i].modele := getItemTab(ligne);
			table[i].utilisation := getItemTab(ligne);
			chaine := getItemTab(ligne);//on va transformer '1.00 xxxx' en '1.00'
			chaine := getItemSep(chaine, ' ');
			table[i].puissance := getItemTab(chaine);
			table[i].nbHeureHebdo := getItemTab(ligne);
			table[i].nbJourUtilAnnee := getItemTab(ligne);
			table[i].consoAnnuelle := getItemTab(ligne);
		End;
		Temp.free;
	End;


end.
