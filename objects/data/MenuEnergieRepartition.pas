unit MenuEnergieRepartition;

interface

uses
	BiblioData;

	type
		TEnergieRepartition = Record
				Energie,
				Fournisseur,
				TypeTarif,
				Mix,
				Tarif : String;
				ChauffDirect,
				ChauffAccumul,
				ECSDirect,
				ECSAccumul,
				Cuisson,
				AuxChauff,
				AuxVentil,
				lavage,
				froid,
				Bureautique,
				Electro,
				Eclairage,
				clim : Double;
	End;

	type TBiblioEnergieRepartition = class(TBiblioData)
		private
			biblio : array of TEnergieRepartition;
		public
			constructor Create(aFileName : string); override;

			function getItem(rank : integer ) : TEnergieRepartition;
	end;



implementation

uses
  Classes, Constantes;



	Constructor TBiblioEnergieRepartition.Create(aFileName : string);
	Var
		Temp : TStringList;
		i : Integer;
		Ligne,Chaine : String;
		Valeur : Double;
		Erreur : Integer;
	Begin
		inherited create(aFileName);
		Temp := getStringList();
		SetLength(biblio, nb+1);
		For i := 0 to Temp.Count -1 do
		Begin
			ligne := Temp[i];
					Ligne := temp[i];

					Chaine := Copy(Ligne,1,Pos(CHAR_TAB,Ligne)-1);
					biblio[i].Energie := Chaine;
					Delete(Ligne,1,Pos(CHAR_TAB,Ligne));

					Chaine := Copy(Ligne,1,Pos(CHAR_TAB,Ligne)-1);
					biblio[i].Fournisseur := Chaine;
					Delete(Ligne,1,Pos(CHAR_TAB,Ligne));

					Chaine := Copy(Ligne,1,Pos(CHAR_TAB,Ligne)-1);
					biblio[i].TypeTarif := Chaine;
					Delete(Ligne,1,Pos(CHAR_TAB,Ligne));

					Chaine := Copy(Ligne,1,Pos(CHAR_TAB,Ligne)-1);
					biblio[i].Mix  := Chaine;
					Delete(Ligne,1,Pos(CHAR_TAB,Ligne));

					Chaine := Copy(Ligne,1,Pos(CHAR_TAB,Ligne)-1);
					biblio[i].Tarif := Chaine;
					Delete(Ligne,1,Pos(CHAR_TAB,Ligne));

					Chaine := Copy(Ligne,1,Pos(CHAR_TAB,Ligne)-1);
					Val(chaine, valeur,erreur);
					biblio[i].ChauffDirect := valeur;
					Delete(Ligne,1,Pos(CHAR_TAB,Ligne));

					Chaine := Copy(Ligne,1,Pos(CHAR_TAB,Ligne)-1);
					Val(chaine, valeur,erreur);
					biblio[i].ChauffAccumul := Valeur;
					Delete(Ligne,1,Pos(CHAR_TAB,Ligne));

					Chaine := Copy(Ligne,1,Pos(CHAR_TAB,Ligne)-1);
					Val(chaine, valeur,erreur);
					biblio[i].ECSDirect := Valeur;
					Delete(Ligne,1,Pos(CHAR_TAB,Ligne));

					Chaine := Copy(Ligne,1,Pos(CHAR_TAB,Ligne)-1);
					Val(chaine, valeur,erreur);
					biblio[i].ECSAccumul := valeur;
					Delete(Ligne,1,Pos(CHAR_TAB,Ligne));

					Chaine := Copy(Ligne,1,Pos(CHAR_TAB,Ligne)-1);
					Val(chaine, valeur,erreur);
					biblio[i].Cuisson := valeur;
					Delete(Ligne,1,Pos(CHAR_TAB,Ligne));

					Chaine := Copy(Ligne,1,Pos(CHAR_TAB,Ligne)-1);
					Val(chaine, valeur,erreur);
					biblio[i].AuxChauff := valeur;
					Delete(Ligne,1,Pos(CHAR_TAB,Ligne));

					Chaine := Copy(Ligne,1,Pos(CHAR_TAB,Ligne)-1);
					Val(chaine, valeur,erreur);
					biblio[i].AuxVentil := valeur;
					Delete(Ligne,1,Pos(CHAR_TAB,Ligne));

					Chaine := Copy(Ligne,1,Pos(CHAR_TAB,Ligne)-1);
					Val(chaine, valeur,erreur);
					biblio[i].Lavage := valeur;
					Delete(Ligne,1,Pos(CHAR_TAB,Ligne));

					Chaine := Copy(Ligne,1,Pos(CHAR_TAB,Ligne)-1);
					Val(chaine, valeur,erreur);
					biblio[i].Froid := valeur;
					Delete(Ligne,1,Pos(CHAR_TAB,Ligne));

					Chaine := Copy(Ligne,1,Pos(CHAR_TAB,Ligne)-1);
					Val(chaine, valeur,erreur);
					biblio[i].Bureautique := valeur;
					Delete(Ligne,1,Pos(CHAR_TAB,Ligne));

					Chaine := Copy(Ligne,1,Pos(CHAR_TAB,Ligne)-1);
					Val(chaine, valeur,erreur);
					biblio[i].Electro := valeur;
					Delete(Ligne,1,Pos(CHAR_TAB,Ligne));

					Chaine := Copy(Ligne,1,Pos(CHAR_TAB,Ligne)-1);
					Val(chaine, valeur,erreur);
					biblio[i].Eclairage := valeur;
					Delete(Ligne,1,Pos(CHAR_TAB,Ligne));

					Val(Ligne, valeur,erreur);
					biblio[i].clim := valeur;
					Delete(Ligne,1,Pos(CHAR_TAB,Ligne));
		End;
		Temp.free;
end;

	function TBiblioEnergieRepartition.getItem(rank: Integer) : TEnergieRepartition;
	begin
		getItem := biblio[rank];
	end;

end.
