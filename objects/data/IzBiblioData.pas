unit IzBiblioData;

interface

uses
	Classes;

	type TBiblioData = class
		protected
			fileName : string;
			innerNb : integer;
			function getStringList() : TStringList;
			procedure addList(var liste : TStringList; chaine : string);
		public
			constructor Create(aFileName : string);virtual;
			property nb : integer read innerNb;
	end;


implementation

uses
	SysUtils;

	constructor TBiblioData.Create(aFileName: string);
	begin
		fileName := aFileName;
	end;

	function TBiblioData.getStringList() : TStringList;
	var
		temp : TStringList;
		fullPath : string;
	begin
		temp := TStringList.Create;
		fullPath := fileName;
		temp.LoadFromfile(fullPath);
		innerNb := temp.count;
		result := temp;
	end;

	procedure TBiblioData.addList(var liste : TStringList; chaine : string);
  begin
		if (chaine <> '') and (liste.indexOf(chaine) = -1) then
			Liste.add(chaine);
	end;

end.
