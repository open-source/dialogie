unit MenuEcsSolaire;

interface

uses
	IzBiblioData;

	//les types définissant les caractéristiques
	type
		TCapteur = Record
				Nom : String;
				Car1,Car2 : Double;
		End;

		TCarac = Record
				Circulation : Integer;
				Circulateur : Double;
				Echangeur, Appoint : Integer;
				Tuyau1,Tuyau2,Regulation, Debit : Double;
		End;

	type TMenuSolaire = array[1..7] of String;

	//les biliothèques proprement dites
	type TBiblioEcsSolaire = class (TBiblioData)
		public
			table : array of TMenuSolaire;
			constructor Create(aFileName : string);override;
      destructor Destroy();override;
	end;

	type TBiblioEcssBallon = class (TBiblioData)
		public
			table : array of double;
			constructor Create(aFileName : string);override;
      destructor Destroy();override;
	end;

	type TBiblioEcssCarac = class (TBiblioData)
		public
			table : array of TCarac;
			constructor Create(aFileName : string);override;
      destructor Destroy();override;
	end;

	type TBiblioEcssCapteur = class (TBiblioData)
		public
			table : array of TCapteur;
			constructor Create(aFileName : string);override;
      destructor Destroy();override;
	end;

implementation

uses
	System.Classes,
  System.SysUtils,
  IzUtilitaires;

	Constructor TBiblioEcsSolaire.Create(aFileName : string);
	Var
		Temp : TStringList;
		i : integer;
		Ligne : String;
  j: Integer;
	Begin
		inherited create(aFileName);

		Temp := getStringList();
		setLength(table, nb + 1);
		For i := 1 to nb do
		Begin
			ligne := Temp[i - 1];
			for j := 1 to 7  do
			begin
        table[i, j] := getItemTab(ligne);
			end;

		 End;
     temp.free;
	End;


destructor TBiblioEcsSolaire.Destroy();
begin
  setLength(table, 0);
  inherited Destroy;
end;



	Constructor TBiblioEcssBallon.Create(aFileName : string);
	Var
		Temp : TStringList;
		i: integer;
	Begin
		inherited create(aFileName);
		Temp := getStringList();
		setLength(table, nb + 1);
		For i := 1 to nb do
		Begin
			table[i] := getDouble(Temp[i - 1]);
		End;
    temp.free;
	End;


destructor TBiblioEcssBallon.Destroy();
begin
  setLength(table, 0);
  inherited Destroy;
end;


	Constructor TBiblioEcssCarac.Create(aFileName : string);
	Var
		Temp : TStringList;
		i : integer;
		Ligne : String;
	Begin
		inherited create(aFileName);
		Temp := getStringList();

		SetLength(table, nb + 1);
		For i := 1 to nb do
		Begin
			ligne := Temp[i - 1];
      getItemTab(ligne);//on passe le premier élément
			table[i].Circulation := getInt(getItemTab(ligne));
			table[i].Circulateur := getInt(getItemTab(ligne));
			table[i].Echangeur := getInt(getItemTab(ligne));
			table[i].Appoint := getInt(getItemTab(ligne));
			table[i].Tuyau1 := getDouble(getItemTab(ligne));
			table[i].Tuyau2 := getDouble(getItemTab(ligne));
			table[i].Regulation := getDouble(getItemTab(ligne));
			table[i].Debit := getDouble(getItemTab(ligne));
		End;
    temp.free;
	End;


destructor TBiblioEcssCarac.Destroy();
begin
  setLength(table, 0);
  inherited Destroy;
end;


	Constructor TBiblioEcssCapteur.Create(aFileName : string);
	Var
		Temp : TStringList;
		i, erreur : integer;
		valeur : double;
		Ligne : String;
	Begin
		inherited create(aFileName);
		Temp := getStringList();
		SetLength(table, nb + 1);
		For i := 1 to nb do
		Begin
			ligne := Temp[i - 1];

			table[i].Nom := getItemTab(ligne);

			Val(getItemTab(ligne),Valeur,Erreur);
			table[i].Car1 := Valeur;

			Val(getItemTab(ligne),Valeur,Erreur);
			table[i].Car2 := Valeur;
		End;
    temp.free;
	End;


destructor TBiblioEcssCapteur.Destroy();
begin
  setLength(table, 0);
  inherited Destroy;
end;

end.
