unit MenuEnergieTarif;

interface

uses
	IzBiblioData, MenuEnergieRepartition, Classes;

	type
		TEnergieTarif = Record
				Energie,
				Fournisseur,
				TType,
				Tarif,
				Usage,
				Reference,
				miseAjour : String;
				Tarif1,
				Abonnement,
				CO2,
				SO2,
				Nucleaire,
				Uranium,
				Gaznat,
				Petrole,
				Charbon,
				Solaire,
				Biomasse,
				vent,
				Hydraul,
				geothermie : Double;
		End;

	type TBiblioEnergieTarif = class(TBiblioData)
		private
			biblio : array of TEnergieTarif;
			innerEnergieRepartition : TBiblioEnergieRepartition;
			function getEnergieRepartition() : TBiblioEnergieRepartition;

		public
			constructor Create(aFileName : string); override;

			property energieRepartition : TBiblioEnergieRepartition read getEnergieRepartition;
			
			function getMenuFournisseur(TypeEnergie : Integer) : TStringList;
			function getMenuType(TypeEnergie : Integer; Fourniss : String) : TStringList;

			Function ressource(four, ty : string ; TypeEnergie,Usage,TypeRessource : Integer) : Double;
			Function reference(four, ty : string ; TypeEnergie : Integer) : String;
			Function dateMAJ(four, ty : string ; TypeEnergie : Integer) : TDateTime;

			Function prixAbonnement(four, ty : string ; TypeEnergie : Integer) : Double;
			Function prixkWh(four, ty : string ; TypeEnergie : Integer) : Double;
			Function prixEnergie(four, ty : string ; TypeEnergie,Usage : Integer) : Double;

			Function emissionCO2(four, ty : string ; TypeEnergie,Usage : Integer) : Double;
			Function emissionNucleaire(four, ty : string ; TypeEnergie,Usage : Integer) : Double;
			Function emissionSO2(four, ty : string ; TypeEnergie,Usage : Integer) : Double;
			Function emissionTEP(four, ty : string ; TypeEnergie,Usage : Integer) : Double;
	end;


implementation

uses
	Constantes, Utilitaires, Dialogs, SysUtils;



	Constructor TBiblioEnergieTarif.Create(aFileName : string);
	Var
		Temp : TStringList;
		i : Integer;
		Ligne,Chaine : String;
		Valeur : Double;
		Erreur : Integer;
	Begin
		inherited create(aFileName);

		Temp := getStringList();
		SetLength(biblio, nb+1);
		For i := 0 to Temp.Count - 1 do
		Begin
			ligne := Temp[i];

			Chaine := Copy(Ligne,1,Pos(CHAR_TAB,Ligne)-1);
			biblio[i].Energie := Chaine;
			Delete(Ligne,1,Pos(CHAR_TAB,Ligne));

			Chaine := Copy(Ligne,1,Pos(CHAR_TAB,Ligne)-1);
			biblio[i].Fournisseur := Chaine;
			Delete(Ligne,1,Pos(CHAR_TAB,Ligne));

			Chaine := Copy(Ligne,1,Pos(CHAR_TAB,Ligne)-1);
			biblio[i].TType := Chaine;
			Delete(Ligne,1,Pos(CHAR_TAB,Ligne));

			Chaine := Copy(Ligne,1,Pos(CHAR_TAB,Ligne)-1);
			biblio[i].Tarif  := Chaine;
			Delete(Ligne,1,Pos(CHAR_TAB,Ligne));

			Chaine := Copy(Ligne,1,Pos(CHAR_TAB,Ligne)-1);
			biblio[i].Usage := Chaine;
			Delete(Ligne,1,Pos(CHAR_TAB,Ligne));

			Chaine := Copy(Ligne,1,Pos(CHAR_TAB,Ligne)-1);
			Val(chaine, valeur,erreur);
			biblio[i].Tarif1 := Valeur;
			Delete(Ligne,1,Pos(CHAR_TAB,Ligne));

			Chaine := Copy(Ligne,1,Pos(CHAR_TAB,Ligne)-1);
			Val(chaine, valeur,erreur);
			biblio[i].Abonnement := Valeur;
			Delete(Ligne,1,Pos(CHAR_TAB,Ligne));

			Chaine := Copy(Ligne,1,Pos(CHAR_TAB,Ligne)-1);
			biblio[i].Reference := Chaine;
			Delete(Ligne,1,Pos(CHAR_TAB,Ligne));

			Chaine := Copy(Ligne,1,Pos(CHAR_TAB,Ligne)-1);
			biblio[i].MiseAJour := Chaine;
			Delete(Ligne,1,Pos(CHAR_TAB,Ligne));

			Chaine := Copy(Ligne,1,Pos(CHAR_TAB,Ligne)-1);
			Val(chaine, valeur,erreur);
			biblio[i].CO2 := valeur;
			Delete(Ligne,1,Pos(CHAR_TAB,Ligne));

			Chaine := Copy(Ligne,1,Pos(CHAR_TAB,Ligne)-1);
			Val(chaine, valeur,erreur);
			biblio[i].SO2 := valeur;
			Delete(Ligne,1,Pos(CHAR_TAB,Ligne));

			Chaine := Copy(Ligne,1,Pos(CHAR_TAB,Ligne)-1);
			Val(chaine, valeur,erreur);
			biblio[i].Nucleaire := valeur;
			Delete(Ligne,1,Pos(CHAR_TAB,Ligne));

			Chaine := Copy(Ligne,1,Pos(CHAR_TAB,Ligne)-1);
			Val(chaine, valeur,erreur);
			biblio[i].Uranium := valeur;
			Delete(Ligne,1,Pos(CHAR_TAB,Ligne));

			Chaine := Copy(Ligne,1,Pos(CHAR_TAB,Ligne)-1);
			Val(chaine, valeur,erreur);
			biblio[i].Gaznat := valeur;
			Delete(Ligne,1,Pos(CHAR_TAB,Ligne));

			Chaine := Copy(Ligne,1,Pos(CHAR_TAB,Ligne)-1);
			Val(chaine, valeur,erreur);
			biblio[i].Petrole := valeur;
			Delete(Ligne,1,Pos(CHAR_TAB,Ligne));

			Chaine := Copy(Ligne,1,Pos(CHAR_TAB,Ligne)-1);
			Val(chaine, valeur,erreur);
			biblio[i].Charbon := valeur;
			Delete(Ligne,1,Pos(CHAR_TAB,Ligne));

			Chaine := Copy(Ligne,1,Pos(CHAR_TAB,Ligne)-1);
			Val(chaine, valeur,erreur);
			biblio[i].Solaire := valeur;
			Delete(Ligne,1,Pos(CHAR_TAB,Ligne));

			Chaine := Copy(Ligne,1,Pos(CHAR_TAB,Ligne)-1);
			Val(chaine,valeur ,erreur);
			biblio[i].Biomasse := valeur;
			Delete(Ligne,1,Pos(CHAR_TAB,Ligne));

			Chaine := Copy(Ligne,1,Pos(CHAR_TAB,Ligne)-1);
			Val(chaine, valeur,erreur);
			biblio[i].Vent := valeur;
			Delete(Ligne,1,Pos(CHAR_TAB,Ligne));

			Chaine := Copy(Ligne,1,Pos(CHAR_TAB,Ligne)-1);
			Val(chaine, valeur,erreur);
			biblio[i].Hydraul := valeur;
			Delete(Ligne,1,Pos(CHAR_TAB,Ligne));
		End;
		Temp.free;
	end;

	function TBiblioEnergieTarif.getEnergieRepartition() : TBiblioEnergieRepartition;
	begin
		if (innerEnergieRepartition = nil) then
		begin
			innerEnergieRepartition := TBiblioEnergieRepartition.Create('Fichiers internes/Repartition.txt');
		end;
		getEnergieRepartition := innerEnergieRepartition;
	end;



	Function TBiblioEnergieTarif.getMenuFournisseur(TypeEnergie : Integer) : TStringList;
	Var
		i,j : Integer;
		Liste : TStringList;
		Double : Boolean;
	Begin
		Liste := TStringList.create;
		For i := 0 to nb-1 do
		Begin
			if Energie(biblio[i].Energie) = TypeEnergie then
				Liste.add(biblio[i].Fournisseur);
		End;
		For i := Liste.count-1 downto 1 do
		Begin
			Double := False;
			For j := i-1 downto 0 do
				if Liste[i] = Liste[j] then
					Double := True;
			if Double then
				Liste.Delete(i);
		End;
		getMenuFournisseur := Liste;
	End;


	function TBiblioEnergieTarif.getMenuType(TypeEnergie : Integer; Fourniss : String) : TStringList;
	Var
		i,j : Integer;
		Liste : TStringList;
		Double : Boolean;
	Begin
		Liste := TStringList.create;
		For i := 0 to nb -1 do
		Begin
			if (Energie(biblio[i].Energie) = TypeEnergie) and (Fourniss = biblio[i].Fournisseur) then
				Liste.add(biblio[i].TType);
		End;
		For i := Liste.count-1 downto 1 do
		Begin
			Double := False;
			For j := i-1 downto 0 do
				if Liste[i] = Liste[j] then
					Double := True;
			if Double then
					Liste.Delete(i);
		End;
		getMenuType := Liste;
	End;



Function TBiblioEnergieTarif.Ressource(four, ty : string ; TypeEnergie,Usage,TypeRessource : Integer) : Double;
Var
	 i,j, colRepartition : Integer;
	 energo : String;
	 PourcentageUsage : Double;
	 PrixE : Double;
Begin
		PourcentageUsage := 0;
		ressource := 0;
		 Energo := TableEnergies[TypeEnergie];
	 If TypeEnergie = 1 then
	 Begin
		 PrixE := 0;
		 For i := 0 to energieRepartition.nb-1 do
		 Begin
					if (energieRepartition.getItem(i).Fournisseur = Four) and
						 (energieRepartition.getItem(i).Energie = Energo) then
					Begin
							 Case Usage of
								 1  : PourcentageUsage := energieRepartition.getItem(i).ChauffDirect;
								 11 : Begin
												PourcentageUsage := energieRepartition.getItem(i).ChauffAccumul;
											End;
								 2  : PourcentageUsage := energieRepartition.getItem(i).ECSDirect;
								 12 : PourcentageUsage := energieRepartition.getItem(i).ECSAccumul;
								 3  : PourcentageUsage := energieRepartition.getItem(i).Cuisson;
								 20  : PourcentageUsage := energieRepartition.getItem(i).AuxChauff;
								 21  : PourcentageUsage := energieRepartition.getItem(i).AuxVentil;
								 22  : PourcentageUsage := energieRepartition.getItem(i).lavage;
								 23  : PourcentageUsage := energieRepartition.getItem(i).Froid;
								 24  : PourcentageUsage := energieRepartition.getItem(i).Bureautique;
								 25  : PourcentageUsage := energieRepartition.getItem(i).Electro;
								 5  : PourcentageUsage := energieRepartition.getItem(i).Eclairage;
								 6  : PourcentageUsage := energieRepartition.getItem(i).clim;
								 else Begin
															ShowMessage('Erreur d''usage');
											End;
							 End;
							 For j := 0 to nb-1 do
							 Begin
										if (biblio[j].Energie = Energo) and
											 (biblio[j].Fournisseur = Four) and
											 (biblio[j].TType = Ty) and
											 (biblio[j].Tarif = energieRepartition.getItem(i).Tarif) and
											 (biblio[j].Usage = energieRepartition.getItem(i).Mix) then
										Case TypeRessource Of
											 1 : PrixE := PrixE + (PourcentageUsage*biblio[j].Uranium);
											 2 : PrixE := PrixE + (PourcentageUsage*biblio[j].Gaznat);
											 3 : PrixE := PrixE + (PourcentageUsage*biblio[j].Petrole);
											 4 : PrixE := PrixE + (PourcentageUsage*biblio[j].Charbon);
											 5 : PrixE := PrixE + (PourcentageUsage*biblio[j].Solaire);
											 6 : PrixE := PrixE + (PourcentageUsage*biblio[j].Biomasse);
											 7 : PrixE := PrixE + (PourcentageUsage*biblio[j].vent);
											 8 : PrixE := PrixE + (PourcentageUsage*biblio[j].Hydraul);
											 9 : PrixE := PrixE + (PourcentageUsage*biblio[j].Geothermie);
										End;
							 End;
					End;
		 End;
		 Ressource := PrixE;
	 End
	 else if TypeEnergie = 9 then
	 Begin
				If TypeRessource = 5 then
						Ressource := 1
				else
						Ressource := 0;

	 End
	 else
	 Begin
				For j := 0 to nb-1 do
				Begin
						 if (biblio[j].Energie = Energo) and
											 (biblio[j].Fournisseur = Four) and
											 (biblio[j].TType = Ty) then
										Case TypeRessource Of
											 1 : Ressource := biblio[j].Uranium;
											 2 : Ressource := biblio[j].Gaznat;
											 3 : Ressource := biblio[j].Petrole;
											 4 : Ressource := biblio[j].Charbon;
											 5 : Ressource := biblio[j].Solaire;
											 6 : Ressource := biblio[j].Biomasse;
											 7 : Ressource := biblio[j].vent;
											 8 : Ressource := biblio[j].Hydraul;
											 9 : Ressource := biblio[j].Geothermie;
										End;
						 //Ressource := (TableEnergie[j].CO2);
				End;
	 End;
End;

Function TBiblioEnergieTarif.PrixAbonnement(four, ty : string ; TypeEnergie : Integer) : Double;
Var
	 i : Integer;
Begin
	 PrixAbonnement :=0;
	 if TypeEnergie = 9 then
	 Begin
				prixAbonnement := 0;
	 End
	 else
		 For i := 0 to nb-1 do
		 Begin
					if (Energie(biblio[i].Energie) = TypeEnergie) and
						 (biblio[i].Fournisseur = Four) and
						 (biblio[i].TType = Ty) then
									PrixAbonnement := biblio[i].Abonnement;
		 End;
End;

Function TBiblioEnergieTarif.Reference(four, ty : string ; TypeEnergie : Integer) : String;
Var
	 i : Integer;
Begin

	 if TypeEnergie = 9 then
	 Begin
				Reference := '';
	 End
	 else
		 For i := 0 to nb-1 do
		 Begin
					if (Energie(biblio[i].Energie) = TypeEnergie) and
						 (biblio[i].Fournisseur = Four) and
						 (biblio[i].TType = Ty) then
									Reference := biblio[i].Reference;
		 End;
End;


Function TBiblioEnergieTarif.DateMAJ(four, ty : string ; TypeEnergie : Integer) : TDateTime;
Var
	 i : Integer;
Begin
		dateMAJ := Date;
	 if TypeEnergie <> 9 then
		 For i := 0 to nb-1 do
		 Begin
					if (Energie(biblio[i].Energie) = TypeEnergie) and
						 (biblio[i].Fournisseur = Four) and
						 (biblio[i].TType = Ty) then
					Begin

									DateMAJ := StrToInt(biblio[i].miseAjour)+1462;
					End;
		 End;
End;

Function TBiblioEnergieTarif.PrixkWh(four, ty : string ; TypeEnergie : Integer) : Double;
Var
	 i : Integer;
	 Maxi : Double;
Begin

	 Maxi := 0;
	 if TypeEnergie = 9 then
	 Begin
				PrixkWh := 0;
	 End
	 else
		 For i := 0 to nb-1 do
		 Begin
					if (Energie(biblio[i].Energie) = TypeEnergie) and
						 (biblio[i].Fournisseur = Four) and
						 (biblio[i].TType = Ty) then
					Begin
							 if biblio[i].Tarif1 > Maxi then
									Maxi := biblio[i].Tarif1;
					End;
		 End;
		 PrixkWh := Maxi;
End;


	Function TBiblioEnergieTarif.PrixEnergie(four, ty : string ; TypeEnergie,Usage : Integer) : Double;
	Var
		i,j, colRepartition : Integer;
		energo : String;
		PourcentageUsage : Double;
		PrixE : Double;
	Begin
		Energo := TableEnergies[TypeEnergie];
		PourcentageUsage := 0;

		If TypeEnergie = 1 then
		Begin
			PrixE := 0;
			For i := 0 to energieRepartition.nb-1 do
			Begin
					if (energieRepartition.getItem(i).Fournisseur = Four) and
						 (energieRepartition.getItem(i).Energie = Energo) then
					Begin
							 Case Usage of
								 1  : PourcentageUsage := energieRepartition.getItem(i).ChauffDirect;
								 11 : Begin
												PourcentageUsage := energieRepartition.getItem(i).ChauffAccumul;
											End;
								 2  : PourcentageUsage := energieRepartition.getItem(i).ECSDirect;
								 12 : PourcentageUsage := energieRepartition.getItem(i).ECSAccumul;
								 3  : PourcentageUsage := energieRepartition.getItem(i).Cuisson;
								 20  : PourcentageUsage := energieRepartition.getItem(i).AuxChauff;
								 21  : PourcentageUsage := energieRepartition.getItem(i).AuxVentil;
								 22  : PourcentageUsage := energieRepartition.getItem(i).lavage;
								 23  : PourcentageUsage := energieRepartition.getItem(i).Froid;
								 24  : PourcentageUsage := energieRepartition.getItem(i).Bureautique;
								 25  : PourcentageUsage := energieRepartition.getItem(i).Electro;
								 5  : PourcentageUsage := energieRepartition.getItem(i).Eclairage;
								 6  : PourcentageUsage := energieRepartition.getItem(i).clim;
								 else begin
													 ShowMessage('Erreur d''usage')
											end;
							 End;
							 For j := 0 to nb-1 do
							 Begin
										if (biblio[j].Energie = Energo) and
											 (biblio[j].Fournisseur = Four) and
											 (biblio[j].TType = Ty) and
											 (biblio[j].Tarif = energieRepartition.getItem(i).Tarif) and
											 (biblio[j].Usage = energieRepartition.getItem(i).Mix) then
										PrixE := PrixE + (PourcentageUsage*biblio[j].Tarif1);
							 End;
					End;
			End;
			PrixEnergie := PrixE;
		End
		else if TypeEnergie = 9 then
		Begin
			PrixEnergie := 0;
		End
		else
		Begin
				For j := 0 to nb-1 do
				Begin
						 if (biblio[j].Energie = Energo) and
											 (biblio[j].Fournisseur = Four) and
											 (biblio[j].TType = Ty) then
						 PrixEnergie := (biblio[j].Tarif1);
				End;
		End;
	End;

Function TBiblioEnergieTarif.EmissionCO2(four, ty : string ; TypeEnergie,Usage : Integer) : Double;
Var
	 i,j, colRepartition : Integer;
	 energo : String;
	 PourcentageUsage : Double;
	 PrixE : Double;
Begin

		 Energo := TableEnergies[TypeEnergie];
	 If TypeEnergie = 1 then
	 Begin
		 PrixE := 0;
		 For i := 0 to energieRepartition.nb-1 do
		 Begin
					if (energieRepartition.getItem(i).Fournisseur = Four) and
						 (energieRepartition.getItem(i).Energie = Energo) then
					Begin
							 Case Usage of
								 1  : PourcentageUsage := energieRepartition.getItem(i).ChauffDirect;
								 11 : Begin
												PourcentageUsage := energieRepartition.getItem(i).ChauffAccumul;
											End;
								 2  : PourcentageUsage := energieRepartition.getItem(i).ECSDirect;
								 12 : PourcentageUsage := energieRepartition.getItem(i).ECSAccumul;
								 3  : PourcentageUsage := energieRepartition.getItem(i).Cuisson;
								 20  : PourcentageUsage := energieRepartition.getItem(i).AuxChauff;
								 21  : PourcentageUsage := energieRepartition.getItem(i).AuxVentil;
								 22  : PourcentageUsage := energieRepartition.getItem(i).lavage;
								 23  : PourcentageUsage := energieRepartition.getItem(i).Froid;
								 24  : PourcentageUsage := energieRepartition.getItem(i).Bureautique;
								 25  : PourcentageUsage := energieRepartition.getItem(i).Electro;
								 5  : PourcentageUsage := energieRepartition.getItem(i).Eclairage;
								 6  : PourcentageUsage := energieRepartition.getItem(i).clim;
								 else begin
													 ShowMessage('Erreur d''usage');
											end;
							 End;
							 For j := 0 to nb-1 do
							 Begin
										if (biblio[j].Energie = Energo) and
											 (biblio[j].Fournisseur = Four) and
											 (biblio[j].TType = Ty) and
											 (biblio[j].Tarif = energieRepartition.getItem(i).Tarif) and
											 (biblio[j].Usage = energieRepartition.getItem(i).Mix) then
										PrixE := PrixE + (PourcentageUsage*biblio[j].CO2/1000);
							 End;
					End;
		 End;
		 EmissionCO2 := PrixE;
	 End
	 else if TypeEnergie = 9 then
	 Begin
				EmissionCO2 := 0;
	 End
	 else
	 Begin
				For j := 0 to nb-1 do
				Begin
						 if (biblio[j].Energie = Energo) and
											 (biblio[j].Fournisseur = Four) and
											 (biblio[j].TType = Ty) then
						 EmissionCO2 := (biblio[j].CO2/1000);
				End;
	 End;
End;

Function TBiblioEnergieTarif.EmissionNucleaire(four, ty : string ; TypeEnergie,Usage : Integer) : Double;
Var
	 i,j, colRepartition : Integer;
	 energo : String;
	 PourcentageUsage : Double;
	 PrixE : Double;
Begin
		 Energo := TableEnergies[TypeEnergie];
	 If TypeEnergie = 1 then
	 Begin
		 PrixE := 0;
		 For i := 0 to energieRepartition.nb-1 do
		 Begin
					if (energieRepartition.getItem(i).Fournisseur = Four) and
						 (energieRepartition.getItem(i).Energie = Energo) then
					Begin
							 Case Usage of
								 1  : PourcentageUsage := energieRepartition.getItem(i).ChauffDirect;
								 11 : Begin
												PourcentageUsage := energieRepartition.getItem(i).ChauffAccumul;
											End;
								 2  : PourcentageUsage := energieRepartition.getItem(i).ECSDirect;
								 12 : PourcentageUsage := energieRepartition.getItem(i).ECSAccumul;
								 3  : PourcentageUsage := energieRepartition.getItem(i).Cuisson;
								 20  : PourcentageUsage := energieRepartition.getItem(i).AuxChauff;
								 21  : PourcentageUsage := energieRepartition.getItem(i).AuxVentil;
								 22  : PourcentageUsage := energieRepartition.getItem(i).lavage;
								 23  : PourcentageUsage := energieRepartition.getItem(i).Froid;
								 24  : PourcentageUsage := energieRepartition.getItem(i).Bureautique;
								 25  : PourcentageUsage := energieRepartition.getItem(i).Electro;
								 5  : PourcentageUsage := energieRepartition.getItem(i).Eclairage;
								 6  : PourcentageUsage := energieRepartition.getItem(i).clim;
								 else begin
													 ShowMessage('Erreur d''usage')
											end;
							 End;
							 For j := 0 to nb-1 do
							 Begin
										if (biblio[j].Energie = Energo) and
											 (biblio[j].Fournisseur = Four) and
											 (biblio[j].TType = Ty) and
											 (biblio[j].Tarif = energieRepartition.getItem(i).Tarif) and
											 (biblio[j].Usage = energieRepartition.getItem(i).Mix) then
										PrixE := PrixE + (PourcentageUsage*biblio[j].Nucleaire/1000);
							 End;
					End;
		 End;
		 EmissionNucleaire := PrixE;
	 End
	 else if TypeEnergie = 9 then
	 Begin
				EmissionNucleaire := 0;
	 End
	 else
	 Begin
				For j := 0 to nb-1 do
				Begin
						 if (biblio[j].Energie = Energo) and
											 (biblio[j].Fournisseur = Four) and
											 (biblio[j].TType = Ty) then
						 EmissionNucleaire := (PourcentageUsage*biblio[j].Nucleaire/1000);
				End;
	 End;
End;

Function TBiblioEnergieTarif.EmissionSO2(four, ty : string ; TypeEnergie,Usage : Integer) : Double;
Var
	 i,j, colRepartition : Integer;
	 energo : String;
	 PourcentageUsage : Double;
	 PrixE : Double;
Begin
	PourcentageUsage := 0;
		 Energo := TableEnergies[TypeEnergie];
	 If TypeEnergie = 1 then
	 Begin
		 PrixE := 0;
		 For i := 0 to energieRepartition.nb-1 do
		 Begin
					if (energieRepartition.getItem(i).Fournisseur = Four) and
						 (energieRepartition.getItem(i).Energie = Energo) then
					Begin
							 Case Usage of
								 1  : PourcentageUsage := energieRepartition.getItem(i).ChauffDirect;
								 11 : Begin
												PourcentageUsage := energieRepartition.getItem(i).ChauffAccumul;
											End;
								 2  : PourcentageUsage := energieRepartition.getItem(i).ECSDirect;
								 12 : PourcentageUsage := energieRepartition.getItem(i).ECSAccumul;
								 3  : PourcentageUsage := energieRepartition.getItem(i).Cuisson;
								 20  : PourcentageUsage := energieRepartition.getItem(i).AuxChauff;
								 21  : PourcentageUsage := energieRepartition.getItem(i).AuxVentil;
								 22  : PourcentageUsage := energieRepartition.getItem(i).lavage;
								 23  : PourcentageUsage := energieRepartition.getItem(i).Froid;
								 24  : PourcentageUsage := energieRepartition.getItem(i).Bureautique;
								 25  : PourcentageUsage := energieRepartition.getItem(i).Electro;
								 5  : PourcentageUsage := energieRepartition.getItem(i).Eclairage;
								 6  : PourcentageUsage := energieRepartition.getItem(i).clim;
								 else begin
													 ShowMessage('Erreur d''usage')
											end;
							 End;
							 For j := 0 to nb-1 do
							 Begin
										if (biblio[j].Energie = Energo) and
											 (biblio[j].Fournisseur = Four) and
											 (biblio[j].TType = Ty) and
											 (biblio[j].Tarif = energieRepartition.getItem(i).Tarif) and
											 (biblio[j].Usage = energieRepartition.getItem(i).Mix) then
										PrixE := PrixE + (PourcentageUsage*biblio[j].SO2/1000);
							 End;
					End;
		 End;
		 EmissionSO2 := PrixE;
	 End
	 else if TypeEnergie = 9 then
	 Begin
				EmissionSO2 := 0;
	 End
	 else
	 Begin
				For j := 0 to nb-1 do
				Begin
						 if (biblio[j].Energie = Energo) and
											 (biblio[j].Fournisseur = Four) and
											 (biblio[j].TType = Ty) then
						 EmissionSO2 := (biblio[j].SO2/1000);
				End;
	 End;
End;

Function TBiblioEnergieTarif.EmissionTEP(four, ty : string ; TypeEnergie,Usage : Integer) : Double;
Var
	 i,j, colRepartition : Integer;
	 energo : String;
	 PourcentageUsage : Double;
	 PrixE : Double;
Begin
	EmissionTEP := 0;
		 Energo := TableEnergies[TypeEnergie];
	 If TypeEnergie = 1 then
	 Begin
		 PrixE := 0;
		 For i := 0 to energieRepartition.nb-1 do
		 Begin
					if (energieRepartition.getItem(i).Fournisseur = Four) and
						 (energieRepartition.getItem(i).Energie = Energo) then
					Begin
							 Case Usage of
								 1  : PourcentageUsage := energieRepartition.getItem(i).ChauffDirect;
								 11 : Begin
												PourcentageUsage := energieRepartition.getItem(i).ChauffAccumul;
											End;
								 2  : PourcentageUsage := energieRepartition.getItem(i).ECSDirect;
								 12 : PourcentageUsage := energieRepartition.getItem(i).ECSAccumul;
								 3  : PourcentageUsage := energieRepartition.getItem(i).Cuisson;
								 20  : PourcentageUsage := energieRepartition.getItem(i).AuxChauff;
								 21  : PourcentageUsage := energieRepartition.getItem(i).AuxVentil;
								 22  : PourcentageUsage := energieRepartition.getItem(i).lavage;
								 23  : PourcentageUsage := energieRepartition.getItem(i).Froid;
								 24  : PourcentageUsage := energieRepartition.getItem(i).Bureautique;
								 25  : PourcentageUsage := energieRepartition.getItem(i).Electro;
								 5  : PourcentageUsage := energieRepartition.getItem(i).Eclairage;
								 6  : PourcentageUsage := energieRepartition.getItem(i).clim;
								 else begin
													 ShowMessage('Erreur d''usage')
											end;
							 End;
							 For j := 0 to nb-1 do
							 Begin
										if (biblio[j].Energie = Energo) and
											 (biblio[j].Fournisseur = Four) and
											 (biblio[j].TType = Ty) and
											 (biblio[j].Tarif = energieRepartition.getItem(i).Tarif) and
											 (biblio[j].Usage = energieRepartition.getItem(i).Mix) then
										//PrixE := PrixE + (PourcentageUsage*TableEnergie[j].TEP);
							 End;
					End;
		 End;
		 EmissionTEP := PrixE;
	 End
	 else if TypeEnergie = 9 then
	 Begin
				EmissionTEP := 0;
	 End
	 else
	 Begin
				For j := 0 to nb-1 do
				Begin
						 if (biblio[j].Energie = Energo) and
											 (biblio[j].Fournisseur = Four) and
											 (biblio[j].TType = Ty) then
						 //EmissionTEP := (PourcentageUsage*TableEnergie[j].TEP);
				End;
	 End;
End;



end.
