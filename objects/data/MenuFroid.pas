unit MenuFroid;

interface

uses
	IzBiblioData;

	type
		TBiblioFroidItem = record
			categorie,
			description,
			classe,
			puissance,
			nbHeureJour,
			nbJourAnnee,
			kWhAnnee : string;
		end;

	type TBiblioFroid = class(TBiblioData)
		public
			table : Array of TBiblioFroidItem;
			constructor Create(aFileName : string); override;

	end;

implementation

uses
	Classes, IzUtilitaires;

	Constructor TBiblioFroid.Create(aFileName : string);
	Var
	 Temp : TStringList;
	 i : Integer;
	 Ligne : String;
	 chaine : string;
	Begin
		inherited create(aFileName);
		Temp := getStringList();
		SetLength(table, nb+1);
		For i := 1 to Temp.Count do
		Begin
			ligne := Temp[i-1];

			table[i].categorie := getItemTab(ligne);
			table[i].description := getItemTab(ligne);
			table[i].classe := getItemTab(ligne);
			chaine := getItemTab(ligne);//on va transformer '1.00 xxxx' en '1.00'
			chaine := getItemSep(chaine, ' ');
			table[i].puissance := getItemTab(chaine);
			table[i].nbHeureJour := getItemTab(ligne);
			table[i].nbJourAnnee := getItemTab(ligne);
			table[i].kWhAnnee := getItemTab(ligne);
		End;
		Temp.free;
	End;


end.
