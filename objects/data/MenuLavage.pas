unit MenuLavage;

interface

uses
	IzBiblioData;

	type
		TBiblioLavageItem = record
			typeAppareil,
			modele,
			classe,
			consoCycle,
			freqUtilHebdo,
			dureeAnnuelle,
			consoEstimee : string;
		end;

	type TBiblioLavage = class(TBiblioData)
		public
			table : Array of TBiblioLavageItem;
			constructor Create(aFileName : string); override;

	end;

implementation

uses
	Classes, IzUtilitaires;

	Constructor TBiblioLavage.Create(aFileName : string);
	Var
	 Temp : TStringList;
	 i : Integer;
	 Ligne : String;
	 chaine : string;
	Begin
		inherited create(aFileName);
		Temp := getStringList();
		SetLength(table, nb+1);
		For i := 1 to Temp.Count do
		Begin
			ligne := Temp[i-1];

			table[i].typeAppareil := getItemTab(ligne);
			table[i].modele := getItemTab(ligne);
			table[i].classe := getItemTab(ligne);
			chaine := getItemTab(ligne);//on va transformer '1.00 kWh/c' en '1.00'
			chaine := getItemSep(chaine, ' ');
			table[i].consoCycle := getItemTab(chaine);
			table[i].freqUtilHebdo := getItemTab(ligne);
			table[i].dureeAnnuelle := getItemTab(ligne);
			table[i].consoEstimee := getItemTab(ligne);
		End;
		Temp.free;
	End;


end.
