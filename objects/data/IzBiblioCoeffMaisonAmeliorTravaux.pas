unit IzBiblioCoeffMaisonAmeliorTravaux;
interface

uses
	IzBiblioData, Classes, UtilitairesDialogie;

	type TBiblioCoeffMaisonAmeliorTravauxItem = record
		anneeMin : integer;
		anneeMax : integer;
		ameliorMur : double;
		ameliorSol : double;
		ameliorToitTerrasse : double;
		ameliorToitPerdu : double;
		ameliorToitHabite : double;
		ameliorVit : double;
	end;

	type TBiblioCoeffMaisonAmeliorTravaux = class(TBiblioData)
	//on obitne des coeff d'am�lioration : les Kg, ils doivetn �tre divis�s
	//par la HSP standard : 2.5 pour obtenir le AMELIOR-XX
  //  ce qui est fait dans SaisieDesc (on * 0.4 )
		private
			table : Array of TBiblioCoeffMaisonAmeliorTravauxItem;
		public
			constructor Create(aFileName : string); override;
			function getAmeliorations(nbNiveaux : double ; typeToiture : TTypeToiture ; annee : integer ; var ameliorMur : double ; var ameliorSol : double ; var ameliorToit : double ; var ameliorVit : double) : boolean;
			function getAmelioration(nbNiveaux : double ; typeToiture : TTypeToiture ; annee : integer ; typeAmelioration : TTypeAmelioration) : double;
	end;

implementation

uses
  System.SysUtils,
  IzUtilitaires;

	Constructor TBiblioCoeffMaisonAmeliorTravaux.Create(aFileName : string);
	Var
	 Temp : TStringList;
	 i : Integer;
	 Ligne : String;
	Begin
		inherited create(aFileName);
		Temp := getStringList();
		SetLength(table, nb);
		For i := 0 to Temp.Count - 1 do
		Begin
			ligne := Temp[i];
			table[i].anneeMin := getInt(getItemTab(ligne));
			table[i].anneeMax := getInt(getItemTab(ligne));
			table[i].ameliorMur := getDouble(getItemTab(ligne));
			table[i].ameliorSol := getDouble(getItemTab(ligne));
			table[i].ameliorToitPerdu := getDouble(getItemTab(ligne));
			table[i].ameliorToitHabite := getDouble(getItemTab(ligne));
			table[i].ameliorToitTerrasse := getDouble(getItemTab(ligne));
			table[i].ameliorVit := getDouble(getItemTab(ligne));
		End;
		Temp.free;
	End;


	function TBiblioCoeffMaisonAmeliorTravaux.getAmeliorations(nbNiveaux : double ; typeToiture : TTypeToiture ; annee : integer ; var ameliorMur : double ; var ameliorSol : double ; var ameliorToit : double ; var ameliorVit : double) : boolean;
	var
		i: integer;
	begin
		result := false;
		for i := 0 to nb - 1 do
		begin
			if (
				((table[i].anneeMin  <= annee) or (table[i].anneeMin = -1)) and
				((table[i].anneeMax = -1) or (table[i].anneeMax  >= annee))
			 ) then
			begin
				result := true;
				ameliorMur := table[i].ameliorMur;
				ameliorSol := table[i].ameliorSol;
				//si on a des niveaux entiers, c'est forc�ment comble perdu
				//donc demi niveau = niveau habit�
        //et si mixte, alors moiti� terrasse, moitie niveau ou demi niveau (perdu ou habit�)
				case typeToiture of
					TOITURE_COMBLE:
						if isInteger(nbNiveaux) then
							ameliorToit := table[i].ameliorToitPerdu
						else
							ameliorToit := table[i].ameliorToitHabite;
					TOITURE_MIXTE:
						if isInteger(nbNiveaux) then
							ameliorToit := (table[i].ameliorToitPerdu + table[i].ameliorToitTerrasse) / 2
						else
							ameliorToit := (table[i].ameliorToitHabite + table[i].ameliorToitTerrasse) / 2;
					TOITURE_TERRASSE:
						ameliorToit := table[i].ameliorToitTerrasse;
				end;
				ameliorVit := table[i].ameliorVit;
				exit;
			end;
		end;
	end;

	function TBiblioCoeffMaisonAmeliorTravaux.getAmelioration(nbNiveaux : double ; typeToiture : TTypeToiture ; annee : integer ; typeAmelioration : TTypeAmelioration) : double;
	var
		aMur : double;
		aSol : double;
		aToit : double;
		aVit : double;
	begin
		result := -1;
		if not(getAmeliorations(nbNiveaux, typeToiture, annee, aMur, aSol, aToit, aVit)) then
		begin
			exit;
		end;
		case typeAmelioration of
			AMELIORATION_MUR :
				result := aMur;
			AMELIORATION_SOL :
				result := aSol;
			AMELIORATION_TOIT :
				result := aToit;
			AMELIORATION_VIT :
      	result := aVit;
		end;
	end;

end.
