unit IzBiblioCoeffMaisonAb;

interface

uses
	IzBiblioData, Classes;

	type TBiblioCoeffMaisonAbItem = record
		formeBatiment : integer;
		nbNiveaux : double;
		mitoyennete : integer;
		A : double;
		B : double;
	end;

	type TBiblioCoeffMaisonAb = class(TBiblioData)
		private
			table : Array of TBiblioCoeffMaisonAbItem;
		public
			constructor Create(aFileName : string); override;
			function getCoeffs(formeBatiment : integer ; nbNiveaux : double ; mitoyennete : integer ; var A : double ; var B : double) : boolean;
	end;

implementation

uses
  System.SysUtils,
  IzUtilitaires,
  System.Math;

	Constructor TBiblioCoeffMaisonAb.Create(aFileName : string);
	Var
	 Temp : TStringList;
	 i : Integer;
	 Ligne : String;
	Begin
		inherited create(aFileName);
		Temp := getStringList();
		SetLength(table, nb);
		For i := 0 to Temp.Count - 1 do
		Begin
			ligne := Temp[i];
			table[i].formeBatiment := getInt(getItemTab(ligne));
			table[i].nbNiveaux := getDouble(getItemTab(ligne));
			table[i].mitoyennete := getInt(getItemTab(ligne));
			table[i].A := getDouble(getItemTab(ligne));
			table[i].B := getDouble(getItemTab(ligne));
		End;
		Temp.free;
	End;


	function TBiblioCoeffMaisonAb.getCoeffs(formeBatiment : integer ; nbNiveaux : double ; mitoyennete : integer ; var A : double ; var B : double) : boolean;
	var
		i: integer;
	begin
(*$message hint 'attention on borne le nb de niveau max en dur car au del� de 2.5 on doit faire de la saisie par parois'*)

		result := false;
		for i := 0 to nb - 1 do
		begin
			if (
				(table[i].formeBatiment  = formeBatiment) and
				(table[i].nbNiveaux  = min(nbNiveaux, 2.5)) and
				(table[i].mitoyennete  = mitoyennete)
			 ) then
			begin
				result := true;
				A := table[i].A;
				B := table[i].B;
				exit;
			end;
		end;
	end;
end.
