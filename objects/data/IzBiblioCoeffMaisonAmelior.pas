unit IzBiblioCoeffMaisonAmelior;

interface

uses
	IzBiblioData, Classes, UtilitairesDialogie;

	type TBiblioCoeffMaisonAmeliorItem = record
		formeBatiment : TTypeFormeBatiment;
		idEnergie : TEnergieIdentificateur;
		anneeMin : integer;
		anneeMax : integer;
		ameliorMur : double;
		ameliorSol : double;
		ameliorToit : double;
		ameliorVit : double;
	end;

	type TBiblioCoeffMaisonAmelior = class(TBiblioData)
		private
			table : Array of TBiblioCoeffMaisonAmeliorItem;
		public
			constructor Create(aFileName : string); override;
			function getAmeliorations(formeBatiment : TTypeFormeBatiment ; idEnergie : TEnergieIdentificateur ; annee : integer ; var ameliorMur : double ; var ameliorSol : double ; var ameliorToit : double ; var ameliorVit : double) : boolean;
			function getAmelioration(formeBatiment : TTypeFormeBatiment ; idEnergie : TEnergieIdentificateur ; annee : integer ; typeAmelioration : TTypeAmelioration) : double;
	end;

implementation

uses
  System.SysUtils,
  IzUtilitaires;

	Constructor TBiblioCoeffMaisonAmelior.Create(aFileName : string);
	Var
	 Temp : TStringList;
	 i : Integer;
	 Ligne : String;
	Begin
		inherited create(aFileName);
		Temp := getStringList();
		SetLength(table, nb);
		For i := 0 to Temp.Count - 1 do
		Begin
			ligne := Temp[i];
			table[i].formeBatiment := TTypeFormeBatiment(getInt(getItemTab(ligne)));
			table[i].anneeMin := getInt(getItemTab(ligne));
			table[i].anneeMax := getInt(getItemTab(ligne));
			table[i].idEnergie := TEnergieIdentificateur(getInt(getItemTab(ligne)));
			table[i].ameliorMur := getDouble(getItemTab(ligne));
			table[i].ameliorSol := getDouble(getItemTab(ligne));
			table[i].ameliorToit := getDouble(getItemTab(ligne));
			table[i].ameliorVit := getDouble(getItemTab(ligne));
		End;
		Temp.free;
	End;


	function TBiblioCoeffMaisonAmelior.getAmeliorations(formeBatiment : TTypeFormeBatiment ; idEnergie : TEnergieIdentificateur ; annee : integer ; var ameliorMur : double ; var ameliorSol : double ; var ameliorToit : double ; var ameliorVit : double) : boolean;
	var
		i: integer;
	begin
		result := false;
		for i := 0 to nb - 1 do
		begin
			if (
				(table[i].formeBatiment  = formeBatiment) and
				( (table[i].idEnergie  = idEnergie) or  (table[i].idEnergie  = ENERGIE_INCONNUE)) and
				((table[i].anneeMin  <= annee) or (table[i].anneeMin = -1)) and
				((table[i].anneeMax = -1) or (table[i].anneeMax  >= annee))
			 ) then
			begin
				result := true;
				ameliorMur := table[i].ameliorMur;
				ameliorSol := table[i].ameliorSol;
				ameliorToit := table[i].ameliorToit;
				ameliorVit := table[i].ameliorVit;
				exit;
			end;
		end;
	end;

	function TBiblioCoeffMaisonAmelior.getAmelioration(formeBatiment : TTypeFormeBatiment ; idEnergie : TEnergieIdentificateur ; annee : integer ; typeAmelioration : TTypeAmelioration) : double;
	var
		aMur : double;
		aSol : double;
		aToit : double;
		aVit : double;
	begin
		result := -1;
		if not(getAmeliorations(formeBatiment, idEnergie, annee, aMur, aSol, aToit, aVit)) then
		begin
			exit;
		end;
		case typeAmelioration of
			AMELIORATION_MUR :
				result := aMur;
			AMELIORATION_SOL :
				result := aSol;
			AMELIORATION_TOIT :
				result := aToit;
			AMELIORATION_VIT :
      	result := aVit;
		end;
	end;

end.
