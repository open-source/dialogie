unit BiblioData;

interface

uses
	Classes;

	type TBiblioData = class
		protected
			fileName : string;
			function getStringList() : TStringList;
		public
			nb : integer;
			constructor Create(aFileName : string);virtual;
	end;


implementation

uses
	SysUtils;

	constructor TBiblioData.Create(aFileName: string);
	begin
		fileName := aFileName;
	end;

	function TBiblioData.getStringList() : TStringList;
	var
		temp : TStringList;
		fullPath : string;
	begin
		temp := TStringList.Create;
		fullPath := ExtractFilePath(ParamStr(0))+ fileName;
		temp.LoadFromfile(fullPath);
		nb := temp.count;
		getStringList := temp;
	end;


end.
