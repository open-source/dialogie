unit IzBiblioVentilation26;

interface

uses
	Classes, IzBiblioData;
	type
		TVentil26 = record
		  habitat,
      ventilation,
      typeLabel,
      installation,
      complement,
      sumEntreeAir,
      qVarepConv,
      rendementEchangeur,
      consoAux,
      source : string;
		end;

const NULL_VENTIL26 : TVentil26 = (
		  habitat : '';
      ventilation : '';
      typeLabel : '';
      installation : '';
      complement : '';
      sumEntreeAir : '';
      qVarepConv : '';
      rendementEchangeur : '';
      consoAux : '';
      source : '';
);

	type
		TPermeabilite26 = record
      habitat,
      fenetre,
      cheminee,
      permSous4Pa,
      source : string;
		end;

const NULL_PERMEABILITE26 : TPermeabilite26 = (
      habitat : '';
      fenetre : '';
      cheminee : '';
      permSous4Pa : '';
      source : '';
);

	type TBiblioVentil26 = class(TBiblioData)
		private
			table : Array of TVentil26;
		public
			constructor Create(aFileName : string);override;
      destructor Destroy();override;


			function Menu(habitat, ventilation, typeLabel, installation, complement : string; niveau : Integer) : TStringList;
			function getItem(habitat, ventilation, typeLabel, installation, complement : string) : TVentil26;
	end;

	type TBiblioPermeabilite26 = class(TBiblioData)
		private
			table : Array of TPermeabilite26;
		public
			constructor Create(aFileName : string);override;
      destructor Destroy();override;

			function Menu(habitat, fenetre, cheminee : string; niveau : integer) : TStringList;
			function getItem(habitat, fenetre, cheminee : string) : TPermeabilite26;
	end;



implementation

uses
	SysUtils, IzUtilitaires;


//**********************************************************
// TBiblioVentil26
//**********************************************************
constructor TBiblioVentil26.Create(aFileName : string);
var
  Temp : TStringList;
  i : Integer;
  Ligne : string;
begin
  inherited create(aFileName);
  Temp := getStringList();

  SetLength(Table,Nb);
  for i := 0 to Temp.Count - 1 do
  begin
    ligne := Temp[i];
    table[i].habitat := getItemTab(ligne);
    table[i].ventilation := getItemTab(ligne);
    table[i].typeLabel := getItemTab(ligne);
    table[i].installation := getItemTab(ligne);
    table[i].complement := getItemTab(ligne);
    table[i].sumEntreeAir := getItemTab(ligne);
    table[i].qVarepConv := getItemTab(ligne);
    table[i].rendementEchangeur := getItemTab(ligne);
    table[i].consoAux := getItemTab(ligne);
    table[i].source := getItemTab(ligne);
  end;
  Temp.free;

end;


destructor TBiblioVentil26.Destroy();
begin
  SetLength(table,0);
  inherited Destroy();
end;


function TBiblioVentil26.Menu(habitat, ventilation, typeLabel, installation, complement : string; niveau : Integer) : TStringList;
var
  i : Integer;
  Liste : TStringList;
begin
  Liste := TStringList.Create;
  liste.Duplicates := dupIgnore;

  for i := 0 to Nb - 1 do
  begin
    if (Niveau >= 0)  and (Table[i].habitat <> habitat) then
      continue;
    if (Niveau >= 1) and (Table[i].ventilation <> ventilation) then
      continue;
    if (Niveau >= 2) and (Table[i].typeLabel <> typeLabel) then
      continue;
    if (Niveau >= 3) and (Table[i].installation <> installation) then
      continue;
    if (Niveau >= 4) and (Table[i].complement <> complement) then
      continue;
    case niveau of
      -1 :
        addList(liste, table[i].habitat);
      0:
        addList(liste, table[i].ventilation);
      1:
        addList(liste, table[i].typeLabel);
      2:
        addList(liste, table[i].installation);
      3:
        addList(liste, table[i].complement);
    end;
  end;
  result := Liste;
end;


function TBiblioVentil26.getItem(habitat, ventilation, typeLabel, installation, complement : string) : TVentil26;
var
  i : Integer;
begin
  result := NULL_VENTIL26;
  for i := 0 to Nb - 1 do
  begin
    if (Table[i].habitat <> habitat) then
      continue;
    if (Table[i].ventilation <> ventilation) then
      continue;
    if (Table[i].typeLabel <> typeLabel) then
      continue;
    if (Table[i].installation <> installation) then
      continue;
    if (Table[i].complement <> complement) then
      continue;
    result := Table[i];
    exit;
   end;
end;


//**********************************************************
// TBiblioPermeabilite26
//**********************************************************
constructor TBiblioPermeabilite26.Create(aFileName : string);
var
  Temp : TStringList;
  i : Integer;
  Ligne : string;
begin
  inherited create(aFileName);
  Temp := getStringList();

  SetLength(Table,Nb);
  for i := 0 to Temp.Count - 1 do
  begin
    ligne := Temp[i];
    table[i].habitat := getItemTab(ligne);
    table[i].fenetre := getItemTab(ligne);
    table[i].cheminee := getItemTab(ligne);
    table[i].permSous4Pa := getItemTab(ligne);
    table[i].source := getItemTab(ligne);
  end;
  Temp.free;

end;


destructor TBiblioPermeabilite26.Destroy();
begin
  SetLength(table,0);
  inherited Destroy();
end;


function TBiblioPermeabilite26.Menu(habitat, fenetre, cheminee : string; niveau : Integer) : TStringList;
var
  i : Integer;
  Liste : TStringList;
begin
  Liste := TStringList.Create;
  liste.Duplicates := dupIgnore;

  for i := 0 to Nb - 1 do
  begin
    if (Niveau >= 0)  and (Table[i].habitat <> habitat) then
      continue;
    if (Niveau >= 1) and (Table[i].fenetre <> fenetre) then
      continue;
    if (Niveau >= 2) and (Table[i].cheminee <> cheminee) then
      continue;
    case niveau of
      -1 :
        addList(liste, table[i].habitat);
      0:
        addList(liste, table[i].fenetre);
      1:
        addList(liste, table[i].cheminee);
    end;
  end;
  result := Liste;
end;


function TBiblioPermeabilite26.getItem(habitat, fenetre, cheminee : string) : TPermeabilite26;
var
  i : Integer;
begin
  result := NULL_PERMEABILITE26;
  for i := 0 to Nb - 1 do
  begin
    if (Table[i].habitat <> habitat) then
      continue;
    if (Table[i].fenetre <> fenetre) then
      continue;
    if (Table[i].cheminee <> cheminee) then
      continue;
    result := Table[i];
    exit;
   end;
end;


end.
