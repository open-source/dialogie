unit MenuMenager;

interface

uses
	IzBiblioData;

	type
		TBiblioMenagerItem = record
			typeAppareil,
			modele,
			veille,
			consoMarche,
			consoVeille,
			nbHeureMarcheJour,
			nbHeureVeilleJour,
			nbJourUtilAnnee,
			consoTotale,
			consoTotaleMarche,
			consoTotaleVeille : string;
		end;

	type TBiblioMenager = class(TBiblioData)
		public
			table : Array of TBiblioMenagerItem;
			constructor Create(aFileName : string); override;

	end;

implementation

uses
	Classes, IzUtilitaires;

	Constructor TBiblioMenager.Create(aFileName : string);
	Var
	 Temp : TStringList;
	 i : Integer;
	 Ligne : String;
   s : string;
	Begin
		inherited create(aFileName);
		Temp := getStringList();
		SetLength(table, nb+1);
		For i := 1 to Temp.Count do
		Begin
			ligne := Temp[i-1];
			table[i].typeAppareil := getItemTab(ligne);
			table[i].modele := getItemTab(ligne);
			table[i].veille := getItemTab(ligne);
      s := getItemTab(ligne);
			table[i].consoMarche := getItemSep(s, ' ');
      s := getItemTab(ligne);
			table[i].consoVeille := getItemSep(s, ' ');
			table[i].nbHeureMarcheJour := getItemTab(ligne);
			table[i].nbHeureVeilleJour := getItemTab(ligne);
			table[i].nbJourUtilAnnee := getItemTab(ligne);
			table[i].consoTotale := getItemTab(ligne);
			table[i].consoTotaleMarche := getItemTab(ligne);
			table[i].consoTotaleVeille := getItemTab(ligne);
		End;
		Temp.free;
	End;


end.
