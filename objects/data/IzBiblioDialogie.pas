unit IzBiblioDialogie;

interface

uses
	IzBiblioSystem,
  MenuBureau,
  MenuFroid,
  MenuLavage,
  MenuMenager,
	IzBiblioCoeffG,
  IzBiblioCuisine,
  IzBiblioEclairage,
  IzBiblioCorrectifZone,
  IzBiblioCoeffMaisonAb,
  IzBiblioCoeffMaisonAmelior,
  IzBiblioCoeffMaisonAmeliorTravaux,
  IzBiblioDepartement,
  IzBiblioCollectifPosition,
  IzBiblioTau,
  IzBiblioVentilation26;


	type TBiblioFileNamesDialogie = record

		biblio : TBiblioFileNames;

		bureau,
		cuisine,
		froid,
		lavage,
		menager,
		coeffG,
		eclairage,
		corrZone1,
		corrZoneA1,
		corrZone2,
		corrZoneA2,
		coeffMaisonAB,
		coeffMaisonAmelior,
		coeffMaisonAmeliorTravaux,
		departement,
		collectifPosition,
		tau,
    ventil26,
    permeabilite26
			: string;
	end;



	type TBiblioDialogie = class(TBiblioSystem)
    protected
	    class function getDefaultFileNames():TBiblioFileNamesDialogie;
		public
			biblioBureau : TBiblioBureau;
			biblioCuisine : TBiblioCuisine;
			biblioFroid : TBiblioFroid;
			biblioLavage : TBiblioLavage;
			biblioMenager : TBiblioMenager;
			biblioEclairage : TBiblioEclairage;

			biblioCoeffG : TBiblioCoeffG;

			biblioCorrZone1 : TBiblioCorrectifZone;
			biblioCorrZoneA1 : TBiblioCorrectifZone;

			biblioCoeffMaisonAB : TBiblioCoeffMaisonAb;
			biblioCoeffMaisonAmelior : TBiblioCoeffMaisonAmelior;
			biblioCoeffMaisonAmeliorTravaux : TBiblioCoeffMaisonAmeliorTravaux;

			biblioDepartement : TBiblioDepartement;

			biblioCollectifPosition : TBiblioCollectifPosition;

			biblioTau : TBiblioTau;

      biblioVentil26 : TBiblioVentil26;
      biblioPerm26 : TBiblioPermeabilite26;

			constructor Create(fileNames : TBiblioFileNamesDialogie);overload;
			destructor Destroy();override;

	    class function getLib() : TBiblioDialogie;
	    class procedure initDefault();
	end;






implementation

uses
	UtilitairesDialogie, System.SysUtils;

	constructor TBiblioDialogie.Create(fileNames : TBiblioFileNamesDialogie);
	var
		biblioCorrZone2 : TBiblioCorrectifZoneRecent;
		biblioCorrZoneA2 : TBiblioCorrectifZoneRecent;

	begin
		inherited create(fileNames.biblio);
		biblioBureau := TBiblioBureau.Create(fileNames.bureau);
		biblioCuisine := TBiblioCuisine.Create(fileNames.cuisine);
		biblioFroid := TBiblioFroid.Create(fileNames.froid);
		biblioLavage := TBiblioLavage.Create(fileNames.lavage);
		biblioMenager := TBiblioMenager.Create(fileNames.menager);
		biblioEclairage := TBiblioEclairage.Create(fileNames.eclairage);

		biblioCoeffG := TBiblioCoeffG.Create(fileNames.coeffG);

		biblioCorrZone2 := TBiblioCorrectifZoneRecent.create(fileNames.corrZone2);
		biblioCorrZoneA2 := TBiblioCorrectifZoneRecent.create(fileNames.corrZoneA2);

		biblioCorrZone1 := TBiblioCorrectifZone.Create(fileNames.corrZone1, biblioCorrZone2);
		biblioCorrZoneA1 := TBiblioCorrectifZone.Create(fileNames.corrZoneA1, biblioCorrZoneA2);

		biblioCoeffMaisonAB := TBiblioCoeffMaisonAb.Create(fileNames.coeffMaisonAB);
		biblioCoeffMaisonAmelior := TBiblioCoeffMaisonAmelior.Create(fileNames.coeffMaisonAmelior);
		biblioCoeffMaisonAmeliorTravaux := TBiblioCoeffMaisonAmeliorTravaux.Create(fileNames.coeffMaisonAmeliorTravaux);

		biblioCollectifPosition := TBiblioCollectifPosition.Create(fileNames.collectifPosition);

		biblioDepartement := TBiblioDepartement.Create(fileNames.departement);

		biblioTau := TBiblioTau.Create(fileNames.tau);

    biblioVentil26 := TBiblioVentil26.Create(fileNames.ventil26);
    biblioPerm26 := TBiblioPermeabilite26.Create(fileNames.permeabilite26);

	end;

destructor TBiblioDialogie.Destroy();
begin
  freeAndNil(biblioBureau);
  freeAndNil(biblioCuisine);
  freeAndNil(biblioFroid);
  freeAndNil(biblioLavage);
  freeAndNil(biblioMenager);
  freeAndNil(biblioEclairage);
  freeAndNil(biblioCoeffG);
  freeAndNil(biblioCorrZone1);
  freeAndNil(biblioCorrZoneA1);
  freeAndNil(biblioCoeffMaisonAB);
  freeAndNil(biblioCoeffMaisonAmelior);
  freeAndNil(biblioCoeffMaisonAmeliorTravaux);
  freeAndNil(biblioDepartement);
  freeAndNil(biblioCollectifPosition);
  freeAndNil(biblioTau);
  freeAndNil(biblioVentil26);
  freeAndNil(biblioPerm26);
  inherited Destroy();
end;





	class function TBiblioDialogie.getLib() : TBiblioDialogie;
	begin
		if (biblioSystem = nil) then
		begin
			initDefault();
		end;
		result := TBiblioDIalogie(biblioSystem);
	end;

	class procedure TBiblioDialogie.initDefault();
	var
		fileNames : TBiblioFileNamesDialogie;
	begin
		fileNames := getDefaultFileNames();
		if biblioSystem <> nil then
			TBiblioDIalogie(biblioSystem).Free;

		biblioSystem := TBiblioDialogie.Create(fileNames);

	end;

	class function TBiblioDialogie.getDefaultFileNames():TBiblioFileNamesDialogie;
	var
		fileNames : TBiblioFileNamesDialogie;
	begin

//on garde pour remettre le jour o� tout est conforme
//		fileNames.biblio := getDefaultBiblioFileNames(aBiblioType);
//en attendant, on replace par
		//la partie commune
//		fileNames.biblio.chauffageMaison := dialogieLibPath + 'Chauffage Maison.txt';
		fileNames.biblio.chauffageMaison := dialogieLibPath + 'indiv-chauff.txt';
//		fileNames.biblio.chauffageAppart := dialogieLibPath + 'Chauffage Appart.txt';
		fileNames.biblio.chauffageAppart := dialogieLibPath + 'collectif-chauff.txt';
		fileNames.biblio.ecsMaison := dialogieLibPath + 'ECS Maison.txt';
		fileNames.biblio.ecsAppart := dialogieLibPath + 'ECS Appart.txt';

		//attention : vient du DPE!!!
    fileNames.biblio.energieRepartition := dialogieLibPath + 'Repartition.txt';
    fileNames.biblio.energieTarif := dialogieLibPath + 'EnergieBase.txt';
		//fileNames.biblio.climatisation := dialogieLibPath + 'Climatisation.txt';
		fileNames.biblio.climatisation := dialogieLibPath + 'clim.xml';

		fileNames.bureau := dialogieLibPath + 'bureautique.txt';
		fileNames.froid := dialogieLibPath + 'froid.txt';
		fileNames.lavage := dialogieLibPath + 'lavage.txt';
		fileNames.menager := dialogieLibPath + 'electro.txt';
		fileNames.cuisine := dialogieLibPath + 'Cuisine.txt';
		fileNames.eclairage := dialogieLibPath + 'eclairage.txt';

		fileNames.coeffG := dialogieLibPath + 'coeff-G-batiment.txt';
		fileNames.corrZone1 := dialogieLibPath + 'CorrZone1.txt';
		fileNames.corrZoneA1 := dialogieLibPath + 'CorrZoneA1.txt';

		fileNames.corrZone2 := dialogieLibPath + 'CorrZone2.txt';
		fileNames.corrZoneA2 := dialogieLibPath + 'CorrZoneA2.txt';

		fileNames.coeffMaisonAB := dialogieLibPath + 'coeff-maison-AB.txt';
		fileNames.coeffMaisonAmelior := dialogieLibPath + 'coeff-maison-amelior.txt';
		fileNames.coeffMaisonAmeliorTravaux := dialogieLibPath + 'coeff-maison-amelior-travaux.txt';

		fileNames.collectifPosition := dialogieLibPath + 'collectif-position.txt';

		fileNames.departement := dialogieLibPath + 'Fichiers Meteo\departements.txt';

		fileNames.tau := dialogieLibPath + 'tau.txt';
		fileNames.ventil26 := dialogieLibPath + 'ventil26.txt';
		fileNames.permeabilite26 := dialogieLibPath + 'permeabilite26.txt';

		result := fileNames;
	end;


end.
