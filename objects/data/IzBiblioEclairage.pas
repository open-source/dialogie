unit IzBiblioEclairage;

interface

uses
	IzBiblioData, Classes;

  type TBiblioEclairageItem = record
		libelle : string;
		modele : string;
		puissance : double;
		nbHeureJour : double;
		nbJourAn : double;
	end;

	type TBiblioEclairage = class(TBiblioData)
		private
			table : Array of TBiblioEclairageItem;
			function getLibelle(item : TBiblioEclairageItem) : string;

		public
			constructor Create(aFileName : string); override;
			function getLibelles() : TStrings;
			function getEclairage(libelle : string) : TBiblioEclairageItem;
	end;


implementation

	uses IzUtilitaires;

(*
pour BiblioEclairage
*)
	Constructor TBiblioEclairage.Create(aFileName : string);
	Var
	 Temp : TStringList;
	 i : Integer;
	 Ligne : String;
	 chaine : string;
	 err : integer;
	Begin
		inherited create(aFileName);
		Temp := getStringList();
		SetLength(table, nb);
		For i := 0 to Temp.Count - 1 do
		Begin
			ligne := Temp[i];

			table[i].libelle := getItemTab(ligne);
			table[i].modele := getItemTab(ligne);

			chaine := getItemTab(ligne);//on va transformer '100 W' en '100'
      chaine := getItemSep(chaine, ' ');


			val(chaine, table[i].puissance, err);
			chaine := getItemTab(ligne);
			val(chaine, table[i].nbHeureJour, err);
			chaine := getItemTab(ligne);
			val(chaine, table[i].nbJourAn, err);
		End;
		Temp.free;
	End;

	function TBiblioEclairage.getLibelles() : TStrings;
	var
		i: Integer;
		libelles : TStringList;
	begin
		libelles := createComboList;
		for i := 0 to nb - 1 do
		begin
			libelles.Add(getLibelle(table[i]));
		end;
		result := libelles;
	end;


	function TBiblioEclairage.getEclairage(libelle : string) : TBiblioEclairageItem;
	var
		i: Integer;
		s : string;
	begin
		for i := 0 to nb - 1 do
		begin
			s := getLibelle(table[i]);
			if (s = libelle) then
			begin
				result := table[i];
				exit;
			end;
		end;
	end;

	function TBiblioEclairage.getLibelle(item : TBiblioEclairageItem) : string;
	begin
		result := item.libelle + ' / ' + item.modele + ' / ' + getString(round(item.puissance)) + ' W';
	end;

end.

