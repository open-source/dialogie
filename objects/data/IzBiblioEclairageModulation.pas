unit IzBiblioEclairageModulation;

interface

uses
	IzBiblioData, Classes;



	type TBiblioEclairageModulation = class(TBiblioData)
		private
			table : Array of double;
		public
			constructor Create(aFileName : string); override;
			function getModulation(rank : integer) : double;
	end;




	function getBiblioEclairageModulation() : TBiblioEclairageModulation;


	var
		biblioEclairageModulation : TBiblioEclairageModulation;



implementation

	uses IzUtilitaires, UtilitairesDialogie;

	Constructor TBiblioEclairageModulation.Create(aFileName : string);
	Var
	 Temp : TStringList;
	 i : Integer;
   s : string;
	Begin
		inherited create(aFileName);
		Temp := getStringList();
		SetLength(table, nb);
		For i := 0 to Temp.Count - 1 do
		Begin
      s := Temp[i];
			table[i] := getDouble(getItemTab(s));
		End;
		Temp.free;
	End;


	function TBiblioEclairageModulation.getModulation(rank : integer) : double;
	begin

  	//on fait commencer � 1 et on fait correspondre � 0

		if (rank < 1) or (rank > nb) then
		begin
			result := 0;
		end
		else
			result := table[rank - 1];
	end;


	function getBiblioEclairageModulation() : TBiblioEclairageModulation;
	begin
		if biblioEclairageModulation = nil then
		begin
			biblioEclairageModulation := TBiblioEclairageModulation.Create(dialogieLibPath + 'modulationEclairage.txt');
		end;

		result := biblioEclairageModulation;
  end;



	initialization
		biblioEclairageModulation := nil;

	finalization
		if (biblioEclairageModulation <> nil) then
		begin
			biblioEclairageModulation.free;
			biblioEclairageModulation := nil;
		end;
end.

