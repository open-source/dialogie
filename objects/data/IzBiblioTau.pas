unit IzBiblioTau;

interface

uses
	Classes, IzBiblioData;

	type TBiblioTauItem = record
		idCategorie : string;
		libelleCategorie : string;
		val3 : string;
		libelle : string;
		val5 : string;
		val6 : string;
		tau : double;
	end;


	type TBiblioTau = class(TBiblioData)
		private
			table : Array of TBiblioTauItem;
		public
			constructor Create(aFileName : string); override;

			function getCatLibelles() : TStrings;
			function getCatIds() : TStrings;
			function getCatIdFromLibelle(libelleCategorie : string) : string;
			function getCatLibelleFromId(idCategorie : string) : string;
			function getLibellesFromCat(libelleCategorie : string) : TStrings;

			function getItem(libelleCategorie : string ; libelle : string) : TBiblioTauItem;
			function getTau(libelleCategorie : string ; libelle : string) : double;inline;
	end;

implementation
	uses IzUtilitaires, SysUtils;

	constructor TBiblioTau.Create(aFileName : string);
	Var
	 Temp : TStringList;
	 i : Integer;
	 Ligne : String;
	 chaine : string;
	 idCategorie : string;
   libCategorie : string;
   s : string;
   dbl : double;
	Begin
		inherited create(aFileName);
		Temp := getStringList();
		SetLength(table, nb);
		innerNb := 0;
		For i := 0 to Temp.Count - 1 do
		Begin
			ligne := Temp[i];

			chaine := getItemTab(ligne);
			if chaine <> '' then
			begin
				//on est en train de lire une cat�gorie
				idCategorie := chaine;
				libCategorie := getItemTab(ligne);
			end
			else
			begin
				//on est entrain de lire un d�tail
				table[innerNb].idCategorie := idCategorie;
				getItemTab(ligne);//ne sert � rien ici
				table[innerNb].libelleCategorie := libCategorie;
				table[innerNb].val3 := getItemTab(ligne);
				table[innerNb].libelle := getItemTab(ligne);
				table[innerNb].val5 := getItemTab(ligne);
				table[innerNb].val6 := getItemTab(ligne);
        s := getItemTab(ligne);
        if s = '' then
          dbl := -1
        else
          dbl := getDouble(s);
				table[innerNb].tau := dbl;
				//ici on incr�mente le nombre d'�l�ments seulement quand on en rencontre un
				inc(innerNb);
			end;
		End;
		Temp.free;
	End;


	function TBiblioTau.getCatLibelles() : TStrings;
	var
		i: Integer;
		libelles : TStringList;
	begin
		libelles := createComboList(false);
		for i := 0 to nb - 1 do
		begin
      if libelles.IndexOf(table[i].libelleCategorie) = -1 then
  			libelles.Add(table[i].libelleCategorie);
		end;
		result := libelles;
	end;

	function TBiblioTau.getCatIds() : TStrings;
	var
		i: Integer;
		liste : TStringList;
	begin
		liste := createComboList(false);
		for i := 0 to nb - 1 do
		begin
      if liste.IndexOf(table[i].idCategorie) = -1 then
  			liste.Add(table[i].idCategorie);
		end;
		result := liste;
	end;


	function TBiblioTau.getLibellesFromCat(libelleCategorie : string) : TStrings;
	var
		i: Integer;
		libelles : TStringList;
	begin
		libelles := createComboList(false);
		for i := 0 to nb - 1 do
		begin
			if libelleCategorie = table[i].libelleCategorie then
				libelles.Add(table[i].libelle);
		end;
		result := libelles;
	end;


	function TBiblioTau.getCatIdFromLibelle(libelleCategorie : string) : string;
	Var
		i : Integer;
	Begin
		result := '';
		For i := 0 to nb - 1 do
		Begin
			if (table[i].libelleCategorie = libelleCategorie)  then
			begin
				result := table[i].idCategorie;
				exit;
			end;
		End;
	End;


	function TBiblioTau.getCatLibelleFromId(idCategorie : string) : string;
	Var
		i : Integer;
	Begin
		result := '';
		For i := 0 to nb - 1 do
		Begin
			if (table[i].idCategorie = idCategorie)  then
			begin
				result := table[i].libelleCategorie;
				exit;
			end;
		End;
	End;


	function TBiblioTau.getItem(libelleCategorie : string ; libelle : string) : TBiblioTauItem;
	Var
		i : Integer;
		item : TBiblioTauItem;
	Begin

		result := table[0];
		For i := 0 to nb - 1 do
		Begin
			item := table[i];
			if (item.libelleCategorie = libelleCategorie) and (item.libelle = libelle) then
			begin
				result := item;
				exit;
			end;
		End;
	End;


	function TBiblioTau.getTau(libelleCategorie : string ; libelle : string) : double;
	Var
		i : Integer;
		item : TBiblioTauItem;
	Begin
			result := -1;// ce qui est une h�r�sie
		if (libelleCategorie = '') or (libelle = '') then
		begin
			exit;
		end;
		For i := 0 to nb - 1 do
		Begin
			item := table[i];
			if (item.libelleCategorie = libelleCategorie) and (item.libelle = libelle) then
			begin
				result := item.tau;
				exit;
			end;
		End;
	End;

end.
