unit IzBiblioCoeffG;

interface

uses
  IzBiblioData, Classes, IzUtilitaires;

  type TBiblioCoeffGItem = record
    typeBatiment : TTypeBatiment;
		libelle : string;
    renovationFaible : boolean;
		coeffG : array[0..2] of double;
	end;

	type TBiblioCoeffG = class(TBiblioData)
		private
			table : Array of TBiblioCoeffGItem;
		public
			constructor Create(aFileName : string); override;
			function getLibelles() : TStrings;overload;
			function getLibelles(typeBatiment : TTypeBatiment) : TStrings;overload;
			function getCoeffG(typeBatiment : TTypeBatiment ; libelle : string ; reno : boolean ; zoneClim : integer) : double;
	end;

implementation

uses
  sysutils;

	Constructor TBiblioCoeffG.Create(aFileName : string);
	Var
	 Temp : TStringList;
	 i : Integer;
   cpt : integer;
	 Ligne : String;
	 chaine : string;
	Begin
		inherited create(aFileName);
		Temp := getStringList();
		SetLength(table, nb);
		For i := 0 to Temp.Count - 1 do
		Begin
			ligne := Temp[i];
      chaine := lowercase(getItemTab(ligne));
      if (chaine = 'maison') or (chaine = 'individuel') then
      begin
        table[i].typeBatiment := BATIMENT_Maison;
      end
      else
      begin
        table[i].typeBatiment := BATIMENT_Appartement;
      end;
			table[i].libelle := getItemTab(ligne);
      chaine := lowercase(getItemTab(ligne));
      chaine := getItemSep(chaine, ' ');
			table[i].renovationFaible := (chaine = 'reno') or (chaine = 'r�no') or (chaine = 'renovation') or (chaine = 'r�novation');
      for cpt := 0 to 2 do
      begin
  			table[i].coeffG[cpt] := getDouble(getItemTab(ligne));
      end;
		End;
		Temp.free;
	End;

	function TBiblioCoeffG.getLibelles() : TStrings;
	var
		i: Integer;
		libelles : TStringList;
	begin
		libelles := TStringList.Create;
		for i := 0 to nb - 1 do
		begin
			libelles.Add(table[i].libelle);
		end;
		result := libelles;
	end;


	function TBiblioCoeffG.getLibelles(typeBatiment : TTypeBatiment) : TStrings;
	var
		i: Integer;
		libelles : TStringList;
    item : TBiblioCoeffGItem;
	begin
		libelles := TStringList.Create;
		for i := 0 to nb - 1 do
		begin
      item := table[i];
			if (typeBatiment = item.typeBatiment) and (item.renovationFaible) then
				libelles.Add(table[i].libelle);
		end;
		result := libelles;
	end;



	function TBiblioCoeffG.getCoeffG(typeBatiment : TTypeBatiment ; libelle : string ; reno : boolean ; zoneClim : integer) : double;
	var
		i: integer;
    item : TBiblioCoeffGItem;
	begin
		for i := 0 to nb - 1 do
		begin
      item := table[i];
      if item.typeBatiment <> typeBatiment then
        continue;
      if item.renovationFaible <> reno then
        continue;
			if item.libelle <> libelle then
        continue;
  		result := item.coeffG[zoneClim];
      exit;
		end;
    result := -1;
	end;
end.
