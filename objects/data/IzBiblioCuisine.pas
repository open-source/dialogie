unit IzBiblioCuisine;

interface

uses
  IzBiblioData, Classes;

  type TBiblioCuisineItem = record
		libelle : string;
		energie : string;
		consommation : double;
	end;

	type TBiblioCuisine = class(TBiblioData)
		private
			table : Array of TBiblioCuisineItem;
		public
			constructor Create(aFileName : string); override;
			function getLibelles() : TStrings;
			function getEnergies(libelle : string) : TStrings;
			function getConsommation(libelle : string; energie : string) : double;
	end;

implementation

uses
  System.SysUtils,
  IzUtilitaires;

	Constructor TBiblioCuisine.Create(aFileName : string);
	Var
	 Temp : TStringList;
	 i : Integer;
	 Ligne : String;
	Begin
		inherited create(aFileName);
		Temp := getStringList();
		SetLength(table, nb);
		For i := 0 to Temp.Count - 1 do
		Begin
			ligne := Temp[i];

			table[i].energie := getItemTab(ligne);
			table[i].libelle := getItemTab(ligne);
			//deux infos non renseign�es
			getItemTab(ligne);
			getItemTab(ligne);
			table[i].consommation := getDouble(getItemTab(ligne));
		End;
		Temp.free;
	End;

	function TBiblioCuisine.getLibelles() : TStrings;
	var
		i: Integer;
		libelles : TStringList;
	begin
		libelles := createComboList;
		for i := 0 to nb - 1 do
		begin
			libelles.Add(table[i].libelle);
		end;
		result := libelles;
	end;

	function TBiblioCuisine.getEnergies(libelle : string) : TStrings;
	var
		i: Integer;
		energies : TStringList;
	begin
		energies := createComboList;
		for i := 0 to nb - 1 do
		begin
			if (table[i].libelle = libelle) then
				energies.Add(table[i].energie);
		end;
		result := energies;
	end;


	function TBiblioCuisine.getConsommation(libelle : string; energie : string) : double;
	var
		i: integer;
	begin
		result := 2;//un r�sultat max si pas trouv�
		for i := 0 to nb - 1 do
		begin
			if table[i].libelle  = libelle then
			begin
				result := table[i].consommation;
        exit;
      end;
		end;
	end;
end.

