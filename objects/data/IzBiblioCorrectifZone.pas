unit IzBiblioCorrectifZone;

interface

uses
  IzBiblioData, Classes, UtilitairesDialogie;

	type TBiblioCorrectifZoneRecentItem = record
		zone0Elec : double;
		zone1Elec : double;
		zone2Elec : double;
		zone0Autre : double;
		zone1Autre : double;
		zone2Autre : double;
	end;

	type TBiblioCorrectifZoneItem = record
		departement : string;
		av1979Elec : double;
		av1979Autre : double;
		depuis1979Elec : double;
		depuis1979Autre : double;
	end;


	type TBiblioCorrectifZoneRecent = class(TBiblioData)
		private
			table : Array of TBiblioCorrectifZoneRecentItem;
		public
			constructor Create(aFileName : string); override;
			function getItem(rank : integer) : TBiblioCorrectifZoneRecentItem;

			function getCorZone(zoneClimatique : integer ; anneeConstruction : integer; idEnergie : TEnergieIdentificateur) : double;

	end;


	type TBiblioCorrectifZone = class(TBiblioData)
		private
			table : Array of TBiblioCorrectifZoneItem;
			innerBiblio : TBiblioCorrectifZoneRecent;
		public
			constructor Create(aFileName : string); reintroduce;overload;
			constructor Create(aFileName : string ; biblio : TBiblioCorrectifZoneRecent); reintroduce;overload;

			destructor Destroy();override;

			function getNumeros() : TStrings;

			function getItem(numDepartement : string) : TBiblioCorrectifZoneItem;

			function getCorZone(numDepartement : string ; zoneClimatique : integer; anneeConstruction :integer ; idEnergie : TEnergieIdentificateur) : double;

	end;


implementation

uses
  System.SysUtils,
  IzUtilitaires;




(*
class TBiblioCorrectifZoneRecent
*)
	constructor TBiblioCorrectifZoneRecent.Create(aFileName : string);
	Var
	 Temp : TStringList;
	 i : Integer;
	 Ligne : String;
	Begin
		inherited create(aFileName);
		Temp := getStringList();
		SetLength(table, nb);
		For i := 0 to Temp.Count - 1 do
		Begin
			ligne := Temp[i];

			table[i].zone0Elec := getDouble(getItemTab(ligne));
			table[i].zone1Elec := getDouble(getItemTab(ligne));
			table[i].zone2Elec := getDouble(getItemTab(ligne));
			table[i].zone0Autre := getDouble(getItemTab(ligne));
			table[i].zone1Autre := getDouble(getItemTab(ligne));
			table[i].zone2Autre := getDouble(getItemTab(ligne));
		End;
		Temp.free;
	End;

	function TBiblioCorrectifZoneRecent.getItem(rank : integer) : TBiblioCorrectifZoneRecentItem;
	begin
		result := table[rank];
	end;

	function TBiblioCorrectifZoneRecent.getCorZone(zoneClimatique : integer ; anneeConstruction : integer; idEnergie : TEnergieIdentificateur) : double;
	var
		item : TBiblioCorrectifZoneRecentItem;
	Begin
    result := 1;
		Case anneeConstruction Of
			1983..1988 :
				item := getItem(0);
			1989..2001 :
				item := getItem(1);
			else
				item := getItem(2);
		End;
		if idEnergie = ENERGIE_ELECTRICITE then
		begin
			case zoneClimatique of
				0 :
					result := item.zone0Elec;
				1 :
					result := item.zone1Elec;
				2 :
					result := item.zone2Elec;
			end;
		end
		else
		begin
			case zoneClimatique of
				0 :
					result := item.zone0Autre;
				1 :
					result := item.zone1Autre;
				2 :
					result := item.zone2Autre;
			end;
		end;
	End;



(*
class TBiblioCorrectifZone
*)

	Constructor TBiblioCorrectifZone.Create(aFileName : string ; biblio : TBiblioCorrectifZoneRecent);
	begin
		if(biblio <> nil) then
		begin
			innerBiblio := biblio;
		end;
		create(aFileName);
	end;


	constructor TBiblioCorrectifZone.Create(aFileName : string);
	Var
	 Temp : TStringList;
	 i : Integer;
	 Ligne : String;
	Begin
		inherited create(aFileName);
		Temp := getStringList();
		SetLength(table, nb);
		For i := 0 to Temp.Count - 1 do
		Begin
			ligne := Temp[i];

			table[i].departement := getItemTab(ligne);
			table[i].av1979Elec := getDouble(getItemTab(ligne));
			table[i].av1979Autre := getDouble(getItemTab(ligne));
			table[i].depuis1979Elec := getDouble(getItemTab(ligne));
			table[i].depuis1979Autre := getDouble(getItemTab(ligne));
		End;
		Temp.free;
	End;


	destructor TBiblioCorrectifZone.Destroy();
	begin
		if innerBiblio <> nil then
		begin
      innerBiblio.Free;
    end;
	end;


	function TBiblioCorrectifZone.getNumeros() : TStrings;
	var
		i: Integer;
		libelles : TStringList;
	begin
		libelles := createComboList;
		for i := 0 to nb - 1 do
		begin
			libelles.Add(table[i].departement);
		end;
		result := libelles;
	end;


	function TBiblioCorrectifZone.getItem(numDepartement : string) : TBiblioCorrectifZoneItem;
	Var
	 i : Integer;
	 item : TBiblioCorrectifZoneItem;
	Begin

		result := table[0];
		For i := 0 to nb - 1 do
		Begin
			item := table[i];
			if (item.departement = numDepartement) then
			begin
				result := item;
				exit;
			end;
		End;
	End;

	function TBiblioCorrectifZone.getCorZone(numDepartement : string ; zoneClimatique : integer; anneeConstruction :integer ; idEnergie : TEnergieIdentificateur) : double;
	var
		item : TBiblioCorrectifZoneItem;
	begin
		item := getItem(numDepartement);
			result := 1;

			case anneeConstruction of
				1979..1982:
				begin
					if idEnergie = ENERGIE_ELECTRICITE then
					begin
						result := item.depuis1979Elec;
					end
					else
					begin
						result := item.depuis1979Autre;
					end;
				end;
				1983..3000:
				begin
					result := innerBiblio.getCorZone(zoneClimatique, anneeConstruction, idEnergie);
				end;
				else //1974..1978:
				begin
					if idEnergie = ENERGIE_ELECTRICITE then
					begin
						result := item.av1979Elec;
					end
					else
					begin
						result := item.av1979Autre;
					end;
				end;
			end;

(*
			if anneeConstruction < 1974 then
				exit
			else
			if (anneeConstruction >= 1974) and (anneeConstruction <= 1982) then
			Begin

				if anneeConstruction  <= 1978 then
				Begin
					if idEnergie = ENERGIE_ELECTRICITE then
					begin
						result := item.av1979Elec;
						exit;
					end
					else
					begin
						result := item.av1979Autre;
						exit;
					end;
				End
				else
				Begin
					if idEnergie = ENERGIE_ELECTRICITE then
					begin
						result := item.depuis1979Elec;
						exit;
					end
					else
					begin
						result := item.depuis1979Autre;
						exit;
					end;
				End;
			End
			else// annee construction > 1982
			Begin
				result := innerBiblio.getCorZone(zoneClimatique, anneeConstruction, idEnergie);
			End;
*)
	end;

end.
