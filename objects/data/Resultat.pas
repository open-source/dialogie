unit Resultat;

interface

uses
	utilitairesDialogie, Classes, IzObjectPersist, IzDeperdition,
  IzConstantesConso, Xml.XMLIntf;

const COEFF_EP_ELEC : double = 2.58;



type TDiaGlobalResultData = class
  public
    besoinAnnuel_m2 : double;
    consoAnnuel_m2 : double;

    besoinAnnuel : double;
    consoAnnuel : double;

    consoPrimaireAnnuel : double;
    emissionCO2Annuel : double;
    nucleaire : double;
    prixAnnuel : double;

    Constructor Create;reintroduce;
    destructor Destroy;override;

    procedure reset();
    procedure assign(src_ : TDiaGlobalResultData);

    procedure load(node : IXMLNode);
    procedure save(node : IXMLNode);
end;


type TResultat = Class(TObjectPersist)
  private
	protected
    procedure innerLoadList(liste : TStringList);override;
    procedure innerSaveList(liste : TStringList);override;

    procedure innerLoadXml(node : IXMLNode); override;
    procedure innerSaveXml(node : IXMLNode); override;

    function getConsoEP : double;
    function getConsoElecSpec: double;
	public
    IECS : double;
    ICH : double;

    consoLavageAction : double;
    consoFroidAction : double;
    consoBureautiqueAction : double;
    consoElectroAction : double;
    consoEclairageAction : double;

//			GVentilation : double;
    Emissions : double;
    TEP : double;

    TablekWh : TTableResult;

    TableEuro : TTableResult;

    TableCO2 : TTableResult;

    tabNucleaire : TTableResult;
    DechetsNucleaires : double;

    tabBECS : TValeursMensuelles;

    TabECSSolaire : TValeursMensuelles;

    tabCECS : TMensuel;
    CECS : double;

    TabCECSAccu : TMensuel;

    tabBesoinsBruts : TValeursMensuelles;

    tabApportsInternes : TValeursMensuelles;

    tabApportsSolaires : TValeursMensuelles;

    tabTauxDeRecuperation : TValeursMensuelles;

    TableRessources : TRessources;

    TableEnvironnement : TEnvironnement;

    tabConsoChauff : TMensuel;
    ConsoChauff : double;

    clim : TDiaGlobalResultData;

    TabConsoChauffAccu : TMensuel;

    TabBesoinsNets : TValeursMensuelles;

    tabConsoAuxChauff : TValeursMensuelles;

    tabConsoAuxVentil: TValeursMensuelles;

    tabConsoCuisine : TMensuel;
    consoCuisine : double;
    tabConsoCuisineApport : TValeursMensuelles;
    tabConsoCuisineHorsApport : TValeursMensuelles;

    tabConsoEclairage : TValeursMensuelles;
    tabConsoEclairageApport : TValeursMensuelles;
    tabConsoEclairageHorsApport : TValeursMensuelles;

    //on a ces membres pour les appareils hors ou dans une zone chauff�e, il va falloir
    //stocker les conso s�par�es
    tabConsoLavage : TValeursMensuelles;
    tabConsoFroid : TValeursMensuelles;
    tabConsoElectro : TValeursMensuelles;
    tabConsoBureautique : TValeursMensuelles;

(*$message warn 'il va falloir sauver et charger les consos hors et dans les zones chauff�e et aussi initialiser dans le constructeur'*)
    //ce que l'on fait ici
    //d'abord ce qui compte dans les apports
    tabConsoLavageApport : TValeursMensuelles;
    tabConsoFroidApport : TValeursMensuelles;
    tabConsoElectroApport : TValeursMensuelles;
    tabConsoBureautiqueApport : TValeursMensuelles;

    //puis ce qui ne compte pas dans les apports
    tabConsoLavageHorsApport : TValeursMensuelles;
    tabConsoFroidHorsApport : TValeursMensuelles;
    tabConsoElectroHorsApport : TValeursMensuelles;
    tabConsoBureautiqueHorsApport : TValeursMensuelles;


    deperdition : TDeperdition;


    property consoEP : double read getConsoEP;
    property consoElecSpec : double read getConsoElecSpec;


    Constructor Create;reintroduce;
    destructor Destroy;override;

    procedure reset();

    procedure processSumConso(surfHab : double);

    function getMonthMaxi(useChauff : boolean ;
      useEcs : boolean ;
      useEclairage : boolean ;
      useAppareil : boolean ;
      useCuisine : boolean
      ) : double;
End;

implementation

uses
	IzUtilitaires, SysUtils, XmlUtil;

	Constructor TResultat.Create;
	Begin
		inherited Create;
    innerTagName := 'resultat';
    reset();
		tabBesoinsBruts.total := 0;
		tabApportsInternes.total := 0;
		tabApportsSolaires.total := 0;
		tabTauxDeRecuperation.total := 0;
		tabBesoinsNets.total := 0;

    clim := TDiaGlobalResultData.Create;


		ICH := 0;

		deperdition := TDeperdition.Create;
	End;

	destructor TResultat.Destroy;
	begin
    try
    	freeAndNil(deperdition);
      freeAndNil(clim);
    finally

    end;
		inherited destroy;
  end;


function TResultat.getConsoEP: double;
begin
(*
//ici on travaille avec des totaux (donc tout compris)
  result := TablekWh[7, 1] * COEFF_EP_ELEC
          + TablekWh[7, 2]//ep combustible (gaz, fioul ...)
          + TablekWh[7, 3];//ep bois ( * 0.6 en BBC)
*)
//on ne prend en compte que chauffage, clim et ECS (donc rang 1 et 2)
  result := (TablekWh[1, 1] + TablekWh[2, 1]) * COEFF_EP_ELEC//ep �lec
          + (TablekWh[1, 2] + TablekWh[2, 2]) * 1//ep combustible (gaz, fioul ...)
          + (TablekWh[1, 3] + TablekWh[2, 3]) *1;//ep bois ( * 0.6 en BBC)

end;


function TResultat.getConsoElecSpec: double;
begin
  result := (tabConsoAuxVentil.total +
        tabConsoAuxChauff.total + tabconsoLavage.total +
        tabconsoFroid.total + tabconsoBureautique.total +
        tabconsoElectro.total)
end;

function TResultat.getMonthMaxi(useChauff : boolean ;
			useEcs : boolean ;
			useEclairage : boolean ;
			useAppareil : boolean ;
			useCuisine : boolean
			) : double;
	var
		maxi : double;
		i : integer;
		dbl : double;
	begin
		Maxi := -Maxlongint;
		For i := 1 to 12 do
		begin
			if useChauff and (tabConsoChauff[i,0] > Maxi) then
				Maxi := tabConsoChauff[i,0];
			if useEcs and (tabCECS[i,0] > Maxi) then
				Maxi := tabCECS[i,0];
			if useEclairage and (TabConsoEclairage.content[i] > Maxi) then
				Maxi := TabConsoEclairage.content[i];
			dbl := tabConsoAuxChauff.content[i]
					+tabConsoAuxVentil.content[i]
					+tabConsoLavage.content[i]
					+tabConsoFroid.content[i]
					+tabConsoBureautique.content[i]
					+tabConsoElectro.content[i];
			if useAppareil and (dbl > Maxi) then
				Maxi := dbl;
			if useCuisine and (TabConsoCuisine[i,0] > Maxi) then
				Maxi := TabConsoCuisine[i,0];
		end;
		if Maxi <> -MaxLongint then
			result := Maxi
		else
			result := 0;
	end;



	procedure TResultat.reset();
	var
		i: integer;
		jr: TTypeRessource;
		j: integer;
	begin

  //  clim.reset;

		For i := 1 to 2 do
			For jr := RESSOURCE_URANIUM to RESSOURCE_17 do
				TableRessources[i,jr] := 0;

		For i := 1 to 3 do
			For j := 1 to 17 do
				TableEnvironnement[i,j] := 0;

		For i := 1 to 12 do
		Begin
			tabConsoAuxVentil.content[i] := 0;
			tabConsoAuxChauff.content[i] := 0;

			tabBesoinsBruts.content[i] := 0;
			tabApportsInternes.content[i] := 0;
			tabApportsSolaires.content[i] := 0;
			tabTauxDeRecuperation.content[i] := 0;
			TabBesoinsNets.content[i] := 0;
			TabConsoEclairage.content[i] := 0;
			tabBECS.content[i] := 0;
			TabECSSolaire.content[i] := 0;

			//on va d�coupler les consos
			tabConsoLavage.content[i] := 0;
			tabConsoFroid.content[i] := 0;
			tabConsoElectro.content[i] := 0;
			tabConsoBureautique.content[i] := 0;
			//avec apport et sans
			tabConsoLavageApport.content[i] := 0;
			tabConsoFroidApport.content[i] := 0;
			tabConsoElectroApport.content[i] := 0;
			tabConsoBureautiqueApport.content[i] := 0;
			tabConsoLavageHorsApport.content[i] := 0;
			tabConsoFroidHorsApport.content[i] := 0;
			tabConsoElectroHorsApport.content[i] := 0;
			tabConsoBureautiqueHorsApport.content[i] := 0;
			For j := 0 to 10 do
			Begin
				tabConsoChauff[i,j] := 0;
				TabConsoChauffAccu[i,j] := 0;
				tabCECS[i,j] := 0;
				TabCECSAccu[i,j] := 0;
				TabConsoCuisine[i,j] := 0;
			End;
		End;
	end;


	procedure TResultat.processSumConso(surfHab : double);
	var
		i: integer;
  j: integer;
	begin

		// Calcul totaux
		tabBesoinsBruts.total := 0;
		tabApportsInternes.total := 0;
		tabApportsSolaires.total := 0;
		tabBesoinsNets.total := 0;
		ConsoChauff := 0;
		CECS := 0;
		For i := 1 to 12 do
		Begin
			tabBesoinsBruts.total := tabBesoinsBruts.total + tabBesoinsBruts.content[i];
			tabApportsInternes.total := tabApportsInternes.total + tabApportsInternes.content[i];
			tabApportsSolaires.total := tabApportsSolaires.total + tabApportsSolaires.content[i];
			tabBesoinsNets.total := tabBesoinsNets.total + TabBesoinsNets.content[i];
			ConsoChauff := ConsoChauff+tabConsoChauff[i,0];
			CECS := CECS + tabCECS[i,0];
		end;

		For i := 1 to 9 do
			For j := 1 to 6 do
			Begin
				TablekWh[i,j] := 0;
				TableEuro[i,j] := 0;
				TableCO2[i,j] := 0;
				tabNucleaire[i,j] := 0;
			End;

		// Table kWh chauffage
		For i := 1 to 12 do
		Begin
			TablekWh[1,1] := TablekWh[1,1]+
					tabConsoChauff[i,1];
			TablekWh[1,2] := TablekWh[1,2]+
					tabConsoChauff[i,2]+tabConsoChauff[i,3]+
					tabConsoChauff[i,4]+tabConsoChauff[i,5]+
					tabConsoChauff[i,6]+
					tabConsoChauff[i,8];
			TablekWh[1,3] := TablekWh[1,3] +tabConsoChauff[i,7] +tabConsoChauff[i,10];
		End;
		For i := 1 to 12 do
		Begin
			TablekWh[1,1] := TablekWh[1,1]+
					TabConsoChauffAccu[i,1];
			TablekWh[1,2] := TablekWh[1,2]+
					TabConsoChauffAccu[i,2]+TabConsoChauffAccu[i,3]+
					TabConsoChauffAccu[i,4]+TabConsoChauffAccu[i,5]+
					TabConsoChauffAccu[i,6]+
					TabConsoChauffAccu[i,8];
			TablekWh[1,3] := TablekWh[1,3] +TabConsoChauffAccu[i,7] +TabConsoChauffAccu[i,10];
		End;

		// Table kWh ECS
		For i := 1 to 12 do
		Begin
			TablekWh[2,1] := TablekWh[2,1]+
					tabCECS[i,1];
			TablekWh[2,2] := TablekWh[2,2]
					+tabCECS[i,2]+tabCECS[i,3]
					+tabCECS[i,4]+tabCECS[i,5]
					+tabCECS[i,6]
					+tabCECS[i,8];
    (*
			TablekWh[2,3] := TablekWh[2,3] +tabCECS[i,9]
					+tabCECS[i,7];
    *)
    //on s�pare bois et solaire maintenant
			TablekWh[2,3] := TablekWh[2,3] +tabCECS[i,7] + tabCECS[i,10];
			TablekWh[2,4] := TablekWh[2,4] +tabCECS[i,9];
		End;

		For i := 1 to 12 do
		Begin
			TablekWh[2,1] := TablekWh[2,1]+
					TabCECSAccu[i,1];
			TablekWh[2,2] := TablekWh[2,2]
					+TabCECSAccu[i,2]+TabCECSAccu[i,3]
					+TabCECSAccu[i,4]+TabCECSAccu[i,5]
					+TabCECSAccu[i,6]
					+TabCECSAccu[i,8];
  (*
			TablekWh[2,3] := TablekWh[2,3] +TabCECSAccu[i,7]
					+TabCECSAccu[i,9];
    *)
    //on s�pare bois et solaire maintenant
			TablekWh[2,3] := TablekWh[2,3] +TabCECSAccu[i,7] +TabCECSAccu[i,10];
			TablekWh[2,4] := TablekWh[2,4] +TabCECSAccu[i,9];
		End;

		// Table kWh Cuisine
		For i := 1 to 12 do
		Begin
			TablekWh[3,1] := TablekWh[3,1]+
					TabConsoCuisine[i,1];
			TablekWh[3,2] := TablekWh[3,2]
					+TabConsoCuisine[i,2]+TabConsoCuisine[i,3]
					+TabConsoCuisine[i,4]+TabConsoCuisine[i,5]
					+TabConsoCuisine[i,6]
					+TabConsoCuisine[i,8];
			TablekWh[3,3] := TablekWh[3,3] +TabConsoCuisine[i,7]  +TabConsoCuisine[i,10];
		End;

		// Table kWh Appareillage
		TablekWh[4,1] :=
				TablekWh[4,1]+
				tabConsoAuxVentil.total+
				tabConsoAuxChauff.total+
				tabconsoLavage.total+
				tabconsoFroid.total+
				tabconsoElectro.total+
				tabconsoBureautique.total;

		 // Table kWh Eclairage
		TablekWh[5,1] := TablekWh[5,1]+tabConsoEclairage.total;

    //table kWh clim
		TablekWh[6,1] := TablekWh[6,1] + clim.consoAnnuel;

		 // Sommes kWH
		For i := 1 to 4 do
			For j := 1 to 7 do
				TablekWh[8,i] := TablekWh[8,i]
						+ TablekWh[j,i];
    //on ne prend plus en compte le solaire
		For i := 1 to 8 do
			//For j := 1 to 4 do
			For j := 1 to 3 do
			Begin
				TablekWh[i,5] := TablekWh[i,5]
						+ TablekWh[i,j];
				if surfHab <> 0 then
					TablekWh[i,6] := TablekWh[i,5]/surfHab
				else
					TablekWh[i,6] := 0;
			End;

		For i := 1 to 5 do
    begin
//			For j := 7 to 8 do
      j := 8;
				TablekWh[9,i] := TablekWh[9,i]
						+ TablekWh[j,i];
    end;
	end;


	procedure TResultat.innerLoadList(liste : TStringList);
	Var
		i : integer;
		j : Integer;
		jr : TTypeRessource;
    maxVal : integer;
    s : string;
	Begin
    s := removeFirst(liste);//liste.add('<annee>'); ou max-energ
    if isFullTag(s, 'max-energ') then
    begin
      maxVal := getTagInt(s, 'max-energ');
      removeFirst(liste);//liste.add('<annee>');
    end
    else
    begin
      maxVal := 9;
    end;


		tabBesoinsBruts.total := getTagDouble(Liste,'besoin-brut');

		tabApportsInternes.total := getTagDouble(Liste,'apport-interne');
		tabApportsSolaires.total := getTagDouble(Liste,'apport-solaire');
		tabTauxDeRecuperation.total := getTagDouble(Liste,'taux-recup');
		tabBesoinsNets.total := getTagDouble(Liste,'besoin-net');

		ICH := getTagDouble(Liste,'ich');
		ConsoChauff := getTagDouble(Liste,'conso-chauff');
		IECS := getTagDouble(Liste,'iecs');
		tabBECS.total := getTagDouble(Liste,'besoin-ecs');
		CECS := getTagDouble(Liste,'conso-ecs');
		ConsoCuisine := getTagDouble(Liste,'conso-cuisine');

		tabConsoAuxVentil.total := getTagDouble(Liste,'conso-aux-ventil');
		tabConsoAuxchauff.total := getTagDouble(Liste,'conso-aux-chauff');
		tabConsoLavage.total := getTagDouble(Liste,'conso-lavage');
		tabConsoFroid.total := getTagDouble(Liste,'conso-froid');
		tabConsoBureautique.total := getTagDouble(Liste,'conso-bureau');
		tabConsoElectro.total := getTagDouble(Liste,'conso-menager');

		tabConsoEclairage.total := getTagDouble(Liste,'conso-lumiere');

		Emissions := getTagDouble(Liste,'emissions');
		DechetsNucleaires := getTagDouble(Liste,'dechet-nucleaire');
		TEP := getTagDouble(Liste,'tep');
		tabEcsSolaire.total := getTagDouble(Liste,'ecs-solaire');

		;
		if isTag(liste, deperdition.tagName) then
		begin
			deperdition.loadList(liste);
		end;



		removeFirst(liste);//liste.add('</annee>');

		 For i := 1 to 12 do
		 begin
			removeFirst(liste);//liste.add('<mois id=''' + intToSTr(i) + '''>');
			tabBesoinsBruts.content[i] := getTagDouble(Liste,'besoin-brut');
			tabApportsInternes.content[i] := getTagDouble(Liste,'apport-interne');
			tabApportsSolaires.content[i] := getTagDouble(Liste,'apport-solaire');
			tabTauxDeRecuperation.content[i] := getTagDouble(Liste,'taux-recup');
			TabBesoinsNets.content[i] := getTagDouble(Liste,'besoin-net');


				removeFirst(liste);//liste.add('<conso-chauff>');
				for j := 0 to maxVal do
				begin
					tabConsoChauff[i,j] := getdouble(removeFirst(liste));
				end;
				removeFirst(liste);//liste.add('</conso-chauff>');

				removeFirst(liste);//liste.add('<conso-chauff-accu>');
				for j := 0 to maxVal do
				begin
					TabConsoChauffAccu[i,j] := getdouble(removeFirst(liste));
				end;
				removeFirst(liste);//liste.add('</conso-chauff-accu>');

				removeFirst(liste);//liste.add('<conso-ecs>');
				for j := 0 to maxVal do
				begin
					tabCECS[i,j] := getdouble(removeFirst(liste));
				end;
				removeFirst(liste);//liste.add('</conso-ecs>');

				removeFirst(liste);//liste.add('<conso-ecs-accu>');
				for j := 0 to maxVal do
				begin
					TabCECSAccu[i,j] := getdouble(removeFirst(liste));
				end;
				removeFirst(liste);//liste.add('</conso-ecs-accu>');

				tabBECS.content[i] := getTagDouble(Liste,'besoin-ecs');

				tabConsoAuxVentil.content[i] := getTagDouble(Liste,'conso-aux-ventil');
				tabConsoAuxChauff.content[i] := getTagDouble(Liste,'conso-aux-chauff');
				tabConsoLavage.content[i] := getTagDouble(Liste,'conso-lavage');
				tabConsoFroid.content[i] := getTagDouble(Liste,'conso-froid');
				tabConsoBureautique.content[i] := getTagDouble(Liste,'conso-bureau');
				tabConsoElectro.content[i] := getTagDouble(Liste,'conso-menager');

				removeFirst(liste);//liste.add('<conso-cuisine>');
				for j := 0 to 8 do
				begin
					TABConsoCuisine[i,j] := getdouble(removeFirst(liste));
				end;
				removeFirst(liste);//liste.add('</conso-cuisine>');

				TABConsoEclairage.content[i] := getTagDouble(Liste,'conso-lumiere');

				TABECSSolaire.content[i] := getTagDouble(Liste,'ecs-solaire');
				removeFirst(liste);//liste.add('</mois>');
			End;

//et maintenant on s�pare les cat�gories

		removeFirst(liste);//liste.add('<kwh>');
		For i := 1 to 9 do
			For j := 1 to 6 do
			Begin
				TablekWh[i,j] := getdouble(removeFirst(liste));
			End;
		removeFirst(liste);//liste.add('</kwh>');

		removeFirst(liste);//liste.add('<euro>');
		For i := 1 to 9 do
			For j := 1 to 6 do
			Begin
				TableEuro[i,j] := getdouble(removeFirst(liste));
			End;
		removeFirst(liste);//liste.add('</euro>');

		removeFirst(liste);//liste.add('<co2>');
		For i := 1 to 9 do
			For j := 1 to 6 do
			Begin
				TableCO2[i,j] := getdouble(removeFirst(liste));
			End;
		removeFirst(liste);//liste.add('</co2>');


		removeFirst(liste);//liste.add('<ressources>');
		For i := 1 to 2 do
				For jr := RESSOURCE_URANIUM to RESSOURCE_17 do
				Begin
					TableRessources[i,jr] := getdouble(removeFirst(liste));
				End;
		removeFirst(liste);//liste.add('</ressources>');

		removeFirst(liste);//liste.add('<environnements>');
		For i := 1 to 3 do
				For j := 1 to 17 do
				Begin
					TableEnvironnement[i,j] := getdouble(removeFirst(liste));
				End;
		removeFirst(liste);//liste.add('</environnements>');
	end;



	procedure TResultat.innerSaveList(liste : TStringList);
	Var
		i : integer;
		j : Integer;
		jr : TTypeRessource;
    maxVal : integer;
	Begin
    maxVal := integer(high(TEnergieIdentificateur));
		liste.add(setTag('max-energ', maxVal));
		liste.add('<annee>');

		liste.add(setTag('besoin-brut', tabBesoinsBruts.total));
		liste.add(setTag('apport-interne', tabApportsInternes.total));
		liste.add(setTag('apport-solaire', tabApportsSolaires.total));
		liste.add(setTag('taux-recup', tabTauxDeRecuperation.total));
		liste.add(setTag('besoin-net', tabBesoinsNets.total));
		liste.add(setTag('ich', ICH));
		liste.add(setTag('conso-chauff', ConsoChauff));
		liste.add(setTag('iecs', IECS));
		liste.add(setTag('besoin-ecs', tabBECS.total));
		liste.add(setTag('conso-ecs', CECS));
		liste.add(setTag('conso-cuisine', ConsoCuisine));
		liste.add(setTag('conso-aux-ventil', tabConsoAuxVentil.total));
		liste.add(setTag('conso-aux-chauff', tabConsoAuxChauff.total));
		liste.add(setTag('conso-lavage', tabConsoLavage.total));
		liste.add(setTag('conso-froid', tabConsoFroid.total));
		liste.add(setTag('conso-bureau', tabConsoBureautique.total));
		liste.add(setTag('conso-menager', tabConsoElectro.total));
		liste.add(setTag('conso-lumiere', tabConsoEclairage.total));
		liste.add(setTag('emissions', Emissions));
		liste.add(setTag('dechet-nucleaire', DechetsNucleaires));
		liste.add(setTag('tep', TEP));
		liste.add(setTag('ecs-solaire', tabECSSolaire.total));

		deperdition.saveList(liste);
		liste.add('</annee>');



		 For i := 1 to 12 do
		 Begin
				liste.Add('<mois id=''' + getString(i) + '''>');
					liste.add(setTag('besoin-brut', tabBesoinsBruts.content[i]));
					liste.add(setTag('apport-interne', tabApportsInternes.content[i]));
					liste.add(setTag('apport-solaire', tabApportsSolaires.content[i]));
					liste.add(setTag('taux-recup', tabTauxDeRecuperation.content[i]));
					liste.add(setTag('besoin-net', TabBesoinsNets.content[i]));

					liste.add('<conso-chauff>');
					for j := 0 to maxVal do
					begin
						Liste.Add(getFullString(tabConsoChauff[i,j]));
					end;
					liste.add('</conso-chauff>');

					liste.add('<conso-chauff-accu>');
					for j := 0 to maxVal do
					begin
						Liste.Add(getFullString(TabConsoChauffAccu[i,j]));
					end;
					liste.add('</conso-chauff-accu>');

					liste.add('<conso-ecs>');
					for j := 0 to maxVal do
					begin
						Liste.Add(getFullString(tabCECS[i,j]));
					end;
					liste.add('</conso-ecs>');

					liste.add('<conso-ecs-accu>');
					for j := 0 to maxVal do
					begin
						Liste.Add(getFullString(TabCECSAccu[i,j]));
					end;
					liste.add('</conso-ecs-accu>');

					liste.add(setTag('besoin-ecs', tabBECS.content[i]));

					liste.add(setTag('conso-aux-ventil', tabConsoAuxVentil.content[i]));
					liste.add(setTag('conso-aux-chauff', tabConsoAuxChauff.content[i]));
					liste.add(setTag('conso-lavage', tabConsoLavage.content[i]));
					liste.add(setTag('conso-froid', tabConsoFroid.content[i]));
					liste.add(setTag('conso-bureau', tabConsoBureautique.content[i]));
					liste.add(setTag('conso-menager', tabConsoElectro.content[i]));

					liste.add('<conso-cuisine>');
					for j := 0 to 8 do
					begin
						Liste.Add(getFullString(TabConsoCuisine[i,j]));
					end;
					liste.add('</conso-cuisine>');

					liste.add(setTag('conso-lumiere', TabConsoEclairage.content[i]));

					liste.add(setTag('ecs-solaire', TabECSSolaire.content[i]));
				liste.Add('</mois>');
		 End;


		 liste.Add('<kwh>');
		 For i := 1 to 9 do
				For j := 1 to 6 do
				Begin
						 Liste.add(getFullString(TablekWh[i,j]));
				End;
		 liste.Add('</kwh>');

		 liste.Add('<euro>');
		 For i := 1 to 9 do
				For j := 1 to 6 do
				Begin
						 Liste.add(getFullString(TableEuro[i,j]));
				End;
		 liste.Add('</euro>');

		 liste.Add('<co2>');
		 For i := 1 to 9 do
				For j := 1 to 6 do
				Begin
						 Liste.add(getFullString(TableCO2[i,j]));
				End;
		 liste.Add('</co2>');

		 liste.Add('<ressources>');
		 For i := 1 to 2 do
				For jr := RESSOURCE_URANIUM to RESSOURCE_17 do
				Begin
						 Liste.Add(getFullString(TableRessources[i,jr]));
				End;
		 liste.Add('</ressources>');

		 liste.Add('<environnements>');
		 For i := 1 to 3 do
				For j := 1 to 17 do
				Begin
						 Liste.Add(getFullString(TableEnvironnement[i,j]));
				End;
		 liste.Add('</environnements>');
	End;



	procedure TResultat.innerLoadXml(node : IXmlNode);
	Var
		i : integer;
		j : Integer;
		jr : TTypeRessource;
    maxVal : integer;
    s : string;

    cpt1 : integer;
    cpt2 : integer;

    child : IXmlNode;
    child2 : IXmlNode;
    child3 : IXmlNode;
    child4 : IXmlNode;

	Begin
    maxVal := TXmlUtil.getTagInt(node, 'max-energ');

    child := TXmlUtil.getChild(node, 'annee');

		tabBesoinsBruts.total := TXmlUtil.getTagDouble(child,'besoin-brut');
		tabApportsInternes.total := TXmlUtil.getTagDouble(child,'apport-interne');
		tabApportsSolaires.total := TXmlUtil.getTagDouble(child,'apport-solaire');
		tabTauxDeRecuperation.total := TXmlUtil.getTagDouble(child,'taux-recup');
		tabBesoinsNets.total := TXmlUtil.getTagDouble(child,'besoin-net');
		ICH := TXmlUtil.getTagDouble(child,'ich');
		ConsoChauff := TXmlUtil.getTagDouble(child,'conso-chauff');

    clim.load(TXmlUtil.getChild(child, 'clim'));

		IECS := TXmlUtil.getTagDouble(child,'iecs');
		tabBECS.total := TXmlUtil.getTagDouble(child,'besoin-ecs');
		CECS := TXmlUtil.getTagDouble(child,'conso-ecs');
		ConsoCuisine := TXmlUtil.getTagDouble(child,'conso-cuisine');
		tabConsoAuxVentil.total := TXmlUtil.getTagDouble(child,'conso-aux-ventil');
		tabConsoAuxchauff.total := TXmlUtil.getTagDouble(child,'conso-aux-chauff');
		tabConsoLavage.total := TXmlUtil.getTagDouble(child,'conso-lavage');
		tabConsoFroid.total := TXmlUtil.getTagDouble(child,'conso-froid');
		tabConsoBureautique.total := TXmlUtil.getTagDouble(child,'conso-bureau');
		tabConsoElectro.total := TXmlUtil.getTagDouble(child,'conso-menager');
		tabConsoEclairage.total := TXmlUtil.getTagDouble(child,'conso-lumiere');
		Emissions := TXmlUtil.getTagDouble(child,'emissions');
		DechetsNucleaires := TXmlUtil.getTagDouble(child,'dechet-nucleaire');
		TEP := TXmlUtil.getTagDouble(child,'tep');
		tabEcsSolaire.total := TXmlUtil.getTagDouble(child,'ecs-solaire');
	  deperdition.loadParentXml(child);


    child := TXmlUtil.getChild(node, 'detail-mensuel');
    For cpt1 := 0 to child.ChildNodes.Count - 1 do
    begin
      child2 := child.ChildNodes[cpt1];
      i := TXmlUtil.getAttInt(child2, 'id');
      tabBesoinsBruts.content[i] := TXmlUtil.getTagdouble(child2,'besoin-brut');
      tabApportsInternes.content[i] := TXmlUtil.getTagdouble(child2,'apport-interne');
      tabApportsSolaires.content[i] := TXmlUtil.getTagdouble(child2,'apport-solaire');
      tabTauxDeRecuperation.content[i] := TXmlUtil.getTagdouble(child2,'taux-recup');
      TabBesoinsNets.content[i] := TXmlUtil.getTagdouble(child2,'besoin-net');

      child3 := TXmlUtil.getChild(child2, 'energy-values');
      for cpt2 := 0 to child3.ChildNodes.Count - 1 do
      begin
        child4 := child3.ChildNodes[cpt2];
        j := TXmlUtil.getAttInt(child4, 'energy-id');
        tabConsoChauff[i,j] := TXmlUtil.getTagDouble(child4, 'conso-chauff');
        TabConsoChauffAccu[i,j] := TXmlUtil.getTagDouble(child4, 'conso-chauff-accu');
        tabCECS[i,j] := TXmlUtil.getTagDouble(child4, 'conso-ecs');
        TabCECSAccu[i,j] := TXmlUtil.getTagDouble(child4, 'conso-ecs-accu');
      end;

      tabBECS.content[i] := TXmlUtil.getTagdouble(child2,'besoin-ecs');

      tabConsoAuxVentil.content[i] := TXmlUtil.getTagdouble(child2,'conso-aux-ventil');
      tabConsoAuxChauff.content[i] := TXmlUtil.getTagdouble(child2,'conso-aux-chauff');
      tabConsoLavage.content[i] := TXmlUtil.getTagdouble(child2,'conso-lavage');
      tabConsoFroid.content[i] := TXmlUtil.getTagdouble(child2,'conso-froid');
      tabConsoBureautique.content[i] := TXmlUtil.getTagdouble(child2,'conso-bureau');
      tabConsoElectro.content[i] := TXmlUtil.getTagdouble(child2,'conso-menager');

      child3 := TXmlUtil.getChild(child2, 'consos-cuisine');
      for cpt2 := 0 to child3.ChildNodes.Count - 1 do
      begin
        child4 := child3.ChildNodes[cpt2];
        j := TXmlUtil.getAttInt(child4, 'rank');
        TABConsoCuisine[i,j] := getDouble(child4.Text);
      end;

      TABConsoEclairage.content[i] := TXmlUtil.getTagdouble(child2,'conso-lumiere');

      TABECSSolaire.content[i] := TXmlUtil.getTagdouble(child2,'ecs-solaire');
    End;

//et maintenant on s�pare les cat�gories
    child := TXmlUtil.getChild(node, 'kwh');
    for cpt1 := 0 to child.ChildNodes.Count - 1 do
    begin
      child2 := child.ChildNodes[cpt1];
		  TablekWh[TXmlUtil.getAttInt(child2, 'x'),TXmlUtil.getAttInt(child2, 'y')] := getdouble(child2.Text);
    end;

    child := TXmlUtil.getChild(node, 'euro');
    for cpt1 := 0 to child.ChildNodes.Count - 1 do
    begin
      child2 := child.ChildNodes[cpt1];
		  TableEuro[TXmlUtil.getAttInt(child2, 'x'),TXmlUtil.getAttInt(child2, 'y')] := getdouble(child2.Text);
    end;

    child := TXmlUtil.getChild(node, 'co2');
    for cpt1 := 0 to child.ChildNodes.Count - 1 do
    begin
      child2 := child.ChildNodes[cpt1];
		  TableCO2[TXmlUtil.getAttInt(child2, 'x'),TXmlUtil.getAttInt(child2, 'y')] := getdouble(child2.Text);
    end;


    child := TXmlUtil.getChild(node, 'ressources');
    for cpt1 := 0 to child.ChildNodes.Count - 1 do
    begin
      child2 := child.ChildNodes[cpt1];
		  TableRessources[TXmlUtil.getAttInt(child2, 'x'),TTypeRessource(TXmlUtil.getAttInt(child2, 'y'))] := getdouble(child2.Text);
    end;

    child := TXmlUtil.getChild(node, 'environnement');
    for cpt1 := 0 to child.ChildNodes.Count - 1 do
    begin
      child2 := child.ChildNodes[cpt1];
		  TableEnvironnement[TXmlUtil.getAttInt(child2, 'x'),TXmlUtil.getAttInt(child2, 'y')] := getdouble(child2.Text);
    end;
	end;



	procedure TResultat.innerSaveXml(node : IXmlNode);
	Var
		i : integer;
		j : Integer;
		jr : TTypeRessource;
    maxVal : integer;

    child : IXmlNode;
    child2 : IXmlNode;
    child3 : IXmlNode;
    child4 : IXmlNode;
	Begin
    maxVal := integer(high(TEnergieIdentificateur));
		TXmlUtil.setTag(node, 'max-energ', maxVal);
    child := TXmlUtil.addChild(node, 'annee');

		TXmlUtil.setTag(child, 'besoin-brut', tabBesoinsBruts.total);
		TXmlUtil.setTag(child, 'apport-interne', tabApportsInternes.total);
		TXmlUtil.setTag(child, 'apport-solaire', tabApportsSolaires.total);
		TXmlUtil.setTag(child, 'taux-recup', tabTauxDeRecuperation.total);
		TXmlUtil.setTag(child, 'besoin-net', tabBesoinsNets.total);
		TXmlUtil.setTag(child, 'ich', ICH);
		TXmlUtil.setTag(child, 'conso-chauff', ConsoChauff);

    clim.save(TXmlUtil.addChild(child, 'clim'));

		TXmlUtil.setTag(child, 'iecs', IECS);
		TXmlUtil.setTag(child, 'besoin-ecs', tabBECS.total);
		TXmlUtil.setTag(child, 'conso-ecs', CECS);
		TXmlUtil.setTag(child, 'conso-cuisine', ConsoCuisine);
		TXmlUtil.setTag(child, 'conso-aux-ventil', tabConsoAuxVentil.total);
		TXmlUtil.setTag(child, 'conso-aux-chauff', tabConsoAuxChauff.total);
		TXmlUtil.setTag(child, 'conso-lavage', tabConsoLavage.total);
		TXmlUtil.setTag(child, 'conso-froid', tabConsoFroid.total);
		TXmlUtil.setTag(child, 'conso-bureau', tabConsoBureautique.total);
		TXmlUtil.setTag(child, 'conso-menager', tabConsoElectro.total);
		TXmlUtil.setTag(child, 'conso-lumiere', tabConsoEclairage.total);
		TXmlUtil.setTag(child, 'emissions', Emissions);
		TXmlUtil.setTag(child, 'dechet-nucleaire', DechetsNucleaires);
		TXmlUtil.setTag(child, 'tep', TEP);
		TXmlUtil.setTag(child, 'ecs-solaire', tabECSSolaire.total);
    deperdition.saveXml(child);


    child := TXmlUtil.addChild(node, 'detail-mensuel');
    For i := 1 to 12 do
		Begin
      child2 := TXmlUtil.addChild(child, 'mois');
      TXmlUtil.setAtt(child2, 'id', i);
			TXmlUtil.setTag(child2, 'besoin-brut', tabBesoinsBruts.content[i]);
			TXmlUtil.setTag(child2, 'apport-interne', tabApportsInternes.content[i]);
			TXmlUtil.setTag(child2, 'apport-solaire', tabApportsSolaires.content[i]);
			TXmlUtil.setTag(child2, 'taux-recup', tabTauxDeRecuperation.content[i]);
			TXmlUtil.setTag(child2, 'besoin-net', TabBesoinsNets.content[i]);

      child3 := TXmlUtil.addChild(child2, 'energy-values');
      for j := 0 to maxVal do
      begin
        child4 := TXmlUtil.addChild(child3, 'value');
        TXmlUtil.setAtt(child4, 'energy-id', j);
        TXmlUtil.setTag(child4, 'conso-chauff', tabConsoChauff[i,j]);
        TXmlUtil.setTag(child4, 'conso-chauff-accu', TabConsoChauffAccu[i,j]);
        TXmlUtil.setTag(child4, 'conso-ecs', tabCECS[i,j]);
        TXmlUtil.setTag(child4, 'conso-ecs-accu', TabCECSAccu[i,j]);
      end;

			TXmlUtil.setTag(child2, 'besoin-ecs', tabBECS.content[i]);

			TXmlUtil.setTag(child2, 'conso-aux-ventil', tabConsoAuxVentil.content[i]);
			TXmlUtil.setTag(child2, 'conso-aux-chauff', tabConsoAuxChauff.content[i]);
			TXmlUtil.setTag(child2, 'conso-lavage', tabConsoLavage.content[i]);
			TXmlUtil.setTag(child2, 'conso-froid', tabConsoFroid.content[i]);
			TXmlUtil.setTag(child2, 'conso-bureau', tabConsoBureautique.content[i]);
			TXmlUtil.setTag(child2, 'conso-menager', tabConsoElectro.content[i]);

      child3 := TXmlUtil.addChild(child2, 'consos-cuisine');
			for j := 0 to 8 do
			begin
        child4 := TXmlUtil.setTag(child3, 'value', TabConsoCuisine[i,j]);
        TXmlUtil.setAtt(child4, 'rank', j);
			end;

			TXmlUtil.setTag(child2, 'conso-lumiere', TabConsoEclairage.content[i]);

			TXmlUtil.setTag(child2, 'ecs-solaire', TabECSSolaire.content[i]);
		End;


    child := TXmlUtil.addChild(node, 'kwh');
    For i := 1 to 9 do
      For j := 1 to 6 do
      Begin
        child2 := TXmlUtil.setTag(child, 'value', TablekWh[i,j]);
        TXmlUtil.setAtt(child2, 'x', i);
        TXmlUtil.setAtt(child2, 'y', j);
      End;

    child := TXmlUtil.addChild(node, 'euro');
    For i := 1 to 9 do
      For j := 1 to 6 do
      Begin
        child2 := TXmlUtil.setTag(child, 'value', TableEuro[i,j]);
        TXmlUtil.setAtt(child2, 'x', i);
        TXmlUtil.setAtt(child2, 'y', j);
      End;

    child := TXmlUtil.addChild(node, 'co2');
    For i := 1 to 9 do
      For j := 1 to 6 do
      Begin
        child2 := TXmlUtil.setTag(child, 'value', TableCO2[i,j]);
        TXmlUtil.setAtt(child2, 'x', i);
        TXmlUtil.setAtt(child2, 'y', j);
      End;

    child := TXmlUtil.addChild(node, 'ressources');
    For i := 1 to 2 do
      For jr := RESSOURCE_URANIUM to RESSOURCE_17 do
      Begin
        child2 := TXmlUtil.setTag(child, 'value', TableRessources[i,jr]);
        TXmlUtil.setAtt(child2, 'x', i);
        TXmlUtil.setAtt(child2, 'y', integer(jr));
      End;

    child := TXmlUtil.addChild(node, 'environnement');
    For i := 1 to 3 do
      For j := 1 to 17 do
      Begin
        child2 := TXmlUtil.setTag(child, 'value', TableEnvironnement[i,j]);
        TXmlUtil.setAtt(child2, 'x', i);
        TXmlUtil.setAtt(child2, 'y', j);
      End;
	End;
{ TDiaGlobalResultData }

procedure TDiaGlobalResultData.assign(src_: TDiaGlobalResultData);
begin
  if src_ = nil then
  begin
    reset;
    exit;
  end;
  besoinAnnuel_m2 := src_.besoinAnnuel_m2;
  consoAnnuel_m2 := src_.consoAnnuel_m2;

  besoinAnnuel := src_.besoinAnnuel;
  consoAnnuel := src_.consoAnnuel;

  consoPrimaireAnnuel := src_.consoPrimaireAnnuel;
  emissionCO2Annuel := src_.emissionCO2Annuel;
  nucleaire := src_.nucleaire;
  prixAnnuel := src_.prixAnnuel;
end;

constructor TDiaGlobalResultData.Create;
begin
  inherited;
  reset;
end;

destructor TDiaGlobalResultData.Destroy;
begin

  inherited;
end;

procedure TDiaGlobalResultData.load(node: IXMLNode);
begin
  if node = nil then
  begin
    reset;
    exit;
  end;
  besoinAnnuel_m2 := TXmlUtil.getTagDouble(node, 'need-surf');
  consoAnnuel_m2 := TXmlUtil.getTagDouble(node, 'conso-surf');

  besoinAnnuel := TXmlUtil.getTagDouble(node, 'need');
  consoAnnuel := TXmlUtil.getTagDouble(node, 'conso');

  consoPrimaireAnnuel := TXmlUtil.getTagDouble(node, 'conso-primary');
  emissionCO2Annuel := TXmlUtil.getTagDouble(node, 'co2');
  nucleaire := TXmlUtil.getTagDouble(node, 'nuke');
  prixAnnuel := TXmlUtil.getTagDouble(node, 'price');
end;


procedure TDiaGlobalResultData.save(node: IXMLNode);
begin
  TXmlUtil.setTag(node, 'need-surf', besoinAnnuel_m2);
  TXmlUtil.setTag(node, 'conso-surf', consoAnnuel_m2);

  TXmlUtil.setTag(node, 'need', besoinAnnuel);
  TXmlUtil.setTag(node, 'conso', consoAnnuel);
  TXmlUtil.setTag(node, 'conso-primary', consoPrimaireAnnuel);
  TXmlUtil.setTag(node, 'co2', emissionCO2Annuel);
  TXmlUtil.setTag(node, 'nuke', nucleaire);
  TXmlUtil.setTag(node, 'price', prixAnnuel);
end;

procedure TDiaGlobalResultData.reset;
begin
  besoinAnnuel_m2 := 0;
  consoAnnuel_m2 := 0;

  besoinAnnuel := 0;
  consoAnnuel := 0;

  nucleaire := 0;

  consoPrimaireAnnuel := 0;
  emissionCO2Annuel := 0;
  prixAnnuel := 0;
end;

end.
