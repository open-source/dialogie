unit IzApp;

interface

uses
  Windows, Controls, Classes, SysUtils;


const
{$IFDEF APP_VIRVOLT}
		dialogieName = 'DialogIE op�ration Vir''Volt';
{$ELSE}
		dialogieName = 'DialogIE';
{$ENDIF}

    FILE_EXT_DIALOGIE2 = '.eie2';
    FILE_EXT_DIALOGIEX = '.xeie';


    dialogie3DFileExt = '.eie3d';//fichier de description des parois
    internal3DFileExt = '.i3d';//fichier de g�om�trie de l'�diteur 3D


    dialogie3DFileName = dialogieName + dialogie3DFileExt;//fichier de description des parois
    i3dFileName = dialogieName + internal3DFileExt;//fichier de g�om�trie de l'�diteur 3D

    //nouvelle version XML
    dialogieX3DFileExt = '.xeie3d';//fichier de description des parois
    internalX3DFileExt = '.xi3d';//fichier de g�om�trie de l'�diteur 3D



    dialogieX3DFileName = dialogieName + dialogieX3DFileExt;//fichier de description des parois
    internalX3DFileName = dialogieName + internalX3DFileExt;//fichier de g�om�trie de l'�diteur 3D

type
  TApp = class
  private
    class var innerFormatSettings: TFormatSettings;
    class var innerCommonDataPath: string;
    class var innerDataPath: string;
    class var innerRootTempPath: string;
    class var innerTempPath: string;
    class var innerPath: string;
    class var innerExeName : string;
    class var innerDocumentPath: string;
    class var innerCommonDocumentPath: string;
    class var innerName: string;
    class var innerLaunchedByAssociation: boolean;
    class var innerLaunchedFile: string;
    class var innerVersion: string;
    class var innerDebug: boolean;

    class var innerExeDate: TDate;
    class function getKnownFolderIdFromCSIDL(csidl: integer): TGUID; static;
    class function getDefaultPathFromCSIDL(csidl: integer): string; static;
    class function expandEnvVar(const s: string): string; static;

  public
    class var isMaster: boolean;


    class procedure init(createDocumentDir: boolean = false); overload;

    class function getVersionString(): string;


    class function winExec(const appName: string; commandLine: string; Cache: boolean = false ; waitEnd : boolean = true ; quoteParams : boolean = true): integer; overload;
    class function winExec(const appName: string; parameters: TStrings; Cache: boolean = false ; waitEnd : boolean = true ; quoteParams : boolean = true): integer; overload;


    class function winExecAndWait32(const appName: string; commandLine: string;
      Cache: boolean): integer; overload;
    class function winExecAndWait32(const appName: string;
      parameters: TStrings; Cache: boolean): integer; overload;

    class function getPath(pathId: integer): string;

    class function getMainHandle() : HWND;


    class function openDocument(fileName: string ; openAs : string = ''): boolean;
    class function openURL(URL: string): boolean;
    class property formatSettings: TFormatSettings Read innerFormatSettings;
    class property path: string Read innerPath;
    class property exeName: string Read innerExeName;
    class property commonDataPath: string Read innerCommonDataPath;
    class property dataPath: string Read innerDataPath;
    class property commonDocumentPath: string Read innerCommonDocumentPath;
    class property documentPath: string Read innerDocumentPath;
    class property tempPath: string Read innerTempPath;
    class property rootTempPath: string Read innerRootTempPath;
    class property Name: string Read innerName;
    class property launchedByAssociation
      : boolean Read innerLaunchedByAssociation;
    class property launchedFile: string Read innerLaunchedFile;
    class property exeDate: TDate Read innerExeDate;
    class property version: string Read innerVersion;
    class property isDebug: boolean Read innerDebug;

  end;

var
  dialogie3DPath : string;//chemin du fichier de description des parois
  internal3DPath : string;//chemin du fichier de g�om�trie de l'�diteur 3D

  //version XML
  dialogieX3DPath : string;//chemin du fichier de description des parois
  internalX3DPath : string;//chemin du fichier de g�om�trie de l'�diteur 3D


implementation

uses
  ShlObj, Forms, ShellAPI, knownFolders, IzSysUtil;

(*
  * CSIDL_ADMINTOOLS
  * CSIDL_APPDATA
  * CSIDL_COMMON_ADMINTOOLS
  * CSIDL_COMMON_APPDATA
  * CSIDL_COMMON_DOCUMENTS
  * CSIDL_COOKIES
  * CSIDL_FLAG_CREATE
  * CSIDL_FLAG_DONT_VERIFY
  * CSIDL_HISTORY
  * CSIDL_INTERNET_CACHE
  * CSIDL_LOCAL_APPDATA
  * CSIDL_MYPICTURES
  * CSIDL_PERSONAL
  * CSIDL_PROGRAM_FILES
  * CSIDL_PROGRAM_FILES_COMMON
  * CSIDL_SYSTEM
  * CSIDL_WINDOWS
*)
class procedure TApp.init(createDocumentDir: boolean = false);
var
  Buffer: array [0 .. MAX_PATH] of char;
  rank: integer;
  lSearchRec: TSearchRec;
  s : string;
  // fileHandle : integer;
  // fileDate : longint;
begin

  GetLocaleFormatSettings(LOCALE_SYSTEM_DEFAULT, innerFormatSettings);

  innerDebug := DebugHook <> 0;

  innerName := ExtractFileName(Application.ExeName);
  rank := Pos('.exe', innerName);
  if (rank > 0) then
  begin
    innerName := copy(innerName, 1, rank - 1);
  end;

  innerPath := ExtractFilePath(Application.ExeName); // + '\';
  innerExeName := Application.ExeName;
  delete(innerExeName,1, length(innerPath));

  innerVersion := getVersionString();

  if FindFirst(Application.ExeName, faAnyFile, lSearchRec) = 0 then
  begin
    // extract Date only from File-Date
    innerExeDate := FileDateToDateTime(lSearchRec.Time);
    FindClose(lSearchRec);
  end
  else
    innerExeDate := 0;


  //innerExeDate := 0;
  //fileHandle := fileOpen(Application.ExeName, fmOpenRead);
  //if (fileHandle <> -1) then
  //begin
  //fileDate := fileGetdate(fileHandle);
  //fileClose(fileHandle);
  //innerExeDate := FileDateToDateTime(fileDate);
  //end;

  innerCommonDataPath := getPath(CSIDL_COMMON_APPDATA) + dialogieName + '\';
  if not(directoryExists(innerCommonDataPath)) then
  begin
    createDir(innerCommonDataPath);
  end;

  innerDataPath := getPath(CSIDL_APPDATA) + dialogieName + '\';
  //innerDataPath := getPath(CSIDL_LOCAL_APPDATA) + dialogieName + '\';
  if not(directoryExists(innerDataPath)) then
  begin
    createDir(innerDataPath);
  end;

  //innerDocumentPath := getPath(CSIDL_PERSONAL) + 'Fichiers ' + innerName + '\';
  innerDocumentPath := getPath(CSIDL_PERSONAL) + dialogieName + '\';
  if not(directoryExists(innerDocumentPath)) and createDocumentDir then
  begin
    createDir(innerDocumentPath);
  end;

  s := getPath(CSIDL_COMMON_DOCUMENTS);

  innerCommonDocumentPath := s + dialogieName + '\';
  if not(directoryExists(innerCommonDocumentPath)) and createDocumentDir then
  begin
    createDir(innerCommonDocumentPath);
  end;

  GetTempPath(Length(Buffer) - 1, Buffer);
  innerRootTempPath := StrPas(Buffer);
  innerTempPath := innerRootTempPath + dialogieName + '\';
  if not(directoryExists(innerTempPath)) then
  begin
    createDir(innerTempPath);
  end;

  // l'appli a-t-elle �t� lanc�e en cliquant sur un fichier?
  innerLaunchedByAssociation := False;
  innerLaunchedFile := '';

  // on ne va g�rer que les fichiers cliqu�s pour lancer l'application
  // donc le premier param�tre
  if paramCount() > 0 then
  begin
    if (fileExists(ParamStr(1))) then
    begin
      innerLaunchedFile := ParamStr(1);
      innerLaunchedByAssociation := True;
    end;
  end;
end;

class function TApp.getPath(pathId: integer): string;
var
  idList: PITEMIDLIST;
  path: array [0 .. MAX_PATH] of char;
begin
  result := '';
  if SHGetSpecialFolderLocation(0, pathId, idList) = NOERROR then
  begin
    SHGetPathFromIDList(idList, path); // pour r�cup�rer le dossier � partir de IdList
    Result := string(path);
  end;
  if result = '' then
  begin
    Result := getDefaultPathFromCSIDL(pathId);
  end;
  if result <> '' then
    result := result + '\';
end;


class function TApp.getDefaultPathFromCSIDL(csidl : integer) : string;
begin
// http://msdn.microsoft.com/en-us/library/windows/desktop/dd378457%28v=vs.85%29.aspx
  case csidl of
    CSIDL_ADMINTOOLS : result := '%USERPROFILE%\Start Menu\Programs\Administrative Tools';
    CSIDL_ALTSTARTUP : result := '%USERPROFILE%\Start Menu\Programs\StartUp';
    CSIDL_APPDATA : result := '%APPDATA%';
    CSIDL_CDBURN_AREA : result := '%USERPROFILE%\Local Settings\Application Data\Microsoft\CD Burning';
    CSIDL_COMMON_ADMINTOOLS : result := '%ALLUSERSPROFILE%\Start Menu\Programs\Administrative Tools' ;
    CSIDL_COMMON_ALTSTARTUP : result := '%ALLUSERSPROFILE%\Start Menu\Programs\StartUp';
    CSIDL_COMMON_APPDATA : result := '%ALLUSERSPROFILE%\Application Data';
    CSIDL_COMMON_DESKTOPDIRECTORY : result := '%ALLUSERSPROFILE%\Desktop';
    CSIDL_COMMON_DOCUMENTS : result := '%ALLUSERSPROFILE%\Documents';
    CSIDL_COMMON_FAVORITES : result := '%USERPROFILE%\Favorites';
    CSIDL_COMMON_PROGRAMS : result := '%ALLUSERSPROFILE%\Start Menu\Programs';
    CSIDL_COMMON_STARTMENU : result := '%ALLUSERSPROFILE%\Start Menu';
    CSIDL_COMMON_STARTUP : result := '%ALLUSERSPROFILE%\Start Menu\Programs\StartUp';
    CSIDL_COOKIES : result := '%USERPROFILE%\Cookies' ;
    CSIDL_DESKTOP : result := '%USERPROFILE%\Desktop';
    CSIDL_DESKTOPDIRECTORY : result := '%USERPROFILE%\Desktop';
    CSIDL_FONTS : result := '%windir%\Fonts';
    CSIDL_HISTORY : result := '%USERPROFILE%\Local Settings\History' ;
    CSIDL_INTERNET_CACHE : result := '%USERPROFILE%\Local Settings\Temporary Internet Files' ;
    CSIDL_LOCAL_APPDATA : result := '%USERPROFILE%\Local Settings\Application Data' ;
    CSIDL_MYDOCUMENTS : result := '%USERPROFILE%\My Documents';
    CSIDL_MYPICTURES : result := '%USERPROFILE%\My Documents\My Pictures' ;
    CSIDL_NETHOOD : result := '%USERPROFILE%\NetHood';
    CSIDL_PROFILE : result := '%USERPROFILE%';
    CSIDL_PROGRAM_FILES : result := '%ProgramFiles%' ;
    CSIDL_PROGRAM_FILESX86 : result := '%ProgramFiles%';
    CSIDL_PROGRAM_FILES_COMMON : result := '%ProgramFiles%\Common Files' ;
    CSIDL_PROGRAM_FILES_COMMONX86 : result := '%ProgramFiles%\Common Files';
    CSIDL_PROGRAMS : result := '%USERPROFILE%\Start Menu\Programs';
    CSIDL_STARTMENU : result := '%USERPROFILE%\Start Menu';
    CSIDL_STARTUP : result := '%USERPROFILE%\Start Menu\Programs\StartUp';
    CSIDL_SYSTEM : result := '%windir%\system32' ;
    CSIDL_SYSTEMX86 : result := '%windir%\system32';
    CSIDL_WINDOWS : result := '%windir%' ;
    else
      result := '';
  end;
  result := expandEnvVar(result);
end;

class function TApp.expandEnvVar(const s: string): string;
var bufSize: Integer;
begin
  // Get required buffer size
  bufSize := ExpandEnvironmentStrings( PChar(s), nil, 0);
  if bufSize > 0 then
  begin
    // Read expanded string into result string
    SetLength(Result, bufSize - 1);
    ExpandEnvironmentStrings(PChar(s), PChar(Result), bufSize);
  end
  else
  begin
    Result := '';
  end;
end;


class function TApp.getKnownFolderIdFromCSIDL(csidl : integer) : TGUID;
begin
// http://msdn.microsoft.com/en-us/library/windows/desktop/bb762494%28v=vs.85%29.aspx
  case csidl of
    CSIDL_ADMINTOOLS : result := FOLDERID_AdminTools;
    CSIDL_ALTSTARTUP : result := FOLDERID_Startup;
    CSIDL_APPDATA : result := FOLDERID_RoamingAppData;
    CSIDL_BITBUCKET : result := FOLDERID_RecycleBinFolder;
    CSIDL_CDBURN_AREA : result := FOLDERID_CDBurning;
    CSIDL_COMMON_ADMINTOOLS : result := FOLDERID_CommonAdminTools ;
    CSIDL_COMMON_ALTSTARTUP : result := FOLDERID_CommonStartup;
    CSIDL_COMMON_APPDATA : result := FOLDERID_ProgramData;
    CSIDL_COMMON_DESKTOPDIRECTORY : result := FOLDERID_PublicDesktop;
    CSIDL_COMMON_DOCUMENTS : result := FOLDERID_PublicDocuments;
    CSIDL_COMMON_FAVORITES : result := FOLDERID_Favorites;
    CSIDL_COMMON_PROGRAMS : result := FOLDERID_CommonPrograms;
    CSIDL_COMMON_STARTMENU : result := FOLDERID_CommonStartMenu;
    CSIDL_COMMON_STARTUP : result := FOLDERID_CommonStartup;
    CSIDL_COMPUTERSNEARME : result := FOLDERID_NetworkFolder;
    CSIDL_CONTROLS : result := FOLDERID_ControlPanelFolder;
    CSIDL_COOKIES : result := FOLDERID_Cookies ;
    CSIDL_DESKTOP : result := FOLDERID_Desktop;
    CSIDL_DESKTOPDIRECTORY : result := FOLDERID_Desktop;
    CSIDL_DRIVES : result := FOLDERID_ComputerFolder;
    CSIDL_FONTS : result := FOLDERID_Fonts;
    CSIDL_HISTORY : result := FOLDERID_History ;
    CSIDL_INTERNET_CACHE : result := FOLDERID_InternetCache ;
    CSIDL_LOCAL_APPDATA : result := FOLDERID_LocalAppData ;
    CSIDL_MYDOCUMENTS : result := FOLDERID_Documents;
    CSIDL_MYPICTURES : result := FOLDERID_Pictures ;
    CSIDL_NETHOOD : result := FOLDERID_NetHood;
    CSIDL_NETWORK : result := FOLDERID_NetworkFolder;
    CSIDL_PRINTERS : result := FOLDERID_PrintersFolder;
    CSIDL_PROFILE : result := FOLDERID_Profile;
    CSIDL_PROGRAM_FILES : result := FOLDERID_ProgramFiles ;
    CSIDL_PROGRAM_FILESX86 : result := FOLDERID_ProgramFilesX86;
    CSIDL_PROGRAM_FILES_COMMON : result := FOLDERID_ProgramFilesCommon ;
    CSIDL_PROGRAM_FILES_COMMONX86 : result := FOLDERID_ProgramFilesCommonX86;
    CSIDL_PROGRAMS : result := FOLDERID_Programs;
    CSIDL_STARTMENU : result := FOLDERID_StartMenu;
    CSIDL_STARTUP : result := FOLDERID_Startup;
    CSIDL_SYSTEM : result := FOLDERID_System ;
    CSIDL_SYSTEMX86 : result := FOLDERID_SystemX86;
    CSIDL_WINDOWS : result := FOLDERID_Windows ;
  end;

end;


class function TApp.winExecAndWait32(const appName: string;
  commandLine: string; Cache: boolean): integer;
var
  params: TStringList;
begin
  params := TStringList.Create;
  if (commandLine <> '') then
    params.Add(commandLine);
  Result := winExecAndWait32(appName, params, Cache);
  params.Free;
end;

class function TApp.winExecAndWait32(const appName: string;
  parameters: TStrings; Cache: boolean): integer;
var
  zCommandLine, zdirectory: PChar;
  StartupInfo: TStartupInfo;
  ProcessInfo: TProcessInformation;
  ExitCode: DWORD;
  commandLine: string;
  cpt: integer;
begin

  commandLine := appName;

  for cpt := 0 to parameters.Count - 1 do
  begin
    commandLine := commandLine + ' "' + parameters[cpt] + '"';
  end;

  zCommandLine := PChar(commandLine);
  zdirectory := PChar(ExtractFilePath(ExpandFileName(appName)));

  FillChar(StartupInfo, Sizeof(StartupInfo), #0);
  StartupInfo.cb := Sizeof(StartupInfo);

  StartupInfo.dwFlags := STARTF_USESHOWWINDOW;
  if not Cache then
    StartupInfo.wShowWindow := SW_SHOWDEFAULT
  else
    StartupInfo.wShowWindow := SW_HIDE;
  if not CreateProcess(nil, { application name }
    zCommandLine, { pointer to command line string }
    nil, { pointer to process security attributes }
    nil, { pointer to thread security attributes }
    False, { handle inheritance flag }
    CREATE_NEW_CONSOLE or { creation flags }
    NORMAL_PRIORITY_CLASS, nil,
    { pointer to new environment block }
    zdirectory, { pointer to current directory name }
    StartupInfo, { pointer to STARTUPINFO }
    ProcessInfo) then { pointer to PROCESS_INF }
  begin
    Result := -1;
  end
  else
  begin
    while WaitforSingleObject(ProcessInfo.hProcess, 100) = WAIT_TIMEOUT do
    begin
      Sleep(1000);
      // Application.ProcessMessages;
    end;
    GetExitCodeProcess(ProcessInfo.hProcess, ExitCode);
    Result := ExitCode;
  end;
end;

class function TApp.getVersionString(): string;
var
  S: string;
  nb: cardinal;
  Buffer: PChar;
  versionAppl: PChar;
  versionLongueur: cardinal;
begin
  Result := '0.0.0.0'; // Valeur par d�faut
  Buffer := '';
  S := Application.ExeName;

  nb := GetFileVersionInfoSize(PChar(S), nb);
  if nb > 0 then
    try
      { --- R�servation en m�moire d'une zone de la taille voulue --- }
      Buffer := AllocMem(nb);
      { --- Copie dans le buffer des informations --- }
      GetFileVersionInfo(PChar(S), 0, nb, Buffer);
      { --- Recherche de l'information de version --- }
      if VerQueryValue(Buffer, PChar('\StringFileInfo\040C04E4\FileVersion'),
        Pointer(versionAppl), versionLongueur) then
        Result := versionAppl;
    finally
      FreeMem(Buffer, nb);
    end;
end;

class function TApp.openDocument(fileName: string ; openAs : string = ''): boolean;
var
  openAsAppPath : string;
begin
  if openAs = '' then
  begin
    try
      ShellExecute(getMainHandle, PChar('open'), PChar(fileName),
        PChar(''), PChar(ExtractFilePath(fileName)), SW_SHOWNORMAL);
      Result := True;
    except
      Result := False;
    end;
    exit;
  end;
  openAsAppPath := TIzSysUtil.getAppPathFromFileAssociation(openAs);
  if openAsAppPath = '' then
  begin
    Result := false;
    exit;
  end;
  winExec(openAsAppPath, fileName, false, false);
  result := true;
end;

class function TApp.openURL(URL: string): boolean;
begin
  try
    ShellExecute(getMainHandle, PChar('open'), PChar(URL),
      PChar(''), PChar(''), SW_SHOWNORMAL);
    Result := True;
  except
    Result := False;
  end;
end;


class function TApp.getMainHandle() : HWND;
begin
  if Application.MainForm = nil then
  begin
    result := GetDesktopWindow;
  end
  else
  begin
    result := Application.MainForm.Handle;
  end;
end;


class function TApp.winExec(const appName: string; commandLine: string; Cache: boolean ; waitEnd : boolean ; quoteParams : boolean): integer;
var
  params: TStringList;
begin
  params := TStringList.Create;
  if (commandLine <> '') then
    params.Add(commandLine);
  Result := winExec(appName, params, Cache, waitEnd, quoteParams);
  params.Free;
end;

class function TApp.winExec(const appName: string; parameters: TStrings; Cache: boolean ; waitEnd : boolean ; quoteParams : boolean): integer;
var
  zCommandLine, zdirectory: PChar;
  StartupInfo: TStartupInfo;
  ProcessInfo: TProcessInformation;
  ExitCode: DWORD;
  commandLine: string;
  cpt: integer;
begin

  commandLine := appName;

  for cpt := 0 to parameters.Count - 1 do
  begin
    if quoteParams then
    begin
      commandLine := commandLine + ' "' + parameters[cpt] + '"';
    end
    else
    begin
      commandLine := commandLine + ' ' + parameters[cpt];
    end;
  end;

  zCommandLine := PChar(commandLine);
  zdirectory := PChar(ExtractFilePath(ExpandFileName(appName)));

  FillChar(StartupInfo, Sizeof(StartupInfo), #0);
  StartupInfo.cb := Sizeof(StartupInfo);

  StartupInfo.dwFlags := STARTF_USESHOWWINDOW;
  if not Cache then
    StartupInfo.wShowWindow := SW_SHOWDEFAULT
  else
    StartupInfo.wShowWindow := SW_HIDE;
  if not CreateProcess(nil, { application name }
    zCommandLine, { pointer to command line string }
    nil, { pointer to process security attributes }
    nil, { pointer to thread security attributes }
    False, { handle inheritance flag }
    CREATE_NEW_CONSOLE or { creation flags }
    NORMAL_PRIORITY_CLASS, nil,
    { pointer to new environment block }
    zdirectory, { pointer to current directory name }
    StartupInfo, { pointer to STARTUPINFO }
    ProcessInfo) then { pointer to PROCESS_INF }
  begin
    Result := -1;
  end
  else
  begin
    if not(waitEnd) then
      exit(0);
    while WaitforSingleObject(ProcessInfo.hProcess, 100) = WAIT_TIMEOUT do
    begin
      Sleep(1000);
      // Application.ProcessMessages;
    end;
    GetExitCodeProcess(ProcessInfo.hProcess, ExitCode);
    Result := ExitCode;
  end;
end;



initialization

TApp.init(true);

CreateDir(TApp.documentPath + 'Projets');
CreateDir(TApp.documentPath + 'Biblioth�ques');
CreateDir(TApp.documentPath + 'Biblioth�ques\Fichiers Meteo');

dialogie3DPath := TApp.tempPath + dialogie3DFileName;
internal3DPath := TApp.tempPath + i3dFileName;

dialogieX3DPath := TApp.tempPath + dialogieX3DFileName;
internalX3DPath := TApp.tempPath + internalX3DFileName;



TApp.isMaster := false;

end.

