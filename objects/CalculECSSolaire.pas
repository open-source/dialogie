unit CalculECSSolaire;

interface

uses
  CalculMeteo,
  SysUtils,
  Classes,

  EquipementECSSolaire,
  UtilitairesDialogie,
  diaMeteo;

const
  Declinaison: TListedecl = (-20.8, -12.7, -1.9, 9.9, 18.9, 23.1, 21.3, 13.7, 3.0, -8.8, -18.4, -23.0, -20.8);


type
  tableMois = array[1..12] of double;


type
  TCECS = class
  private
    Consommation: array[1..12] of double;
    GlobalFinal: array[1..12] of double;
    BesoinsMensuels: array[1..12] of double;
    Couverture: array[1..12] of double;

    Bcapteur,
    Kcapteur,
    AbnpCapteur,
    KsBCapteur,
    DnCapteur: double;

    Inclinaison,
    Orientation,
    Surface: double;

    RendementAppoint,
    TEauChaude,
    VolumeStockage: double;

    TypeCapteur: integer;

    ConstanteRefroidissement,
    DeperditionTuyauterie: double;

    Circulation,
    Echangeur,
    Ballon: integer;

    TypeAppoint,
    EnergieAppoint: integer;

    StationMeteo: TStation;

    PrixkWh,
    RdEchangeur: double;

    GefSol:    TGefSol;
    situation: double;


    procedure calculECS;
    procedure process; overload;
  public
    constructor Create;
    destructor Destroy(); override;


    procedure process(var tabResult: TValeursMensuelles;
      eCSsolaire: TEquipementECSSolaire; site: TStation; surfHab: double;
      tabBECS: TValeursMensuelles); overload;
  end;

implementation


uses
  IzConstantes, Math;



function puissance(base: double; exposant: double): double;
begin
  result := exp(ln(base) * exposant);
end;



constructor TCECS.Create;
begin
  inherited Create();
  GefSol := TGefSol.Create;
end;


destructor TCECS.Destroy();
begin
  if gefSol <> nil then
    FreeAndNil(gefSol);
  inherited Destroy();
end;


procedure TCECS.process;
var
  i:  integer;
  GM: array[1..12] of double;
begin
  GefSol.CalculO(StationMeteo.latitude, Inclinaison, Orientation, 1);
  for i := 1 to 12 do
  begin
    //rayonnement solaire global sur le plan des capteurs en moyenne mensuelle
    //remplacer 24 * durr�e mois par nbheure d'ensolleilement mensuel
    //    GM[i] := Sqrt(2 * (StationMeteo.Insolation[i] / (24 * DureeMois[i])) + 1) - 0.72;
    GM[i] := Sqrt(2 * (StationMeteo.Insolation.content[i] /
      (GefSol.LongueurJour[i] * DureeMois[i])) + 1) - 0.72;
  end;
  for i := 1 to 12 do
    GlobalFinal[i] := Gm[i] * GefSol.RayGlobalM[i] * (1 - situation);
  calculECS;
end;


procedure TCECS.calculECS;
var
  k:  double;
  R:  double;
  mCp: double;
  X:  double;
  Kech: double;
  Xech: double;
  npr: double;
  np: double;
  Xelec: double;
  Vcelec: double;
  Vs: double;
  PaecsAn: double;
  PasecsAn: double;
  FecsAn: double;
  EcoEcs: double;
  EcoFEcs: double;
  Ds: double;
  becs: tableMois;
  g:  tableMois;
  Textj: tableMois;
  delta: tableMois;
  delta1: tableMois;
  delta2: tableMois;
  Tlocal: tableMois;
  Em: tableMois;
  S:  tableMois;
  T:  tableMois;
  Q:  tableMois;
  Z:  tableMois;
  F2: tableMois;
  Fecs: tableMois;

  Dtu: double;   { 1 : Kt=forfait,   2 : Kt=calcul }
  Kt: double;    { d�perdition par les tuyauteries }
  A:  double;     { surface du capteur }
  Kc: double;    { coefficient de d�perdition par le capteur }
  Cir: double;  { 1 : Circulation forc�e,   2 : thermosyphon }
  Ech: double;  { 1: Sans echangeur, 2 : echangeur int�gr�; 3 : �changeur s�par� }
  Bal: double;
  { 1 : ballon dans piece non chauffee, 2 : ballon dans piece chauffe, 3 : ballon a l'exterieur}
  Cecs: double;  { prix du Kwh }
  Ap: double;    { 1 : appoint int�gr�, 2 : appoint s�par� }
  Vn: double;    { volume de stockage }
  Ctr: double;   { Cte de refroidissement  => 1 : calcul forfaitaire, 2 : Connue, 3 : Calcul }
  Cr: double;      { Cte de refroidissement dans le cas ou Ctr = 2 }
  RdEcs: double;   { Rendement de l'echangeur }
  Bc: double;          { facteur optique du capteur }
  Zone: double;       { 1 : Zone continentale, 2 : Zone Maritime }
  tref: double;    { temperature d'eau chaude }
  modele: double;   { 1 : s�par�, 2 : monobloc }
  ABnp: double;     { caracteristique 1 monobloc }
  Dn: double;          { caracteristique 2 monobloc }
  KsB: double;     { caract�ristique 3 monobloc }
  Al: double;      { 1 : Heures normales, 2 : heure creuses }
  rau: double;     { latitude du site }
  Nj: tableMois;       { nombre de jours ???? ����� A voir avec St�phane ������ }
  Er: TableMois;      { Energie solaire recut sur le capteur }
  Tef: tableMois;     { temperature d'eau froide }
  Text: tableMois;   { Temperature exterieure }
  petitdelta: tableMois; { d�clinaison en moyenne mensuelle }
  Cm: tableMois;    { consommation maximale mensuelle}

  i: integer;




  procedure decoupage;
  var
    //      i: longint;
    j: longint;
    //      indice: longint;
    //      valeur: String;
  begin
    //    i := 0;

    if DeperditionTuyauterie = -1 then
      Dtu := 1
    else
    begin
      Dtu := 2;
      Kt  := DeperditionTuyauterie;
    end;
    A  := Surface;
    Kc := Kcapteur;
    Cir := Circulation;
    Ech := Echangeur;
    Bal := Ballon;
    Cecs := PrixkWh;
    Ap := TypeAppoint;
    Vn := VolumeStockage;
    if ConstanteRefroidissement = -1 then
      Ctr := 1
    else
    begin
      Ctr := 2;
      Cr  := ConstanteRefroidissement;
    end;

    RdEcs := RdEchangeur;
    Bc := Bcapteur;

    //A v�rifier !
    //if StationMeteo.ZoneMerTerre = 'Continentale' then
    Zone := 1;
    //else
    //   Zone := 2;


    tref := TEauChaude;
    modele := TypeCapteur;
    ABnp := ABnpCapteur;
    Dn  := DnCapteur;
    KsB := KsbCapteur;
    Al  := 1;
    Rau := StationMeteo.Latitude;
    for j := 1 to 12 do
      Nj[j] := dureeMois[j];
    for j := 1 to 12 do
      Er[j] := GlobalFinal[j] / 1000;
    for j := 1 to 12 do
      Tef[j] := StationMeteo.EauFroide.content[j];
    for j := 1 to 12 do
      Text[j] := StationMeteo.Moy.content[j];
    for j := 1 to 12 do
      PetitDelta[j] := Declinaison[j];
    for j := 1 to 12 do
      Cm[j] := Consommation[j];
  end;


  procedure analyse;
  var
    mois: integer;
    somme1: double;
  begin
    if dtu = 1 then
      Kt := 5 + 0.5 * A;

    k := Kc + Kt / A;

    if cir = 1 then
    begin
      R := 0.9;
      mCp := 40 * A;
    end
    else if cir = 2 then
    begin
      R := 0.95;
      mCp := 10 * A;
    end;

    X := mCp / (Kc * A + Kt);
    if Ech = 1 then
      np := R * (1 - 1 / (2 * X))
    else if Ech = 2 then
    begin
      Kech := 50 * A;
      Xech := mCp / Kech;
      np := R * ((1 - 1 / (2 * X + 12 * X * Xech)) / (1 + Xech / X));
    end
    else if Ech = 3 then
    begin
      Kech := 50 * A;
      npr := Kech / (mCp + Kech);
      np := R * X / (1 / npr + 1 / (exp(1 / X) - 1));
    end;

    if Ap = 1 then
    begin

      if Al = 1 then
        Xelec := 0.9
      else
        Xelec := 0.6;

      Vcelec := 0.3 * Vn;
      Vs := Vn * (1 - Xelec * (Vcelec / Vn));
    end
    else
      Vs := 0.85 * Vn;
    if CtR = 1 then
      Ds := 0.01 * Vs
    else if CtR = 2 then
      Ds := CR * Vs / 24;

    for mois := 1 to 12 do
    begin
      G[mois] := Nj[mois] * Er[mois];

      if zone = 1 then
        Textj[mois] := Text[mois] + 4
      else
        Textj[mois] := Text[mois] + 2;

      if bal = 3 then
        Tlocal[mois] := Text[mois]
      else if bal = 1 then
        Tlocal[mois] := (Text[Mois] + 19) / 2
      else
        Tlocal[mois] := 19;

      delta1[mois] := Textj[mois] - Tef[mois];
      delta2[mois] := Tlocal[mois] - Tef[mois];
      delta[mois]  := Tref - Tef[mois];

      Becs[mois] := 0.00116 * delta[mois] * Cm[mois] * Nj[mois];
      Em[mois] := 650 + 800 * sin((1.8 * (60 - Rau + petitdelta[mois])) / 180 * pi);

      if modele = 1 then
      begin
        S[mois] := 16.536 * Ds / Cm[mois];
        T[mois] := (delta1[mois] + Em[mois] / (K / Bc)) / delta[mois];
        if G[mois] <> 0 then
          Q[mois] := Becs[mois] * Em[mois] / (G[mois] * A * np * K * delta[mois])
        else
          Q[mois] := 0;
      end
      else
      begin
        S[mois] := 16.536 * Dn / Cm[mois];
        T[mois] := (delta1[mois] + Em[mois] / (KsB)) / delta[mois];
        if G[mois] <> 0 then
          Q[mois] := Becs[mois] * Em[mois] / (G[mois] * ABnp * (KsB) * delta[mois])
        else
          Q[mois] := 0;
      end;

      Z[mois]  := Cm[mois] * (1 + T[mois] * delta[mois] / 80) / (T[mois] * Vs);
      F2[mois] := (1 / (1 + S[mois])) * (T[mois] / (1 + Q[mois])) +
        S[mois] * (delta2[mois] / delta[mois]);
      Fecs[mois] := sqrt(1 / (1 + (2 / (exp(2 * puissance(F2[mois], 2)) - 1)) +
        puissance(0.2 * Z[mois], 2)));
    end;

    PaecsAn := 0;
    somme1  := 0;
    for mois := 1 to 12 do
    begin
      PaecsAn := PaecsAn + (Becs[mois] * Fecs[mois]);
      somme1  := somme1 + Becs[mois];
    end;
    FecsAn  := PaecsAn / somme1;
    PasecsAn := PaecsAn / A;
    Ecoecs  := PaecsAn / Rdecs;
    EcoFecs := Ecoecs * Cecs;
  end;

begin
  decoupage;
  analyse;
  for i := 1 to 12 do
  begin
    BesoinsMensuels[i] := BEcs[i];
    Couverture[i] := FEcs[i];
  end;
end;




procedure TCECS.process(var tabResult: TValeursMensuelles;
  ECSsolaire: TEquipementECSSolaire; site: TStation; surfHab: double;
  tabBECS: TValeursMensuelles);
var
  i: integer;
  volumeMitige: double;
begin
  if ECSSolaire.systeme = 0 then
  begin
    KsBCapteur := ECSSolaire.menuCapteur.table[1].Car1;
    DnCapteur  := ECSSolaire.menuCapteur.table[1].Car2;
    // ATTENTION A COMPLETER !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    AbnpCapteur := 1.3;
  end
  else
  begin
    Kcapteur := ECSSolaire.menuCapteur.table[2 + ECSSolaire.Tyype].Car1;
    BCapteur := ECSSolaire.menuCapteur.table[2 + ECSSolaire.Tyype].Car2;
  end;
  Inclinaison := ECSSolaire.Inclinaison;
  Orientation := ECSSolaire.Orientation;
  Surface := ECSSolaire.Surface;
  RendementAppoint := 1; // Finalement non utilis�
  TEauChaude := ECSSolaire.TemperatureEau;
  VolumeStockage := ECSSolaire.Volume;
  situation := ECSsolaire.Situation;
  if ECsSolaire.Systeme = 0 then
    TypeCapteur := 2
  else
    TypeCapteur := 1;
  if ECSSolaire.Isolation = 1 then
    ConstanteRefroidissement := Power(ECSSolaire.Volume, -0.33)
  else
    ConstanteRefroidissement := 3.3 * Power(ECSSolaire.Volume, -0.45);

  DeperditionTuyauterie := ECSSolaire.menuCaracteristique.table[
    ECsSolaire.Systeme + 1].Tuyau1 +
    ECSSolaire.menuCaracteristique.table[ECsSolaire.Systeme + 1].Tuyau1 * surface;

  Circulation := ECSSolaire.menuCaracteristique.table[ECsSolaire.Systeme +
    1].Circulation;
  Echangeur := 2;
  if ECSSolaire.Systeme = 0 then
    Ballon := 3
  else
    Ballon := ECSSolaire.Emplacement + 1;

  // A v�rifier
  TypeAppoint := 1;
  EnergieAppoint := 1;
  prixkWh := 0;
  RdEchangeur := 0.8;
  StationMeteo := Site;
  //    Volumemitige := 12.1 * SurfHab * 52 / 365;

  if (SurfHab <= 27) then
  begin
    Volumemitige := 17.7;
  end
  else
  begin
    Volumemitige := (365.24 / 330) * (470.9 * ln(SurfHab) - 1075) * 52 / 365;
  end;
  for i := 1 to 12 do
  begin
    Consommation[i] := volumeMitige * (40 - Site.EauFroide.content[i]) /
      (TEauChaude - Site.EauFroide.content[i]);
  end;

  process;
  tabResult.total := 0;
  for i := 1 to 12 do
  begin
    tabResult.content[i] := tabBECS.content[i] * Couverture[i];
    tabResult.total := tabResult.total + tabResult.content[i];
  end;
end;

end.
