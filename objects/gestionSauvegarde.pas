unit gestionSauvegarde;

interface

uses
  Classes, CreerSauvegarde;

type
  Tsection = class
    Nom: string;
    Debut: integer;
    Fin: integer;
    constructor Create(Nom_: string; Debut_, Fin_: integer);
  end;

  TListeSection = class(TList)
  private
    function GetSection(const i: integer): Tsection; inline;
    procedure SetSection(const i: integer; const Valeur: Tsection); inline;
  public
    property objets[const Index: integer]
      : Tsection Read GetSection Write SetSection; default;
    function Courant: Tsection; inline;
    procedure Push(const Section: Tsection); inline;
    procedure Pop; inline;
  end;

  TSauvegarde = class
  private
    Sections: TListeSection;
    Fposition: integer;
  public
    Liste: TCreationSauvegarde;
    procedure LoadFromFile(const FileName: string);
    procedure SaveToFile(const FileName: string);
      overload;
    constructor Create;
    destructor Destroy; override;
    function Count: integer; inline; // Nombre de ligne du fichier
    procedure reset; inline; // Ram�ne au debut du fichier
    procedure Debut; inline; // Ram�ne au d�but de la section en cours
    procedure Fin; inline; // Ram�ne au d�but de la section en cours
    function Suivant: Boolean; inline; // Ligne Suivante de la section
    function Precedent: Boolean; inline; // Ligne pr�c�dente de la section
    function OuvreSection(const NomSection: string): Boolean;
    function OuvreSectionPrecedente(const NomSection: string): Boolean; inline;
    procedure FermeSection; inline;
    procedure FermeSectionPuisPrecedent; inline;
    function GetLigne: string; inline;
    function GetLigneBool: Boolean; inline;
    function GetLigneReel: double; inline;
    function GetLigneEntier: integer; inline;
    function GetLigneSansBouger: string; inline;
    procedure AddStrings(S: TSauvegarde);
    function EOF: Boolean; inline;
    procedure SetLigne(const Chaine: string); inline;
    procedure setList(const liste_: TStringList); inline;
    property position: integer Read Fposition;
    function SuivantSiEgal(const Valeur: string): Boolean;
    procedure Clear;
  end;

implementation

uses
  SysUtils;

// TSection

constructor Tsection.Create(Nom_: string; Debut_, Fin_: integer);
begin
  inherited Create;
  Nom := Nom_;
  Debut := Debut_;
  Fin := Fin_;
end;

// TListeSection

procedure TListeSection.Push(const Section: Tsection);
begin
  Add(Section);
end;

procedure TListeSection.Pop;
begin
  Courant.Free;
  Delete(Count - 1);
end;

function TListeSection.GetSection(const i: integer): Tsection;
begin
  Result := items[i];
end;

procedure TListeSection.SetSection(const i: integer; const Valeur: Tsection);
begin
  items[i] := Valeur;
end;

function TListeSection.Courant: Tsection;
begin
  Result := items[Count - 1];
end;

// TSauvegarde

procedure TSauvegarde.LoadFromFile(const FileName: string);
begin
  Liste.LoadFromFile(FileName);
  reset;
end;


procedure TSauvegarde.SaveToFile(const FileName: string);
begin
  Liste.SaveToFile(FileName, TEncoding.Unicode);
end;


constructor TSauvegarde.Create;
begin
  inherited Create;
  Fposition := 0;
  Liste := TCreationSauvegarde.Create;
  Sections := TListeSection.Create;
  Sections.Push(Tsection.Create('', 0, Liste.Count - 1));
end;

destructor TSauvegarde.Destroy;
begin
  reset;
  Liste.Free;
  Sections.Pop;
  Sections.Free;
  inherited Destroy;
end;

function TSauvegarde.Count: integer;
begin
  Result := Liste.Count;
end;

procedure TSauvegarde.reset;
begin
  Fposition := 0;
  while Sections.Count <> 1 do
    Sections.Pop;
  Sections.Courant.Fin := Liste.Count - 1;
end;

procedure TSauvegarde.Debut;
begin
  Fposition := Sections.Courant.Debut;
end;

procedure TSauvegarde.Fin;
begin
  Fposition := Sections.Courant.Fin;
end;

function TSauvegarde.Suivant: Boolean;
begin
  if Fposition <= Sections.Courant.Fin then
  begin
    Inc(Fposition);
    Result := True;
  end
  else
    Result := False;
end;

function TSauvegarde.Precedent: Boolean;
begin
  if Fposition = Sections.Courant.Debut then
    Result := False
  else
  begin
    Dec(Fposition);
    Result := True;
  end;
end;

function TSauvegarde.OuvreSection(const NomSection: string): Boolean;
var
  i: integer;
  Debut, Fin: integer;
  BaliseOuverture, BaliseFermeture: string;
begin
  Debut := -100;
  Fin := -100;
  BaliseOuverture := '<' + NomSection + '>';
  BaliseFermeture := '</' + NomSection + '>';
  for i := Fposition to Sections.Courant.Fin do
  begin
    if (Debut = -100) and (Liste[i] = BaliseOuverture) then
      Debut := i + 1;
    if (Fin = -100) and (Liste[i] = BaliseFermeture) then
      Fin := i - 1;
  end;
  if (Debut <> -100) and (Fin <> -100) then
  begin
    Sections.Push(Tsection.Create(NomSection, Debut, Fin));
    Fposition := Debut;
    Result := True;
  end
  else
    Result := False;
end;

function TSauvegarde.OuvreSectionPrecedente(const NomSection: string): Boolean;
var
  i: integer;
  Debut, Fin: integer;
  BaliseOuverture, BaliseFermeture: string;
begin
  Debut := -100;
  Fin := -100;
  BaliseOuverture := '<' + NomSection + '>';
  BaliseFermeture := '</' + NomSection + '>';
  for i := Fposition downto Sections.Courant.Debut do
  begin
    if (Debut = -100) and (Liste[i] = BaliseOuverture) then
      Debut := i + 1;
    if (Fin = -100) and (Liste[i] = BaliseFermeture) then
      Fin := i - 1;
  end;
  if (Debut <> -100) and (Fin <> -100) then
  begin
    Sections.Push(Tsection.Create(NomSection, Debut, Fin));
    Fposition := Debut;
    Result := True;
  end
  else
    Result := False;
end;

procedure TSauvegarde.FermeSection;
begin
  if Sections.Count <> 1 then
  begin
    Fposition := Sections.Courant.Fin + 2;
    Sections.Pop;
  end
  else
    reset;
end;

procedure TSauvegarde.FermeSectionPuisPrecedent;
begin
  if Sections.Count <> 1 then
  begin
    Fposition := Sections.Courant.Debut - 2;
    Sections.Pop;
  end
  else
    reset;
end;

function TSauvegarde.GetLigne: string;
begin
  if Fposition <= Sections.Courant.Fin then
  begin
    Result := Liste[Fposition];
    Inc(Fposition);
  end
  else
    Result := '';
end;

function TSauvegarde.GetLigneBool: Boolean;
begin
  Result := GetLigne = '1';
end;

function TSauvegarde.GetLigneReel: double;
var
  Erreur: integer;
begin
  val(GetLigne, Result, Erreur);
end;

function TSauvegarde.GetLigneEntier: integer;
var
  Erreur: integer;
begin
  val(GetLigne, Result, Erreur);
  if Erreur <> 0 then
    Result := 0;
end;

procedure TSauvegarde.SetLigne(const Chaine: string);
begin
  if Fposition <= Sections.Courant.Fin then
  begin
    Liste[Fposition] := Chaine;
    Inc(Fposition);
  end;
end;

function TSauvegarde.GetLigneSansBouger: string;
begin
  Result := Liste[Fposition];
end;

procedure TSauvegarde.AddStrings(S: TSauvegarde);
var
  i: integer;
begin
  Liste.Capacity := Liste.Count + S.Liste.Count;
  for i := 0 to S.Liste.Count - 1 do
    Liste.Add(S.Liste[i]);
  Sections.Courant.Fin := Liste.Count - 1;
end;

function TSauvegarde.EOF: Boolean;
begin
  Result := Fposition = Liste.Count;
end;

procedure TSauvegarde.setList(const liste_: TStringList);
begin
  Liste.Assign(liste_);
  reset();
end;

function TSauvegarde.SuivantSiEgal(const Valeur: string): Boolean;
begin
  if GetLigneSansBouger <> Valeur then
    Exit(False)
  else
  begin
    Suivant;
    Result := True;
  end;
end;

procedure TSauvegarde.Clear;
begin
  reset;
  Liste.Clear;
end;

end.

