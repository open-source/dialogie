unit HelpId;
interface
const
HelpID_Avant_de_commencer = 2;
HelpID_Introduction = 5;
HelpID_Configuration = 6;
HelpID_Tutorial = 12;
HelpID_Description_detaillee = 13;
HelpID_Saisie = 39;
HelpID_Projet = 15;
HelpID_Batiment__par_coefficient_ = 16;
HelpID_Batiment__Maison___saisie_descriptive_ = 17;
HelpID_Batiment__Appartement___saisie_descriptive_ = 0;
HelpID_Batiment__Saisie_par_parois_ = 18;
HelpID_Batiment__Saisie_graphique_ = 19;
HelpID_Chauffage = 20;
HelpID_ECS = 21;
HelpID_Cuisine__par_description_ = 23;
HelpID_Appareillage__par_ratios_ = 24;
HelpID_Appareillage__par_description_ = 25;
HelpID_Eclairage = 26;
HelpID_Energies = 27;
HelpID_Resultats = 40;
HelpID_Synthese = 28;
HelpID_Details = 29;
HelpID_Environnement = 30;
HelpID_Graphique_mensuel = 31;
HelpID_Variantes = 32;
HelpID_Repartition = 33;
HelpID_Sankey = 34;
HelpID_Edition_de_rapport = 35;
HelpID_Analyse = 41;
HelpID_Saisie_des_factures = 36;
HelpID_Ajustement_des_calculs = 37;
HelpID_Actions = 38;
HelpID_Bibliotheques = 42;
HelpID_EIE = 43;
HelpID_Meteorologie = 44;
HelpID_Compositions = 45;
HelpID_Menuiseries = 46;
HelpID_Algorithmes = 14;
implementation
end.
