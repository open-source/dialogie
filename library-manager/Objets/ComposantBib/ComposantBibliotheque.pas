unit ComposantBibliotheque;

interface

uses
  Classes,
  GestionSauvegarde,
  ComCtrls,
  CreerSauvegarde,
  Graphics,
  IzObjectPersist, Xml.XMLIntf;

type
  TDouzeReels = array [1 .. 12] of double;
  TDouzeChaines = array [1 .. 12] of string;

  TComposantBibliotheque = class(TObjectPersist)
  protected

    procedure innerLoadXml(node : IXMLNode); override;
    procedure innerSaveXml(node : IXMLNode); override;


  public
    categorie, nom, Complement, Source : string;
    Identificateur : integer;
    ReadOnly, Utilise : boolean;
    Image1, Image2 : string;

    function SauveDebut(var Contenu : TCreationSauvegarde) : boolean;
    function SauveFin(var Contenu : TCreationSauvegarde) : boolean;
    function ChargeDebut(var Contenu : TSauvegarde) : boolean;
    function ChargeFin(var Contenu : TSauvegarde) : boolean;
    procedure Assign(Composant : TComposantBibliotheque);
    function Sauve(var Contenu : TCreationSauvegarde) : boolean; virtual;
      abstract;
    function Charge(var Contenu : TSauvegarde; Numversion : integer) : boolean;
      virtual; abstract;
    constructor Create(Nom_ : string); overload;
    function ValeursIdentiques(comp : TComposantBibliotheque) : boolean; virtual;
      abstract;
    class function idVersion : integer; virtual; abstract;
  protected
    constructor Create(Nom_, categorie_, complement_, source_ : string);
      overload;
  end;

implementation

uses
  XmlUtil;

constructor TComposantBibliotheque.Create(Nom_ : string);
begin
  Create;
  nom := Nom_;
end;

constructor TComposantBibliotheque.Create(Nom_, categorie_, complement_,
  source_ : string);
begin
  inherited Create;
  categorie := categorie_;
  nom := Nom_;
  Complement := complement_;
  Source := source_;
end;

procedure TComposantBibliotheque.Assign(Composant : TComposantBibliotheque);
begin
  categorie := Composant.categorie;
  nom := Composant.nom;
  Complement := Composant.Complement;
  Source := Composant.Source;
  Identificateur := Composant.Identificateur;
  readonly := Composant.ReadOnly;
  Utilise := Composant.Utilise;
  Image1 := Composant.Image1;
  Image2 := Composant.Image2;
end;


procedure TComposantBibliotheque.innerLoadXml(node : IXMLNode);
begin
  nom := TXmlUtil.getTagString(node, 'nom');
  categorie := TXmlUtil.getTagString(node, 'categorie');
  if categorie = '' then
    categorie := '� classer';
  Complement := TXmlUtil.getTagString(node, 'complement');
  Source := TXmlUtil.getTagString(node, 'source');
  Identificateur := TXmlUtil.getTagInt(node, 'id');
  readonly := TXmlUtil.getTagBool(node, 'readonly');
end;

procedure TComposantBibliotheque.innerSaveXml(node : IXMLNode);
begin
  TXmlUtil.setTag(node, 'nom', nom);
  TXmlUtil.setTag(node, 'categorie', categorie);
  TXmlUtil.setTag(node, 'complement',Complement);
  TXmlUtil.setTag(node, 'source', Source);
  TXmlUtil.setTag(node, 'id', Identificateur);
  TXmlUtil.setTag(node, 'readonly', readonly);
end;


function TComposantBibliotheque.SauveDebut(var Contenu : TCreationSauvegarde) : boolean;
begin
  Contenu.Add(categorie);
  Contenu.Add(nom);
  Contenu.Add(Complement);
  Contenu.Add(Source);
  Result := True;
end;

function TComposantBibliotheque.SauveFin(var Contenu : TCreationSauvegarde)
  : boolean;
begin
  Contenu.Add(Identificateur);
  Contenu.Add(readonly);
  Result := True;
end;

function TComposantBibliotheque.ChargeDebut(var Contenu : TSauvegarde) : boolean;
begin
  categorie := Contenu.GetLigne;
  if categorie = '' then
    categorie := '� classer';
  nom := Contenu.GetLigne;
  Complement := Contenu.GetLigne;
  Source := Contenu.GetLigne;
  Result := True;
end;

function TComposantBibliotheque.ChargeFin(var Contenu : TSauvegarde) : boolean;
begin
  Identificateur := Contenu.GetLigneEntier;
  readonly := Contenu.GetLigneBool;
  Result := True;
end;

end.
