unit Menuiseries;

interface

uses
  // Delphi
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, Buttons, ComposantBibliotheque,
  GestionSauvegarde, CreerSauvegarde, Xml.XMLIntf;

type
  TMenuiseries = class(TComposantBibliotheque)
  private
    function GetConducMoy: double;
    function GetCoeffMoy: double;
  private const
    FIdVersion: Integer = 1;
  protected
    procedure innerLoadXml(node : IXMLNode); override;
    procedure innerSaveXml(node : IXMLNode); override;

  public
    pourcentG, Uf, Ug: double;
    FacteurSolaire0, FacteurSolaire1, FacteurSolaire2, FacteurSolaire3: double;
    Angle1, Angle2, Angle3: double;
    NbVitrage: Integer;
    TransmissionLumineuse: double;

    constructor Create();override;
    constructor Create(Nom_, categorie_, complement_, source_, ConducMoy_,
      CoeffMoy_, PourcentG_, ConducG_, ConducNG_, Coeff0_, Coeff1_, Coeff2_,
      Coeff3_, Angle1_, Angle2_, Angle3_: string; NbVitrage_: Integer;
      TransmissionLumineuse_: string); overload;
    function Sauve(var Contenu: TCreationSauvegarde): boolean; override;
    function Charge(var Contenu: TSauvegarde; NumVersion: Integer): boolean;
      override;
    procedure Assign(comp: TMenuiseries);
    procedure ConvertirPhoto(T: double);

    function isDoor(): boolean;
    function ValeursIdentiques(comp: TComposantBibliotheque): boolean; override;
    class function idVersion: Integer; override;

    property ConducMoy: double Read GetConducMoy;
    property coeffMoy: double Read GetCoeffMoy;
  end;

  TListeMenuiseries = class(TList)
  private
    function GetMenuiseries(const i: Integer): TMenuiseries;
    procedure SetMenuiseries(const i: Integer; const Valeur: TMenuiseries);
  public
    property objets[const Index: Integer]
      : TMenuiseries Read GetMenuiseries Write SetMenuiseries;
    default;
    procedure ClearAndFree;
    function IndexOf(Chaine: string): Integer; overload;
    procedure AddStrings(S: TListeMenuiseries);
    procedure Assign(S: TListeMenuiseries);
  end;

implementation

uses
  IzUtilitaires, XmlUtil;

class function TMenuiseries.idVersion: Integer;
begin
  result := FIdVersion;
end;

function TMenuiseries.ValeursIdentiques(comp: TComposantBibliotheque): boolean;
var
  Men: TMenuiseries;
begin
  Men := comp as TMenuiseries;
  result := pourcentG = Men.pourcentG;
  result := result and (Uf = Men.Uf);
  result := result and (Ug = Men.Ug);
  result := result and (FacteurSolaire0 = Men.FacteurSolaire0);
  result := result and (FacteurSolaire1 = Men.FacteurSolaire1);
  result := result and (FacteurSolaire2 = Men.FacteurSolaire2);
  result := result and (FacteurSolaire3 = Men.FacteurSolaire3);
  result := result and (Angle1 = Men.Angle1);
  result := result and (Angle2 = Men.Angle2);
  result := result and (Angle3 = Men.Angle3);
  result := result and (NbVitrage = Men.NbVitrage);
  result := result and (TransmissionLumineuse = Men.TransmissionLumineuse);
end;

procedure TListeMenuiseries.Assign(S: TListeMenuiseries);
begin
  Clear;
  AddStrings(S);
end;

procedure TListeMenuiseries.AddStrings(S: TListeMenuiseries);
var
  i: Integer;
begin
  Capacity := Count + S.Count;
  for i := 0 to S.Count - 1 do
    Add(S[i]);
end;

function TListeMenuiseries.IndexOf(Chaine: string): Integer;
begin
  for result := 0 to Count - 1 do
    if TMenuiseries(items[result]).nom = Chaine then
      Exit;
  result := -1;
end;

procedure TListeMenuiseries.ClearAndFree;
var
  i: Integer;
begin
  for i := 0 to Count - 1 do
    TMenuiseries(items[i]).Free;
  Clear;
end;

function TListeMenuiseries.GetMenuiseries(const i: Integer): TMenuiseries;
begin
  if i < Count then
    result := Self.items[i]
  else
    result := nil;
end;

procedure TListeMenuiseries.SetMenuiseries(const i: Integer;
  const Valeur: TMenuiseries);
begin
  Self.items[i] := Valeur;
end;

function TMenuiseries.GetConducMoy;
begin
  result := (Ug * pourcentG / 100) + (Uf * (100 - pourcentG) / 100);
end;

function TMenuiseries.GetCoeffMoy;
begin
  result := (FacteurSolaire0 * pourcentG / 100);
end;


constructor TMenuiseries.Create;
begin
  inherited;
  innerTagName := 'menuiserie';
end;

constructor TMenuiseries.Create(Nom_, categorie_, complement_, source_,
  ConducMoy_, CoeffMoy_, PourcentG_, ConducG_, ConducNG_, Coeff0_, Coeff1_,
  Coeff2_, Coeff3_, Angle1_, Angle2_, Angle3_: string; NbVitrage_: Integer;
  TransmissionLumineuse_: string);
var
  erreur: Integer;
begin
  inherited Create(Nom_, categorie_, complement_, source_);
  innerTagName := 'menuiserie';
  Val(ConvertPoint(ConducG_), Ug, erreur);
  Val(ConvertPoint(PourcentG_), pourcentG, erreur);
  Val(ConvertPoint(ConducNG_), Uf, erreur);
  Val(ConvertPoint(Coeff0_), FacteurSolaire0, erreur);
  Val(ConvertPoint(Coeff1_), FacteurSolaire1, erreur);
  Val(ConvertPoint(Coeff2_), FacteurSolaire2, erreur);
  Val(ConvertPoint(Coeff3_), FacteurSolaire3, erreur);
  Val(ConvertPoint(Angle1_), Angle1, erreur);
  Val(ConvertPoint(Angle2_), Angle2, erreur);
  Val(ConvertPoint(Angle3_), Angle3, erreur);
  if TransmissionLumineuse_ = '' then
    TransmissionLumineuse := FacteurSolaire0
  else
    Val(ConvertPoint(TransmissionLumineuse_), TransmissionLumineuse, erreur);
  NbVitrage := NbVitrage_;
end;

procedure TMenuiseries.Assign(comp: TMenuiseries);
begin
  inherited Assign(comp);
  pourcentG := comp.pourcentG;
  Uf := comp.Uf;
  Ug := comp.Ug;
  FacteurSolaire0 := comp.FacteurSolaire0;
  FacteurSolaire1 := comp.FacteurSolaire1;
  FacteurSolaire2 := comp.FacteurSolaire2;
  FacteurSolaire3 := comp.FacteurSolaire3;
  Angle1 := comp.Angle1;
  Angle2 := comp.Angle2;
  Angle3 := comp.Angle3;
  TransmissionLumineuse := comp.TransmissionLumineuse;
  NbVitrage := comp.NbVitrage;
end;

function TMenuiseries.isDoor(): boolean;
begin
  result := (NbVitrage = 0) or (pourcentG = 0);
end;

procedure TMenuiseries.ConvertirPhoto(T: double);
const
  TauxPV = 0.9;
  // coefficient de transmission du vitrage qui s�par� les cellules de l'ext�rieur
  AlphaPV = 0.95; // coefficient d'absorption des cellules PV
  Rendement = 0.10; // rendement photo�lectrique
  R1 = 0.0475;
var
  R2: double;
begin
  R2 := 1 / Ug - R1;
  FacteurSolaire0 := (1 - T) * (TauxPV * (AlphaPV - Rendement)) * R1 /
    (R1 + R2) + FacteurSolaire0 * T;
end;

function TMenuiseries.Sauve(var Contenu: TCreationSauvegarde): boolean;
begin
  result := inherited SauveDebut(Contenu);

  Contenu.Add(ConducMoy);
  Contenu.Add(coeffMoy);
  Contenu.Add(pourcentG);
  Contenu.Add(Uf);
  Contenu.Add(Ug);
  Contenu.Add(FacteurSolaire0);
  Contenu.Add(FacteurSolaire1);
  Contenu.Add(FacteurSolaire2);
  Contenu.Add(FacteurSolaire3);
  Contenu.Add(Angle1);
  Contenu.Add(Angle2);
  Contenu.Add(Angle3);
  Contenu.Add(NbVitrage);
  Contenu.Add('Transmission');
  Contenu.Add(TransmissionLumineuse);
  result := result and inherited SauveFin(Contenu);
end;

function TMenuiseries.Charge(var Contenu: TSauvegarde;
  NumVersion: Integer): boolean;
begin
  result := inherited ChargeDebut(Contenu);

  Contenu.Suivant;
  Contenu.Suivant;

  pourcentG := Contenu.GetLigneReel;
  Uf := Contenu.GetLigneReel;
  Ug := Contenu.GetLigneReel;
  FacteurSolaire0 := Contenu.GetLigneReel;
  FacteurSolaire1 := Contenu.GetLigneReel;
  FacteurSolaire2 := Contenu.GetLigneReel;
  FacteurSolaire3 := Contenu.GetLigneReel;
  Angle1 := Contenu.GetLigneReel;
  Angle2 := Contenu.GetLigneReel;
  Angle3 := Contenu.GetLigneReel;
  NbVitrage := Contenu.GetLigneEntier;
  if Contenu.SuivantSiEgal('Transmission') then
    TransmissionLumineuse := Contenu.GetLigneReel;

  result := result and inherited ChargeFin(Contenu);
end;

procedure TMenuiseries.innerLoadXml(node : IXMLNode);
begin
  inherited innerLoadXml(node);
  pourcentG := TXmlUtil.getTagDouble(node, 'pct-vitrage');
  Uf := TXmlUtil.getTagDouble(node, 'u-f');
  Ug := TXmlUtil.getTagDouble(node, 'u-g');
  FacteurSolaire0 := TXmlUtil.getTagDouble(node, 'facteur-solaire-0');
  FacteurSolaire1 := TXmlUtil.getTagDouble(node, 'facteur-solaire-1');
  FacteurSolaire2 := TXmlUtil.getTagDouble(node, 'facteur-solaire-2');
  FacteurSolaire3 := TXmlUtil.getTagDouble(node, 'facteur-solaire-3');
  Angle1 := TXmlUtil.getTagDouble(node, 'angle-1');
  Angle2 := TXmlUtil.getTagDouble(node, 'angle-2');
  Angle3 := TXmlUtil.getTagDouble(node, 'angle-1');
  NbVitrage := TXmlUtil.getTagInt(node, 'nb-vitrage');
  TransmissionLumineuse := TXmlUtil.getTagDouble(node, 'transmission-luminieuse');
end;

procedure TMenuiseries.innerSaveXml(node : IXMLNode);
begin
  inherited innerSaveXml(node);
  TXmlUtil.setTag(node, 'conductivite-moy', ConducMoy);
  TXmlUtil.setTag(node, 'coeff-moy', coeffMoy);
  TXmlUtil.setTag(node, 'pct-vitrage', pourcentG);
  TXmlUtil.setTag(node, 'u-f', Uf);
  TXmlUtil.setTag(node, 'u-g', Ug);
  TXmlUtil.setTag(node, 'facteur-solaire-0', FacteurSolaire0);
  TXmlUtil.setTag(node, 'facteur-solaire-1', FacteurSolaire1);
  TXmlUtil.setTag(node, 'facteur-solaire-2', FacteurSolaire2);
  TXmlUtil.setTag(node, 'facteur-solaire-3', FacteurSolaire3);
  TXmlUtil.setTag(node, 'angle-1', Angle1);
  TXmlUtil.setTag(node, 'angle-2', Angle2);
  TXmlUtil.setTag(node, 'angle-1', Angle3);
  TXmlUtil.setTag(node, 'nb-vitrage', NbVitrage);
  TXmlUtil.setTag(node, 'transmission-luminieuse', TransmissionLumineuse);
end;

end.
