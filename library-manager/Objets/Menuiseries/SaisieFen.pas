unit SaisieFen;

interface

uses
  // Delphi
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, ComCtrls;

const
  FichierCadre         = 'cadres.txt';
  FichierPourcentClair = 'pourcentclair.txt';

type
  TFormSaisieFen = class(TForm)
    GroupBox1:  TGroupBox;
    Label2:     TLabel;
    GroupBox2:  TGroupBox;
    Label1:     TLabel;
    Label3:     TLabel;
    Label4:     TLabel;
    Label5:     TLabel;
    Label6:     TLabel;
    Label7:     TLabel;
    EditAngle0: TEdit;
    EditAngle1: TEdit;
    EditAngle2: TEdit;
    EditAngle3: TEdit;
    Label8:     TLabel;
    Label9:     TLabel;
    Label10:    TLabel;
    Label11:    TLabel;
    EditCoeff0: TEdit;
    EditCoeff1: TEdit;
    EditCoeff2: TEdit;
    EditCoeff3: TEdit;
    ConducG:    TEdit;
    Label12:    TLabel;
    GroupBox3:  TGroupBox;
    Image2:     TImage;
    Image1:     TImage;
    BitBtn1:    TBitBtn;
    BitBtn2:    TBitBtn;
    Label13:    TLabel;
    BitBtn3:    TBitBtn;
    PourcentG2: TComboBox;
    ConducNG:   TEdit;
    ConducNG2:  TComboBox;
    PourcentG:  TEdit;
    procedure FormShow(Sender: TObject);
    procedure ConducNG2Change(Sender: TObject);
    procedure PourcentG2Change(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    NbDeVitrages:      integer;
    MessagesInterface: TStringList;
    procedure Controle;
    procedure InitialiseListes(Chemin: string);
  end;

var
  FormSaisieFen: TFormSaisieFen;

implementation

uses
  System.UITypes,
  IzUtilitaires,
  DialogsInternational;

{$R *.DFM}

procedure TFormSaisieFen.InitialiseListes(Chemin: string);
begin
  ConducNG2.Items.LoadFromFile(Chemin + FichierCadre);
  PourcentG2.Items.LoadFromFile(Chemin + FichierPourcentClair);
end;

procedure TFormSaisieFen.Controle;
var
  C1, C2, C3, C4, C5, C6, C7, C8, C9, C10: string;
begin
  ModalResult := mrNone;
  C1  := ConvertPoint(formSaisieFen.conducNG.Text);
  C2  := ConvertPoint(formSaisieFen.conducG.Text);
  C3  := ConvertPoint(formSaisieFen.PourcentG.Text);
  C4  := ConvertPoint(formSaisieFen.EditAngle1.Text);
  C5  := ConvertPoint(formSaisieFen.EditAngle2.Text);
  C6  := ConvertPoint(formSaisieFen.EditAngle3.Text);
  C7  := ConvertPoint(formSaisieFen.EditCoeff0.Text);
  C8  := ConvertPoint(formSaisieFen.EditCoeff1.Text);
  C9  := ConvertPoint(formSaisieFen.EditCoeff2.Text);
  C10 := ConvertPoint(formSaisieFen.EditCoeff3.Text);
  if (C1 = '') or (ControlNum(C1, 0, 10000, 'R') <> C1) then
  begin
    FocusControl(formSaisieFen.conducNG);
    MessageDlgM(MessagesInterface[0], mtWarning, [mbOK], 0);
    Exit;
  end
  else if (C2 = '') or (ControlNum(C2, 0, 10000, 'R') <> C2) then
  begin
    FocusControl(conducG);
    MessageDlgM(MessagesInterface[0], mtWarning, [mbOK], 0);
    Exit;
  end
  else if (C3 = '') or (ControlNum(C3, 0, 100, 'R') <> C3) then
  begin
    FocusControl(PourcentG);
    MessageDlgM(MessagesInterface[1], mtWarning, [mbOK], 0);
    Exit;
  end
  else if (C7 = '') or (ControlNum(C7, 0, 1, 'R') <> C7) then
  begin
    FocusControl(Editcoeff0);
    MessageDlgM(MessagesInterface[2], mtWarning, [mbOK], 0);
    Exit;
  end
  else if EditCoeff1.Enabled then
  begin
    if (C4 = '') or (ControlNum(C4, 1, 89, 'R') <> C4) then
    begin
      FocusControl(EditAngle1);
      MessageDlgM(MessagesInterface[3], mtWarning, [mbOK], 0);
      Exit;
    end
    else if (C5 = '') or (ControlNum(C5, 1, 89, 'R') <> C5) then
    begin
      FocusControl(EditAngle2);
      MessageDlgM(MessagesInterface[3], mtWarning, [mbOK], 0);
      Exit;
    end
    else if (C6 = '') or (ControlNum(C6, 1, 89, 'R') <> C6) then
    begin
      FocusControl(EditAngle3);
      MessageDlgM(MessagesInterface[3], mtWarning, [mbOK], 0);
      Exit;
    end
    else if (C8 = '') or (ControlNum(C8, 0, 1, 'R') <> C8) then
    begin
      FocusControl(EditCoeff1);
      MessageDlgM(MessagesInterface[2], mtWarning, [mbOK], 0);
      Exit;
    end
    else if (C9 = '') or (ControlNum(C9, 0, 1, 'R') <> C9) then
    begin
      FocusControl(EditCoeff2);
      MessageDlgM(MessagesInterface[2], mtWarning, [mbOK], 0);
      Exit;
    end
    else if (C10 = '') or (ControlNum(C10, 0, 1, 'R') <> C10) then
    begin
      FocusControl(EditCoeff3);
      MessageDlgM(MessagesInterface[2], mtWarning, [mbOK], 0);
      Exit;
    end;
  end;
  ModalResult := mrOk;
end;

procedure TFormSaisieFen.FormCreate(Sender: TObject);
begin
  MessagesInterface := TStringList.Create;
end;

procedure TFormSaisieFen.FormDestroy(Sender: TObject);
begin
  MessagesInterface.Free;
end;

procedure TFormSaisieFen.FormShow(Sender: TObject);
begin
  if NbDeVitrages = 4 then
  begin
    Label5.Enabled  := True;
    Label6.Enabled  := True;
    Label7.Enabled  := True;
    Label9.Enabled  := True;
    Label10.Enabled := True;
    Label11.Enabled := True;
    EditAngle1.color := clWindow;
    EditAngle2.color := clWindow;
    EditAngle3.color := clWindow;
    EditCoeff1.color := clWindow;
    EditCoeff2.color := clWindow;
    EditCoeff3.color := clWindow;
    EditAngle1.Enabled := True;
    EditAngle2.Enabled := True;
    EditAngle3.Enabled := True;
    EditCoeff1.Enabled := True;
    EditCoeff2.Enabled := True;
    EditCoeff3.Enabled := True;
  end
  else
  begin
    Label5.Enabled  := False;
    Label6.Enabled  := False;
    Label7.Enabled  := False;
    Label9.Enabled  := False;
    Label10.Enabled := False;
    Label11.Enabled := False;
    EditAngle1.color := clbtnface;
    EditAngle2.color := clbtnface;
    EditAngle3.color := clbtnface;
    EditCoeff1.color := clbtnface;
    EditCoeff2.color := clbtnface;
    EditCoeff3.color := clbtnface;
    EditAngle1.Enabled := False;
    EditAngle2.Enabled := False;
    EditAngle3.Enabled := False;
    EditCoeff1.Enabled := False;
    EditCoeff2.Enabled := False;
    EditCoeff3.Enabled := False;
  end;
end;

procedure TFormSaisieFen.PourcentG2Change(Sender: TObject);
var
  chaine: string;
begin
  Chaine := PourcentG2.Text;
  Delete(Chaine, 1, Length(Chaine) - 2);
  PourcentG.Text := Chaine;
end;

procedure TFormSaisieFen.BitBtn2Click(Sender: TObject);
begin
  Controle;
end;

procedure TFormSaisieFen.ConducNG2Change(Sender: TObject);
var
  chaine: string;
begin
  Chaine := ConducNG2.Text;
  Delete(Chaine, 1, Length(Chaine) - 3);
  ConducNG.Text := Chaine;
end;

end.
