unit NCadreMenuiseries;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, CadreBib, Menus, BarreAdresse, StdCtrls, Buttons, ExtCtrls, ComCtrls,
  ComposantBibliotheque;

const
  FichierCadre = 'cadres.txt';
  FichierPourcentClair = 'pourcentclair.txt';

type
  TFrameMenuiseries = class(TFrameBib)
    GroupBoxVitrage: TGroupBox;
    zLabel86: TLabel;
    Label1: TLabel;
    EditUVitrage: TEdit;
    zGroupBox19: TGroupBox;
    zLabel87: TLabel;
    Label4: TLabel;
    EditUCadre: TEdit;
    GroupOptique: TGroupBox;
    LabelTransmission: TLabel;
    EditTransmission: TEdit;
    ConducNG2: TComboBox;
    zLabel84: TLabel;
    Label3: TLabel;
    EditPourcentageVitrage: TEdit;
    PourcentG2: TComboBox;
    Panel2: TPanel;
    zLabel82: TLabel;
    zLabel83: TLabel;
    Label2: TLabel;
    EditFacteurSolaireMoyen: TEdit;
    EditCoeffUMoyen: TEdit;
    PanelFS0: TPanel;
    LabelFacteurSolaire0: TLabel;
    EditFacteurSolaire0: TEdit;
    EditAngle0: TEdit;
    LabelDegre0: TLabel;
    ComboNbVitrage: TComboBox;
    zLabel81: TLabel;
    PanelFS123: TPanel;
    LabelFacteurSolaire1: TLabel;
    EditFacteurSolaire1: TEdit;
    EditAngle1: TEdit;
    LabelDegre1: TLabel;
    LabelFacteurSolaire2: TLabel;
    EditFacteurSolaire2: TEdit;
    EditAngle2: TEdit;
    LabelDegre2: TLabel;
    LabelFacteurSolaire3: TLabel;
    EditFacteurSolaire3: TEdit;
    EditAngle3: TEdit;
    LabelDegre3: TLabel;
    Image2: TImage;
    Image1: TImage;
    procedure BitBtnNewClick(Sender: TObject);
    procedure ComboCategorieChange(Sender: TObject);
    procedure ConducNG2Change(Sender: TObject);
    procedure PourcentG2Change(Sender: TObject);
    procedure ComboNbVitrageChange(Sender: TObject);
  private
    { Déclarations privées }
    procedure RemplirMoyenne;
  protected
    function Controle: boolean; override;
  public
    { Déclarations publiques }
    Constructor Create(Aowner : TComponent);
    function CreerComposantBiblio: TComposantBibliotheque; override;
    procedure Affiche(comp: TComposantBibliotheque; Survol: boolean); override;
    procedure InitialiseListe(Chemin: string);
  end;

var
  FrameMenuiseries: TFrameMenuiseries;

implementation

uses
  Menuiseries, BiblioMenuiseries, Constantes, Utilitaires, dialogsinternational;
{$R *.dfm}

Constructor TFrameMenuiseries.Create(Aowner : TComponent);
begin
  inherited Create(AOwner);
end;

procedure TFrameMenuiseries.BitBtnNewClick(Sender: TObject);
var
  Ok: boolean;
begin
  if Modif then
    Ok := ControleSauve(False)
  else
    Ok := True;
  if Ok then
  begin
    EditFacteurSolaireMoyen.Text := '';
    EditCoeffUMoyen.Text := '';
    EditUCadre.Text := '';
    EditPourcentageVitrage.Text := '';
    EditUVitrage.Text := '';
    ComboNbVitrage.ItemIndex := 2;
    ComboNbVitrage.Onchange(Self);
    EditFacteurSolaire0.Text := '';
    EditFacteurSolaire1.Text := '';
    EditFacteurSolaire2.Text := '';
    EditFacteurSolaire3.Text := '';
    EditAngle1.Text := '';
    EditAngle2.Text := '';
    EditAngle3.Text := '';
    inherited;
  end;
end;

function TFrameMenuiseries.CreerComposantBiblio: TComposantBibliotheque;
begin
  Result := TMenuiseries.Create(EditNom.Text, BarreCategorie.Chemin,
    MComplement, EditSource.Text, EditCoeffUMoyen.Text,
    EditFacteurSolaireMoyen.Text, EditPourcentageVitrage.Text,
    EditUVitrage.Text, EditUCadre.Text, EditFacteurSolaire0.Text,
    EditFacteurSolaire1.Text, EditFacteurSolaire2.Text,
    EditFacteurSolaire3.Text, EditAngle1.Text, EditAngle2.Text,
    EditAngle3.Text, ComboNbVitrage.ItemIndex, EditTransmission.Text);
end;

procedure TFrameMenuiseries.ComboNbVitrageChange(Sender: TObject);
begin
  inherited;
  GroupBoxVitrage.Visible := ComboNbVitrage.ItemIndex <> 0;
  GroupOptique.Visible := ComboNbVitrage.ItemIndex <> 0;
  PanelFS123.Visible := ComboNbVitrage.ItemIndex = 4;
  ComboCategorieChange(Self);
end;

procedure TFrameMenuiseries.Affiche(comp: TComposantBibliotheque;
  Survol: boolean);
begin
  inherited Affiche(comp, Survol);
  if comp <> Nil then
    with comp as TMenuiseries do
    begin
      EditCoeffUMoyen.Text := FormatFloat('0.##', ConducMoy, tfs);
      EditFacteurSolaireMoyen.Text := FormatFloat('0.##', CoeffMoy, tfs);
      EditPourcentageVitrage.Text := FormatFloat('0.##', pourcentG, tfs);
      EditUVitrage.Text := FormatFloat('0.##', Ug, tfs);
      EditUCadre.Text := FormatFloat('0.##', Uf, tfs);
      EditFacteurSolaire0.Text := FormatFloat('0.##', FacteurSolaire0, tfs);
      EditFacteurSolaire1.Text := FormatFloat('0.##', FacteurSolaire1, tfs);
      EditFacteurSolaire2.Text := FormatFloat('0.##', FacteurSolaire2, tfs);
      EditFacteurSolaire3.Text := FormatFloat('0.##', FacteurSolaire3, tfs);
      EditAngle1.Text := FormatFloat('0', Angle1, tfs);
      EditAngle2.Text := FormatFloat('0', Angle2, tfs);
      EditAngle3.Text := FormatFloat('0', Angle3, tfs);
      ComboNbVitrage.ItemIndex := NbVitrage;
      EditTransmission.Text := FormatFloat('0.##', TransmissionLumineuse, tfs);
      ComboNbVitrage.Onchange(Self);
    end
    else
    begin
      EditCoeffUMoyen.Text := '';
      EditFacteurSolaireMoyen.Text := '';
      EditPourcentageVitrage.Text := '';
      EditUVitrage.Text := '';
      EditUCadre.Text := '';
      EditFacteurSolaire0.Text := '';
      EditFacteurSolaire1.Text := '';
      EditFacteurSolaire2.Text := '';
      EditFacteurSolaire3.Text := '';
      EditAngle1.Text := '';
      EditAngle2.Text := '';
      EditAngle3.Text := '';
      ComboNbVitrage.ItemIndex := 0;
      EditTransmission.Text := '';
      ComboNbVitrage.Onchange(Self);
    end;
  ModeAffichage := False;
end;

procedure TFrameMenuiseries.ComboCategorieChange(Sender: TObject);
begin
  inherited;
  RemplirMoyenne;
end;

procedure TFrameMenuiseries.ConducNG2Change(Sender: TObject);
var
  chaine: string;
begin
  inherited;
  chaine := ConducNG2.Text;
  Delete(chaine, 1, Length(chaine) - 3);
  EditUCadre.Text := chaine;
  ComboCategorieChange(Self);
end;

function TFrameMenuiseries.Controle: boolean;
begin
  ErreurSaisie := 0;
  AOuvrir := Nil;
  if not inherited Controle then
    Exit(False);
  if HorsLimite(ComboNbVitrage) then
    ErreurSaisie := 4
  else if HorsLimite(EditUCadre, 0.001, Maxlongint, 'R') then
    ErreurSaisie := 5;

  if (ErreurSaisie = 0) and (ComboNbVitrage.ItemIndex <> 0) then
  begin
    if HorsLimite(EditUVitrage, 0.001, Maxlongint, 'R') then
      ErreurSaisie := 5
    else if HorsLimite(EditPourcentageVitrage, 0, 100, 'R') then
      ErreurSaisie := 6
    else if HorsLimite(EditFacteurSolaire0, 0, 1, 'R') then
      ErreurSaisie := 7;

    if (ErreurSaisie = 0) and (ComboNbVitrage.ItemIndex = 4) then
    begin
      if HorsLimite(EditFacteurSolaire1, 0, 1, 'R') then
        ErreurSaisie := 7
      else if HorsLimite(EditFacteurSolaire2, 0, 1, 'R') then
        ErreurSaisie := 7
      else if HorsLimite(EditFacteurSolaire3, 0, 1, 'R') then
        ErreurSaisie := 7
      else if HorsLimite(EditAngle1, 1, 89, 'E') then
        ErreurSaisie := 8
      else if HorsLimite(EditAngle2, 1, 89, 'E') then
        ErreurSaisie := 8
      else if HorsLimite(EditAngle3, 1, 89, 'E') then
        ErreurSaisie := 8;
    end;
  end;

  if ErreurSaisie <> 0 then
  begin
    MessageDlgM(MessagesInterface[8 + ErreurSaisie], mtWarning, [mbOK], 0);
    Result := False;
  end
  else
    Result := True;
  if AOuvrir <> Nil then
    AOuvrir.DroppedDown := True;
end;

procedure TFrameMenuiseries.RemplirMoyenne;
var
  Erreur1, Erreur2, Erreur3, erreur4: integer;
  Ucadre, UVitrage, PourcentVitrage, FacteurSolaire: double;
  UMoyen, FacteurMoyen: double;
begin
  Val(EditUCadre.Text, Ucadre, Erreur1);
  Val(EditUVitrage.Text, UVitrage, Erreur2);
  Val(EditPourcentageVitrage.Text, PourcentVitrage, Erreur3);
  Val(EditFacteurSolaire0.Text, FacteurSolaire, erreur4);
  if (Erreur1 = 0) and (Erreur2 = 0) and (Erreur3 = 0) and (erreur4 = 0) then
  begin
    if ComboNbVitrage.ItemIndex = 0 then
    begin
      FacteurMoyen := 0;
      UMoyen := Ucadre;
    end
    else
    begin
      UMoyen := (UVitrage * PourcentVitrage / 100) +
        (Ucadre * (100 - PourcentVitrage) / 100);
      FacteurMoyen := FacteurSolaire * PourcentVitrage / 100;
    end;
    EditCoeffUMoyen.Text := FormatFloat('0.##', UMoyen, tfs);
    EditFacteurSolaireMoyen.Text := FormatFloat('0.##', FacteurMoyen, tfs)
  end
  else
  begin
    EditCoeffUMoyen.Text := '';
    EditFacteurSolaireMoyen.Text := '';
  end;
end;

procedure TFrameMenuiseries.InitialiseListe(Chemin: string);
begin
  ConducNG2.Items.LoadFromFile(Chemin + FichierCadre);
  PourcentG2.Items.LoadFromFile(Chemin + FichierPourcentClair);
end;

procedure TFrameMenuiseries.PourcentG2Change(Sender: TObject);
var
  chaine: string;
begin
  chaine := PourcentG2.Text;
  Delete(chaine, 1, Length(chaine) - 2);
  EditPourcentageVitrage.Text := chaine;
  ComboCategorieChange(Self);
end;

end.
