unit CadreMenuiseries;

interface

uses
  Messages,
  SysUtils,
  Variants,
  Classes,
  Graphics,
  Controls,
  Forms,
  Dialogs,
  StdCtrls,
  Buttons,
  ExtCtrls,
  ComCtrls,
  Menus,
  Menuiseries;

type
  TFrameMenuiseries = class(TFrame)
    zGroupBox16:   TGroupBox;
    Tree:          TTreeView;
    zGroupBox17:   TGroupBox;
    zLabel77:      TLabel;
    zLabel78:      TLabel;
    zLabel79:      TLabel;
    zLabel80:      TLabel;
    zLabel81:      TLabel;
    zLabel82:      TLabel;
    zLabel83:      TLabel;
    zLabel84:      TLabel;
    Label2:        TLabel;
    Label3:        TLabel;
    Categorie:     TComboBox;
    Nom:           TEdit;
    Complement:    TEdit;
    Source:        TEdit;
    zPanel12:      TPanel;
    BitBtnSaveFen: TBitBtn;
    BitBtnExpFen:  TBitBtn;
    BitBtnNewFen:  TBitBtn;
    NGGlazing:     TComboBox;
    BitBtnCreerClasse: TBitBtn;
    CoeffFen:      Tedit;
    ConducFen:     Tedit;
    PourcentFen:   Tedit;
    zGroupBox18:   TGroupBox;
    zLabel85:      TLabel;
    zLabel86:      TLabel;
    Label1:        TLabel;
    ConducFenVitr: Tedit;
    EditCoeff0:    Tedit;
    zGroupBox19:   TGroupBox;
    zLabel87:      TLabel;
    Label4:        TLabel;
    ConducFenOp:   Tedit;
    BitBtnChange:  TBitBtn;
    EditAngle1:    TEdit;
    EditCoeff1:    TEdit;
    EditCoeff2:    TEdit;
    EditAngle2:    TEdit;
    EditCoeff3:    TEdit;
    EditAngle3:    TEdit;
    PopupComponent: TPopupMenu;
    Open1:         TMenuItem;
    Delete1:       TMenuItem;
    Export1:       TMenuItem;
    Rename1:       TMenuItem;
    GroupOptique:  TGroupBox;
    LabelTransmission: TLabel;
    EditTransmission: TEdit;
    Splitter1: TSplitter;
    procedure TreeChange(Sender: TObject; Node: TTreeNode);
    procedure TreeClick(Sender: TObject);
    procedure TreeDragDrop(Sender, Source: TObject; X, Y: integer);
    procedure TreeDragOver(Sender, Source: TObject; X, Y: integer;
      State: TDragState; var Accept: boolean);
    procedure TreeKeyUp(Sender: TObject; var Key: word; Shift: TShiftState);
    procedure TreeMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: integer);
    procedure BitBtnCreerClasseClick(Sender: TObject);
    procedure CategorieChange(Sender: TObject);
    procedure BitBtnChangeClick(Sender: TObject);
    procedure BitBtnNewFenClick(Sender: TObject);
    procedure BitBtnSaveFenClick(Sender: TObject);
    procedure Open1Click(Sender: TObject);
    procedure Delete1Click(Sender: TObject);
    constructor Create(AOwner: TComponent); override;
    procedure BitBtnSaveFenMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: integer);
    destructor Destroy; override;
    procedure Rename1Click(Sender: TObject);
    procedure PopupComponentPopup(Sender: TObject);
  private
    { Déclarations privées }
    ModifMenuiseries: boolean;

    function ControleSauve(Forcee: boolean): boolean;
  protected
    Dragage:       TTreeNode;
    SauveReadOnly: boolean;
    function Controle: boolean;
  public
    { Déclarations publiques }
    ModificationAutorise: boolean;
    FormParent:        TForm;
    CheckHint:         boolean;
    MessagesInterface: TStringList;
    function AccepteDrag: boolean;
    procedure EffaceDragage;
    procedure Affiche(Fen: TMenuiseries);
  end;

implementation

uses
  System.UITypes,
  BiblioMenuiseries,
  SaisieFen,
  DialogsInternational,
  ComposantBibliotheque,
  IzApp,
  Windows,
  IzConstantes,
  IzUtilitaires;

{$R *.dfm}

destructor TFrameMenuiseries.Destroy;
begin
  MessagesInterFace.Free;
  inherited Destroy;
end;

function TFrameMenuiseries.AccepteDrag: boolean;
begin
  Result := (BibMenuiseries.ImageIndex = dragage.ImageIndex) or
    (BibMenuiseries.ImageIndexPorte = dragage.ImageIndex) or
    (dragage.ImageIndex = BibMenuiseries.ImageIndexDossier) or
    (Dragage.ImageIndex = BibMenuiseries.ImageIndexDossier);
end;

procedure TFrameMenuiseries.Affiche(Fen: TMenuiseries);
var
  Chaine: string;
  indice: integer;
begin
  BibMenuiseries.ConvertitCategorie(Fen);
  indice := Categorie.Items.IndexOf(Fen.Categorie);
  if Indice = -1 then
  begin
    BibMenuiseries.AjouteCategorie(Fen.Categorie, Categorie.items, Tree);
    indice := Categorie.Items.IndexOf(Fen.Categorie);
  end;
  Categorie.ItemIndex := Indice;

  Nom.Text := Fen.Nom;
  Complement.Text := Fen.Complement;
  Source.Text := Fen.Source;
  Str(Fen.ConducMoy: 0: 2, Chaine);
  ConducFen.Text := Chaine;
  Str(Fen.CoeffMoy: 0: 2, Chaine);
  CoeffFen.Text := Chaine;
  Str(Fen.PourcentG: 0: 0, Chaine);
  PourcentFen.Text := Chaine;
  Str(Fen.Ug: 0: 2, Chaine);
  ConducFenVitr.Text := Chaine;
  Str(Fen.Uf: 0: 2, Chaine);
  ConducFenOp.Text := Chaine;
  Str(Fen.FacteurSolaire0: 0: 2, Chaine);
  EditCoeff0.Text := Chaine;
  Str(Fen.FacteurSolaire1: 0: 2, Chaine);
  EditCoeff1.Text := Chaine;
  Str(Fen.FacteurSolaire2: 0: 2, Chaine);
  EditCoeff2.Text := Chaine;
  Str(Fen.FacteurSolaire3: 0: 2, Chaine);
  EditCoeff3.Text := Chaine;
  Str(Fen.Angle1: 0: 0, Chaine);
  EditAngle1.Text := Chaine;
  Str(Fen.Angle2: 0: 0, Chaine);
  EditAngle2.Text := Chaine;
  Str(Fen.Angle3: 0: 0, Chaine);
  EditAngle3.Text  := Chaine;
  NGGlazing.ItemIndex := Fen.nbVitrage;
  EditTransmission.text := FormatFloat('0.00',Fen.TransmissionLumineuse,tfs);
  ModifMenuiseries := False;
end;

constructor TFrameMenuiseries.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  SauveReadOnly := False;
  ModificationAutorise := True;
  CheckHint  := True;
  FormParent := nil;
  MessagesInterFace := TStringList.Create;
end;

procedure TFrameMenuiseries.EffaceDragage;
begin
  //Cas des catégories
  if (Dragage.ImageIndex = BibMenuiseries.ImageIndexDossier) or
    (Dragage.ImageIndex = BibMenuiseries.ImageIndexDossierS) then
  begin
    if dragage.HasChildren then
      MessageDlgM(MessagesInterface[0], mtWarning, [mbOK], 0)
    else
    begin
      Categorie.items.Delete(Categorie.Items.IndexOf(dragage.Text));
      Tree.items.Delete(dragage);
    end;
  end
  else if (Dragage.ImageIndex = BibMenuiseries.ImageIndex) or
    (Dragage.ImageIndex = BibMenuiseries.ImageIndexPorte)
    or (TApp.isMaster and (Dragage.ImageIndex = BibMenuiseries.ImageIndexRO ))
    or (TApp.isMaster and (Dragage.ImageIndex = BibMenuiseries.ImageIndexPorteRO ))
  then
  begin
    if (MessageDlgM(MessagesInterface[1] + dragage.Text + MessagesInterface[2],
      mtConfirmation, mbOkCancel, 0) = mrOk) and
      BibMenuiseries.Efface(dragage.Text) then
      begin
        Tree.Items.Delete(dragage);
        BibMenuiseries.SauveListe;
      end;
  end;
end;

procedure TFrameMenuiseries.Open1Click(Sender: TObject);
var
  Noeud: TTreeNode;
  Fen: TMenuiseries;
  Ok:  boolean;
begin
  if ModifMenuiseries then
    Ok := ControleSauve(False)
  else
    Ok := True;
  if not Ok then
    Exit;
  Fen := nil;
  Noeud := Tree.Selected;
  if Noeud <> nil then
    Fen := BibMenuiseries.Get(Noeud.Text);
  if Fen <> nil then
    Affiche(Fen);
end;

procedure TFrameMenuiseries.PopupComponentPopup(Sender: TObject);
begin
  Rename1.Enabled := Tree.Selected.ImageIndex <= 1;
  Open1.Enabled := not Rename1.Enabled;
end;

procedure TFrameMenuiseries.Rename1Click(Sender: TObject);
var
  NouveauNom: string;
  Noeud: TTreeNode;
begin
  if Dragage = nil then
    Exit;
  NouveauNom := Dragage.Text;
  if InputQueryM(MessagesInterface[6], MessagesInterface[6], NouveauNom) then
  begin
    dragage.Text := NouveauNom;
    Noeud := dragage.GetFirstChild;
    while Noeud <> nil do
    begin
      TComposantBibliotheque(Noeud.Data).categorie := NouveauNom;
      Noeud := Noeud.getNextSibling;
    end;
  end;
end;

function TFrameMenuiseries.ControleSauve(Forcee: boolean): boolean;
var
  reponse: word;
  comp:  TMenuiseries;
  Silencieux: boolean;
  Noeud: TTreeNode;
begin
  Result := False;
  if not ModificationAutorise then
  begin
    MessageDlgM(MessagesInterface[7], mtWarning, [mbOK], 0);
    Exit;
  end;
  FormParent.FocusControl(Tree);
  if not Forcee then
    Reponse := MessageDlgM(MessagesInterface[5] + Nom.Text + '''',
      mtWarning, [mbCancel, mbNo, mbYes], 0)
  else
    Reponse := mrYes;
  if Reponse = mrCancel then
    Exit;
  Result := True;
  if (Reponse = mrYes) then
    if Controle then
    begin
      comp := TMenuiseries.Create(Nom.Text, Categorie.Text,
        Complement.Text, Source.Text, ConducFen.Text, CoeffFen.Text,
        PourcentFen.Text, ConducFenVitr.Text, ConducFenOp.Text,
        EditCoeff0.Text, EditCoeff1.Text, EditCoeff2.Text, EditCoeff3.Text,
        EditAngle1.Text, EditAngle2.Text, EditAngle3.Text,
        NGGlazing.ItemIndex, EditTransmission.Text);
      comp.ReadOnly := CtrlDown and ShiftDown;
      Silencieux := False;
      if BibMenuiseries.Ajoute(comp, Silencieux) then
      begin
        Noeud := BibMenuiseries.PoseDansArbre(comp, Tree, True, True);
        Tree.Selected := Noeud;
      end
      else
        comp.Free;
    end
    else
      Result := False;
  ModifMenuiseries := False;
  BibMenuiseries.SauveListe;
end;


procedure TFrameMenuiseries.Delete1Click(Sender: TObject);
var
  I: integer;
  TreeSource: TCustomTreeView;
begin
  if Dragage = nil then
    Exit;
  TreeSource := Dragage.TreeView;
  for I := TreeSource.Selectioncount - 1 downto 0 do
  begin
    Dragage := TreeSource.Selections[i];
    EffaceDragage;
  end;
end;

function TFrameMenuiseries.Controle: boolean;
var
  Erreur: integer;
begin
  if EstNumerique(Nom.Text) then
    Nom.Text := '_' + Nom.Text;
  Erreur := -1;
  if Categorie.ItemIndex = -1 then
  begin
    FormParent.FocusControl(Categorie);
    Erreur := 0;
  end
  else if ControlNum(Nom.Text, 4, 256, 'C') <> Nom.Text then
  begin
    FormParent.FocusControl(Nom);
    Erreur := 1;
  end
  else if ControlNum(Complement.Text, 0, 256, 'C') <> Complement.Text then
  begin
    FormParent.FocusControl(Complement);
    Erreur := 2;
  end
  else if NGGlazing.ItemIndex = -1 then
  begin
    FormParent.FocusControl(NGGlazing);
    Erreur := 3;
  end;
  if Erreur <> -1 then
  begin
    MessageDlgM(MessagesInterface[9 + Erreur], mtWarning, [mbOK], 0);
    Result := False;
  end
  else
    Result := True;
end;


procedure TFrameMenuiseries.BitBtnCreerClasseClick(Sender: TObject);
var
  Valeur: string;
begin
  if InputQueryM(MessagesInterface[6], MessagesInterface[6], valeur) and
    (Valeur <> '') then
    BibMenuiseries.AjouteCategorie(Valeur, Categorie.items, Tree);
end;

procedure TFrameMenuiseries.BitBtnChangeClick(Sender: TObject);
var
  P:  integer;
  CNG: double;
  CG: double;
  Coeff: double;
  ConducMoy: double;
  CoeffMoy: double;
  Chaine1: string;
  chaine2: string;
begin
  formSaisieFen.EditAngle1.Text := EditAngle1.Text;
  formSaisieFen.EditAngle2.Text := EditAngle2.Text;
  formSaisieFen.EditAngle3.Text := EditAngle3.Text;
  formSaisieFen.EditCoeff0.Text := EditCoeff0.Text;
  formSaisieFen.EditCoeff1.Text := EditCoeff1.Text;
  formSaisieFen.EditCoeff2.Text := EditCoeff2.Text;
  formSaisieFen.EditCoeff3.Text := EditCoeff3.Text;

  formSaisieFen.ConducG.Text  := ConducFenVitr.Text;
  formSaisieFen.ConducNG.Text := ConducFenOp.Text;
  formSaisieFen.PourcentG.Text := PourcentFen.Text;
  FormSaisieFen.ConducNG2.ItemIndex := -1;
  FormSaisieFen.PourcentG2.ItemIndex := -1;
  FormSaisieFen.NbDeVitrages  := NGGlazing.ItemIndex;
  if formSaisieFen.ShowModal = mrOk then
  begin
    ConducFenVitr.Text := formSaisieFen.ConducG.Text;
    ConducFenOp.Text := formSaisieFen.ConducNG.Text;
    PourcentFen.Text := formSaisieFen.PourcentG.Text;
    Editangle1.Text  := formSaisieFen.Editangle1.Text;
    Editangle2.Text  := formSaisieFen.Editangle2.Text;
    Editangle3.Text  := formSaisieFen.Editangle3.Text;
    EditCoeff0.Text  := formSaisieFen.EditCoeff0.Text;
    EditCoeff1.Text  := formSaisieFen.EditCoeff1.Text;
    EditCoeff2.Text  := formSaisieFen.EditCoeff2.Text;
    EditCoeff3.Text  := formSaisieFen.EditCoeff3.Text;
    p := getInt(formSaisieFen.PourcentG.Text);
    CNG := getDouble(formSaisieFen.ConducNG.Text);
    cg := getDouble(formSaisieFen.ConducG.Text);
    Coeff := getDouble(EditCoeff0.Text);
    ConducMoy := (CG * P / 100) + (CNG * (100 - P) / 100);
    CoeffMoy  := (Coeff * P / 100);
    Str(ConducMoy: 0: 2, Chaine1);
    Str(CoeffMoy: 0: 2, chaine2);
    CoeffFen.Text  := Chaine2;
    conducFen.Text := Chaine1;
    ModifMenuiseries := True;
  end;
end;

procedure TFrameMenuiseries.BitBtnNewFenClick(Sender: TObject);
var
  Ok: boolean;
begin
  if ModifMenuiseries then
    Ok := ControleSauve(False)
  else
    Ok := True;
  if Ok then
  begin
    Nom.Text := '';
    Complement.Text := '';
    Source.Text := '';
    Categorie.ItemIndex := -1;
    NGGlazing.ItemIndex := -1;
    Editcoeff0.Text := '';
    Editcoeff1.Text := '';
    EditCoeff2.Text := '';
    Editcoeff3.Text := '';
    Editangle1.Text := '';
    EditAngle2.Text := '';
    EditAngle3.Text := '';
    ConducFen.Text := '';
    CoeffFen.Text := '';
    ConducFenVitr.Text := '';
    ConducFenOp.Text := '';
    PourcentFen.Text := '';
    ModifMenuiseries := False;
  end;
end;

procedure TFrameMenuiseries.BitBtnSaveFenClick(Sender: TObject);
begin
  ControleSauve(True);
end;

procedure TFrameMenuiseries.BitBtnSaveFenMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: integer);
begin
  SauveReadOnly := (Shift * [ssShift, ssCtrl] = [ssShift, ssCtrl]);
end;

procedure TFrameMenuiseries.CategorieChange(Sender: TObject);
begin
  ModifMenuiseries := True;
end;

procedure TFrameMenuiseries.TreeChange(Sender: TObject; Node: TTreeNode);
var
  Chaine: string;
begin
  Str(BibMenuiseries.Count, chaine);
  if CheckHint then
    Tree.Hint := Chaine + MessagesInterface[8]
  else
    Tree.Hint := '';
end;

procedure TFrameMenuiseries.TreeClick(Sender: TObject);
var
  Noeud: TTreeNode;
  Menuiserie: TMenuiseries;
  Ok: boolean;
begin
  if ModifMenuiseries then
    Ok := ControleSauve(False)
  else
    Ok := True;
  if not Ok then
    Exit;
  Menuiserie := nil;
  Noeud := Tree.Selected;
  if Noeud <> nil then
    Menuiserie := BibMenuiseries.Get(Noeud.Text);
  if Menuiserie <> nil then
    Affiche(Menuiserie);
  ModifMenuiseries := False;
end;

procedure TFrameMenuiseries.TreeDragDrop(Sender, Source: TObject; X, Y: integer);
var
  Courant: TTreeNode;
  Fen: TMenuiseries;
begin
  if (Tree.dropTarget.ImageIndex = BibMenuiseries.ImageIndexDossier) or
    (Tree.dropTarget.ImageIndex = BibMenuiseries.ImageIndexDossierS) then
  begin
    Fen := BibMenuiseries.Get(dragage.Text);
    Fen.Categorie := Tree.DropTarget.Text;
    Tree.Items.Delete(dragage);
    Courant := BibMenuiseries.PoseDansArbre(Fen, Tree, True, False);
    Tree.alphaSort;
    Tree.FullCollapse;
    Tree.Selected := Courant;
  end;
end;

procedure TFrameMenuiseries.TreeDragOver(Sender, Source: TObject;
  X, Y: integer; State: TDragState; var Accept: boolean);
begin
  if Dragage <> nil then
    Accept := (dragage.ImageIndex = BibMenuiseries.ImageIndex) or
      (dragage.ImageIndex = BibMenuiseries.ImageIndexPorte)
  else
    Accept := False;
end;

procedure TFrameMenuiseries.TreeKeyUp(Sender: TObject; var Key: word;
  Shift: TShiftState);
begin
  if (Key = 46) and (Tree.Selected <> nil) then
    EffaceDragage
  else
    Tree.OnClick(Self);
end;

procedure TFrameMenuiseries.TreeMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: integer);
begin
  Dragage := TTreeView(Sender).GetNodeAt(X, Y);
end;

end.
