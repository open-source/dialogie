unit NCadreMenuiseriesPleiades;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, NCadreMenuiseries, Menus, StdCtrls, BarreAdresse, Buttons, ExtCtrls,
  Menuiseries, ComCtrls;

type
  TFrameMenuiseriesPleiades = class(TFrameMenuiseries)
    BitBtnExp: TBitBtn;
    procedure BitBtnExpClick(Sender: TObject);
    procedure Export1Click(Sender: TObject);
    procedure FrameResize(Sender: TObject);
  private
    { Déclarations privées }
    procedure Exporte(comp: TMenuiseries);
  public
    { Déclarations publiques }
  end;

var
  FrameMenuiseriesPleiades: TFrameMenuiseriesPleiades;

implementation

uses
  Projet, DHMulti, BiblioMenuiseries, MainPleiades;

{$R *.dfm}

procedure TFrameMenuiseriesPleiades.BitBtnExpClick(Sender: TObject);
var
  comp: TMenuiseries;
begin
  inherited;
  if Controle then
  begin
    comp := TMenuiseries.Create(EditNom.Text, BarreCategorie.Chemin,
      MComplement, EditSource.Text, EditCoeffUMoyen.Text,
      EditFacteurSolaireMoyen.Text, EditPourcentageVitrage.Text,
      EditUVitrage.Text, EditUCadre.Text, EditFacteurSolaire0.Text,
      EditFacteurSolaire1.Text, EditFacteurSolaire2.Text, EditFacteurSolaire3.Text,
      EditAngle1.Text, EditAngle2.Text, EditAngle3.Text, ComboNbVitrage.ItemIndex,
      EditTransmission.Text);
    Exporte(comp)
  end;
end;

procedure TFrameMenuiseriesPleiades.Exporte(comp: TMenuiseries);
Var
  Prec : Word;
begin
    prec := mrYes;
    ProjetCourant.AjouteMenuiserie(comp,prec);
    BibMenuiseries.PoseDansArbre(comp, FormDHMulti.FComposants.TreeFen, False, True);
    FormDHMulti.FComposants.TreeFen.OnChange(Self,nil);
    FormDHMulti.FParois.AfficheParoi;
end;

procedure TFrameMenuiseriesPleiades.Export1Click(Sender: TObject);
var
  i: integer;
  comp: TMenuiseries;
begin
  inherited;
  for i := Tree.Selectioncount - 1 downto 0 do
  begin
    comp := TMenuiseries.Create;
    comp.Assign(Tree.Selections[i].Data);
    Exporte(comp);
  end;
end;

procedure TFrameMenuiseriesPleiades.FrameResize(Sender: TObject);
begin
  inherited;
  EditFiltre.Images := FormMainPleiades.ImageListNMenu;
  EditFiltre.RightButton.ImageIndex := 303;
end;

end.
