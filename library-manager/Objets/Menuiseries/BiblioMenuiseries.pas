unit BiblioMenuiseries;

interface

uses
  // Standard                                                
  Classes, ComCtrls,
  //Pleiades
  Menuiseries, GestionBib, ComposantBibliotheque, GestionSauvegarde;

const
  FichierMenuiseriesNS   = 'MenuiseriesNS.txt';
  FichierMenuiseriesS   = 'MenuiseriesS.txt';
  FichierMenuiseriesV2NS = 'MenuiseriesV2NS.txt';

type
  TBiblioMenuiseries = class(TBibliotheque)

  private
//    procedure loadFromFileV0(filePath: string);
    procedure ChargeListeSansOptique(filePath : string);
//    procedure ChargeListeAvecOptique;
    procedure ChargeContenuSansOptique(FichierS: TSauvegarde);
    procedure ChargeContenu(FichierS: TSauvegarde);
  protected
    function CreationPourImport: TComposantBibliotheque; override;
    procedure ChargeListeAncien(standardLib : boolean);override;
  public
    ImageIndexPorte, ImageIndexPorteRO: integer;
    constructor Create(Standard, NonStandard: string);
    function Get(NomMenuiseries: string): TMenuiseries; overload;
    function Get(Identificateur: integer): TMenuiseries; overload;
    function PoseDansArbre(Composant: TComposantBibliotheque;
      Arbre: TTreeView; AvecCategorie, AvecControleExistant: boolean): TTreeNode;
      override;
    function PoseTousPorteDansArbre(Arbre: TTreeView;
      AvecCategorie, AvecControleExistant: boolean): TTreeNode;
    function PoseTousFenetreDansArbre(Arbre: TTreeView;
      AvecCategorie, AvecControleExistant: boolean): TTreeNode;
    function EstDuType(Index: integer): boolean;override;
  end;

var
  BibMenuiseries: TBiblioMenuiseries;

implementation

uses
  // Delphi
  Controls, Dialogs, SysUtils,
  // Pleiades
  CreerSauvegarde, Windows;

function TBiblioMenuiseries.CreationPourImport: TComposantBibliotheque;
begin
  Result := TMenuiseries.Create('', '', '', '', '', '', '', '', '',
    '', '', '', '', '', '', '', 0, '');
end;


constructor TBiblioMenuiseries.Create(Standard, NonStandard: string);
begin
  inherited Create(Standard, NonStandard);
  ImageIndex := 30;
  ImageIndexRO := 32;
  ImageIndexPorteRO := 106;
  ImageIndexPorte := 104;
  TypeComposant := 'Menuiseries';
  ChargeListe;
end;

function TBiblioMenuiseries.EstDuType(Index: integer): boolean;
begin
  Result := inherited EstDuType(Index) or
    (Index = ImageIndexPorte) or (Index = ImageIndexPorteRO);
end;

function TBiblioMenuiseries.PoseDansArbre(Composant: TComposantBibliotheque;
  Arbre: TTreeView; AvecCategorie, AvecControleExistant: boolean): TTreeNode;
begin
  Result := inherited PoseDansArbre(Composant, Arbre, AvecCategorie,
    AvecControleExistant);
  if TMenuiseries(Composant).IsDoor then
  begin
    if Composant.ReadOnly then
      Result.ImageIndex := ImageIndexPorteRO
    else
      Result.Imageindex := ImageIndexPorte;
    Result.SelectedIndex := Result.ImageIndex + 1;
  end;
end;

function TBiblioMenuiseries.PoseTousPorteDansArbre(Arbre: TTreeView;
  AvecCategorie, AvecControleExistant: boolean): TTreeNode;
var
  i: integer;
begin
  Arbre.items.Clear;
  Result := nil;
  for i := 0 to Table.Count - 1 do
    if TMenuiseries(Table.Objects[i]).IsDoor then
      Result := PoseDansArbre(TComposantBibliotheque(Table.Objects[i]),
        Arbre, AvecCategorie, AvecControleExistant);
  Arbre.AlphaSort;
end;

function TBiblioMenuiseries.PoseTousFenetreDansArbre(Arbre: TTreeView;
  AvecCategorie, AvecControleExistant: boolean): TTreeNode;
var
  i: integer;
begin
  Arbre.items.Clear;
  Result := nil;
  for i := 0 to Table.Count - 1 do
    if not TMenuiseries(Table.Objects[i]).IsDoor then
      PoseDansArbre(TComposantBibliotheque(Table.Objects[i]), Arbre,
        AvecCategorie, AvecControleExistant);
  Arbre.AlphaSort;
end;

procedure TBiblioMenuiseries.ChargeListeAncien(standardLib : boolean);
begin
  if standardLib then
    ChargeListeSansOptique(CheminBiblioStandard + FichierMenuiseriesS)
  else
    ChargeListeSansOptique(CheminBiblioNonStandard + FichierMenuiseriesNS);
end;

procedure TBiblioMenuiseries.ChargeListeSansOptique(filePath : string);
var
  FichierS: TSauvegarde;
begin
  FichierS  := TSauvegarde.Create;
  try
    if FileExists(filePath) then
    begin
      hasToSave := true;
      FichierS.LoadFromFile(filePath);
      ChargeContenuSansOptique(fichierS);
      RenameFile(filePath,filePath +'.old');
    end;
  finally
    FichierS.Free;
  end;
end;

procedure TBiblioMenuiseries.ChargeContenuSansOptique(FichierS: TSauvegarde);
var
  Men: TMenuiseries;
begin
    while not FichierS.EOF do
    begin
      Men := TMenuiseries.Create;
      FichierS.Suivant;
      Men.Identificateur := FichierS.GetLigneEntier;
      FichierS.Suivant;
      Men.Categorie := FichierS.GetLigne;
      FichierS.Suivant;
      Men.nom := FichierS.GetLigne;
      FichierS.Suivant;
      Men.complement := FichierS.GetLigne;
      FichierS.Suivant;
      Men.Source := FichierS.GetLigne;

      //obsol�te
      FichierS.Suivant;
      FichierS.Suivant;
      FichierS.Suivant;
      FichierS.Suivant;
      //fin obsol�te

      FichierS.Suivant;
      Men.PourcentG := FichierS.GetLigneReel;
      FichierS.Suivant;
      Men.Ug := FichierS.GetLigneReel;
      FichierS.Suivant;
      Men.Uf := FichierS.GetLigneReel;
      FichierS.Suivant;
      Men.FacteurSolaire0 := FichierS.GetLigneReel;
      FichierS.Suivant;
      Men.FacteurSolaire1 := FichierS.GetLigneReel;
      FichierS.Suivant;
      Men.FacteurSolaire2 := FichierS.GetLigneReel;
      FichierS.Suivant;
      Men.FacteurSolaire3 := FichierS.GetLigneReel;
      FichierS.Suivant;
      Men.Angle1 := FichierS.GetLigneReel;
      FichierS.Suivant;
      Men.Angle2 := FichierS.GetLigneReel;
      FichierS.Suivant;
      Men.Angle3 := FichierS.GetLigneReel;
      FichierS.Suivant;
      Men.nbVitrage := FichierS.GetLigneEntier;

      FichierS.Suivant;
      Men.ReadOnly := FichierS.GetLigneBool;
      if Table.IndexOf(Men.nom) = -1 then
        Table.AddObject(Men.nom, Men);
      FichierS.Suivant;

      if not Men.ReadOnly and (Men.Identificateur < EcartStandard) then
        Men.Identificateur := Men.Identificateur + EcartStandard;

      if (Men.Identificateur > IdMax) and (Men.ReadOnly) then
        IdMax := Men.Identificateur
      else if (Men.Identificateur > IdMax) then
        IdMax := Men.Identificateur - EcartStandard;
    end;
end;


procedure TBiblioMenuiseries.ChargeContenu(FichierS: TSauvegarde);
var
  Men: TMenuiseries;
begin
  while not FichierS.EOF do
  begin
    Men := TMenuiseries.Create;
    FichierS.Suivant;
    Men.Identificateur := FichierS.GetLigneEntier;
    FichierS.Suivant;
    Men.Categorie := FichierS.GetLigne;
    FichierS.Suivant;
    Men.nom := FichierS.GetLigne;
    FichierS.Suivant;
    Men.complement := FichierS.GetLigne;
    FichierS.Suivant;
    Men.Source := FichierS.GetLigne;

    //obsol�te
    FichierS.Suivant;
    FichierS.Suivant;
    FichierS.Suivant;
    FichierS.Suivant;
    //fin obsol�te

    FichierS.Suivant;
    Men.PourcentG := FichierS.GetLigneReel;
    FichierS.Suivant;
    Men.Ug := FichierS.GetLigneReel;
    FichierS.Suivant;
    Men.Uf := FichierS.GetLigneReel;
    FichierS.Suivant;
    Men.FacteurSolaire0 := FichierS.GetLigneReel;
    FichierS.Suivant;
    Men.FacteurSolaire1 := FichierS.GetLigneReel;
    FichierS.Suivant;
    Men.FacteurSolaire2 := FichierS.GetLigneReel;
    FichierS.Suivant;
    Men.FacteurSolaire3 := FichierS.GetLigneReel;
    FichierS.Suivant;
    Men.Angle1 := FichierS.GetLigneReel;
    FichierS.Suivant;
    Men.Angle2 := FichierS.GetLigneReel;
    FichierS.Suivant;
    Men.Angle3 := FichierS.GetLigneReel;
    FichierS.Suivant;
    Men.nbVitrage := FichierS.GetLigneEntier;

    if FichierS.SuivantSiEgal('Optique') then
      Men.TransmissionLumineuse := FichierS.GetLigneReel
    else
      Men.TransmissionLumineuse := Men.FacteurSolaire0;

    FichierS.Suivant;
    Men.ReadOnly := FichierS.GetLigneBool;
    Table.AddObject(Men.nom, Men);
    FichierS.Suivant;

    if not Men.ReadOnly and (Men.Identificateur < EcartStandard) then
      Men.Identificateur := Men.Identificateur + EcartStandard;

    if (Men.Identificateur > IdMax) and (Men.ReadOnly) then
      IdMax := Men.Identificateur
    else if (Men.Identificateur > IdMax) then
      IdMax := Men.Identificateur - EcartStandard;
  end;
end;

//procedure TBiblioMenuiseries.ChargeListeAvecOptique;
//var
//  FichierS: TSauvegarde;
//begin
//  FichierS := TSauvegarde.Create;
//  try
//    if FileExists(CheminBiblioNonStandard + FichierMenuiseriesV2NS) then
//      FichierS.LoadFromFile(CheminBiblioNonStandard + FichierMenuiseriesV2NS)
//    else if FileExists(CheminBiblioNonStandard + FichierMenuiseriesNS) then
//      FichierS.LoadFromFile(CheminBiblioNonStandard + FichierMenuiseriesNS);
//    ChargeContenu(FichierS);
//  finally
//    FichierS.Free;
//  end;
//end;

function TBiblioMenuiseries.Get(NomMenuiseries: string): TMenuiseries;
begin
  Result := inherited Get(NomMenuiseries) as TMenuiseries;
end;

function TBiblioMenuiseries.Get(Identificateur: integer): TMenuiseries;
begin
  Result := TMenuiseries(inherited Get(Identificateur));
end;

end.
