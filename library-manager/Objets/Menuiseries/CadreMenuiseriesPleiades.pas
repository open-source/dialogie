unit CadreMenuiseriesPleiades;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, CadreMenuiseries, StdCtrls, Buttons, ExtCtrls, ComCtrls, Menus;

type
  TFrameMenuiseriesPleiades = class(TFrameMenuiseries)
    procedure BitBtnExpFenClick(Sender: TObject);
    procedure Export1Click(Sender: TObject);
  private
    { D�clarations priv�es }
  public
    { D�clarations publiques }
  end;

implementation

uses
  DHMulti, Utilitaires, BiblioMenuiseries, translateur, Projet,
  Menuiseries, DialogsInternational;

{$R *.dfm}

procedure TFrameMenuiseriesPleiades.BitBtnExpFenClick(Sender: TObject);
var
  Ok:  boolean;
  Fen: TMenuiseries;
begin
  inherited;
  if Controle then
  begin
    // Verifie s'il existe d�ja pour faire une confirmation
    Ok := True;
    if ProjetCourant.ListeMenuiseries.IndexOf(Nom.Text) <> -1 then
      Ok := (MessageDlgM(Labels[257], mtConfirmation, mbOkCancel, 0) = mrOk);
    if Ok then
    begin
      Fen := TMenuiseries.Create(Nom.Text, Categorie.Text, Complement.Text,
        Source.Text, ConducFen.Text, CoeffFen.Text, PourcentFen.Text,
        ConducFenVitr.Text, ConducFenOp.Text, EditCoeff0.Text,
        EditCoeff1.Text, EditCoeff2.Text, EditCoeff3.Text, EditAngle1.Text,
        EditAngle2.Text, EditAngle3.Text, NGGlazing.ItemIndex, EditTransmission.Text);
      ProjetCourant.AjouteMenuiseries(Fen);
      BibMenuiseries.PoseDansArbre(Fen, FormDHMulti.FComposants.TreeFen, False, True);

      FormDHMulti.FParois.AfficheParoi;
      FormDHMulti.FComposants.TreeFen.OnChange(nil, nil);
      FormDHMulti.FComposants.StandardFen.OnChange(nil);
    end
    else
      MessageDlgM(messagees[21], mtWarning, [mbOK], 0);
  end;
end;

procedure TFrameMenuiseriesPleiades.Export1Click(Sender: TObject);
var
  I: integer;
  Men: TMenuiseries;
begin
  inherited;
  for I := Dragage.TreeView.Selectioncount - 1 downto 0 do
  begin
    Dragage := Dragage.TreeView.Selections[i];
    Men := BibMenuiseries.Get(Dragage.Text);
    if Men <> nil then
    begin
      Affiche(Men);
      BitBtnExpFenClick(nil);
    end;
  end;
end;

end.
