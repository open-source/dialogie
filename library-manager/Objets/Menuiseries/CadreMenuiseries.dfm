object FrameMenuiseries: TFrameMenuiseries
  Left = 0
  Top = 0
  Width = 861
  Height = 613
  ParentBackground = False
  TabOrder = 0
  object Splitter1: TSplitter
    Left = 373
    Top = 0
    Height = 613
    Align = alRight
    ExplicitLeft = 8
    ExplicitTop = 96
    ExplicitHeight = 100
  end
  object zGroupBox16: TGroupBox
    Left = 0
    Top = 0
    Width = 373
    Height = 613
    HelpContext = 25
    Align = alClient
    Caption = 'Windows/doors list'
    TabOrder = 0
    object Tree: TTreeView
      Left = 2
      Top = 15
      Width = 369
      Height = 596
      Align = alClient
      AutoExpand = True
      DoubleBuffered = True
      DragMode = dmAutomatic
      HotTrack = True
      Indent = 19
      MultiSelect = True
      MultiSelectStyle = [msControlSelect, msShiftSelect]
      ParentDoubleBuffered = False
      ParentShowHint = False
      PopupMenu = PopupComponent
      ReadOnly = True
      ShowHint = True
      TabOrder = 0
      OnChange = TreeChange
      OnClick = TreeClick
      OnDragDrop = TreeDragDrop
      OnDragOver = TreeDragOver
      OnKeyUp = TreeKeyUp
      OnMouseDown = TreeMouseDown
    end
  end
  object zGroupBox17: TGroupBox
    Left = 376
    Top = 0
    Width = 485
    Height = 613
    HelpContext = 25
    Align = alRight
    Caption = 'Characteristics of the window / door'
    Color = clBtnFace
    Constraints.MinWidth = 485
    ParentColor = False
    TabOrder = 1
    object zLabel77: TLabel
      Left = 6
      Top = 48
      Width = 25
      Height = 13
      Caption = 'Class'
    end
    object zLabel78: TLabel
      Left = 6
      Top = 76
      Width = 27
      Height = 13
      Caption = 'Name'
    end
    object zLabel79: TLabel
      Left = 6
      Top = 104
      Width = 76
      Height = 13
      Caption = 'Additional name'
    end
    object zLabel80: TLabel
      Left = 6
      Top = 132
      Width = 28
      Height = 13
      Caption = 'Origin'
    end
    object zLabel81: TLabel
      Left = 6
      Top = 162
      Width = 91
      Height = 13
      Caption = 'Number of glazings'
    end
    object zLabel82: TLabel
      Left = 6
      Top = 190
      Width = 99
      Height = 13
      Caption = 'Average solar factor'
    end
    object zLabel83: TLabel
      Left = 6
      Top = 218
      Width = 107
      Height = 13
      Caption = 'Average U-value (Uw)'
    end
    object zLabel84: TLabel
      Left = 6
      Top = 246
      Width = 60
      Height = 13
      Caption = '% of glazing'
    end
    object Label2: TLabel
      Left = 250
      Top = 218
      Width = 50
      Height = 13
      AutoSize = False
      Caption = 'W/(m'#178'.K)'
    end
    object Label3: TLabel
      Left = 250
      Top = 246
      Width = 50
      Height = 13
      AutoSize = False
      Caption = '%'
    end
    object Categorie: TComboBox
      Tag = -1
      Left = 168
      Top = 47
      Width = 309
      Height = 21
      Style = csDropDownList
      TabOrder = 0
      OnChange = CategorieChange
      Items.Strings = (
        '')
    end
    object Nom: TEdit
      Left = 168
      Top = 75
      Width = 309
      Height = 21
      TabOrder = 1
      OnChange = CategorieChange
    end
    object Complement: TEdit
      Left = 168
      Top = 103
      Width = 309
      Height = 21
      TabOrder = 2
      OnChange = CategorieChange
    end
    object Source: TEdit
      Left = 168
      Top = 131
      Width = 309
      Height = 21
      TabOrder = 3
      OnChange = CategorieChange
    end
    object zPanel12: TPanel
      Left = 2
      Top = 570
      Width = 481
      Height = 41
      Align = alBottom
      BevelOuter = bvNone
      BorderWidth = 1
      TabOrder = 5
      object BitBtnSaveFen: TBitBtn
        Left = 375
        Top = 12
        Width = 100
        Height = 25
        Hint = 'Save this glazing into Bibliotherm'
        Caption = '&Save'
        Glyph.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00C0686000B0585000A0505000A0505000A050500090485000904840009048
          40008040400080384000803840007038400070383000FF00FF00FF00FF00D068
          7000F0909000E0808000B048200040302000C0B8B000C0B8B000D0C0C000D0C8
          C00050505000A0403000A0403000A038300070384000FF00FF00FF00FF00D070
          7000FF98A000F0888000E0808000705850004040300090787000F0E0E000F0E8
          E00090807000A0403000A0404000A040300080384000FF00FF00FF00FF00D078
          7000FFA0A000F0909000F0888000705850000000000040403000F0D8D000F0E0
          D00080786000B0484000B0484000A040400080404000FF00FF00FF00FF00D078
          8000FFA8B000FFA0A000F0909000705850007058500070585000705850007060
          500080686000C0585000B0505000B048400080404000FF00FF00FF00FF00E080
          8000FFB0B000FFB0B000FFA0A000F0909000F0888000E0808000E0788000D070
          7000D0687000C0606000C0585000B050500090484000FF00FF00FF00FF00E088
          9000FFB8C000FFB8B000D0606000C0605000C0585000C0504000B0503000B048
          3000A0402000A0381000C0606000C058500090484000FF00FF00FF00FF00E090
          9000FFC0C000D0686000FFFFFF00FFFFFF00FFF8F000F0F0F000F0E8E000F0D8
          D000E0D0C000E0C8C000A0381000C060600090485000FF00FF00FF00FF00E098
          A000FFC0C000D0707000FFFFFF00FFFFFF00FFFFFF00FFF8F000F0F0F000F0E8
          E000F0D8D000E0D0C000A0402000D0686000A0505000FF00FF00FF00FF00F0A0
          A000FFC0C000E0787000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFF8F000F0F0
          F000F0E8E000F0D8D000B0483000D0707000A0505000FF00FF00FF00FF00F0A8
          A000FFC0C000E0808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFF8
          F000F0F0F000F0E8E000B0503000E0788000A0505000FF00FF00FF00FF00F0B0
          B000FFC0C000F0889000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFF8F000F0F0F000C050400060303000B0585000FF00FF00FF00FF00F0B0
          B000FFC0C000FF909000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFF8F000C0585000B0586000B0586000FF00FF00FF00FF00F0B8
          B000F0B8B000F0B0B000F0B0B000F0A8B000F0A0A000E098A000E0909000E090
          9000E0889000E0808000D0788000D0787000D0707000FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BitBtnSaveFenClick
        OnMouseDown = BitBtnSaveFenMouseDown
      end
      object BitBtnExpFen: TBitBtn
        Left = 246
        Top = 12
        Width = 100
        Height = 25
        Hint = 'Send the glazing into the project'
        Caption = '&To project'
        Glyph.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00B0A090006048
          3000604830006048300060483000604830006048300060483000604830006048
          300060483000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00B0A09000FFFF
          FF00F0E8E000D0C8C000D0B8B000C0B0A000C0B8B000C0B0A000C0A8A000B0A0
          900060483000FF00FF00FF00FF0000000000FF00FF00FF00FF00B0A09000FFFF
          FF00FFFFFF00FFFFFF00FFF0F000F0E8E000F0E0D000F0D8D000E0D0C000B0A0
          900060483000FF00FF00FF00FF000000000000000000FF00FF00B0A09000FFFF
          FF00FFFFFF00FFFFFF00FFF8F000F0F0F000F0E0E000F0D8D000E0D0C000B0A0
          900060483000FF00FF00FF00FF0000000000FF00FF0000000000B0A09000FFFF
          FF00B0A090007050400060504000604830006048300060483000F0D8D000C0A0
          900060504000FF00FF00FF00FF00FF00FF00FF00FF0000000000C0A89000FFFF
          FF00C0A89000F0E8F000D0B8B000C0A89000B0A0900060483000F0E0E000C0A8
          A00080686000FF00FF00FF00FF00FF00FF00FF00FF0060606000C0A8A000FFFF
          FF00C0A8A000FFFFFF00F0F0F000E0D8D000B0A0900060483000F0E8E000C0B0
          A000C0B0A000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00C0B0A000FFFF
          FF00C0B0A000FFFFFF00FFFFFF00F0E8E000C0A8900060504000F0F0F000D0C8
          C000B0A090007050400060504000604830006048300060483000D0B0A000FFFF
          FF00D0B8A000FFFFFF00FFFFFF00F0F0F000C0A8900060504000FFF8F000E0D8
          D000C0A89000F0E8F000D0B8B000C0A89000B0A0900060483000D0B8A000FFFF
          FF00D0B8B000FFFFFF00FFFFFF00C0A8A000D0B8B00070584000FFF8F000F0E8
          E000C0A8A000FFFFFF00F0F0F000E0D8D000B0A0900060483000D0B8B000FFFF
          FF00D0C0B000FFFFFF00FFFFFF00E0D0D00060483000E0D8D000A0888000E0D8
          D000C0B0A000FFFFFF00FFFFFF00F0E8E000C0A8900060483000D0C0B000FFFF
          FF00E0C0B000E0C0B000E0C0B000D0B0A000E0D8D000B0A09000D0D0C000E0E0
          E000D0B8A000FFFFFF00FFFFFF00F0F0F000C0A8900060483000E0C0B000FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00B0A09000A0888000D0B0
          A000D0B8B000FFFFFF00FFFFFF00C0A8A000D0B8B00070504000E0C0B000E0C0
          B000E0C0B000E0C0B000E0C0B000D0C0B000D0B8B000D0B8A000E0D0D000FF00
          FF00D0C0B000FFFFFF00FFFFFF00E0D0D00060483000E0D8D000FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00E0C0B000E0C0B000E0C0B000D0B0A000E0D8D000FF00FF00}
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
      end
      object BitBtnNewFen: TBitBtn
        Left = 4
        Top = 12
        Width = 100
        Height = 25
        Hint = 'Create a new glazing'
        Caption = '&New'
        Glyph.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00B0A090006048300060483000604830006048300060483000604830006048
          3000604830006048300060483000FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00B0A09000FFFFFF00B0A09000B0A09000B0A09000B0A09000B0A09000B0A0
          9000B0A09000B0A0900060483000FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00B0A09000FFFFFF00FFFFFF00FFF8FF00F0F0F000F0E8E000F0E0D000E0D0
          D000E0C8C000B0A0900060483000FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00B0A09000FFFFFF00FFFFFF00FFFFFF00FFF8F000F0F0F000F0E0E000F0D8
          D000E0D0C000B0A0900060483000FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00B0A09000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFF0F000F0E8E000F0E0
          E000E0D8D000B0A0900060483000FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00C0A89000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFF8F000F0F0F000F0E8
          E000F0D8D000B0A0900060483000FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00C0A8A000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFF8F000F0E8
          E000F0E0E000B0A0900060483000FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00C0B0A000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFF8FF00F0F0
          F000F0E8E000B0A0900060483000FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00D0B0A000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFF8
          F000F0F0F000B0A0900060483000FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00D0B8A000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00B0A09000B0A0900060483000FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00D0B8B000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00B0A0
          9000604830006048300060483000FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00D0C0B000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00C0A8
          9000D0C8C00060483000D0B0A000FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00E0C0B000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00C0A8
          A00060483000D0B0A000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00E0C0B000E0C0B000E0C0B000E0C0B000E0C0B000D0C0B000D0B8B000D0B0
          A000D0B0A000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        OnClick = BitBtnNewFenClick
      end
    end
    object NGGlazing: TComboBox
      Left = 168
      Top = 159
      Width = 145
      Height = 21
      Style = csDropDownList
      TabOrder = 4
      OnChange = CategorieChange
      Items.Strings = (
        'Opaque'
        '1 glazing'
        '2 glazings'
        '3 glazings'
        'Special glazing')
    end
    object BitBtnCreerClasse: TBitBtn
      Left = 297
      Top = 15
      Width = 180
      Height = 25
      Caption = 'Create a new class'
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        1800000000000003000000000000000000000000000000000000FF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF708890707880
        6070705060604050503038402028301020201010201010201010201010201010
        20101020101020FF00FF708890A0E0F070D0F050B8E030B0E030A8E020A0D020
        98C02090C02080B02080B01080B02078A0207090102020FF00FF808890B0E8F0
        90E8FF80E0FF70D8FF70D0F060C8F050C0F040B8F030A8F030A8E02098E01090
        D02078A0202830FF00FF8090A0B0E8F0A0E8FF90E8FF80E0FF70D8FF70D0F060
        C8F050C0F040B8F030A8F030A0E02098E01080B0303840FF00FF8090A0B0F0FF
        B0F0FFA0E8FF90E0FF80E0FF70D8FF70D0F060C8F050C0F040B0F030A8F020A0
        E01080B0404050FF00FF8098A0C0F0FFB0F0F0A0F0FFA0E8FF90E0FF80E0FF70
        D8FF60D0F060C8F050B8F040B0F030A8E01088C0505060FF00FF8098A0C0F0FF
        B0F0FFB0F0FFA0E8FF90E8FF90E0FF80E0FF70D8FF60D0F060C8F050B8F030A8
        E01090C0506070FF00FF90A0A0C0F0FFB0F0FFB0F0FFB0F0F0A0F0FF90E8FF90
        E0FF80E0FF70D0FF60D0F050C0F050B8F02098D0607080FF00FF90A0B0C0F0FF
        C0F0FFC0F0FFC0F0FFB0F0FFB0F0FFA0E8FF90E8FF90E0FF80D8FF70D0FF70C8
        F060C0F0607080FF00FF90A0B090A0B090A0B090A0B090A0B090A0B090A0A090
        98A08098A08098A08098A08098A08098A08098A0FF00FFFF00FF90A8B0B0E8F0
        B0F0FFB0F0FFB0F0F090E0F090A0B0FF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FF90A8B090A8B090A8B090A8B090A8B0FF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FF000000FF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF0000
        00000000000000FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FF000000FF00FFFF00FF}
      TabOrder = 6
      OnClick = BitBtnCreerClasseClick
    end
    object CoeffFen: TEdit
      Left = 168
      Top = 187
      Width = 76
      Height = 21
      Alignment = taRightJustify
      Color = cl3DLight
      ReadOnly = True
      TabOrder = 7
    end
    object ConducFen: TEdit
      Left = 168
      Top = 215
      Width = 76
      Height = 21
      Alignment = taRightJustify
      Color = cl3DLight
      ReadOnly = True
      TabOrder = 8
    end
    object PourcentFen: TEdit
      Left = 168
      Top = 243
      Width = 76
      Height = 21
      Alignment = taRightJustify
      Color = cl3DLight
      ReadOnly = True
      TabOrder = 9
    end
    object zGroupBox18: TGroupBox
      Left = 6
      Top = 265
      Width = 329
      Height = 72
      Caption = 'Glazing'
      TabOrder = 10
      object zLabel85: TLabel
        Left = 12
        Top = 17
        Width = 56
        Height = 13
        Caption = 'Solar factor'
        Color = clBtnFace
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlue
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object zLabel86: TLabel
        Left = 12
        Top = 45
        Width = 98
        Height = 13
        Caption = 'Glazing U-value (Ug)'
        Color = clBtnFace
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlue
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object Label1: TLabel
        Left = 244
        Top = 45
        Width = 45
        Height = 13
        Caption = 'W/(m'#178'.K)'
        Color = clBtnFace
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlue
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object ConducFenVitr: TEdit
        Left = 162
        Top = 42
        Width = 76
        Height = 21
        Alignment = taRightJustify
        Color = clBtnFace
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlue
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
      end
      object EditCoeff0: TEdit
        Left = 162
        Top = 14
        Width = 76
        Height = 21
        Alignment = taRightJustify
        Color = clBtnFace
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlue
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 1
      end
    end
    object zGroupBox19: TGroupBox
      Left = 6
      Top = 337
      Width = 329
      Height = 50
      Caption = 'Frame'
      TabOrder = 11
      object zLabel87: TLabel
        Left = 12
        Top = 17
        Width = 100
        Height = 13
        Caption = 'Opaque U-value (Uf)'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clMaroon
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object Label4: TLabel
        Left = 244
        Top = 17
        Width = 45
        Height = 13
        Caption = 'W/(m'#178'.K)'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clMaroon
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object ConducFenOp: TEdit
        Left = 162
        Top = 14
        Width = 76
        Height = 21
        Alignment = taRightJustify
        Color = cl3DLight
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clMaroon
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
      end
    end
    object BitBtnChange: TBitBtn
      Left = 6
      Top = 452
      Width = 188
      Height = 25
      Caption = 'Change characteristics'
      TabOrder = 12
      OnClick = BitBtnChangeClick
    end
    object EditAngle1: TEdit
      Left = 428
      Top = 351
      Width = 49
      Height = 21
      TabOrder = 13
      Visible = False
    end
    object EditCoeff1: TEdit
      Left = 365
      Top = 326
      Width = 57
      Height = 21
      TabOrder = 14
      Visible = False
    end
    object EditCoeff2: TEdit
      Left = 350
      Top = 353
      Width = 57
      Height = 21
      TabOrder = 15
      Visible = False
    end
    object EditAngle2: TEdit
      Left = 374
      Top = 299
      Width = 49
      Height = 21
      TabOrder = 16
      Visible = False
    end
    object EditCoeff3: TEdit
      Left = 387
      Top = 238
      Width = 19
      Height = 21
      TabOrder = 17
      Visible = False
    end
    object EditAngle3: TEdit
      Left = 402
      Top = 265
      Width = 15
      Height = 21
      TabOrder = 18
      Visible = False
    end
    object GroupOptique: TGroupBox
      Left = 6
      Top = 390
      Width = 471
      Height = 53
      Caption = 'Optical properties'
      TabOrder = 19
      Visible = False
      object LabelTransmission: TLabel
        Left = 12
        Top = 27
        Width = 85
        Height = 13
        Caption = 'Light transmission'
      end
      object EditTransmission: TEdit
        Left = 162
        Top = 24
        Width = 76
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
      end
    end
  end
  object PopupComponent: TPopupMenu
    OnPopup = PopupComponentPopup
    Left = 8
    Top = 24
    object Open1: TMenuItem
      Caption = 'Open'
      Default = True
      OnClick = Open1Click
    end
    object Delete1: TMenuItem
      Caption = 'Delete'
      ShortCut = 46
      OnClick = Delete1Click
    end
    object Export1: TMenuItem
      Caption = 'Export'
      ShortCut = 16453
    end
    object Rename1: TMenuItem
      Caption = 'Rename'
      OnClick = Rename1Click
    end
  end
end
