unit Elements;

interface

uses
  // Delphi
  Windows,
  Messages,
  SysUtils,
  Classes,
  Graphics,
  Controls,
  Forms,
  Dialogs,
  ComCtrls,
  Buttons,
  ComposantBibliotheque,
  GestionSauvegarde,
  CreerSauvegarde,
  Xml.XMLIntf;

type
  TElement = class(TComposantBibliotheque)
    Conductivite, MasseVol, ChaleurMassWh, ConductiviteEquiv, Resistance,
      epaisseur : double;
  private
    const
    FIdVersion : Integer = 2;
  protected

    procedure innerLoadXml(node : IXMLNode); override;
    procedure innerSaveXml(node : IXMLNode); override;

  public
    constructor Create();override;
    constructor Create(Nom_, categorie_, complement_, source_,
      conductivite_, massevolumique_, ChaleurMassiqueJ_, ConductiviteEquiv_,
      Resistance_, epaisseur_, Image1_ : string); overload;
    function Sauve(var Contenu : TCreationSauvegarde) : boolean; override;
    function Charge(var Contenu : TSauvegarde; NumVersion : Integer) : boolean; override;
    procedure Assign(Elem : TElement);
    function ValeursIdentiques(Comp : TComposantBibliotheque) : boolean; override;
    class function idVersion : Integer; override;
  end;

  TListeElement = class(TList)
  private
    function GetElement(const i : Integer) : TElement; inline;
    procedure SetElement(const i : Integer; const Valeur : TElement);
  public
    property objets[const index : Integer] : TElement
      read GetElement write SetElement; default;
    procedure ClearAndFree;
    function IndexOf(const Chaine : string) : Integer; overload;
    procedure AddStrings(S : TListeElement);
    procedure Assign(S : TListeElement);
  end;

implementation

uses
  IzUtilitaires, XmlUtil;

class function TElement.idVersion : Integer;
begin
  result := FIdVersion;
end;

function TElement.ValeursIdentiques(Comp : TComposantBibliotheque) : boolean;
var
  Elem : TElement;
begin
  Elem := Comp as TElement;
  result := Conductivite = Elem.Conductivite;
  result := result and (MasseVol = Elem.MasseVol);
  result := result and (ChaleurMassWh = Elem.ChaleurMassWh);
  result := result and (epaisseur = Elem.epaisseur);
end;

procedure TListeElement.Assign(S : TListeElement);
begin
  Clear;
  AddStrings(S);
end;

procedure TListeElement.AddStrings(S : TListeElement);
var
  i : Integer;
begin
  Capacity := Count + S.Count;
  for i := 0 to S.Count - 1 do
    Add(S[i]);
end;

function TListeElement.IndexOf(const Chaine : string) : Integer;
begin
  for result := 0 to Count - 1 do
    if TElement(items[result]).nom = Chaine then
      Exit;
  result := -1;
end;

procedure TListeElement.ClearAndFree;
var
  i : Integer;
begin
  for i := 0 to Count - 1 do
    TElement(items[i]).Free;
  Clear;
end;

function TListeElement.GetElement(const i : Integer) : TElement;
begin
  if i < Count then
    result := Self.items[i]
  else
    result := nil;
end;

procedure TListeElement.SetElement(const i : Integer; const Valeur : TElement);
begin
  Self.items[i] := Valeur;
end;

constructor TElement.Create;
begin
  inherited;
  innerTagName := 'element';
end;

constructor TElement.Create(Nom_, categorie_, complement_, source_,
  conductivite_, massevolumique_, ChaleurMassiqueJ_, ConductiviteEquiv_,
  Resistance_, epaisseur_, Image1_ : string);
var
  reel : double;
  erreur : Integer;
begin
  inherited Create(Nom_, categorie_, complement_, source_);
  innerTagName := 'element';
  Val(ConvertPoint(conductivite_), reel, erreur);
  Conductivite := reel;
  Val(ConvertPoint(massevolumique_), reel, erreur);
  MasseVol := reel;
  Val(ConvertPoint(ChaleurMassiqueJ_), reel, erreur);
  ChaleurMassWh := reel;
  Val(ConvertPoint(ConductiviteEquiv_), reel, erreur);
  ConductiviteEquiv := reel;
  Val(ConvertPoint(Resistance_), reel, erreur);
  Resistance := reel;
  Val(ConvertPoint(epaisseur_), reel, erreur);
  epaisseur := reel;
  Image1 := Image1_;
end;

procedure TElement.Assign(Elem : TElement);
begin
  inherited Assign(Elem);
  Conductivite := Elem.Conductivite;
  MasseVol := Elem.MasseVol;
  ChaleurMassWh := Elem.ChaleurMassWh;
  Resistance := Elem.Resistance;
  epaisseur := Elem.epaisseur;
  ConductiviteEquiv := Elem.ConductiviteEquiv;
  Image1 := Elem.Image1;
end;

function TElement.Sauve(var Contenu : TCreationSauvegarde) : boolean;
begin
  result := inherited SauveDebut(Contenu);
  Contenu.Add(Conductivite);
  Contenu.Add(MasseVol);
  Contenu.Add(ChaleurMassWh);
  Contenu.Add(ConductiviteEquiv);
  Contenu.Add(Resistance);
  Contenu.Add(epaisseur);
  // V2
  Contenu.Add(Image1);
  result := result and inherited SauveFin(Contenu);
end;

function TElement.Charge(var Contenu : TSauvegarde; NumVersion : Integer) : boolean;
begin
  result := inherited ChargeDebut(Contenu);
  Conductivite := Contenu.GetLigneReel;
  MasseVol := Contenu.GetLigneReel;
  ChaleurMassWh := Contenu.GetLigneReel;
  ConductiviteEquiv := Contenu.GetLigneReel;
  Resistance := Contenu.GetLigneReel;
  epaisseur := Contenu.GetLigneReel;
  if NumVersion >= 2 then
    Image1 := Contenu.GetLigne;
  result := result and inherited ChargeFin(Contenu);
end;

procedure TElement.innerLoadXml(node : IXMLNode);
begin
  inherited innerLoadXml(node);
  Conductivite := TXmlUtil.getTagDouble(node, 'conductivite');
  MasseVol := TXmlUtil.getTagDouble(node, 'masse-volumique');
  ChaleurMassWh := TXmlUtil.getTagDouble(node, 'chaleur-massique-wh');
  ConductiviteEquiv := TXmlUtil.getTagDouble(node, 'conductivite-equiv');
  Resistance := TXmlUtil.getTagDouble(node, 'resistance');
  epaisseur := TXmlUtil.getTagDouble(node, 'epaisseur');;
  Image1 := TXmlUtil.getTagString(node, 'image');;
end;


procedure TElement.innerSaveXml(node : IXMLNode);
begin
  inherited innerSaveXml(node);
  TXmlUtil.setTag(node, 'conductivite', Conductivite);
  TXmlUtil.setTag(node, 'masse-volumique', MasseVol);
  TXmlUtil.setTag(node, 'chaleur-massique-wh', ChaleurMassWh);
  TXmlUtil.setTag(node, 'conductivite-equiv', ConductiviteEquiv);
  TXmlUtil.setTag(node, 'resistance', Resistance);
  TXmlUtil.setTag(node, 'epaisseur', epaisseur);
  TXmlUtil.setTag(node, 'image', Image1);
end;


end.
