unit BiblioElements;

interface

uses
  // Standard
  Classes, ComCtrls,
  // Pleiades
  Elements, GestionBib, ComposantBibliotheque;

const
  FichierElementNS = 'ElementsNS.txt';

type
  TBiblioElements = class(TBibliotheque)
  private
const
  FichierElementS = 'ElementsS.txt';
    procedure loadFromFileV0(filePath: string);
  protected
    function CreationPourImport: TComposantBibliotheque; override;
    procedure ChargeListeAncien(standardLib : boolean); override;
  public
    constructor Create(Standard, NonStandard: string);
    function Get(NomElem: string): TElement; overload;
    function Get(Identificateur: integer): TElement; overload;
  end;

var
  BibElements: TBiblioElements;

implementation

uses
  // Delphi
  Controls, Dialogs, SysUtils,
  // Pleiades
  CreerSauvegarde, GestionSauvegarde;

function TBiblioElements.CreationPourImport: TComposantBibliotheque;
begin
  Result := TElement.Create;
end;

constructor TBiblioElements.Create(Standard, NonStandard: string);
begin
  inherited Create(Standard, NonStandard);
  ImageIndex := 6;
  ImageIndexRO := 8;
  ImageBrute := 161;
  TypeComposant := 'Elements';
  ChargeListe;
end;

function TBiblioElements.Get(NomElem: string): TElement;
begin
  Result := inherited Get(NomElem) as TElement;
end;

function TBiblioElements.Get(Identificateur: integer): TElement;
begin
  Result := TElement(inherited Get(Identificateur));
end;

procedure TBiblioElements.ChargeListeAncien(standardLib : boolean);
begin
  if standardLib then
    loadFromFileV0(CheminBiblioStandard + FichierElementS)
  else
    loadFromFileV0(CheminBiblioNonStandard + FichierElementNS);
end;

procedure TBiblioElements.loadFromFileV0(filePath : string);
var
  Elem: TElement;
  Fichier: TSauvegarde;
begin
  Fichier := TSauvegarde.Create;
  try
    if fileExists(filePath) then
    begin
      hasToSave := true;
      Fichier.LoadFromFile(filePath);
      while not Fichier.EOF do
      begin
        Elem := TElement.Create;
        Fichier.Suivant;
        Elem.Identificateur := Fichier.GetLigneEntier;

        Fichier.Suivant;
        Elem.Categorie := Fichier.GetLigne;

        Fichier.Suivant;
        Elem.nom := Fichier.GetLigne;
        Fichier.Suivant;
        Elem.complement := Fichier.GetLigne;
        Fichier.Suivant;
        Elem.Source := Fichier.GetLigne;
        Fichier.Suivant;
        Elem.Conductivite := Fichier.GetLigneReel;
        Fichier.Suivant;
        Elem.MasseVol := Fichier.GetLigneReel;
        Fichier.Suivant;
        Elem.ChaleurMassWh := Fichier.GetLigneReel;
        Fichier.Suivant;
        Elem.ConductiviteEquiv := Fichier.GetLigneReel;
        Fichier.Suivant;
        Elem.Resistance := Fichier.GetLigneReel;
        Fichier.Suivant;
        Elem.Epaisseur := Fichier.GetLigneReel;
        Fichier.Suivant;
        Elem.ReadOnly := Fichier.GetLigneBool;
        Table.AddObject(Elem.nom, Elem);
        Fichier.Suivant;

        if not Elem.ReadOnly and (Elem.Identificateur < EcartStandard) then
          Elem.Identificateur := Elem.Identificateur + EcartStandard;

        if (Elem.Identificateur > IdMax) and Elem.ReadOnly then
          IdMax := Elem.Identificateur
        else if (Elem.Identificateur > IdMax) then
          IdMax := Elem.Identificateur - EcartStandard;
      end;
    end;
  finally
    Fichier.Free;
  end;
end;

end.
