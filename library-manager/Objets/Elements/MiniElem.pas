unit MiniElem;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, BarreAdresse, ExtCtrls, ComposantBibliotheque;

type
  TFormMiniElem = class(TForm)
    Image1: TImage;
    PanelGenerique: TPanel;
    LabelCategorie: TLabel;
    BarreCategorie: TFrameBarreAdresse;
    Panel4: TPanel;
    LabelOrigine: TLabel;
    LabelComplement: TLabel;
    LabelNom: TLabel;
    Panel5: TPanel;
    EditSource: TEdit;
    MemoComplement: TMemo;
    EditNom: TEdit;
    PanelClient: TPanel;
    PanelGauche: TPanel;
    zLabel15: TLabel;
    zLabel16: TLabel;
    zLabel17: TLabel;
    zLabel18: TLabel;
    zLabel19: TLabel;
    zLabel20: TLabel;
    zLabel21: TLabel;
    zLabel23: TLabel;
    zLabel24: TLabel;
    zLabel25: TLabel;
    zLabel26: TLabel;
    zLabel27: TLabel;
    Label12: TLabel;
    Label37: TLabel;
    EditResistance: TEdit;
    EditCoeffU: TEdit;
    EditConducEquiv: TEdit;
    EditMasseVol: TEdit;
    EditEpaisseur: TEdit;
    EditChaleurMassJ: TEdit;
    EditChaleurMassWh: TEdit;
    GroupBoxImage: TGroupBox;
    ImageElem: TImage;
  private
    { Déclarations privées }
    function GetComplement: string;
    procedure SetComplement(Chaine: string);
  public
    { Déclarations publiques }
    Deplace: Boolean;
    IComp: TComposantBibliotheque;
    property MComplement: string Read GetComplement Write SetComplement;
    procedure Affiche(comp: TComposantBibliotheque);

    Constructor Create(AOwner: TComponent); override;
  end;

var
  FormMiniElem: TFormMiniElem;

implementation

uses
  Elements, Utilitaires, constantes, Bibliotherm;
{$R *.dfm}

Constructor TFormMiniElem.Create;
begin
  inherited Create(AOwner);
  Deplace := False;
end;

procedure TFormMiniElem.Affiche(comp: TComposantBibliotheque);
begin
  if comp <> Nil then
    with comp as TElement do
    begin
      EditNom.Text := comp.Nom;
      MComplement := comp.Complement;
      MemoScrollUp(MemoComplement);
      EditSource.Text := comp.Source;
      BarreCategorie.Chemin := comp.categorie;

      EditResistance.Text := FormatFloat('0.###', Resistance, tfs);
      EditCoeffU.Text := FormatFloat('0.###', Conductivite, tfs);
      EditConducEquiv.Text := FormatFloat('0.###', ConductiviteEquiv, tfs);
      EditMasseVol.Text := FormatFloat('0', MasseVol, tfs);
      EditEpaisseur.Text := FormatFloat('0.#', Epaisseur, tfs);
      EditChaleurMassJ.Text := FormatFloat('0', ChaleurMassWh * 3600, tfs);
      EditChaleurMassWh.Text := FormatFloat('0.###', ChaleurMassWh, tfs);
      EditChaleurMassJ.Text := FormatFloat('0', ChaleurMassWh * 3600, tfs);
      if FileExists(MondossierImages + Image1) then
      begin
        GroupBoxImage.Visible := True;
        RetailleImage(ImageElem, MondossierImages + Image1,False);
        GroupBoxImage.ClientHeight := ImageElem.Height+ImageElem.Top+ImageElem.margins.bottom+2;
        ImageElem.Invalidate;
      end
      else
        GroupBoxImage.Visible := False;

    end
    else
    begin
      EditNom.Text := '';
      MComplement := '';
      MemoScrollUp(MemoComplement);
      EditSource.Text := '';
      BarreCategorie.Chemin := '';

      EditResistance.Text := '';
      EditCoeffU.Text := '';
      EditConducEquiv.Text := '';
      EditMasseVol.Text := '';
      EditEpaisseur.Text := '';
      EditChaleurMassJ.Text := '';
      EditChaleurMassWh.Text := '';
    end;
end;

function TFormMiniElem.GetComplement: string;
var
  I: integer;
begin
  Result := '';
  for I := 0 to MemoComplement.Lines.Count - 1 do
    Result := Result + MemoComplement.Lines[I] + '|';
end;

procedure TFormMiniElem.SetComplement(Chaine: string);
begin
  MemoComplement.Lines.Clear;
  while Chaine <> '' do
    MemoComplement.Lines.Add(GetItemPipe(Chaine));
end;

end.
