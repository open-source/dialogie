unit CadreElementsPleiades;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, CadreElements, StdCtrls, Buttons, ExtCtrls, ComCtrls, Menus,
  DHMulti, NCadreElements, BarreAdresse;

type
  TFrameElementPleiades = class(TFrameElement)
    BitBtnExp: TBitBtn;
    procedure BitBtnExpClick(Sender: TObject);
  private
    { Déclarations privées }
  public
    { Déclarations publiques }
  end;

implementation

uses
  Projet, BiblioElements, Elements;

{$R *.dfm}

procedure TFrameElementPleiades.BitBtnExpClick(Sender: TObject);
var
  comp: TElement;
  prec: word;
begin
  inherited;
  if Controle then
  begin
    comp := TElement.Create(Nom.Text, Categorie.Text, Complement.Text,
      Source.Text, CoeffU.Text, MasseVol.Text, ChaleurMassWh.Text,
      ConducEquiv.Text, Resistance.Text, epaisseur.Text);
    prec := mrYes;
    ProjetCourant.AjouteElem(comp, prec);
    BibElements.PoseDansArbre(comp, FormDHMulti.FComposants.TreeElem, False, True);

    FormDHMulti.FParois.AfficheParoi;
  end;
end;

end.
