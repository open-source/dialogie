unit NCadreElementsPleiades;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, NCadreElements, Menus, BarreAdresse, StdCtrls, Buttons, ExtCtrls,
  Elements, ComCtrls;

type
  TFrameElementPleiades = class(TFrameElement)
    BitBtnExp: TBitBtn;
    procedure BitBtnExpClick(Sender: TObject);
    procedure Export1Click(Sender: TObject);
    procedure FrameResize(Sender: TObject);
  private
    { Déclarations privées }
  public
    { Déclarations publiques }
    procedure Exporte(comp: TElement);
  end;

var
  FrameElementPleiades: TFrameElementPleiades;

implementation

uses
  Projet, BiblioElements, DHMulti, MainPleiades, ComposantBibliotheque;

{$R *.dfm}

procedure TFrameElementPleiades.BitBtnExpClick(Sender: TObject);
var
  comp: TElement;
begin
  inherited;
  if Controle then
  begin
    comp := CreerComposantBiblio as TElement;
    Exporte(comp);
  end;
end;

procedure TFrameElementPleiades.Exporte(comp: TElement);
var
  prec: word;
begin
  prec := mrYes;
  ProjetCourant.AjouteElem(comp, prec);
  BibElements.PoseDansArbre(comp, FormDHMulti.FComposants.TreeElem, False, True);

  FormDHMulti.FParois.AfficheParoi;
end;

procedure TFrameElementPleiades.Export1Click(Sender: TObject);
var
  i: integer;
  comp: TElement;
begin
  inherited;
  for i := Tree.Selectioncount - 1 downto 0 do
  begin
    comp := TElement.Create;
    comp.Assign(Tree.Selections[i].Data);
    Exporte(comp);
  end;
end;

procedure TFrameElementPleiades.FrameResize(Sender: TObject);
begin
  inherited;
  EditFiltre.Images := FormMainPleiades.ImageListNMenu;
  EditFiltre.RightButton.ImageIndex := 303;
end;

end.
