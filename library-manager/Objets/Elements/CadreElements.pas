unit CadreElements;

interface

uses
  Windows,
  Messages,
  SysUtils,
  Variants,
  Classes,
  Graphics,
  Controls,
  Forms,
  Dialogs,
  StdCtrls,
  Buttons,
  ExtCtrls,
  ComCtrls,
  Elements,
  Menus;

type
  TFrameElement = class(TFrame)
    ZGroupBox3:    TGroupBox;
    Tree:          TTreeView;
    ZGroupBox4:    TGroupBox;
    zLabel12:      TLabel;
    zLabel13:      TLabel;
    zLabel14:      TLabel;
    zLabel15:      TLabel;
    zLabel16:      TLabel;
    zLabel17:      TLabel;
    zLabel18:      TLabel;
    zLabel19:      TLabel;
    zLabel20:      TLabel;
    zLabel21:      TLabel;
    zLabel22:      TLabel;
    zLabel23:      TLabel;
    zLabel24:      TLabel;
    zLabel25:      TLabel;
    zLabel26:      TLabel;
    zLabel27:      TLabel;
    Label12:       TLabel;
    Label37:       TLabel;
    Categorie:     TComboBox;
    Nom:           TEdit;
    Complement:    TEdit;
    Source:        TEdit;
    zPanel4:       TPanel;
    BitBtnNewElem: TBitBtn;
    BitBtnSaveElem: TBitBtn;
    BitBtnCreerClasse: TBitBtn;
    Resistance:    Tedit;
    CoeffU:        Tedit;
    ConducEquiv:   Tedit;
    MasseVol:      Tedit;
    Epaisseur:     Tedit;
    ChaleurMassJ:  Tedit;
    ChaleurMassWh: Tedit;
    PopupComponent: TPopupMenu;
    Open1:         TMenuItem;
    Delete1:       TMenuItem;
    Export1:       TMenuItem;
    Rename1: TMenuItem;
    Splitter1: TSplitter;
    procedure TreeChange(Sender: TObject; Node: TTreeNode);
    procedure TreeClick(Sender: TObject);
    procedure TreeDragDrop(Sender, Source: TObject; X, Y: integer);
    procedure TreeDragOver(Sender, Source: TObject; X, Y: integer;
      State: TDragState; var Accept: boolean);
    procedure TreeMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: integer);
    procedure CategorieChange(Sender: TObject);
    procedure ChaleurMassJKeyUp(Sender: TObject; var Key: word; Shift: TShiftState);
    procedure ChaleurMassWhKeyUp(Sender: TObject; var Key: word;
      Shift: TShiftState);
    procedure BitBtnCreerClasseClick(Sender: TObject);
    procedure BitBtnSaveElemClick(Sender: TObject);
    procedure BitBtnNewElemClick(Sender: TObject);
    procedure TreeKeyUp(Sender: TObject; var Key: word; Shift: TShiftState);
    procedure ResistanceKeyUp(Sender: TObject; var Key: word; Shift: TShiftState);
    procedure CoeffUKeyUp(Sender: TObject; var Key: word; Shift: TShiftState);
    constructor Create(AOwner: TComponent); override;
    procedure EpaisseurKeyUp(Sender: TObject; var Key: word; Shift: TShiftState);
    procedure Open1Click(Sender: TObject);
    procedure Delete1Click(Sender: TObject);
    destructor Destroy; override;
    procedure Rename1Click(Sender: TObject);
    procedure PopupComponentPopup(Sender: TObject);
  private
    Modif: boolean;

    function ControleSauve(Forcee: boolean): boolean;
  protected
    Dragage:       TTreeNode;
    SauveReadOnly: boolean;
    function Controle: boolean;
  public
    ModificationAutorise: boolean;
    FormParent:        TForm;
    CheckHint:         boolean;
    MessagesInterface: TStringList;
    function AccepteDrag: boolean;
    procedure EffaceDragage;
    procedure Affiche(Elem: TElement);
  end;

implementation

uses
  System.UITypes,
  IzUtilitaires,
  BiblioElements,
  DialogsInternational,
  ComposantBibliotheque,
  IzApp;

{$R *.dfm}

destructor TFrameElement.Destroy;
begin
  MessagesInterFace.Free;
  inherited Destroy;
end;

function TFrameElement.AccepteDrag: boolean;
begin
  Result := (BibElements.ImageIndex = dragage.ImageIndex) or
    (dragage.ImageIndex = BibElements.ImageIndexDossier) or
    (Dragage.ImageIndex = BibElements.ImageIndexDossierS);
end;

constructor TFrameElement.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  SauveReadOnly := False;
  ModificationAutorise := True;
  CheckHint  := True;
  FormParent := nil;
  MessagesInterFace := TStringList.Create;
end;

procedure TFrameElement.Delete1Click(Sender: TObject);
var
  I: integer;
  TreeSource: TCustomTreeView;
begin
  if Dragage = nil then
    Exit;
  TreeSource := Dragage.TreeView;
  for I := TreeSource.Selectioncount - 1 downto 0 do
  begin
    Dragage := TreeSource.Selections[i];
    EffaceDragage;
  end;
end;

procedure TFrameElement.Affiche(Elem: TElement);
var
  Chaine: string;
  indice: integer;
begin
  BibElements.ConvertitCategorie(Elem);
  Nom.Text := Elem.Nom;
  Complement.Text := Elem.Complement;

  Str(Elem.Conductivite: 0: 2, Chaine);
  CoeffU.Text := chaine;
  Str(Elem.MasseVol: 0: 0, Chaine);
  MasseVol.Text := chaine;
  Str(Elem.ChaleurMassWh: 0: 3, Chaine);
  ChaleurMassWh.Text := chaine;
  Str(Elem.ChaleurMassWh * 3600: 0: 0, Chaine);
  ChaleurMassJ.Text := chaine;
  Str(Elem.ConductiviteEquiv: 0: 3, Chaine);
  ConducEquiv.Text := chaine;
  Str(Elem.Resistance: 0: 2, Chaine);
  Resistance.Text := chaine;
  Str(Elem.Epaisseur: 0: 1, Chaine);
  Epaisseur.Text := chaine;

  indice := Categorie.Items.IndexOf(Elem.Categorie);
  if Indice = -1 then
  begin
    BibElements.AjouteCategorie(Elem.Categorie, Categorie.items, Tree);
    indice := Categorie.Items.IndexOf(Elem.Categorie);
  end;
  Categorie.ItemIndex := Indice;

  Source.Text := Elem.Source;
  Modif := False;
end;

procedure TFrameElement.EffaceDragage;
begin
  //Cas des catégories
  if (Dragage.ImageIndex = BibElements.ImageIndexDossier) or
    (Dragage.ImageIndex = BibElements.ImageIndexDossierS) then
  begin
    if dragage.HasChildren then
      MessageDlgM(MessagesInterface[0], mtWarning, [mbOK], 0)
    else
    begin
      Categorie.items.Delete(Categorie.Items.IndexOf(dragage.Text));
      Tree.items.Delete(dragage);
    end;
  end
  else if (Dragage.ImageIndex = BibElements.imageIndex) or (TApp.isMaster and (Dragage.ImageIndex = BibElements.ImageIndexRO )) then
  begin
    if (MessageDlgM(MessagesInterface[1] + dragage.Text + MessagesInterface[2],
      mtConfirmation, mbOkCancel, 0) = mrOk) and
      BibElements.Efface(dragage.Text) then
    begin
      Tree.Items.Delete(dragage);
      BibElements.SauveListe();
    end;
  end;
end;

procedure TFrameElement.EpaisseurKeyUp(Sender: TObject; var Key: word;
  Shift: TShiftState);
var
  Erreur, ErreurBis: integer;
  Chaine:  string;
  Ep, Res: double;
begin
  Val(ConvertPoint(Resistance.Text), Res, erreur);
  Val(ConvertPoint(Epaisseur.Text), Ep, erreurBis);
  if (erreur = 0) and (erreurBis = 0) and (Res <> 0) then
  begin
    Str(Ep * 0.01 / Res: 0: 3, chaine);
    conducEquiv.Text := chaine;
  end;
end;

procedure TFrameElement.Open1Click(Sender: TObject);
var
  Noeud: TTreeNode;
  Elem: TElement;
  Ok: boolean;
begin
  if Modif then
    Ok := ControleSauve(False)
  else
    Ok := True;
  if not Ok then
    Exit;
  Noeud := Tree.Selected;
  if Noeud <> nil then
  begin
    Elem := BibElements.Get(Noeud.Text);
    if Elem <> nil then
      Affiche(Elem);
  end;
end;

procedure TFrameElement.PopupComponentPopup(Sender: TObject);
begin
  Rename1.Enabled :=  Tree.Selected.ImageIndex <= 1;
  Open1.Enabled := Not Rename1.Enabled;

end;

procedure TFrameElement.Rename1Click(Sender: TObject);
var
  NouveauNom : String;
  Noeud: TTreeNode;
begin
  if Dragage = nil then
    Exit;
  NouveauNom := Dragage.text;
  if InputQueryM(MessagesInterface[6], MessagesInterface[6], NouveauNom) then
  Begin
    dragage.Text := NouveauNom;
    Noeud := dragage.getFirstChild;
    while Noeud <> Nil do
    Begin
      TComposantBibliotheque(Noeud.data).categorie := NouveauNom;
      Noeud := Noeud.getNextSibling;
    End;
  End;
end;

procedure TFrameElement.ResistanceKeyUp(Sender: TObject; var Key: word;
  Shift: TShiftState);
var
  Erreur, ErreurBis: integer;
  Chaine:  string;
  Ep, Res: double;
begin
  Val(ConvertPoint(Resistance.Text), Res, erreur);
  Val(ConvertPoint(Epaisseur.Text), Ep, erreurBis);
  if (Erreur = 0) and (Res <> 0) then
  begin
    Str(1 / Res: 0: 3, chaine);
    CoeffU.Text := Chaine;
  end;
  if (erreur = 0) and (erreurBis = 0) and (Res <> 0) then
  begin
    Str(Ep * 0.01 / Res: 0: 3, chaine);
    conducEquiv.Text := chaine;
  end;
end;

procedure TFrameElement.CoeffUKeyUp(Sender: TObject; var Key: word; Shift: TShiftState);
var
  ConductiviteU, Resist, Epais: double;
  Erreur, ErreurBis: integer;
  chaine: string;
begin
  Val(ConvertPoint(CoeffU.Text), ConductiviteU, Erreur);
  if (Erreur = 0) and (conductiviteU <> 0) then
  begin
    Str(1 / ConductiviteU: 0: 2, Chaine);
    Resistance.Text := chaine;
  end;

  Val(ConvertPoint(Resistance.Text), Resist, erreur);
  Val(ConvertPoint(Epaisseur.Text), Epais, erreurBis);
  if (erreur = 0) and (erreurBis = 0) and (Resist <> 0) then
  begin
    Str(Epais * 0.01 / Resist: 0: 3, chaine);
    conducEquiv.Text := chaine;
  end;
end;

function TFrameElement.Controle: boolean;
var
  Erreur: integer;
begin
  if EstNumerique(Nom.Text) then
    Nom.Text := '_' + Nom.Text;
  Erreur := 0;
  if Categorie.Text = '' then
  begin
    FormParent.FocusControl(Categorie);
    Erreur := 1;
  end
  else if ControlNum(nom.Text, 4, 256, 'C') <> nom.Text then
  begin
    FormParent.FocusControl(nom);
    Erreur := 2;
  end
  else if ControlNum(Complement.Text, 0, 256, 'C') <> Complement.Text then
  begin
    FormParent.FocusControl(Complement);
    Erreur := 3;
  end
  else if ControlNum(ConvertPoint(CoeffU.Text), 0.01, 10000, 'R') <>
    ConvertPoint(CoeffU.Text) then
  begin
    FormParent.FocusControl(CoeffU);
    Erreur := 4;
  end
  else if ControlNum(ConvertPoint(Epaisseur.Text), 0.1, 10000, 'R') <>
    ConvertPoint(Epaisseur.Text) then
  begin
    FormParent.FocusControl(Epaisseur);
    Erreur := 7;
  end;
  if Erreur <> 0 then
  begin
    MessageDlgM(MessagesInterface[8 + Erreur], mtWarning, [mbOK], 0);
    Result := False;
  end
  else
    Result := True;
end;

function TFrameElement.ControleSauve(Forcee: boolean): boolean;
var
  reponse: word;
  comp:  TElement;
  Silencieux: boolean;
  Noeud: TTreeNode;
begin
  if not ModificationAutorise then
  begin
    MessageDlgM(MessagesInterface[7], mtWarning, [mbOK], 0);
    Result := False;
    Exit;
  end;
  Result := False;
  FormParent.FocusControl(Tree);
  if not Forcee then
    Reponse := MessageDlgM(MessagesInterface[5] + Nom.Text + '''',
      mtWarning, [mbCancel, mbNo, mbYes], 0)
  else
    Reponse := mrYes;
  if Reponse = mrCancel then
    Exit;
  Result := True;
  if (Reponse = mrYes) then
    if Controle then
    begin
      comp := TElement.Create(Nom.Text, Categorie.Text, Complement.Text,
        Source.Text, CoeffU.Text, MasseVol.Text, ChaleurMassWh.Text,
        ConducEquiv.Text, Resistance.Text, epaisseur.Text, '');
      comp.ReadOnly := CtrlDown and ShiftDown;
      Silencieux := False;
      if BibElements.Ajoute(comp, Silencieux) then
      begin
        Noeud := BibElements.PoseDansArbre(comp, Tree, True, True);
        Tree.Selected := Noeud;
      end
      else
        comp.Free;
    end
    else
      Result := False;
  Modif := False;
  BibElements.SauveListe;
end;

procedure TFrameElement.BitBtnCreerClasseClick(Sender: TObject);
var
  Valeur: string;
begin
  if InputQueryM(MessagesInterface[6], MessagesInterface[6], valeur) and
    (Valeur <> '') then
    BibElements.AjouteCategorie(Valeur, Categorie.items, Tree);
end;

procedure TFrameElement.BitBtnNewElemClick(Sender: TObject);
var
  Ok: boolean;
begin
  if Modif then
    Ok := ControleSauve(False)
  else
    Ok := True;
  if Ok then
  begin
    nom.Text := '';
    complement.Text := '';
    Source.Text := '';
    CoeffU.Text := '';
    MasseVol.Text := '';
    ChaleurMassWh.Text := '';
    ChaleurMassJ.Text := '';
    Resistance.Text := '';
    Epaisseur.Text := '';
    ConducEquiv.Text := '';
    Categorie.ItemIndex := -1;
    Modif := False;
  end;
end;

procedure TFrameElement.BitBtnSaveElemClick(Sender: TObject);
begin
  ControleSauve(True);
end;

procedure TFrameElement.CategorieChange(Sender: TObject);
begin
  Modif := True;
end;

procedure TFrameElement.ChaleurMassJKeyUp(Sender: TObject; var Key: word;
  Shift: TShiftState);
var
  Reel: double;
  Erreur: integer;
  Chaine: string;
begin
  Val(ConvertPoint(ChaleurMassJ.Text), Reel, erreur);
  if erreur = 0 then
  begin
    Str(Reel / 3600: 0: 3, chaine);
    chaleurMassWh.Text := chaine;
  end;
end;

procedure TFrameElement.ChaleurMassWhKeyUp(Sender: TObject; var Key: word;
  Shift: TShiftState);
var
  Reel: double;
  Erreur: integer;
  Chaine: string;
begin
  Val(ConvertPoint(ChaleurMassWh.Text), Reel, erreur);
  if erreur = 0 then
  begin
    Str(Reel * 3600: 0: 0, chaine);
    chaleurMassJ.Text := chaine;
  end;
end;

procedure TFrameElement.TreeChange(Sender: TObject; Node: TTreeNode);
var
  Chaine: string;
begin
  Str(BibElements.Count, chaine);
  if CheckHint then
    Tree.Hint := Chaine + MessagesInterface[8]
  else
    Tree.Hint := '';
end;

procedure TFrameElement.TreeClick(Sender: TObject);
var
  Noeud: TTreeNode;
  Elem: TElement;
  Ok: boolean;
begin
  if Modif then
    Ok := ControleSauve(False)
  else
    Ok := True;
  if not Ok then
    Exit;
  Elem  := nil;
  Noeud := Tree.Selected;
  if Noeud <> nil then
    Elem := BibElements.Get(Noeud.Text);
  if Elem <> nil then
    Affiche(Elem);
  Modif := False;
end;

procedure TFrameElement.TreeDragDrop(Sender, Source: TObject; X, Y: integer);
var
  Courant: TTreeNode;
  Elem: TElement;
begin
  if (Tree.dropTarget.ImageIndex = BibElements.ImageIndexDossier) or
    (Tree.dropTarget.ImageIndex = BibElements.ImageIndexDossierS) then
  begin
    Elem := BibElements.Get(dragage.Text);
    Elem.Categorie := Tree.DropTarget.Text;
    Tree.Items.Delete(dragage);
    Courant := BibElements.PoseDansArbre(Elem, Tree, True, False);
    Tree.alphaSort;
    Tree.FullCollapse;
    Tree.Selected := Courant;
  end;
end;

procedure TFrameElement.TreeDragOver(Sender, Source: TObject;
  X, Y: integer; State: TDragState; var Accept: boolean);
begin
  if Dragage <> nil then
    Accept := (dragage.ImageIndex = BibElements.ImageIndex)
  else
    Accept := False;
end;

procedure TFrameElement.TreeKeyUp(Sender: TObject; var Key: word; Shift: TShiftState);
begin
  if (Key = 46) and (Tree.Selected <> nil) then
    EffaceDragage
  else
    Tree.OnClick(Self);
end;

procedure TFrameElement.TreeMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: integer);
begin
  Dragage := TTreeView(Sender).GetNodeAt(X, Y);
end;

end.