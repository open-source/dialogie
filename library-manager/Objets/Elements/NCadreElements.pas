unit NCadreElements;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, CadreBib, Menus, BarreAdresse, StdCtrls, Buttons, ExtCtrls, ComCtrls,
  ComposantBibliotheque;

type
  TFrameElement = class(TFrameBib)
    PanelGauche: TPanel;
    zLabel15: TLabel;
    zLabel16: TLabel;
    zLabel17: TLabel;
    zLabel18: TLabel;
    zLabel19: TLabel;
    zLabel20: TLabel;
    zLabel21: TLabel;
    zLabel23: TLabel;
    zLabel24: TLabel;
    zLabel25: TLabel;
    zLabel26: TLabel;
    zLabel27: TLabel;
    Label12: TLabel;
    Label37: TLabel;
    EditResistance: TEdit;
    EditCoeffU: TEdit;
    EditConducEquiv: TEdit;
    EditMasseVol: TEdit;
    EditEpaisseur: TEdit;
    EditChaleurMassJ: TEdit;
    EditChaleurMassWh: TEdit;
    PanelClient: TPanel;
    ImageElem: TImage;
    GroupBoxImage: TGroupBox;
    PanelDroit: TPanel;
    EditNomImage: TEdit;
    procedure EditResistanceKeyUp(Sender: TObject; var Key: word;
      Shift: TShiftState);
    procedure EditCoeffUKeyUp(Sender: TObject; var Key: word;
      Shift: TShiftState);
    procedure EditChaleurMassJKeyUp(Sender: TObject; var Key: word;
      Shift: TShiftState);
    procedure EditChaleurMassWhKeyUp(Sender: TObject; var Key: word;
      Shift: TShiftState);
    procedure BitBtnNewClick(Sender: TObject);
    procedure TreeMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
    procedure AfficheImageElem(NomImage: String);
    procedure EditEpaisseurKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Déclarations privées }
    NoeudPrec: TTreeNode;
  protected
    function Controle: boolean; override;
  public
    { Déclarations publiques }
    function CreerComposantBiblio: TComposantBibliotheque; override;
    procedure Affiche(comp: TComposantBibliotheque; Survol: boolean); override;
  end;

var
  FrameElement: TFrameElement;

implementation

uses Utilitaires, Elements, dialogsinternational, BiblioElements, Constantes;
{$R *.dfm}

procedure TFrameElement.BitBtnNewClick(Sender: TObject);
var
  Ok: boolean;
begin
  if Modif then
    Ok := ControleSauve(False)
  else
    Ok := True;
  if Ok then
  begin
    EditResistance.Text := '';
    EditCoeffU.Text := '';
    EditConducEquiv.Text := '';
    EditMasseVol.Text := '';
    EditEpaisseur.Text := '';
    EditChaleurMassJ.Text := '';
    EditChaleurMassWh.Text := '';
    EditNomImage.Text := '';
    inherited;
  end;
end;

procedure TFrameElement.EditChaleurMassJKeyUp(Sender: TObject; var Key: word;
  Shift: TShiftState);
var
  Reel: double;
  Erreur: Integer;
  Chaine: string;
begin
  Val(ConvertPoint(EditChaleurMassJ.Text), Reel, Erreur);
  if Erreur = 0 then
  begin
    Str(Reel / 3600: 0: 3, Chaine);
    EditChaleurMassWh.Text := Chaine;
  end;
end;

procedure TFrameElement.EditChaleurMassWhKeyUp(Sender: TObject; var Key: word;
  Shift: TShiftState);
var
  Reel: double;
  Erreur: Integer;
  Chaine: string;
begin
  Val(ConvertPoint(EditChaleurMassWh.Text), Reel, Erreur);
  if Erreur = 0 then
  begin
    Str(Reel * 3600: 0: 0, Chaine);
    EditChaleurMassJ.Text := Chaine;
  end;
end;

procedure TFrameElement.EditCoeffUKeyUp(Sender: TObject; var Key: word;
  Shift: TShiftState);
var
  ConductiviteU, Resist, Epais: double;
  Erreur, ErreurBis: Integer;
  Chaine: string;
begin
  Val(ConvertPoint(EditCoeffU.Text), ConductiviteU, Erreur);
  if (Erreur = 0) and (ConductiviteU <> 0) then
  begin
    Str(1 / ConductiviteU: 0: 3, Chaine);
    EditResistance.Text := Chaine;
  end;

  Val(ConvertPoint(EditResistance.Text), Resist, Erreur);
  Val(ConvertPoint(EditEpaisseur.Text), Epais, ErreurBis);
  if (Erreur = 0) and (ErreurBis = 0) and (Resist <> 0) then
  begin
    Str(Epais * 0.01 / Resist: 0: 3, Chaine);
    EditConducEquiv.Text := Chaine;
  end;
end;

procedure TFrameElement.EditEpaisseurKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  EditCoeffUKeyUp(Sender,key,Shift);
end;

procedure TFrameElement.EditResistanceKeyUp(Sender: TObject; var Key: word;
  Shift: TShiftState);
var
  Erreur, ErreurBis: Integer;
  Chaine: string;
  Ep, Res: double;
begin
  Val(ConvertPoint(EditResistance.Text), Res, Erreur);
  Val(ConvertPoint(EditEpaisseur.Text), Ep, ErreurBis);
  if (Erreur = 0) and (Res <> 0) then
  begin
    Str(1 / Res: 0: 3, Chaine);
    EditCoeffU.Text := Chaine;
  end;
  if (Erreur = 0) and (ErreurBis = 0) and (Res <> 0) then
  begin
    Str(Ep * 0.01 / Res: 0: 3, Chaine);
    EditConducEquiv.Text := Chaine;
  end;
end;

procedure TFrameElement.TreeMouseMove(Sender: TObject; Shift: TShiftState;
  X, Y: Integer);
var
  Noeud: TTreeNode;
  Image: String;
begin
  inherited;
  if not (htOnItem in HitTest) or CompAEteAffiche then
  begin
    NoeudPrec := Nil;
    Exit;
  end;
  Noeud := Tree.GetNodeAt(X, Y);
  if (Noeud <> Nil) and (Noeud.data = nil) and (Noeud <> NoeudPrec) then
  begin
    Affiche(CompVide,True);
    CompAffiche := CompVide;
    Image := GetImage1Dossier(Noeud);
    AfficheImageElem(Image);
    NoeudPrec := Noeud;
  end;
end;

function TFrameElement.CreerComposantBiblio: TComposantBibliotheque;
begin
  Result := TElement.Create(EditNom.Text, BarreCategorie.Chemin, MComplement,
    EditSource.Text, EditCoeffU.Text, EditMasseVol.Text,
    EditChaleurMassWh.Text, EditConducEquiv.Text, EditResistance.Text,
    EditEpaisseur.Text, EditNomImage.Text);
end;

function TFrameElement.Controle: boolean;
var
  Erreur: Integer;
begin
  Erreur := 0;
  if not inherited Controle then
    Exit(False);

  if HorsLimite(EditCoeffU, 0.01, 10000, 'R') then
    Erreur := 4
  else if HorsLimite(EditMasseVol, 0.1, 10000, 'R') then
    Erreur := 5
  else if HorsLimite(EditChaleurMassJ, 0.1, 50000, 'R') then
    Erreur := 6
  else if HorsLimite(EditEpaisseur, 0.1, 10000, 'R') then
    Erreur := 7;

  if Erreur <> 0 then
  begin
    MessageDlgM(MessagesInterface[8 + Erreur], mtWarning, [mbOK], 0);
    Result := False;
  end
  else
    Result := True;
end;

procedure TFrameElement.AfficheImageElem(NomImage: String);
begin
  if (NomImage <> '_') and FileExists(MondossierImages + NomImage) then
  begin
    GroupBoxImage.Visible := True;
    if EditNomImage.Text <> NomImage then
      AfficheImage(ImageElem, MondossierImages + NomImage, True);
    GroupBoxImage.ClientHeight := ImageElem.Height + ImageElem.Top +
      ImageElem.margins.bottom + 2;
    ImageElem.Invalidate;
  end
  else
    GroupBoxImage.Visible := False;
end;

procedure TFrameElement.Affiche(comp: TComposantBibliotheque; Survol: boolean);
var
  Key: word;
begin
  inherited Affiche(comp, Survol);
  if comp <> Nil then
    with comp as TElement do
    begin
      EditResistance.Text := FormatFloat('0.###', Resistance, tfs);
      EditCoeffU.Text := FormatFloat('0.###', Conductivite, tfs);
      EditConducEquiv.Text := FormatFloat('0.###', ConductiviteEquiv, tfs);
      EditMasseVol.Text := FormatFloat('0', MasseVol, tfs);
      EditEpaisseur.Text := FormatFloat('0.#', Epaisseur, tfs);
      EditChaleurMassJ.Text := FormatFloat('0', ChaleurMassWh * 3600, tfs);
      EditChaleurMassWh.Text := FormatFloat('0.###', ChaleurMassWh, tfs);
      Key := 13;
      EditChaleurMassJ.OnKeyUp(Self, Key, []);
      AfficheImageElem(Image1);
      EditNomImage.Text := Image1;
    end
    else
    begin
      EditResistance.Text := '';
      EditCoeffU.Text := '';
      EditConducEquiv.Text := '';
      EditMasseVol.Text := '';
      EditEpaisseur.Text := '';
      EditChaleurMassJ.Text := '';
      EditChaleurMassWh.Text := '';
      EditNomImage.Text := '';
      GroupBoxImage.Visible := False;
    end;
  Modif := False;
  ModeAffichage := False;
end;

end.
