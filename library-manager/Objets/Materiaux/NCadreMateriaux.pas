unit NCadreMateriaux;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, CadreBib, Menus, BarreAdresse, StdCtrls, Buttons, ExtCtrls, ComCtrls,
  ComposantBibliotheque, ImgList;

type
  TFrameMateriaux = class(TFrameBib)
    zLabel4: TLabel;
    zLabel5: TLabel;
    zLabel6: TLabel;
    zLabel7: TLabel;
    zLabel8: TLabel;
    zLabel9: TLabel;
    zLabel10: TLabel;
    Label36: TLabel;
    EditConductivite: TEdit;
    EditMasseVol: TEdit;
    EditChaleurMassJ: TEdit;
    EditChaleurMassWh: TEdit;
    LabelEffusivite: TLabel;
    EditEffusivite: TEdit;
    Label1: TLabel;
    EditDiffusivite: TEdit;
    procedure EditChaleurMassJKeyUp(Sender: TObject; var Key: word;
      Shift: TShiftState);
    procedure EditChaleurMassWhKeyUp(Sender: TObject; var Key: word;
      Shift: TShiftState);
    procedure BitBtnNewClick(Sender: TObject);
    procedure ComboCategorieChange(Sender: TObject);
  private
    { Déclarations privées }
    procedure RemplirEffusiviteDiffusivite;
  protected
    function Controle: boolean; override;
  public
    { Déclarations publiques }
    function CreerComposantBiblio: TComposantBibliotheque; override;
    procedure Affiche(comp: TComposantBibliotheque; Survol: boolean); override;
  end;

var
  FrameMateriaux: TFrameMateriaux;

implementation

uses Utilitaires, Materiaux, Constantes, BiblioMateriaux, DialogsInternational;
{$R *.dfm}

procedure TFrameMateriaux.BitBtnNewClick(Sender: TObject);
var
  Ok: boolean;
begin
  if Modif then
    Ok := ControleSauve(False)
  else
    Ok := True;
  if Ok then
  begin
    EditConductivite.Text := '';
    EditMasseVol.Text := '';
    EditChaleurMassJ.Text := '';
    EditChaleurMassWh.Text := '';
    inherited;
  end;
end;

procedure TFrameMateriaux.EditChaleurMassJKeyUp(Sender: TObject; var Key: word;
  Shift: TShiftState);
var
  Reel: double;
  Erreur: integer;
  Chaine: string;
begin
  Val(ConvertPoint(EditChaleurMassJ.Text), Reel, Erreur);
  if Erreur = 0 then
  begin
    Str(Reel / 3600: 0: 3, Chaine);
    EditChaleurMassWh.Text := Chaine;
  end;
end;

procedure TFrameMateriaux.EditChaleurMassWhKeyUp(Sender: TObject;
  var Key: word; Shift: TShiftState);
var
  Reel: double;
  Erreur: integer;
  Chaine: string;
begin
  Val(ConvertPoint(EditChaleurMassWh.Text), Reel, Erreur);
  if Erreur = 0 then
  begin
    Str(Reel * 3600: 0: 0, Chaine);
    EditChaleurMassJ.Text := Chaine;
  end;
end;

function TFrameMateriaux.CreerComposantBiblio: TComposantBibliotheque;
begin
  Result := TMateriau.Create(EditNom.Text, BarreCategorie.Chemin, MComplement,
    EditSource.Text, EditConductivite.Text, EditMasseVol.Text,
    EditChaleurMassJ.Text, EditChaleurMassWh.Text);
end;

procedure TFrameMateriaux.Affiche(comp: TComposantBibliotheque;
  Survol: boolean);
var
  Key: word;
begin
  inherited Affiche(comp, Survol);
  if comp <> Nil then
    with comp as TMateriau do
    begin
      EditConductivite.Text := FormatFloat('0.###', Conductivite, tfs);
      EditMasseVol.Text := FormatFloat('0', MasseVol, tfs);
      EditChaleurMassJ.Text := FormatFloat('0', ChaleurMassJ, tfs);
      Key := 13;
      EditChaleurMassJ.OnKeyUp(Self, Key, []);
    end
    else
    begin
      EditConductivite.Text := '';
      EditMasseVol.Text := '';
      EditChaleurMassJ.Text := '';
      EditChaleurMassWh.Text := '';
    end;
  ModeAffichage := False;
end;

procedure TFrameMateriaux.ComboCategorieChange(Sender: TObject);
begin
  inherited;
  RemplirEffusiviteDiffusivite;
end;

function TFrameMateriaux.Controle: boolean;
var
  Erreur: integer;
begin
  Erreur := 0;
  if not inherited Controle then
    Exit(False);
  if HorsLimite(EditConductivite, 0, Maxlongint, 'R') then
    Erreur := 4
  else if HorsLimite(EditMasseVol, 0, Maxlongint, 'R') then
    Erreur := 5
  else if HorsLimite(EditChaleurMassJ, 0, Maxlongint, 'R') then
    Erreur := 6;

  if Erreur <> 0 then
  begin
    MessageDlgM(MessagesInterface[8 + Erreur], mtWarning, [mbOK], 0);
    Result := False;
  end
  else
    Result := True;
end;

procedure TFrameMateriaux.RemplirEffusiviteDiffusivite;
var
  lambda, Rho, c: double;
  Erreur1, Erreur2, Erreur3: integer;
  Effusivite, Diffusivite: double;
begin
  Val(EditConductivite.Text, lambda, Erreur1);
  Val(EditMasseVol.Text, Rho, Erreur2);
  Val(EditChaleurMassJ.Text, c, Erreur3);
  if (Erreur1 = 0) and (Erreur2 = 0) and (Erreur3 = 0) then
  begin
    Effusivite := Sqrt(lambda * Rho * c);
    if (Rho * c) <> 0 then
      Diffusivite := lambda / (Rho * c)
    else
      Diffusivite := 0;
    EditEffusivite.Text := FormatFloat('0', Effusivite, tfs);
    EditDiffusivite.Text := FormatFloat('0.###E+', Diffusivite, tfs);
  end
  else
  begin
    EditEffusivite.Text := '';
    EditDiffusivite.Text := '';
  end;
end;

end.
