unit CadreMateriauxPleiades;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, NCadreMateriaux, Menus, StdCtrls, Buttons, ExtCtrls, ComCtrls;

type
  TFrameMateriauxPleiades = class(TFrameMateriaux)
    BitBtnExp: TBitBtn;
    procedure BitBtnExpClick(Sender: TObject);
  private
    { Déclarations privées }
  public
    { Déclarations publiques }
  end;

Var
  FrameMateriauxPleiades : TFrameMateriauxPleiades;

implementation

uses
  Materiaux, Projet, DHMulti, DialogsInternational, Translateur,
  BiblioMateriaux;

{$R *.dfm}

procedure TFrameMateriauxPleiades.BitBtnExpClick(Sender: TObject);
var
  comp: TMateriau;
  prec: word;
begin
  inherited;
  if Controle then
  begin
    comp := TMateriau.Create(EditNom.Text, BarreCategorie.Chemin, EditComplement.Text,
      EditSource.Text, EditConductivite.Text,EditMasseVol.Text, EditChaleurMassJ.Text,
      EditchaleurMassWh.Text);
    prec := mrYes;
    ProjetCourant.AjouteMat(comp, prec);
    BibMateriaux.PoseDansArbre(comp, FormDHMulti.FComposants.TreeMat, False, True);

    FormDHMulti.FParois.AfficheParoi;
  end;
end;

end.
