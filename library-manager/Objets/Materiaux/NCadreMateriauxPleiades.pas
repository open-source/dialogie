unit NCadreMateriauxPleiades;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, NCadreMateriaux, Menus, BarreAdresse, StdCtrls, Buttons, ExtCtrls,
  ComCtrls, ImgList, Materiaux;

type
  TFrameMateriauxPleiades = class(TFrameMateriaux)
    BitBtnExp: TBitBtn;
    procedure BitBtnExpClick(Sender: TObject);
    procedure Export1Click(Sender: TObject);
    procedure FrameResize(Sender: TObject);
  private
    { Déclarations privées }
    procedure Exporte(comp: TMateriau);
  public
    { Déclarations publiques }
  end;

var
  FrameMateriauxPleiades: TFrameMateriauxPleiades;

implementation

uses DHMulti, Projet, BiblioMateriaux, MainPleiades;

{$R *.dfm}

procedure TFrameMateriauxPleiades.BitBtnExpClick(Sender: TObject);
var
  comp: TMateriau;
begin
  inherited;
  if Controle then
  begin
    comp := CreerComposantBiblio as TMateriau;
    Exporte(comp);
  end;
end;

procedure TFrameMateriauxPleiades.Exporte(comp: TMateriau);
var
  prec: word;
begin
  prec := mrYes;
  ProjetCourant.AjouteMat(comp, prec);
  BibMateriaux.PoseDansArbre(comp, FormDHMulti.FComposants.TreeMat, False, True);
  FormDHMulti.FParois.AfficheParoi;
end;


procedure TFrameMateriauxPleiades.Export1Click(Sender: TObject);
var
  i: integer;
  comp: TMateriau;
begin
  inherited;
  for i := Tree.Selectioncount - 1 downto 0 do
  begin
    comp := TMateriau.Create;
    comp.Assign(Tree.Selections[i].Data);
    Exporte(comp);
  end;
end;

procedure TFrameMateriauxPleiades.FrameResize(Sender: TObject);
begin
  inherited;
  EditFiltre.Images := FormMainPleiades.ImageListNMenu;
  EditFiltre.RightButton.ImageIndex := 303;
end;

end.
