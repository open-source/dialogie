unit MiniMat;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, BarreAdresse, ExtCtrls, ComposantBibliotheque;

type
  TFormMiniMat = class(TForm)
    PanelGenerique: TPanel;
    LabelCategorie: TLabel;
    BarreCategorie: TFrameBarreAdresse;
    Panel4: TPanel;
    LabelOrigine: TLabel;
    LabelComplement: TLabel;
    LabelNom: TLabel;
    Panel5: TPanel;
    EditSource: TEdit;
    MemoComplement: TMemo;
    EditNom: TEdit;
    zLabel4: TLabel;
    zLabel5: TLabel;
    zLabel6: TLabel;
    zLabel7: TLabel;
    zLabel8: TLabel;
    zLabel9: TLabel;
    zLabel10: TLabel;
    Label36: TLabel;
    LabelEffusivite: TLabel;
    Label1: TLabel;
    EditConductivite: TEdit;
    EditMasseVol: TEdit;
    EditChaleurMassJ: TEdit;
    EditChaleurMassWh: TEdit;
    EditEffusivite: TEdit;
    EditDiffusivite: TEdit;
    Image1: TImage;
  private
    { Déclarations privées }
    function GetComplement: string;
    procedure SetComplement(Chaine: string);
    procedure RemplirEffusiviteDiffusivite;
  public
    { Déclarations publiques }
    Deplace : Boolean;
    IComp : TComposantBibliotheque;
    property MComplement: string Read GetComplement Write SetComplement;
    procedure Affiche(comp: TComposantBibliotheque);

    Constructor Create(AOwner : TComponent);override;
  end;

var
  FormMiniMat: TFormMiniMat;

implementation

uses
  Materiaux, Utilitaires, constantes;

{$R *.dfm}

Constructor TFormMiniMat.Create;
begin
  inherited Create(AOwner);
  Deplace := False;
end;

procedure TFormMiniMat.Affiche(comp: TComposantBibliotheque);
begin
  if comp <> Nil then
    with comp as TMateriau do
    begin
      EditNom.Text := comp.Nom;
      MComplement := comp.Complement;
      MemoScrollUp(MemoComplement);
      EditSource.Text := comp.Source;
      BarreCategorie.Chemin := comp.categorie;
      EditConductivite.Text := FormatFloat('0.###', Conductivite, tfs);
      EditMasseVol.Text := FormatFloat('0', MasseVol, tfs);
      EditChaleurMassJ.Text := FormatFloat('0', ChaleurMassJ, tfs);
      EditChaleurMassWh.Text := FormatFloat('0.000', ChaleurMassJ/3600, tfs);
      RemplirEffusiviteDiffusivite;
    end
    else
    begin
      EditNom.Text := '';
      MComplement := '';
      MemoScrollUp(MemoComplement);
      EditSource.Text := '';
      BarreCategorie.Chemin := '';
      EditConductivite.Text := '';
      EditMasseVol.Text := '';
      EditChaleurMassJ.Text := '';
      EditChaleurMassWh.Text := '';
    end;
end;

function TFormMiniMat.GetComplement: string;
var
  I: integer;
begin
  Result := '';
  for I := 0 to MemoComplement.Lines.Count - 1 do
    Result := Result + MemoComplement.Lines[I] + '|';
end;

procedure TFormMiniMat.SetComplement(Chaine: string);
begin
  MemoComplement.Lines.Clear;
  while Chaine <> '' do
    MemoComplement.Lines.Add(GetItemPipe(Chaine));
end;

procedure TFormMiniMat.RemplirEffusiviteDiffusivite;
var
  lambda, Rho, c: double;
  Erreur1, Erreur2, Erreur3: integer;
  Effusivite, Diffusivite: double;
begin
  Val(EditConductivite.Text, lambda, Erreur1);
  Val(EditMasseVol.Text, Rho, Erreur2);
  Val(EditChaleurMassJ.Text, c, Erreur3);
  if (Erreur1 = 0) and (Erreur2 = 0) and (Erreur3 = 0) then
  begin
    Effusivite := Sqrt(lambda * Rho * c);
    if (Rho * c) <> 0 then
      Diffusivite := lambda / (Rho * c)
    else
      Diffusivite := 0;
    EditEffusivite.Text := FormatFloat('0', Effusivite, tfs);
    EditDiffusivite.Text := FormatFloat('0.###E+', Diffusivite, tfs);
  end
  else
  begin
    EditEffusivite.Text := '';
    EditDiffusivite.Text := '';
  end;
end;

end.
