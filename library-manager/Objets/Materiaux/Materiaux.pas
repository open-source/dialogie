unit Materiaux;

interface

uses
  // Delphi
  Windows,
  Messages,
  SysUtils,
  Classes,
  Graphics,
  Controls,
  Forms,
  Dialogs,
  ComCtrls,
  Buttons,
  ComposantBibliotheque,
  GestionSauvegarde,
  CreerSauvegarde,
  Xml.XMLIntf;

type
  TMateriau = class(TComposantBibliotheque)
  private
    const FIdVersion : integer = 1;

  protected

    procedure innerLoadXml(node : IXMLNode); override;
    procedure innerSaveXml(node : IXMLNode); override;
  public
    Conductivite, MasseVol, ChaleurMassJ, ChaleurMassWh : double;
    constructor Create();override;
    constructor Create(Nom_, categorie_, complement_, source_,
      conductivite_, massevolumique_, ChaleurMassiqueJ_, ChaleurMassiqueWh_ :
      string); overload;
    function Sauve(var Contenu : TCreationSauvegarde) : boolean; override;
    function Charge(var Contenu : TSauvegarde; NumVersion : integer) : boolean; override;
    procedure Assign(Mat : TMateriau);
    function ValeursIdentiques(comp : TComposantBibliotheque) : boolean; override;
    class function idVersion : integer; override;
  end;

  TListeMateriau = class(TList)
  private
    function GetMateriau(const i : integer) : TMateriau;
    procedure SetMateriau(const i : integer; const Valeur : TMateriau);
  public
    property objets[const index : integer] : TMateriau read GetMateriau write SetMateriau;
    default;
    procedure ClearAndFree;
    function IndexOf(Chaine : string) : integer; overload;
  end;


implementation

uses
  IzUtilitaires, XmlUtil;

class function TMateriau.idVersion : integer;
begin
  Result := FIdVersion;
end;

function TMateriau.ValeursIdentiques(comp : TComposantBibliotheque) : boolean;
var
  Mat : TMateriau;
begin
  Mat := comp as TMateriau;
  Result := Conductivite = Mat.Conductivite;
  Result := Result and (MasseVol = Mat.MasseVol);
  Result := Result and (ChaleurMassJ = Mat.ChaleurMassJ);
  Result := Result and (ChaleurMassWh = Mat.ChaleurMassWh);
end;

function TListeMateriau.IndexOf(Chaine : string) : integer;
begin
  for Result := 0 to Count - 1 do
    if TMateriau(items[Result]).nom = Chaine then
      Exit;
  Result := -1;
end;

procedure TListeMateriau.ClearAndFree;
var
  i : integer;
begin
  for i := 0 to Count - 1 do
    TMateriau(items[i]).Free;
  Clear;
end;

function TListeMateriau.GetMateriau(const i : integer) : TMateriau;
begin
  if i < Count then
    Result := Self.items[i]
  else
    Result := nil;
end;

procedure TListeMateriau.SetMateriau(const i : integer; const Valeur : TMateriau);
begin
  Self.items[i] := Valeur;
end;

constructor TMateriau.Create;
begin
  inherited;
  innerTagName := 'materiau';
end;

constructor TMateriau.Create(Nom_, categorie_, complement_, source_,
  conductivite_, massevolumique_, ChaleurMassiqueJ_, ChaleurMassiqueWh_ : string);
var
  erreur : integer;
begin
  inherited Create(Nom_, categorie_, complement_, source_);
  innerTagName := 'materiau';
  Val(ConvertPoint(conductivite_), Conductivite, erreur);
  Val(ConvertPoint(massevolumique_), MasseVol, erreur);
  Val(ConvertPoint(ChaleurMassiqueJ_), ChaleurMassJ, erreur);
  Val(ConvertPoint(ChaleurMassiqueWh_), ChaleurMassWh, erreur);
end;

procedure TMateriau.Assign(Mat : TMateriau);
begin
  inherited Assign(Mat);
  Conductivite := Mat.Conductivite;
  MasseVol := Mat.MasseVol;
  ChaleurMassJ := Mat.ChaleurMassJ;
  ChaleurMassWh := Mat.ChaleurMassWh;
end;

function TMateriau.Sauve(var Contenu : TCreationSauvegarde) : boolean;
begin
  Result := inherited SauveDebut(Contenu);
  Contenu.Add(Conductivite);
  Contenu.Add(MasseVol);
  Contenu.Add(ChaleurMassJ);
  Contenu.Add(ChaleurMassWh);
  Result := Result and inherited SauveFin(Contenu);
end;

function TMateriau.Charge(var Contenu : TSauvegarde; NumVersion : integer) : boolean;
begin
  Result := inherited ChargeDebut(Contenu);
  Conductivite := Contenu.GetLigneReel;
  MasseVol := Contenu.GetLigneReel;
  ChaleurMassJ := Contenu.GetLigneReel;
  ChaleurMassWh := Contenu.GetLigneReel;
  if (ChaleurMassWh = 0) and (ChaleurMassJ <> 0) then
    ChaleurMassWh := ChaleurMassJ / 3600;

  Result := Result and inherited ChargeFin(Contenu);
end;

procedure TMateriau.innerLoadXml(node : IXMLNode);
begin
  inherited innerLoadXml(node);
  Conductivite := TXmlUtil.getTagDouble(node, 'conductivite');
  MasseVol := TXmlUtil.getTagDouble(node, 'masse-volumique');
  ChaleurMassJ := TXmlUtil.getTagDouble(node, 'chaleur-massique-j');
  ChaleurMassWh := TXmlUtil.getTagDouble(node, 'chaleur-massique-wh');
end;


procedure TMateriau.innerSaveXml(node : IXMLNode);
begin
  inherited innerSaveXml(node);
  TXmlUtil.setTag(node, 'conductivite', Conductivite);
  TXmlUtil.setTag(node, 'masse-volumique', MasseVol);
  TXmlUtil.setTag(node, 'chaleur-massique-j', ChaleurMassJ);
  TXmlUtil.setTag(node, 'chaleur-massique-wh', ChaleurMassWh);
end;

end.
