unit CadreMateriaux;

interface

uses
  Windows,
  Messages,
  SysUtils,
  Variants,
  Classes,
  Graphics,
  Controls,
  Forms,
  Dialogs,
  StdCtrls,
  Buttons,
  ExtCtrls,
  ComCtrls,
  Materiaux,
  Menus;

type
  TFrameMateriaux = class(TFrame)
    ZGroupBox1:    TGroupBox;
    Tree:          TTreeView;
    ZGroupBox2:    TGroupBox;
    zLabel1:       TLabel;
    zLabel2:       TLabel;
    zLabel3:       TLabel;
    zLabel4:       TLabel;
    zLabel5:       TLabel;
    zLabel6:       TLabel;
    zLabel7:       TLabel;
    zLabel8:       TLabel;
    zLabel9:       TLabel;
    zLabel10:      TLabel;
    zLabel11:      TLabel;
    Label36:       TLabel;
    Categorie:     TComboBox;
    Nom:           TEdit;
    Complement:    TEdit;
    Source:        TEdit;
    zPanel3:       TPanel;
    BitBtnNewMat:  TBitBtn;
    BitBtnsaveMat: TBitBtn;
    BitBtnCreerClasse: TBitBtn;
    Conductivite:  Tedit;
    MasseVol:      Tedit;
    ChaleurMassJ:  Tedit;
    ChaleurMassWh: Tedit;
    PopupComponent: TPopupMenu;
    Open1:         TMenuItem;
    Delete1:       TMenuItem;
    Export1:       TMenuItem;
    Rename1: TMenuItem;
    Splitter1: TSplitter;
    procedure TreeChange(Sender: TObject; Node: TTreeNode);
    procedure TreeClick(Sender: TObject);
    procedure TreeDragDrop(Sender, Source: TObject; X, Y: integer);
    procedure TreeDragOver(Sender, Source: TObject; X, Y: integer;
      State: TDragState; var Accept: boolean);
    procedure TreeMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: integer);
    procedure BitBtnCreerClasseClick(Sender: TObject);
    procedure BitBtnNewMatClick(Sender: TObject);
    procedure BitBtnsaveMatClick(Sender: TObject);
    procedure BitBtnsaveMatMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: integer);
    procedure CategorieChange(Sender: TObject);
    procedure ChaleurMassJKeyUp(Sender: TObject; var Key: word; Shift: TShiftState);
    procedure ChaleurMassWhKeyUp(Sender: TObject; var Key: word;
      Shift: TShiftState);
    constructor Create(AOwner: TComponent); override;
    procedure TreeKeyUp(Sender: TObject; var Key: word; Shift: TShiftState);
    procedure Open1Click(Sender: TObject);
    procedure Delete1Click(Sender: TObject);
    destructor Destroy; override;
    procedure PopupComponentPopup(Sender: TObject);
    procedure Rename1Click(Sender: TObject);
  private
    { Déclarations privées }
    Modif: boolean;

    function ControleSauve(Forcee: boolean): boolean;
  protected
    Dragage:       TTreeNode;
    SauveReadOnly: boolean;
    function Controle: boolean;
  public
    { Déclarations publiques }
    ModificationAutorise: boolean;
    FormParent:        TForm;
    CheckHint:         boolean;
    MessagesInterface: TStringList;
    procedure EffaceDragage;
    procedure Affiche(Mat: TMateriau);
    function AccepteDrag: boolean;
  end;

implementation

uses
  System.UITypes,
  BiblioMateriaux,
  IzUtilitaires,
  DialogsInternational,
  IzApp;

{$R *.dfm}

destructor TFrameMateriaux.Destroy;
begin
  MessagesInterFace.Free;
  inherited Destroy;
end;

function TFrameMateriaux.AccepteDrag: boolean;
begin
  Result := (BibMateriaux.ImageIndex = dragage.ImageIndex) or
    (dragage.ImageIndex = BibMateriaux.ImageIndexDossier) or
    (Dragage.ImageIndex = BibMateriaux.ImageIndexDossierS);
end;

constructor TFrameMateriaux.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  SauveReadOnly := False;
  ModificationAutorise := True;
  CheckHint  := True;
  FormParent := nil;
  MessagesInterFace := TStringList.Create;
end;

procedure TFrameMateriaux.Delete1Click(Sender: TObject);
var
  I: integer;
  TreeSource: TCustomTreeView;
begin
  if Dragage = nil then
    Exit;
  TreeSource := Dragage.TreeView;
  for I := TreeSource.Selectioncount - 1 downto 0 do
  begin
    Dragage := TreeSource.Selections[i];
    EffaceDragage;
  end;
end;

procedure TFrameMateriaux.EffaceDragage;
begin
  //Cas des catégories
  if (Dragage.ImageIndex = BibMateriaux.ImageIndexDossier) or
    (Dragage.ImageIndex = BibMateriaux.ImageIndexDossierS) then
  begin
    if dragage.HasChildren then
      MessageDlgM(MessagesInterface[0], mtWarning, [mbOK], 0)
    else
    begin
      Categorie.items.Delete(Categorie.Items.IndexOf(dragage.Text));
      Tree.items.Delete(dragage);
    end;
  end
  else if (Dragage.ImageIndex = BibMateriaux.ImageIndex) or (TApp.isMaster and (Dragage.ImageIndex = BibMateriaux.ImageIndexRO ))  then
  begin
    if (MessageDlgM(MessagesInterface[1] + dragage.Text + MessagesInterface[2],
      mtConfirmation, mbOkCancel, 0) = mrOk) and
      BibMateriaux.Efface(dragage.Text) then
      begin
        Tree.Items.Delete(dragage);
        BibMateriaux.SauveListe();
      end;
  end;
end;

procedure TFrameMateriaux.Open1Click(Sender: TObject);
var
  Noeud: TTreeNode;
  Mat: TMateriau;
  Ok:  boolean;
begin
  if Modif then
    Ok := ControleSauve(False)
  else
    Ok := True;
  if not Ok then
    Exit;
  Noeud := Tree.Selected;
  if Noeud <> nil then
  begin
    Mat := BibMateriaux.Get(Noeud.Text);
    if Mat <> nil then
      Affiche(Mat);
  end;
end;

procedure TFrameMateriaux.PopupComponentPopup(Sender: TObject);
begin
  Rename1.Enabled :=  Tree.Selected.ImageIndex <= 1;
  Open1.Enabled := Not Rename1.Enabled;
end;

procedure TFrameMateriaux.Rename1Click(Sender: TObject);
var
  NouveauNom : String;
begin
  if Dragage = nil then
    Exit;
  NouveauNom := Dragage.text;
  if InputQueryM(MessagesInterface[6], MessagesInterface[6], NouveauNom) then
    BibMateriaux.RenommeCategorie(Dragage,NouveauNom);
end;

procedure TFrameMateriaux.Affiche(Mat: TMateriau);
var
  Chaine: string;
  indice: integer;
begin
  BibMateriaux.ConvertitCategorie(Mat);
  Nom.Text := Mat.Nom;
  Complement.Text := Mat.Complement;
  Str(Mat.Conductivite: 0: 3, Chaine);
  Conductivite.Text := chaine;
  Str(Mat.MasseVol: 0: 0, Chaine);
  MasseVol.Text := chaine;
  Str(Mat.ChaleurMassJ: 0: 0, Chaine);
  ChaleurMassJ.Text := chaine;
  Str(Mat.ChaleurMassWh: 0: 3, Chaine);
  ChaleurMassWh.Text := chaine;

  indice := Categorie.Items.IndexOf(Mat.Categorie);
  if Indice = -1 then
  begin
    BibMateriaux.AjouteCategorie(Mat.Categorie, Categorie.items, Tree);
    indice := Categorie.Items.IndexOf(Mat.Categorie);
  end;
  Categorie.ItemIndex := Indice;
  //BarreAdresse.Chemin := Mat.categorie;

  Source.Text := Mat.Source;
  Modif := False;
end;

procedure TFrameMateriaux.TreeChange(Sender: TObject; Node: TTreeNode);
var
  Chaine: string;
begin
  Str(BibMateriaux.Count, chaine);
  if CheckHint then
    Tree.Hint := Chaine + MessagesInterface[8]
  else
    Tree.Hint := '';
end;

procedure TFrameMateriaux.TreeClick(Sender: TObject);
var
  Noeud: TTreeNode;
  Mat: TMateriau;
  Ok:  boolean;
begin
  if Modif then
    Ok := ControleSauve(False)
  else
    Ok := True;
  if not Ok then
    Exit;
  Mat := nil;
  Noeud := Tree.Selected;
  if Noeud <> nil then
    Mat := BibMateriaux.Get(Noeud.Text);
  if Mat <> nil then
    Affiche(Mat);
  Modif := False;
end;

procedure TFrameMateriaux.TreeDragDrop(Sender, Source: TObject; X, Y: integer);
var
  Courant: TTreeNode;
  Mat: TMateriau;
begin
  if (Tree.dropTarget.ImageIndex = BibMateriaux.ImageIndexDossier) or
    (Tree.dropTarget.ImageIndex = BibMateriaux.ImageIndexDossierS) then
  begin
    Mat := BibMateriaux.Get(dragage.Text);
    Mat.Categorie := BibMateriaux.GetNomAscendants(Tree.DropTarget);
    Tree.Items.Delete(dragage);
    Courant := BibMateriaux.PoseDansArbre(Mat, Tree, True, False);
    Tree.alphaSort;
    Tree.FullCollapse;
    Tree.Selected := Courant;
  end;
end;

procedure TFrameMateriaux.TreeDragOver(Sender, Source: TObject;
  X, Y: integer; State: TDragState; var Accept: boolean);
begin
  if Dragage <> nil then
    Accept := (dragage.ImageIndex = BibMateriaux.ImageIndex)
  else
    Accept := False;
end;

procedure TFrameMateriaux.TreeKeyUp(Sender: TObject; var Key: word; Shift: TShiftState);
begin
  if (Key = 46) and (Tree.Selected <> nil) then
    EffaceDragage
  else
    Tree.OnClick(Self);
end;

procedure TFrameMateriaux.TreeMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: integer);
begin
  Dragage := TTreeView(Sender).GetNodeAt(X, Y);
end;

procedure TFrameMateriaux.BitBtnCreerClasseClick(Sender: TObject);
var
  Valeur: string;
begin
  if InputQueryM(MessagesInterface[6], MessagesInterface[6], valeur) and
    (Valeur <> '') then
    BibMateriaux.AjouteCategorie(Valeur, Categorie.items, Tree);
end;

procedure TFrameMateriaux.BitBtnNewMatClick(Sender: TObject);
var
  Ok: boolean;
begin
  if Modif then
    Ok := ControleSauve(False)
  else
    Ok := True;
  if Ok then
  begin
    nom.Text := '';
    complement.Text := '';
    Source.Text := '';
    Conductivite.Text := '';
    MasseVol.Text := '';
    ChaleurMassJ.Text := '';
    ChaleurMassWh.Text := '';
    Categorie.ItemIndex := -1;
    Modif := False;
  end;
end;

procedure TFrameMateriaux.BitBtnsaveMatClick(Sender: TObject);
begin
  ControleSauve(True);
end;

procedure TFrameMateriaux.BitBtnsaveMatMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: integer);
begin
  SauveReadOnly := (Shift * [ssShift, ssCtrl] = [ssShift, ssCtrl]);
end;

procedure TFrameMateriaux.CategorieChange(Sender: TObject);
begin
  Modif := True;
end;

procedure TFrameMateriaux.ChaleurMassJKeyUp(Sender: TObject; var Key: word;
  Shift: TShiftState);
var
  Reel: double;
  Erreur: integer;
  Chaine: string;
begin
  Val(ConvertPoint(ChaleurMassJ.Text), Reel, erreur);
  if erreur = 0 then
  begin
    Str(Reel / 3600: 0: 3, chaine);
    chaleurMassWh.Text := chaine;
  end;
end;

procedure TFrameMateriaux.ChaleurMassWhKeyUp(Sender: TObject;
  var Key: word; Shift: TShiftState);
var
  Reel: double;
  Erreur: integer;
  Chaine: string;
begin
  Val(ConvertPoint(ChaleurMassWh.Text), Reel, erreur);
  if erreur = 0 then
  begin
    Str(Reel * 3600: 0: 0, chaine);
    chaleurMassJ.Text := chaine;
  end;
end;

function TFrameMateriaux.ControleSauve(Forcee: boolean): boolean;
var
  reponse: word;
  comp:  TMateriau;
  Silencieux: boolean;
  Noeud: TTreeNode;
begin
  if not ModificationAutorise then
  begin
    MessageDlgM(MessagesInterface[7], mtWarning, [mbOK], 0);
    Result := False;
    Exit;
  end;
  Result := False;
  FormParent.FocusControl(Tree);
  if not Forcee then
    Reponse := MessageDlgM(MessagesInterface[5] + Nom.Text + '''',
      mtWarning, [mbCancel, mbNo, mbYes], 0)
  else
    Reponse := mrYes;
  if Reponse = mrCancel then
    Exit;
  Result := True;
  if (Reponse = mrYes) then
    if Controle then
    begin
      comp := TMateriau.Create(Nom.Text, Categorie.text, Complement.Text,
        Source.Text, Conductivite.Text, MasseVol.Text, ChaleurMassJ.Text,
        chaleurMassWh.Text);
      comp.ReadOnly := CtrlDown and ShiftDown;
      Silencieux := False;
      if BibMateriaux.Ajoute(comp, Silencieux) then
      begin
        Noeud := BibMateriaux.PoseDansArbre(comp, Tree, True, True);
        Tree.Selected := Noeud;
      end
      else
        comp.Free;
    end
    else
      Result := False;
  Modif := False;
  BibMateriaux.SauveListe;
end;

function TFrameMateriaux.Controle: boolean;
var
  Erreur: integer;
begin
  if EstNumerique(Nom.Text) then
    Nom.Text := '_' + Nom.Text;
  Erreur := 0;
  if Categorie.Text = '' then
  begin
    FormParent.FocusControl(Categorie);
    Erreur := 1;
  end
  else if ControlNum(nom.Text, 4, 256, 'C') <> nom.Text then
  begin
    FormParent.FocusControl(nom);
    Erreur := 2;
  end
  else if ControlNum(Complement.Text, 0, 256, 'C') <> Complement.Text then
  begin
    FormParent.FocusControl(Complement);
    Erreur := 3;
  end
  else if ControlNum(ConvertPoint(Conductivite.Text), 0.001, 10000, 'R') <>
    ConvertPoint(Conductivite.Text) then
  begin
    FormParent.FocusControl(Conductivite);
    Erreur := 4;
  end;
  if Erreur <> 0 then
  begin
    MessageDlgM(MessagesInterface[8 + Erreur], mtWarning, [mbOK], 0);
    Result := False;
  end
  else
    Result := True;
end;

end.