unit BiblioMateriaux;

interface

uses
  // Standard
  Classes, ComCtrls,
  // Pleiades
  Materiaux, GestionBib, ComposantBibliotheque;

const
  FichierMateriauNS = 'MateriauxNS.txt';

type
  TBiblioMateriaux = class(TBibliotheque)
  private
const
  FichierMateriauS = 'MateriauxS.txt';
    procedure loadFromFileV0(filePath: string);
  protected
    function CreationPourImport: TComposantBibliotheque; override;
    procedure ChargeListeAncien(standardLib : boolean); override;
  public
    constructor Create(Standard, NonStandard: string);
    function Get(Nom: string): TMateriau; overload;
    function Get(Identificateur: integer): TMateriau; overload;
  end;

var
  BibMateriaux: TBiblioMateriaux;

implementation

uses
  // Delphi
  Controls, Dialogs, SysUtils,
  // Pleiades
  CreerSauvegarde, GestionSauvegarde;

function TBiblioMateriaux.CreationPourImport: TComposantBibliotheque;
begin
  Result := TMateriau.Create('', '', '', '', '', '', '', '');
end;

constructor TBiblioMateriaux.Create(Standard, NonStandard: string);
begin
  inherited Create(Standard, NonStandard);
  ImageIndex := 2;
  ImageIndexRO := 4;
  ImageBrute := 160;
  TypeComposant := 'Materiaux';
  ChargeListe;
end;

function TBiblioMateriaux.Get(Nom: string): TMateriau;
begin
  Result := inherited Get(Nom) as TMateriau;
end;

function TBiblioMateriaux.Get(Identificateur: integer): TMateriau;
begin
  Result := TMateriau(inherited Get(Identificateur));
end;

procedure TBiblioMateriaux.ChargeListeAncien(standardLib : boolean);
begin
  if standardLib then
    loadFromFileV0(CheminBiblioStandard + FichierMateriauS)
  else
    loadFromFileV0(CheminBiblioNonStandard + FichierMateriauNS);
end;


procedure TBiblioMateriaux.loadFromFileV0(filePath : string);
var
  Mat: TMateriau;
  Fichier: TSauvegarde;
begin
  Fichier := TSauvegarde.Create;
  try
    if fileExists(filePath) then
    begin
      hasToSave := true;
      Fichier.LoadFromFile(filePath);
      while not Fichier.EOF do
      begin
        Mat := TMateriau.Create;
        Fichier.Suivant;
        Mat.Identificateur := Fichier.GetLigneEntier;

        Fichier.Suivant;
        Mat.Categorie := Fichier.GetLigne;

        Fichier.Suivant;
        Mat.nom := Fichier.GetLigne;
        Fichier.Suivant;
        Mat.complement := Fichier.GetLigne;
        Fichier.Suivant;
        Mat.Source := Fichier.GetLigne;
        Fichier.Suivant;
        Mat.Conductivite := Fichier.GetLigneReel;
        Fichier.Suivant;
        Mat.MasseVol := Fichier.GetLigneReel;
        Fichier.Suivant;
        Mat.ChaleurMassWh := Fichier.GetLigneReel;
        Fichier.Suivant;
        Mat.ChaleurMassJ := Fichier.GetLigneReel;
        Fichier.Suivant;
        Mat.ReadOnly := Fichier.GetLigneBool;
        Table.AddObject(Mat.nom, Mat);
        Fichier.Suivant;

        if not Mat.ReadOnly and (Mat.Identificateur < EcartStandard) then
          Mat.Identificateur := Mat.Identificateur + EcartStandard;

        if (Mat.Identificateur > IdMax) and Mat.ReadOnly then
          IdMax := Mat.Identificateur
        else if (Mat.Identificateur > IdMax) then
          IdMax := Mat.Identificateur - EcartStandard;
      end;
    end;
  finally
    Fichier.Free;
  end;
end;

end.
