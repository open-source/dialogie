unit NCadreCompositionsPleiades;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, NCadreCompositions, Menus, ExtCtrls, Grids, ComCtrls, StdCtrls,
  Buttons, BarreAdresse, Compositions, ImgList;

type
  TFrameCompositionsPleiades = class(TFrameCompositions)
    BitBtnExp: TBitBtn;
    procedure BitBtnExpClick(Sender: TObject);
    procedure FrameResize(Sender: TObject);
    procedure Export1Click(Sender: TObject);
  private
    { Déclarations privées }
    procedure Exporte(comp: TComposition);
  public
    { Déclarations publiques }
  end;

var
  FrameCompositionsPleiades: TFrameCompositionsPleiades;

implementation

uses
  Projet, BiblioCompositions, DHMulti, Materiaux, dialogsinternational,
  BiblioMateriaux, Elements, BiblioElements, MatChangementPhase, BiblioMCP,
  Translateur, MainPleiades, PontsIntegres, BiblioPontsIntegres;
{$R *.dfm}

procedure TFrameCompositionsPleiades.BitBtnExpClick(Sender: TObject);
begin
  inherited;
  if Controle then
    Exporte(CreerComposantBiblio as TComposition);
end;

procedure TFrameCompositionsPleiades.Export1Click(Sender: TObject);
var
  i: integer;
  comp: TComposition;
begin
  inherited;
  for i := Tree.Selectioncount - 1 downto 0 do
  begin
    comp := TComposition.Create;
    comp.Assign(Tree.Selections[i].Data);
    Exporte(comp);
  end;
end;

procedure TFrameCompositionsPleiades.Exporte(comp: TComposition);
var
  prec: word;
  Mat: TMateriau;
  Elem: TElement;
  MCP: TMCP;
  Noeud: TTreeNode;
  i: integer;
  Pont: TPontIntegre;
begin
  prec := mrYes;
  ProjetCourant.AjouteCompo(comp);
  if comp.Identificateur = 0 then
    ProjetCourant.IdentifieCompo(comp);
  Noeud := BibCompositions.PoseDansArbre(comp,
    FormDHMulti.FComposants.TreeCompo, False, True);

  FormDHMulti.FParois.AfficheParoi;
  FormDHMulti.FComposants.TreeCompo.OnChange(nil, nil);
  FormDHMulti.FComposants.ComboStandard.OnChange(nil);

  if Noeud <> nil then
  begin
    prec := mrYes;
    for i := 1 to comp.NbMat do
      if comp.TypeMat[i] = LettreMat then
      begin
        Mat := TMateriau.Create;
        Mat.Assign(BibMateriaux.Get(comp.IdentiMat[i]));
        ProjetCourant.AjouteMat(Mat, prec);
        BibMateriaux.PoseDansArbre(Mat, FormDHMulti.FComposants.TreeMat, False,
          True);
      end
      else if comp.TypeMat[i] = LettreElem then
      begin
        Elem := TElement.Create;
        Elem.Assign(BibElements.Get(comp.IdentiMat[i]));
        ProjetCourant.AjouteElem(Elem, prec);
        BibElements.PoseDansArbre(Elem, FormDHMulti.FComposants.TreeElem,
          False, True);
      end
      else if comp.TypeMat[i] = LettreMCP then
      begin
        MCP := TMCP.Create;
        MCP.Assign(BibMCP.Get(comp.IdentiMat[i]));
        ProjetCourant.AjouteMCP(MCP, prec);
        BibMCP.PoseDansArbre(MCP, FormDHMulti.FComposants.TreeMCP, False, True);
      end;
    for i := 1 to comp.NbPont do
    begin
      Pont := TPontIntegre.Create;
      Pont.Assign(BibPontsIntegres.Get(comp.IdentiPont[i]));
      ProjetCourant.AjoutePontIntegre(Pont, prec);
      BibPontsIntegres.PoseDansArbre(Pont,
        FormDHMulti.FComposants.TreePontsIntegres, False, True);
    end
  end;
end;

procedure TFrameCompositionsPleiades.FrameResize(Sender: TObject);
begin
  inherited;
  EditFiltre.Images := FormMainPleiades.ImageListNMenu;
  EditFiltre.RightButton.ImageIndex := 303;
  EditFiltreSimple.Images := FormMainPleiades.ImageListNMenu;
  EditFiltreSimple.RightButton.ImageIndex := 303;
end;

end.
