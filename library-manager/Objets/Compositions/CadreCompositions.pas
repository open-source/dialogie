unit CadreCompositions;

interface

uses
  Windows,
  Messages,
  SysUtils,
  Variants,
  Classes,
  Graphics,
  Controls,
  Forms,
  Compositions,
  Dialogs,
  Grids,
  StdCtrls,
  Buttons,
  ExtCtrls,
  ComCtrls,
  Menus,
  Elements,
  Materiaux,
  ImgList, System.ImageList;

type
  TFrameCompositions = class(TFrame)
    zPanel1:       TPanel;
    Splitter1:     TSplitter;
    ZGroupBox14:   TGroupBox;
    Tree:          TTreeView;
    ZGroupBox15:   TGroupBox;
    TreeElemCompos: TTreeView;
    TreeMatCompos: TTreeView;
    ZGroupBox13:   TGroupBox;
    zLabel28:      TLabel;
    zLabel29:      TLabel;
    zLabel30:      TLabel;
    zLabel69:      TLabel;
    zLabel73:      TLabel;
    zLabel74:      TLabel;
    ZImageFleche:  TImage;
    Label14:       TLabel;
    Label19:       TLabel;
    Label20:       TLabel;
    Label23:       TLabel;
    Label24:       TLabel;
    Label25:       TLabel;
    Label26:       TLabel;
    Label27:       TLabel;
    Categorie:     TComboBox;
    Nom:           TEdit;
    complement:    TEdit;
    Source:        TEdit;
    zPanel5:       TPanel;
    BitBtnNewCompo: TBitBtn;
    BitBtnExpCompo: TBitBtn;
    BitBtnSaveCompo: TBitBtn;
    TreeTemp:      TTreeView;
    Grille:        TStringGrid;
    BitBtnCreerClasse: TBitBtn;
    Edit1:         TEdit;
    Edit2:         TEdit;
    Edit3:         TEdit;
    PopupComponent: TPopupMenu;
    Open1:         TMenuItem;
    Delete1:       TMenuItem;
    Export1:       TMenuItem;
    PopupGrille:   TPopupMenu;
    DeleteGrille:  TMenuItem;
    CopyGrille:    TMenuItem;
    PasteGrille:   TMenuItem;
    CutGrille:     TMenuItem;
    Rename1:       TMenuItem;
    CategoryPanelGroupMatElem: TCategoryPanelGroup;
    CategoryPanelMat: TCategoryPanel;
    CategoryPanelElem: TCategoryPanel;
    ImageListCategory: TImageList;
    Splitter2: TSplitter;
    procedure TreeMatComposDragOver(Sender, Source: TObject; X, Y: integer;
      State: TDragState; var Accept: boolean);
    procedure TreeElemComposDragOver(Sender, Source: TObject;
      X, Y: integer; State: TDragState; var Accept: boolean);
    procedure TreeChange(Sender: TObject; Node: TTreeNode);
    procedure TreeClick(Sender: TObject);
    procedure TreeDragDrop(Sender, Source: TObject; X, Y: integer);
    procedure TreeDragOver(Sender, Source: TObject; X, Y: integer;
      State: TDragState; var Accept: boolean);
    procedure TreeKeyUp(Sender: TObject; var Key: word; Shift: TShiftState);
    procedure CategorieChange(Sender: TObject);
    procedure GrilleDragDrop(Sender, Source: TObject; X, Y: integer);
    procedure GrilleDragOver(Sender, Source: TObject; X, Y: integer;
      State: TDragState; var Accept: boolean);
    procedure GrilleDrawCell(Sender: TObject; ACol, ARow: integer;
      Rect: TRect; State: TGridDrawState);
    procedure GrilleMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: integer);
    procedure GrilleSelectCell(Sender: TObject; ACol, ARow: integer;
      var CanSelect: boolean);
    procedure GrilleSetEditText(Sender: TObject; ACol, ARow: integer;
      const Value: string);
    procedure BitBtnNewCompoClick(Sender: TObject);
    procedure BitBtnSaveCompoClick(Sender: TObject);
    procedure BitBtnSaveCompoMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: integer);
    procedure FrameResize(Sender: TObject);
    procedure BitBtnCreerClasseClick(Sender: TObject);
    procedure TreeMatComposMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: integer);
    procedure TreeElemComposMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: integer);
    procedure TreeMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: integer);
    procedure Open1Click(Sender: TObject);
    procedure Delete1Click(Sender: TObject);
    procedure GrilleMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: integer);
    procedure DeleteGrilleClick(Sender: TObject);
    procedure GrilleKeyUp(Sender: TObject; var Key: word; Shift: TShiftState);
    procedure CopyGrilleClick(Sender: TObject);
    procedure CutGrilleClick(Sender: TObject);
    procedure PasteGrilleClick(Sender: TObject);
    procedure Rename1Click(Sender: TObject);
    procedure PopupGrillePopup(Sender: TObject);
    procedure TreeMatComposDblClick(Sender: TObject);
    procedure TreeElemComposDblClick(Sender: TObject);
    procedure CategoryPanelElemExpand(Sender: TObject);
    procedure TreeMCPComposDragOver(Sender, Source: TObject; X, Y: integer;
      State: TDragState; var Accept: boolean);
    procedure TreeMCPComposMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: integer);
    procedure Splitter1Moved(Sender: TObject);
  private
    Modif:        boolean;
    PressePapier: TStringList;
    function ControleSauve(Forcee: boolean): boolean;
    procedure CalculSommeGrille;
    procedure AfficheElem(Elem: TElement; Ligne: integer);
    procedure AfficheMat(Mat: TMateriau; Ligne: integer; Epaisseur: double);
    procedure ResizeCategoryPanel;
  protected
    Dragage:       TTreeNode;
    SauveReadOnly: boolean;
    AutoCollapse:  boolean;
    function Controle: boolean;
  public
    ModificationAutorise: boolean;
    FormParent:        TForm;
    CheckHint:         boolean;
    MessagesInterface: TStringList;

		constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;

    procedure EffaceDragage;
    procedure Affiche(Compo: TComposition);
    function AccepteDrag: boolean;
  end;

implementation

uses
  System.UITypes,
  BiblioCompositions,
  IzUtilitaires,
  BiblioMateriaux,
  BiblioElements,
  DialogsInternational,
  ComposantBibliotheque,
  IzConstantes,
  IzApp;

{$R *.dfm}

destructor TFrameCompositions.Destroy;
begin
  MessagesInterFace.Free;
  PressePapier.Free;
  inherited Destroy;
end;

function TFrameCompositions.AccepteDrag: boolean;
begin
  Result := BibCompositions.EstDuType(dragage.ImageIndex) or
    (dragage.ImageIndex = BibCompositions.ImageIndexDossier) or
    (Dragage.ImageIndex = BibCompositions.ImageIndexDossierS) or
    (Dragage.ImageIndex > 10000);
end;

constructor TFrameCompositions.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  SauveReadOnly := False;
  ModificationAutorise := True;
  CheckHint  := True;
  FormParent := nil;
  MessagesInterFace := TStringList.Create;
  PressePapier := TStringList.Create;
  AutoCollapse := True;

  with grille do
  begin
    ColCount := 6;
    RowCount := 9;
    DefaultColWidth := 60;
    DefaultRowHeight := 17;
  end;

  Grille.Cells[0, 0] := 'Nom';
  Grille.Cells[1, 0] := 'Type';
  Grille.Cells[2, 0] := 'cm';
  Grille.Cells[3, 0] := 'kg/m�';
  Grille.Cells[5, 0] := 'Rm';
end;


procedure TFrameCompositions.CutGrilleClick(Sender: TObject);
begin
  CopyGrille.Click;
  DeleteGrille.Click;
  Modif := True;
  CalculSommeGrille;
end;

procedure TFrameCompositions.Delete1Click(Sender: TObject);
var
  I: integer;
  TreeSource: TCustomTreeView;
begin
  if Dragage = nil then
    Exit;
  TreeSource := Dragage.TreeView;
  for I := TreeSource.Selectioncount - 1 downto 0 do
  begin
    Dragage := TreeSource.Selections[i];
    EffaceDragage;
  end;
end;

procedure TFrameCompositions.DeleteGrilleClick(Sender: TObject);
var
  i: integer;
begin
  if (Grille.Selection.Top > 0) and (Grille.Selection.Top <= 7) then
    for i := 0 to Grille.ColCount - 1 do
      Grille.Cells[i, Grille.Selection.top] := '';
  Modif := True;
  CalculSommeGrille;
end;

procedure TFrameCompositions.BitBtnCreerClasseClick(Sender: TObject);
var
  Valeur: string;
begin
  if InputQueryM(MessagesInterface[6], MessagesInterface[6], valeur) and
    (Valeur <> '') then
    BibCompositions.AjouteCategorie(Valeur, Categorie.items, Tree);
end;

procedure TFrameCompositions.BitBtnNewCompoClick(Sender: TObject);
var
  Ok: boolean;
  i:  integer;
begin
  if Modif then
    Ok := ControleSauve(False)
  else
    Ok := True;
  if Ok then
  begin
    nom.Text := '';
    complement.Text := '';
    Source.Text := '';
    Categorie.ItemIndex := -1;
    for i := 1 to 7 do
      Grille.Rows[i].Clear;
    for i := 1 to 5 do
      Grille.Cells[i, 8] := '';
    Modif := False;
  end;
end;

procedure TFrameCompositions.BitBtnSaveCompoClick(Sender: TObject);
begin
  ControleSauve(True);
end;

procedure TFrameCompositions.BitBtnSaveCompoMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: integer);
begin
  SauveReadOnly := (Shift * [ssShift, ssCtrl] = [ssShift, ssCtrl]);
end;

procedure TFrameCompositions.CategorieChange(Sender: TObject);
begin
  Modif := True;
end;

procedure TFrameCompositions.CategoryPanelElemExpand(Sender: TObject);
var
  i: integer;
begin
  if AutoCollapse then
  begin
    for i := 0 to CategoryPanelGroupMatElem.Panels.Count - 1 do
      if CategoryPanelGroupMatElem.Panels[i] <> Sender then
        TCategoryPanel(CategoryPanelGroupMatElem.Panels[i]).collapsed := True;
    CategoryPanelGroupMatElem.VertScrollBar.Position := 0;
    CategoryPanelGroupMatElem.Invalidate;
  end;
end;

procedure TFrameCompositions.GrilleDragDrop(Sender, Source: TObject; X, Y: integer);
var
  Nom:  string;
  Col, lig: integer;
  Tampon: TStrings;
  Resultat: integer;
  Mat:  TMateriau;
  Elem: TElement;
  position: integer;
begin
  Grille.MouseToCell(X, Y, Col, Lig);
  if Lig < 8 then
  begin
    if (dragage.imageIndex = BibMateriaux.ImageIndex) or
      (dragage.imageIndex = BibMateriaux.ImageIndexRO) then
    begin
      Mat := BibMateriaux.Get(dragage.Text);
      if FaireLeTrou(Grille, Lig) <> 0 then
        AfficheMat(Mat, Lig, -1)
      else
        MessageBeep(MB_ICONEXCLAMATION);
    end;
    if (dragage.imageIndex = BibElements.ImageIndex) or
      (dragage.imageIndex = BibElements.ImageIndexRO) then
    begin
      Nom  := dragage.Text;
      Elem := BibElements.Get(Dragage.Text);
      if FaireLeTrou(Grille, Lig) <> 0 then
        AfficheElem(Elem, Lig)
      else
        MessageBeep(MB_ICONEXCLAMATION);
    end;
    if (dragage.imageIndex >= 10001) then
    begin
      Position := dragage.imageIndex - 10000;
      Grille.MouseToCell(X, Y, Col, Lig);
      if lig <> Position then
        Resultat := FaireLeTrou(Grille, Lig)
      else
        Resultat := -1;
      if (Resultat = 1) or ((Resultat = 2) and (Position < Lig)) then
      begin
        Tampon := Grille.rows[Position];
        Grille.rows[Lig] := Tampon;
        if not CtrlDown then
          Grille.Rows[Position].Clear;
      end
      else if (Resultat = 2) and (Position > Lig) then
      begin
        Tampon := Grille.rows[Position + 1];
        Grille.rows[Lig] := Tampon;
        if not CtrlDown then
          Grille.Rows[Position + 1].Clear;
      end;
    end;
  end;
  Modif := True;
end;

procedure TFrameCompositions.GrilleDragOver(Sender, Source: TObject;
  X, Y: integer; State: TDragState; var Accept: boolean);
var
  ARow, ACol: integer;
begin
  Grille.MouseTOCell(X, Y, ACol, ARow);
  if Dragage <> nil then
    Accept := ((dragage.ImageIndex = BibMateriaux.ImageIndex) or
      (dragage.ImageIndex = BibMateriaux.ImageIndexRO) or
      (dragage.imageIndex = BibElements.ImageIndex) or
      (dragage.imageIndex = BibElements.ImageIndexRO) or
      (dragage.imageIndex > 10000)) and (ARow > 0)
  else
    Accept := False;
end;

procedure TFrameCompositions.GrilleDrawCell(Sender: TObject;
  ACol, ARow: integer; Rect: TRect; State: TGridDrawState);
var
  Recto: TRect;
begin
  if (ARow = 0) then
  begin
    if (Acol = 4) then
    begin
      Recto := Rect;
      Grille.Canvas.Copymode := cmMergeCopy;
      Grille.Canvas.Font.Name := 'Symbol';
      Grille.Canvas.Font.size := 8;
      Grille.Canvas.Textout(Recto.left + 2, Recto.top + 2, 'l');
    end
    else
    begin

    end;
  end
  else if (ACol = 2) then
    if (Grille.Cells[1, Arow] = ' ' + LettreMat) then
    begin
      Recto := Rect;
      Grille.Canvas.pen.color := clRed;
      Grille.Canvas.Brush.color := clRed;
      Grille.Canvas.Brush.style := bsclear;
      Grille.Canvas.Rectangle(recto.left, recto.top,
        recto.right, Recto.bottom);
    end;
end;

procedure TFrameCompositions.GrilleKeyUp(Sender: TObject; var Key: word;
  Shift: TShiftState);
begin
  case Key of
    46: DeleteGrille.Click;
    88: CutGrille.Click;
    67: CopyGrille.Click;
    86: PasteGrille.Click;
  end;
end;

procedure TFrameCompositions.GrilleMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: integer);
var
  Col, Lig: integer;
begin
  Grille.MousetoCell(X, Y, Col, Lig);
  if (Lig < 8) and (Button = mbLeft) then
  begin
    Grille.BeginDrag(False);
    dragage := nil;
    dragage := TreeTemp.Items.getFirstNode;
    dragage.imageIndex := Lig + 10000;
    dragage.Text := Grille.cells[0, Lig];
  end;
end;

procedure TFrameCompositions.GrilleMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: integer);
var
  Acol, ARow: integer;
  Rect: TGridRect;
  p: TPoint;
begin
  if Button = mbRight then
  begin
    Grille.MouseToCell(X, Y, Acol, Arow);
    Rect.Top := Arow;
    Rect.Bottom := Arow;
    Rect.Left := 0;
    Rect.Right := Grille.ColCount - 1;
    Grille.Selection := Rect;
    p.x := X;
    p.Y := Y;
    P := Grille.ClientToScreen(P);
    PopupGrille.Popup(P.x, P.y);
  end;
end;

procedure TFrameCompositions.GrilleSelectCell(Sender: TObject;
  ACol, ARow: integer; var CanSelect: boolean);
var
  Sel: TGRidrect;
begin
  Sel.Left := 2;
  Sel.Right := 2;
  Sel.Bottom := ARow;
  Sel.Top := ARow;
  if (ACol = 2) and (Arow <> 0) and (ARow <> 8) and
    (Grille.Cells[1, ARow] <> ' ' + LettreElem) and
    {(Grille.Cells[1, ARow] <> ' ' + LettreMCP) and} (Grille.Cells[0, ARow] <> '') then
  begin
    Grille.Options := Grille.Options - [goRowselect];
    Grille.Options := Grille.Options + [goediting];
    Grille.Selection := Sel;
    if Grille.Cells[ACol, ARow] = '?' then
      Grille.Cells[ACol, ARow] := '';
    Grille.EditorMode := True;
  end
  else
  begin
    Grille.Options := Grille.Options + [goRowselect];
    Grille.Options := Grille.Options - [goediting];
  end;
end;

procedure TFrameCompositions.GrilleSetEditText(Sender: TObject;
  ACol, ARow: integer; const Value: string);
var
  epaisseur: double;
  Erreur: integer;
  Mat: TMateriau;
  Chaine: string;
begin
  if Value = '' then
    Exit;
  if ControlNum(ConvertPoint(Value), 0, 10000, 'R') = ConvertPoint(Value) then
  begin
    Mat := BibMateriaux.Get(Grille.Cells[0, Arow]);
    if Mat <> nil then
    begin
      Val(ConvertPoint(Value), epaisseur, erreur);
      Str(Mat.MasseVol * Epaisseur / 100: 0: 0, chaine);
      Grille.Cells[3, ARow] := chaine;
      Str(Epaisseur / 100 / Mat.Conductivite: 0: 2, chaine);
      Grille.Cells[5, ARow] := chaine;
    end;
  end;
  CalculSommeGrille;
  Modif := True;
end;

procedure TFrameCompositions.Open1Click(Sender: TObject);
var
  Noeud: TTreeNode;
  Compo: TComposition;
  Ok: boolean;
begin
  if Modif then
    Ok := ControleSauve(False)
  else
    Ok := True;
  if not Ok then
    Exit;
  Noeud := Tree.Selected;
  if Noeud <> nil then
  begin
    Compo := BibCompositions.Get(Noeud.Text);
    if Compo <> nil then
      Affiche(Compo);
  end;
end;

procedure TFrameCompositions.PasteGrilleClick(Sender: TObject);
begin
  if (Grille.Selection.Top > 0) and (Grille.Selection.Top <= 7) and
    (PressePapier.Count <> 0) then
    Grille.Rows[Grille.Selection.Top].Assign(PressePapier);
  Modif := True;
  CalculSommeGrille;
end;

procedure TFrameCompositions.PopupGrillePopup(Sender: TObject);
begin
  Rename1.Enabled := Tree.Selected.ImageIndex <= 1;
  Open1.Enabled := not Rename1.Enabled;

end;

procedure TFrameCompositions.Rename1Click(Sender: TObject);
var
  NouveauNom: string;
  Noeud: TTreeNode;
begin
  if Dragage = nil then
    Exit;
  NouveauNom := Dragage.Text;
  if InputQueryM(MessagesInterface[6], MessagesInterface[6], NouveauNom) then
  begin
    dragage.Text := NouveauNom;
    Noeud := dragage.GetFirstChild;
    while Noeud <> nil do
    begin
      TComposantBibliotheque(Noeud.Data).categorie := NouveauNom;
      Noeud := Noeud.getNextSibling;
    end;
  end;
end;

procedure TFrameCompositions.TreeChange(Sender: TObject; Node: TTreeNode);
var
  Chaine: string;
begin
  Str(BibCompositions.Count, chaine);
  if CheckHint then
    Tree.Hint := Chaine + MessagesInterface[8]
  else
    Tree.Hint := '';
end;

procedure TFrameCompositions.TreeClick(Sender: TObject);
var
  Noeud: TTreeNode;
  Compo: TComposition;
  Ok: boolean;
begin
  if Modif then
    Ok := ControleSauve(False)
  else
    Ok := True;
  if not Ok then
    Exit;
  Compo := nil;
  Noeud := Tree.Selected;
  if Noeud <> nil then
    Compo := BibCompositions.Get(Noeud.Text);
  if Compo <> nil then
    Affiche(Compo);
  Modif := False;
  CalculSommeGrille;
end;

procedure TFrameCompositions.TreeDragDrop(Sender, Source: TObject; X, Y: integer);
var
  Courant: TTreeNode;
  Compo: TComposition;
begin
  if (Tree.dropTarget.ImageIndex = BibCompositions.ImageIndexDossier) or
    (Tree.dropTarget.ImageIndex = BibCompositions.ImageIndexDossierS) then
  begin
    Compo := BibCompositions.Get(Dragage.Text);
    Compo.Categorie := Tree.DropTarget.Text;
    Tree.Items.Delete(dragage);
    Courant := BibCompositions.PoseDansArbre(Compo, Tree, True, False);
    Tree.alphaSort;
    Tree.FullCollapse;
    Tree.Selected := Courant;
  end;
end;

procedure TFrameCompositions.TreeDragOver(Sender, Source: TObject;
  X, Y: integer; State: TDragState; var Accept: boolean);
begin
  Accept := Sender = self;
  if Dragage <> nil then
    Accept := (dragage.ImageIndex = BibCompositions.ImageIndex)
  else
    Accept := False;
end;

procedure TFrameCompositions.TreeElemComposDblClick(Sender: TObject);
var
  i: integer;
begin
  if (TreeElemCompos.Selected.ImageIndex = BibElements.ImageIndex) or
    (TreeElemCompos.Selected.ImageIndex = BibElements.ImageIndexRO) then
    for i := 1 to 7 do
      if Grille.Cells[0, i] = '' then
      begin
        AfficheElem(BibElements.Get(TreeElemCompos.Selected.Text), i);
        Exit;
      end;
end;

procedure TFrameCompositions.TreeElemComposDragOver(Sender, Source: TObject;
  X, Y: integer; State: TDragState; var Accept: boolean);
begin
  Accept := Sender = self;
end;

procedure TFrameCompositions.TreeElemComposMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: integer);
begin
  Dragage := TTreeView(Sender).GetNodeAt(X, Y);
end;

procedure TFrameCompositions.TreeKeyUp(Sender: TObject; var Key: word;
  Shift: TShiftState);
begin
  if (Key = 46) and (Tree.Selected <> nil) then
    EffaceDragage
  else
    Tree.OnClick(Self);
end;

procedure TFrameCompositions.TreeMatComposDblClick(Sender: TObject);
var
  i: integer;
begin
  if (TreeMatCompos.Selected.ImageIndex = BibMateriaux.ImageIndex) or
    (TreeMatCompos.Selected.ImageIndex = BibMateriaux.ImageIndexRO) then
    for i := 1 to 7 do
      if Grille.Cells[0, i] = '' then
      begin
        AfficheMat(BibMateriaux.Get(TreeMatCompos.Selected.Text), i, -1);
        Exit;
      end;
end;

procedure TFrameCompositions.TreeMatComposDragOver(Sender, Source: TObject;
  X, Y: integer; State: TDragState; var Accept: boolean);
begin
  Accept := Sender = self;
end;

procedure TFrameCompositions.TreeMatComposMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: integer);
begin
  Dragage := TTreeView(Sender).GetNodeAt(X, Y);
end;

procedure TFrameCompositions.TreeMCPComposDragOver(Sender, Source: TObject;
  X, Y: integer; State: TDragState; var Accept: boolean);
begin
  Accept := Sender = self;
end;

procedure TFrameCompositions.TreeMCPComposMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: integer);
begin
  Dragage := TTreeView(Sender).GetNodeAt(X, Y);
end;

procedure TFrameCompositions.TreeMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: integer);
begin
  Dragage := TTreeView(Sender).GetNodeAt(X, Y);
end;

procedure TFrameCompositions.CalculSommeGrille;
var
  i: integer;
  Somme1, Somme2, Somme3: double;
  Chaine: string;
begin
  Somme1 := 0;
  Somme2 := 0;
  Somme3 := 0;
  for i := 1 to 7 do
  begin
    Somme1 := Somme1 + getDouble(Grille.Cells[2, i]);
    Somme2 := Somme2 + getDouble(Grille.Cells[3, i]);
    Somme3 := Somme3 + getDouble(Grille.Cells[5, i]);
  end;
  Str(Somme1: 0: 1, Chaine);
  Grille.Cells[2, 8] := Chaine;
  Str(Somme2: 0: 0, Chaine);
  Grille.Cells[3, 8] := Chaine;
  Str(Somme3: 0: 2, Chaine);
  Grille.Cells[5, 8] := Chaine;
  Grille.Cells[0, 8] := 'TOTAL';
end;

function TFrameCompositions.ControleSauve(Forcee: boolean): boolean;
var
  reponse: word;
  Silencieux: boolean;
  comp:  TComposition;
  Noeud: TTreeNode;
begin
  if not(ModificationAutorise) and not(TApp.isMaster) then
  begin
    MessageDlgM(MessagesInterface[7], mtWarning, [mbOK], 0);
    Result := False;
    Exit;
  end;
  Result := False;
  FormParent.ActiveControl := Tree;
  if not Forcee then
    Reponse := MessageDlgM(MessagesInterface[5] + Nom.Text + '''',
      mtWarning, [mbCancel, mbNo, mbYes], 0)
  else
    Reponse := mrYes;
  if Reponse = mrCancel then
    Exit;
  Result := True;
  if (Reponse = mrYes) then
    if Controle then
    begin
      comp := TComposition.Create(Nom.Text, Categorie.Text, Complement.Text, Source.Text, Grille);
      comp.ReadOnly := CtrlDown and ShiftDown;
      Silencieux := False;
      if BibCompositions.Ajoute(comp, Silencieux) then
      begin
        Noeud := BibCompositions.PoseDansArbre(comp, Tree, True, True);
        Tree.Selected := Noeud;
      end
      else
        comp.Free;
    end
    else
      Result := False;
  Modif := False;
  BibCompositions.SauveListe;
end;

procedure TFrameCompositions.CopyGrilleClick(Sender: TObject);
begin
  if (Grille.Selection.top > 0) and (Grille.Selection.Top <= 7) then
    PressePapier.Assign(Grille.Rows[Grille.Selection.top]);
end;

function TFrameCompositions.Controle: boolean;
var
  I: integer;
  Rectan: TgridRect;
  Erreur: integer;
  CanSelect: boolean;
  Compteur: integer;
begin
  if EstNumerique(Nom.Text) then
    Nom.Text := '_' + Nom.Text;
  Erreur := 0;
  if Categorie.Text = '' then
  begin
    FormParent.FocusControl(Categorie);
    Erreur := 1;
  end
  else if ControlNum(nom.Text, 4, 256, 'C') <> nom.Text then
  begin
    FormParent.FocusControl(nom);
    Erreur := 2;
  end
  else if ControlNum(Complement.Text, 0, 256, 'C') <> Complement.Text then
  begin
    FormParent.FocusControl(Complement);
    Erreur := 3;
  end;
  Compteur := 0;
  for i := 1 to NbMatParCompo do
  begin
    if (grille.Cells[0, i] <> '') then
    begin
      Inc(Compteur);
      if ControlNum(ConvertPoint(grille.Cells[2, i])
        , 0.01, 10000, 'R') <> ConvertPoint(grille.Cells[2, i]) then
      begin
        FormParent.FocusControl(Grille);
        Erreur := 4;
        Rectan.Top := i;
        Rectan.Bottom := i;
        rectan.Left := 2;
        Rectan.Right := 2;
        Grille.Selection := rectan;
        CanSelect := True;
        GrilleSelectCell(Self, rectan.Left, Rectan.Top, CanSelect);
      end;
    end;
  end;
  if Compteur = 0 then
  begin
    FormParent.FocusControl(Grille);
    Erreur := 5;
  end;
  if Erreur <> 0 then
  begin
    MessageDlgM(MessagesInterface[8 + Erreur], mtWarning, [mbOK], 0);
    Result := False;
  end
  else
    Result := True;
end;

procedure TFrameCompositions.EffaceDragage;
var
  i, index: integer;
begin
  //Cas des cat�gories
  if (Dragage.ImageIndex = BibCompositions.ImageIndexDossier) or
    (Dragage.ImageIndex = BibCompositions.ImageIndexDossierS) then
  begin
    if dragage.HasChildren then
      MessageDlgM(MessagesInterface[0], mtWarning, [mbOK], 0)
    else
    begin
      Categorie.items.Delete(Categorie.Items.IndexOf(dragage.Text));
      Tree.items.Delete(dragage);
    end;
  end
  else if (Dragage.ImageIndex = BibCompositions.ImageIndex)  or (TApp.isMaster and (Dragage.ImageIndex = BibCompositions.ImageIndexRO )) then
  begin
    if (MessageDlgM(MessagesInterface[1] + dragage.Text + MessagesInterface[2],
      mtConfirmation, mbOkCancel, 0) = mrOk) and
      BibCompositions.Efface(dragage.Text) then
      begin
        Tree.Items.Delete(dragage);
        BibCompositions.SauveListe();
      end;
  end
  else if (Dragage.ImageIndex >= 10001) then
  begin
    index := dragage.imageIndex - 10000;
    for i := Index + 1 to NbMatParCompo do
      Grille.Rows[i - 1] := Grille.Rows[i];
    Grille.Rows[7].Clear;
    CalculSommeGrille;
  end;
end;

procedure TFrameCompositions.ResizeCategoryPanel;
Var
  PrecedentOuvert: TCategoryPanel;
  i: integer;
  VisibleCount : Integer;
begin
  AutoCollapse := False;
  CategoryPanelGroupMatElem.DisableAlign;
  PrecedentOuvert := nil;
  for i := 0 to CategoryPanelGroupMatElem.Panels.Count - 1 do
    if not TcategoryPanel(CategoryPanelGroupMatElem.Panels[i]).Collapsed then
      PrecedentOuvert := CategoryPanelGroupMatElem.Panels[i];
  CategoryPanelGroupMatElem.ExpandAll;

  VisibleCount := 0;
  for i := 0 to CategoryPanelGroupMatElem.Panels.Count - 1 do
     if TcategoryPanel(CategoryPanelGroupMatElem.Panels[i]).visible then
       inc(visibleCount);

  for i := 0 to CategoryPanelGroupMatElem.Panels.Count - 1 do
  begin
    TcategoryPanel(CategoryPanelGroupMatElem.Panels[i]).Height :=
      CategoryPanelGroupMatElem.ClientHeight -
      (CategoryPanelGroupMatElem.HeaderHeight * visibleCount);
  end;
  CategoryPanelGroupMatElem.CollapseAll;
  PrecedentOuvert.Expand;
  CategoryPanelGroupMatElem.EnableAlign;
  AutoCollapse := True;
end;

procedure TFrameCompositions.Splitter1Moved(Sender: TObject);
begin
  ResizeCategoryPanel;
end;

procedure TFrameCompositions.FrameResize(Sender: TObject);
begin
  with Grille do
  begin
    FixedColor := clbtnface;

    DefaultColWidth := ClientWidth div 13;//15;
    ColWidths[5] := DefaultColWidth * 2;
    ColWidths[4] := DefaultColWidth * 2;
    ColWidths[3] := 0;//DefaultColWidth * 2;
    ColWidths[2] := DefaultColWidth * 2;
    ColWidths[1] := DefaultColWidth;
    ColWidths[0] := ClientWidth - (DefaultColWidth + 1) * 7(*9*) + 3;
  end;
  ZGroupBox15.Height  := zpanel1.ClientHeight div 2;
  TreeMatCompos.Width := (zGroupBox15.ClientWidth - 9) div 2;
  ResizeCategoryPanel;
end;

procedure TFrameCompositions.Affiche(Compo: TComposition);
var
  i: integer;
  Mat: TMateriau;
  Elem: TElement;
  indice: integer;
begin
  Nom.Text := Compo.Nom;
  Complement.Text := Compo.Complement;
  Source.Text := Compo.Source;

  BibCompositions.ConvertitCategorie(Compo);
  indice := Categorie.Items.IndexOf(Compo.Categorie);
  if Indice = -1 then
  begin
    BibCompositions.AjouteCategorie(Compo.Categorie, Categorie.items, Tree);
    indice := Categorie.Items.IndexOf(Compo.Categorie);
  end;
  Categorie.ItemIndex := Indice;

  for i := 1 to 7 do
    Grille.Rows[i].Clear;
  for i := 1 to Compo.NbMat do
  begin
    case Compo.TypeMat[i] of
      LettreMat:
      begin
        Mat := BibMateriaux.Get(Compo.IdentiMat[i]);
        if Mat <> nil then
          AfficheMat(Mat, i, Compo.Epaisseur[i]);
      end;
      LettreElem:
      begin
        Elem := BibElements.Get(Compo.IdentiMat[i]);
        if Elem <> nil then
          AfficheElem(Elem, i);
      end;
    end;
  end;
  CalculSommeGrille;
  modif := false;
end;

procedure TFrameCompositions.AfficheElem(Elem: TElement; Ligne: integer);
begin
  if Elem = nil then
    Exit;
  Grille.cells[0, Ligne] := Elem.Nom;
  Grille.Cells[1, Ligne] := ' ' + LettreElem;
  Grille.Cells[2, Ligne] := FormatFloat('0.0', Elem.epaisseur, tfs);
  Grille.Cells[3, Ligne] :=
    FormatFloat('0', Elem.MasseVol * Elem.epaisseur / 100, tfs);
  Grille.Cells[4, Ligne] := FormatFloat('0.000', Elem.ConductiviteEquiv, tfs);
  Grille.Cells[5, Ligne] := FormatFloat('0.00', Elem.Resistance, tfs);
  CalculSommeGrille;
end;

procedure TFrameCompositions.AfficheMat(Mat: TMateriau; Ligne: integer;
  Epaisseur: double);
begin
  if Mat = nil then
    Exit;
  Grille.cells[0, Ligne] := Mat.Nom;
  Grille.Cells[1, Ligne] := ' ' + LettreMat;
  Grille.Cells[4, Ligne] := FormatFloat('0.000', Mat.Conductivite, tfs);
  if Epaisseur = -1 then
  begin
    Grille.Cells[2, Ligne] := '?';
    Grille.Cells[3, Ligne] := '?';
    Grille.Cells[5, Ligne] := '?';
  end
  else
  begin
    Grille.Cells[2, Ligne] := FormatFloat('0.0', Epaisseur, tfs);
    Grille.Cells[3, Ligne] := FormatFloat('0', Mat.MasseVol * Epaisseur / 100, tfs);
    if Mat.Conductivite <> 0 then
      Grille.Cells[5, Ligne] :=
        FormatFloat('0.00', 1 / Mat.Conductivite * Epaisseur / 100, tfs)
    else
      Grille.Cells[5, Ligne] := '';

  end;
  CalculSommeGrille;
end;


end.
