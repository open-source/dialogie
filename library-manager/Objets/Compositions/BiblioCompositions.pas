unit BiblioCompositions;

interface

uses
  Classes,
  ComCtrls,
  GestionBib,
  Compositions,
  ComposantBibliotheque;

const
  FichierCompositionNS = 'CompositionsNS.txt';

type
  TBiblioCompositions = class(TBibliotheque)
  private
const
  FichierCompositionS = 'CompositionsS.txt';
    procedure loadFromFileV0(filePath: string);
  protected
    function CreationPourImport: TComposantBibliotheque; override;
    procedure ChargeListeAncien(standardLib : boolean); override;
  public
    // Commun
    constructor Create(Standard, NonStandard: string);
    function Get(Nom: string): TComposition; overload;
    function Get(Identificateur: integer): TComposition; overload;

    // Specifique
    procedure ReconstruitAncienneBiblio;
  end;

var
  BibCompositions: TBiblioCompositions;

implementation

uses
  SysUtils,
  Materiaux,
  Elements,
  BiblioMateriaux,
  BiblioElements,
  Dialogs,
  IzUtilitaires;


function TBiblioCompositions.CreationPourImport: TComposantBibliotheque;
begin
  result := TComposition.Create;
end;

constructor TBiblioCompositions.Create(Standard, NonStandard: string);
begin
  inherited Create(Standard, NonStandard);
  ImageIndex := 10;
  ImageIndexRO := 12;
  TypeComposant := 'Compositions';
  ChargeListe;
end;

function TBiblioCompositions.Get(Nom: string): TComposition;
begin
  result := inherited Get(Nom) as TComposition;
end;

function TBiblioCompositions.Get(Identificateur: integer): TComposition;
begin
  result := TComposition(inherited Get(Identificateur));
end;

procedure TBiblioCompositions.ReconstruitAncienneBiblio;
var
  i: integer;
  compo: TComposition;
  j: integer;
  Erreur: integer;
  Entier: integer;
  Mat: TMateriau;
  Elem: TElement;
begin
  for i := 0 to Table.Count - 1 do
  begin
    compo := TComposition(Table.Objects[i]);
    for j := 1 to compo.nbMat do
    begin
      val(compo.identiMat[j], Entier, Erreur);
      if Erreur = 0 then
      begin
        if compo.TypeMat[j] = LettreMat then
        begin
          Mat := BibMateriaux.Get(Entier);
          if Mat <> nil then
            compo.identiMat[j] := Mat.nom
          else
            compo.identiMat[j] := '';
        end
        else if compo.TypeMat[j] = LettreElem then
        begin
          Elem := BibElements.Get(Entier);
          if Elem <> nil then
            compo.identiMat[j] := Elem.nom
          else
            compo.identiMat[j] := '';
        end;
      end;
    end;
  end;
end;


procedure TBiblioCompositions.ChargeListeAncien(standardLib : boolean);
begin
  if standardLib then
    loadFromFileV0(CheminBiblioStandard + FichierCompositionS)
  else
    loadFromFileV0(CheminBiblioNonStandard + FichierCompositionNS);
end;

procedure TBiblioCompositions.loadFromFileV0(filePath : string);
var
  f: Textfile;
  ligne: string;
  compo: TComposition;
  reel: double;
  Identi, Entier, i: integer;
  SansID: boolean;
begin
  if FileExists(filePath) then
  begin
    hasToSave := true;
    Assign(f, filePath);
    Reset(f);
    ReadLn(f, ligne);
    SansID := ligne = 'SansID';
    if not SansID then
      Reset(f);
    while not EOF(f) do
    begin
      compo := TComposition.Create;
      ReadLn(f, ligne);
      ReadLn(f, ligne);
      Identi := getInt(ligne);
      compo.Identificateur := Identi;
      ReadLn(f, ligne);
      ReadLn(f, ligne);
      compo.Categorie := ligne;
      ReadLn(f, ligne);
      ReadLn(f, ligne);
      compo.nom := ligne;
      ReadLn(f, ligne);
      ReadLn(f, ligne);
      compo.complement := ligne;
      ReadLn(f, ligne);
      ReadLn(f, ligne);
      compo.Source := ligne;
      ReadLn(f, ligne);
      ReadLn(f, ligne);
      Entier := getInt(ligne);
      compo.nbMat := Entier;
      for i := 1 to compo.nbMat do
      begin
        ReadLn(f, ligne);
        compo.TypeMat[i] := ligne[1];

        ReadLn(f, ligne);
        reel := getDouble(ligne);
        compo.Epaisseur[i] := reel;

        ReadLn(f, ligne);
        compo.identiMat[i] := ligne;
      end;
      ReadLn(f, ligne);
      ReadLn(f, ligne);
      compo.ReadOnly := (ligne = '1');
      Table.AddObject(compo.nom, compo);
      ReadLn(f, ligne);

      if not compo.ReadOnly and (compo.Identificateur < EcartStandard) then
        compo.Identificateur := compo.Identificateur + EcartStandard;

      if (Identi > IdMax) and compo.ReadOnly then
        IdMax := Identi
      else if (Identi > IdMax) then
        IdMax := Identi - EcartStandard;
    end;
    Closefile(f);
    if not SansID then
      ReconstruitAncienneBiblio;
  end;
end;

end.
