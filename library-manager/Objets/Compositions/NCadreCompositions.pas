unit NCadreCompositions;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, CadreBib, Menus, StdCtrls, Buttons, BarreAdresse, ExtCtrls, ComCtrls,
  Grids, ComposantBibliotheque, Elements, Materiaux, MatChangementPhase,
  ImgList, PontsIntegres;

type
  TFrameCompositions = class(TFrameBib)
    PanelSpecifique: TPanel;
    zLabel73: TLabel;
    zLabel74: TLabel;
    ZImageFleche: TImage;
    TreeTemp: TTreeView;
    Grille: TStringGrid;
    PopupGrille: TPopupMenu;
    DeleteGrille: TMenuItem;
    CutGrille: TMenuItem;
    CopyGrille: TMenuItem;
    PasteGrille: TMenuItem;
    Splitter1: TSplitter;
    ZGroupBox15: TGroupBox;
    CategoryPanelGroupMatElem: TCategoryPanelGroup;
    CategoryPanelMCP: TCategoryPanel;
    TreeMCPCompos: TTreeView;
    CategoryPanelElem: TCategoryPanel;
    TreeElemCompos: TTreeView;
    CategoryPanelMat: TCategoryPanel;
    TreeMatCompos: TTreeView;
    TreeToutFiltre: TTreeView;
    PanelFiltreSimple: TPanel;
    Label2: TLabel;
    EditFiltreSimple: TButtonedEdit;
    ImageListCategory: TImageList;
    CategoryPanelPont: TCategoryPanel;
    TreePontCompos: TTreeView;
    LabelPonts: TLabel;
    GrilleP: TStringGrid;
    PopupGrilleP: TPopupMenu;
    DeleteGrilleP: TMenuItem;
    CutGrilleP: TMenuItem;
    CopyGrilleP: TMenuItem;
    PasteGrilleP: TMenuItem;
    GrillePP: TStringGrid;
    Label1: TLabel;
    procedure GrilleDragDrop(Sender, Source: TObject; X, Y: integer);
    procedure GrilleDragOver(Sender, Source: TObject; X, Y: integer;
      State: TDragState; var Accept: boolean);
    procedure GrilleDrawCell(Sender: TObject; ACol, ARow: integer; Rect: TRect;
      State: TGridDrawState);
    procedure GrilleKeyUp(Sender: TObject; var Key: word; Shift: TShiftState);
    procedure GrilleMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: integer);
    procedure GrilleMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: integer);
    procedure GrilleSelectCell(Sender: TObject; ACol, ARow: integer;
      var CanSelect: boolean);
    procedure GrilleSetEditText(Sender: TObject; ACol, ARow: integer;
      const Value: string);
    procedure DeleteGrilleClick(Sender: TObject);
    procedure CutGrilleClick(Sender: TObject);
    procedure CopyGrilleClick(Sender: TObject);
    procedure PasteGrilleClick(Sender: TObject);
    procedure BitBtnNewClick(Sender: TObject);
    procedure FrameResize(Sender: TObject);
    procedure CategoryPanelElemExpand(Sender: TObject);
    procedure TreeElemComposDblClick(Sender: TObject);
    procedure TreeElemComposDragOver(Sender, Source: TObject; X, Y: integer;
      State: TDragState; var Accept: boolean);
    procedure TreeElemComposMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: integer);
    procedure TreeMatComposDblClick(Sender: TObject);
    procedure TreeMatComposDragOver(Sender, Source: TObject; X, Y: integer;
      State: TDragState; var Accept: boolean);
    procedure TreeMatComposMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: integer);
    procedure TreeMCPComposDblClick(Sender: TObject);
    procedure TreeMCPComposDragOver(Sender, Source: TObject; X, Y: integer;
      State: TDragState; var Accept: boolean);
    procedure TreeMCPComposMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: integer);
    procedure EditFiltreSimpleChange(Sender: TObject);
    procedure EditFiltreSimpleRightButtonClick(Sender: TObject);
    procedure TreeToutFiltreChange(Sender: TObject; Node: TTreeNode);
    procedure TreeToutFiltreMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: integer);
    procedure TreeToutFiltreMouseMove(Sender: TObject; Shift: TShiftState;
      X, Y: integer);
    procedure TreeElemComposMouseMove(Sender: TObject; Shift: TShiftState;
      X, Y: integer);
    procedure TreeMatComposMouseMove(Sender: TObject; Shift: TShiftState;
      X, Y: integer);
    procedure TreeMCPComposMouseMove(Sender: TObject; Shift: TShiftState;
      X, Y: integer);
    procedure TreeMatComposMouseLeave(Sender: TObject);
    procedure TreeElemComposMouseLeave(Sender: TObject);
    procedure TreeMCPComposMouseLeave(Sender: TObject);
    procedure TreeToutFiltreMouseLeave(Sender: TObject);
    procedure Splitter1Moved(Sender: TObject);
    procedure TreePontComposMouseLeave(Sender: TObject);
    procedure TreePontComposMouseMove(Sender: TObject; Shift: TShiftState;
      X, Y: integer);
    procedure GrillePDragDrop(Sender, Source: TObject; X, Y: integer);
    procedure GrillePDragOver(Sender, Source: TObject; X, Y: integer;
      State: TDragState; var Accept: boolean);
    procedure GrillePDrawCell(Sender: TObject; ACol, ARow: integer;
      Rect: TRect; State: TGridDrawState);
    procedure GrillePKeyUp(Sender: TObject; var Key: word; Shift: TShiftState);
    procedure DeleteGrillePClick(Sender: TObject);
    procedure CutGrillePClick(Sender: TObject);
    procedure CopyGrillePClick(Sender: TObject);
    procedure PasteGrillePClick(Sender: TObject);
    procedure GrillePMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: integer);
    procedure GrillePMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: integer);
    procedure GrillePSelectCell(Sender: TObject; ACol, ARow: integer;
      var CanSelect: boolean);
    procedure GrillePSetEditText(Sender: TObject; ACol, ARow: integer;
      const Value: string);
    procedure CategoryPanelMatExpand(Sender: TObject);
    procedure CategoryPanelMCPExpand(Sender: TObject);
    procedure CategoryPanelPontExpand(Sender: TObject);
    procedure GrillePPClick(Sender: TObject);
    procedure GrillePPDrawCell(Sender: TObject; ACol, ARow: integer;
      Rect: TRect; State: TGridDrawState);
    procedure GrillePPDragOver(Sender, Source: TObject; X, Y: integer;
      State: TDragState; var Accept: boolean);
    procedure GrillePPDragDrop(Sender, Source: TObject; X, Y: integer);
    procedure GrillePPKeyUp(Sender: TObject; var Key: word; Shift: TShiftState);
    procedure GrillePPMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: integer);
  private
    { Déclarations privées }
    PressePapier: TStringList;
    procedure CalculSommeGrille;
    procedure CalculSommePont;
    function FaireLeTrou(Grille: TStringGrid; Position: integer): integer;
    function FaireLeTrouP(Grille: TStringGrid; Position: integer): integer;
    procedure AfficheElem(Elem: TElement; Ligne: integer);
    procedure AfficheMat(Mat: TMateriau; Ligne: integer; Epaisseur: double);
    procedure AfficheMCP(MCP: TMCP; Ligne: integer);
    procedure AffichePont(Pont: TPontIntegre; Ligne: integer; entraxe: double);
    procedure GestionCollapse(Sender: TObject);
    Function GrillePVide: boolean;
  protected
    AutoCollapse: boolean;
    function Controle: boolean; override;
  public
    { Déclarations publiques }
    function CreerComposantBiblio: TComposantBibliotheque; override;
    procedure Affiche(comp: TComposantBibliotheque; Survol: boolean); override;
    function AccepteDragGrille: boolean;
    procedure EffaceDragageGrille;
    constructor Create(AOwner: TComponent); override;
    Destructor Destroy; override;
  end;

var
  FrameCompositions: TFrameCompositions;

implementation

uses
  Compositions, Utilitaires, dialogsinternational, BiblioMateriaux,
  BiblioMCP, BiblioElements,
  BiblioCompositions, Constantes, MiniMat, MiniElem, MiniMCP, MiniImage,
  MiniPontIntegre, BiblioPontsIntegres, translateur;
{$R *.dfm}

constructor TFrameCompositions.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  PressePapier := TStringList.Create;
  GrillePP.OnClick(Self);
end;

Destructor TFrameCompositions.Destroy;
begin
  PressePapier.Free;
  inherited Destroy;
end;

procedure TFrameCompositions.EffaceDragageGrille;
var
  i: integer;
  index: integer;
begin
  if dragage.ImageIndex > 0 then
  begin
    index := dragage.ImageIndex - 10000;
    for i := Index + 1 to NbMatParCompo do
      Grille.Rows[i - 1] := Grille.Rows[i];
    Grille.Rows[7].Clear;
    CalculSommeGrille;
  end
  else if dragage.ImageIndex = -10000 then
  begin
    GrillePP.Rows[0].Clear;
  end
  else
  begin
    index := -(dragage.ImageIndex + 10000);
    for i := Index + 1 to NbPontParCompoMax do
      GrilleP.Rows[i - 1] := GrilleP.Rows[i];
    GrilleP.Rows[5].Clear;
    GrillePP.Selection := TGridRect(Rect(-1, -1, -1, -1));
    CalculSommePont;
  end;
  Store;
end;

function TFrameCompositions.AccepteDragGrille: boolean;
begin
  Result := dragage.ImageIndex <= 10000 + NbMatParCompo;
end;

function TFrameCompositions.CreerComposantBiblio: TComposantBibliotheque;
begin
  Result := TComposition.Create(EditNom.Text, BarreCategorie.Chemin,
    MComplement, EditSource.Text, Grille, GrilleP, GrillePP, False);
end;

function TFrameCompositions.Controle: boolean;
var
  Erreur: integer;
  Compteur: integer;
  Rectan: TGridRect;
  i: integer;
  CanSelect: boolean;
begin
  Erreur := 0;
  if not inherited Controle then
    Exit(False);
  Compteur := 0;
  for i := 1 to NbMatParCompo do
  begin
    if (Grille.Cells[0, i] <> '') then
    begin
      Inc(Compteur);
      if ControlNum(ConvertPoint(Grille.Cells[2, i]), 0.01, 10000, 'R')
        <> ConvertPoint(Grille.Cells[2, i]) then
      begin
        FormParent.FocusControl(Grille);
        Erreur := 4;
        Rectan.Top := i;
        Rectan.Bottom := i;
        Rectan.Left := 2;
        Rectan.Right := 2;
        Grille.Selection := Rectan;
        CanSelect := True;
        GrilleSelectCell(Self, Rectan.Left, Rectan.Top, CanSelect);
      end;
    end;
  end;

  for i := 1 to NbPontParCompoMax - 1 do
  begin
    if (GrilleP.Cells[0, i] <> '') then
    begin
      if TTypePontIntegre(StrToInt(GrilleP.Cells[1, i])) = TPI_Lineique then
      begin
        if ControlNum(ConvertPoint(GrilleP.Cells[2, i]), 0.0001, 1000000, 'R')
          <> ConvertPoint(GrilleP.Cells[2, i]) then
        begin
          FormParent.FocusControl(GrilleP);
          Erreur := 6;
          Rectan.Top := i;
          Rectan.Bottom := i;
          Rectan.Left := 2;
          Rectan.Right := 2;
          Grille.Selection := Rectan;
          CanSelect := True;
          GrilleSelectCell(Self, Rectan.Left, Rectan.Top, CanSelect);
        end;
      end
      else if ControlNum(ConvertPoint(GrilleP.Cells[4, i]), 0.0001, 1000000,
        'R') <> ConvertPoint(GrilleP.Cells[4, i]) then
      begin
        FormParent.FocusControl(GrilleP);
        Erreur := 6;
        Rectan.Top := i;
        Rectan.Bottom := i;
        Rectan.Left := 4;
        Rectan.Right := 4;
        Grille.Selection := Rectan;
        CanSelect := True;
        GrilleSelectCell(Self, Rectan.Left, Rectan.Top, CanSelect);
      end;

    end;
  end;

  if Compteur = 0 then
  begin
    FormParent.FocusControl(Grille);
    Erreur := 5;
  end;
  if Erreur <> 0 then
  begin
    MessageDlgM(MessagesInterface[8 + Erreur], mtWarning, [mbOK], 0);
    Result := False;
  end
  else
    Result := True;
end;

procedure TFrameCompositions.GrilleDragDrop(Sender, Source: TObject;
  X, Y: integer);
var
  Nom: string;
  Col, lig: integer;
  Tampon: TStrings;
  Resultat: integer;
  Mat: TMateriau;
  Elem: TElement;
  Position: integer;
  MCP: TMCP;
begin
  Grille.MouseToCell(X, Y, Col, lig);
  if lig < 8 then
  begin
    if (dragage.ImageIndex = BibMateriaux.ImageIndex) or
      (dragage.ImageIndex = BibMateriaux.ImageIndexRO) then
    begin
      Mat := BibMateriaux.Get(dragage.Text);
      if FaireLeTrou(Grille, lig) <> 0 then
        AfficheMat(Mat, lig, -1)
      else
        MessageBeep(MB_ICONEXCLAMATION);
    end;
    if (dragage.ImageIndex = BibElements.ImageIndex) or
      (dragage.ImageIndex = BibElements.ImageIndexRO) then
    begin
      Nom := dragage.Text;
      Elem := BibElements.Get(dragage.Text);
      if FaireLeTrou(Grille, lig) <> 0 then
        AfficheElem(Elem, lig)
      else
        MessageBeep(MB_ICONEXCLAMATION);
    end;
    if (dragage.ImageIndex = BibMCP.ImageIndex) or
      (dragage.ImageIndex = BibMCP.ImageIndexRO) then
    begin
      Nom := dragage.Text;
      MCP := BibMCP.Get(dragage.Text);
      if FaireLeTrou(Grille, lig) <> 0 then
        AfficheMCP(MCP, lig)
      else
        MessageBeep(MB_ICONEXCLAMATION);
    end;
    if (dragage.ImageIndex >= 10001) then
    begin
      Position := dragage.ImageIndex - 10000;
      Grille.MouseToCell(X, Y, Col, lig);
      if lig <> Position then
        Resultat := FaireLeTrou(Grille, lig)
      else
        Resultat := -1;
      if (Resultat = 1) or ((Resultat = 2) and (Position < lig)) then
      begin
        Tampon := Grille.Rows[Position];
        Grille.Rows[lig] := Tampon;
        if not CtrlDown then
          Grille.Rows[Position].Clear;
      end
      else if (Resultat = 2) and (Position > lig) then
      begin
        Tampon := Grille.Rows[Position + 1];
        Grille.Rows[lig] := Tampon;
        if not CtrlDown then
          Grille.Rows[Position + 1].Clear;
      end;
    end;
  end;
  CalculSommeGrille;
end;

procedure TFrameCompositions.GrilleDragOver(Sender, Source: TObject;
  X, Y: integer; State: TDragState; var Accept: boolean);
var
  ARow, ACol: integer;
begin
  Grille.MouseToCell(X, Y, ACol, ARow);
  if dragage <> nil then
    Accept := ((dragage.ImageIndex = BibMateriaux.ImageIndex) or
        (dragage.ImageIndex = BibMateriaux.ImageIndexRO) or
        (dragage.ImageIndex = BibElements.ImageIndex) or
        (dragage.ImageIndex = BibElements.ImageIndexRO) or
        (dragage.ImageIndex = BibMCP.ImageIndex) or
        (dragage.ImageIndex = BibMCP.ImageIndexRO) or
        (dragage.ImageIndex > 10000)) and (ARow > 0)
  else
    Accept := False;
end;

procedure TFrameCompositions.GrilleDrawCell(Sender: TObject;
  ACol, ARow: integer; Rect: TRect; State: TGridDrawState);
var
  Recto: TRect;
begin
  if (ACol = 2) then
    if (Grille.Cells[1, ARow] = ' ' + LettreMat) then
    begin
      Recto := Rect;
      Grille.Canvas.pen.color := clRed;
      Grille.Canvas.Brush.color := clRed;
      Grille.Canvas.Brush.style := bsclear;
      Grille.Canvas.Rectangle(Recto.Left, Recto.Top, Recto.Right, Recto.Bottom);
    end;
  if (ACol = 1) and (ARow > 0) then
  begin
    Grille.Canvas.Brush.style := bsSolid;
    Grille.Canvas.Brush.color := Grille.color;
    Grille.Canvas.FillRect(Rect);
    if (Grille.Cells[1, ARow] = ' ' + LettreMat) then
      TreeMatCompos.Images.Draw(Grille.Canvas, Rect.Left, Rect.Top + 1,
        BibMateriaux.ImageBrute, True)
    else if (Grille.Cells[1, ARow] = ' ' + LettreElem) then
      TreeMatCompos.Images.Draw(Grille.Canvas, Rect.Left, Rect.Top + 1,
        BibElements.ImageBrute, True)
    else if (Grille.Cells[1, ARow] = ' ' + LettreMCP) then
      TreeMatCompos.Images.Draw(Grille.Canvas, Rect.Left, Rect.Top + 1,
        BibMCP.ImageBrute, True);
  end;
end;

procedure TFrameCompositions.GrilleKeyUp(Sender: TObject; var Key: word;
  Shift: TShiftState);
begin
  case Key of
    46:
      DeleteGrille.Click;
    88:
      CutGrille.Click;
    67:
      CopyGrille.Click;
    86:
      PasteGrille.Click;
  end;
  CalculSommeGrille;
end;

procedure TFrameCompositions.GrilleMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: integer);
var
  Col, lig: integer;
begin
  Grille.MouseToCell(X, Y, Col, lig);
  if (lig < 8) and (Button = mbLeft) then
  begin
    Grille.BeginDrag(False);
    dragage := nil;
    dragage := TreeTemp.Items.getFirstNode;
    dragage.ImageIndex := lig + 10000;
    dragage.Text := Grille.Cells[0, lig];
  end;
end;

procedure TFrameCompositions.GrilleMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: integer);
var
  ACol, ARow: integer;
  Rect: TGridRect;
  p: TPoint;
begin
  if Button = mbRight then
  begin
    Grille.MouseToCell(X, Y, ACol, ARow);
    Rect.Top := ARow;
    Rect.Bottom := ARow;
    Rect.Left := 0;
    Rect.Right := Grille.ColCount - 1;
    Grille.Selection := Rect;
    p.X := X;
    p.Y := Y;
    p := Grille.ClientToScreen(p);
    PopupGrille.Popup(p.X, p.Y);
  end;
end;

procedure TFrameCompositions.GrilleSelectCell(Sender: TObject;
  ACol, ARow: integer; var CanSelect: boolean);
var
  Sel: TGridRect;
begin
  Sel.Left := 2;
  Sel.Right := 2;
  Sel.Bottom := ARow;
  Sel.Top := ARow;
  if (ACol = 2) and (ARow <> 0) and (ARow <> 8) and
    (Grille.Cells[1, ARow] <> ' ' + LettreElem) and
    (Grille.Cells[1, ARow] <> ' ' + LettreMCP) and
    (Grille.Cells[0, ARow] <> '') then
  begin
    Grille.Options := Grille.Options - [goRowselect];
    Grille.Options := Grille.Options + [goediting];
    Grille.Selection := Sel;
    if Grille.Cells[ACol, ARow] = '?' then
      Grille.Cells[ACol, ARow] := '';
    Grille.EditorMode := True;
  end
  else
  begin
    Grille.Options := Grille.Options + [goRowselect];
    Grille.Options := Grille.Options - [goediting];
  end;
end;

procedure TFrameCompositions.GrilleSetEditText(Sender: TObject;
  ACol, ARow: integer; const Value: string);
var
  Epaisseur: double;
  Erreur: integer;
  Mat: TMateriau;
  Chaine: string;
begin
  if Value = '' then
    Exit;
  if ControlNum(ConvertPoint(Value), 0, 10000, 'R') = ConvertPoint(Value) then
  begin
    Mat := BibMateriaux.Get(Grille.Cells[0, ARow]);
    if Mat <> nil then
    begin
      Val(ConvertPoint(Value), Epaisseur, Erreur);
      Str(Mat.MasseVol * Epaisseur / 100: 0: 0, Chaine);
      Grille.Cells[3, ARow] := Chaine;
      Str(Epaisseur / 100 / Mat.Conductivite: 0: 2, Chaine);
      Grille.Cells[5, ARow] := Chaine;
    end;
  end;
  CalculSommeGrille;
end;

procedure TFrameCompositions.PasteGrilleClick(Sender: TObject);
begin
  if (Grille.Selection.Top > 0) and (Grille.Selection.Top <= 7) and
    (PressePapier.Count <> 0) then
    Grille.Rows[Grille.Selection.Top].Assign(PressePapier);
  CalculSommeGrille;
end;

procedure TFrameCompositions.PasteGrillePClick(Sender: TObject);
begin
  if (GrilleP.Selection.Top > 0) and (GrilleP.Selection.Top <= 7) and
    (PressePapier.Count <> 0) then
    GrilleP.Rows[GrilleP.Selection.Top].Assign(PressePapier);
  CalculSommePont;
end;

procedure TFrameCompositions.Splitter1Moved(Sender: TObject);
begin
  inherited;
  OnResize(Splitter1);
end;

procedure TFrameCompositions.GrillePPClick(Sender: TObject);
begin
  inherited;
  GrillePP.Selection := TGridRect(Rect(-1, -1, -1, -1));
end;

procedure TFrameCompositions.GrillePPDragDrop(Sender, Source: TObject;
  X, Y: integer);
var
  Col, lig: integer;
  Pont: TPontIntegre;
begin
  GrillePP.MouseToCell(X, Y, Col, lig);
  if BibPontsIntegres.EstDuTypeGlobal(dragage.ImageIndex) then
  begin
    Pont := BibPontsIntegres.Get(dragage.Text);
    AffichePont(Pont, 0, -1)
  end
end;

procedure TFrameCompositions.GrillePPDragOver(Sender, Source: TObject;
  X, Y: integer; State: TDragState; var Accept: boolean);
var
  ARow, ACol: integer;
begin
  GrilleP.MouseToCell(X, Y, ACol, ARow);
  if dragage <> nil then
    Accept := (BibPontsIntegres.EstDuTypeGlobal(dragage.ImageIndex))
  else
    Accept := False;
end;

procedure TFrameCompositions.GrillePPDrawCell(Sender: TObject;
  ACol, ARow: integer; Rect: TRect; State: TGridDrawState);
begin
  GrillePP.Canvas.pen.color := clWindow;
  GrillePP.Canvas.Brush.color := clWindow;
  GrillePP.Canvas.Brush.style := bsclear;
  if (ACol = 1) then
  begin
    GrillePP.Canvas.Brush.style := bsSolid;
    GrillePP.Canvas.Brush.color := Grille.color;
    GrillePP.Canvas.FillRect(Rect);
    if GrillePP.Cells[0, 0] <> '' then
    begin
      TreePontCompos.Images.Draw(GrillePP.Canvas, Rect.Left, Rect.Top + 1,
        BibPontsIntegres.ImageBruteGlobal, True)
    end;
  end;
  GrillePP.Canvas.pen.color := clWindow;
  GrillePP.Canvas.Brush.color := clWindow;
  GrillePP.Canvas.Brush.style := bsclear;
end;

procedure TFrameCompositions.GrillePPKeyUp(Sender: TObject; var Key: word;
  Shift: TShiftState);
begin
  if Key = 46 then
    GrillePP.Rows[0].Clear;
  Store;
end;

procedure TFrameCompositions.GrillePPMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: integer);
var
  Col, lig: integer;
begin
  GrillePP.MouseToCell(X, Y, Col, lig);
  if (lig < 7) and (Button = mbLeft) then
  begin
    GrillePP.BeginDrag(False);
    dragage := nil;
    dragage := TreeTemp.Items.getFirstNode;
    dragage.ImageIndex := -(lig + 10000);
    dragage.Text := GrillePP.Cells[0, lig];
  end;
end;

procedure TFrameCompositions.GrillePDragDrop(Sender, Source: TObject;
  X, Y: integer);
var
  Col, lig: integer;
  Tampon: TStrings;
  Resultat: integer;
  Pont: TPontIntegre;
  Position: integer;
begin
  GrilleP.MouseToCell(X, Y, Col, lig);
  if (lig < 6) and (lig > 0) then
  begin
    if BibPontsIntegres.EstDuType(dragage.ImageIndex) then
    begin
      Pont := BibPontsIntegres.Get(dragage.Text);
      if FaireLeTrouP(GrilleP, lig) <> 0 then
        AffichePont(Pont, lig, -1)
      else
        MessageBeep(MB_ICONEXCLAMATION);
    end
    else if (dragage.ImageIndex <= -10001) then
    begin
      Position := -(dragage.ImageIndex + 10000);
      GrilleP.MouseToCell(X, Y, Col, lig);
      if lig <> Position then
        Resultat := FaireLeTrouP(GrilleP, lig)
      else
        Resultat := -1;
      if (Resultat = 1) or ((Resultat = 2) and (Position < lig)) then
      begin
        Tampon := GrilleP.Rows[Position];
        GrilleP.Rows[lig] := Tampon;
        if not CtrlDown then
          GrilleP.Rows[Position].Clear;
      end
      else if (Resultat = 2) and (Position > lig) then
      begin
        Tampon := GrilleP.Rows[Position + 1];
        GrilleP.Rows[lig] := Tampon;
        if not CtrlDown then
          GrilleP.Rows[Position + 1].Clear;
      end;
    end;
  end;
  CalculSommePont;
end;

procedure TFrameCompositions.GrillePDragOver(Sender, Source: TObject;
  X, Y: integer; State: TDragState; var Accept: boolean);
var
  ARow, ACol: integer;
begin
  GrilleP.MouseToCell(X, Y, ACol, ARow);
  if dragage <> nil then
    Accept := (BibPontsIntegres.EstDuTypeNonGlobal(dragage.ImageIndex) or
        (dragage.ImageIndex < -10000)) and (ARow > 0)
  else
    Accept := False;
end;

procedure TFrameCompositions.GrillePDrawCell(Sender: TObject;
  ACol, ARow: integer; Rect: TRect; State: TGridDrawState);
var
  Recto: TRect;
  TypP: TTypePontIntegre;
begin
  if ARow = 0 then
    Exit;
  GrilleP.Canvas.pen.color := clWindow;
  GrilleP.Canvas.Brush.color := clWindow;
  GrilleP.Canvas.Brush.style := bsclear;
  if (GrilleP.Cells[1, ARow] <> '') and (ARow <> 0) then
    TypP := TTypePontIntegre(StrToInt(GrilleP.Cells[1, ARow]))
  else
    TypP := TPI_Aucun;
  if ((ACol = 2) and (TypP = TPI_Lineique)) or
    ((ACol = 4) and (TypP = TPI_Ponctuel)) then
  begin
    Recto := Rect;
    GrilleP.Canvas.pen.color := clRed;
    GrilleP.Canvas.Brush.color := clRed;
    GrilleP.Canvas.Brush.style := bsclear;
    GrilleP.Canvas.Rectangle(Recto.Left, Recto.Top, Recto.Right, Recto.Bottom);
  end
  else if ((ACol = 2) or (ACol = 3)) and (TypP = TPI_Ponctuel) then
  begin
    Recto := Rect;
    GrilleP.Canvas.Brush.color := clsilver;
    GrilleP.Canvas.Brush.style := bsSolid;
    GrilleP.Canvas.Rectangle(Recto.Left, Recto.Top, Recto.Right, Recto.Bottom);
  end
  else if ((ACol = 4) or (ACol = 5)) and (TypP = TPI_Lineique) then
  begin
    Recto := Rect;
    GrilleP.Canvas.Brush.color := clsilver;
    GrilleP.Canvas.Brush.style := bsSolid;
    GrilleP.Canvas.Rectangle(Recto.Left, Recto.Top, Recto.Right, Recto.Bottom);
  end
  else if (ACol = 1) then
  begin
    GrilleP.Canvas.Brush.style := bsSolid;
    GrilleP.Canvas.Brush.color := Grille.color;
    GrilleP.Canvas.FillRect(Rect);
    if TypP = TPI_Lineique then
      TreePontCompos.Images.Draw(GrilleP.Canvas, Rect.Left, Rect.Top + 1,
        BibPontsIntegres.ImageBruteLineaire, True)
    else if TypP = TPI_Ponctuel then
      TreePontCompos.Images.Draw(GrilleP.Canvas, Rect.Left, Rect.Top + 1,
        BibPontsIntegres.ImageBrutePonctuelle, True)
  end;
  GrilleP.Canvas.pen.color := clWindow;
  GrilleP.Canvas.Brush.color := clWindow;
  GrilleP.Canvas.Brush.style := bsclear;
end;

procedure TFrameCompositions.GrillePKeyUp(Sender: TObject; var Key: word;
  Shift: TShiftState);
begin
  case Key of
    46:
      DeleteGrilleP.Click;
    88:
      CutGrilleP.Click;
    67:
      CopyGrilleP.Click;
    86:
      PasteGrilleP.Click;
  end;
  CalculSommePont;
end;

procedure TFrameCompositions.GrillePMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: integer);
var
  Col, lig: integer;
begin
  GrilleP.MouseToCell(X, Y, Col, lig);
  if (lig < 7) and (Button = mbLeft) then
  begin
    GrilleP.BeginDrag(False);
    dragage := nil;
    dragage := TreeTemp.Items.getFirstNode;
    dragage.ImageIndex := -(lig + 10000);
    dragage.Text := GrilleP.Cells[0, lig];
  end;
end;

procedure TFrameCompositions.GrillePMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: integer);
var
  ACol, ARow: integer;
  Rect: TGridRect;
  p: TPoint;
begin
  if Button = mbRight then
  begin
    GrilleP.MouseToCell(X, Y, ACol, ARow);
    Rect.Top := ARow;
    Rect.Bottom := ARow;
    Rect.Left := 0;
    Rect.Right := GrilleP.ColCount - 1;
    GrilleP.Selection := Rect;
    p.X := X;
    p.Y := Y;
    p := GrilleP.ClientToScreen(p);
    PopupGrilleP.Popup(p.X, p.Y);
  end;
end;

procedure TFrameCompositions.GrillePSelectCell(Sender: TObject;
  ACol, ARow: integer; var CanSelect: boolean);
var
  Sel: TGridRect;
  TypP: TTypePontIntegre;
begin
  if GrilleP.Cells[1, ARow] <> '' then
    TypP := TTypePontIntegre(StrToInt(GrilleP.Cells[1, ARow]))
  else
    TypP := TPI_Aucun;

  Sel.Left := 2;
  Sel.Right := 2;
  Sel.Bottom := ARow;
  Sel.Top := ARow;
  if (ACol = 2) and (ARow <> 0) and (ARow <> 7) and (TypP = TPI_Lineique) then
  begin
    GrilleP.Options := GrilleP.Options - [goRowselect];
    GrilleP.Options := GrilleP.Options + [goediting];
    GrilleP.Selection := Sel;
    if GrilleP.Cells[ACol, ARow] = '?' then
      GrilleP.Cells[ACol, ARow] := '';
    GrilleP.EditorMode := True;
  end
  else if (ACol = 4) and (ARow <> 0) and (ARow <> 7) and (TypP = TPI_Ponctuel)
    then
  begin
    GrilleP.Options := GrilleP.Options - [goRowselect];
    GrilleP.Options := GrilleP.Options + [goediting];
    GrilleP.Selection := Sel;
    if GrilleP.Cells[ACol, ARow] = '?' then
      GrilleP.Cells[ACol, ARow] := '';
    GrilleP.EditorMode := True;
  end
  else
  begin
    GrilleP.Options := GrilleP.Options + [goRowselect];
    GrilleP.Options := GrilleP.Options - [goediting];
  end;
end;

procedure TFrameCompositions.GrillePSetEditText(Sender: TObject;
  ACol, ARow: integer; const Value: string);
begin
  CalculSommePont;
end;

procedure TFrameCompositions.TreeElemComposDblClick(Sender: TObject);
var
  i: integer;
begin
  if (TreeElemCompos.Selected.ImageIndex = BibElements.ImageIndex) or
    (TreeElemCompos.Selected.ImageIndex = BibElements.ImageIndexRO) then
    for i := 1 to 7 do
      if Grille.Cells[0, i] = '' then
      begin
        AfficheElem(BibElements.Get(TreeElemCompos.Selected.Text), i);
        Exit;
      end;
end;

procedure TFrameCompositions.TreeElemComposDragOver(Sender, Source: TObject;
  X, Y: integer; State: TDragState; var Accept: boolean);
begin
  Accept := Sender = Self;
end;

procedure TFrameCompositions.TreeElemComposMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: integer);
begin
  dragage := TTreeView(Sender).GetNodeAt(X, Y);
end;

procedure TFrameCompositions.TreeElemComposMouseLeave(Sender: TObject);
begin
  inherited;
  FormMiniElem.Hide;
  FormMiniImage.Hide;
end;

procedure TFrameCompositions.TreeElemComposMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: integer);
Var
  Noeud: TTreeNode;
  comp: TComposantBibliotheque;
  Image: string;
  Test: THitTests;
begin
  Test := TTreeView(Sender).GetHitTestInfoAt(X, Y);
  if not(htOnItem in Test) then
  begin
    FormMiniElem.IComp := Nil;
    FormMiniElem.Hide;
    Exit;
  end;
  Noeud := TTreeView(Sender).GetNodeAt(X, Y);
  if Noeud <> Nil then
    comp := TComposantBibliotheque(Noeud.data)
  else
    comp := nil;

  if (comp <> Nil) and (comp <> FormMiniElem.IComp) then
  begin
    FormMiniElem.show;
    FormMiniElem.Affiche(comp);
    SetActiveWindow(FormParent.handle);
  end
  else if comp = nil then
    FormMiniElem.Hide;
  FormMiniElem.IComp := comp;

  if (Noeud <> Nil) and (comp = nil) then
  begin
    Image := GetImage1Dossier(Noeud);
    if FileExists(MondossierImages + Image) then
    begin
      if (FormMiniImage.Image <> Image) then
      begin
        FormMiniImage.Image := Image;
        AfficheImage(FormMiniImage.Image1, MondossierImages + Image, False);
        FormMiniImage.Image1.Repaint;
      end;
      if Not FormMiniImage.visible then
        FormMiniImage.show;
      SetActiveWindow(FormParent.handle);
    end
    else
      FormMiniImage.Hide;
  end
  else
    FormMiniImage.Hide;
end;

procedure TFrameCompositions.TreeMatComposDblClick(Sender: TObject);
var
  i: integer;
begin
  if BibMateriaux.EstDuType(TTreeView(Sender).Selected.ImageIndex)
    or BibElements.EstDuType(TTreeView(Sender).Selected.ImageIndex)
    or BibMCP.EstDuType(TTreeView(Sender).Selected.ImageIndex) then

    for i := 1 to 7 do
      if Grille.Cells[0, i] = '' then
      begin
        if BibMateriaux.EstDuType(TTreeView(Sender).Selected.ImageIndex) then
          AfficheMat(BibMateriaux.Get(TreeMatCompos.Selected.Text), i, -1)
        else if BibElements.EstDuType(TTreeView(Sender).Selected.ImageIndex)
          then
          AfficheElem(BibElements.Get(TreeElemCompos.Selected.Text), i)
        else if BibMCP.EstDuType(TTreeView(Sender).Selected.ImageIndex) then
          AfficheMCP(BibMCP.Get(TreeMCPCompos.Selected.Text), i);
        Exit;
      end;
  if BibPontsIntegres.EstDuType(TTreeView(Sender).Selected.ImageIndex) then
    for i := 1 to 5 do
      if GrilleP.Cells[0, i] = '' then
      begin
        AffichePont(BibPontsIntegres.Get(TTreeView(Sender).Selected.Text), i,
          -1);
        Exit;
      end;
end;

procedure TFrameCompositions.TreeMatComposDragOver(Sender, Source: TObject;
  X, Y: integer; State: TDragState; var Accept: boolean);
begin
  Accept := Sender = Self;
end;

procedure TFrameCompositions.TreeMatComposMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: integer);
begin
  dragage := TTreeView(Sender).GetNodeAt(X, Y);
end;

procedure TFrameCompositions.TreeMatComposMouseLeave(Sender: TObject);
begin
  inherited;
  FormMiniMat.Hide;
  FormMiniImage.Hide;
end;

procedure TFrameCompositions.TreeMatComposMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: integer);
Var
  Noeud: TTreeNode;
  comp: TComposantBibliotheque;
  Test: THitTests;
begin
  Test := TTreeView(Sender).GetHitTestInfoAt(X, Y);
  if not(htOnItem in Test) then
  begin
    FormMiniMat.IComp := Nil;
    FormMiniMat.Hide;
    Exit;
  end;
  Noeud := TTreeView(Sender).GetNodeAt(X, Y);
  if Noeud <> Nil then
    comp := TComposantBibliotheque(Noeud.data)
  else
    comp := nil;

  if (comp <> Nil) and (comp <> FormMiniMat.IComp) then
  begin
    FormMiniMat.Affiche(comp);
    FormMiniMat.show;
    SetActiveWindow(FormParent.handle);
  end
  else if comp = nil then
    FormMiniMat.Hide;
  FormMiniMat.IComp := comp;
end;

procedure TFrameCompositions.TreeMCPComposDblClick(Sender: TObject);
var
  i: integer;
begin
  if (TreeMCPCompos.Selected.ImageIndex = BibMCP.ImageIndex) or
    (TreeMCPCompos.Selected.ImageIndex = BibMCP.ImageIndexRO) then
    for i := 1 to 7 do
      if Grille.Cells[0, i] = '' then
      begin
        AfficheMCP(BibMCP.Get(TreeMCPCompos.Selected.Text), i);
        Exit;
      end;
end;

procedure TFrameCompositions.TreeMCPComposDragOver(Sender, Source: TObject;
  X, Y: integer; State: TDragState; var Accept: boolean);
begin
  Accept := Sender = Self;
end;

procedure TFrameCompositions.TreeMCPComposMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: integer);
begin
  dragage := TTreeView(Sender).GetNodeAt(X, Y);
end;

procedure TFrameCompositions.TreeMCPComposMouseLeave(Sender: TObject);
begin
  inherited;
  FormMiniMCP.Hide;
  FormMiniImage.Hide;
end;

procedure TFrameCompositions.TreeMCPComposMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: integer);
Var
  Noeud: TTreeNode;
  comp: TComposantBibliotheque;
  Test: THitTests;
begin
  Test := TTreeView(Sender).GetHitTestInfoAt(X, Y);
  if not(htOnItem in Test) then
  begin
    FormMiniMCP.IComp := Nil;
    FormMiniMCP.Hide;
    Exit;
  end;
  Noeud := TTreeView(Sender).GetNodeAt(X, Y);
  if Noeud <> Nil then
    comp := TComposantBibliotheque(Noeud.data)
  else
    comp := nil;

  if (comp <> Nil) and (comp <> FormMiniMCP.IComp) then
  begin
    FormMiniMCP.Affiche(comp);
    FormMiniMCP.show;
    SetActiveWindow(FormParent.handle);
  end
  else if comp = nil then
    FormMiniMCP.Hide;
  FormMiniMCP.IComp := comp;
end;

procedure TFrameCompositions.TreePontComposMouseLeave(Sender: TObject);
begin
  inherited;
  FormMiniPontIntegre.Hide;
  FormMiniImage.Hide;
end;

procedure TFrameCompositions.TreePontComposMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: integer);
Var
  Noeud: TTreeNode;
  comp: TComposantBibliotheque;
  Image: string;
  Test: THitTests;
begin
  Test := TTreeView(Sender).GetHitTestInfoAt(X, Y);
  if not(htOnItem in Test) then
  begin
    FormMiniPontIntegre.IComp := Nil;
    FormMiniPontIntegre.Hide;
    Exit;
  end;
  Noeud := TTreeView(Sender).GetNodeAt(X, Y);
  if Noeud <> Nil then
    comp := TComposantBibliotheque(Noeud.data)
  else
    comp := nil;

  if (comp <> Nil) and (comp <> FormMiniPontIntegre.IComp) then
  begin
    FormMiniPontIntegre.show;
    FormMiniPontIntegre.Affiche(comp);
    SetActiveWindow(FormParent.handle);
  end
  else if comp = nil then
    FormMiniPontIntegre.Hide;
  FormMiniPontIntegre.IComp := comp;

  if (Noeud <> Nil) and (comp = nil) then
  begin
    Image := GetImage1Dossier(Noeud);
    if FileExists(MondossierImages + Image) then
    begin
      if (FormMiniImage.Image <> Image) then
      begin
        FormMiniImage.Image := Image;
        AfficheImage(FormMiniImage.Image1, MondossierImages + Image, False);
        FormMiniImage.Image1.Repaint;
      end;
      if Not FormMiniImage.visible then
        FormMiniImage.show;
      SetActiveWindow(FormParent.handle);
    end
    else
      FormMiniImage.Hide;
  end
  else
    FormMiniImage.Hide;
end;

procedure TFrameCompositions.TreeToutFiltreChange(Sender: TObject;
  Node: TTreeNode);
var
  i: integer;
begin
  inherited;
  if TreeToutFiltre.Selected = nil then
  begin
    TreeMatCompos.Selected := nil;
    TreeElemCompos.Selected := nil;
    TreeMCPCompos.Selected := nil;
  end
  else
  begin
    for i := 0 to TreeMatCompos.Items.Count - 1 do
      if TreeMatCompos.Items.Item[i].Text = TreeToutFiltre.Selected.Text then
      begin
        TreeMatCompos.Selected := TreeMatCompos.Items.Item[i];
        CategoryPanelMat.Expand;
        Exit;
      end;
    for i := 0 to TreeElemCompos.Items.Count - 1 do
      if TreeElemCompos.Items.Item[i].Text = TreeToutFiltre.Selected.Text then
      begin
        TreeElemCompos.Selected := TreeElemCompos.Items.Item[i];
        CategoryPanelElem.Expand;
        Exit;
      end;
    for i := 0 to TreeMCPCompos.Items.Count - 1 do
      if TreeMCPCompos.Items.Item[i].Text = TreeToutFiltre.Selected.Text then
      begin
        TreeMCPCompos.Selected := TreeMCPCompos.Items.Item[i];
        CategoryPanelMCP.Expand;
        Exit;
      end;
  end;
end;

procedure TFrameCompositions.TreeToutFiltreMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: integer);
begin
  dragage := TTreeView(Sender).GetNodeAt(X, Y);
end;

procedure TFrameCompositions.TreeToutFiltreMouseLeave(Sender: TObject);
begin
  inherited;
  FormMiniMat.Hide;
  FormMiniElem.Hide;
  FormMiniMCP.Hide;
  FormMiniImage.Hide;
end;

procedure TFrameCompositions.TreeToutFiltreMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: integer);
Var
  Noeud: TTreeNode;
  comp: TComposantBibliotheque;
  Test: THitTests;
begin
  Test := TTreeView(Sender).GetHitTestInfoAt(X, Y);
  if not(htOnItem in Test) then
    comp := nil
  else
  begin
    Noeud := TTreeView(Sender).GetNodeAt(X, Y);
    if Noeud <> Nil then
      comp := TComposantBibliotheque(Noeud.data)
    else
      comp := nil;
  end;

  if (comp <> Nil) and (comp <> FormMiniMat.IComp) then
  begin
    if comp is TMateriau then
    begin
      FormMiniMat.Affiche(comp);
      FormMiniMat.show;
      FormMiniMCP.Hide;
      FormMiniElem.Hide;
      FormMiniPontIntegre.Hide;
    end
    else if comp is TElement then
    begin
      FormMiniElem.Affiche(comp);
      FormMiniElem.show;
      FormMiniMat.Hide;
      FormMiniMCP.Hide;
      FormMiniPontIntegre.Hide;
    end
    else if comp is TMCP then
    begin
      FormMiniMCP.Affiche(comp);
      FormMiniMCP.show;
      FormMiniMat.Hide;
      FormMiniElem.Hide;
      FormMiniPontIntegre.Hide;
    end
    else if comp is TPontIntegre then
    begin
      FormMiniPontIntegre.Affiche(comp);
      FormMiniPontIntegre.show;
      FormMiniMCP.Hide;
      FormMiniMat.Hide;
      FormMiniElem.Hide;
    end
  end
  else if comp = nil then
  begin
    FormMiniMat.Hide;
    FormMiniMCP.Hide;
    FormMiniElem.Hide;
    FormMiniPontIntegre.Hide;
  end;
  FormMiniImage.Hide;
  FormMiniMat.IComp := comp;
end;

procedure TFrameCompositions.CalculSommePont;
var
  i: integer;
  Reel, Somme: double;
  Erreur: integer;
  Pont: TPontIntegre;
  entraxe: double;
begin
  Somme := 0;
  for i := 1 to NbPontParCompoMax do
  begin
    Pont := BibPontsIntegres.Get(GrilleP.Cells[0, i]);
    if (Pont <> nil) then
    begin
      if Pont.TypePontIntegre = TPI_Lineique then
      begin
        Val(ConvertPoint(GrilleP.Cells[2, i]), entraxe, Erreur);
        if entraxe <> 0 then
          GrilleP.Cells[6, i] := FormatFloat('0.00',
            Pont.GJeux.Courant.psi * (1 / entraxe), tfs)
        else
          GrilleP.Cells[6, i] := ''
      end
      else
      begin
        Val(ConvertPoint(GrilleP.Cells[4, i]), entraxe, Erreur);
        if entraxe <> 0 then
          GrilleP.Cells[6, i] := FormatFloat('0.00',
            Pont.GJeux.Courant.xi * entraxe, tfs)
        else
          GrilleP.Cells[6, i] := ''
      end
    end;
    Val(ConvertPoint(GrilleP.Cells[6, i]), Reel, Erreur);
    Somme := Somme + Reel;
  end;
  GrilleP.Cells[6, 6] := FormatFloat('0.00', Somme, tfs);
  ComboCategorieChange(Self);
end;

procedure TFrameCompositions.CalculSommeGrille;
var
  i: integer;
  Reel, Somme1, Somme2, Somme3: double;
  Erreur: integer;
  Chaine: string;
begin
  Somme1 := 0;
  Somme2 := 0;
  Somme3 := 0;
  for i := 1 to 7 do
  begin
    Val(ConvertPoint(Grille.Cells[2, i]), Reel, Erreur);
    Somme1 := Somme1 + Reel;
    Val(ConvertPoint(Grille.Cells[3, i]), Reel, Erreur);
    Somme2 := Somme2 + Reel;
    Val(ConvertPoint(Grille.Cells[5, i]), Reel, Erreur);
    Somme3 := Somme3 + Reel;
  end;
  Str(Somme1: 0: 1, Chaine);
  Grille.Cells[2, 8] := Chaine;
  Str(Somme2: 0: 0, Chaine);
  Grille.Cells[3, 8] := Chaine;
  Str(Somme3: 0: 2, Chaine);
  Grille.Cells[5, 8] := Chaine;
  ComboCategorieChange(Self);
  Store;
end;

procedure TFrameCompositions.GestionCollapse(Sender: TObject);
var
  i: integer;
begin
  if AutoCollapse then
  begin
    for i := 0 to CategoryPanelGroupMatElem.Panels.Count - 1 do
      if CategoryPanelGroupMatElem.Panels[i] <> Sender then
        TCategoryPanel(CategoryPanelGroupMatElem.Panels[i]).collapsed := True;
    CategoryPanelGroupMatElem.VertScrollBar.Position := 0;
    CategoryPanelGroupMatElem.Invalidate;
  end;
end;

procedure TFrameCompositions.CategoryPanelElemExpand(Sender: TObject);
begin
  GestionCollapse(Sender);
  // TreeElemCompos.SetFocus;
end;

procedure TFrameCompositions.CategoryPanelMatExpand(Sender: TObject);
begin
  inherited;
  GestionCollapse(Sender);
  // TreeMatCompos.SetFocus;
end;

procedure TFrameCompositions.CategoryPanelMCPExpand(Sender: TObject);
begin
  inherited;
  GestionCollapse(Sender);
  // TreeMCPCompos.SetFocus;
end;

procedure TFrameCompositions.CategoryPanelPontExpand(Sender: TObject);
begin
  GestionCollapse(Sender);
  // TreePontCompos.SetFocus;
end;

procedure TFrameCompositions.DeleteGrilleClick(Sender: TObject);
var
  i: integer;
begin
  if (Grille.Selection.Top > 0) and (Grille.Selection.Top <= 7) then
    for i := 0 to Grille.ColCount - 1 do
      Grille.Cells[i, Grille.Selection.Top] := '';
  CalculSommeGrille;
end;

procedure TFrameCompositions.DeleteGrillePClick(Sender: TObject);
var
  i: integer;
begin
  if (GrilleP.Selection.Top > 0) and (GrilleP.Selection.Top <= 6) then
    for i := 0 to GrilleP.ColCount - 1 do
      GrilleP.Cells[i, GrilleP.Selection.Top] := '';
  CalculSommePont;
end;

procedure TFrameCompositions.EditFiltreSimpleChange(Sender: TObject);
begin
  Filtrer(TreeMatCompos, TreeToutFiltre, EditFiltreSimple.Text, True);
  While ThreadFiltre <> Nil do
    Application.ProcessMessages;
  Filtrer(TreeElemCompos, TreeToutFiltre, EditFiltreSimple.Text, False);
  While ThreadFiltre <> Nil do
    Application.ProcessMessages;
  Filtrer(TreeMCPCompos, TreeToutFiltre, EditFiltreSimple.Text, False);
  While ThreadFiltre <> Nil do
    Application.ProcessMessages;
  Filtrer(TreePontCompos, TreeToutFiltre, EditFiltreSimple.Text, False);
  While ThreadFiltre <> Nil do
    Application.ProcessMessages;
end;

procedure TFrameCompositions.EditFiltreSimpleRightButtonClick(Sender: TObject);
begin
  inherited;
  EditFiltreSimple.Text := '';
end;

procedure TFrameCompositions.CutGrilleClick(Sender: TObject);
begin
  CopyGrille.Click;
  DeleteGrille.Click;
  CalculSommeGrille;
end;

procedure TFrameCompositions.CutGrillePClick(Sender: TObject);
begin
  CopyGrilleP.Click;
  DeleteGrilleP.Click;
  CalculSommePont;
end;

procedure TFrameCompositions.CopyGrilleClick(Sender: TObject);
begin
  if (Grille.Selection.Top > 0) and (Grille.Selection.Top <= 7) then
    PressePapier.Assign(Grille.Rows[Grille.Selection.Top]);
end;

procedure TFrameCompositions.CopyGrillePClick(Sender: TObject);
begin
  if (GrilleP.Selection.Top > 0) and (GrilleP.Selection.Top <= 6) then
    PressePapier.Assign(GrilleP.Rows[GrilleP.Selection.Top]);
end;

function TFrameCompositions.FaireLeTrou(Grille: TStringGrid;
  Position: integer): integer;
var
  i: integer;
begin
  if Grille.Cells[0, Position] <> '' then
  begin
    if Grille.Cells[0, 7] = '' then
    begin
      for i := 7 downto Position + 1 do
        Grille.Rows[i] := Grille.Rows[i - 1];
      Result := 2;
    end
    else
      Result := 0;
  end
  else
    Result := 1;
end;

function TFrameCompositions.FaireLeTrouP(Grille: TStringGrid;
  Position: integer): integer;
var
  i: integer;
begin
  if (Grille.Cells[0, Position] <> '') then
  begin
    if Grille.Cells[0, 5] = '' then
    begin
      for i := 5 downto Position + 1 do
        Grille.Rows[i] := Grille.Rows[i - 1];
      Result := 2;
    end
    else
      Result := 0;
  end
  else if Position < 6 then
    Result := 1
  else
    Result := 0;
end;

procedure TFrameCompositions.FrameResize(Sender: TObject);
var
  i: integer;
  PrecedentOuvert: TCategoryPanel;
begin
  inherited;
  with Grille do
  begin
    FixedColor := clbtnface;
    DefaultColWidth := ClientWidth div 15;
    ColWidths[5] := DefaultColWidth * 2;
    ColWidths[4] := DefaultColWidth * 2;
    ColWidths[3] := DefaultColWidth * 2;
    ColWidths[2] := DefaultColWidth * 2;
    ColWidths[1] := DefaultColWidth;
    ColWidths[0] := ClientWidth - (DefaultColWidth + 1) * 9 + 3;
  end;
  if Sender <> Splitter1 then
    ZGroupBox15.Height := PanelListe.ClientHeight div 2;
  TreeMatCompos.Width := (ZGroupBox15.ClientWidth - 9) div 2;

  AutoCollapse := False;
  CategoryPanelGroupMatElem.DisableAlign;
  PrecedentOuvert := nil;
  for i := 0 to CategoryPanelGroupMatElem.Panels.Count - 1 do
    if not TCategoryPanel(CategoryPanelGroupMatElem.Panels[i]).collapsed then
      PrecedentOuvert := CategoryPanelGroupMatElem.Panels[i];
  CategoryPanelGroupMatElem.ExpandAll;
  for i := 0 to CategoryPanelGroupMatElem.Panels.Count - 1 do
  begin
    TCategoryPanel(CategoryPanelGroupMatElem.Panels[i]).Height :=
      CategoryPanelGroupMatElem.ClientHeight -
      (CategoryPanelGroupMatElem.HeaderHeight *
        CategoryPanelGroupMatElem.Panels.Count);
  end;
  CategoryPanelGroupMatElem.CollapseAll;
  if PrecedentOuvert <> Nil then
    PrecedentOuvert.Expand;
  CategoryPanelGroupMatElem.EnableAlign;
  AutoCollapse := True;

  FormMiniMat.Width := GroupBoxListe.Width;
  FormMiniElem.Width := GroupBoxListe.Width;
  FormMiniMCP.Width := GroupBoxListe.Width;
  FormMiniImage.Width := GroupBoxListe.Width;
  FormMiniPontIntegre.Width := GroupBoxListe.Width;
  FormMiniMat.Height := GroupBoxListe.Height;
  FormMiniElem.Height := GroupBoxListe.Height;
  FormMiniMCP.Height := GroupBoxListe.Height;
  FormMiniImage.Height := GroupBoxListe.Height;
  FormMiniPontIntegre.Height := GroupBoxListe.Height;
end;

procedure TFrameCompositions.AfficheMCP(MCP: TMCP; Ligne: integer);
begin
  if MCP = nil then
    Exit;
  Grille.Cells[0, Ligne] := MCP.Nom;
  Grille.Cells[1, Ligne] := ' ' + LettreMCP;
  Grille.Cells[2, Ligne] := FormatFloat('0.0', MCP.Epaisseur, tfs);
  Grille.Cells[3, Ligne] := FormatFloat('0',
    MCP.MasseVol * MCP.Epaisseur / 100, tfs);
  Grille.Cells[4, Ligne] := FormatFloat('0.000', MCP.Conductivite, tfs);
  if MCP.Conductivite <> 0 then
    Grille.Cells[5, Ligne] := FormatFloat('0.00',
      1 / MCP.Conductivite * MCP.Epaisseur / 100, tfs)
  else
    Grille.Cells[5, Ligne] := '';
  CalculSommeGrille;
end;

procedure TFrameCompositions.BitBtnNewClick(Sender: TObject);
var
  Ok: boolean;
  i: integer;
begin
  if Modif then
    Ok := ControleSauve(False)
  else
    Ok := True;
  if Ok then
  begin
    for i := 1 to 7 do
      Grille.Rows[i].Clear;
    for i := 1 to 5 do
      Grille.Cells[i, 8] := '';
    for i := 1 to 6 do
      GrilleP.Rows[i].Clear;
    GrillePP.Rows[0].Clear;
    Modif := False;
    inherited;
  end;
end;

procedure TFrameCompositions.AfficheElem(Elem: TElement; Ligne: integer);
begin
  if Elem = nil then
    Exit;
  Grille.Cells[0, Ligne] := Elem.Nom;
  Grille.Cells[1, Ligne] := ' ' + LettreElem;
  Grille.Cells[2, Ligne] := FormatFloat('0.0', Elem.Epaisseur, tfs);
  Grille.Cells[3, Ligne] := FormatFloat('0',
    Elem.MasseVol * Elem.Epaisseur / 100, tfs);
  Grille.Cells[4, Ligne] := FormatFloat('0.000', Elem.ConductiviteEquiv, tfs);
  Grille.Cells[5, Ligne] := FormatFloat('0.00', Elem.Resistance, tfs);
  CalculSommeGrille;
end;

procedure TFrameCompositions.AffichePont(Pont: TPontIntegre; Ligne: integer;
  entraxe: double);
var
  i: integer;
  Ok: boolean;
begin
  if Pont = nil then
    Exit;

  if Pont.TypePontIntegre = TPI_Lineique then
  begin
    Ok := True;
    if GrillePP.Cells[0, 0] <> '' then
    begin
      if MessageDlgM(Labels[854], mtConfirmation, mbOkCancel, 0) = mrOk then
        GrillePP.Rows[0].Clear
      else
        Ok := False;
    end;
    if Ok then
    begin
      GrilleP.Cells[0, Ligne] := Pont.Nom;
      GrilleP.Cells[1, Ligne] := IntToStr(integer(Pont.TypePontIntegre));
      if entraxe = -1 then
        GrilleP.Cells[2, Ligne] := '?'
      else
        GrilleP.Cells[2, Ligne] := FormatFloat('0.0', entraxe, tfs);
      GrilleP.Cells[3, Ligne] := FormatFloat('0.000', Pont.GJeux.Courant.psi,
        tfs);
      GrilleP.Cells[4, Ligne] := '';
      GrilleP.Cells[5, Ligne] := ''
    end;
  end
  else if Pont.TypePontIntegre = TPI_Ponctuel then
  begin
    Ok := True;
    if GrillePP.Cells[0, 0] <> '' then
    begin
      if MessageDlgM(Labels[854], mtConfirmation, mbOkCancel, 0) = mrOk then
        GrillePP.Rows[0].Clear
      else
        Ok := False;
    end;

    if Ok then
    begin
      GrilleP.Cells[0, Ligne] := Pont.Nom;
      GrilleP.Cells[1, Ligne] := IntToStr(integer(Pont.TypePontIntegre));
      if entraxe = -1 then
        GrilleP.Cells[4, Ligne] := '?'
      else
        GrilleP.Cells[4, Ligne] := FormatFloat('0.0', entraxe, tfs);
      GrilleP.Cells[5, Ligne] := FormatFloat('0.000', Pont.GJeux.Courant.xi,
        tfs);
      GrilleP.Cells[2, Ligne] := '';
      GrilleP.Cells[3, Ligne] := ''
    end;
  end
  else if Pont.TypePontIntegre = TPI_Global then
  begin
    Ok := True;
    if Not GrillePVide then
    begin
      if MessageDlgM(Labels[854], mtConfirmation, mbOkCancel, 0) = mrOk then
      begin
        for i := 1 to GrilleP.RowCount - 1 do
          GrilleP.Rows[i].Clear;
      end
      else
        Ok := False;
    end;
    if Ok then
    begin
      GrillePP.Cells[0, 0] := Pont.Nom;
      GrillePP.Cells[1, 0] := IntToStr(integer(Pont.TypePontIntegre));
      GrillePP.Cells[2, 0] := FormatFloat('0.#',
        Pont.GJeux.Courant.Pourcentage) + ' %';
    end;
  end;
  CalculSommePont;
end;

procedure TFrameCompositions.AfficheMat(Mat: TMateriau; Ligne: integer;
  Epaisseur: double);
begin
  if Mat = nil then
    Exit;
  Grille.Cells[0, Ligne] := Mat.Nom;
  Grille.Cells[1, Ligne] := ' ' + LettreMat;
  Grille.Cells[4, Ligne] := FormatFloat('0.000', Mat.Conductivite, tfs);
  if Epaisseur = -1 then
  begin
    Grille.Cells[2, Ligne] := '?';
    Grille.Cells[3, Ligne] := '?';
    Grille.Cells[5, Ligne] := '?';
  end
  else
  begin
    Grille.Cells[2, Ligne] := FormatFloat('0.0', Epaisseur, tfs);
    Grille.Cells[3, Ligne] := FormatFloat('0', Mat.MasseVol * Epaisseur / 100,
      tfs);
    if Mat.Conductivite <> 0 then
      Grille.Cells[5, Ligne] := FormatFloat('0.00',
        1 / Mat.Conductivite * Epaisseur / 100, tfs)
    else
      Grille.Cells[5, Ligne] := '';

  end;
  CalculSommeGrille;
end;

procedure TFrameCompositions.Affiche(comp: TComposantBibliotheque;
  Survol: boolean);
var
  i: integer;
  Mat: TMateriau;
  Elem: TElement;
  MCP: TMCP;
  Pont: TPontIntegre;
begin
  inherited Affiche(comp, Survol);
  for i := 1 to 7 do
    Grille.Rows[i].Clear;
  if comp <> Nil then
    with comp as TComposition do
    begin
      for i := 1 to NbMat do
      begin
        case TypeMat[i] of
          LettreMat:
            begin
              Mat := BibMateriaux.Get(IdentiMat[i]);
              if Mat <> nil then
                AfficheMat(Mat, i, Epaisseur[i]);
            end;
          LettreElem:
            begin
              Elem := BibElements.Get(IdentiMat[i]);
              if Elem <> nil then
                AfficheElem(Elem, i);
            end;
          LettreMCP:
            begin
              MCP := BibMCP.Get(IdentiMat[i]);
              if MCP <> nil then
                AfficheMCP(MCP, i);
            end
        end;
      end;
      CalculSommeGrille;
      for i := 1 to 6 do
        GrilleP.Rows[i].Clear;

      GrillePP.Rows[0].Clear;
      for i := 1 to NbPont do
      begin
        Pont := BibPontsIntegres.Get(IdentiPont[i]);
        if Pont <> nil then
          AffichePont(Pont, i, entraxe[i]);
      end;
      CalculSommePont;

    end;
  ModeAffichage := False;
end;

Function TFrameCompositions.GrillePVide: boolean;
var
  i: integer;
begin
  for i := 1 to GrilleP.RowCount - 1 do
    if GrilleP.Cells[0, i] <> '' then
      Exit(False);
  Result := True;
end;

end.
