unit CadreCompositionsMultiples;

interface

uses
  // Delphi
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, StdCtrls, Buttons, ComCtrls, ExtCtrls,
  //Pleiades
  Compositions, Menus;

type
  TFrameCompositionsMultiples = class(TFrame)
    Panel5:       TPanel;
    Splitter2:    TSplitter;
    GroupBox4:    TGroupBox;
    Tree:         TTreeView;
    GroupBox5:    TGroupBox;
    TreeSimple:   TTreeView;
    GroupBox3:    TGroupBox;
    Label28:      TLabel;
    Label29:      TLabel;
    Label30:      TLabel;
    Label31:      TLabel;
    Categorie:    TComboBox;
    Nom:          TEdit;
    Complement:   TEdit;
    Source:       TEdit;
    Panel4:       TPanel;
    BitBtnNouveauMulti: TBitBtn;
    BitBtnExpMulti: TBitBtn;
    BitBtnSauveMulti: TBitBtn;
    TreeTemp:     TTreeView;
    Grille:       TStringGrid;
    BitBtnCreerClasse: TBitBtn;
    PopupComponent: TPopupMenu;
    Open1:        TMenuItem;
    Delete1:      TMenuItem;
    Export1:      TMenuItem;
    PopupGrille:  TPopupMenu;
    DeleteGrille: TMenuItem;
    CutGrille:    TMenuItem;
    CopyGrille:   TMenuItem;
    PasteGrille:  TMenuItem;
    Rename1: TMenuItem;
    procedure TreeChange(Sender: TObject; Node: TTreeNode);
    procedure TreeClick(Sender: TObject);
    procedure TreeDragDrop(Sender, Source: TObject; X, Y: integer);
    procedure TreeKeyUp(Sender: TObject; var Key: word; Shift: TShiftState);
    procedure BitBtnCreerClasseClick(Sender: TObject);
    procedure CategorieChange(Sender: TObject);
    procedure GrilleDragDrop(Sender, Source: TObject; X, Y: integer);
    procedure GrilleDragOver(Sender, Source: TObject; X, Y: integer;
      State: TDragState; var Accept: boolean);
    procedure GrilleDrawCell(Sender: TObject; ACol, ARow: integer;
      Rect: TRect; State: TGridDrawState);
    procedure GrilleMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: integer);
    procedure GrilleSelectCell(Sender: TObject; ACol, ARow: integer;
      var CanSelect: boolean);
    procedure GrilleSetEditText(Sender: TObject; ACol, ARow: integer;
      const Value: string);
    procedure BitBtnNouveauMultiClick(Sender: TObject);
    procedure BitBtnSauveMultiClick(Sender: TObject);
    procedure FrameResize(Sender: TObject);
    procedure TreeMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: integer);
    procedure TreeSimpleMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: integer);
    procedure Open1Click(Sender: TObject);
    procedure Delete1Click(Sender: TObject);
    procedure GrilleMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: integer);
    procedure DeleteGrilleClick(Sender: TObject);
    procedure CutGrilleClick(Sender: TObject);
    procedure CopyGrilleClick(Sender: TObject);
    procedure PasteGrilleClick(Sender: TObject);
    procedure GrilleKeyUp(Sender: TObject; var Key: word; Shift: TShiftState);
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure Rename1Click(Sender: TObject);
    procedure PopupComponentPopup(Sender: TObject);
  private
    { Déclarations privées }
    Modif:        boolean;
    PressePapier: TStringList;
    function ControleSauve(Forcee: boolean): boolean;
    procedure CalculSommeGrille;
    function FaireLeTrou(Grille: TStringGrid; Position: integer): integer;
  protected
    Dragage:       TTreeNode;
    SauveReadOnly: boolean;
    function Controle: boolean;
  public
    { Déclarations publiques }
    ModificationAutorise: boolean;
    FormParent:        TForm;
    CheckHint:         boolean;
    MessagesInterface: TStringList;
    procedure EffaceDragage;
    procedure Affiche(Compo: TComposition);
    function AccepteDrag: boolean;
  end;

implementation

uses
  // Pleiades
  Utilitaires, BiblioCompositions, izUtilitaires, DialogsInternational, 
  ComposantBibliotheque;

{$R *.dfm}

function TFrameCompositionsMultiples.AccepteDrag: boolean;
begin
  Result := (BibCompositions.ImageIndex = dragage.ImageIndex) or
    (dragage.ImageIndex = BibCompositions.ImageIndexDossier) or
    (Dragage.ImageIndex = BibCompositions.ImageIndexDossierS) or
    (Dragage.ImageIndex > 10000);
end;


constructor TFrameCompositionsMultiples.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  SauveReadOnly := False;
  ModificationAutorise := True;
  CheckHint  := True;
  FormParent := nil;
  MessagesInterFace := TStringList.Create;
  PressePapier := TStringList.Create;
end;

destructor TFrameCompositionsMultiples.Destroy;
begin
  MessagesInterFace.Free;
  PressePapier.Free;
  inherited Destroy;
end;

procedure TFrameCompositionsMultiples.CutGrilleClick(Sender: TObject);
begin
  CopyGrille.Click;
  DeleteGrille.Click;
end;

procedure TFrameCompositionsMultiples.Delete1Click(Sender: TObject);
var
  I: integer;
  TreeSource: TCustomTreeView;
begin
  if Dragage = nil then
    Exit;
  TreeSource := Dragage.TreeView;
  for I := TreeSource.Selectioncount - 1 downto 0 do
  begin
    Dragage := TreeSource.Selections[i];
    EffaceDragage;
  end;
end;

procedure TFrameCompositionsMultiples.DeleteGrilleClick(Sender: TObject);
var
  i: integer;
begin
  if (Grille.Selection.Top > 0) and (Grille.Selection.Top <= 7) then
    for i := 0 to Grille.ColCount - 1 do
      Grille.Cells[i, Grille.Selection.top] := '';
end;

procedure TFrameCompositionsMultiples.BitBtnNouveauMultiClick(Sender: TObject);
var
  Ok: boolean;
  i:  integer;
begin
  if Modif then
    Ok := ControleSauve(False)
  else
    Ok := True;
  if Ok then
  begin
    nom.Text := '';
    complement.Text := '';
    Source.Text := '';
    Categorie.ItemIndex := -1;
    for i := 1 to 7 do
      Grille.Rows[i].Clear;
    Modif := False;
  end;
end;

procedure TFrameCompositionsMultiples.BitBtnSauveMultiClick(Sender: TObject);
begin
  ControleSauve(True);
end;

procedure TFrameCompositionsMultiples.BitBtnCreerClasseClick(Sender: TObject);
var
  Valeur: string;
begin
  if InputQueryM(MessagesInterface[6], MessagesInterface[6], valeur) and
    (Valeur <> '') then
    BibCompositions.AjouteCategorie(Valeur, Categorie.items, Tree);
end;

procedure TFrameCompositionsMultiples.CategorieChange(Sender: TObject);
begin
  Modif := True;
end;

procedure TFrameCompositionsMultiples.GrilleDragDrop(Sender, Source: TObject;
  X, Y: integer);
var
  Position, Col, lig: integer;
  Tampon: TStrings;
  Resultat: integer;
  Compo:  TComposition;
begin
  Grille.MouseToCell(X, Y, Col, Lig);
  if Lig < 8 then
  begin
    if (Dragage.ImageIndex = BibCompositions.ImageIndex) or
      (Dragage.ImageIndex = BibCompositions.ImageIndexRO) then
    begin
      Compo := BibCompositions.Get(dragage.Text);
      if not Compo.MultiComposition and (FaireLeTrou(Grille, Lig) <> 0) then
      begin
        Grille.cells[0, Lig] := Compo.Nom;
        Grille.Cells[1, Lig] := '';
        CalculSommeGrille;
      end
      else
        MessageBeep(MB_ICONEXCLAMATION);
    end
    else if (Dragage.ImageIndex >= 10001) then
    begin
      Position := dragage.imageIndex - 10000;
      Grille.MouseToCell(X, Y, Col, Lig);
      if lig <> Position then
        Resultat := FaireLeTrou(Grille, Lig)
      else
        Resultat := -1;
      if (Resultat = 1) or ((Resultat = 2) and (Position < Lig)) then
      begin
        Tampon := Grille.rows[Position];
        Grille.rows[Lig] := Tampon;
        Grille.Rows[Position].Clear;
      end
      else if (Resultat = 2) and (Position > Lig) then
      begin
        Tampon := Grille.rows[Position + 1];
        Grille.rows[Lig] := Tampon;
        Grille.Rows[Position + 1].Clear;
      end;
    end;
  end;
  Modif := True;
end;

procedure TFrameCompositionsMultiples.GrilleDragOver(Sender, Source: TObject;
  X, Y: integer; State: TDragState; var Accept: boolean);
var
  ARow, ACol: integer;
begin
  Grille.MouseTOCell(X, Y, ACol, ARow);
  if Dragage <> nil then
    Accept := ((dragage.ImageIndex = BibCompositions.ImageIndex) or
      (dragage.ImageIndex = BibCompositions.ImageIndexRO) or
      (dragage.imageIndex > 10000)) and (ARow > 0)
  else
    Accept := False;
end;

procedure TFrameCompositionsMultiples.GrilleDrawCell(Sender: TObject;
  ACol, ARow: integer; Rect: TRect; State: TGridDrawState);
var
  Recto: TRect;
begin
  if (ACol = 1) and (Grille.Cells[0, ARow] <> '') then
  begin
    Recto := Rect;
    Grille.Canvas.pen.color := clRed;
    Grille.Canvas.Brush.color := clRed;
    Grille.Canvas.Brush.style := bsclear;
    Grille.Canvas.Rectangle(recto.left, recto.top,
      recto.right, Recto.bottom);
  end;
end;

procedure TFrameCompositionsMultiples.GrilleKeyUp(Sender: TObject;
  var Key: word; Shift: TShiftState);
begin
  case Key of
    46: DeleteGrille.Click;
    88: CutGrille.Click;
    67: CopyGrille.Click;
    86: PasteGrille.Click;
  end;
end;

procedure TFrameCompositionsMultiples.GrilleMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: integer);
var
  Col, Lig: integer;
  Sel: TGridRect;
begin
  Grille.MousetoCell(X, Y, Col, Lig);
  if (Lig < 8) and (Button = mbLeft) then
  begin
    Grille.BeginDrag(False);
    dragage  := nil;
    dragage  := TreeTemp.Items.getFirstNode;
    dragage.imageIndex := Lig + 10000;
    dragage.Text := Grille.cells[0, Lig];
    Sel.Left := 1;
    Sel.Right := 1;
    Sel.Bottom := Lig;
    Sel.Top  := Lig;
    if (Col = 1) and (Lig <> 0) and (Lig <> 8) and (Grille.Cells[0, Lig] <> '') then
    begin
      Grille.Options := Grille.Options + [goediting];
      Grille.Options := Grille.Options - [goRowselect];
      Grille.Selection := Sel;
      Grille.EditorMode := True;
    end
    else
    begin
      Grille.Options := Grille.Options - [goediting];
      Grille.Options := Grille.Options + [goRowselect];
    end;
  end;
end;

procedure TFrameCompositionsMultiples.GrilleMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: integer);
var
  Acol, ARow: integer;
  Rect: TGridRect;
  p: TPoint;
begin
  if Button = mbRight then
  begin
    Grille.MouseToCell(X, Y, Acol, Arow);
    Rect.Top := Arow;
    Rect.Bottom := Arow;
    Rect.Left := 0;
    Rect.Right := Grille.ColCount - 1;
    Grille.Selection := Rect;
    p.x := X;
    p.Y := Y;
    P := Grille.ClientToScreen(P);
    PopupGrille.Popup(P.x, P.y);
  end;
end;

procedure TFrameCompositionsMultiples.GrilleSelectCell(Sender: TObject;
  ACol, ARow: integer; var CanSelect: boolean);
begin
  if ACol <> 1 then
  begin
    Grille.Options := Grille.Options - [goediting];
    Grille.Options := Grille.Options + [goRowselect];
  end;
end;

procedure TFrameCompositionsMultiples.GrilleSetEditText(Sender: TObject;
  ACol, ARow: integer; const Value: string);
begin
  CalculSommeGrille;
  Modif := True;
end;

procedure TFrameCompositionsMultiples.Open1Click(Sender: TObject);
var
  Noeud: TTreeNode;
  Compo: TComposition;
  Ok: boolean;
begin
  if Modif then
    Ok := ControleSauve(False)
  else
    Ok := True;
  if not Ok then
    Exit;
  Noeud := Tree.Selected;
  if Noeud <> nil then
  begin
    Compo := BibCompositions.Get(Noeud.Text);
    if Compo <> nil then
      Affiche(Compo);
  end;
end;

procedure TFrameCompositionsMultiples.PasteGrilleClick(Sender: TObject);
begin
  if (Grille.Selection.Top > 0) and (Grille.Selection.Top <= 7) and
    (PressePapier.Count <> 0) then
    Grille.Rows[Grille.Selection.Top].Assign(PressePapier);
end;

procedure TFrameCompositionsMultiples.PopupComponentPopup(Sender: TObject);
begin
  Rename1.Enabled :=  Tree.Selected.ImageIndex <= 1;
  Open1.Enabled := Not Rename1.Enabled;
end;

procedure TFrameCompositionsMultiples.Rename1Click(Sender: TObject);
var
  NouveauNom : String;
  Noeud: TTreeNode;
begin
  if Dragage = nil then
    Exit;
  NouveauNom := Dragage.text;
  if InputQueryM(MessagesInterface[6], MessagesInterface[6], NouveauNom) then
  Begin
    dragage.Text := NouveauNom;
    Noeud := dragage.getFirstChild;
    while Noeud <> Nil do
    Begin
      TComposantBibliotheque(Noeud.data).categorie := NouveauNom;
      Noeud := Noeud.getNextSibling;
    End;
  End;
end;

procedure TFrameCompositionsMultiples.TreeChange(Sender: TObject; Node: TTreeNode);
var
  Chaine: string;
begin
  Str(BibCompositions.Count, chaine);
  if CheckHint then
    Tree.Hint := Chaine + MessagesInterface[8]
  else
    Tree.Hint := '';
end;

procedure TFrameCompositionsMultiples.TreeClick(Sender: TObject);
var
  Noeud: TTreeNode;
  Compo: TComposition;
  Ok: boolean;
begin
  if Modif then
    Ok := ControleSauve(False)
  else
    Ok := True;
  if not Ok then
    Exit;
  Compo := nil;
  Noeud := Tree.Selected;
  if Noeud <> nil then
    Compo := BibCompositions.Get(Noeud.Text);
  if Compo <> nil then
    Affiche(Compo);
  Modif := False;
  CalculSommeGrille;
end;

procedure TFrameCompositionsMultiples.TreeDragDrop(Sender, Source: TObject;
  X, Y: integer);
var
  Courant: TTreeNode;
  Compo: TComposition;
begin
  if (Tree.dropTarget.ImageIndex = BibCompositions.ImageIndex) then
  begin
    Compo := BibCompositions.Get(Dragage.Text);
    Compo.Categorie := Tree.DropTarget.Text;
    Tree.Items.Delete(dragage);
    Courant := BibCompositions.PoseDansArbre(Compo, Tree, True, False);
    Tree.alphaSort;
    Tree.FullCollapse;
    Tree.Selected := Courant;
  end;
end;

procedure TFrameCompositionsMultiples.TreeKeyUp(Sender: TObject;
  var Key: word; Shift: TShiftState);
begin
  if (Key = 46) and (Tree.Selected <> nil) then
    EffaceDragage
  else
    Tree.OnClick(Self);
end;

procedure TFrameCompositionsMultiples.TreeMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: integer);
begin
  Dragage := TTreeView(Sender).GetNodeAt(X, Y);
end;

procedure TFrameCompositionsMultiples.TreeSimpleMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: integer);
begin
  Dragage := TTreeView(Sender).GetNodeAt(X, Y);
end;

function TFrameCompositionsMultiples.ControleSauve(Forcee: boolean): boolean;
var
  reponse: word;
  Silencieux: boolean;
  comp:  TComposition;
  Noeud: TTreeNode;
begin
  if not ModificationAutorise then
  begin
    MessageDlgM(MessagesInterface[7], mtWarning, [mbOK], 0);
    Result := False;
    Exit;
  end;
  Result := False;
  FormParent.ActiveControl := Tree;
  if not Forcee then
    Reponse := MessageDlgM(MessagesInterface[5] + Nom.Text + '''',
      mtWarning, [mbCancel, mbNo, mbYes], 0)
  else
    Reponse := mrYes;
  if Reponse = mrCancel then
    Exit;
  Result := True;
  if (Reponse = mrYes) then
    if Controle then
    begin
      comp := TComposition.Create(Nom.Text, Categorie.Text,
        Complement.Text, Source.Text, Grille, True);
      comp.ReadOnly := CtrlDown and ShiftDown;
      Silencieux := False;
      if BibCompositions.Ajoute(comp, Silencieux) then
      begin
        Noeud := BibCompositions.PoseDansArbre(comp, Tree, True, True);
        Tree.Selected := Noeud;
      end
      else
        comp.Free;
    end
    else
      Result := False;
  Modif := False;
  BibCompositions.SauveListe;
end;

procedure TFrameCompositionsMultiples.CopyGrilleClick(Sender: TObject);
begin
  if (Grille.Selection.top > 0) and (Grille.Selection.Top <= 7) then
    PressePapier.Assign(Grille.Rows[Grille.Selection.top]);
end;

procedure TFrameCompositionsMultiples.CalculSommeGrille;
var
  i: integer;
  Reel, Somme1: double;
  Erreur: integer;
  Chaine: string;
begin
  Somme1 := 0;
  for i := 1 to 7 do
  begin
    Val(ConvertPoint(Grille.Cells[1, i]), reel, erreur);
    Somme1 := Somme1 + Reel;
  end;
  Str(Somme1: 0: 1, Chaine);
  Grille.Cells[1, 8] := Chaine;
end;

function TFrameCompositionsMultiples.Controle: boolean;
var
  I: integer;
  Rectan: TgridRect;
  Erreur: integer;
  CanSelect: boolean;
  Compteur: integer;
begin
  if EstNumerique(Nom.Text) then
    Nom.Text := '_' + Nom.Text;
  Erreur := 0;
  if Categorie.Text = '' then
  begin
    FormParent.FocusControl(Categorie);
    Erreur := 1;
  end
  else if ControlNum(nom.Text, 4, 256, 'C') <> nom.Text then
  begin
    FormParent.FocusControl(nom);
    Erreur := 2;
  end
  else if ControlNum(Complement.Text, 0, 256, 'C') <> Complement.Text then
  begin
    FormParent.FocusControl(Complement);
    Erreur := 3;
  end;
  Compteur := 0;
  for i := 1 to NbMatParCompo do
  begin
    if (Grille.Cells[0, i] <> '') then
    begin
      Inc(Compteur);
      if ControlNum(ConvertPoint(Grille.Cells[1, i]), 0.01, 100, 'R') <>
        ConvertPoint(Grille.Cells[1, i]) then
      begin
        FormParent.FocusControl(Grille);
        Erreur := 4;
        Rectan.Top := i;
        Rectan.Bottom := i;
        rectan.Left := 1;
        Rectan.Right := 1;
        Grille.Selection := rectan;
        CanSelect := True;
        GrilleSelectCell(Self, rectan.Left, Rectan.Top, CanSelect);
      end;
    end;
  end;
  if ControlNum(Grille.Cells[1, 8], 100, 100, 'R') <> Grille.Cells[1, 8] then
  begin
    FormParent.FocusControl(Grille);
    Erreur := 4;
  end;
  if Compteur = 0 then
  begin
    FormParent.FocusControl(Grille);
    Erreur := 5;
  end;
  if Erreur <> 0 then
  begin
    MessageDlgM(MessagesInterface[8 + Erreur], mtWarning, [mbOK], 0);
    Result := False;
  end
  else
    Result := True;
end;

procedure TFrameCompositionsMultiples.EffaceDragage;
var
  i, index: integer;
begin
  //Cas des catégories
  if (Dragage.ImageIndex = BibCompositions.ImageIndexDossier) or
    (Dragage.ImageIndex = BibCompositions.ImageIndexDossierS) then
  begin
    if dragage.HasChildren then
      MessageDlgM(MessagesInterface[0], mtWarning, [mbOK], 0)
    else
    begin
      Categorie.items.Delete(Categorie.Items.IndexOf(dragage.Text));
      Tree.items.Delete(dragage);
    end;
  end
  else if (Dragage.ImageIndex = BibCompositions.ImageIndex) then
  begin
    if (MessageDlgM(MessagesInterface[1] + dragage.Text + MessagesInterface[2],
      mtConfirmation, mbOkCancel, 0) = mrOk) and
      BibCompositions.Efface(dragage.Text) then
      Tree.Items.Delete(dragage);
  end
  else if (Dragage.ImageIndex >= 10001) then
  begin
    index := dragage.imageIndex - 10000;
    for i := Index + 1 to NbMatParCompo do
      Grille.Rows[i - 1] := Grille.Rows[i];
    Grille.Rows[7].Clear;
    CalculSommeGrille;
  end;
end;

procedure TFrameCompositionsMultiples.FrameResize(Sender: TObject);
begin
  with Grille do
  begin
    FixedColor := clbtnface;
    DefaultColWidth := ClientWidth div 12;
    ColWidths[1] := DefaultColWidth * 2;
    ColWidths[0] := ClientWidth - (DefaultColWidth + 1) * 2;
  end;
  GroupBox5.Height := panel5.ClientHeight div 2;
end;

procedure TFrameCompositionsMultiples.Affiche(Compo: TComposition);
var
  i: integer;
  comp: TComposition;
  chaine: string;
  indice: integer;
begin
  Nom.Text := Compo.Nom;
  Complement.Text := Compo.Complement;
  Source.Text := Compo.Source;
  BibCompositions.ConvertitCategorie(Compo);
  indice := Categorie.Items.IndexOf(Compo.Categorie);
  if Indice = -1 then
  begin
    BibCompositions.AjouteCategorie(Compo.Categorie, Categorie.items, Tree);
    indice := Categorie.Items.IndexOf(Compo.Categorie);
  end;
  Categorie.ItemIndex := Indice;

  for i := 1 to 7 do
    Grille.Rows[i].Clear;
  for i := 1 to Compo.NbMat do
  begin
    comp := BibCompositions.Get(Compo.IdentiMat[i]);
    if comp <> nil then
    begin
      Grille.Cells[0, i] := comp.Nom;
      Str(Compo.Epaisseur[i]: 0: 1, chaine);
      Grille.Cells[1, i] := chaine;
    end;
  end;
  CalculSommeGrille;
end;

function TFrameCompositionsMultiples.FaireLeTrou(Grille: TStringGrid; Position: integer): integer;
var
  I: integer;
begin
  if Grille.Cells[0, Position] <> '' then
  begin
    if Grille.Cells[0, 7] = '' then
    begin
      for i := 7 downto position + 1 do
        grille.Rows[i] := Grille.Rows[i - 1];
      Result := 2;
    end
    else
      Result := 0;
  end
  else
    Result := 1;
end;

end.
