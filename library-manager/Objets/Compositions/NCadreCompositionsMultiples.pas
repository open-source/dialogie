unit NCadreCompositionsMultiples;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, CadreBib, Menus, ExtCtrls, ComCtrls, StdCtrls, Buttons, BarreAdresse,
  Grids, ComposantBibliotheque, Compositions;

type
  TFrameCompositionsMultiples = class(TFrameBib)
    Grille: TStringGrid;
    TreeTemp: TTreeView;
    GroupBox5: TGroupBox;
    TreeSimple: TTreeView;
    Splitter2: TSplitter;
    PopupGrille: TPopupMenu;
    DeleteGrille: TMenuItem;
    CutGrille: TMenuItem;
    CopyGrille: TMenuItem;
    PasteGrille: TMenuItem;
    TreeSimpleFiltre: TTreeView;
    PanelFiltreSimple: TPanel;
    Label2: TLabel;
    EditFiltreSimple: TButtonedEdit;
    procedure TreeSimpleMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: integer);
    procedure GrilleDragDrop(Sender, Source: TObject; X, Y: integer);
    procedure GrilleDragOver(Sender, Source: TObject; X, Y: integer;
      State: TDragState; var Accept: boolean);
    procedure GrilleDrawCell(Sender: TObject; ACol, ARow: integer; Rect: TRect;
      State: TGridDrawState);
    procedure GrilleKeyUp(Sender: TObject; var Key: word; Shift: TShiftState);
    procedure GrilleMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: integer);
    procedure GrilleMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: integer);
    procedure GrilleSelectCell(Sender: TObject; ACol, ARow: integer;
      var CanSelect: boolean);
    procedure GrilleSetEditText(Sender: TObject; ACol, ARow: integer;
      const Value: string);
    procedure DeleteGrilleClick(Sender: TObject);
    procedure CutGrilleClick(Sender: TObject);
    procedure CopyGrilleClick(Sender: TObject);
    procedure PasteGrilleClick(Sender: TObject);
    procedure BitBtnNewClick(Sender: TObject);
    procedure TreeSimpleFiltreChange(Sender: TObject; Node: TTreeNode);
    procedure EditFiltreSimpleChange(Sender: TObject);
    procedure EditFiltreSimpleRightButtonClick(Sender: TObject);
    procedure TreeSimpleFiltreMouseMove(Sender: TObject; Shift: TShiftState;
      X, Y: integer);
    procedure TreeSimpleMouseMove(Sender: TObject; Shift: TShiftState;
      X, Y: integer);
    procedure TreeSimpleDblClick(Sender: TObject);
    procedure TreeSimpleFiltreDblClick(Sender: TObject);
    procedure TreeSimpleFiltreMouseLeave(Sender: TObject);
    procedure TreeSimpleMouseLeave(Sender: TObject);
    procedure FrameResize(Sender: TObject);
    procedure Splitter2Moved(Sender: TObject);
  private
    { Déclarations privées }
    PressePapier: TStringList;
    procedure CalculSommeGrille;
    function FaireLeTrou(Grille: TStringGrid; Position: integer): integer;
  protected
    function Controle: boolean; override;
    procedure AfficheCompo(Compo: TComposition; Ligne: integer);
  public
    { Déclarations publiques }
    function CreerComposantBiblio: TComposantBibliotheque; override;
    constructor Create(Aowner: TComponent); override;
    destructor Destroy; override;
    function AccepteDragGrille: boolean;
    procedure EffaceDragageGrille;

    procedure Affiche(comp: TComposantBibliotheque; Survol: boolean); override;
  end;

var
  FrameCompositionsMultiples: TFrameCompositionsMultiples;

implementation

uses
  BiblioCompositions, Utilitaires, DialogsInternational, MiniCompo;
{$R *.dfm}

procedure TFrameCompositionsMultiples.EffaceDragageGrille;
var
  i: integer;
  index: integer;
begin
  index := dragage.imageIndex - 10000;
  for i := Index + 1 to NbMatParCompo do
    Grille.Rows[i - 1] := Grille.Rows[i];
  Grille.Rows[7].Clear;
  CalculSommeGrille;
end;

function TFrameCompositionsMultiples.AccepteDragGrille: boolean;
begin
  Result := dragage.imageIndex <= 10000 + NbMatParCompo;
end;

destructor TFrameCompositionsMultiples.Destroy;
begin
  PressePapier.Free;
  inherited Destroy;
end;

procedure TFrameCompositionsMultiples.EditFiltreSimpleChange(Sender: TObject);
begin
  Filtrer(TreeSimple, TreeSimpleFiltre, EditFiltreSimple.Text, True);
end;

procedure TFrameCompositionsMultiples.EditFiltreSimpleRightButtonClick
  (Sender: TObject);
begin
  inherited;
  EditFiltreSimple.Text := '';
end;

constructor TFrameCompositionsMultiples.Create(Aowner: TComponent);
begin
  inherited Create(Aowner);
  PressePapier := TStringList.Create;
end;

procedure TFrameCompositionsMultiples.AfficheCompo(Compo: TComposition;
  Ligne: integer);
begin
  Grille.cells[0, Ligne] := Compo.Nom;
  Grille.cells[1, Ligne] := '';
  CalculSommeGrille;
end;

procedure TFrameCompositionsMultiples.GrilleDragDrop(Sender, Source: TObject;
  X, Y: integer);
var
  Position, Col, lig: integer;
  Tampon: TStrings;
  Resultat: integer;
  Compo: TComposition;
begin
  Grille.MouseToCell(X, Y, Col, lig);
  if lig < 8 then
  begin
    if (dragage.imageIndex = BibCompositions.imageIndex) or
      (dragage.imageIndex = BibCompositions.ImageIndexRO) then
    begin
      Compo := BibCompositions.Get(dragage.Text);
      if not Compo.MultiComposition and (FaireLeTrou(Grille, lig) <> 0) then
        AfficheCompo(Compo, lig)
      else
        MessageBeep(MB_ICONEXCLAMATION);
    end
    else if (dragage.imageIndex >= 10001) then
    begin
      Position := dragage.imageIndex - 10000;
      Grille.MouseToCell(X, Y, Col, lig);
      if lig <> Position then
        Resultat := FaireLeTrou(Grille, lig)
      else
        Resultat := -1;
      if (Resultat = 1) or ((Resultat = 2) and (Position < lig)) then
      begin
        Tampon := Grille.Rows[Position];
        Grille.Rows[lig] := Tampon;
        Grille.Rows[Position].Clear;
      end
      else if (Resultat = 2) and (Position > lig) then
      begin
        Tampon := Grille.Rows[Position + 1];
        Grille.Rows[lig] := Tampon;
        Grille.Rows[Position + 1].Clear;
      end;
    end;
  end;
  CalculSommeGrille;
end;

procedure TFrameCompositionsMultiples.GrilleDragOver(Sender, Source: TObject;
  X, Y: integer; State: TDragState; var Accept: boolean);
var
  ARow, ACol: integer;
begin
  Grille.MouseToCell(X, Y, ACol, ARow);
  if dragage <> nil then
    Accept := ((dragage.imageIndex = BibCompositions.imageIndex) or
        (dragage.imageIndex = BibCompositions.ImageIndexRO) or
        (dragage.imageIndex > 10000)) and (ARow > 0)
  else
    Accept := False;
end;

procedure TFrameCompositionsMultiples.GrilleDrawCell(Sender: TObject;
  ACol, ARow: integer; Rect: TRect; State: TGridDrawState);
var
  Recto: TRect;
begin
  if (ACol = 1) and (ARow <> 0) and (Grille.cells[0, ARow] <> '') then
  begin
    Recto := Rect;
    Grille.Canvas.pen.color := clRed;
    Grille.Canvas.Brush.color := clRed;
    Grille.Canvas.Brush.style := bsclear;
    Grille.Canvas.Rectangle(Recto.left, Recto.top, Recto.right, Recto.bottom);
  end;
end;

procedure TFrameCompositionsMultiples.GrilleKeyUp(Sender: TObject;
  var Key: word; Shift: TShiftState);
begin
  case Key of
    46:
      DeleteGrille.Click;
    88:
      CutGrille.Click;
    67:
      CopyGrille.Click;
    86:
      PasteGrille.Click;
  end;
  CalculSommeGrille;
end;

procedure TFrameCompositionsMultiples.GrilleMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: integer);
var
  Col, lig: integer;
  Sel: TGridRect;
begin
  Grille.MouseToCell(X, Y, Col, lig);
  if (lig < 8) and (Button = mbLeft) then
  begin
    Grille.BeginDrag(False);
    dragage := nil;
    dragage := TreeTemp.Items.getFirstNode;
    dragage.imageIndex := lig + 10000;
    dragage.Text := Grille.cells[0, lig];
    Sel.left := 1;
    Sel.right := 1;
    Sel.bottom := lig;
    Sel.top := lig;
    if (Col = 1) and (lig <> 0) and (lig <> 8) and (Grille.cells[0, lig] <> '')
      then
    begin
      Grille.Options := Grille.Options + [goediting];
      Grille.Options := Grille.Options - [goRowselect];
      Grille.Selection := Sel;
      Grille.EditorMode := True;
    end
    else
    begin
      Grille.Options := Grille.Options - [goediting];
      Grille.Options := Grille.Options + [goRowselect];
    end;
  end;
end;

procedure TFrameCompositionsMultiples.GrilleMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: integer);
var
  ACol, ARow: integer;
  Rect: TGridRect;
  p: TPoint;
begin
  if Button = mbRight then
  begin
    Grille.MouseToCell(X, Y, ACol, ARow);
    Rect.top := ARow;
    Rect.bottom := ARow;
    Rect.left := 0;
    Rect.right := Grille.ColCount - 1;
    Grille.Selection := Rect;
    p.X := X;
    p.Y := Y;
    p := Grille.ClientToScreen(p);
    PopupGrille.Popup(p.X, p.Y);
  end;
end;

procedure TFrameCompositionsMultiples.GrilleSelectCell(Sender: TObject;
  ACol, ARow: integer; var CanSelect: boolean);
begin
  if ACol <> 1 then
  begin
    Grille.Options := Grille.Options - [goediting];
    Grille.Options := Grille.Options + [goRowselect];
  end;
end;

procedure TFrameCompositionsMultiples.GrilleSetEditText(Sender: TObject;
  ACol, ARow: integer; const Value: string);
begin
  CalculSommeGrille;
end;

procedure TFrameCompositionsMultiples.PasteGrilleClick(Sender: TObject);
begin
  if (Grille.Selection.top > 0) and (Grille.Selection.top <= 7) and
    (PressePapier.Count <> 0) then
    Grille.Rows[Grille.Selection.top].Assign(PressePapier);
  CalculSommeGrille;
end;

procedure TFrameCompositionsMultiples.Splitter2Moved(Sender: TObject);
begin
  inherited;
  OnResize(self);
end;

procedure TFrameCompositionsMultiples.TreeSimpleDblClick(Sender: TObject);
var
  i: integer;
begin
  if Bibliotheque.EstDuType(TTreeView(Sender).Selected.imageIndex) then
    for i := 1 to 7 do
      if Grille.cells[0, i] = '' then
      begin
        if Bibliotheque.EstDuType(TTreeView(Sender).Selected.imageIndex) then
          AfficheCompo(BibCompositions.Get(TreeSimple.Selected.Text), i);
        Exit;
      end;
end;

procedure TFrameCompositionsMultiples.TreeSimpleFiltreChange(Sender: TObject;
  Node: TTreeNode);
var
  i: integer;
begin
  inherited;
  if TreeSimpleFiltre.Selected = nil then
    TreeSimple.Selected := nil
  else
    for i := 0 to TreeSimple.Items.Count - 1 do
      if TreeSimple.Items.Item[i].Text = TreeSimpleFiltre.Selected.Text then
      begin
        TreeSimple.Selected := TreeSimple.Items.Item[i];
        Exit;
      end;
end;

procedure TFrameCompositionsMultiples.TreeSimpleFiltreDblClick(Sender: TObject);
var
  i: integer;
begin
  if Bibliotheque.EstDuType(TTreeView(Sender).Selected.imageIndex) then
    for i := 1 to 7 do
      if Grille.cells[0, i] = '' then
      begin
        if Bibliotheque.EstDuType(TTreeView(Sender).Selected.imageIndex) then
          AfficheCompo(BibCompositions.Get(TTreeView(Sender).Selected.Text), i);
        Exit;
      end;
end;

procedure TFrameCompositionsMultiples.TreeSimpleFiltreMouseLeave(
  Sender: TObject);
begin
  inherited;
  FormMiniCompo.Hide;
end;

procedure TFrameCompositionsMultiples.TreeSimpleFiltreMouseMove
  (Sender: TObject; Shift: TShiftState; X, Y: integer);
Var
  Noeud: TTreeNode;
  comp: TComposantBibliotheque;
  Test: THitTests;
begin
  Test := TTreeView(Sender).GetHitTestInfoAt(X, Y);
  if not(htOnItem in Test) then
  begin
    FormMiniCompo.IComp := Nil;
    FormMiniCompo.Hide;
    Exit;
  end;
  Noeud := TTreeView(Sender).GetNodeAt(X, Y);
  if Noeud <> Nil then
    comp := TComposantBibliotheque(Noeud.data)
  else
    comp := nil;

  if (comp <> Nil) and (comp <> FormMiniCompo.IComp) then
  begin
    FormMiniCompo.Affiche(comp);
    FormMiniCompo.Show;
    SetActiveWindow(FormParent.handle);
 end
  else if comp = nil then
    FormMiniCompo.Hide;
  FormMiniCompo.IComp := comp;
end;

procedure TFrameCompositionsMultiples.TreeSimpleMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: integer);
begin
  inherited;
  dragage := TTreeView(Sender).GetNodeAt(X, Y);
end;

procedure TFrameCompositionsMultiples.TreeSimpleMouseLeave(Sender: TObject);
begin
  inherited;
  FormMiniCompo.Hide;
end;

procedure TFrameCompositionsMultiples.TreeSimpleMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: integer);
begin
  HintNormal(TreeSimple.GetNodeAt(X, Y), TreeSimple, Bibliotheque);
end;

procedure TFrameCompositionsMultiples.CopyGrilleClick(Sender: TObject);
begin
  if (Grille.Selection.top > 0) and (Grille.Selection.top <= 7) then
    PressePapier.Assign(Grille.Rows[Grille.Selection.top]);
end;

procedure TFrameCompositionsMultiples.CutGrilleClick(Sender: TObject);
begin
  CopyGrille.Click;
  DeleteGrille.Click;
end;

procedure TFrameCompositionsMultiples.DeleteGrilleClick(Sender: TObject);
var
  i: integer;
begin
  if (Grille.Selection.top > 0) and (Grille.Selection.top <= 7) then
    for i := 0 to Grille.ColCount - 1 do
      Grille.cells[i, Grille.Selection.top] := '';
end;

function TFrameCompositionsMultiples.FaireLeTrou(Grille: TStringGrid;
  Position: integer): integer;
var
  i: integer;
begin
  if Grille.cells[0, Position] <> '' then
  begin
    if Grille.cells[0, 7] = '' then
    begin
      for i := 7 downto Position + 1 do
        Grille.Rows[i] := Grille.Rows[i - 1];
      Result := 2;
    end
    else
      Result := 0;
  end
  else
    Result := 1;
end;

procedure TFrameCompositionsMultiples.FrameResize(Sender: TObject);
begin
  inherited;
  FormMiniCompo.Width := GroupBoxListe.Width;
  FormMiniCompo.Height := GroupBoxListe.Height;
  if Sender <> Splitter2 then
    GroupBox5.Height := PanelListe.ClientHeight div 2;
end;

procedure TFrameCompositionsMultiples.BitBtnNewClick(Sender: TObject);
var
  Ok: boolean;
  i: integer;
begin
  if Modif then
    Ok := ControleSauve(False)
  else
    Ok := True;
  if Ok then
  begin
    for i := 1 to 7 do
      Grille.Rows[i].Clear;
    for i := 1 to 5 do
      Grille.cells[i, 8] := '';
    Modif := False;
    inherited;
  end;
  Grille.Invalidate;
end;

procedure TFrameCompositionsMultiples.CalculSommeGrille;
var
  i: integer;
  Reel, Somme1: double;
  Erreur: integer;
  Chaine: string;
begin
  Somme1 := 0;
  for i := 1 to 7 do
  begin
    Val(ConvertPoint(Grille.cells[1, i]), Reel, Erreur);
    Somme1 := Somme1 + Reel;
  end;
  Str(Somme1: 0: 1, Chaine);
  Grille.cells[1, 8] := Chaine;
end;

function TFrameCompositionsMultiples.CreerComposantBiblio
  : TComposantBibliotheque;
begin
  Result := TComposition.Create(EditNom.Text, BarreCategorie.Chemin,
    MComplement, EditSource.Text, Grille, nil, nil, True);
end;

function TFrameCompositionsMultiples.Controle;
var
  Erreur: integer;
  Compteur, i: integer;
  Rectan: TGridRect;
  CanSelect: boolean;
begin
  Erreur := 0;
  if not inherited Controle then
    Exit(False);

  Compteur := 0;
  for i := 1 to NbMatParCompo do
  begin
    if (Grille.cells[0, i] <> '') then
    begin
      Inc(Compteur);
      if ControlNum(ConvertPoint(Grille.cells[1, i]), 0.01, 100, 'R')
        <> ConvertPoint(Grille.cells[1, i]) then
      begin
        FormParent.FocusControl(Grille);
        Erreur := 4;
        Rectan.top := i;
        Rectan.bottom := i;
        Rectan.left := 1;
        Rectan.right := 1;
        Grille.Selection := Rectan;
        CanSelect := True;
        GrilleSelectCell(Self, Rectan.left, Rectan.top, CanSelect);
      end;
    end;
  end;
  if ControlNum(Grille.cells[1, 8], 100, 100, 'R') <> Grille.cells[1, 8] then
  begin
    FormParent.FocusControl(Grille);
    Erreur := 4;
  end;
  if Compteur = 0 then
  begin
    FormParent.FocusControl(Grille);
    Erreur := 5;
  end;

  if Erreur <> 0 then
  begin
    MessageDlgM(MessagesInterface[8 + Erreur], mtWarning, [mbOK], 0);
    Result := False;
  end
  else
    Result := True;
end;

procedure TFrameCompositionsMultiples.Affiche(comp: TComposantBibliotheque;
  Survol: boolean);
var
  i: integer;
  Chaine: string;
  Compo: TComposition;
begin
  inherited Affiche(comp, Survol);
  for i := 1 to 7 do
    Grille.Rows[i].Clear;
  if comp <> Nil then
    with comp as TComposition do
    begin
      for i := 1 to NbMat do
      begin
        Compo := BibCompositions.Get(IdentiMat[i]);
        if Compo <> nil then
        begin
          Grille.cells[0, i] := Compo.Nom;
          Str(Epaisseur[i]: 0: 1, Chaine);
          Grille.cells[1, i] := Chaine;
        end;
      end;
      CalculSommeGrille;
    end;
  ModeAffichage := False;
end;

end.
