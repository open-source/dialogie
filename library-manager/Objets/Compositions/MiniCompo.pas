unit MiniCompo;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, StdCtrls, BarreAdresse, ExtCtrls, ComposantBibliotheque,
  Elements, Materiaux, MatChangementPhase;

type
  TFormMiniCompo = class(TForm)
    Image1: TImage;
    PanelGenerique: TPanel;
    LabelCategorie: TLabel;
    BarreCategorie: TFrameBarreAdresse;
    Panel4: TPanel;
    LabelOrigine: TLabel;
    LabelComplement: TLabel;
    LabelNom: TLabel;
    Panel5: TPanel;
    EditSource: TEdit;
    MemoComplement: TMemo;
    EditNom: TEdit;
    zLabel73: TLabel;
    zLabel74: TLabel;
    ZImageFleche: TImage;
    Grille: TStringGrid;
    procedure GrilleDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure FormShow(Sender: TObject);
  private
    { Déclarations privées }
    function GetComplement: string;
    procedure SetComplement(Chaine: string);
    procedure AfficheElem(Elem: TElement; Ligne: integer);
    procedure AfficheMat(Mat: TMateriau; Ligne: integer; Epaisseur: double);
    procedure AfficheMCP(MCP: TMCP; Ligne: integer);
    procedure CalculSommeGrille;
  public
    { Déclarations publiques }
    Deplace: Boolean;
    IComp: TComposantBibliotheque;
    property MComplement: string Read GetComplement Write SetComplement;
    procedure Affiche(comp: TComposantBibliotheque);

    Constructor Create(AOwner: TComponent); override;
  end;

var
  FormMiniCompo: TFormMiniCompo;

implementation

uses
  Compositions, Utilitaires, constantes, BiblioMateriaux, BiblioElements,
  BiblioMCP, MainPleiades, Translateur;
{$R *.dfm}

Constructor TFormMiniCompo.Create;
begin
  inherited Create(AOwner);
  Deplace := False;
end;

procedure TFormMiniCompo.FormShow(Sender: TObject);
begin
  with Grille do
  begin
    Cells[0, 0] := Labels[32];
    Cells[1, 0] := Labels[35];
    Cells[2, 0] := LabelCm;
    Cells[3, 0] := Labeldens;
    Cells[4, 0] := Labels[33];
    Cells[5, 0] := Labels[34];
    Cells[0, 8] := Labels[310];
  end;
end;

procedure TFormMiniCompo.Affiche(comp: TComposantBibliotheque);
var
  i: integer;
  Mat: TMateriau;
  Elem: TElement;
  MCP: TMCP;
begin
  if comp <> Nil then
    with comp as TComposition do
    begin
      EditNom.Text := comp.Nom;
      MComplement := comp.Complement;
      MemoScrollUp(MemoComplement);
      EditSource.Text := comp.Source;
      BarreCategorie.Chemin := comp.categorie;

      for i := 1 to 7 do
        Grille.Rows[i].Clear;
      for i := 1 to NbMat do
      begin
        case TypeMat[i] of
          LettreMat:
            begin
              Mat := BibMateriaux.Get(IdentiMat[i]);
              if Mat <> nil then
                AfficheMat(Mat, i, Epaisseur[i]);
            end;
          LettreElem:
            begin
              Elem := BibElements.Get(IdentiMat[i]);
              if Elem <> nil then
                AfficheElem(Elem, i);
            end;
          LettreMCP:
            begin
              MCP := BibMCP.Get(IdentiMat[i]);
              if MCP <> nil then
                AfficheMCP(MCP, i);
            end
        end;
        CalculSommeGrille;
      end;
    end
    else
    begin
      EditNom.Text := '';
      MComplement := '';
      MemoScrollUp(MemoComplement);
      EditSource.Text := '';
      BarreCategorie.Chemin := '';

      for i := 1 to 8 do
        Grille.Rows[i].Clear;
    end;
end;

function TFormMiniCompo.GetComplement: string;
var
  i: integer;
begin
  Result := '';
  for i := 0 to MemoComplement.Lines.Count - 1 do
    Result := Result + MemoComplement.Lines[i] + '|';
end;

procedure TFormMiniCompo.GrilleDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
begin
  if (ACol = 1) and (ARow > 0) then
  begin
    Grille.Canvas.Brush.Style := bsSolid;
    Grille.Canvas.Brush.Color := Grille.Color;
    Grille.Canvas.FillRect(Rect);
    if (Grille.Cells[1, ARow] = ' ' + LettreMat) then
      FormMainPleiades.ListeIconesMat.Draw(Grille.Canvas,Rect.Left,Rect.Top+1,BibMateriaux.ImageBrute,True)
    else if (Grille.Cells[1, ARow] = ' ' + LettreElem) then
      FormMainPleiades.ListeIconesMat.Draw(Grille.Canvas,Rect.Left,Rect.Top+1,BibElements.ImageBrute,True)
    else if (Grille.Cells[1, ARow] = ' ' + LettreMCP) then
      FormMainPleiades.ListeIconesMat.Draw(Grille.Canvas,Rect.Left,Rect.Top+1,BibMCP.ImageBrute,True);
  end;
end;

procedure TFormMiniCompo.SetComplement(Chaine: string);
begin
  MemoComplement.Lines.Clear;
  while Chaine <> '' do
    MemoComplement.Lines.Add(GetItemPipe(Chaine));
end;

procedure TFormMiniCompo.AfficheElem(Elem: TElement; Ligne: integer);
begin
  if Elem = nil then
    Exit;
  Grille.Cells[0, Ligne] := Elem.Nom;
  Grille.Cells[1, Ligne] := ' ' + LettreElem;
  Grille.Cells[2, Ligne] := FormatFloat('0.0', Elem.Epaisseur, tfs);
  Grille.Cells[3, Ligne] := FormatFloat('0',
    Elem.MasseVol * Elem.Epaisseur / 100, tfs);
  Grille.Cells[4, Ligne] := FormatFloat('0.000', Elem.ConductiviteEquiv, tfs);
  Grille.Cells[5, Ligne] := FormatFloat('0.00', Elem.Resistance, tfs);
  CalculSommeGrille;
end;

procedure TFormMiniCompo.AfficheMat(Mat: TMateriau; Ligne: integer;
  Epaisseur: double);
begin
  if Mat = nil then
    Exit;
  Grille.Cells[0, Ligne] := Mat.Nom;
  Grille.Cells[1, Ligne] := ' ' + LettreMat;
  Grille.Cells[4, Ligne] := FormatFloat('0.000', Mat.Conductivite, tfs);
  if Epaisseur = -1 then
  begin
    Grille.Cells[2, Ligne] := '?';
    Grille.Cells[3, Ligne] := '?';
    Grille.Cells[5, Ligne] := '?';
  end
  else
  begin
    Grille.Cells[2, Ligne] := FormatFloat('0.0', Epaisseur, tfs);
    Grille.Cells[3, Ligne] := FormatFloat('0', Mat.MasseVol * Epaisseur / 100,
      tfs);
    if Mat.Conductivite <> 0 then
      Grille.Cells[5, Ligne] := FormatFloat('0.00',
        1 / Mat.Conductivite * Epaisseur / 100, tfs)
    else
      Grille.Cells[5, Ligne] := '';

  end;
  CalculSommeGrille;
end;

procedure TFormMiniCompo.AfficheMCP(MCP: TMCP; Ligne: integer);
begin
  if MCP = nil then
    Exit;
  Grille.Cells[0, Ligne] := MCP.Nom;
  Grille.Cells[1, Ligne] := ' ' + LettreMCP;
  Grille.Cells[2, Ligne] := FormatFloat('0.0', MCP.Epaisseur, tfs);
  Grille.Cells[3, Ligne] := FormatFloat('0',
    MCP.MasseVol * MCP.Epaisseur / 100, tfs);
  Grille.Cells[4, Ligne] := FormatFloat('0.000', MCP.Conductivite, tfs);
  if MCP.Conductivite <> 0 then
    Grille.Cells[5, Ligne] := FormatFloat('0.00',
      1 / MCP.Conductivite * MCP.Epaisseur / 100, tfs)
  else
    Grille.Cells[5, Ligne] := '';
  CalculSommeGrille;
end;

procedure TFormMiniCompo.CalculSommeGrille;
var
  i: integer;
  Reel, Somme1, Somme2, Somme3: double;
  Erreur: integer;
  Chaine: string;
begin
  Somme1 := 0;
  Somme2 := 0;
  Somme3 := 0;
  for i := 1 to 7 do
  begin
    Val(ConvertPoint(Grille.Cells[2, i]), Reel, Erreur);
    Somme1 := Somme1 + Reel;
    Val(ConvertPoint(Grille.Cells[3, i]), Reel, Erreur);
    Somme2 := Somme2 + Reel;
    Val(ConvertPoint(Grille.Cells[5, i]), Reel, Erreur);
    Somme3 := Somme3 + Reel;
  end;
  Str(Somme1: 0: 1, Chaine);
  Grille.Cells[2, 8] := Chaine;
  Str(Somme2: 0: 0, Chaine);
  Grille.Cells[3, 8] := Chaine;
  Str(Somme3: 0: 2, Chaine);
  Grille.Cells[5, 8] := Chaine;
end;

end.
