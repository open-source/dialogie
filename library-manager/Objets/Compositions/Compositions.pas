unit Compositions;

interface

uses
  // Delphi
  Windows,
  Messages,
  SysUtils,
  Classes,
  Graphics,
  Controls,
  Forms,
  Dialogs,
  ComCtrls,
  Buttons,
  ComposantBibliotheque,
  Grids,
  GestionSauvegarde,
  CreerSauvegarde,
  Materiaux,
  Elements,
  Xml.XMLIntf;

const
  NbMatParCompo = 7;
  LettreMat = 'M';
  LettreElem = 'E';


type
  TComposition = class(TComposantBibliotheque)
    NbMat : integer;
    TypeMat : array [1 .. NbMatParCompo] of char;
    IdentiMat : array [1 .. NbMatParCompo] of string;
    Epaisseur : array [1 .. NbMatParCompo] of double;
    RTtotal : double;
    RTotal : double;
    AbsF, AbsB : double; // Uniquement l� pour NDBM
  private
  var
    FGetEpaisseur : double;

  const
    FIdVersion : integer = 2;

    function getEpaisseur : double;
  protected

    procedure innerLoadXml(node : IXMLNode); override;
    procedure innerSaveXml(node : IXMLNode); override;

  public
    procedure CalculRtotal;
    procedure CalculRtotalProjet(ListeMat : TListeMateriau; ListeElem : TListeElement);
    constructor Create(Nom_, categorie_, complement_, source_ : string; Grille_ : TStringGrid); overload;
    constructor Create; overload;

    function Sauve(var Contenu : TCreationSauvegarde) : boolean; override;
    function Charge(var Contenu : TSauvegarde; NumVersion : integer) : boolean; override;

    procedure Assign(Compo : TComposition);
    function GetListeMat : TStringList;
    function GetListeElem : TStringList;
    function GetEpaisseurT : double;
    function Insere(Position : integer) : boolean;
    procedure AutoFiltrage;
    function Symetrique : boolean;
    property epaisseurTotale : double read getEpaisseur;
    function ValeursIdentiques(comp : TComposantBibliotheque) : boolean; override;
    class function idVersion : integer; override;
  end;

  TListeComposition = class(TList)
  private
    function GetComposition(const i : integer) : TComposition;
    procedure SetComposition(const i : integer; const Valeur : TComposition);
  public
    property objets[const index : integer] : TComposition read GetComposition write SetComposition; default;
    procedure ClearAndFree;
    function IndexOf(Chaine : string) : integer; overload;
    procedure AddItems(S : TListeComposition);
    procedure Assign(S : TListeComposition);
  end;

implementation

uses
  IzUtilitaires,
  BiblioMateriaux,
  BiblioElements, XmlUtil;

class function TComposition.idVersion : integer;
begin
  result := FIdVersion;
end;

function TComposition.ValeursIdentiques(comp : TComposantBibliotheque) : boolean;
var
  i : integer;
  Compo : TComposition;
begin
  Compo := comp as TComposition;
  result := True;
  result := result and (NbMat = Compo.NbMat);
  for i := 1 to NbMat do
  begin
    result := result and (TypeMat[i] = Compo.TypeMat[i]);
    result := result and (IdentiMat[i] = Compo.IdentiMat[i]);
    result := result and (Epaisseur[i] = Compo.Epaisseur[i]);
  end;
end;

procedure TListeComposition.Assign(S : TListeComposition);
begin
  Clear;
  AddItems(S);
end;

procedure TListeComposition.AddItems(S : TListeComposition);
var
  i : integer;
begin
  Capacity := Count + S.Count;
  for i := 0 to S.Count - 1 do
    Add(S[i]);
end;

function TListeComposition.IndexOf(Chaine : string) : integer;
begin
  for result := 0 to Count - 1 do
    if TComposition(items[result]).nom = Chaine then
      Exit;
  result := -1;
end;

procedure TListeComposition.ClearAndFree;
var
  i : integer;
begin
  for i := 0 to Count - 1 do
    TComposition(items[i]).Free;
  Clear;
end;

function TListeComposition.GetComposition(const i : integer) : TComposition;
begin
  if i < Count then
    result := Self.items[i]
  else
    result := nil;
end;

procedure TListeComposition.SetComposition(const i : integer; const Valeur : TComposition);
begin
  Self.items[i] := Valeur;
end;

function TComposition.Insere(Position : integer) : boolean;
var
  i : integer;
begin
  if NbMat < NbMatParCompo then
  begin
    for i := NbMat downto Position do
    begin
      TypeMat[i + 1] := TypeMat[i];
      IdentiMat[i + 1] := IdentiMat[i];
      Epaisseur[i + 1] := Epaisseur[i];
    end;
    Inc(NbMat);
    result := True;
  end
  else
    result := False;
end;

procedure TComposition.AutoFiltrage;
var
  ISource, Idest : integer;
begin
  Idest := 1;
  ISource := 1;
  while ISource <= NbMat do
  begin
    while Epaisseur[ISource] = -100 do
      Inc(ISource);
    Epaisseur[Idest] := Epaisseur[ISource];
    IdentiMat[Idest] := IdentiMat[ISource];
    TypeMat[Idest] := TypeMat[ISource];
    Inc(Idest);
    Inc(ISource);
  end;
  NbMat := NbMat - (ISource - Idest);
end;

procedure TComposition.CalculRtotalProjet(ListeMat : TListeMateriau; ListeElem : TListeElement);
var
  i : integer;
  Mat : TMateriau;
  Elem : TElement;
  index : integer;
begin
  RTotal := 0;
  for i := 1 to NbMat do
    case TypeMat[i] of
      LettreMat :
        begin
          index := ListeMat.IndexOf(IdentiMat[i]);
          if index <> -1 then
          begin
            Mat := ListeMat[index];
            if (Epaisseur[i] <> 0) then
              RTotal := RTotal + (1 / (Mat.Conductivite / (Epaisseur[i] / 100)));
          end;
        end;
      LettreElem :
        begin
          index := ListeElem.IndexOf(IdentiMat[i]);
          if index <> -1 then
          begin
            Elem := ListeElem[index];
            RTotal := RTotal + (1 / Elem.Conductivite);
          end;
        end;
    end;
end;

procedure TComposition.CalculRtotal();
var
  i : integer;
  m : TMateriau;
  e : TElement;
begin
  RTotal := 0;
  for i := 1 to NbMat do
  begin
    if IdentiMat[i] = '' then
      continue;
    try
      case TypeMat[i] of
        LettreMat :
          begin
            m := BibMateriaux.Get(IdentiMat[i]);
            if m <> nil then
            begin
              if Epaisseur[i] <> 0 then
                RTotal := RTotal + (1 / (m.Conductivite / (Epaisseur[i] / 100)));
            end
            else
              showMessage('Le mat�riau "' + IdentiMat[i] + '" n''est pas dans la biblioth�que.');
          end;
        LettreElem :
          begin
            e := BibElements.Get(IdentiMat[i]);
            if e <> nil then
              RTotal := RTotal + (1 / e.Conductivite)
            else
              showMessage('L''�l�ment "' + IdentiMat[i] + '" n''est pas dans la biblioth�que.');
          end;
      end;
    finally

    end;
  end;
end;

constructor TComposition.Create;
begin
  inherited Create;
  innerTagName := 'composition';
  FGetEpaisseur := -1;
end;

function TComposition.GetEpaisseurT : double;
var
  i : integer;
begin
  if Self = nil then
    result := 0
  else
  begin
    result := 0;
    for i := 1 to NbMat do
      result := result + Epaisseur[i] / 100;
  end;
end;


function TComposition.GetListeMat : TStringList;
var
  i : integer;
begin
  result := TStringList.Create;
  result.Duplicates := dupIgnore;
  for i := 1 to NbMat do
    if TypeMat[i] = LettreMat then
      result.Add(IdentiMat[i]);
end;

function TComposition.GetListeElem : TStringList;
var
  i : integer;
begin
  result := TStringList.Create;
  result.Duplicates := dupIgnore;
  for i := 1 to NbMat do
    if TypeMat[i] = LettreElem then
      result.Add(IdentiMat[i]);
end;


constructor TComposition.Create(Nom_, categorie_, complement_, source_ : string; Grille_ : TStringGrid);
var
  i : integer;
  Reel : double;
  Erreur : integer;
begin
  inherited Create(Nom_, categorie_, complement_, source_);
  innerTagName := 'composition';
  if Grille_ <> nil then
  begin
    NbMat := 0;
    for i := 1 to NbMatParCompo do
    begin
      if Grille_.Cells[0, i] <> '' then
      begin
        Inc(NbMat);
        IdentiMat[NbMat] := Grille_.Cells[0, i];
        TypeMat[NbMat] := Grille_.Cells[1, i][2];
        Val(ConvertPoint(Grille_.Cells[2, i]), Reel, Erreur);
        Epaisseur[NbMat] := Reel;
      end;
    end;
  end;
  FGetEpaisseur := -1;
  CalculRtotal;
end;

function TComposition.Sauve(var Contenu : TCreationSauvegarde) : boolean;
var
  i : integer;
begin
  inherited SauveDebut(Contenu);

  Contenu.Add(NbMat);
  for i := 1 to NbMatParCompo do
  begin
    if i <= NbMat then
    begin
      Contenu.Add(TypeMat[i]);
      Contenu.Add(IdentiMat[i]);
      Contenu.Add(Epaisseur[i]);
    end
    else
    begin
      Contenu.Add(' ');
      Contenu.Add('0');
      Contenu.Add('0');
    end;
  end;

  inherited SauveFin(Contenu);
  result := True;
end;

function TComposition.Charge(var Contenu : TSauvegarde; NumVersion : integer) : boolean;
var
  i : integer;
  Ligne : string;
begin

  inherited chargeDebut(Contenu);

  NbMat := Contenu.GetLigneEntier;
  for i := 1 to NbMatParCompo do
  begin
    Ligne := Contenu.GetLigne;
    if Length(Ligne) > 0 then
      TypeMat[i] := Ligne[1]
    else
      TypeMat[i] := ' ';
    IdentiMat[i] := Contenu.GetLigne;
    Epaisseur[i] := Contenu.GetLigneReel;
  end;

  inherited ChargeFin(Contenu);
  result := True;
end;


procedure TComposition.innerLoadXml(node : IXMLNode);
var
  i : integer;
  child : IXmlNode;
  child1 : IXmlNode;
  s : string;
begin
  inherited innerLoadXml(node);
  child := TXmlUtil.getChild(node, 'items');
  NbMat := TXmlUtil.getAttInt(child, 'nb');
  for i := 1 to NbMatParCompo do
  begin
    child1 := child.ChildNodes[i - 1];
    s := TXmlUtil.getTagString(child1, 'type');
    if s = '' then
      s := ' ';
    TypeMat[i] := s[1];
    IdentiMat[i] := TXmlUtil.getTagString(child1, 'id');
    Epaisseur[i] := TXmlUtil.getTagDouble(child1, 'epaisseur');
  end;
end;


procedure TComposition.innerSaveXml(node : IXMLNode);
var
  i : integer;
  child : IXmlNode;
  child1 : IXmlNode;
begin
  inherited innerSaveXml(node);
  child := TXmlUtil.addChild(node, 'items');
  TXmlUtil.setAtt(child, 'nb', NbMat);
  for i := 1 to NbMatParCompo do
  begin
    child1 := TXmlUtil.addChild(child, 'item');
    if i <= NbMat then
    begin
      TXmlUtil.setTag(child1, 'type', TypeMat[i]);
      TXmlUtil.setTag(child1, 'id', IdentiMat[i]);
      TXmlUtil.setTag(child1, 'epaisseur', Epaisseur[i]);
    end
    else
    begin
      TXmlUtil.setTag(child1, 'type', ' ');
      TXmlUtil.setTag(child1, 'id', '0');
      TXmlUtil.setTag(child1, 'epaisseur', '0');
    end;
  end;
end;


procedure TComposition.Assign(Compo : TComposition);
begin
  inherited Assign(Compo);
  NbMat := Compo.NbMat;
  TypeMat := Compo.TypeMat;
  IdentiMat := Compo.IdentiMat;
  Epaisseur := Compo.Epaisseur;
  RTotal := Compo.RTotal;
  AbsF := Compo.AbsF;
  AbsB := Compo.AbsB;
end;

function TComposition.Symetrique : boolean;
var
  i : integer;
begin
  for i := 1 to NbMat do
    if (Epaisseur[i] <> Epaisseur[NbMat - i + 1]) or (IdentiMat[i] <> IdentiMat[NbMat - i + 1]) then
      Exit(False);
  result := True;
end;

function TComposition.getEpaisseur : double;
var
  i : integer;
begin
  if Self = nil then
    Exit(0.001);
  if FGetEpaisseur = -1 then
  begin
    FGetEpaisseur := 0;
    for i := 1 to NbMat do
      FGetEpaisseur := FGetEpaisseur + Epaisseur[i] / 100;
  end;
  result := FGetEpaisseur;
end;

end.
