unit CadreCompositionsPleiades;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, CadreCompositions, Menus, Grids, StdCtrls, Buttons, ExtCtrls,
  ComCtrls, ImgList, NCadreCompositions, BarreAdresse;

type
  TFrameCompositionsPleiades = class(TFrameCompositions)
    procedure BitBtnExpCompoClick(Sender: TObject);
    procedure Export1Click(Sender: TObject);
  private
    { Déclarations privées }
  public
    { Déclarations publiques }
  end;

implementation

uses
  Materiaux, Elements, Projet, compositions, BiblioCompositions, Utilitaires,
  DHMulti, BiblioMateriaux, BiblioElements, Translateur, DialogsInternational,
  MatChangementPhase, BiblioMCP;

{$R *.dfm}

procedure TFrameCompositionsPleiades.BitBtnExpCompoClick(Sender: TObject);
var
  i:  integer;
  mat: TMateriau;
  Elem: TElement;
  MCP: TMCP;
  Compo: TComposition;
  Noeud: TTreeNode;
  prec: word;
  Ok: boolean;
begin
  inherited;
  if Controle then
  begin
    Ok := True;
    Noeud := nil;
    if ProjetCourant.ListeCompo.IndexOf(Nom.Text) <> -1 then
      Ok := (MessageDlgM(Labels[256], mtConfirmation, mbOkCancel, 0) = mrOk);
    if Ok then
    begin
      Compo := TComposition.Create(Nom.Text, Categorie.Text,
        Complement.Text, Source.Text, Grille, False);
      ProjetCourant.AjouteCompo(Compo);
      if Compo.Identificateur = 0 then
        ProjetCourant.IdentifieCompo(Compo);
      Noeud := BibCompositions.PoseDansArbre(
        Compo, FormDHMulti.FComposants.TreeCompo, False, True);

      FormDHMulti.FParois.AfficheParoi;
      FormDHMulti.FComposants.TreeCompo.OnChange(nil, nil);
      FormDHMulti.FComposants.ComboStandard.OnChange(nil);
    end;

    if Noeud <> nil then
    begin
      prec := mrYes;
      for i := 1 to NbMatParCompo do
        if (Grille.Cells[0, i] <> '') and (Grille.cells[1, i][2] =
          LettreMat) then
        begin
          Mat := TMateriau.Create;
          Mat.Assign(BibMateriaux.Get(Grille.Cells[0, i]));
          ProjetCourant.AjouteMat(Mat, prec);
          BibMateriaux.PoseDansArbre(Mat, FormDHMulti.FComposants.TreeMat, False, True);
        end
        else if (Grille.Cells[0, i] <> '') and
          (Grille.cells[1, i][2] = LettreElem) then
        begin
          Elem := TElement.Create;
          Elem.Assign(BibElements.Get(Grille.Cells[0, i]));
          ProjetCourant.AjouteElem(Elem, prec);
          BibElements.PoseDansArbre(Elem, FormDHMulti.FComposants.TreeElem, False, True);
        end
        else if (Grille.Cells[0, i] <> '') and
          (Grille.cells[1, i][2] = LettreMCP) then
        begin
          MCP := TMCP.Create;
          MCP.Assign(BibMCP.Get(Grille.Cells[0, i]));
          ProjetCourant.AjouteMCP(MCP, prec);
          BibMCP.PoseDansArbre(MCP, FormDHMulti.FComposants.TreeMCP, False, True);
        end;
    end
    else
      MessageDlgM(messagees[19], mtWarning, [mbOK], 0);
  end;
end;

procedure TFrameCompositionsPleiades.Export1Click(Sender: TObject);
var
  I: integer;
  Compo: TComposition;
begin
  inherited;
  for I := Dragage.TreeView.Selectioncount - 1 downto 0 do
  begin
    Dragage := Dragage.TreeView.Selections[i];
    Compo := BibCompositions.Get(Dragage.Text);
    if Compo <> nil then
    begin
      Affiche(Compo);
      BitBtnExpCompoClick(nil);
    end;
  end;
end;

end.
