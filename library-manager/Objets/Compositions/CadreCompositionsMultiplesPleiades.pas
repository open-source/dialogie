unit CadreCompositionsMultiplesPleiades;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, CadreCompositionsMultiples, Grids, StdCtrls, Buttons, ComCtrls,
  ExtCtrls, Menus, NCadreCompositionsMultiples, BarreAdresse;

type
  TFrameCompositionsMultiplesPleiades = class(TFrameCompositionsMultiples)
    procedure BitBtnExpMultiClick(Sender: TObject);
    procedure Export1Click(Sender: TObject);
  private
    { Déclarations privées }
  public
    { Déclarations publiques }
  end;

var
  FrameCompositionsMultiplesPleiades: TFrameCompositionsMultiplesPleiades;

implementation

uses
  Materiaux, Elements, Compositions, Projet, Utilitaires, Translateur,
  BiblioCompositions, DHMulti, BiblioMateriaux, BiblioElements, DialogsInternational,
  MatChangementPhase, BiblioMCP;

{$R *.dfm}

procedure TFrameCompositionsMultiplesPleiades.BitBtnExpMultiClick(Sender: TObject);
var
  i, j: integer;
  mat: TMateriau;
  Elem: TElement;
  MCPH: TMCP;
  Noeud: TTreeNode;
  prec: word;
  Ok: boolean;
  comp, SousCompo: TComposition;
  Compo: TComposition;
begin
  inherited;
  if Controle then
  begin
    Ok := True;
    if ProjetCourant.ListeCompo.IndexOf(Nom.Text) <> -1 then
      Ok := (MessageDlgM(Labels[256], mtConfirmation, mbOkCancel, 0) = mrOk);
    Noeud := nil;
    Compo := nil;
    if Ok then
    begin
      Compo := TComposition.Create(Nom.Text, Categorie.Text,
        Complement.Text, Source.Text, Grille, True);
      ProjetCourant.AjouteCompo(Compo);
      if Compo.Identificateur = 0 then
        ProjetCourant.IdentifieCompo(Compo);
      Noeud := BibCompositions.PoseDansArbre(
        Compo, FormDHMulti.FComposants.TreeCompo, False, True);

      FormDHMulti.FParois.AfficheParoi;
      FormDHMulti.FComposants.TreeCompo.OnChange(nil, nil);
      FormDHMulti.FComposants.ComboStandard.OnChange(nil);
    end;

    if Noeud <> nil then
    begin
      for i := 1 to Compo.NbMat do
      begin
        SousCompo := BibCompositions.Get(Compo.IdentiMat[i]);
        comp := TComposition.Create;
        comp.Assign(SousCompo);
        ProjetCourant.AjouteCompo(comp);
        Noeud := BibCompositions.PoseDansArbre(
          comp, FormDHMulti.FComposants.TreeCompo, False, True);
        if Noeud <> nil then
        begin
          prec := mrYes;
          for j := 1 to NbMatParCompo do
            if (SousCompo.IdentiMat[j] <> '') and
              (SousCompo.TypeMat[j] = LettreMat) then
            begin
              Mat := TMateriau.Create;
              Mat.Assign(BibMateriaux.Get(SousCompo.IdentiMat[j]));
              ProjetCourant.AjouteMat(Mat, prec);
              BibMateriaux.PoseDansArbre(Mat, FormDHMulti.FComposants.TreeMat,
                False, True);
            end
            else if (SousCompo.IdentiMat[j] <> '') and
              (SousCompo.TypeMat[j] = LettreElem) then
            begin
              Elem := TElement.Create;
              Elem.Assign(BibElements.Get(SousCompo.IdentiMat[j]));
              ProjetCourant.AjouteElem(Elem, prec);
              BibElements.PoseDansArbre(Elem, FormDHMulti.FComposants.TreeElem,
                False, True);
            end
            else if (SousCompo.IdentiMat[j] <> '') and
              (SousCompo.TypeMat[j] = LettreMCP) then
            begin
              MCPH := TMCP.Create;
              MCPH.Assign(BibMCP.Get(SousCompo.IdentiMat[j]));
              ProjetCourant.AjouteMCP(MCPH, prec);
              BibElements.PoseDansArbre(MCPH, FormDHMulti.FComposants.TreeMCP,
                False, True);
            end;
        end;
      end;
    end
    else
      MessageDlgM(messagees[19], mtWarning, [mbOK], 0);
  end;
end;

procedure TFrameCompositionsMultiplesPleiades.Export1Click(Sender: TObject);
var
  I: integer;
  Compo: TComposition;
begin
  inherited;
  for I := Dragage.TreeView.Selectioncount - 1 downto 0 do
  begin
    Dragage := Dragage.TreeView.Selections[i];
    Compo := BibCompositions.Get(Dragage.Text);
    if Compo <> nil then
    begin
      Affiche(Compo);
      BitBtnExpMultiClick(nil);
    end;
  end;
end;

end.
