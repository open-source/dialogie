unit NCadreCompositionsMultiplesPleiades;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, NCadreCompositionsMultiples, Menus, Grids, ExtCtrls, ComCtrls,
  StdCtrls, Buttons, BarreAdresse, Compositions;

type
  TFrameCompositionsMultiplesPleiades = class(TFrameCompositionsMultiples)
    BitBtnExp: TBitBtn;
    procedure BitBtnExpClick(Sender: TObject);
    procedure Export1Click(Sender: TObject);
    procedure FrameResize(Sender: TObject);
  private
    { Déclarations privées }
  public
    { Déclarations publiques }
    procedure Exporte(comp: TComposition);
  end;

var
  FrameCompositionsMultiplesPleiades: TFrameCompositionsMultiplesPleiades;

implementation

uses
  Projet, DHMulti, BiblioCompositions, Materiaux, BiblioMateriaux, Elements,
  BiblioElements, MatChangementPhase, BiblioMCP, dialogsinternational,
  Translateur, MainPleiades, PontsIntegres, BiblioPontsIntegres;
{$R *.dfm}

procedure TFrameCompositionsMultiplesPleiades.BitBtnExpClick(Sender: TObject);
var
  comp: TComposition;
begin
  inherited;
  if Controle then
  begin
    comp := CreerComposantBiblio as TComposition;
    Exporte(comp);
  end;
end;

procedure TFrameCompositionsMultiplesPleiades.Export1Click(Sender: TObject);
var
  i: integer;
  comp: TComposition;
begin
  inherited;
  for i := Tree.Selectioncount - 1 downto 0 do
  begin
    comp := TComposition.Create;
    comp.Assign(Tree.Selections[i].Data);
    Exporte(comp);
  end;
end;

procedure TFrameCompositionsMultiplesPleiades.Exporte(comp: TComposition);
var
  prec: word;
  Noeud: TTreeNode;
  i: integer;
  SousCompo, SComp: TComposition;
  j: integer;
  Mat: TMateriau;
  Elem: TElement;
  MCPH: TMCP;
  Pont: TPontIntegre;
begin
  prec := mrYes;
  ProjetCourant.AjouteCompo(comp);
  if comp.Identificateur = 0 then
    ProjetCourant.IdentifieCompo(comp);
  Noeud := BibCompositions.PoseDansArbre(comp,
    FormDHMulti.FComposants.TreeCompo, False, True);
  FormDHMulti.FComposants.TreeCompo.OnChange(nil, nil);
  FormDHMulti.FComposants.ComboStandard.OnChange(nil);

  if Noeud <> nil then
  begin
    for i := 1 to comp.NbMat do
    begin
      SousCompo := BibCompositions.Get(comp.IdentiMat[i]);
      if SousCompo <> nil then
      begin
        SComp := TComposition.Create;
        SComp.Assign(SousCompo);
        ProjetCourant.AjouteCompo(SComp);
        Noeud := BibCompositions.PoseDansArbre(SComp,
          FormDHMulti.FComposants.TreeCompo, False, True);
        if Noeud <> nil then
        begin
          prec := mrYes;
          for j := 1 to SousCompo.NbMat do
            if (SousCompo.IdentiMat[j] <> '') and
              (SousCompo.TypeMat[j] = LettreMat) then
            begin
              Mat := TMateriau.Create;
              Mat.Assign(BibMateriaux.Get(SousCompo.IdentiMat[j]));
              ProjetCourant.AjouteMat(Mat, prec);
              BibMateriaux.PoseDansArbre(Mat, FormDHMulti.FComposants.TreeMat,
                False, True);
            end
            else if (SousCompo.IdentiMat[j] <> '') and
              (SousCompo.TypeMat[j] = LettreElem) then
            begin
              Elem := TElement.Create;
              Elem.Assign(BibElements.Get(SousCompo.IdentiMat[j]));
              ProjetCourant.AjouteElem(Elem, prec);
              BibElements.PoseDansArbre(Elem, FormDHMulti.FComposants.TreeElem,
                False, True);
            end
            else if (SousCompo.IdentiMat[j] <> '') and
              (SousCompo.TypeMat[j] = LettreMCP) then
            begin
              MCPH := TMCP.Create;
              MCPH.Assign(BibMCP.Get(SousCompo.IdentiMat[j]));
              ProjetCourant.AjouteMCP(MCPH, prec);
              BibElements.PoseDansArbre(MCPH, FormDHMulti.FComposants.TreeMCP,
                False, True);
            end;
          for j := 1 to comp.NbPont do
          begin
            Pont := TPontIntegre.Create;
            Pont.Assign(BibPontsIntegres.Get(SousCompo.IdentiPont[j]));
            ProjetCourant.AjoutePontIntegre(Pont, prec);
            BibPontsIntegres.PoseDansArbre(Pont,
              FormDHMulti.FComposants.TreePontsIntegres, False, True);
          end
        end;
      end;
    end;
  end
  else
    MessageDlgM(messagees[19], mtWarning, [mbOK], 0);
  FormDHMulti.FParois.AfficheParoi;
end;

procedure TFrameCompositionsMultiplesPleiades.FrameResize(Sender: TObject);
begin
  inherited;
  EditFiltre.Images := FormMainPleiades.ImageListNMenu;
  EditFiltre.RightButton.ImageIndex := 303;
  EditFiltreSimple.Images := FormMainPleiades.ImageListNMenu;
  EditFiltreSimple.RightButton.ImageIndex := 303;
end;

end.
