unit TacheFiltre;

interface

uses
  Classes, ComCtrls, GestionBib;

type
  TThreadFiltre = class(TThread)
  private
    { D�clarations priv�es }
    Selection: string;
    DestVisible, SourceVisible: boolean;
  protected
    procedure Execute; override;
    procedure GetSelection;
    procedure CacheEtVide;
    procedure CacheMontre;
    // procedure Insere;
  public
    TreeSource, TreeDest: TTreeView;
    Chaine: string;
    Vider: boolean;
    Bibliotheque: TBibliotheque;
  end;

implementation

Uses Utilitaires, ComposantBibliotheque, forms, Controls;

{ Important : les m�thodes et propri�t�s des objets de la VCL peuvent uniquement
  �tre utilis�es dans une m�thode appel�e en utilisant Synchronize, comme :

  Synchronize(UpdateCaption);

  o� UpdateCaption serait de la forme

  procedure TThreadFiltre.UpdateCaption;
  begin
  Form1.Caption := 'Mis � jour dans le thread';
  end;

  or

  Synchronize(
  procedure
  begin
  Form1.Caption := 'Mis � jour dans le thread via une m�thode anonyme'
  end
  )
  );

  o� une m�thode anonyme est pass�e.

  De la m�me fa�on, le d�veloppeur peut appeler la m�thode Queue avec des param�tres similaires
  � ci-dessus, au lieu de passer une autre classe TThread en tant que premier param�tre, en pla�ant
  le thread appelant dans une file d'attente avec l'autre thread.

}

{ TThreadFiltre }

procedure TThreadFiltre.CacheMontre;
var
  I: Integer;
begin
  if Selection <> '' then
    for I := 0 to TreeSource.items.Count - 1 do
      if TreeSource.items.Item[I].Text = Selection then
      begin
        TreeSource.Selected := TreeSource.items.Item[I];
        Break;
      end;
end;

procedure TThreadFiltre.CacheEtVide;
begin
  if Vider then
    TreeDest.items.Clear;
end;

procedure TThreadFiltre.GetSelection;
begin
  Screen.Cursor := crHourGlass;
  TreeDest.items.BeginUpdate;
  if TreeDest.SelectionCount > 0 then
    Selection := TreeDest.Selections[0].Text
  else
    Selection := '';
end;

procedure TThreadFiltre.Execute;
var
  I: integer;
  Node: TTreeNode;
  ListeCondition: TStringList;
  Condition: string;
  Trouve: boolean;
  j: integer;
begin
  // if (Length(Chaine) >= 1) and (Length(Chaine) <= 2) then
  // Exit;
  Synchronize(GetSelection);
  try
  if Chaine = '' then
  begin
    DestVisible := False;
    SourceVisible := True;
  end
  else
  begin
    SourceVisible := False;
    DestVisible := True;
    ListeCondition := TStringList.Create;
    while Chaine <> '' do
    begin
      Condition := GetItemEspace(Chaine);
      if Condition <> '' then
        ListeCondition.Add(Condition);
    end;

    {TreeSource.visible := True;
    TreeDest.visible := False;
    if Vider then
      TreeDest.items.Clear;}
    Synchronize(CacheEtVide);
    for I := 0 to TreeSource.items.Count - 1 do
    begin
      if Terminated then
      Begin
        Selection := '';
        Exit;
      End;
      if (TreeSource.items[I].ImageIndex <> Bibliotheque.ImageIndexDossier) then
      begin
        Trouve := True;
        for j := 0 to ListeCondition.Count - 1 do
        begin
          if (Pos(SansAccentMAJ(ListeCondition[j]),
              SansAccentMAJ(TreeSource.items[I].Text)) = 0) and
            (Pos(SansAccentMAJ(ListeCondition[j]),
              SansAccentMAJ(TComposantBibliotheque(TreeSource.items[I].Data)
                  .categorie)) = 0) then
            Trouve := False;
        end;

        if Trouve then
        begin
          Node := TreeDest.items.AddChildObject(nil, TreeSource.items[I].Text,
            TreeSource.items[I].Data);
          Node.ImageIndex := TreeSource.items[I].ImageIndex;
          Node.SelectedIndex := TreeSource.items[I].SelectedIndex;
        end;
      end;
    end;
    ListeCondition.Free;
  end;
  Synchronize(CacheMontre);
  finally
    TreeDest.Items.EndUpdate;
    Screen.Cursor := crDefault;
  end;
end;

end.
