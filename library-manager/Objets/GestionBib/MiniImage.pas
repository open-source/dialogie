unit MiniImage;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls;

type
  TFormMiniImage = class(TForm)
    Image1: TImage;
  public
    { Déclarations publiques }
    Deplace: Boolean;
    Image : String;
    Constructor Create(AOwner: TComponent); override;
  end;

var
  FormMiniImage: TFormMiniImage;

implementation

{$R *.dfm}
Constructor TFormMiniImage.Create;
begin
  inherited Create(AOwner);
  Deplace := False;
end;

end.
