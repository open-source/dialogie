unit CadreBib;

interface

uses
  // Delphi
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls, ComCtrls,
  // Pleiades
  Menus, BarreAdresse, GestionBib, ComposantBibliotheque, ImgList, TacheFiltre;

type
  TFrameBib = class(TFrame)
    GroupBoxListe: TGroupBox;
    Tree: TTreeView;
    GroupBoxCaracteristiques: TGroupBox;
    zPanel3: TPanel;
    BitBtnNew: TBitBtn;
    BitBtnsave: TBitBtn;
    PopupComponent: TPopupMenu;
    Open1: TMenuItem;
    Delete1: TMenuItem;
    Export1: TMenuItem;
    Rename1: TMenuItem;
    PanelGenerique: TPanel;
    LabelCategorie: TLabel;
    BarreCategorie: TFrameBarreAdresse;
    PanelFiltre: TPanel;
    LabelFiltre: TLabel;
    TreeFiltre: TTreeView;
    EditFiltre: TButtonedEdit;
    PanelListe: TPanel;
    Splitter3: TSplitter;
    Panel4: TPanel;
    LabelOrigine: TLabel;
    LabelComplement: TLabel;
    LabelNom: TLabel;
    Panel5: TPanel;
    EditSource: TEdit;
    MemoComplement: TMemo;
    EditNom: TEdit;
    procedure TreeChange(Sender: TObject; Node: TTreeNode);
    procedure TreeDragDrop(Sender, Source: TObject; X, Y: integer);
    procedure TreeDragOver(Sender, Source: TObject; X, Y: integer;
      State: TDragState; var Accept: boolean);
    procedure TreeMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: integer);
    procedure BitBtnNewClick(Sender: TObject);
    procedure BitBtnsaveClick(Sender: TObject);
    procedure BitBtnsaveMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: integer);
    procedure ComboCategorieChange(Sender: TObject);
    procedure TreeKeyUp(Sender: TObject; var Key: word; Shift: TShiftState);
    procedure Open1Click(Sender: TObject);
    procedure Delete1Click(Sender: TObject);
    procedure PopupComponentPopup(Sender: TObject);
    procedure Rename1Click(Sender: TObject);
    procedure FrameEnter(Sender: TObject);
    procedure BarreCategorieMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: integer);
    procedure EditFiltreChange(Sender: TObject);
    procedure EditFiltreRightButtonClick(Sender: TObject);
    procedure TreeFiltreMouseMove(Sender: TObject; Shift: TShiftState;
      X, Y: integer);
    procedure TreeMouseMove(Sender: TObject; Shift: TShiftState; X, Y: integer);
    procedure Splitter3Moved(Sender: TObject);
    procedure TreeMouseLeave(Sender: TObject);
    procedure TreeFiltreMouseLeave(Sender: TObject);
    procedure FrameResize(Sender: TObject);
    procedure TreeAddition(Sender: TObject; Node: TTreeNode);
    procedure TreeDeletion(Sender: TObject; Node: TTreeNode);
    procedure TreeEdited(Sender: TObject; Node: TTreeNode; var S: string);
  private
    { Déclarations privées }
    function GetComplement: string;
    procedure SetComplement(Chaine: string);
    procedure MemoScrollUp;
    procedure AffichageDirect(comp: TComposantBibliotheque;
      EstSelection: boolean);
    procedure LibereFiltre(Sender: TObject);
  protected
    ThreadFiltre: TThreadFiltre;
    HitTest: THitTests;
    CompStore, CompAffiche, CompVide: TComposantBibliotheque;
    NomPrecedent: string;
    Modif: boolean;
    Dragage: TTreeNode;
    SauveReadOnly: boolean;
    NoeudNon: TTreeNode;
    ModeAffichage: boolean;
    CompAEteAffiche: boolean;
    AOuvrir: TCustomCombo;
    ErreurSaisie: integer;
    function Controle: boolean; virtual;
    // function ControleSauve(Forcee: boolean): TTreeNode;
    function ControleSauve(Forcee: boolean; var Noeud: TTreeNode): boolean;
      overload;
    function ControleSauve(Forcee: boolean): boolean; overload;
    function HorsLimite(ChampEdit: TEdit; mini, maxi: double;
      T: char): boolean; overload;
    function HorsLimite(Combo: TCustomCombo): boolean; overload;
    procedure Filtrer(TreeSource, TreeDest: TTreeView; Chaine: string;
      Vider: boolean);
    procedure HintFiltre(Arbre: TTreeView; Noeud: TTreeNode);
    procedure HintNormal(Noeud: TTreeNode; Arbre: TTreeView;
      Biblio: TBibliotheque);
    procedure AfficheImage(Image: TImage; NomImage: String;
      PleineLargeur: boolean);

    function GetImage1Dossier(Noeud: TTreeNode): string;
    function GetImage2Dossier(Noeud: TTreeNode): string;

    property MComplement: string Read GetComplement Write SetComplement;
  public
    { Déclarations publiques }
    ModificationAutorise: boolean;
    FormParent: TForm;
    CheckHint: boolean;
    MessagesInterface: TStringList;
    Bibliotheque: TBibliotheque;
    TreeModifie: boolean;
    function CreerComposantBiblio: TComposantBibliotheque; virtual; abstract;
    procedure Store;
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure EffaceDragage(Confirmation, Sauvegarde: boolean);
    procedure Affiche(comp: TComposantBibliotheque; Survol: boolean); virtual;
    function AccepteDrag: boolean;
  end;

implementation

uses
  Utilitaires, DialogsInternational, uVistafuncs, Constantes, translateur,
  pngimage, Resample;
{$R *.dfm}

procedure TFrameBib.Store;
begin
  if Not ModeAffichage then
  begin
    if CompAffiche = CompStore then
      CompAffiche := Nil;
    CompStore.Free;
    CompStore := CreerComposantBiblio;
  end;
end;

destructor TFrameBib.Destroy;
begin
  CompStore.Free;
  MessagesInterface.Free;
  ThreadFiltre.Free;
  CompVide.Free;
  inherited Destroy;
end;

function TFrameBib.AccepteDrag: boolean;
begin
  Result := (Bibliotheque.EstDuType(Dragage.ImageIndex)
      and not TComposantBibliotheque(Dragage.Data).ReadOnly) or
    (Dragage.ImageIndex = Bibliotheque.ImageIndexDossier) or
    (Dragage.ImageIndex = Bibliotheque.ImageIndexDossierS);
end;

constructor TFrameBib.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  NoeudNon := nil;
  CompStore := Nil;
  CompAffiche := Nil;
  SauveReadOnly := False;
  ModificationAutorise := True;
  CheckHint := True;
  FormParent := nil;
  MessagesInterface := TStringList.Create;
  BarreCategorie.Tree := Tree;
  BarreCategorie.FiltreImageIndex := 0;
  BarreCategorie.Chemin := '';
  Modif := False;
  CompVide := CreerComposantBiblio;
  TreeModifie := False;
  ThreadFiltre := Nil;
end;

procedure TFrameBib.Delete1Click(Sender: TObject);
var
  I: integer;
  TreeSource: TCustomTreeView;
begin
  if Dragage = nil then
    Exit;
  TreeSource := Dragage.TreeView;
  if TreeSource.SelectionCount > 1 then
  begin
    if MessageDLGM(Labels[837], mtConfirmation, mbOkCancel, 0) = mrOk then
    begin
      for I := TreeSource.SelectionCount - 1 downto 0 do
      begin
        Dragage := TreeSource.Selections[I];
        EffaceDragage(False, False);
      end;
      Bibliotheque.SauveListe;
    end;
  end
  else
  begin
    for I := TreeSource.SelectionCount - 1 downto 0 do
    begin
      Dragage := TreeSource.Selections[I];
      EffaceDragage(True, True);
    end;
  end;
end;

procedure TFrameBib.LibereFiltre(Sender: TObject);
Begin
  ThreadFiltre := Nil;
End;

procedure TFrameBib.Filtrer(TreeSource, TreeDest: TTreeView; Chaine: string;
  Vider: boolean);
begin
  While ThreadFiltre <> Nil do
  begin
    ThreadFiltre.Terminate;
    Application.ProcessMessages;
  end;

  if Chaine = '' then
  begin
    TreeDest.visible := False;
    TreeSource.visible := True;
  end
  else
  begin
    TreeDest.visible := False;
    TreeSource.visible := True;
    ThreadFiltre := TThreadFiltre.Create(True);
    ThreadFiltre.FreeOnTerminate := True;
    ThreadFiltre.OnTerminate := LibereFiltre;

    ThreadFiltre.TreeSource := TreeSource;
    ThreadFiltre.TreeDest := TreeDest;
    ThreadFiltre.Chaine := Chaine;
    ThreadFiltre.Vider := Vider;
    ThreadFiltre.Bibliotheque := Bibliotheque;
    ThreadFiltre.Start;
    TreeDest.visible := True;
    TreeSource.visible := False;
  end;
end;

procedure TFrameBib.EditFiltreChange(Sender: TObject);
begin
  Filtrer(Tree, TreeFiltre, EditFiltre.Text, True);
end;

procedure TFrameBib.EditFiltreRightButtonClick(Sender: TObject);
begin
  EditFiltre.Text := '';
end;

procedure TFrameBib.EffaceDragage(Confirmation, Sauvegarde: boolean);
begin
  // Cas des catégories
  if (Dragage.ImageIndex = Bibliotheque.ImageIndexDossier) or
    (Dragage.ImageIndex = Bibliotheque.ImageIndexDossierS) then
  begin
    if Dragage.HasChildren then
      MessageDLGM(MessagesInterface[0], mtWarning, [mbOK], 0)
    else
      Tree.items.Delete(Dragage);
  end
  else if Bibliotheque.EstDuType(Dragage.ImageIndex) and
    (not TComposantBibliotheque(Dragage.Data)
      .ReadOnly or Bibliotheque.ModeDeveloppeur) then
  begin
    if not Confirmation or (MessageDLGM(MessagesInterface[1] + Dragage.Text +
          MessagesInterface[2], mtConfirmation, mbOkCancel, 0) = mrOk) then
      if Bibliotheque.Efface(Dragage.Text) then
        Tree.items.Delete(Dragage);
  end;
  if Sauvegarde then
    Bibliotheque.SauveListe;
  EditFiltre.OnChange(Self);
end;

procedure TFrameBib.FrameEnter(Sender: TObject);
begin
  BarreCategorie.Bibliotheque := Bibliotheque;
  BarreCategorie.Question := MessagesInterface[6];
  TreeFiltre.Images := Tree.Images;
end;

procedure TFrameBib.FrameResize(Sender: TObject);
Var
  Cont: TWinControl;
begin
  Cont := Self;
  repeat
    Cont := Cont.Parent
  until Cont.Parent = nil;
  if Cont is TForm then
    TForm(Cont).OnResize(Self);
end;

procedure TFrameBib.Open1Click(Sender: TObject);
begin
  Tree.Selected := Tree.Selected;
end;

procedure TFrameBib.PopupComponentPopup(Sender: TObject);
begin
  if Tree.visible then
  begin
    Rename1.Enabled := Tree.Selected.ImageIndex <= 1;
    Export1.Enabled := not(Tree.Selected.ImageIndex <= 1);
  end
  else
  begin
    Rename1.Enabled := TreeFiltre.Selected.ImageIndex <= 1;
    Export1.Enabled := not(TreeFiltre.Selected.ImageIndex <= 1);
  end;
  Open1.Enabled := not Rename1.Enabled;
end;

procedure TFrameBib.Rename1Click(Sender: TObject);
var
  NouveauNom: string;
begin
  if Dragage = nil then
    Exit;
  NouveauNom := Dragage.Text;
  if InputQueryM(MessagesInterface[6], MessagesInterface[6], NouveauNom) then
    Bibliotheque.RenommeCategorie(Dragage, NouveauNom);
end;

procedure TFrameBib.Affiche(comp: TComposantBibliotheque; Survol: boolean);
Var
  PrevModif: boolean;
begin
  ModeAffichage := True;
  PrevModif := Modif;
  if Survol then
  begin
    GroupBoxCaracteristiques.Font.Style :=
      GroupBoxCaracteristiques.Font.Style + [fsItalic];
    GroupBoxCaracteristiques.Font.Color := clGray;
  end
  else
  begin
    GroupBoxCaracteristiques.Font.Style :=
      GroupBoxCaracteristiques.Font.Style - [fsItalic];
    GroupBoxCaracteristiques.Font.Color := clWindowText;
  end;
  if comp <> Nil then
  begin
    Bibliotheque.ConvertitCategorie(comp);
    EditNom.Text := comp.Nom;
    MComplement := comp.Complement;
    MemoScrollUp;
    EditSource.Text := comp.Source;
    BarreCategorie.Chemin := comp.categorie;
  end
  else
  begin
    EditNom.Text := '';
    MComplement := '';
    MemoScrollUp;
    EditSource.Text := '';
    BarreCategorie.Chemin := '';
  end;
  Modif := PrevModif;
end;

procedure TFrameBib.TreeAddition(Sender: TObject; Node: TTreeNode);
begin
  TreeModifie := True;
end;

procedure TFrameBib.TreeChange(Sender: TObject; Node: TTreeNode);
var
  Noeud: TTreeNode;
  comp: TComposantBibliotheque;
  Ok: boolean;
begin
  comp := nil;
  Noeud := TTreeView(Sender).Selected;
  if Noeud <> nil then
    comp := Bibliotheque.Get(Noeud.Text);
  if comp <> nil then
  begin
    if Modif then
      Ok := ControleSauve(False)
    else
      Ok := True;
    if not Ok then
    begin
      TTreeView(Sender).Selected := Nil;
      Exit;
    end;
    Affiche(comp, False);
    Store;
    CompStore.Assign(comp);
    Modif := False;
  end;
end;

procedure TFrameBib.TreeDeletion(Sender: TObject; Node: TTreeNode);
begin
  TreeModifie := True;
end;

procedure TFrameBib.TreeDragDrop(Sender, Source: TObject; X, Y: integer);
var
  comp: TComposantBibliotheque;
  Noeud: TTreeNode;
  Cat, DestCat, SourceCat: String;
  NoeudDest, NoeudSource, Cible: TTreeNode;
  Test: THitTests;
  I: integer;
begin
  Test := Tree.GetHitTestInfoAt(X, Y);
  if Test = [htNowhere] then
    Cible := Nil
  else
    Cible := Tree.dropTarget;
  if ((Cible = Nil) or (Cible.ImageIndex = Bibliotheque.ImageIndexDossier) or
      (Cible.ImageIndex = Bibliotheque.ImageIndexDossierS)) then
  begin
    // Construit le nom de la categorie de destination
    NoeudDest := Cible;
    DestCat := '';
    while NoeudDest <> Nil do
    begin
      DestCat := NoeudDest.Text + SeparateurChemin + DestCat;
      NoeudDest := NoeudDest.Parent;
    end;

    if Dragage.ImageIndex = Bibliotheque.ImageIndexDossier then
    begin
      // Construit le nom de la categorie source
      NoeudSource := Dragage.Parent;
      SourceCat := '';
      while NoeudSource <> Nil do
      begin
        SourceCat := NoeudSource.Text + SeparateurChemin + SourceCat;
        NoeudSource := NoeudSource.Parent;
      end;
      Delete(SourceCat, Length(SourceCat), 1);

      Noeud := Dragage.getFirstChild;
      while Noeud <> Nil do
      begin
        if Noeud.ImageIndex <> Bibliotheque.ImageIndexDossier then
        begin
          if not TComposantBibliotheque(Noeud.Data).ReadOnly then
          begin
            Cat := TComposantBibliotheque(Noeud.Data).categorie;
            Delete(Cat, Pos(SourceCat, Cat), Length(SourceCat) + 1);
            TComposantBibliotheque(Noeud.Data).categorie := DestCat + Cat;
          end;
        end;
        Noeud := Noeud.getNextSibling;
      end;
      Bibliotheque.PoseTousDansArbre(Tree, True, False);
    end
    else if (Cible <> Nil) then
    begin
      for I := Tree.SelectionCount - 1 downto 0 do
      begin
        Dragage := Tree.Selections[I];
        comp := Bibliotheque.Get(Dragage.Text);
        if not comp.ReadOnly then
        begin
          comp.categorie := Bibliotheque.GetNomAscendants(Cible);
          Tree.items.Delete(Dragage);
          Tree.alphaSort;
          Tree.FullCollapse;
          Bibliotheque.SauveListe;
        end;
      end;
      Bibliotheque.PoseTousDansArbre(Tree, True, False);
    end;
  end;
end;

procedure TFrameBib.TreeDragOver(Sender, Source: TObject; X, Y: integer;
  State: TDragState; var Accept: boolean);
begin
  if (Source = Tree) and (Dragage <> nil) then
    Accept := Bibliotheque.EstDuType(Dragage.ImageIndex) or
      (Dragage.ImageIndex = Bibliotheque.ImageIndexDossier)
  else
    Accept := False;
end;

procedure TFrameBib.TreeEdited(Sender: TObject; Node: TTreeNode; var S: string);
begin
  TreeModifie := True;
end;

procedure TFrameBib.HintFiltre(Arbre: TTreeView; Noeud: TTreeNode);
var
  Chaine: string;
begin
  if Noeud <> nil then
  begin
    Chaine := StringReplace(TComposantBibliotheque(Noeud.Data).categorie, '|',
      '/', [rfReplaceAll]);
    Chaine := Chaine + CR + StringReplace(TComposantBibliotheque(Noeud.Data)
        .Complement, '|', CR, [rfReplaceAll]);
    while (Length(Chaine) > 1) and (Chaine[Length(Chaine)] = CR[2]) do
      Delete(Chaine, Length(Chaine) - 1, 2);
    if NomPrecedent <> TComposantBibliotheque(Noeud.Data).Nom then
    begin
      Arbre.Hint := Chaine;
      Application.ActivateHint(Mouse.CursorPos);
      NomPrecedent := TComposantBibliotheque(Noeud.Data).Nom;
    end;
  end
  else if Arbre.Hint <> '' then
  begin
    Arbre.Hint := '';
    Application.ActivateHint(Mouse.CursorPos);
    NomPrecedent := '';
  end;
end;

procedure TFrameBib.TreeFiltreMouseLeave(Sender: TObject);
begin
  Affiche(CompStore, False);
end;

procedure TFrameBib.TreeFiltreMouseMove(Sender: TObject; Shift: TShiftState;
  X, Y: integer);
Var
  Noeud: TTreeNode;
  comp: TComposantBibliotheque;
begin
  comp := Nil;
  Noeud := TreeFiltre.GetNodeAt(X, Y);
  if Noeud <> Nil then
    comp := TComposantBibliotheque(Noeud.Data);
  AffichageDirect(comp, Noeud = TreeFiltre.Selected);
end;

procedure TFrameBib.TreeKeyUp(Sender: TObject; var Key: word;
  Shift: TShiftState);
begin
  if (Key = 46) and (TTreeView(Sender).Selected <> nil) then
    EffaceDragage(True, True)
  else
    TTreeView(Sender).OnChange(Self, nil);
end;

procedure TFrameBib.TreeMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: integer);
var
  I: integer;
begin
  Dragage := TTreeView(Sender).GetNodeAt(X, Y);
  if (Sender = TreeFiltre) and (Dragage <> nil) then
  begin
    for I := 0 to Tree.items.Count - 1 do
      if Dragage.Text = Tree.items[I].Text then
      begin
        Dragage := Tree.items[I];
        Exit;
      end;
  end;
end;

procedure TFrameBib.TreeMouseLeave(Sender: TObject);
begin
  Affiche(CompStore, False);
end;

procedure TFrameBib.HintNormal(Noeud: TTreeNode; Arbre: TTreeView;
  Biblio: TBibliotheque);
var
  Chaine: string;
begin
  if (Noeud <> nil) and (Noeud.ImageIndex <> Biblio.ImageIndexDossier) then
  begin
    Chaine := StringReplace(TComposantBibliotheque(Noeud.Data).Complement, '|',
      CR, [rfReplaceAll]);
    while (Length(Chaine) > 1) and (Chaine[Length(Chaine)] = CR[2]) do
      Delete(Chaine, Length(Chaine) - 1, 2);
    if NomPrecedent <> TComposantBibliotheque(Noeud.Data).Nom then
    begin
      Arbre.Hint := Chaine;
      Application.ActivateHint(Mouse.CursorPos);
      NomPrecedent := TComposantBibliotheque(Noeud.Data).Nom;
    end;
  end
  else if Arbre.Hint <> '' then
  begin
    Arbre.Hint := '';
    Application.ActivateHint(Mouse.CursorPos);
    NomPrecedent := '';
  end;
end;

procedure TFrameBib.AffichageDirect(comp: TComposantBibliotheque;
  EstSelection: boolean);
begin
  if (comp = nil) then
  begin
    if (CompStore = nil) and (CompAffiche <> Nil) then
    begin
      Affiche(CompStore, False);
      CompAffiche := CompStore;
    end
    else if (CompAffiche <> Nil) and not CompStore.ValeursIdentiques
      (CompAffiche) then
    begin
      Affiche(CompStore, False);
      CompAffiche := CompStore;
    end
  end
  else
  begin
    if EstSelection and (CompAffiche <> comp) then
    begin
      Affiche(comp, False);
      CompAffiche := comp;
    end
    else if (comp <> CompAffiche) then
    begin
      Affiche(comp, True);
      CompAffiche := comp;
    end;
  end;
  CompAEteAffiche := True;
end;

procedure TFrameBib.TreeMouseMove(Sender: TObject; Shift: TShiftState;
  X, Y: integer);
Var
  Noeud: TTreeNode;
  comp: TComposantBibliotheque;
begin
  CompAEteAffiche := False;
  HitTest := TTreeView(Sender).GetHitTestInfoAt(X, Y);
  if not(htOnItem in HitTest) then
  begin
    AffichageDirect(CompStore, True);
    CompAffiche := CompStore;
    Exit;
  end;
  Noeud := TTreeView(Sender).GetNodeAt(X, Y);
  if Noeud <> Nil then
    comp := TComposantBibliotheque(Noeud.Data)
  else
    comp := Nil;
  if comp <> Nil then
    AffichageDirect(comp, Noeud = TTreeView(Sender).Selected);
end;

procedure TFrameBib.BarreCategorieMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: integer);
begin
  ComboCategorieChange(Self);
end;

procedure TFrameBib.BitBtnNewClick(Sender: TObject);
begin
  Tree.Selected := Nil;
  EditNom.Text := '';
  MComplement := '';
  EditSource.Text := '';
  BarreCategorie.Chemin := '';
  Modif := False;
  Store;
end;

procedure TFrameBib.BitBtnsaveClick(Sender: TObject);
Var
  Noeud: TTreeNode;
begin
  ControleSauve(True, Noeud);
  EditFiltre.Text := '';
  if (Noeud <> Nil) and (Noeud <> NoeudNon) then
    Tree.Selected := Noeud;
  Tree.alphaSort;
end;

procedure TFrameBib.BitBtnsaveMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: integer);
begin
  SauveReadOnly := (Shift * [ssShift, ssCtrl] = [ssShift, ssCtrl]);
end;

procedure TFrameBib.ComboCategorieChange(Sender: TObject);
begin
  if Not ModeAffichage then
    Modif := True;
  Store;
end;

function TFrameBib.ControleSauve(Forcee: boolean): boolean;
Var
  N: TTreeNode;
begin
  Result := ControleSauve(Forcee, N);
end;

function TFrameBib.ControleSauve(Forcee: boolean;
  var Noeud: TTreeNode): boolean;
var
  reponse: word;
  comp: TComposantBibliotheque;
  Silencieux: boolean;
begin
  Noeud := NoeudNon;
  if not ModificationAutorise then
  begin
    MessageDLGM(MessagesInterface[7], mtWarning, [mbOK], 0);
    Exit(False);
  end;
  if Tree.visible then
    FormParent.FocusControl(Tree);
  if not Forcee then
    reponse := MessageDLGM(MessagesInterface[5] + CompStore.Nom + '''',
      mtWarning, [mbCancel, mbNo, mbYes], 0)
  else
    reponse := mrYes;
  if reponse = mrCancel then
    Exit(False);
  if (reponse = mrYes) then
    if Controle then
    begin
      comp := CreerComposantBiblio;
      comp.ReadOnly := CtrlDown and ShiftDown;
      Silencieux := False;
      if Bibliotheque.Ajoute(comp, Silencieux) then
      begin
        Modif := False;
        Noeud := Bibliotheque.PoseDansArbre(comp, Tree, True, True);
      end
      else
        comp.Free;
    end
    else
      Exit(False);
  Result := True;
  Bibliotheque.SauveListe;
end;

{ function TFrameBib.CreerComposantBiblio: TComposantBibliotheque;
  begin
  Result := nil;
  end; }

function TFrameBib.Controle: boolean;
begin
  if EstNumerique(EditNom.Text) then
    EditNom.Text := '_' + EditNom.Text;
  ErreurSaisie := 0;
  if BarreCategorie.Chemin = '' then
  begin
    FormParent.FocusControl(BarreCategorie);
    ErreurSaisie := 1;
  end
  else if HorsLimite(EditNom, 1, 256, 'C') then
    ErreurSaisie := 2;

  if ErreurSaisie <> 0 then
  begin
    MessageDLGM(MessagesInterface[8 + ErreurSaisie], mtWarning, [mbOK], 0);
    Result := False;
  end
  else
    Result := True;
end;

function TFrameBib.HorsLimite(Combo: TCustomCombo): boolean;
begin
  if (ErreurSaisie = 0) and (AOuvrir = nil) and (Combo.ItemIndex = -1)
    and Combo.visible then
  begin
    FormParent.FocusControl(Combo);
    AOuvrir := Combo;
    Result := True
  end
  else
    Result := False;
end;

function TFrameBib.HorsLimite(ChampEdit: TEdit; mini, maxi: double;
  T: char): boolean;
begin
  if ErreurSaisie <> 0 then
    Exit(False);
  if T = 'R' then
  begin
    if ControlNum(ConvertPoint(ChampEdit.Text), mini, maxi, 'R') <> ConvertPoint
      (ChampEdit.Text) then
    begin
      FormParent.FocusControl(ChampEdit);
      Exit(True);
    end;
  end
  else if (T = 'C') or (T = 'E') then
  begin
    if ControlNum(ChampEdit.Text, mini, maxi, T) <> ChampEdit.Text then
    begin
      FormParent.FocusControl(ChampEdit);
      Exit(True);
    end;
  end;

  Result := False;
end;

function TFrameBib.GetComplement: string;
var
  I: integer;
begin
  Result := '';
  for I := 0 to MemoComplement.Lines.Count - 1 do
    Result := Result + MemoComplement.Lines[I] + '|';
end;

procedure TFrameBib.SetComplement(Chaine: string);
begin
  MemoComplement.Lines.Clear;
  while Chaine <> '' do
    MemoComplement.Lines.Add(GetItemPipe(Chaine));
end;

procedure TFrameBib.Splitter3Moved(Sender: TObject);
begin
  resize;
end;

procedure TFrameBib.MemoScrollUp;
var
  ScrollMessage: TWMVScroll;
begin
  ScrollMessage.Msg := WM_VScroll;
  ScrollMessage.ScrollCode := SB_TOP;
  MemoComplement.Dispatch(ScrollMessage);
end;

procedure TFrameBib.AfficheImage(Image: TImage; NomImage: String;
  PleineLargeur: boolean);
begin
  RetailleImage(Image, NomImage, PleineLargeur);
end;

function TFrameBib.GetImage1Dossier(Noeud: TTreeNode): string;
var
  Niveau: integer;
  comp: TComposantBibliotheque;
begin
  Result := '';
  if Noeud <> Nil then
  begin
    Niveau := Noeud.level;
    comp := TComposantBibliotheque(Noeud.Data);
    if comp = nil then
    begin
      Noeud := Noeud.getFirstChild;
      while (Noeud <> nil) and (Noeud.level <> Niveau) and (Result <> '_') do
      begin
        comp := TComposantBibliotheque(Noeud.Data);
        if comp <> Nil then
        begin
          if (Result = '') then
            Result := comp.Image1
          else if Result <> comp.Image1 then
            Result := '_';
        end;
        Noeud := Noeud.GetNext;
      end;
    end;
  end;
end;

function TFrameBib.GetImage2Dossier(Noeud: TTreeNode): string;
var
  Niveau: integer;
  comp: TComposantBibliotheque;
begin
  Result := '';
  if Noeud <> Nil then
  begin
    Niveau := Noeud.level;
    comp := TComposantBibliotheque(Noeud.Data);
    if comp = nil then
    begin
      Noeud := Noeud.getFirstChild;
      while (Noeud <> nil) and (Noeud.level <> Niveau) and (Result <> '_') do
      begin
        comp := TComposantBibliotheque(Noeud.Data);
        if comp <> Nil then
        begin
          if (Result = '') then
            Result := comp.Image2
          else if Result <> comp.Image2 then
            Result := '_';
        end;
        Noeud := Noeud.GetNext;
      end;
    end;
  end;
end;

end.
