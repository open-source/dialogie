unit GestionBib;

interface

uses
  Classes,
  ComposantBibliotheque,
  ComCtrls,
  GestionSauvegarde,
  CreerSauvegarde;

const
  EcartStandard = 100000;
  TagVersion = 'version';

type
  TBibliotheque = class
  Const
    ImageIndexDossier = 0;
    ImageIndexDossierS = 1;
  private
    procedure GetDescendants(Noeud: TTreeNode; Liste: TList);
  protected
    hasToSave : boolean;
    CheminBiblioStandard, CheminBiblioNonStandard: string;
    Table: TStringList;
    function CreationPourImport: TComposantBibliotheque; virtual; abstract;
    procedure ChargeListe;//virtual;
    procedure ChargeListeAncien(standardLib : boolean); virtual; abstract;
  public
    IdMax: integer;
    TypeComposant: string;
    ImageIndex, ImageIndexRO, ImageBrute : integer;
    MessagesInterface: TStringList;
    constructor Create(Standard, NonStandard: string);
    destructor Destroy; override;
    function Get(Nom: string): TComposantBibliotheque; overload;
    function Get(Identificateur: integer): TComposantBibliotheque; overload;

    function Ajoute(Composant: TComposantBibliotheque;
      var ModeSilencieux: boolean): boolean;
    function Efface(Nom: string): boolean;
    function PoseDansArbre(Composant: TComposantBibliotheque; Arbre: TTreeView;
      AvecCategorie, AvecControleExistant: boolean): TTreeNode; virtual;
    procedure PoseTousDansArbre(Arbre: TTreeView; AvecCategorie,
      AvecControleExistant: boolean);overload;
    procedure PoseTousDansArbre(Arbre: TTreeView; AvecCategorie,
      AvecControleExistant, videArbre: boolean);overload;
    procedure SelectionneNoeud(Arbre: TTreeView; PreSelection: string);
    procedure Exporte(Liste: TCreationSauvegarde);
    procedure Importe(Liste: TSauvegarde);
    procedure GetListeCategorie(Liste: TStrings);
    procedure AjouteCategorie(var Nom: string; ListeCategorie: TStrings;
      Arbre: TTreeView);
    function Count: integer;
    function Existe(Nom: string): boolean;
    function EstDuType(Index: integer): boolean; virtual;
    procedure ConvertitCategorie(Composant: TComposantBibliotheque);
    function ExisteIdentique(comp: TComposantBibliotheque): boolean;
    procedure RenommeCategorie(Noeud: TTreeNode; NouveauNom: String);
    function GetNomAscendants(Noeud: TTreeNode): String;
    procedure SauveListe(filePathStd : string = '' ; filePathUser : string = '');
    function GetNomFichier(Standard: boolean): string;
    procedure CompteStandard(var NbStandard, NbNonStandard: integer);
    function GetListe : TStringList;
  end;

implementation

uses
  System.UITypes,
  Winapi.Windows,
  SysUtils,
  Dialogs,
  Controls,
  IzUtilitaires,
  DialogsInternational,
  IzApp;

  function TBibliotheque.GetListe : TStringList;
var
  i: Integer;
begin
  result := TStringList.Create;
  for i := 0 to Table.count - 1 do
     result.Add(Table[i]);
end;

procedure TBibliotheque.ChargeListe;
var
  NomFichier: string;
  Liste: TSauvegarde;
  Version, nb: integer;
  i,j: integer;
  comp: TComposantBibliotheque;
begin
  hasToSave := false;
 Liste := TSauvegarde.Create;
  NomFichier := CheminBiblioStandard + GetNomFichier(True);
  if FileExists(NomFichier) then
  begin
    Liste.LoadFromFile(NomFichier);
    Version := Liste.GetLigneEntier;
    nb := Liste.GetLigneEntier;
    for i := 0 to nb - 1 do
    begin
      comp := CreationPourImport;
      comp.Charge(Liste, Version);
      if comp.nom <> '' then
        Table.AddObject(comp.nom, comp)
      else
        comp.Free;
      if (comp.Identificateur > IdMax) and (comp.ReadOnly) then
        IdMax := comp.Identificateur
      else if (comp.Identificateur > IdMax) then
        IdMax := comp.Identificateur - EcartStandard;
    end;
  end
  else
  begin
    ChargeListeAncien(true);
  end;

  NomFichier := CheminBiblioNonStandard + GetNomFichier(False);
  if FileExists(NomFichier) then
  begin
    Liste.LoadFromFile(NomFichier);
    Version := Liste.GetLigneEntier;
    nb := Liste.GetLigneEntier;
    for i := 0 to nb - 1 do
    begin
      comp := CreationPourImport;
      comp.Charge(Liste, Version);
      if comp.nom <> '' then
        Table.AddObject(comp.nom, comp)
      else
        comp.Free;
      if (comp.Identificateur > IdMax) and (comp.ReadOnly) then
        IdMax := comp.Identificateur
      else if (comp.Identificateur > IdMax) then
        IdMax := comp.Identificateur - EcartStandard;
    end;
  end
  else
  begin
    ChargeListeAncien(false);
  end;

  for i := 0 to Table.count - 2 do
    for j := i+1 to Table.count - 1 do
      if (TComposantBibliotheque(Table.Objects[i]).Identificateur =
         TComposantBibliotheque(Table.Objects[j]).Identificateur) and
           not TComposantBibliotheque(Table.Objects[j]).ReadOnly then
      begin
        inc(IdMax);
        TComposantBibliotheque(Table.Objects[j]).Identificateur :=
          IdMax + EcartStandard;
      end;

  Liste.Free;
  if (hasToSave) then
  begin
    SauveListe;
  end;
end;

procedure TBibliotheque.CompteStandard(var NbStandard, NbNonStandard: integer);
var
  i: integer;
begin
  NbStandard := 0;
  NbNonStandard := 0;
  for i := 0 to Table.Count - 1 do
    if TComposantBibliotheque(Table.Objects[i]).readonly then
      inc(NbStandard)
    else
      inc(NbNonStandard);
end;

Function TBibliotheque.GetNomFichier(Standard: boolean): string;
begin
  if Standard then
    result := 'Biblio' + TypeComposant + 'Standard.txt'
  else
    result := 'Biblio' + TypeComposant + 'NonStandard.txt'
end;

procedure TBibliotheque.SauveListe(filePathStd : string = '' ; filePathUser : string = '');
var
  Liste: TCreationSauvegarde;
  NbStandard, NbNonStandard: integer;
  i: integer;
  Erreur: Boolean;
  compBib : TComposantBibliotheque;
begin
  CompteStandard(NbStandard, NbNonStandard);

  if filePathStd = '' then
    filePathStd := CheminBiblioStandard;
  if filePathUser = '' then
    filePathUser := CheminBiblioNonStandard;


  if (isDevMode) or (hasToSave) or (TApp.isMaster) then
  begin
    hasToSave := false;
    Liste := TCreationSauvegarde.Create;
    if Table.Count > 0 then
      Liste.Add(TComposantBibliotheque(Table.Objects[0]).idVersion)
    else
      Liste.Add(0);


    Liste.Add(NbStandard);
    for i := 0 to Table.Count - 1 do
    begin
      compBib := TComposantBibliotheque(Table.Objects[i]);
      //pour pouvoir sauver tout en tant que biblio standard dans la biblio non standard
      if compBib.readOnly then
        compBib.Sauve(Liste)
    end;
    Liste.SaveToFile(filePathStd + GetNomFichier(True), TEncoding.Unicode);
    Liste.Free;
  end;

  Erreur := False;
  Try
  Liste := TCreationSauvegarde.Create;
  if Table.Count > 0 then
    Liste.Add(TComposantBibliotheque(Table.Objects[0]).idVersion)
  else
    Liste.Add(0);
  Liste.Add(NbNonStandard);
  for i := 0 to Table.Count - 1 do
  begin
    compBib := TComposantBibliotheque(Table.Objects[i]);
    if not compBib.readOnly then
      compBib.Sauve(Liste)
  end;
  Liste.SaveToFile(filePathUser + GetNomFichier(False)+'.temp',
    TEncoding.Unicode);
  Except
    MessageDLGM('Impossible d''enregistrer la biblioth�que non standard ' + filePathUser + GetNomFichier(False),mtError,[mbOk],0);
    Erreur := True;
  End;
  if Not Erreur then
  begin
    DeleteFile(filePathUser + GetNomFichier(False));
    RenameFile(filePathUser + GetNomFichier(False)+'.temp',
      filePathUser + GetNomFichier(False));
  end;

  Liste.Free;
end;

function TBibliotheque.ExisteIdentique(comp: TComposantBibliotheque): boolean;
var
  CompBiblio: TComposantBibliotheque;
begin
  CompBiblio := Get(comp.nom);
  if CompBiblio <> nil then
    result := CompBiblio.ValeursIdentiques(comp)
  else
    result := False;
end;

function TBibliotheque.EstDuType(Index: integer): boolean;
begin
  result := (Index = ImageIndex) or (Index = ImageIndexRO);
end;

procedure TBibliotheque.ConvertitCategorie(Composant: TComposantBibliotheque);
var
  Entier, Erreur: integer;
  dbl : double;
  Liste: TStringList;
begin
  Val(Composant.categorie, Entier, Erreur);
  if Erreur = 0 then
  begin
    Liste := TStringList.Create;
    GetListeCategorie(Liste);
    if Entier < Liste.Count then
      Composant.categorie := Liste[Entier]
    else
      Composant.categorie := 'C' + Composant.categorie;
  end
  else
  begin
    Val(Composant.categorie, dbl, Erreur);
    if Erreur = 0 then
    begin
      showMessage('Erreur de conversion de cat�gorie : valeur ind�finie ' + composant.categorie);
    end;
  end;
end;

procedure TBibliotheque.SelectionneNoeud(Arbre: TTreeView;
  PreSelection: string);
var
  Noeud: TTreeNode;
begin
  Arbre.Selected := nil;
  Noeud := Arbre.Items.GetFirstNode;
  PreSelection := UpperCase(PreSelection);
  while Noeud <> nil do
  begin
    if UpperCase(Noeud.Text) = PreSelection then
      Arbre.Selected := Noeud;
    Noeud := Noeud.GetNext;
  end;
end;

procedure TBibliotheque.AjouteCategorie(var Nom: string;
  ListeCategorie: TStrings; Arbre: TTreeView);
var
  Courant, Pere: TTreeNode;
  Cat, SousCat: string;
begin
  if EstNumerique(Nom) then
    Nom := 'C' + Nom;
  if (ListeCategorie <> nil) and (ListeCategorie.IndexOf(Nom) = -1) then
    ListeCategorie.Add(Nom);
  Courant := Arbre.Items.GetFirstNode;
  Cat := Nom;
  Pere := Nil;
  while Cat <> '' do
  begin
    SousCat := getItemPipe(Cat);
    while (Courant <> nil) and (UpperCase(Courant.Text) <> UpperCase(SousCat)) do
      Courant := Courant.GetNextSibling;
    if Courant = nil then
    begin
      Courant := Arbre.Items.AddChild(Pere, SousCat);
      Courant.ImageIndex := ImageIndexDossier;
      Courant.SelectedIndex := ImageIndexDossierS;
    end;
    Pere := Courant;
    Courant := Courant.getFirstChild;
  end;
end;

function TBibliotheque.Existe(Nom: string): boolean;
begin
  result := Table.IndexOf(Nom) <> -1;
end;

constructor TBibliotheque.Create(Standard, NonStandard: string);
begin
  inherited Create;
  hasToSave := false;
  Table := TStringList.Create;
  MessagesInterface := TStringList.Create;

  CheminBiblioStandard := Standard;
  CheminBiblioNonStandard := NonStandard;
end;

destructor TBibliotheque.Destroy;
var
  i: integer;
begin
  MessagesInterface.Free;
  for i := 0 to Table.Count - 1 do
    TComposantBibliotheque(Table.Objects[i]).Free;
  Table.Free;
  inherited Destroy;
end;

function TBibliotheque.Get(Nom: string): TComposantBibliotheque;
var
  Index: integer;
begin
  Index := Table.IndexOf(Nom);
  if index <> -1 then
    result := TComposantBibliotheque(Table.Objects[index])
  else
    result := nil;
end;

function TBibliotheque.Get(Identificateur: integer): TComposantBibliotheque;
var
  i: integer;
begin
  result := nil;
  for i := 0 to Table.Count - 1 do
  begin
    if TComposantBibliotheque(Table.Objects[i])
      .Identificateur = Identificateur then
    begin
      result := TComposantBibliotheque(Table.Objects[i]);
      Break;
    end;
  end;
end;

function TBibliotheque.Ajoute(Composant: TComposantBibliotheque;
  var ModeSilencieux: boolean): boolean;
var
  Index: integer;
  Reponse: word;
begin
  result := True;
  ConvertitCategorie(Composant);
  Index := Table.IndexOf(Composant.nom);
  if Index = -1 then
  begin
    // Il n'existe pas alors, on le cr�e et �a roule
    Table.AddObject(Composant.nom, Composant);
    inc(IdMax);
    if Composant.ReadOnly then
      Composant.Identificateur := IdMax
    else
      Composant.Identificateur := IdMax + EcartStandard;
  end
  else
  begin
    if not Composant.ReadOnly and TComposantBibliotheque(Table.Objects[Index]).ReadOnly and not ModeSilencieux then
    begin
      // On ne remplace pas une menuiserie ReadOnly
      MessageDlgM(MessagesInterface[0], mtWarning, [mbOK], 0);
      Exit(False);
    end
    else
    begin
      // on demande confirmation s'il existe d�ja
      if not ModeSilencieux then
      begin
        Reponse := MessageDlgM(MessagesInterface[1] + Composant.nom +
            MessagesInterface[2], mtConfirmation, [mbOK, mbCancel, mbAll], 0);
        if (Reponse = mrCancel) then
          Exit(False)
        else if Reponse = mrAll then
          ModeSilencieux := True;
      end;
      // On r�cup�re l'identificateur et on remplace l'objet attach�.
      Composant.Identificateur := TComposantBibliotheque(Table.Objects[Index])
        .Identificateur;
      Table.Objects[Index] := Composant;
    end;
  end;
end;

function TBibliotheque.Efface(Nom: string): boolean;
var
  Index: integer;
  Ok: boolean;
begin
  Ok := True;
  Index := Table.IndexOf(Nom);
  Table.Delete(Index);
  result := Ok;
end;

function TBibliotheque.PoseDansArbre(Composant: TComposantBibliotheque;
  Arbre: TTreeView; AvecCategorie, AvecControleExistant: boolean): TTreeNode;
var
  Noeud, NoeudPrev, Trouve, categorie: TTreeNode;
  SousCat, Cat: string;
begin
  // Si AvecCategorie, alors, chercher la categorie
  Noeud := Arbre.Items.GetFirstNode;
  categorie := nil;
  NoeudPrev := nil;
  if AvecCategorie then
  begin
    Cat := Composant.categorie;
    while Cat <> '' do
    begin
      categorie := nil;
      SousCat := getItemPipe(Cat);
      while Noeud <> nil do
      begin
        if UpperCase(Noeud.Text) = UpperCase(SousCat) then
          categorie := Noeud;
        Noeud := Noeud.GetNextSibling;
      end;
      if categorie = nil then
        categorie := Arbre.Items.AddChild(NoeudPrev, SousCat);
      categorie.ImageIndex := 0;
      categorie.SelectedIndex := 1;
      NoeudPrev := categorie;
      Noeud := categorie.getFirstChild;
    end;
  end;
  // Cherche si le noeud existe d�ja
  Trouve := nil;
  if AvecControleExistant then
  begin
    Noeud := Arbre.Items.GetFirstNode;
    while Noeud <> nil do
    begin
      if UpperCase(Noeud.Text) = UpperCase(Composant.nom) then
      begin
        Trouve := Noeud;
        Noeud.Text := Composant.nom
      end;
      Noeud := Noeud.GetNext;
    end;
  end;
  // S'il existe d�ja, il faut v�rifier s'il est dans la bonne categorie
  // S'il ne l'est pas, il faut l'effacer
  if (Trouve <> nil) and (Trouve.Parent <> nil) and AvecCategorie then
  begin
    if (UpperCase(Composant.categorie) <> UpperCase(Trouve.Parent.Text)) then
    begin
      Arbre.Items.Delete(Trouve);
      Trouve := nil;
    end;
  end;

  // Si non, alors il faut le creer.
  if Trouve = nil then
  begin
    if AvecCategorie then
      Trouve := Arbre.Items.AddChildObject(categorie, Composant.nom, Composant)
    else
      Trouve := Arbre.Items.AddObject(nil, Composant.nom, Composant);
  end
  else
    Trouve.Data := Composant;

  if Composant.ReadOnly then
    Trouve.ImageIndex := ImageIndexRO
  else
    Trouve.ImageIndex := ImageIndex;
  Trouve.SelectedIndex := Trouve.ImageIndex + 1;
  result := Trouve;
end;

procedure TBibliotheque.PoseTousDansArbre(Arbre: TTreeView;
  AvecCategorie, AvecControleExistant: boolean);
begin
  PoseTousDansArbre(Arbre, AvecCategorie, AvecControleExistant, true);
end;

procedure TBibliotheque.PoseTousDansArbre(Arbre: TTreeView;
  AvecCategorie, AvecControleExistant, videArbre: boolean);
var
  i: integer;
begin
  if videArbre then
    Arbre.Items.Clear;
  for i := 0 to Table.Count - 1 do
    PoseDansArbre(TComposantBibliotheque(Table.Objects[i]), Arbre,
      AvecCategorie, AvecControleExistant);
  Arbre.AlphaSort;
end;

procedure TBibliotheque.Importe(Liste: TSauvegarde);
var
  NbComposants: integer;
  Composant: TComposantBibliotheque;
  i: integer;
  Silencieux: boolean;
  Version: integer;
begin
  if Liste.Count = 0 then
    Exit;
  NbComposants := Liste.GetLigneEntier;
  for i := 0 to NbComposants - 1 do
  begin
    if Liste.OuvreSection(TagVersion) then
    begin
      Version := Liste.GetLigneEntier;
      Liste.FermeSection;
    end
    else
      Version := 1;
    Composant := CreationPourImport;
    Composant.Charge(Liste, Version);
    if not Existe(Composant.nom) then
    begin
      Silencieux := True;
      Ajoute(Composant, Silencieux);
    end
    else
      Composant.Free;
  end;
end;

procedure TBibliotheque.Exporte(Liste: TCreationSauvegarde);
var
  i, indice, Compte: integer;
  Composant: TComposantBibliotheque;
begin
  Liste.Clear;
  Liste.OuvreTag(TypeComposant);
  Liste.Add('0');
  indice := Liste.Count - 1;
  Compte := 0;
  for i := 0 to Table.Count - 1 do
  begin
    Composant := TComposantBibliotheque(Table.Objects[i]);
    if not Composant.ReadOnly then
    begin
      Liste.OuvreTag(TagVersion);
      Liste.Add(Composant.idVersion);
      Liste.FermeTag(TagVersion);
      Composant.Sauve(Liste);
      inc(Compte);
    end;
  end;
  Liste[indice] := IntToStr(Compte);
  Liste.FermeTag(TypeComposant);
end;

procedure TBibliotheque.GetListeCategorie(Liste: TStrings);
var
  i: integer;
begin
  Liste.Clear;
  for i := 0 to Table.Count - 1 do
    if Liste.IndexOf(TComposantBibliotheque(Table.Objects[i]).categorie)
      = -1 then
      Liste.Add(TComposantBibliotheque(Table.Objects[i]).categorie);
end;

function TBibliotheque.Count: integer;
begin
  result := Table.Count;
end;

procedure TBibliotheque.RenommeCategorie(Noeud: TTreeNode; NouveauNom: String);
Var
  ListeDescendants: TList;
  i: integer;
Begin
  Noeud.Text := NouveauNom;
  ListeDescendants := TList.Create;
  try
    GetDescendants(Noeud, ListeDescendants);
    for i := 0 to ListeDescendants.Count - 1 do
      TComposantBibliotheque(TTreeNode(ListeDescendants[i]).Data).categorie :=
        GetNomAscendants(TTreeNode(ListeDescendants[i]).Parent);
  finally
    ListeDescendants.Free;
  end;
End;

procedure TBibliotheque.GetDescendants(Noeud: TTreeNode; Liste: TList);
Begin
  Noeud := Noeud.getFirstChild;
  while Noeud <> Nil do
  begin
    if Noeud.Data = nil then
      GetDescendants(Noeud, Liste)
    else
      Liste.Add(Noeud);
    Noeud := Noeud.GetNextSibling;
  end;
End;

function TBibliotheque.GetNomAscendants(Noeud: TTreeNode): String;
begin
  result := '';
  while Noeud <> Nil do
  begin
    result := '|' + Noeud.Text + result;
    Noeud := Noeud.Parent;
  end;
  Delete(result, 1, 1);
end;

end.
