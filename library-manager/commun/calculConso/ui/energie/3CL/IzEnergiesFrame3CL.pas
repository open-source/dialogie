unit IzEnergiesFrame3CL;

interface

uses
	Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
	Dialogs, IzEnergieAbonnementFrame3CL, contnrs, IzEnergies3CL, IzTypeEnergie;

type
	TEnergiesFrame3CL = class(TFrame)
	private
		innerEnergies : TTypeEnergie;

		class var nextId : integer;

	protected
		procedure showFromLibrary();
		function getEnergies() : TTypeEnergie;
		procedure setEnergies(aType : TTypeEnergie);

		class function getNewName() : string;
		
	public
		constructor Create(AOwner: TComponent; aParent : TWinControl);reintroduce;overload;virtual;
		constructor Create(AOwner: TComponent; aParent : TWinControl; aType : TTypeEnergie);reintroduce;overload;virtual;
		destructor Destroy();override;

		procedure removeAll();
		procedure removeFrames();
		procedure refresh();
		procedure initFromLibrary(aType : TTypeEnergie);

		property energies : TTypeEnergie read getEnergies write setEnergies;
	end;

implementation

uses
  IzTypeObjectPersist,
	IzBiblioSystem;

{$R *.dfm}


	constructor TEnergiesFrame3CL.Create(AOwner: TComponent; aParent : TWinControl);
	begin
		inherited create(AOwner);
		if (aParent <> nil) then
		begin
			parent := aParent;
		end;

(*
		if (innerEnergies = nil) then
		begin
			innerEnergies := TTypeEnergie.Create;
		end
		else
		begin
			innerEnergies.removeAll;
		end;
//appel plus g�n�rique d'initialisation depuis le biblioth�que
  	initFromLibrary(innerEnergies);
*)
	end;


	constructor TEnergiesFrame3CL.Create(AOwner: TComponent; aParent : TWinControl; aType : TTypeEnergie);
	begin
		if aType <> nil then
			innerEnergies := aType
    else
      innerEnergies := nil;
		create(AOwner, aParent);
	end;


	procedure TEnergiesFrame3CL.showFromLibrary();
	var
		frame : TEnergieAbonnementFrame3CL;
		energie : TIzEnergie;
		nb: Integer;
		energieName : string;
	begin

    if innerEnergies = nil then
      exit;

		nb := 0;
		energieName := getBiblioSystem().biblioEnergieTarif.getEnergie(nb);
		while energieName <> '' do
		begin
			frame := TEnergieAbonnementFrame3CL.Create(self, self);
			frame.name := Frame.ClassName + intToStr(nb);
			if nb mod 2 = 0 then
			begin
				frame.Left := 0;
			end
			else
			begin
				frame.Left := frame.Width;
			end;
			frame.top := frame.Height * (nb div 2);
			energie := TIzEnergie.Create;
			energie.nomEnergie := energieName;
			innerEnergies.setEnergie(energie);

			frame.energie := energie;
      frame.initToDefault();
			inc (nb);
			energieName := getBiblioSystem().biblioEnergieTarif.getEnergie(nb);
		end;
	end;


	destructor TEnergiesFrame3CL.Destroy();
	begin
//		innerEnergies.Free;
		inherited destroy;
	end;

	function TEnergiesFrame3CL.getEnergies() : TTypeEnergie;
	begin
		getEnergies := innerEnergies;
	end;

	procedure TEnergiesFrame3CL.setEnergies(aType : TTypeEnergie);
	begin
		if (aType = nil) then
			exit;

//    if (innerEnergies <> nil) then
//    begin
//      innerEnergies.Free;
//    end;

		innerEnergies := aType;
		refresh();
	end;




	procedure TEnergiesFrame3CL.removeAll();
	begin
		removeFrames;
    if (innerEnergies <> nil) then
		  innerEnergies.removeAll;
	end;


	procedure TEnergiesFrame3CL.removeFrames();
	var
		cpt: Integer;
		item : TControl;
	begin
		for cpt := ControlCount - 1 downto 0 do
		begin
			item := Controls[cpt];
			RemoveControl(item);
			item.Free;
		end;
	end;

	procedure TEnergiesFrame3CL.initFromLibrary(aType : TTypeEnergie);
	//permet de remplir le typeEnergie pass� en param�tre avec
	//les valeurs lues depuis le biblioth�que
	begin

		if aType = nil then
			exit;
		//on va vider les types d�j� pr�sents pour r�initialiser
		aType.removeAll;
		//on ne supprime que les frames, sinon on perd les �nergies eventuellement stock�s dans un autre objet
		removeFrames;
		innerEnergies := aType;
		showFromLibrary;
	end;



	procedure TEnergiesFrame3CL.refresh();
	var
		frame : TEnergieAbonnementFrame3CL;
		energie : TIzEnergie;
		i : integer;
	begin
		removeFrames;

    if innerEnergies = nil then
      exit;

		for I := 0 to innerEnergies.nb - 1 do
		begin
			frame := TEnergieAbonnementFrame3CL.Create(self, self);
			frame.name := Frame.ClassName + intToStr(i);
			if i mod 2 = 0 then
			begin
				frame.Left := 0;
			end
			else
			begin
				frame.Left := frame.Width;
			end;
			frame.top := frame.Height * (i div 2);
			energie := TIzEnergie(innerEnergies.getObject(i));

			frame.energie := energie;
		end;
	end;


	class function TEnergiesFrame3CL.getNewName() : string;
	begin
		result := className + intToStr(nextId);
		inc(nextId);
	end;

initialization
	TEnergiesFrame3CL.nextId := 0;
finalization

end.
