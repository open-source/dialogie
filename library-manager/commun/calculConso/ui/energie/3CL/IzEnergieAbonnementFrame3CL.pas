unit IzEnergieAbonnementFrame3CL;

interface

uses
	Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
	Dialogs, StdCtrls, IzEnergies3CL;

type
	TEnergieAbonnementFrame3CL = class(TFrame)
    labelNomEnergie: TGroupBox;
    labelEnergieFournisseur: TLabel;
    labelEnergieType: TLabel;
		ComboFournisseur: TComboBox;
		ComboType: TComboBox;
		Label101: TLabel;
		Label102: TLabel;
		Label62: TLabel;
		EditSource: TEdit;
		EditAbo: TEdit;
		EditkWh: TEdit;
		Label134: TLabel;
		EditDate: TEdit;



		procedure ComboFournisseurChange(Sender: TObject);
		procedure ComboTypeChange(Sender: TObject);
	private
		innerEnergie : TIzEnergie;
		procedure setEnergie(aEnergie : TIzEnergie);
		procedure sendParent(sender: TObject);
		class var nextId : integer;

	protected
		class function getNewName() : string;

	public
		property energie : TIzEnergie read innerEnergie write setEnergie;


		constructor Create(AOwner: TComponent ;aParent : TWinControl);reintroduce;overload; virtual;
    procedure initToDefault();
	end;

implementation

uses
	IzConstantes, IzUiUtil, IzBiblioSystem, IzUtilitaires, IzProjet;

{$R *.dfm}

	constructor TEnergieAbonnementFrame3CL.Create(AOwner: TComponent ;aParent : TWinControl);
	begin
		inherited create(AOwner);
		name := getNewName;
		if (aParent <> nil) then
		begin
			parent := aParent;
		end;
	end;

	procedure TEnergieAbonnementFrame3CL.ComboFournisseurChange(Sender: TObject);
	begin

		if (sender <> nil) then
		begin
			innerEnergie.fournisseur := ComboFournisseur.text;
		end;
		fillComboFreeList(ComboType, getBiblioSystem().biblioEnergieTarif.GetMenuType(innerEnergie.nomEnergie,innerEnergie.fournisseur));

		if (sender <> nil) then
			selectComboItem(ComboType, 'standard')
		else
			if (innerEnergie.fournisseur <> '') then
			begin
				if (innerEnergie.typeTarif = '') then
					selectComboItem(ComboType, 'standard')
				else
					selectComboItem(comboType, innerEnergie.typeTarif);
			end;
		setStyleFromCombo(labelEnergieFournisseur, ComboFournisseur);
		ComboTypeChange(sender);
//		setComboVisible(comboType);
		setStyleFromCombo(labelEnergieType,comboType);

		sendParent(sender);
	end;

	procedure TEnergieAbonnementFrame3CL.ComboTypeChange(Sender: TObject);
	begin
		setStyle(labelEnergieType, true);
		if (sender <> nil) or (ComboType.Items.Count = 1) then
		begin
			innerEnergie.typeTarif := ComboType.text;
			getProjet().aSauvegarder := True;
			getProjet().Acalculer := True;
		end;
		EditAbo.Text := getString(getBiblioSystem.biblioEnergieTarif.PrixAbonnement(innerEnergie.fournisseur, innerEnergie.typeTarif, innerEnergie.nomEnergie));
		EditkWh.Text := getString(getBiblioSystem.biblioEnergieTarif.PrixkWh(innerEnergie.fournisseur, innerEnergie.typeTarif, innerEnergie.nomEnergie), 3);

		EditSource.text := getBiblioSystem.biblioEnergieTarif.Reference(innerEnergie.fournisseur, innerEnergie.typeTarif, innerEnergie.nomEnergie);
		EditDate.Text := getBiblioSystem.biblioEnergieTarif.getDateMajString(innerEnergie.fournisseur, innerEnergie.typeTarif, innerEnergie.nomEnergie);


		sendParent(sender);
	end;

procedure TEnergieAbonnementFrame3CL.sendParent(Sender: TObject);
	var msg : TMessage;
	begin
		msg.Msg := IZ_FRAME_CHANGED;
		msg.WParam := Integer(sender);
		msg.LParam := Integer(self);
		parent.WindowProc(msg);
	end;

	

	procedure TEnergieAbonnementFrame3CL.setEnergie(aEnergie : TIzEnergie);
	begin
		innerEnergie := aEnergie;
		labelNomEnergie.Caption := innerEnergie.nomEnergie;
		fillComboFreeList(ComboFournisseur, getBiblioSystem().biblioEnergieTarif.getMenuFournisseur(innerEnergie.nomEnergie));

		if (innerEnergie.fournisseur = '') and (ComboFournisseur.items.Count = 1) then
			innerEnergie.fournisseur := ComboFournisseur.items[0];

		selectComboItem(ComboFournisseur, innerEnergie.fournisseur);
		ComboFournisseurChange(nil);
	end;


	class function TEnergieAbonnementFrame3CL.getNewName() : string;
	begin
		result := className + intToStr(nextId);
		inc(nextId);
	end;

procedure TEnergieAbonnementFrame3CL.initToDefault;
begin
  if innerEnergie = nil then
    exit;

	innerEnergie.fournisseur := ComboFournisseur.items[0];
  selectComboItem(ComboFournisseur, innerEnergie.fournisseur);
  ComboFournisseurChange(self);
  Application.ProcessMessages;

  innerEnergie.typeTarif := ComboType.Items[0];
	selectComboItem(comboType, innerEnergie.typeTarif);
	ComboTypeChange(self);
  Application.ProcessMessages;
end;

initialization
	TEnergieAbonnementFrame3CL.nextId := 0;
finalization

end.
