inherited ChauffageFrame3CL: TChauffageFrame3CL
  Width = 737
  Height = 134
  ParentCtl3D = False
  ExplicitWidth = 737
  ExplicitHeight = 134
  inherited frameLabel: TGroupBox
    Width = 718
    Height = 134
    ExplicitWidth = 718
    ExplicitHeight = 134
    inherited labelPourcent: TLabel
      Left = 12
      Top = 16
      ExplicitLeft = 12
      ExplicitTop = 16
    end
    inherited labelPourcentSymbol: TLabel
      Left = 193
      Top = 16
      ExplicitLeft = 193
      ExplicitTop = 16
    end
    object labChauffage: TLabel [2]
      Left = 12
      Top = 40
      Width = 51
      Height = 13
      HelpContext = 20
      Caption = 'Chauffage'
    end
    object labGenerateur: TLabel [3]
      Left = 84
      Top = 40
      Width = 55
      Height = 13
      HelpContext = 20
      Caption = 'G'#233'n'#233'rateur'
    end
    object labEnergie: TLabel [4]
      Left = 219
      Top = 40
      Width = 36
      Height = 13
      HelpContext = 20
      Caption = 'Energie'
    end
    object labType: TLabel [5]
      Left = 338
      Top = 40
      Width = 24
      Height = 13
      HelpContext = 20
      Caption = 'Type'
    end
    object labEmetteur: TLabel [6]
      Left = 549
      Top = 40
      Width = 44
      Height = 13
      HelpContext = 20
      Caption = 'Emetteur'
    end
    object labRobinet: TLabel [7]
      Left = 12
      Top = 82
      Width = 50
      Height = 13
      HelpContext = 20
      Caption = 'Robinet th'
    end
    object Label1: TLabel [8]
      Left = 471
      Top = 16
      Width = 35
      Height = 13
      HelpContext = 20
      Caption = 'Ich PCI'
    end
    object labProg: TLabel [9]
      Left = 83
      Top = 148
      Width = 30
      Height = 13
      HelpContext = 20
      Caption = 'Progr.'
      Visible = False
    end
    object labIsolationReseau: TLabel [10]
      Left = 76
      Top = 82
      Width = 80
      Height = 13
      Caption = 'Isolation R'#233'seau'
      Visible = False
    end
    object labOption1: TLabel [11]
      Left = 208
      Top = 82
      Width = 32
      Height = 13
      Caption = 'Option'
      Visible = False
    end
    inherited editPourcent: TEdit
      Left = 143
      Top = 13
      ExplicitLeft = 143
      ExplicitTop = 13
    end
    object ComboCChauff: TComboBox [13]
      Left = 7
      Top = 59
      Width = 71
      Height = 21
      HelpContext = 20
      Style = csDropDownList
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnChange = ComboCChauffChange
    end
    object ComboCGenerateur: TComboBox [14]
      Left = 84
      Top = 59
      Width = 134
      Height = 21
      HelpContext = 20
      Style = csDropDownList
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      OnChange = ComboCGenerateurChange
    end
    object ComboCEnergie: TComboBox [15]
      Left = 219
      Top = 59
      Width = 118
      Height = 21
      HelpContext = 20
      Style = csDropDownList
      ParentShowHint = False
      ShowHint = True
      TabOrder = 3
      OnChange = ComboCEnergieChange
    end
    object ComboCType: TComboBox [16]
      Left = 338
      Top = 59
      Width = 210
      Height = 21
      HelpContext = 20
      Style = csDropDownList
      ParentShowHint = False
      ShowHint = True
      TabOrder = 4
      OnChange = ComboCTypeChange
    end
    object ComboCEmetteur: TComboBox [17]
      Left = 549
      Top = 59
      Width = 160
      Height = 21
      HelpContext = 20
      Style = csDropDownList
      ParentShowHint = False
      ShowHint = True
      TabOrder = 5
      OnChange = ComboCEmetteurChange
    end
    object ComboCRobinet: TComboBox [18]
      Left = 12
      Top = 101
      Width = 63
      Height = 21
      HelpContext = 20
      Style = csDropDownList
      ParentShowHint = False
      ShowHint = True
      TabOrder = 6
      OnChange = ComboCRobinetChange
    end
    object ComboCProgrammateur: TComboBox [19]
      Left = 83
      Top = 167
      Width = 65
      Height = 21
      HelpContext = 20
      Style = csDropDownList
      ParentShowHint = False
      ShowHint = True
      TabOrder = 7
      Visible = False
      OnChange = ComboCProgrammateurChange
    end
    object EditIch: TMemo [20]
      Left = 549
      Top = 13
      Width = 92
      Height = 21
      Alignment = taRightJustify
      Color = clBtnFace
      TabOrder = 8
      WantReturns = False
      OnChange = EditIch2Change
    end
    inherited checkApportInterne: TCheckBox
      Left = 226
      Top = 15
      TabOrder = 9
      ExplicitLeft = 226
      ExplicitTop = 15
    end
    object comboIsolationReseau: TComboBox
      Left = 76
      Top = 101
      Width = 131
      Height = 21
      Style = csDropDownList
      ParentShowHint = False
      ShowHint = True
      TabOrder = 10
      Visible = False
      OnChange = comboIsolationReseauChange
    end
    object comboOption1: TComboBox
      Left = 208
      Top = 101
      Width = 118
      Height = 21
      Style = csDropDownList
      ParentShowHint = False
      ShowHint = True
      TabOrder = 11
      Visible = False
      OnChange = comboOption1Change
    end
  end
  inherited checkSelection: TCheckBox
    Height = 134
    ExplicitHeight = 129
  end
end
