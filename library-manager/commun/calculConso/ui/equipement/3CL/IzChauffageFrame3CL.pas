unit IzChauffageFrame3CL;

interface

uses
	Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
	Dialogs, IzEquipementFrame, StdCtrls, IzObjectUtil, IzBiblioChauffage3CL,
  IzConstantesConso, Buttons, IzEquipementChauffage3CL;



type
	TChauffageFrame3CL = class(TEquipementFrame)
		labChauffage: TLabel;
		labGenerateur: TLabel;
		labEnergie: TLabel;
		labType: TLabel;
		labEmetteur: TLabel;
		labRobinet: TLabel;
		Label1: TLabel;
		ComboCChauff: TComboBox;
		ComboCGenerateur: TComboBox;
		ComboCEnergie: TComboBox;
		ComboCType: TComboBox;
		ComboCEmetteur: TComboBox;
		ComboCRobinet: TComboBox;
		labProg: TLabel;
		ComboCProgrammateur: TComboBox;
		EditIch: TMemo;
		labIsolationReseau: TLabel;
		comboIsolationReseau: TComboBox;
		labOption1: TLabel;
		comboOption1: TComboBox;
		procedure ComboCChauffChange(Sender: TObject);
		procedure EditPourcentChange(Sender: TObject);
		procedure ComboCGenerateurChange(Sender: TObject);
		procedure ComboCEnergieChange(Sender: TObject);
		procedure ComboCTypeChange(Sender: TObject);
		procedure ComboCEmetteurChange(Sender: TObject);
		procedure ComboCRobinetChange(Sender: TObject);
		procedure EditIch2Change(Sender: TObject);
		procedure ComboCProgrammateurChange(Sender: TObject);
		procedure comboIsolationReseauChange(Sender: TObject);
		procedure comboOption1Change(Sender: TObject);
	private
		innerChauffageFilter : TChauffageFilter;

	protected
		procedure setChauffageFilter(filter : TChauffageFilter);

		procedure setEquipement(anEquipement : TIzPercentObject);override;
		procedure setIch(setFromUi : boolean);virtual;
		function getBiblio() : TBiblioChauffage3CL;inline;
	public
		constructor Create(AOwner: TComponent ;aParent : TWinControl);override;


		function getEquipementClass : TIzPercentObjectClass; override;
		procedure setNewEquipement();override;
		property chauffageFilter : TChauffageFilter read innerChauffageFilter write setChauffageFilter;
	end;

implementation

{$R *.dfm}

uses
  System.UITypes,
  IzBiblioSystem,
  IzUtilitaires,
  IzUiUtil,
  IzConstantes,
  IzProjet,
  Projet;



	constructor TChauffageFrame3CL.Create(AOwner: TComponent ;aParent : TWinControl);
	begin
		inherited create(AOwner, aParent);
    chauffageFilter := CHAUFFAGE_FILTER_NONE;
	end;

	procedure TChauffageFrame3CL.setChauffageFilter(filter : TChauffageFilter);
	begin
		innerChauffageFilter := filter;
	end;


procedure TChauffageFrame3CL.ComboCChauffChange(Sender: TObject);
	Var
		Ancien : String;
		eqp : TEquipementChauffage3CL;
	begin
		eqp := TEquipementChauffage3CL(equipement);


    ComboCChauff.Hint := ComboCChauff.Text;

		if (sender <> nil) then
			eqp.Chauffage := ComboCChauff.Text;



		if lowerCase(ComboCChauff.Text) = 'autre' then
		Begin
					EditICh.Readonly := False;
					EditIch.Color := clWindow;
		End
		else
		Begin
					EditICh.Readonly := True;
					EditIch.Color := clBtnFace;
		End;

		Ancien := ComboCGenerateur.text;

		fillComboFreeList(ComboCGenerateur, getBiblio().getGenerateur(innerChauffageFilter, ComboCChauff.Text));

		if sender <> nil then
			selectComboItem(ComboCGenerateur, ancien)
		else
			selectComboItem(ComboCGenerateur, eqp.Generateur);

		ComboCGenerateurChange(sender);

		if (sender <> nil) then
		begin
			eqp.Chauffage := ComboCChauff.Text;
			getProjet().ASauvegarder := True;
			getProjet().Acalculer := True;
		end;

		setComboVisible(comboCGenerateur);
    labGenerateur.Visible := ComboCGenerateur.Visible;
		setStyleFromCombo(labGenerateur, ComboCGenerateur);

		sendOwner(sender);
	end;


	procedure TChauffageFrame3CL.ComboCEmetteurChange(Sender: TObject);
	Var
		Ancien : String;
		eqp : TEquipementChauffage3CL;
begin
    ComboCEmetteur.Hint := ComboCEmetteur.text;
		eqp := TEquipementChauffage3CL(equipement);
		labEmetteur.Font.Color := clblack;
		labEmetteur.Font.Style := labEmetteur.Font.Style;//-[fsbold];


		if (sender <> nil) then
			eqp.Emetteur := ComboCEmetteur.text;

		Ancien := ComboCRobinet.text;

		fillComboFreeList(comboCRobinet, getBiblio().getRobinet(innerChauffageFilter, ComboCChauff.Text,ComboCGenerateur.text ,ComboCEnergie.text,ComboCType.text,ComboCEmetteur.text));

		if (sender <> nil) then
			selectComboItem(ComboCRobinet, ancien)
		else
			selectComboItem(ComboCRobinet, eqp.Robinet);

		ComboCRobinetChange(sender);
		if (sender <> nil) then
		begin
			eqp.Emetteur := ComboCEmetteur.text;
			getProjet().ASauvegarder := True;
			getProjet().Acalculer := True;
		end;

		setComboVisible(comboCRobinet);
    labRobinet.Visible := ComboCRobinet.Visible;
		setStyleFromCombo(labRobinet, ComboCRobinet);

		sendOwner(sender);
	end;


procedure TChauffageFrame3CL.ComboCEnergieChange(Sender: TObject);
	Var
		Ancien : String;
		eqp : TEquipementChauffage3CL;
begin
    ComboCEnergie.hint := ComboCEnergie.text;
		eqp := TEquipementChauffage3CL(equipement);
		labEnergie.Font.Color := clblack;
		labEnergie.Font.Style := labEnergie.Font.Style;//-[fsbold];


		if (sender <> nil) then
			eqp.Energie := ComboCEnergie.text;


		Ancien := ComboCType.text;

		fillComboFreeList(comboCType, getBiblio().
				getTypeChauffage(innerChauffageFilter, ComboCChauff.Text,ComboCGenerateur.text ,ComboCEnergie.text));

		if (sender <> nil) then
			selectComboItem(ComboCType, ancien)
		else
			selectComboItem(ComboCType, eqp.TypeC);

		ComboCTypeChange(sender);
		if (sender <> nil) then
		begin
			eqp.Energie := ComboCEnergie.text;
			getProjet().aSauvegarder := True;
			getProjet().aCalculer := True;
		end;

		setComboVisible(comboCType);
		labType.Visible := ComboCType.Visible;
		setStyleFromCombo(labType, ComboCType);

		sendOwner(sender);

	end;

procedure TChauffageFrame3CL.ComboCGenerateurChange(Sender: TObject);
	var
		Ancien : String;
		eqp : TEquipementChauffage3CL;
begin
    ComboCGenerateur.Hint := ComboCGenerateur.Text;
		eqp := TEquipementChauffage3CL(equipement);

		labGenerateur.Font.Color := clblack;
		labGenerateur.Font.Style := labGenerateur.Font.Style;//-[fsbold];

		if (sender <> nil) then
			eqp.Generateur := ComboCGenerateur.text;
		setComboVisible(comboCEnergie);

		Ancien := ComboCEnergie.text;

		fillComboFreeList(comboCEnergie, getBiblio().
				getEnergie(innerChauffageFilter, ComboCChauff.Text,ComboCGenerateur.text));

		if (sender <> nil) then
			selectComboItem(ComboCEnergie, ancien)
		else
			selectComboItem(ComboCEnergie, eqp.Energie);

		ComboCEnergieChange(sender);
		if (sender <> nil) then
		begin
			eqp.Generateur := ComboCGenerateur.text;
			getProjet().aSauvegarder := True;
			getProjet().aCalculer := True;
		end;

		setComboVisible(comboCEnergie);
  	labEnergie.Visible := ComboCEnergie.Visible;
		setStyleFromCombo(labEnergie, ComboCEnergie);

		sendOwner(sender);
	end;


	procedure TChauffageFrame3CL.ComboCProgrammateurChange(Sender: TObject);
	Var
		ancien : string;
		eqp : TEquipementChauffage3CL;
	begin

    ComboCProgrammateur.Hint := ComboCProgrammateur.text;
		//ici, les calculs influaient sur des choses plus globales
		eqp := TEquipementChauffage3CL(equipement);
		labProg.Font.Color := clblack;
		labProg.Font.Style := labProg.Font.Style;//-[fsbold];

		ancien := comboIsolationReseau.text;

		fillComboFreeList(comboIsolationReseau, getBiblio().
				getIsolationReseau(innerChauffageFilter, ComboCChauff.Text,ComboCGenerateur.text ,
						ComboCEnergie.text,
						ComboCType.text,
						ComboCEmetteur.text,
						ComboCRobinet.text,
						ComboCProgrammateur.Text));

		if (sender <> nil) then
		begin
			selectComboItem(comboIsolationReseau, ancien);
		end
		else
		begin
			selectComboItem(comboIsolationReseau, eqp.isolationReseau);
		end;
		comboIsolationReseauChange(sender);

		if (sender <> nil) then
		begin
			eqp.Programmateur := ComboCProgrammateur.text;
			getProjet().aSauvegarder := True;
			getProjet().aCalculer := True;
		end;
//le programmateur n'est plus en bout de chaine
//		setIch(sender <> nil);

//pour ne pas le prendre en compte actuellement
		setComboVisible(comboIsolationReseau);
		labIsolationReseau.Visible := comboIsolationReseau.Visible;
		setStyleFromCombo(labIsolationReseau, comboIsolationReseau);

		sendOwner(sender);
	end;


procedure TChauffageFrame3CL.ComboCRobinetChange(Sender: TObject);
	Var
		Ancien : String;
		eqp : TEquipementChauffage3CL;
begin
    ComboCRobinet.hint := ComboCRobinet.text;
		eqp := TEquipementChauffage3CL(equipement);
		labRobinet.Font.Color := clblack;
		labRobinet.Font.Style := labRobinet.Font.Style;//-[fsbold];
		Ancien := ComboCProgrammateur.text;

		fillComboFreeList(comboCProgrammateur, getBiblio().
				getProgrammateur(innerChauffageFilter, ComboCChauff.Text,ComboCGenerateur.text ,
						ComboCEnergie.text,
						ComboCType.text,
						ComboCEmetteur.text,
						ComboCRobinet.text));

		if (sender <> nil) then
		begin
			selectComboItem(ComboCProgrammateur, ancien);//ne sert � rien //revenu en arri�re
//			ComboCProgrammateur.ItemIndex := 0;
		end
		else
		begin
			selectComboItem(ComboCProgrammateur, eqp.Programmateur);
		end;


		ComboCProgrammateurChange(sender);
		if (sender <> nil) then
		begin
			eqp.Robinet := ComboCRobinet.text;
			getProjet().aSauvegarder := True;
			getProjet().aCalculer := True;
		end;

		setComboVisible(ComboCProgrammateur);
		labProg.Visible := ComboCProgrammateur.Visible;
		setStyleFromCombo(labProg, ComboCProgrammateur);

		sendOwner(sender);
	end;

procedure TChauffageFrame3CL.ComboCTypeChange(Sender: TObject);
	Var
		Ancien : String;
		eqp : TEquipementChauffage3CL;
begin
    ComboCType.hint := ComboCType.text;
		eqp := TEquipementChauffage3CL(equipement);
		labType.Font.Color := clblack;
		labType.Font.Style := labType.Font.Style;//-[fsbold];
		Ancien := ComboCEmetteur.text;

		fillComboFreeList(comboCEmetteur, getBiblio().
				getEmetteur(innerChauffageFilter, ComboCChauff.Text,
						ComboCGenerateur.text ,
						ComboCEnergie.text,
						ComboCType.text));

		if (sender <> nil) then
			selectComboItem(ComboCEmetteur, ancien)
		else
			selectComboItem(ComboCEmetteur, eqp.Emetteur);


		ComboCEmetteurChange(sender);
		if (sender <> nil) then
		begin
			eqp.TypeC := ComboCType.text;
			getProjet().aSauvegarder := True;
			getProjet().aCalculer := True;
		end;

		setComboVisible(ComboCEmetteur);
		labEmetteur.Visible := ComboCEmetteur.Visible;
		setStyleFromCombo(labEmetteur, ComboCEmetteur);

		sendOwner(sender);
end;



	procedure TChauffageFrame3CL.comboIsolationReseauChange(Sender: TObject);
	Var
		ancien : string;
		eqp : TEquipementChauffage3CL;
	begin
    comboIsolationReseau.hint := comboIsolationReseau.text;
		//ici, les calculs influaient sur des choses plus globales
		eqp := TEquipementChauffage3CL(equipement);
		labIsolationReseau.Font.Color := clblack;
		labIsolationReseau.Font.Style := labProg.Font.Style;//-[fsbold];

		ancien := comboOption1.text;

		fillComboFreeList(comboOption1, getBiblio().
				getOption1(innerChauffageFilter, ComboCChauff.Text,ComboCGenerateur.text ,
						ComboCEnergie.text,
						ComboCType.text,
						ComboCEmetteur.text,
						ComboCRobinet.text,
						ComboCProgrammateur.Text,
						comboIsolationReseau.Text));

		if (sender <> nil) then
		begin
			selectComboItem(comboOption1, ancien);
		end
		else
		begin
			selectComboItem(comboOption1, eqp.option1);
		end;
		comboOption1Change(sender);

		if (sender <> nil) then
		begin
			eqp.isolationReseau := comboIsolationReseau.text;
			getProjet().aSauvegarder := True;
			getProjet().aCalculer := True;
		end;
//le programmateur n'est plus en bout de chaine
//		setIch(sender <> nil);

		setComboVisible(comboOption1);
		labOption1.Visible := comboOption1.Visible;
		setStyleFromCombo(labOption1, comboOption1);

		sendOwner(sender);
	end;



	procedure TChauffageFrame3CL.comboOption1Change(Sender: TObject);
	Var
		eqp : TEquipementChauffage3CL;
	begin
    comboOption1.Hint := comboOption1.text;
		//ici, les calculs influaient sur des choses plus globales
		eqp := TEquipementChauffage3CL(equipement);
		labOption1.Font.Color := clblack;
		labOption1.Font.Style := labProg.Font.Style;//-[fsbold];

		if (sender <> nil) then
		begin
			eqp.option1 := comboOption1.text;
			getProjet().aSauvegarder := True;
			getProjet().aCalculer := True;
		end
		else
		begin
			selectComboItem(comboOption1, eqp.option1);
		end;

		setIch(sender <> nil);
(*
		setComboVisible(comboOption1);
		setStyleFromCombo(labOption1, comboOption1);
*)
		sendOwner(sender);
	end;



procedure TChauffageFrame3CL.EditPourcentChange(Sender: TObject);
	begin
		inherited EditPourcentChange(Sender);
		if (sender <> nil) then
		begin
			getProjet().aSauvegarder := True;
			getProjet().aCalculer := True;
		end;
	end;


	procedure TChauffageFrame3CL.EditIch2Change(Sender: TObject);
	Var
		Valeur : Double;
		eqp : TEquipementChauffage3CL;
begin
		if (sender <> nil) then
		begin
			eqp := TEquipementChauffage3CL(equipement);
			Valeur := getDouble(EditIch.text);
			eqp.ichIndicatif := Valeur;
      if not(EditIch.readOnly) then
      begin
        eqp.Rd := 0;
        eqp.Re := 0;
        eqp.Rg := 0;
        eqp.Rr := 0;
      end;

			if (EditIch.Text <> '') and (EditIch.Text <> '-0.0') then
				sendOwner(sender, IZ_FRAME_OBJECT_DATA_COMPLETE);
		end;
	end;


	function TChauffageFrame3CL.getEquipementClass : TIzPercentObjectClass;
	begin
		getEquipementClass := TEquipementChauffage3CL;
	end;


	procedure TChauffageFrame3CL.setEquipement(anEquipement : TIzPercentObject);
	var
		eqp : TEquipementChauffage3CL;
	begin
		inherited setEquipement(anEquipement);
		eqp := TEquipementChauffage3CL(equipement);

		fillComboFreeList(comboCChauff, getBiblio().getNomChauffage(innerChauffageFilter));

    if (eqp <> nil) then
    begin
    	selectComboItem(ComboCChauff, eqp.Chauffage);
		  ComboCChauffChange(nil);
    end;
	end;


	procedure TChauffageFrame3CL.setNewEquipement();
	begin
		inherited setNewEquipement();

		fillComboFreeList(ComboCChauff, getBiblio().getNomChauffage(innerChauffageFilter));

		if (ComboCChauff.Items.Count > 0) then
			ComboCChauff.ItemIndex := 0;

		ComboCChauffChange(self);
	end;


	procedure TChauffageFrame3CL.setIch(setFromUi : boolean);
	var
		eqp : TEquipementChauffage3CL;
		chauff : TChauffage;
	begin
		eqp := TEquipementChauffage3CL(equipement);
		if (eqp.Chauffage <> '') and (eqp.Chauffage <> 'Aucun') then
		Begin
			if ((setFromUi) or (projetDialogie.rescanData)) then
			begin
				chauff := getBiblio().getChauffage(innerChauffageFilter,
						ComboCChauff.Text, ComboCGenerateur.text, ComboCEnergie.text,
						ComboCType.text,ComboCEmetteur.text, ComboCRobinet.text,
						ComboCProgrammateur.text, comboIsolationReseau.Text,
						comboOption1.Text);
				if chauff.chauffage <> CHAUFFAGE_VIDE.chauffage then
				begin
					eqp.ichIndicatif := getDouble(chauff.ICh);
					eqp.Accu := getBool(chauff.Accu);

					eqp.isolationReseau := chauff.isolationReseau;
					eqp.option1 := chauff.option1;
					eqp.ichDpe := chauff.ichDpe;
					eqp.progDpe := chauff.progDpe;
					eqp.Rd := chauff.Rd;
					eqp.Re := chauff.Re;
					eqp.Rg := chauff.Rg;
					eqp.Rr := chauff.Rr;

					eqp.A1 := chauff.A1;
					eqp.A2 := chauff.A2;
//					eqp.corCh := chauff.corCh;
//					eqp.appointChaudiere := chauff.chaudiereAppoint;

//old school
					EditIch.Text := getString(getDouble(chauff.ICh));
//fa�on 3CL
//					EditIch.Text := getString(eqp.computeIch(4000));
				end
        else
        begin
					EditIch.Text := '';
        end;
(*
				EditIch.text := getBiblioSystem().getBiblioChauffage(getProjet().typeBatiment).getICh(ComboCChauff.Text,
					ComboCGenerateur.text, ComboCEnergie.text,ComboCType.text,ComboCEmetteur.text,
					ComboCRobinet.text,ComboCProgrammateur.text);
				Val(EditICh.text,eqp.Ich,Erreur);

				eqp.Accu := getBiblioSystem().getBiblioChauffage(getProjet().typeBatiment).getAccumulation(ComboCChauff.Text,
					ComboCGenerateur.text, ComboCEnergie.text,ComboCType.text,ComboCEmetteur.text,
					ComboCRobinet.text,ComboCProgrammateur.text) = 'oui';
*)
			end
			else
			begin
				EditIch.text := getString(eqp.ichIndicatif);
			end;
		end
    else
    begin
			EditIch.Text := '';
    end;
	end;


	function TChauffageFrame3CL.getBiblio() : TBiblioChauffage3CL;
	begin
		result := getBiblioSystem().getBiblioChauffage(getProjet().typeBatiment);
	end;

end.
