object FrameECSolaire: TFrameECSolaire
  Left = 0
  Top = 0
  Width = 915
  Height = 170
  Ctl3D = True
  ParentCtl3D = False
  TabOrder = 0
  TabStop = True
  object frameLabel: TGroupBox
    Left = 0
    Top = 0
    Width = 915
    Height = 170
    Align = alClient
    Caption = 'Solaire'
    TabOrder = 0
    object Label89: TLabel
      Left = 8
      Top = 56
      Width = 66
      Height = 13
      HelpContext = 21
      Caption = 'Inclinaison ('#176')'
    end
    object Label90: TLabel
      Left = 80
      Top = 56
      Width = 70
      Height = 13
      HelpContext = 21
      Caption = 'Orientation ('#176')'
    end
    object Label91: TLabel
      Left = 152
      Top = 56
      Width = 42
      Height = 13
      HelpContext = 21
      Caption = 'Situation'
    end
    object Label92: TLabel
      Left = 288
      Top = 56
      Width = 41
      Height = 13
      HelpContext = 21
      Caption = 'Syst'#232'me'
    end
    object Label93: TLabel
      Left = 592
      Top = 56
      Width = 56
      Height = 13
      HelpContext = 21
      Caption = 'Surface (m)'
    end
    object Label94: TLabel
      Left = 656
      Top = 56
      Width = 24
      Height = 13
      HelpContext = 21
      Caption = 'Type'
    end
    object Label95: TLabel
      Left = 8
      Top = 120
      Width = 70
      Height = 13
      HelpContext = 21
      Caption = 'Temp eau ('#176'C)'
    end
    object Label96: TLabel
      Left = 96
      Top = 120
      Width = 47
      Height = 13
      HelpContext = 21
      Caption = 'Volume (l)'
    end
    object Label97: TLabel
      Left = 168
      Top = 120
      Width = 41
      Height = 13
      HelpContext = 21
      Caption = 'Isolation'
    end
    object Label98: TLabel
      Left = 272
      Top = 120
      Width = 63
      Height = 13
      HelpContext = 21
      Caption = 'Emplacement'
    end
    object Label99: TLabel
      Left = 8
      Top = 104
      Width = 53
      Height = 13
      HelpContext = 21
      Caption = 'Stockage'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label100: TLabel
      Left = 8
      Top = 40
      Width = 51
      Height = 13
      HelpContext = 21
      Caption = 'Capteurs'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object ComboSolaireInclinaison: TComboBox
      Left = 8
      Top = 72
      Width = 73
      Height = 21
      HelpContext = 21
      TabOrder = 1
      Items.Strings = (
        '17  '#176
        '30 '#176
        '45 '#176
        '60 '#176
        '90 '#176)
    end
    object ComboSolaireOrientation: TComboBox
      Left = 80
      Top = 72
      Width = 73
      Height = 21
      HelpContext = 21
      TabOrder = 2
      Items.Strings = (
        '-30 '#176
        '-15 '#176
        '0 '#176
        '15 '#176
        '30 '#176)
    end
    object ComboSolaireSituation: TComboBox
      Left = 152
      Top = 72
      Width = 137
      Height = 21
      HelpContext = 21
      TabOrder = 3
      Items.Strings = (
        '0'
        '0.1 D'#233'gag'#233
        '0.2 L'#233'ger masque'
        '0.3 Masque significatif')
    end
    object ComboSolaireSysteme: TComboBox
      Left = 288
      Top = 72
      Width = 305
      Height = 21
      HelpContext = 21
      Style = csDropDownList
      TabOrder = 4
    end
    object ComboSolaireSurface: TComboBox
      Left = 592
      Top = 72
      Width = 65
      Height = 21
      HelpContext = 21
      TabOrder = 5
    end
    object ComboSolaireType: TComboBox
      Left = 656
      Top = 72
      Width = 113
      Height = 21
      HelpContext = 21
      Style = csDropDownList
      TabOrder = 6
    end
    object ComboSolaireTemp: TComboBox
      Left = 8
      Top = 136
      Width = 89
      Height = 21
      HelpContext = 21
      TabOrder = 7
    end
    object ComboSolaireVolume: TComboBox
      Left = 96
      Top = 136
      Width = 73
      Height = 21
      HelpContext = 21
      TabOrder = 8
    end
    object ComboSolaireIsolation: TComboBox
      Left = 168
      Top = 136
      Width = 105
      Height = 21
      HelpContext = 21
      Style = csDropDownList
      TabOrder = 9
      Items.Strings = (
        'Normale'
        'Renforc'#233'e')
    end
    object ComboSolaireEmplacement: TComboBox
      Left = 272
      Top = 136
      Width = 153
      Height = 21
      HelpContext = 21
      Style = csDropDownList
      TabOrder = 10
    end
    object CheckSolaire: TCheckBox
      Left = 8
      Top = 16
      Width = 153
      Height = 17
      HelpContext = 21
      Caption = 'Activer l'#39'eau chaude solaire'
      TabOrder = 0
    end
  end
end
