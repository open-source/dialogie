unit IzEcsSolaireFrame3CL;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, 
  Dialogs, StdCtrls;

type
  TFrameECSolaire = class(TFrame)
    frameLabel: TGroupBox;
    Label89: TLabel;
    Label90: TLabel;
    Label91: TLabel;
    Label92: TLabel;
    Label93: TLabel;
    Label94: TLabel;
    Label95: TLabel;
    Label96: TLabel;
    Label97: TLabel;
    Label98: TLabel;
    Label99: TLabel;
    Label100: TLabel;
    ComboSolaireInclinaison: TComboBox;
    ComboSolaireOrientation: TComboBox;
    ComboSolaireSituation: TComboBox;
    ComboSolaireSysteme: TComboBox;
    ComboSolaireSurface: TComboBox;
    ComboSolaireType: TComboBox;
    ComboSolaireTemp: TComboBox;
    ComboSolaireVolume: TComboBox;
    ComboSolaireIsolation: TComboBox;
    ComboSolaireEmplacement: TComboBox;
    CheckSolaire: TCheckBox;
  private
    { Déclarations privées }
  public
    { Déclarations publiques }
  end;

implementation

{$R *.dfm}

end.
