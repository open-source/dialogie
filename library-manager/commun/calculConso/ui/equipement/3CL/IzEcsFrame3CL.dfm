inherited EcsFrame3CL: TEcsFrame3CL
  Width = 827
  Height = 90
  ParentCtl3D = False
  ExplicitWidth = 827
  ExplicitHeight = 90
  inherited frameLabel: TGroupBox
    Width = 808
    Height = 90
    ExplicitWidth = 808
    ExplicitHeight = 90
    inherited labelPourcent: TLabel
      Left = 108
      Top = 16
      ExplicitLeft = 108
      ExplicitTop = 16
    end
    inherited labelPourcentSymbol: TLabel
      Left = 285
      Top = 16
      ExplicitLeft = 285
      ExplicitTop = 16
    end
    object LabelE81: TLabel [2]
      Left = 720
      Top = 40
      Width = 40
      Height = 13
      Caption = 'Iecs PCI'
    end
    object LabelE71: TLabel [3]
      Left = 665
      Top = 40
      Width = 41
      Height = 13
      Caption = 'Veilleuse'
    end
    object LabelE61: TLabel [4]
      Left = 606
      Top = 40
      Width = 28
      Height = 13
      Caption = 'Ballon'
    end
    object LabelE51: TLabel [5]
      Left = 432
      Top = 40
      Width = 53
      Height = 13
      Caption = 'Installation'
    end
    object LabelE41: TLabel [6]
      Left = 346
      Top = 40
      Width = 44
      Height = 13
      Caption = 'Stockage'
    end
    object LabelE31: TLabel [7]
      Left = 234
      Top = 40
      Width = 24
      Height = 13
      Caption = 'Type'
    end
    object LabelE21: TLabel [8]
      Left = 121
      Top = 40
      Width = 36
      Height = 13
      Caption = 'Energie'
    end
    object LabelE11: TLabel [9]
      Left = 11
      Top = 40
      Width = 41
      Height = 13
      Caption = 'Syst'#232'me'
    end
    inherited editPourcent: TEdit
      Left = 235
      Top = 13
      ExplicitLeft = 235
      ExplicitTop = 13
    end
    object ComboSys: TComboBox [11]
      Left = 11
      Top = 59
      Width = 113
      Height = 21
      HelpContext = 20
      Style = csDropDownList
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnChange = ComboSysChange
    end
    object ComboEnerg: TComboBox [12]
      Left = 121
      Top = 59
      Width = 116
      Height = 21
      Style = csDropDownList
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      OnChange = ComboEnergChange
    end
    object ComboType: TComboBox [13]
      Left = 234
      Top = 59
      Width = 115
      Height = 21
      Style = csDropDownList
      ParentShowHint = False
      ShowHint = True
      TabOrder = 3
      OnChange = ComboTypeChange
    end
    object ComboStock: TComboBox [14]
      Left = 346
      Top = 59
      Width = 89
      Height = 21
      Style = csDropDownList
      ParentShowHint = False
      ShowHint = True
      TabOrder = 4
      OnChange = ComboStockChange
    end
    object ComboInst: TComboBox [15]
      Left = 432
      Top = 59
      Width = 177
      Height = 21
      Style = csDropDownList
      ParentShowHint = False
      ShowHint = True
      TabOrder = 5
      OnChange = ComboInstChange
    end
    object ComboBall: TComboBox [16]
      Left = 606
      Top = 59
      Width = 62
      Height = 21
      Style = csDropDownList
      ParentShowHint = False
      ShowHint = True
      TabOrder = 6
      OnChange = ComboBallChange
    end
    object ComboVeille: TComboBox [17]
      Left = 665
      Top = 59
      Width = 49
      Height = 21
      Style = csDropDownList
      ParentShowHint = False
      ShowHint = True
      TabOrder = 7
      OnChange = ComboVeilleChange
    end
    object EditRdtECS: TMemo [18]
      Left = 720
      Top = 59
      Width = 86
      Height = 21
      Alignment = taRightJustify
      Color = clBtnFace
      TabOrder = 8
      WantReturns = False
      OnChange = EditRdtECSChange
    end
    inherited checkApportInterne: TCheckBox
      Left = 328
      Top = 15
      TabOrder = 9
      ExplicitLeft = 328
      ExplicitTop = 15
    end
  end
  inherited checkSelection: TCheckBox
    Height = 90
    ExplicitHeight = 90
  end
end
