unit IzEcsFrame3CL;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
	Dialogs, IzEquipementFrame, StdCtrls, IzEquipementEcs3CL, IzObjectUtil;

type
  TEcsFrame3CL = class(TEquipementFrame)
    ComboSys: TComboBox;
    ComboEnerg: TComboBox;
    ComboType: TComboBox;
    ComboStock: TComboBox;
    ComboInst: TComboBox;
    ComboBall: TComboBox;
    ComboVeille: TComboBox;
    LabelE81: TLabel;
    LabelE71: TLabel;
    LabelE61: TLabel;
    LabelE51: TLabel;
    LabelE41: TLabel;
    LabelE31: TLabel;
    LabelE21: TLabel;
    LabelE11: TLabel;
    EditRdtECS: TMemo;
    procedure EditPourcentChange(Sender: TObject);
		procedure ComboSysChange(Sender: TObject);
    procedure ComboEnergChange(Sender: TObject);
    procedure ComboTypeChange(Sender: TObject);
    procedure ComboStockChange(Sender: TObject);
    procedure ComboInstChange(Sender: TObject);
    procedure ComboBallChange(Sender: TObject);
		procedure ComboVeilleChange(Sender: TObject);
		procedure EditRdtECSChange(Sender: TObject);
		private
			function getECS() : TEquipementEcs3CL;

		protected
			procedure setEquipement(anEquipement : TIzPercentObject);override;
		public
			function getEquipementClass : TIzPercentObjectClass; override;
			procedure setNewEquipement();override;
	end;

implementation

uses
  System.UITypes,
	IzConstantes,
  IzUtilitaires,
  IzUiUtil,
  IzBiblioSystem,
  IzProjet,
  IzBiblioEcs3CL,
  Projet;

{$R *.dfm}


	function TEcsFrame3CL.getECS() : TEquipementEcs3CL;
  begin
		result := TEquipementEcs3CL(equipement);
	end;


	procedure TEcsFrame3CL.ComboBallChange(Sender: TObject);
	Var
		Ancien : String;
	begin
    ComboBall.Hint := ComboBall.Text;
		setStyle(LabelE61, true);

		Ancien := ComboVeille.text;
		fillComboFreeList(ComboVeille,
        getBiblioSystem().getBiblioEcs(getProjet().typeBatiment).getVeilleuse(
          ComboSys.Text,
					ComboEnerg.Text,
          ComboType.Text,
          ComboStock.text,
          comboInst.Text,
					ComboBall.text));
		if (sender <> nil) then
			selectComboItem(ComboVeille, ancien)
		else
			selectComboItem(ComboVeille, getECS().veilleuse);

		ComboVeilleChange(sender);

		if (sender <> nil) then
			getECS().Ballon := ComboBall.text;
		setComboVisible(comboVeille);

		setStyleFromCombo(LabelE71, comboVeille);
		getProjet().aSauvegarder := True;
		getProjet().aCalculer := True;

		sendOwner(sender);
	end;

procedure TEcsFrame3CL.ComboEnergChange(Sender: TObject);
	Var
		Ancien : String;
	begin
    ComboEnerg.Hint := ComboEnerg.Text;
		LabelE21.Font.Color := clblack;
		LabelE21.Font.Style := LabelE21.Font.Style;//-[fsbold];

		Ancien := ComboType.text;
		fillComboFreeList(ComboType,
        getBiblioSystem().getBiblioEcs(getProjet().typeBatiment).getTypeEcs(
          ComboSys.Text,
          ComboEnerg.Text));

		if (sender <> nil) then
			selectComboItem(ComboType, ancien)
		else
			selectComboItem(ComboType, getECS().typpe);

		ComboTypeChange(sender);

		if (sender <> nil) then
			getECS().energie := ComboEnerg.text;

		setComboVisible(comboType);

		setStyleFromCombo(labelE31, comboType);

		getProjet().aSauvegarder := True;
		getProjet().aCalculer := True;

		sendOwner(sender);
	end;

	procedure TEcsFrame3CL.ComboInstChange(Sender: TObject);
	Var
		Ancien : String;
	begin
    ComboInst.Hint := ComboInst.text;
		LabelE51.Font.Color := clblack;
		LabelE51.Font.Style := LabelE51.Font.Style;//-[fsbold];

		Ancien := ComboBall.text;
		fillComboFreeList(ComboBall,
        getBiblioSystem().getBiblioEcs(getProjet().typeBatiment).getBallon(
          ComboSys.Text,
					ComboEnerg.Text,
          ComboType.Text,
          ComboStock.text,
          comboInst.Text));

		if (sender <> nil) then
			selectComboItem(ComboBall, ancien)
		else
			selectComboItem(ComboBall, getECS().ballon);

		ComboBallChange(sender);

		if (sender <> nil) then
			getECS().Installation := ComboInst.text;

		setComboVisible(comboBall);
		setStyleFromCombo(LabelE61, ComboBall);
		getProjet().aSauvegarder := True;
		getProjet().aCalculer := True;

		sendOwner(sender);
	end;

procedure TEcsFrame3CL.ComboStockChange(Sender: TObject);
	Var
		Ancien : String;
	begin
    ComboStock.Hint := ComboStock.text;
		LabelE41.Font.Color := clblack;
		LabelE41.Font.Style := LabelE41.Font.Style;//-[fsbold];

		Ancien := ComboInst.text;
		fillComboFreeList(ComboInst,
        getBiblioSystem().getBiblioEcs(getProjet().typeBatiment).getInstallation(
          ComboSys.Text,
					ComboEnerg.Text,
          ComboType.Text,
          ComboStock.text));

		if (sender <> nil) then
			selectComboItem(ComboInst, ancien)
		else
			selectComboItem(ComboInst, getECS().installation);

		ComboInstChange(sender);
		if (sender <> nil) then
			getECS().Stockage := ComboStock.text;

		setComboVisible(ComboInst);

		setStyleFromCombo(LabelE51, ComboInst);
		getProjet().aSauvegarder := True;
		getProjet().aCalculer := True;


		sendOwner(sender);
	end;

	procedure TEcsFrame3CL.ComboSysChange(Sender: TObject);
	Var
		Ancien : String;
	begin
    comboSys.hint := comboSys.text;
		Ancien := ComboEnerg.text;
		fillComboFreeList(ComboEnerg,
        getBiblioSystem().getBiblioEcs(getProjet().typeBatiment).getEnergie(
          ComboSys.Text));

		if (sender <> nil) then
			selectComboItem(comboEnerg, ancien)
		else
			selectComboItem(ComboEnerg, getECS().energie);

		ComboEnergChange(sender);

		if (sender <> nil) then
			getECS().systeme := ComboSys.text;

		setComboVisible(comboEnerg);


		setStyleFromCombo(LabelE21, ComboEnerg);

		getProjet().ASauvegarder := True;
		getProjet().Acalculer := True;

		if lowercase(ComboSys.text) = 'autre' then
		Begin
			EditRdtECS.Readonly := False;
			EditRdtECS.Color := clWindow;
		End
		else
		Begin
			EditRdtECS.Readonly := True;
			EditRdtECS.Color := clBtnFace;
		End;
		sendOwner(sender);
end;

	procedure TEcsFrame3CL.ComboTypeChange(Sender: TObject);
	Var
		Ancien : String;
	begin
    comboType.hint := comboType.text;
		LabelE31.Font.Color := clblack;
		LabelE31.Font.Style := LabelE31.Font.Style;//-[fsbold];

		Ancien := ComboStock.text;
		fillComboFreeList(ComboStock,
        getBiblioSystem().getBiblioEcs(getProjet().typeBatiment).getStockage(
          ComboSys.Text,
					ComboEnerg.Text,
          ComboType.Text));

		if (sender <> nil) then
			selectComboItem(ComboStock, ancien)
		else
			selectComboItem(ComboStock, getECS().stockage);

		ComboStockChange(sender);
		if (sender <> nil) then
			getECS().Typpe := ComboType.text;
		setComboVisible(comboStock);
		if (ComboStock.itemIndex = -1) and (ComboStock.visible = True) then
		Begin
			LabelE41.FOnt.Color := clRed;
			LabelE41.Font.Style := LabelE41.Font.Style;//+[fsbold];
		End
		else
		Begin
			LabelE41.Font.Color := clblack;
			LabelE41.Font.Style := LabelE41.Font.Style;//-[fsbold];
		End;

		getProjet().ASauvegarder := True;
		getProjet().Acalculer := True;

		sendOwner(sender);
	end;

	procedure TEcsFrame3CL.ComboVeilleChange(Sender: TObject);
var
    eqEcs : TIEcsItem;
	begin
    comboVeille.Hint := comboVeille.text;
		LabelE71.Font.Color := clblack;
		LabelE71.Font.Style := LabelE71.Font.Style;//-[fsbold];

		if ((sender <> nil) or (projetDialogie.rescanData)) then
		begin
      eqEcs := getBiblioSystem.getBiblioEcs(getProjet().typeBatiment).getECS(
          ComboSys.Text,
					ComboEnerg.Text,
          ComboType.Text,
          ComboStock.text,
          comboInst.Text,
					ComboBall.text,
          ComboVeille.text);

			EditRdtECS.text := eqEcs.IEcs;

			getECS().Accu := getBool(eqEcs.Accu);

			getECS().Veilleuse := ComboVeille.text;
//			getECS().Veilleuse := ComboVeille.itemIndex;
			getECS().Rdt := getDouble(EditRdtECS.text);
		end
		else
		begin
			EditRdtECS.Text := getString(getECS().rdt);
		end;
		getProjet().ASauvegarder := True;
		getProjet().Acalculer := True;

		sendOwner(sender);
	end;

	procedure TEcsFrame3CL.EditPourcentChange(Sender: TObject);
	begin
		inherited EditPourcentChange(Sender);
		if (sender <> nil) then
		begin
			getProjet().ASauvegarder := True;
			getProjet().Acalculer := True;
		end;
	end;

	procedure TEcsFrame3CL.EditRdtECSChange(Sender: TObject);
	begin
		if (sender <> nil) then
		begin
			getECS().Rdt := getDouble(EditRdtECS.text);
			getProjet().ASauvegarder := True;
			getProjet().Acalculer := True;
			if (EditRdtECS.Text <> '') and (EditRdtECS.Text <> '-0.0') then
				sendOwner(sender, IZ_FRAME_OBJECT_DATA_COMPLETE);
		end;
	end;

	function TECSFrame3CL.getEquipementClass : TIzPercentObjectClass;
	begin
		getEquipementClass := TEquipementECS3CL;
	end;



	procedure TEcsFrame3CL.setEquipement(anEquipement : TIzPercentObject);
	begin
		inherited setEquipement(anEquipement);

    fillComboFreeList(ComboSys, getBiblioSystem.getBiblioEcs(getProjet().typeBatiment).getSysteme());
		selectComboItem(ComboSys, getECS().systeme);
		ComboSysChange(nil);
	end;

	procedure TEcsFrame3CL.setNewEquipement();
	begin
		inherited setNewEquipement();

		fillComboFreeList(ComboSys, getBiblioSystem.getBiblioEcs(getProjet().typeBatiment).getSysteme());
		if (ComboSys.Items.Count > 0) then
			ComboSys.ItemIndex := 0;
		ComboSysChange(self);
	end;

end.
