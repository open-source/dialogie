unit IzEquipementFrame;

interface

uses
	Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
	Dialogs, StdCtrls, IzConstantes, IzObjectUtil;



type
	TEquipementFrame = class(TFrame)
		editPourcent: TEdit;
		labelPourcentSymbol: TLabel;
		labelPourcent: TLabel;
		frameLabel: TGroupBox;
		checkSelection: TCheckBox;
    checkApportInterne: TCheckBox;
		procedure editPourcentChange(Sender: TObject);
    procedure checkApportInterneClick(Sender: TObject);
	private
		{ Déclarations privées }
		innerEquipement : TIzPercentObject;
		function isShowPercent : boolean;
		procedure setShowPercent(newShow : boolean);
		function isShowApportInterne : boolean;
		procedure setShowApportInterne(newShow : boolean);


		function getSelectVisible : boolean;
		procedure setSelectVisible(isSelectVisible : boolean);
		function getSelected : boolean;
		procedure setSelected(isSelected : boolean);

		class var nextId : integer;

	protected
		procedure setEquipement(anEquipement : TIzPercentObject);virtual;
		procedure sendOwner(sender: TObject);overload;
		procedure sendOwner(Sender: TObject; messageId : cardinal);overload;
		class function getNewName() : string;

	public
		property equipement : TIzPercentObject read innerEquipement write setEquipement;
		function getEquipementClass : TIzPercentObjectClass; virtual;
		function createNewItem() : TIzPercentObject; virtual;
		procedure setNewEquipement(); virtual;

		procedure refresh();
		property showPercent : boolean read isShowPercent write setShowPercent;
		property showApportInterne : boolean read isShowApportInterne write setShowApportInterne;

		property selectVisible : boolean read getSelectVisible write setSelectVisible;
		property selected : boolean read getSelected write setSelected;

		constructor Create(AOwner: TComponent ;aParent : TWinControl);reintroduce;overload;virtual;

		{ Déclarations publiques }
	end;

	type TEquipementFrameClass = class of TEquipementFrame;

implementation

uses IzUtilitaires;

{$R *.dfm}


	procedure TEquipementFrame.checkApportInterneClick(Sender: TObject);
	begin
		if innerEquipement <> nil then
			innerEquipement.apportInterne := checkApportInterne.Checked;
	end;

constructor TEquipementFrame.Create(AOwner: TComponent ;aParent : TWinControl);
	begin
		inherited create(AOwner);
		name := getNewName;
		if (aParent <> nil) then
		begin
			parent := aParent;
		end;
		selectVisible := false;
    showApportInterne := false;
		selected := false;
	end;

	procedure TEquipementFrame.editPourcentChange(Sender: TObject);
	begin
		if (sender <> nil) then
		begin
			equipement.Pourcent := getDouble(EditPourcent.Text);
		end;
		sendOwner(sender, IZ_FRAME_OBJECT_PERCENT_CHANGE);
	end;

function TEquipementFrame.getEquipementClass() : TIzPercentObjectClass;
		begin
			getEquipementClass := TIzPercentObject;
		end;

	function TEquipementFrame.createNewItem() : TIzPercentObject;
	begin
		createNewItem := getEquipementClass().Create;
	end;

	procedure TEquipementFrame.setEquipement(anEquipement : TIzPercentObject);
	begin
		innerEquipement := anEquipement;
		refresh;
	end;

	procedure TEquipementFrame.setNewEquipement();
	begin
		innerEquipement := createNewItem;
		refresh;
	end;


	procedure TEquipementFrame.sendOwner(Sender: TObject);
	begin
		sendOwner(sender, IZ_FRAME_CHANGED);
	end;

	procedure TEquipementFrame.sendOwner(Sender: TObject; messageId : cardinal);
	var
		msg : TMessage;
	begin
		if (owner = nil) or not(owner is TControl) then
			exit;
		msg.Msg := messageId;
		msg.WParam := Integer(sender);
		msg.LParam := Integer(self);

		TControl(Owner).WindowProc(msg);
//		parent.WindowProc(msg);
	end;


	procedure TEquipementFrame.refresh();
	begin
    if (innerEquipement <> nil) then
    begin
		  EditPourcent.Text := getString(innerEquipement.Pourcent);
		  checkApportInterne.Checked := innerEquipement.apportInterne;
    end;
	end;

	function TEquipementFrame.isShowPercent : boolean;
	begin
		result := editPourcent.Visible;
	end;

	procedure TEquipementFrame.setShowPercent(newShow : boolean);
	begin
		labelPourcent.Visible := newShow;
		labelPourcentSymbol.Visible := newShow;
		editPourcent.Visible := newShow;
	end;


	function TEquipementFrame.isShowApportInterne : boolean;
	begin
		result := checkApportInterne.Visible;
	end;

	procedure TEquipementFrame.setShowApportInterne(newShow : boolean);
	begin
  	checkApportInterne.Visible := newShow;
  end;


	class function TEquipementFrame.getNewName() : string;
	begin
		result := className + intToStr(nextId);
		inc(nextId);
	end;


	function TEquipementFrame.getSelectVisible : boolean;
	begin
		result := checkSelection.Visible;
	end;


	procedure TEquipementFrame.setSelectVisible(isSelectVisible : boolean);
	begin
		checkSelection.Visible := isSelectVisible;
	end;


	function TEquipementFrame.getSelected : boolean;
	begin
		result := checkSelection.Checked;
	end;


	procedure TEquipementFrame.setSelected(isSelected : boolean);
	begin
		checkSelection.Checked := isSelected;
	end;



initialization
	TEquipementFrame.nextId := 0;
finalization

end.

