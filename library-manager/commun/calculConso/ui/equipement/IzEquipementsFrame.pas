unit IzEquipementsFrame;

interface

uses
	Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
	Dialogs, StdCtrls, Buttons, IzEquipementFrame,
  IzTypeObjectPersist, ExtCtrls;

type
	TEquipementsFrame = class(TFrame)
		buttonAdd: TBitBtn;
		buttonRemove: TBitBtn;
		boxEquipement: TScrollBox;
    panelAction: TPanel;
		procedure buttonAddClick(Sender: TObject);
		procedure FrameResize(Sender: TObject);
		procedure buttonRemoveClick(Sender: TObject);
	private
		innertypeEquipement : TTypeObjectPersist;
		uiClass : TEquipementFrameClass;
		innerMaxElements : integer;
		innerPercentVisible : boolean;
		innerApportInterneVisible : boolean;

		procedure setMaxElements(max : integer);
		procedure checkPercent();
		procedure normalizeButtons();

		class var nextId : integer;

	protected
		function refreshOperations() : boolean;virtual;
		procedure removeFrames();
		procedure sendOwner(sender: TObject);overload;
		procedure sendOwner(Sender: TObject; messageId : cardinal);overload;
		procedure sendOwnerMessage(msg : TMessage);overload;
		function getTypeEquipement() : TTypeObjectPersist;
		procedure setTypeEquipement(aType : TTypeObjectPersist);

		procedure setPercentVisible(isVisible : boolean);
		procedure setApportInterneVisible(isVisible : boolean);

		class function getNewName() : string;

	public
		constructor Create(AOwner: TComponent ; aParent : TWinControl);reintroduce;overload;virtual;
		constructor Create(AOwner: TComponent ; aParent : TWinControl ; anUiClass : TEquipementFrameClass); reintroduce;overload;virtual;
		constructor Create(AOwner: TComponent ; aParent : TWinControl ; anUiClass : TEquipementFrameClass ; aType : TTypeObjectPersist); reintroduce;overload;virtual;
		destructor Destroy();override;

		procedure removeAll();
		procedure refresh();

		property typeEquipement : TTypeObjectPersist read getTypeEquipement write setTypeEquipement;
		property maxElements : integer read innerMaxElements write setMaxElements;
		property percentVisible : boolean read innerPercentVisible write setPercentVisible;
		property apportInterneVisible : boolean read innerApportInterneVisible write setApportInterneVisible;

		procedure setAddLabel(newLabel : string);
		procedure setRemoveLabel(newLabel : string);


		procedure WndProc(var msg : TMessage);override;



		{ Déclarations publiques }
	end;

implementation

uses
	Contnrs, IzUiUtil, IzConstantes, Math, IzObjectUtil;

{$R *.dfm}


procedure TEquipementsFrame.buttonAddClick(Sender: TObject);
	var
		frame : TEquipementFrame;
	begin
    if innertypeEquipement = nil then
      exit;


		frame := uiClass.create(self, boxEquipement);
		frame.selectVisible := true;
		frame.showPercent := innerPercentVisible;
		frame.showApportInterne := innerApportInterneVisible;

		frame.setNewEquipement;
		innertypeEquipement.addObject(frame.equipement);

		frame.Align := alTop;
//		frame.Top := (innertypeEquipement.nb - 1) * frame.Height;
		//Frame.Width := BoxEquipement.ClientWidth;

		if (innertypeEquipement.nb = 1) then
			frame.equipement.pourCent := 100
		else
			frame.equipement.pourCent := 0;
		frame.refresh;
		refreshOperations;
		checkPercent();

	end;


	constructor TEquipementsFrame.Create(AOwner: TComponent ; aParent : TWinControl);
	begin
		inherited create(AOwner);
		name := getNewName();
		if (aParent <> nil) then
		begin
			parent := aParent;
		end;

		innerPercentVisible := true;
		innerApportInterneVisible := false;

//		if (innertypeEquipement = nil) then
//    begin
//			innertypeEquipement := TTypeObjectPersist.Create;
//    end;
		maxElements := -1;

		setAddLabel('Ajouter');
		setRemoveLabel('Supprimer');
	end;


	destructor TEquipementsFrame.Destroy();
	begin
//		innertypeEquipement.free;
		inherited destroy;
	end;



	procedure TEquipementsFrame.FrameResize(Sender: TObject);
	begin
(*
		boxEquipement.Height := self.Height - (buttonAdd.Height + 2);
		boxEquipement.Width := self.Width - 2;
*)
	end;



	constructor TEquipementsFrame.Create(AOwner: TComponent ; aParent : TWinControl ; anUiClass : TEquipementFrameClass);
	begin
		create(aOwner, aParent);
		uiClass := anUiClass;
	end;

	constructor TEquipementsFrame.Create(AOwner: TComponent ; aParent : TWinControl ; anUiClass : TEquipementFrameClass ; aType : TTypeObjectPersist);
	begin
		if (aType <> nil) then
			innertypeEquipement := aType;
//		setTypeEquipement(aType);
		create(aOwner, aParent, anUiClass);
	end;


	function TEquipementsFrame.getTypeEquipement() : TTypeObjectPersist;
	begin
		getTypeEquipement := innertypeEquipement;
	end;

	procedure TEquipementsFrame.setTypeEquipement(aType : TTypeObjectPersist);
	begin
		if (aType <> nil) then
		begin
			innertypeEquipement := aType;
			refresh;
		end;
	end;

	procedure TEquipementsFrame.buttonRemoveClick(Sender: TObject);
		var
//			item : TControl;
		persistFrame : TEquipementFrame;
  cpt: integer;
	begin
    if innertypeEquipement = nil then
      exit;
//nouvelle version : on supprime tous les éléments sélectionnés
		for cpt := boxEquipement.ControlCount - 1 downto 0 do
		begin
			persistFrame := TEquipementFrame(boxEquipement.Controls[cpt]);
			if persistFrame.selected then
			begin
				boxEquipement.RemoveControl(persistFrame);
				innertypeEquipement.removeObject(persistFrame.equipement);
				persistFrame.Free;
				sendOwner(sender, IZ_FRAME_CONTAINER_OBJECT_REMOVED);
			end;
		end;
		refreshOperations;
		checkPercent();



(*
		innertypeEquipement.remove(innertypeEquipement.nb - 1);
		item := boxEquipement.Controls[boxEquipement.ControlCount - 1];
		boxEquipement.RemoveControl(item);
		item.Free;
		refreshOperations;
		checkPercent();
		sendOwner(sender, IZ_FRAME_CONTAINER_OBJECT_REMOVED);
*)
	end;

function TEquipementsFrame.refreshOperations() : boolean;
	var
		ret : boolean;
	begin
    if innertypeEquipement = nil then
    begin
      ret := false;
		  buttonRemove.Enabled := false;
    end
    else
    begin
		  ret := ((innerMaxElements < 0) or (innertypeEquipement.nb < innerMaxElements));
		  buttonRemove.Enabled := (innertypeEquipement.nb > 0);
    end;
		buttonAdd.enabled := ret;
		result := ret;
	end;


	procedure TEquipementsFrame.setMaxElements(max : integer);
	begin
		innerMaxElements := max;
		refreshOperations;
	end;


	procedure TEquipementsFrame.removeAll();
	begin
    if innertypeEquipement <> nil then
		  innertypeEquipement.removeAll;
		removeFrames;
	end;


	procedure TEquipementsFrame.removeFrames();
	var
		cpt: Integer;
		item : TControl;
	begin
		for cpt := boxEquipement.ControlCount - 1 downto 0 do
		begin
			item := boxEquipement.Controls[cpt];
			boxEquipement.RemoveControl(item);
			item.Free;
		end;
		refreshOperations;
	end;


	procedure TEquipementsFrame.refresh();
	var
		equipement : TIzPercentObject;
		frame : TEquipementFrame;
		cpt: Integer;
	begin
		removeFrames;
    if innertypeEquipement = nil then
      exit;

		for cpt := 0 to innertypeEquipement.nb - 1 do
		begin
			frame := uiClass.create(self, boxEquipement);
			frame.selectVisible := true;
			frame.showPercent := innerPercentVisible;
      frame.showApportInterne := innerApportInterneVisible;

			equipement := TIzPercentObject(innertypeEquipement.getObject(cpt));
			frame.equipement := equipement;

			frame.Align := alTop;
(*
			frame.Top := (cpt) * frame.Height;
*)
		end;

		refreshOperations;
		checkPercent;
	end;



	procedure TEquipementsFrame.WndProc(var msg : TMessage);
	begin
		if (msg.Msg = IZ_FRAME_OBJECT_PERCENT_CHANGE) then
		begin
			checkPercent();
		end;

		//et on fait remonter tous les messages qui sont des messages 'IZUBA'
		if (msg.Msg >= IZ_FRAME_CHANGED) and (msg.msg < WM_APP) then
		begin
			sendOwnerMessage(msg);
			exit;
		end;
		inherited WndProc(msg);
	end;



	procedure TEquipementsFrame.checkPercent();
	var
		equipement : TIzPercentObject;
		frame : TEquipementFrame;
		cpt : integer;
		cumul : double;
		normalStyle : boolean;
		chaine : string;
	begin
		cumul := 0;
		for cpt := boxEquipement.ControlCount - 1 downto 0 do
		begin
			frame := TEquipementFrame(boxEquipement.Controls[cpt]);

			equipement := frame.equipement;

			cumul := cumul + equipement.pourCent;
		end;

		normalStyle :=  (cumul = 100);
		for cpt := boxEquipement.ControlCount - 1 downto 0 do
		begin
			frame := TEquipementFrame(boxEquipement.Controls[cpt]);
			setStyle(frame.labelPourcent, normalStyle);
		end;

		ShowHint := not(normalStyle);
		if not(normalStyle) then
			str(cumul:0:2, chaine);
			hint := 'Total des pourcentages : ' + chaine + '% !';
	end;



	procedure TEquipementsFrame.setPercentVisible(isVisible : boolean);
	var
		cpt: integer;
		frame : TEquipementFrame;
	begin
		innerPercentVisible := isVisible;
		for cpt := boxEquipement.ControlCount - 1 downto 0 do
		begin
			frame := TEquipementFrame(boxEquipement.Controls[cpt]);
			frame.showPercent := innerPercentVisible;
		end;
	end;

	procedure TEquipementsFrame.setApportInterneVisible(isVisible : boolean);
	var
		cpt: integer;
		frame : TEquipementFrame;
	begin
		innerApportInterneVisible := isVisible;
		for cpt := boxEquipement.ControlCount - 1 downto 0 do
		begin
			frame := TEquipementFrame(boxEquipement.Controls[cpt]);
			frame.showApportInterne := innerApportInterneVisible;
		end;
	end;


	procedure TEquipementsFrame.setAddLabel(newLabel : string);
	begin
		setButtonText(buttonAdd, newLabel);
		normalizeButtons;
	end;


	procedure TEquipementsFrame.setRemoveLabel(newLabel : string);
	begin
		setButtonText(buttonRemove, newLabel);
		normalizeButtons;
	end;

	procedure TEquipementsFrame.normalizeButtons();
	var
		maxW : integer;
	begin
		maxW := max(buttonAdd.Width, buttonRemove.Width);
		buttonAdd.Width := maxW;
		buttonRemove.Width := maxW;
    buttonRemove.Left := buttonAdd.Left + buttonAdd.Width + 8;
	end;


	procedure TEquipementsFrame.sendOwner(Sender: TObject);
	begin
		sendOwner(sender, IZ_FRAME_CHANGED);
	end;

	procedure TEquipementsFrame.sendOwner(Sender: TObject; messageId : cardinal);
	var
		msg : TMessage;
	begin
		if (owner = nil) then
			exit;
		msg.Msg := messageId;
		msg.WParam := Integer(sender);
		msg.LParam := Integer(self);

		TControl(Owner).WindowProc(msg);
//		parent.WindowProc(msg);
	end;

	procedure TEquipementsFrame.sendOwnerMessage(msg : TMessage);
	begin
		if (owner = nil) or not(owner is TControl) then
			exit;
		TControl(Owner).WindowProc(msg);
//		parent.WindowProc(msg);
	end;



	class function TEquipementsFrame.getNewName() : string;
	begin
		result := className + intToStr(nextId);
		inc(nextId);
	end;

initialization
	TEquipementsFrame.nextId := 0;
finalization



end.
