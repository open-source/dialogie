object EquipementFrame: TEquipementFrame
  Left = 0
  Top = 0
  Width = 342
  Height = 62
  TabOrder = 0
  TabStop = True
  object frameLabel: TGroupBox
    Left = 19
    Top = 0
    Width = 323
    Height = 62
    Align = alClient
    Caption = 'Syst'#232'me'
    TabOrder = 0
    object labelPourcent: TLabel
      Left = 23
      Top = 26
      Width = 117
      Height = 13
      Alignment = taRightJustify
      Caption = 'Pourcentage d'#39'utilisation'
    end
    object labelPourcentSymbol: TLabel
      Left = 196
      Top = 26
      Width = 11
      Height = 13
      HelpContext = 20
      Caption = '%'
    end
    object editPourcent: TEdit
      Left = 146
      Top = 23
      Width = 44
      Height = 21
      TabOrder = 0
      Text = 'editPourcent'
      OnChange = editPourcentChange
    end
    object checkApportInterne: TCheckBox
      Left = 224
      Top = 25
      Width = 97
      Height = 17
      Caption = 'Zone chauff'#233'e'
      Checked = True
      State = cbChecked
      TabOrder = 1
      OnClick = checkApportInterneClick
    end
  end
  object checkSelection: TCheckBox
    Left = 0
    Top = 0
    Width = 19
    Height = 62
    Hint = 'Cocher pour suppression'
    Align = alLeft
    ParentShowHint = False
    ShowHint = True
    TabOrder = 1
  end
end
