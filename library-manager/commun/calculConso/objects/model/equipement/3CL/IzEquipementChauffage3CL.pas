unit IzEquipementChauffage3CL;

interface

uses
	IzEquipementChauffage, Classes, IzUtilitaires, GestionSauvegarde,
  CreerSauvegarde, Xml.XMLIntf;


	type TEquipementChauffage3CL = class(TEquipementChauffage)
//		private
//			innerChaudiereAppoint : TEquipementChauffage3CL;
		protected


			Procedure innerSaveList(liste : TStringList);override;
			procedure innerLoadList(liste : TStringList);override;
			procedure innerLoadList(liste : TSauvegarde);override;

			procedure innerSaveComplement(liste : TStringList);
			procedure innerLoadComplement(liste : TStringList);overload;
			procedure innerLoadComplement(liste : TSauvegarde);overload;

      procedure innerLoadXml(node : IXMLNode); override;
      procedure innerSaveXml(node : IXMLNode); override;
		protected
//			procedure setChaudiereAppoint(chauff3CL : TEquipementChauffage3CL);


		public
			Chauffage : string;
			Generateur : string;
			Energie : string;
			TypeC : string;
			Emetteur : string;
			Robinet : string;
			Programmateur : string;

			ichIndicatif : Double;
			Accu : Boolean;


//ajout pour DPE
			isolationReseau : string;
			option1 : string;
			alphaPCSI : double;
			ichDpe : double;
			progDpe : double;
			Rd : double;
			Re : double;
			Rg : double;
			Rr : double;

			A1 : double;
			A2 : double;
//			corCh : boolean;
//			appointChaudiere : boolean;

//			property chaudiereAppoint : TEquipementChauffage3CL read innerChaudiereAppoint write setChaudiereAppoint;
//      function hasChaudiereAppoint() : boolean;inline;

			constructor Create; override;
			destructor Destroy; override;

			//incompatible avec ce qu'on trouve dans dialogie o� la sauvegarde imbrique les valeurs de chaque
			//�quipement
			//le saveToList n'est en principe plus utilis� et le
			//loadFromList est conserv� pour charger les ancienne sauvegardes
			procedure SaveToList(Liste : TStringList);
			procedure LoadFromList(Liste : TStringList);

			function getIch(besoinChauff : double) : double;
			function getLabel() : string;override;
			function getLabelFull() : string;
			function getLabelNoEnergy() : string;
			function getLabelSeparator() : string;
	end;

implementation

uses
	SysUtils,

  IzObject,
  XmlUtil,
  UtilitairesDialogie;

	Constructor TEquipementChauffage3CL.Create;
	Begin
		inherited create;
		Chauffage := '';
		Generateur := '';
		Energie := '';
		TypeC := '';
		Emetteur := '';
		Robinet := '';
		Programmateur := '';

		Pourcent := 100;
		ichIndicatif := 0;
		Accu := False;

//ajout pour DPE
		isolationReseau :='';
		option1 := '';
		alphaPCSI := 1;
		ichDpe := 0;
		progDpe := 0;
		Rd := 0;
		Re := 0;
		Rg := 0;
		Rr := 0;

		A1 := 0;
		A2 := 0;
//		corCh := false;

//		appointChaudiere := false;
//		innerChaudiereAppoint := nil;
	end;


	destructor TEquipementChauffage3CL.Destroy;
	begin
		inherited destroy;
//		setChaudiereAppoint(nil);
	end;

//	procedure TEquipementChauffage3CL.setChaudiereAppoint(chauff3CL : TEquipementChauffage3CL);
//	begin
//		if (innerChaudiereAppoint <> nil) then
//    begin
//			if (chauff3CL <> innerChaudiereAppoint) then
//				innerChaudiereAppoint.Free;
//		end;
//    innerChaudiereAppoint := chauff3CL;
//	end;

	Procedure TEquipementChauffage3CL.LoadFromList(Liste : TStringList);
	Begin
		chauffage := removeFirst(liste);
		pourcent := getDouble(removeFirst(liste));
		Energie := removeFirst(liste);
		ichIndicatif := getDouble(removeFirst(liste));
		Accu := removeFirst(liste) = '1';
		Generateur := removeFirst(liste);
		TypeC := removeFirst(liste);
		Emetteur := removeFirst(liste);
		Robinet := removeFirst(liste);
		Programmateur := removeFirst(liste);
  	innerLoadComplement(liste);
	End;



	Procedure TEquipementChauffage3CL.SaveToList(Liste : TStringList);
	//ce serait bien mieux en changeant le format pour regrouper les appareils
	//==> on pourrait faire une boucle for
	Begin
		Liste.Add(Chauffage);
		Liste.add(getString(pourcent));
		Liste.add(Energie);
		Liste.add(getString(ichIndicatif));
		liste.add(getString(accu));

		liste.add(Generateur);
		Liste.add(TypeC);
		Liste.add(Emetteur);
		Liste.add(Robinet);
		Liste.add(Programmateur);
		innerSaveComplement(liste);
	End;



	function TEquipementChauffage3CL.getLabel() : string;
	var
		ret : string;
	begin
		ret  := Chauffage + ' ';

		ret := ret + Energie + ' ';
		ret := ret + Generateur + ' ';
		ret := ret + TypeC + ' ';
		ret := ret + Emetteur;
  result := TDialogieUtil.cleanSeparatorLabel(ret);
	end;


function TEquipementChauffage3CL.getLabelFull: string;
begin
  result := getLabel + self.robinet + ' ' + robinet;
end;

function TEquipementChauffage3CL.getLabelNoEnergy: string;
var
  ret : string;
begin
  ret  := Chauffage + ' ';

  ret := ret + Generateur + ' ';
  ret := ret + TypeC + ' ';
  ret := ret + Emetteur;
  result := TDialogieUtil.cleanSeparatorLabel(ret);
end;

function TEquipementChauffage3CL.getLabelSeparator: string;
var
  ret : string;
begin
  ret  := Chauffage + ' \ ';

  ret := ret + Generateur + ' \ ';
  ret := ret + TypeC + ' \ ';
  ret := ret + Emetteur;
  result := TDialogieUtil.cleanSeparatorLabel(ret);
end;


	function TEquipementChauffage3CL.getIch(besoinChauff : double) : double;
//	var
//		correctionChauff : double;
//		dbl : double;
//    dblApp : double;
	begin

		result := ichIndicatif


//		correctionChauff := 0;
//		if (corCh) then
//		begin
//    	//pour �tre conforme 3CL
//			if (besoinChauff < 2000) then // 2000-
//			begin
//				correctionChauff := 1.7 - 0.0006 * besoinChauff;
//			end
//			else if besoinChauff < 6000 then //2000� 6000
//			begin
//				correctionChauff := 0.75 - 0.000125 * besoinChauff;
//			end
//		end;
//		dbl := Rg * Re * Rd * Rr;
//		if dbl = 0 then
//			// si on a pas les valeurs, on retombe sur le pr�d�fini
//			dbl := ichIndicatif
//		else
//			dbl := progDpe * (1/dbl + correctionChauff) * alphaPCSI;
////    if hasChaudiereAppoint() then
////    begin
////      dblApp := chaudiereAppoint.getIch(besoinChauff);
////      dbl := 0.7 * min(dbl, dblApp) + 0.3 * max(dbl, dblApp);
////    end;
//
//    result := dbl;
	end;

//  function TEquipementChauffage3CL.hasChaudiereAppoint() : boolean;
//  begin
//    result := appointChaudiere and (chaudiereAppoint <> nil);
//  end;







procedure TEquipementChauffage3CL.innerSaveList(liste : TStringList);
	begin
		inherited innerSaveList(liste);
		Liste.Add(setTag('systeme', Chauffage));
		Liste.add(setTag('energie', Energie));
		Liste.add(setTag('ich', ichIndicatif));
		liste.add(setTag('accumulation', accu));

		liste.add(setTag('generateur', Generateur));
		Liste.add(setTag('type', TypeC));
		Liste.add(setTag('emetteur', Emetteur));
		Liste.add(setTag('robinet', Robinet));
		Liste.add(setTag('programmateur', Programmateur));
		innerSaveComplement(liste);
	end;


	procedure TEquipementChauffage3CL.innerLoadList(liste : TStringList);
	begin
		inherited innerLoadList(liste);
		chauffage := getTagString(Liste, 'systeme');
		Energie := getTagString( removeFirst(liste), 'energie');
		ichIndicatif := getTagDouble(Liste, 'ich');
		Accu :=  getTagBool(Liste, 'accumulation');
		Generateur :=  getTagString(Liste, 'generateur');
		TypeC :=  getTagString(Liste, 'type');
		Emetteur :=  getTagString(Liste, 'emetteur');
		Robinet :=  getTagString(Liste, 'robinet');
		Programmateur :=  getTagString(Liste, 'programmateur');
		innerLoadComplement(liste);
	end;


	procedure TEquipementChauffage3CL.innerLoadList(liste : TSauvegarde);
	begin
		inherited innerLoadList(liste);
		chauffage := getTagString(liste.getLigne(), 'systeme');
		Energie := getTagString( liste.getLigne(), 'energie');
		ichIndicatif := getTagDouble(liste.getLigne(), 'ich');
		Accu :=  getTagBool(liste.getLigne(), 'accumulation');
		Generateur :=  getTagString(liste.getLigne(), 'generateur');
		TypeC :=  getTagString(liste.getLigne(), 'type');
		Emetteur :=  getTagString(liste.getLigne(), 'emetteur');
		Robinet :=  getTagString(liste.getLigne(), 'robinet');
		Programmateur :=  getTagString(liste.getLigne(), 'programmateur');
		innerLoadComplement(liste);
	end;


	procedure TEquipementChauffage3CL.innerSaveComplement(liste : TStringList);
	begin
		Liste.Add(setTag('isolation-reseau', isolationReseau));
		Liste.add(setTag('option1', option1));
		Liste.add(setTag('alpha-pcsi', alphaPCSI));
		Liste.add(setTag('ich-dpe', ichDpe));
		liste.add(setTag('prog-dpe', progDpe));

		liste.add(setTag('Rd', Rd));
		Liste.add(setTag('Re', Re));
		Liste.add(setTag('Rg', Rg));
		Liste.add(setTag('Rr', Rr));

		Liste.add(setTag('A1', A1));
		Liste.add(setTag('A2', A1));
//		Liste.add(setTag('cor-ch', corCh));
//		Liste.add(setTag('appoint-chaudiere', appointChaudiere));
//    if (appointChaudiere) then
//    begin
//      Liste.add(setTag('appoint-present', hasChaudiereAppoint()));
//      if hasChaudiereAppoint then
//      begin
//        chaudiereAppoint.saveList(liste);
//      end;
//    end;

	end;


	procedure TEquipementChauffage3CL.innerLoadComplement(liste : TStringList);
//  var
//     appointPresent : boolean;
	begin
		if not(isTag(liste, 'isolation-reseau')) then
		begin
			exit;
		end;
		isolationReseau := getTagString(Liste, 'isolation-reseau');
		option1 := getTagString(Liste, 'option1');
		alphaPCSI := getTagDouble(Liste, 'alpha-pcsi');
		ichDpe := getTagDouble(Liste, 'ich-dpe');
		progDpe := getTagDouble(Liste, 'prog-dpe');;
		Rd := getTagDouble(Liste, 'Rd');
		Re := getTagDouble(Liste, 'Re');
		Rg := getTagDouble(Liste, 'Rg');
		Rr := getTagDouble(Liste, 'Rr');

		A1 := getTagDouble(Liste, 'A1');
		A2 := getTagDouble(Liste, 'A2');
//		corCh := getTagBool(Liste, 'cor-ch');
//		appointChaudiere := getTagBool(Liste, 'appoint-chaudiere');
//    if appointChaudiere then
//    begin
//		   appointPresent := getTagBool(Liste, 'appoint-present');
//       if appointPresent then
//       begin
//         chaudiereAppoint := TEquipementChauffage3CL.Create;
//         chaudiereAppoint.loadList(liste);
//       end;
//
//    end;
	end;


	procedure TEquipementChauffage3CL.innerLoadComplement(liste : TSauvegarde);
  var
     appointPresent : boolean;
	begin
		if not(isTag(liste.GetLigneSansBouger, 'isolation-reseau')) then
		begin
			exit;
		end;
		isolationReseau := getTagString(liste.getLigne(), 'isolation-reseau');
		option1 := getTagString(liste.getLigne(), 'option1');
		alphaPCSI := getTagDouble(liste.getLigne(), 'alpha-pcsi');
		ichDpe := getTagDouble(liste.getLigne(), 'ich-dpe');
		progDpe := getTagDouble(liste.getLigne(), 'prog-dpe');;
		Rd := getTagDouble(liste.getLigne(), 'Rd');
		Re := getTagDouble(liste.getLigne(), 'Re');
		Rg := getTagDouble(liste.getLigne(), 'Rg');
		Rr := getTagDouble(liste.getLigne(), 'Rr');

		A1 := getTagDouble(liste.getLigne(), 'A1');
		A2 := getTagDouble(liste.getLigne(), 'A2');
//		corCh := getTagBool(liste.getLigne(), 'cor-ch');
//		appointChaudiere := getTagBool(liste.getLigne(), 'appoint-chaudiere');
//    if appointChaudiere then
//    begin
//		   appointPresent := getTagBool(liste.getLigne(), 'appoint-present');
//       if appointPresent then
//       begin
//         chaudiereAppoint := TEquipementChauffage3CL.Create;
//         chaudiereAppoint.loadList(liste);
//       end;
//
//    end;
	end;



procedure TEquipementChauffage3CL.innerSaveXml(node : IXmlNode);
	begin
		inherited innerSaveXml(node);
		TXmlUtil.setTag(node, 'systeme', Chauffage);
		TXmlUtil.setTag(node, 'energie', Energie);
		TXmlUtil.setTag(node, 'ich', ichIndicatif);
		TXmlUtil.setTag(node, 'accumulation', accu);

		TXmlUtil.setTag(node, 'generateur', Generateur);
		TXmlUtil.setTag(node, 'type', TypeC);
		TXmlUtil.setTag(node, 'emetteur', Emetteur);
		TXmlUtil.setTag(node, 'robinet', Robinet);
		TXmlUtil.setTag(node, 'programmateur', Programmateur);

		TXmlUtil.setTag(node, 'isolation-reseau', isolationReseau);
		TXmlUtil.setTag(node, 'option1', option1);
		TXmlUtil.setTag(node, 'alpha-pcsi', alphaPCSI);
		TXmlUtil.setTag(node, 'ich-dpe', ichDpe);
		TXmlUtil.setTag(node, 'prog-dpe', progDpe);

		TXmlUtil.setTag(node, 'Rd', Rd);
		TXmlUtil.setTag(node, 'Re', Re);
		TXmlUtil.setTag(node, 'Rg', Rg);
		TXmlUtil.setTag(node, 'Rr', Rr);

		TXmlUtil.setTag(node, 'A1', A1);
		TXmlUtil.setTag(node, 'A2', A1);
//		Liste.add(setTag('cor-ch', corCh));
//		Liste.add(setTag('appoint-chaudiere', appointChaudiere));
//    if (appointChaudiere) then
//    begin
//      Liste.add(setTag('appoint-present', hasChaudiereAppoint()));
//      if hasChaudiereAppoint then
//      begin
//        chaudiereAppoint.saveList(liste);
//      end;
//    end;

	end;

	procedure TEquipementChauffage3CL.innerLoadXml(node : IXmlNode);
	begin
		inherited innerLoadXml(node);
		chauffage := TXmlUtil.getTagString(node, 'systeme');
		Energie := TXmlUtil.getTagString(node, 'energie');
		ichIndicatif := TXmlUtil.getTagDouble(node, 'ich');
		Accu :=  TXmlUtil.getTagBool(node, 'accumulation');
		Generateur :=  TXmlUtil.getTagString(node, 'generateur');
		TypeC :=  TXmlUtil.getTagString(node, 'type');
		Emetteur :=  TXmlUtil.getTagString(node, 'emetteur');
		Robinet :=  TXmlUtil.getTagString(node, 'robinet');
		Programmateur :=  TXmlUtil.getTagString(node, 'programmateur');

		isolationReseau := TXmlUtil.getTagString(node, 'isolation-reseau');
		option1 := TXmlUtil.getTagString(node, 'option1');
		alphaPCSI := TXmlUtil.getTagDouble(node, 'alpha-pcsi');
		ichDpe := TXmlUtil.getTagDouble(node, 'ich-dpe');
		progDpe := TXmlUtil.getTagDouble(node, 'prog-dpe');;
		Rd := TXmlUtil.getTagDouble(node, 'Rd');
		Re := TXmlUtil.getTagDouble(node, 'Re');
		Rg := TXmlUtil.getTagDouble(node, 'Rg');
		Rr := TXmlUtil.getTagDouble(node, 'Rr');

		A1 := TXmlUtil.getTagDouble(node, 'A1');
		A2 := TXmlUtil.getTagDouble(node, 'A2');
//		corCh := getTagBool(Liste, 'cor-ch');
//		appointChaudiere := getTagBool(Liste, 'appoint-chaudiere');
//    if appointChaudiere then
//    begin
//		   appointPresent := getTagBool(Liste, 'appoint-present');
//       if appointPresent then
//       begin
//         chaudiereAppoint := TEquipementChauffage3CL.Create;
//         chaudiereAppoint.loadList(liste);
//       end;
//
//    end;
	end;



end.
