unit IzEquipementEcs3CL;

interface

uses
	IzEquipementEcs, IzUtilitaires, Classes, GestionSauvegarde, CreerSauvegarde,
  Xml.XMLIntf;

	
	type TEquipementEcs3CL = class(TEquipementEcs)
		protected
			Procedure innerSaveList(liste : TStringList);override;
			procedure innerLoadList(liste : TStringList);override;
			procedure innerLoadList(liste : TSauvegarde);override;

      procedure innerLoadXml(node : IXMLNode); override;
      procedure innerSaveXml(node : IXMLNode); override;
		public
			systeme,
			energie,
			typpe,
			stockage,
			installation,
			ballon,
			veilleuse : string;
			rdt : Double;
			Accu : Boolean;

			constructor Create ; override;

			procedure SaveToList(Liste : TStringList);
			procedure LoadFromList(Liste : TStringList);

			function getLabel() : string;override;
			function getLabelNoEnergy() : string;
			function getLabelSeparator() : string;

	end;

implementation

uses
	SysUtils,
  UtilitairesDialogie,
  IzObject,
  XmlUtil;

Constructor TEquipementEcs3CL.Create;
	Begin
		inherited Create;
		systeme := '';
		energie := '';
		typpe := '';
		stockage := '';
		installation := '';
		ballon := '';
		veilleuse := '';

		rdt := 0;
		Accu := False;
	End;


	Procedure TEquipementEcs3CL.SaveToList(Liste : TStringList);
	Begin
		Liste.Add(systeme);
		Liste.Add(energie);
		Liste.Add(typpe);
		Liste.Add(stockage);
		Liste.Add(installation);
		Liste.Add(ballon);
		Liste.Add(veilleuse);
		Liste.add(getString(pourcent));
		Liste.add(getString(rdt));
		liste.add(getString(accu));
	End;

	Procedure TEquipementECS3CL.LoadFromList;
	Begin
		systeme := removeFirst(liste);
		energie := removeFirst(liste);
		typpe := removeFirst(liste);
		stockage := removeFirst(liste);
		installation := removeFirst(liste);
		ballon := removeFirst(liste);
		veilleuse := removeFirst(liste);
		pourCent := getDouble(removeFirst(liste));
		rdt := getDouble(removeFirst(liste));
		Accu := getBool(removeFirst(liste));
	End;


function TEquipementECS3CL.getLabel() : string;
var
  ret : string;
begin
  ret  := systeme + ' ';

  ret := ret + Energie + ' ';
  ret := ret + typpe + ' ';
  ret := ret + stockage + ' ';
  ret := ret + installation;
  ret := StringReplace(ret, '*', '',[rfReplaceAll]);
  ret := StringReplace(ret, '  ', ' ',[rfReplaceAll]);
  result := ret;
end;



function TEquipementEcs3CL.getLabelNoEnergy: string;
var
  ret : string;
begin
  ret  := systeme + ' ';
  ret := ret + stockage + ' ';
  ret := ret + installation;
  ret := StringReplace(ret, '*', '',[rfReplaceAll]);
  ret := StringReplace(ret, '  ', ' ',[rfReplaceAll]);
  ret := StringReplace(ret, '""', '"',[rfReplaceAll]);
  result := ret;
end;

function TEquipementEcs3CL.getLabelSeparator: string;
var
  ret : string;
begin
  ret  := systeme + ' \ ';
  ret := ret + stockage + ' \ ';
  ret := ret + installation;
  result := TDialogieUtil.cleanSeparatorLabel(ret);
end;

procedure TEquipementECS3CL.innerSaveList(liste : TStringList);
	begin
		inherited innerSaveList(liste);
		Liste.Add(setTag('systeme', systeme));
		Liste.Add(setTag('energie', energie));
		Liste.Add(setTag('type', typpe));
		Liste.Add(setTag('stockage', stockage));
		Liste.Add(setTag('installation', installation));
		Liste.Add(setTag('ballon', ballon));
		Liste.Add(setTag('veilleuse', veilleuse));
		Liste.add(setTag('rendement', rdt));
		liste.add(setTag('accumulation', accu));
	end;


	procedure TEquipementECS3CL.innerLoadList(liste : TStringList);
	begin
		inherited innerLoadList(liste);
		systeme := getTagString( removeFirst(liste), 'systeme');
		energie := getTagString( removeFirst(liste), 'energie');
		typpe := getTagString( removeFirst(liste), 'type');
		stockage := getTagString( removeFirst(liste), 'stockage');
		installation := getTagString( removeFirst(liste), 'installation');
		ballon := getTagString( removeFirst(liste), 'ballon');
		veilleuse := getTagString( removeFirst(liste), 'veilleuse');
		rdt := getTagDouble(Liste, 'rendement');
		Accu := getTagBool(Liste, 'accumulation');
	end;

  procedure TEquipementECS3CL.innerSaveXml(node : IXmlNode);
	begin
		inherited innerSaveXml(node);
		TXmlUtil.setTag(node, 'systeme', systeme);
		TXmlUtil.setTag(node, 'energie', energie);
		TXmlUtil.setTag(node, 'type', typpe);
		TXmlUtil.setTag(node, 'stockage', stockage);
		TXmlUtil.setTag(node, 'installation', installation);
		TXmlUtil.setTag(node, 'ballon', ballon);
		TXmlUtil.setTag(node, 'veilleuse', veilleuse);
		TXmlUtil.setTag(node, 'rendement', rdt);
		TXmlUtil.setTag(node, 'accumulation', accu);
	end;


	procedure TEquipementECS3CL.innerLoadXml(node : IXmlNode);
	begin
		inherited innerLoadXml(node);
		systeme := TXmlUtil.getTagString(node, 'systeme');
		energie := TXmlUtil.getTagString(node, 'energie');
		typpe := TXmlUtil.getTagString(node, 'type');
		stockage := TXmlUtil.getTagString(node, 'stockage');
		installation := TXmlUtil.getTagString(node, 'installation');
		ballon := TXmlUtil.getTagString(node, 'ballon');
		veilleuse := TXmlUtil.getTagString(node, 'veilleuse');
		rdt := TXmlUtil.getTagDouble(node, 'rendement');
		Accu := TXmlUtil.getTagBool(node, 'accumulation');
	end;


	procedure TEquipementECS3CL.innerLoadList(liste : TSauvegarde);
	begin
		inherited innerLoadList(liste);
		systeme := getTagString( liste.getLigne(), 'systeme');
		energie := getTagString( liste.getLigne(), 'energie');
		typpe := getTagString( liste.getLigne(), 'type');
		stockage := getTagString( liste.getLigne(), 'stockage');
		installation := getTagString( liste.getLigne(), 'installation');
		ballon := getTagString( liste.getLigne(), 'ballon');
		veilleuse := getTagString( liste.getLigne(), 'veilleuse');
		rdt := getTagDouble(liste.getLigne(), 'rendement');
		Accu := getTagBool(liste.getLigne(), 'accumulation');
	end;

end.
