unit IzEquipementEcsSolaire3CL;

interface

uses
	IzEquipementEcsSolaire, Classes, GestionSauvegarde;

	type
		TmenuSolaire = array[1..7] of String;

		TCapteur = Record
				Nom : String;
				Car1,Car2 : Double;
		End;

		TCarac = Record
				Circulation : Integer;
				Circulateur : Double;
				Echangeur, Appoint : Integer;
				Tuyau1,Tuyau2,Regulation, Debit : Double;
		End;

	
	type TEquipementEcsSolaire3CL = class(TEquipementEcsSolaire)
		protected
			procedure innerSaveList(liste : TStringList);override;
			procedure innerLoadList(liste : TStringList);override;
			procedure innerLoadList(liste : TSauvegarde);override;
	end;

implementation

(*$message warn 'il va falloir prendre en compte ce type d �quipement et le g�rer dans dialogie'*)
	procedure TEquipementEcsSolaire3CL.innerSaveList(liste : TStringList);
	begin
		inherited innerSaveList(liste);
	end;


	procedure TEquipementEcsSolaire3CL.innerLoadList(liste : TStringList);
	begin
		inherited innerLoadList(liste);
	end;


	procedure TEquipementEcsSolaire3CL.innerLoadList(liste : TSauvegarde);
	begin
		inherited innerLoadList(liste);
	end;

end.
