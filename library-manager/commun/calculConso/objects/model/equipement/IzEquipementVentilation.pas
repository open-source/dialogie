unit IzEquipementVentilation;

interface

uses
	Classes,
  IzObjectUtil,
  GestionSauvegarde,
  CreerSauvegarde,
  Xml.XMLIntf;


	type TEquipementVentilation = class(TIzPercentObject)
		protected
			Procedure innerSaveList(liste : TStringList);override;
			procedure innerLoadList(liste : TStringList);override;
			procedure innerLoadList(liste : TSauvegarde);override;

      procedure innerLoadXml(node : IXMLNode); override;
      procedure innerSaveXml(node : IXMLNode); override;
		public
			libelle : string;
			volH : double;
			constructor Create; override;

			procedure SaveToList(Liste : TStringList);
			procedure LoadFromList(Liste : TStringList);

			function getLabel() : string;override;
	end;

implementation


uses
	SysUtils,
  IzUtilitaires,
  XmlUtil;

	Constructor TEquipementVentilation.Create;
	Begin
		inherited create;
		innerTagName := 'ventilation';
		libelle := '';
		volH := 0;
end;




	Procedure TEquipementVentilation.LoadFromList(Liste : TStringList);
	Begin
		libelle := removeFirst(liste);
		volH := getDouble(removeFirst(liste));
	End;



	Procedure TEquipementVentilation.SaveToList(Liste : TStringList);
	var
		chaine : string;
	Begin
		Liste.Add(libelle);
		Str(volH,Chaine);
		Liste.add(Chaine);
	End;

	function TEquipementVentilation.getLabel() : string;
	begin
		result := libelle;
	end;



	procedure TEquipementVentilation.innerSaveList(liste : TStringList);
	begin
		inherited innerSaveList(liste);
		liste.add(setTag('systeme', libelle));
		liste.add(setTag('vol-h', volH));
	end;


	procedure TEquipementVentilation.innerLoadList(liste : TStringList);
	begin
		inherited innerLoadList(liste);
		libelle := getTagString(Liste, 'systeme');
		volH := getTagDouble(Liste, 'vol-h');
	end;

	procedure TEquipementVentilation.innerSaveXml(node : IXmlNode);
	begin
		inherited innerSaveXml(node);
		TXmlUtil.setTag(node, 'systeme', libelle);
		TXmlUtil.setTag(node, 'vol-h', volH);
	end;


	procedure TEquipementVentilation.innerLoadXml(node : IXmlNode);
	begin
		inherited innerLoadXml(node);
		libelle := TXmlUtil.getTagString(node, 'systeme');
		volH := TXmlUtil.getTagDouble(node, 'vol-h');
	end;


	procedure TEquipementVentilation.innerLoadList(liste : TSauvegarde);
	begin
		inherited innerLoadList(liste);
		libelle := getTagString(liste.getLigne(), 'systeme');
		volH := getTagDouble(liste.getLigne(), 'vol-h');
	end;

end.
