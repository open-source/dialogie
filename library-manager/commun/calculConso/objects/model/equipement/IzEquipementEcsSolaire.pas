unit IzEquipementEcsSolaire;

interface

uses
	IzObjectPersist;


	type TEquipementEcsSolaire = class(TObjectPersist)
		constructor Create();reintroduce;override;

	end;

implementation

	constructor TEquipementEcsSolaire.Create();
	begin
		inherited create();
		innerTagName := 'ecs-solaire';
	end;
end.
