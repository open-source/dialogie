unit IzEquipementClimatisation;

interface

uses
	Classes, IzUtilitaires, IzObjectUtil, GestionSauvegarde, CreerSauvegarde,
  Xml.XMLIntf;


	type TEquipementClimatisation = class(TIzPercentObject)
		protected
			Procedure innerSaveList(liste : TStringList);override;
			procedure innerLoadList(liste : TStringList);override;
			procedure innerLoadList(liste : TSauvegarde);override;

      procedure innerLoadXml(node : IXMLNode); override;
      procedure innerSaveXml(node : IXMLNode); override;
		public
			Climatisation : string;
			Energie : string;
			pourcent : Double;
			iClim : Double;

			constructor Create; override;

			procedure SaveToList(Liste : TStringList);
			procedure LoadFromList(Liste : TStringList);

	end;

implementation

uses
	SysUtils, XmlUtil;

	Constructor TEquipementClimatisation.Create;
	Begin
		inherited create;
		innerTagName := 'climatisation';

		Climatisation := '';
		Energie := '';
		iClim := 0;
		pourcent := 100;
end;


	Procedure TEquipementClimatisation.LoadFromList(Liste : TStringList);
	Begin
		 Climatisation := removeFirst(liste);
		 Energie := removeFirst(liste);
		 iCLim := getDouble(removeFirst(liste));
		 pourcent := getDouble(removeFirst(liste));
	End;



Procedure TEquipementClimatisation.SaveToList(Liste : TStringList);
Begin

		 Liste.Add(Climatisation);
		 Liste.add(Energie);
		 Liste.add(getString(iClim));
		 Liste.add(getString(pourcent));
End;



	procedure TEquipementClimatisation.innerSaveList(liste : TStringList);
	begin
		inherited innerSaveList(liste);
		liste.Add(setTag('type', Climatisation));
		liste.Add(setTag('energie', Energie));
		liste.Add(setTag('i-clim', iClim));
	end;


	procedure TEquipementClimatisation.innerLoadList(liste : TStringList);
	begin
		inherited innerLoadList(liste);
		climatisation := getTagString(Liste ,'type');
		energie := getTagString(Liste ,'energie');
		iClim := getTagDouble(Liste ,'i-clim');
	end;

	procedure TEquipementClimatisation.innerSaveXml(node : IXmlNode);
	begin
		inherited innerSaveXml(node);
		TXmlUtil.setTag(node, 'type', Climatisation);
		TXmlUtil.setTag(node, 'energie', Energie);
		TXmlUtil.setTag(node, 'i-clim', iClim);
	end;


	procedure TEquipementClimatisation.innerLoadXml(node : IXmlNode);
	begin
		inherited innerLoadXml(node);
		climatisation := TXmlUtil.getTagString(node ,'type');
		energie := TXmlUtil.getTagString(node ,'energie');
		iClim := TXmlUtil.getTagDouble(node ,'i-clim');
	end;


	procedure TEquipementClimatisation.innerLoadList(liste : TSauvegarde);
	begin
		inherited innerLoadList(liste);
		climatisation := getTagString(liste.getLigne() ,'type');
		energie := getTagString(liste.getLigne() ,'energie');
		iClim := getTagDouble(liste.getLigne() ,'i-clim');
	end;
end.
