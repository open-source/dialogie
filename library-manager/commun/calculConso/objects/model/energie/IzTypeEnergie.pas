unit IzTypeEnergie;

interface

uses
  IzUtilitaires,
  Classes,
  IzTypeObjectPersist,
  IzEnergies3CL,
  IzConstantesConso,
  Xml.XMLIntf;

type
  TTypeEnergie = class(TTypeObjectPersist)
  protected

    procedure innerLoadXml(node : IXMLNode); override;
    procedure innerSaveXml(node : IXMLNode); override;
  public
    constructor Create; override;

    function getElectricite() : TIzEnergie;

    function getEnergie(energieName : string) : TIzEnergie;
    function setEnergie(energie : TIzEnergie) : boolean;

    function prixAbonnement(energie : TIzEnergie) : Double;
    function prixkWh(energie : TIzEnergie) : Double;
    function prixEnergie(energie : TIzEnergie; Usage : TTypeUsage) : Double;

    function emissionCO2(energie : TIzEnergie; Usage : TTypeUsage) : Double;
    function emissionNucleaire(energie : TIzEnergie; Usage : TTypeUsage) : Double;
    function emissionSO2(energie : TIzEnergie; Usage : TTypeUsage) : Double;
// Function emissionTEP(energie : TIzEnergie; Usage : TTypeUsage) : Double;
    procedure saveToList(Liste : TStringList);
    procedure loadFromList(Liste : TStringList);

  end;

implementation

uses
  Contnrs,
  IzBiblioSystem,
  IzObject,
  SysUtils;


constructor TTypeEnergie.Create;
begin
  inherited Create();
  innerTagName := 'energies';
  contentClass := TIzEnergie;
end;


function TTypeEnergie.getElectricite: TIzEnergie;
begin
  result := getEnergie('Electricité')
end;

function TTypeEnergie.getEnergie(energieName : string) : TIzEnergie;
var
  energie : TIzEnergie;
  i : Integer;
begin
  getEnergie := nil;
  for i := 0 to nb - 1 do
  begin
    energie := TIzEnergie(getObject(i));
    if energie.nomEnergie = energieName then
    begin
      getEnergie := energie;
      exit;
    end;
  end;
end;

function TTypeEnergie.setEnergie(energie : TIzEnergie) : boolean;
var
  obj : TIzEnergie;
begin
  setEnergie := false;
  if energie = nil then
    exit;
  obj := getEnergie(energie.nomEnergie);
  if obj <> nil then
  begin
    removeObject(obj);
  end;
  setEnergie := (addObject(energie) <> -1);
end;

function TTypeEnergie.prixAbonnement(energie : TIzEnergie) : Double;
begin
  prixAbonnement := getBiblioSystem().biblioEnergieTarif.prixAbonnement(energie.fournisseur, energie.typeTarif,
    energie.nomEnergie);
end;


function TTypeEnergie.prixkWh(energie : TIzEnergie) : Double;
begin
  prixkWh := getBiblioSystem().biblioEnergieTarif.prixkWh(energie.fournisseur, energie.typeTarif, energie.nomEnergie);
end;

function TTypeEnergie.prixEnergie(energie : TIzEnergie; Usage : TTypeUsage) : Double;
begin
  prixEnergie := getBiblioSystem().biblioEnergieTarif.prixEnergie(energie.fournisseur, energie.typeTarif,
    energie.nomEnergie, Usage);
end;


function TTypeEnergie.emissionCO2(energie : TIzEnergie; Usage : TTypeUsage) : Double;
begin
  emissionCO2 := getBiblioSystem().biblioEnergieTarif.emissionCO2(energie.fournisseur, energie.typeTarif,
    energie.nomEnergie, Usage);
end;

function TTypeEnergie.emissionNucleaire(energie : TIzEnergie; Usage : TTypeUsage) : Double;
begin
  emissionNucleaire := getBiblioSystem().biblioEnergieTarif.emissionNucleaire(energie.fournisseur, energie.typeTarif,
    energie.nomEnergie, Usage);
end;

function TTypeEnergie.emissionSO2(energie : TIzEnergie; Usage : TTypeUsage) : Double;
begin
  emissionSO2 := getBiblioSystem().biblioEnergieTarif.emissionSO2(energie.fournisseur, energie.typeTarif,
    energie.nomEnergie, Usage);
end;
(*
		Function TTypeEnergie.emissionTEP(energie : TIzEnergie; Usage : TTypeUsage) : Double;
		begin
			emissionTEP := getBiblioSystem().biblioEnergieTarif.emissionTEP(energie.fournisseur, energie.typeTarif, energie.nomEnergie, usage);
		end;
*)

procedure TTypeEnergie.saveToList(Liste : TStringList);
var
  i : Integer;
  energie : TIzEnergie;
begin
  Liste.add(intToStr(nb));
  for i := 0 to nb - 1 do
  begin
    energie := TIzEnergie(getObject(i));
    energie.saveToList(Liste);
  end;
end;

procedure TTypeEnergie.loadFromList(Liste : TStringList);
var
  i : Integer;
  cpt : Integer;
  energie : TIzEnergie;
begin
  removeAll;
  cpt := getInt(removeFirst(Liste));
  for i := 0 to cpt - 1 do
  begin
    energie := TIzEnergie.Create;
    energie.loadFromList(Liste);
    addObject(energie);
  end;
end;


procedure TTypeEnergie.innerSaveXml(node : IXmlNode);
var
  i : Integer;
  energie : TIzEnergie;
begin
  for i := 0 to nb - 1 do
  begin
    energie := TIzEnergie(getObject(i));
    energie.saveXml(node);
  end;
end;

procedure TTypeEnergie.innerLoadXml(node : IXmlNode);
var
  i : Integer;
  energie : TIzEnergie;
  child : IXmlNode;
begin
  removeAll;
  for i := 0 to node.ChildNodes.Count - 1 do
  begin
    child := node.ChildNodes[i];
    energie := TIzEnergie.Create;
    energie.loadXml(child);
    addObject(energie);
  end;
end;


end.
