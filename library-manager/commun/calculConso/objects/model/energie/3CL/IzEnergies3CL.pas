unit IzEnergies3CL;

interface

uses
  Classes,
  IzObjectPersist,
  GestionSauvegarde,
  CreerSauvegarde,
  Xml.XMLIntf;


type
  TIzEnergie = class(TObjectPersist)
  protected
    procedure innerSaveList(liste : TStringList); override;
    procedure innerLoadList(liste : TStringList); override;
    procedure innerLoadList(liste : TSauvegarde); override;

    procedure innerLoadXml(node : IXMLNode); override;
    procedure innerSaveXml(node : IXMLNode); override;
  public
    nomEnergie : string;
    fournisseur : string;
    typeTarif : string;

    constructor Create; override;

    procedure SaveToList(liste : TStringList);
    procedure LoadFromList(liste : TStringList);
  end;

implementation

uses
  SysUtils,
  IzConstantes,
  IzUtilitaires, XmlUtil;

procedure TIzEnergie.SaveToList;
begin
  liste.Add(nomEnergie);
  liste.Add(fournisseur);
  liste.Add(typeTarif);
end;

procedure TIzEnergie.LoadFromList(liste : TStringList);
begin
  nomEnergie := removeFirst(liste);
  fournisseur := removeFirst(liste);
  typeTarif := removeFirst(liste);
end;

constructor TIzEnergie.Create;
begin
  inherited Create;
  innerTagName := 'energie';
  nomEnergie := '';
  fournisseur := '';
  typeTarif := CHAINE_VIDE;
end;


procedure TIzEnergie.innerSaveList(liste : TStringList);
begin
  liste.Add(setTag('nom', nomEnergie));
  liste.Add(setTag('fournisseur', fournisseur));
  liste.Add(setTag('type-tarif', typeTarif));
end;


procedure TIzEnergie.innerLoadList(liste : TSauvegarde);
begin
  nomEnergie := getTagString(liste.getLigne(), 'nom');
  fournisseur := getTagString(liste.getLigne(), 'fournisseur');
  typeTarif := getTagString(liste.getLigne(), 'type-tarif');
end;


procedure TIzEnergie.innerLoadList(liste : TStringList);
begin
  nomEnergie := getTagString(Liste, 'nom');
  fournisseur := getTagString(Liste, 'fournisseur');
  typeTarif := getTagString(Liste, 'type-tarif');
end;



procedure TIzEnergie.innerSaveXml(node : IXmlNode);
begin
  TXmlUtil.setTag(node, 'nom', nomEnergie);
  TXmlUtil.setTag(node, 'fournisseur', fournisseur);
  TXmlUtil.setTag(node, 'type-tarif', typeTarif);
end;


procedure TIzEnergie.innerLoadXml(node : IXmlNode);
begin
  nomEnergie := TXmlUtil.getTagString(node, 'nom');
  fournisseur := TXmlUtil.getTagString(node, 'fournisseur');
  typeTarif := TXmlUtil.getTagString(node, 'type-tarif');
end;


end.
