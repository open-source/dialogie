unit IzCalculConso3CL;

interface

uses
  IzCalculConso,
  IzUtilitaires,
  Classes,
  GestionSauvegarde,
  Xml.XMLIntf;

(*
pour faire un calcul de conso conforme � la m�thode 3CL
on consid�re que la couverture des besoins par le solaire a d�j� �t�
prise en compte dans les besoins fournis :

CchPCS = Bch � (1-Fch) � Ich
==> le bch fourni sera de Bch � (1-Fch)
idem pour ECS


*)


type
  TCalculConso3CL = class(TCalculConso)
  private
// innerAppointBois : boolean;
  protected
    managing : boolean;
    typeBatiment : TTypeBatiment;
    typeChauffageAppart : TTypeChauffageAppart;


    procedure innerLoadList(liste : TSauvegarde); override;
    procedure innerLoadList(liste : TStringList); override;
    procedure innerSaveList(liste : TStringList); override;

    procedure innerLoadXml(node : IXMLNode); override;
    procedure innerSaveXml(node : IXMLNode); override;
// procedure setAppointBois(valeur : boolean);virtual;
// function getAppointBois():boolean;virtual;

			// gestion du chauffage
    function getConsoChauffKWhAnnuel() : double; override;

			// get/set percent pour un �quipement
    procedure setEqChauffagePercent(aRank : integer; aPercentage : double); override;


      // caract�ristiques de clim
    class function getRClimIndiv(zoneEte : string; surfaceClim : double) : double;
    class function getRClimCollectif(zoneEte : string; dernierEtage : boolean) : double;


  public


    constructor Create(); reintroduce; virtual;


    procedure setTypeBatiment(tb : TTypeBatiment);
    procedure setTypeChauffageAppart(tca : TTypeChauffageAppart);




// property appointBois : boolean read getAppointBois write setAppointBois;

// fonctions d'int�r�t g�n�ral
    class function getTef(zoneHiver : string) : double;
    class function getBecs(surface : double; zoneHiver : string) : double;
    class function getConsoClim(zoneEte : string; typeBatiment : TTypeBatiment; surfaceClim : double;
      dernierEtage : boolean; energieElec : boolean) : double;
    class function getRClim(zoneEte : string; typeBatiment : TTypeBatiment; surfaceClim : double;
      dernierEtage : boolean) : double;
  end;

implementation

uses
  SysUtils;


constructor TCalculConso3CL.Create();
begin
  inherited Create();
  managing := false;
  typeBatiment := BATIMENT_Maison;
  typeChauffageAppart := CHAUFFAGE_APPART_INDIVIDUEL;
// appointBois := false;
end;


procedure TCalculConso3CL.setTypeBatiment(tb : TTypeBatiment);
begin
  typeBatiment := tb;
end;


procedure TCalculConso3CL.setTypeChauffageAppart(tca : TTypeChauffageAppart);
begin
  typeChauffageAppart := tca;
end;

function TCalculConso3CL.getConsoChauffKWhAnnuel() : double;
begin

(*$MESSAGE warn 'finaliser le calcul' *)

(*
			case typeBatiment of
				BATIMENT_Maison:
				begin
					result := getConsoChauffKWhAnnuelIndiv();
				end;
				BATIMENT_Appartement:
				begin
					case typeChauffageAppart of
						CHAUFFAGE_APPART_COLLECTIF_AVEC_COMPTEUR:
						begin
							result := getConsoChauffKWhAnnuelCollecChIndiv();
						end;
						CHAUFFAGE_APPART_INDIVIDUEL:
						begin
							result := getConsoChauffKWhAnnuelCollecChCollecSansCompteur();
						end;
						CHAUFFAGE_APPART_COLLECTIF_SANS_COMPTEUR:
						begin
							result := getConsoChauffKWhAnnuelCollecChCollecAvecCompteur();
						end;
					end;
				end;
			end;
*)
  result := inherited getConsoChauffKWhAnnuel();
			// et ici, qqe soit le type de batiment on va traiter l'appoint bois
// if (appointBois) then
// begin
// //on arrive ici avec un seul �quipement qui a un pourcentage d'utilisation de 100
// //et on va ramener �a � ce qui est indiqu� dans 3CL :
// //S�il y a un syst�me de chauffage (Ich1) et un insert ou po�le � bois :
// //Cch1PCS = 0.75 � Bch � Ich1
// //Cch2PCS = 0.25 � Bch � 2   --> ich fixe de 2 pour l'appoint bois
// result := 0.75 * result + 0.25 * getBesoinChauffAnnuel() * 2;
// end;
end;


procedure TCalculConso3CL.setEqChauffagePercent(aRank : integer; aPercentage : double);
begin
  inherited setEqChauffagePercent(aRank, aPercentage);
end;



// procedure TCalculConso3CL.setAppointBois(valeur : boolean);
// begin
// innerAppointBois := valeur;
// end;
//
// function TCalculConso3CL.getAppointBois():boolean;
// begin
// result := innerAppointBois;
// end;


procedure TCalculConso3CL.innerLoadList(liste : TSauvegarde);
begin
  inherited innerLoadList(liste);
// if (isFullTag(liste.GetLigneSansBouger, 'appoint-bois')) then
// begin
// appointBois := getTagBool(liste.GetLigne, 'appoint-bois');
// end;
end;


procedure TCalculConso3CL.innerLoadList(liste : TStringList);
begin
  inherited innerLoadList(liste);
// if (isFullTag(liste, 'appoint-bois')) then
// begin
// appointBois := getTagBool(liste, 'appoint-bois');
// end;
 if (isFullTag(liste, 'appoint-bois')) then
   removeFirst(liste);
end;


procedure TCalculConso3CL.innerSaveList(liste : TStringList);
begin
  inherited innerSaveList(liste);
end;


procedure TCalculConso3CL.innerLoadXml(node : IXmlNode);
begin
  inherited innerLoadXml(node);
end;


procedure TCalculConso3CL.innerSaveXml(node : IXmlNode);
begin
  inherited innerSaveXml(node);
end;


class function TCalculConso3CL.getBecs(surface : double; zoneHiver : string) : double;
var
  Qecs : double;
begin
  if (surface > 27) then
  begin
    Qecs := 470.9 * ln(surface) - 1075;
  end
  else
  begin
    Qecs := 17.7 * surface;
  end;
// result := 1.163 * Qecs * (40 - getTef(zoneHiver)) * 48 / 1000;
  result := 1163.6 * Qecs * (40 - getTef(zoneHiver)) * 48;
end;


class function TCalculConso3CL.getTef(zoneHiver : string) : double;
begin
  result := -1;
  if (lowerCase(zoneHiver) = 'h1') or (zoneHiver = '1') then
  begin
    result := 10.5;
  end
  else
    if (lowerCase(zoneHiver) = 'h2') or (zoneHiver = '2') then
  begin
    result := 12;
  end
  else
    if (lowerCase(zoneHiver) = 'h3') or (zoneHiver = '3') then
  begin
    result := 14.5;
  end

end;


class function TCalculConso3CL.getConsoClim(zoneEte : string; typeBatiment : TTypeBatiment; surfaceClim : double;
  dernierEtage : boolean; energieElec : boolean) : double;
  // � faire en deux calculs si refroidissement collectif : dernier �tage et autres �tages
var
  rClim : double;
  CORclim : double;
begin
  rClim := getRClim(zoneEte, typeBatiment, surfaceClim, dernierEtage);
  if not(energieElec) and (typeBatiment = BATIMENT_Appartement) then
    // correction seulement pour le collectif :-S
    CORclim := 2.8
  else
    CORclim := 1;
  result := surfaceClim * rClim * CORclim;
end;

class function TCalculConso3CL.getRClim(zoneEte : string; typeBatiment : TTypeBatiment; surfaceClim : double;
  dernierEtage : boolean) : double;
begin
  result := -1;
  case typeBatiment of
    BATIMENT_Maison :
      begin
        result := getRClimIndiv(zoneEte, surfaceClim);
      end;
    BATIMENT_Appartement :
      begin
        result := getRClimCollectif(zoneEte, dernierEtage);
      end;
  end;
end;

class function TCalculConso3CL.getRClimIndiv(zoneEte : string; surfaceClim : double) : double;
begin
  result := -1;
  if lowerCase(zoneEte) = 'ea' then
  begin
    if (surfaceClim < 150) then
      result := 2
    else
      result := 4;
  end
  else
    if lowerCase(zoneEte) = 'eb' then
  begin
    if (surfaceClim < 150) then
      result := 3
    else
      result := 5;
  end
  else
    if lowerCase(zoneEte) = 'ec' then
  begin
    if (surfaceClim < 150) then
      result := 4
    else
      result := 6;
  end
  else
    if lowerCase(zoneEte) = 'ed' then
  begin
    if (surfaceClim < 150) then
      result := 5
    else
      result := 7;
  end;

end;


class function TCalculConso3CL.getRClimCollectif(zoneEte : string; dernierEtage : boolean) : double;
begin
  result := -1;
  if lowerCase(zoneEte) = 'ea' then
  begin
    if dernierEtage then
      result := 1.5
    else
      result := 2;
  end
  else
    if lowerCase(zoneEte) = 'eb' then
  begin
    if dernierEtage then
      result := 2
    else
      result := 3;
  end
  else
    if lowerCase(zoneEte) = 'ec' then
  begin
    if dernierEtage then
      result := 3
    else
      result := 4;
  end
  else
    if lowerCase(zoneEte) = 'ed' then
  begin
    if dernierEtage then
      result := 4
    else
      result := 5;
  end;

end;


end.
