unit IzCalculConso;

interface

uses
  Classes,
  IzObjectUtil,
  IzObjectPersist,
  IzTypeEnergie,
  IzUtilitaires,
  IzEquipementChauffage3CL,
  IzEquipementEcs3CL,
  GestionSauvegarde,
  CreerSauvegarde,
  UtilitairesDialogie,
  Xml.XMLIntf;


type
  TCalculConso = class(TObjectPersist)
  private

    innerBesoinChauffAnnuel : double;
    innerBesoinChauffPourCorrection : double;
    innerBesoinEcsAnnuel : double;
    innerBesoinClimAnnuel : double;

    innertypeEqChauffage : TTypePercentObject;
    innertypeEqEcs : TTypePercentObject;
    innertypeEqEcsSolaire : TTypePercentObject;
    innertypeEqVentilation : TTypePercentObject;

    innertypeEnergie : TTypeEnergie;

			// pas encore de traitement particulier pr�vus pour ces �l�ments
    function getConsoEcsKWhAnnuel() : double; overload;
    function getConsoEcsKWhPrimaireAnnuel() : double;
    function getEmissionCO2EcsAnnuel() : double; overload;
    function getPrixEcsAnnuel() : double; overload;

//    function getConsoClimKWhAnnuel() : double;
//    function getConsoClimKWhPrimaireAnnuel() : double;
//    function getEmissionCO2ClimAnnuel() : double;
//    function getPrixClimAnnuel() : double;

    function getVentilationVolH() : double;


  protected
    stTagName : string;

			// en pr�vision de gestion particuli�re des besoins
    procedure setBesoinChauffAnnuel(dbl : double); virtual;
    function getBesoinChauffAnnuel() : double; virtual;
    procedure setBesoinChauffPourCorrection(dbl : double); virtual;
    function getBesoinChauffPourCorrection() : double; virtual;

    procedure setBesoinEcsAnnuel(dbl : double); virtual;
    function getBesoinEcsAnnuel() : double; virtual;
    procedure setBesoinClimAnnuel(dbl : double); virtual;
    function getBesoinClimAnnuel() : double; virtual;


    function getPercent(aList : TTypePercentObject; aRank : integer) : double; virtual;
    procedure setPercent(aList : TTypePercentObject; aRank : integer; aPercentage : double); virtual;

    procedure innerLoadFromList(Liste : TStringList); overload; virtual;
    procedure innerSaveToList(Liste : TStringList); virtual;

    procedure innerLoadList(Liste : TSauvegarde); override;
    procedure innerLoadList(Liste : TStringList); override;
    procedure innerSaveList(Liste : TStringList); override;

    procedure innerLoadXml(node : IXMLNode); override;
    procedure innerSaveXml(node : IXMLNode); override;


// d�plac� de private en protected en vue d'h�ritage possible pour des traitements particuliers
    function getConsoChauffKWhAnnuel() : double; overload; virtual;
    function getConsoChauffKWhPrimaireAnnuel() : double; virtual;
    function getEmissionCO2ChauffAnnuel() : double; overload; virtual;
    function getPrixChauffAnnuel() : double; overload; virtual;

    function getPrixAbonnementAnnuel() : double; virtual;


// gestion des pourcentages de ces �quipements
			// get/set percent pour un �quipement
			// d�plac� de private en protected en vue d'h�ritage possible pour des traitements particuliers
    function getEqChauffagePercent(aRank : integer) : double; virtual;
    procedure setEqChauffagePercent(aRank : integer; aPercentage : double); virtual;
    function getEqEcsPercent(aRank : integer) : double; virtual;
    procedure setEqEcsPercent(aRank : integer; aPercentage : double); virtual;
//    function getEqClimPercent(aRank : integer) : double; virtual;
//    procedure setEqClimPercent(aRank : integer; aPercentage : double); virtual;
    function getEqVentilPercent(aRank : integer) : double; virtual;
    procedure setEqVentilPercent(aRank : integer; aPercentage : double); virtual;

    function addEnergieAbonnement(nomEnergie : string; var listeNomEnergies : TStringList) : double;

  public

			// les propri�t�s de types d'�quipment/�nergie
    property typeEqChauffage : TTypePercentObject read innertypeEqChauffage;
    property typeEqEcs : TTypePercentObject read innertypeEqEcs;
    property typeEqEcsSolaire : TTypePercentObject read innertypeEqEcsSolaire;
    property typeEqVentilation : TTypePercentObject read innertypeEqVentilation;

    property typeEnergie : TTypeEnergie read innertypeEnergie;


// li� au chauffage
    property besoinChauffAnnuel : double read getBesoinChauffAnnuel write setBesoinChauffAnnuel;
    property besoinChauffPourCorrection : double read getBesoinChauffPourCorrection write setBesoinChauffPourCorrection;
    property consommationChauffKWhAnnuelle : double read getConsoChauffKWhAnnuel;
    property consommationChauffKWhPrimaireAnnuelle : double read getConsoChauffKWhPrimaireAnnuel;
    property emissionCO2ChauffAnnuel : double read getEmissionCO2ChauffAnnuel;
    property prixChauffAnnuel : double read getPrixChauffAnnuel;
    function checkChauffage() : TCalculErreur; overload;
    function checkChauffage(chauff : TEquipementChauffage3CL) : TCalculErreur; overload;

    function getConsoChauffKWhAnnuel(rank : integer) : double; overload; virtual;
    function getEmissionCO2ChauffAnnuel(rank : integer) : double; overload; virtual;
    function getPrixChauffAnnuel(rank : integer) : double; overload; virtual;


			// li� � l'ECS
    property besoinEcsAnnuel : double read getBesoinEcsAnnuel write setBesoinEcsAnnuel;
    property consommationEcsKWhAnnuelle : double read getConsoEcsKWhAnnuel;
    property consommationEcsKWhPrimaireAnnuelle : double read getConsoEcsKWhPrimaireAnnuel;
    property emissionCO2EcsAnnuel : double read getEmissionCO2EcsAnnuel;
    property prixEcsAnnuel : double read getPrixEcsAnnuel;
    function checkEcs() : TCalculErreur; overload;
    function checkEcs(ecs : TEquipementEcs3CL) : TCalculErreur; overload;

    function getConsoEcsKWhAnnuel(rank : integer) : double; overload; virtual;
    function getEmissionCO2EcsAnnuel(rank : integer) : double; overload; virtual;
    function getPrixEcsAnnuel(rank : integer) : double; overload; virtual;

			// li� � la clim
//    property besoinClimAnnuel : double read getBesoinClimAnnuel write setBesoinClimAnnuel;
//    property consommationClimKWhAnnuelle : double read getConsoClimKWhAnnuel;
//    property consommationClimKWhPrimaireAnnuelle : double read getConsoClimKWhPrimaireAnnuel;
//    property emissionCO2ClimAnnuel : double read getEmissionCO2ClimAnnuel;
//    property prixClimAnnuel : double read getPrixClimAnnuel;
//    function checkClim() : TCalculErreur;


// gestion des pourcentages de ces �quipements
    property eqChauffagePercent[aRank : integer] : double read getEqChauffagePercent write setEqChauffagePercent;
    property eqEcsPercent[aRank : integer] : double read getEqEcsPercent write setEqEcsPercent;
//    property eqClimPercent[aRank : integer] : double read getEqClimPercent write setEqClimPercent;
    property eqVentilPercent[aRank : integer] : double read getEqVentilPercent write setEqVentilPercent;


			// li� � la ventilation
    property ventilationVolH : double read getVentilationVolH;

			// usage global
    property prixAbonnementAnnuel : double read getPrixAbonnementAnnuel;

    procedure saveToFile(filePath : string);
    procedure loadFromFile(filePath : string);

    procedure saveToList(Liste : TStringList);
    procedure loadFromList(Liste : TStringList); overload;
    procedure loadFromList(Liste : TSauvegarde); overload;

    constructor Create(); override;
    destructor Destroy(); override;

  end;

implementation

uses
  System.Contnrs,
  IzTypeObjectPersist,
  IzBiblioSystem,
  Dialogs,
  SysUtils,
  IzEquipementEcsSolaire3CL,
  IzEquipementClimatisation,
  IzEquipementVentilation,
  IzEnergies3CL,
  IzConstantesConso,
  IzConstantes;

constructor TCalculConso.Create();
begin
  inherited Create();

  stTagName := 'calcul-conso';

  innerBesoinChauffAnnuel := -1;
  innerBesoinChauffPourCorrection := -1;
  innerBesoinEcsAnnuel := -1;
  innerBesoinClimAnnuel := -1;
		// 20080220 : maintenant, on cr�� les types d'�quipements ici plut�t que dans l'UI
		// il va donc falloir maintenant affecter le type �quipement du calculConso � l'UI
  innertypeEqChauffage := TTypePercentObject.Create;
  innertypeEqChauffage.tagName := 'equips-chauff';
  innertypeEqEcs := TTypePercentObject.Create;
  innertypeEqEcs.tagName := 'equips-ecs';
  innertypeEqEcsSolaire := TTypePercentObject.Create;
  innertypeEqEcsSolaire.tagName := 'equips-ecs-solaire';
  innertypeEqVentilation := TTypePercentObject.Create;
  innertypeEqVentilation.tagName := 'equips-ventil';

		// 20081114 : valable pour le calculConso de base,
		// peut-�tre � modifier pour marcher avec P+C
  typeEqChauffage.contentClass := TEquipementChauffage3CL;
  typeEqEcs.contentClass := TEquipementEcs3CL;
  typeEqEcsSolaire.contentClass := TEquipementEcsSolaire3CL;
  typeEqVentilation.contentClass := TEquipementVentilation;


  innertypeEnergie := TTypeEnergie.Create;

end;


destructor TCalculConso.Destroy();
begin
  freeAndNil(innertypeEqChauffage);
  freeAndNil(innertypeEqEcs);
  freeAndNil(innertypeEqEcsSolaire);
  freeAndNil(innertypeEqVentilation);

  freeAndNil(innertypeEnergie);
  inherited Destroy();
end;


(* ********************** *)
(* gestion du chauffage *)
(* ********************** *)

function TCalculConso.getConsoChauffKWhAnnuel() : double;
var
  i : integer;
begin
  result := 0;
  for i := 0 to typeEqChauffage.nb - 1 do
  begin
    result := result + getConsoChauffKWhAnnuel(i);
  end;
end;

function TCalculConso.getConsoChauffKWhAnnuel(rank : integer) : double;
var
  chauff : TEquipementChauffage3CL;
begin
  result := 0;
  chauff := TEquipementChauffage3CL(typeEqChauffage.getObject(rank));
  result := (besoinChauffAnnuel * getEqChauffagePercent(rank) / 100 * chauff.getIch(besoinChauffPourCorrection));
// ret := (besoinChauffAnnuel * getEqChauffagePercent(rank) /100 * chauff.Ich);
end;


function TCalculConso.getConsoChauffKWhPrimaireAnnuel() : double;
var
  i : integer;
  chauff : TEquipementChauffage3CL;
  energie : TIzEnergie;
  ret : double;
begin
  ret := 0;

  for i := 0 to typeEqChauffage.nb - 1 do
  begin
    chauff := TEquipementChauffage3CL(typeEqChauffage.getObject(i));
    energie := typeEnergie.getEnergie(chauff.energie);
    ret := ret + (besoinChauffAnnuel * getEqChauffagePercent(i) / 100 * chauff.getIch(besoinChauffPourCorrection) *
      getBiblioSystem().biblioEnergieTarif.ressource(energie, USAGE_CHAUFFAGE_DIRECT, RESSOURCE_ENERGIE_PRIMAIRE));
// ret := ret + (besoinCHauffAnnuel * getEqChauffagePercent(i) /100 * chauff.Ich * getBiblioSystem().biblioEnergieTarif.ressource(energie, USAGE_CHAUFFAGE_DIRECT, RESSOURCE_ENERGIE_PRIMAIRE));
  end;

  getConsoChauffKWhPrimaireAnnuel := ret;
end;


function TCalculConso.getEmissionCO2ChauffAnnuel() : double;
var
  i : integer;
begin
  result := 0;

  for i := 0 to typeEqChauffage.nb - 1 do
  begin
    result := result + getEmissionCO2ChauffAnnuel(i);
  end;
end;

function TCalculConso.getEmissionCO2ChauffAnnuel(rank : integer) : double;
var
  chauff : TEquipementChauffage3CL;
  energie : TIzEnergie;
begin
  result := 0;
  chauff := TEquipementChauffage3CL(typeEqChauffage.getObject(rank));
  energie := typeEnergie.getEnergie(chauff.energie);
  result := (besoinChauffAnnuel * getEqChauffagePercent(rank) / 100 * chauff.getIch(besoinChauffPourCorrection) *
    getBiblioSystem().biblioEnergieTarif.emissionCO2(energie.fournisseur, energie.typeTarif, energie.nomEnergie,
    USAGE_CHAUFFAGE_DIRECT));
// ret := (besoinChauffAnnuel * getEqChauffagePercent(i) /100 * chauff.Ich * getBiblioSystem().biblioEnergieTarif.emissionCO2(energie.fournisseur, energie.typeTarif, energie.nomEnergie, USAGE_CHAUFFAGE_DIRECT));
end;


function TCalculConso.getPrixChauffAnnuel() : double;
var
  i : integer;
begin
  result := 0;

  for i := 0 to typeEqChauffage.nb - 1 do
  begin
    result := result + getPrixChauffAnnuel(i);
  end;

end;

function TCalculConso.getPrixChauffAnnuel(rank : integer) : double;
var
  chauff : TEquipementChauffage3CL;
  energie : TIzEnergie;
begin
  result := 0;
  chauff := TEquipementChauffage3CL(typeEqChauffage.getObject(rank));
  energie := typeEnergie.getEnergie(chauff.energie);
  if energie = nil then
    exit;
  result := (besoinChauffAnnuel * getEqChauffagePercent(rank) / 100 * chauff.getIch(besoinChauffPourCorrection) *
    getBiblioSystem().biblioEnergieTarif.prixEnergie(energie.fournisseur, energie.typeTarif, energie.nomEnergie,
    USAGE_CHAUFFAGE_DIRECT));
// ret := (besoinCHauffAnnuel * getEqChauffagePercent(i) /100 * chauff.Ich * getBiblioSystem().biblioEnergieTarif.prixEnergie(energie.fournisseur, energie.typeTarif, energie.nomEnergie, USAGE_CHAUFFAGE_DIRECT));
end;


function TCalculConso.checkChauffage() : TCalculErreur;
var
  i : integer;
  chauff : TEquipementChauffage3CL;
  totalPercent : double;
begin
  totalPercent := 0;

  if (besoinChauffAnnuel = -1) then
  begin
    result := CALCUL_ERREUR_BESOIN;
// showMessage('Il faut renseigner le besoin de chauffage annuel');
    exit;
  end;

  for i := 0 to typeEqChauffage.nb - 1 do
  begin
    chauff := TEquipementChauffage3CL(typeEqChauffage.getObject(i));
    result := checkChauffage(chauff);
    if (result <> CALCUL_ERREUR_AUCUNE) then
      exit;
// if chauff.hasChaudiereAppoint() then
// begin
// chauff := chauff.chaudiereAppoint;
// result := checkChauffage(chauff);
// if (result <> CALCUL_ERREUR_AUCUNE) then
// exit;
// end;
    totalPercent := totalPercent + getEqChauffagePercent(i);
  end;

// if (totalPercent <> 100) then
// on peut aussi n'avoir aucun pourcentage : on utilise aucun �quipement
  if (totalPercent <> 100) and (totalPercent <> 0) then
  begin
    result := CALCUL_ERREUR_POURCENTAGE;
// showMessage('Le pourcentage cumul� des �quipements de chauffage est de ' + floatToStr(totalPercent));
    exit;
  end;
  result := CALCUL_ERREUR_AUCUNE;
end;


function TCalculConso.checkChauffage(chauff : TEquipementChauffage3CL) : TCalculErreur;
var
  energie : TIzEnergie;
begin
  energie := typeEnergie.getEnergie(chauff.energie);

  if (chauff.ichIndicatif = 0) and (chauff.Chauffage <> 'Aucun') then
  begin
    if (chauff.energie = '') and (chauff.energie = '*') then
    begin
      result := CALCUL_ERREUR_ENERGIE_EQUIPEMENT;
// showMessage('Il faut renseigner le chauffage ' + intToStr(i+1) + ' pour l''�nergie ' + chauff.Energie);
    end
    else
    begin
      result := CALCUL_ERREUR_EQUIPEMENT;
// showMessage('Il faut renseigner le chauffage ' + intToStr(i+1) );
    end;
    exit;
  end;
  if ((energie = nil) or (energie.typeTarif = CHAINE_VIDE)) and (chauff.Chauffage <> 'Aucun') then
  begin
    result := CALCUL_ERREUR_ENERGIE;
// showMessage('Il faut renseigner l''�nergie ' + chauff.Energie);
    exit;
  end;
  result := CALCUL_ERREUR_AUCUNE;
end;


(* ****************** *)
(* gestion de l'ECS *)
(* ****************** *)
function TCalculConso.getConsoEcsKWhAnnuel() : double;
var
  i : integer;
begin
  result := 0;
  for i := 0 to typeEqEcs.nb - 1 do
  begin
    result := result + getConsoEcsKWhAnnuel(i);
  end;
end;

function TCalculConso.getConsoEcsKWhAnnuel(rank : integer) : double;
var
  ecs : TEquipementEcs3CL;
begin
  result := 0;
  ecs := TEquipementEcs3CL(typeEqEcs.getObject(rank));
  result := (besoinEcsAnnuel * getEqEcsPercent(rank) / 100 * ecs.rdt);
end;


function TCalculConso.getConsoEcsKWhPrimaireAnnuel() : double;
var
  i : integer;
  ecs : TEquipementEcs3CL;
  energie : TIzEnergie;
  ret : double;
begin
  ret := 0;

  for i := 0 to typeEqEcs.nb - 1 do
  begin
    ecs := TEquipementEcs3CL(typeEqEcs.getObject(i));
    energie := typeEnergie.getEnergie(ecs.energie);
    ret := ret + (besoinEcsAnnuel * getEqEcsPercent(i) / 100 * ecs.rdt * getBiblioSystem()
      .biblioEnergieTarif.ressource(energie, USAGE_ECS_DIRECT, RESSOURCE_ENERGIE_PRIMAIRE));
  end;

  getConsoEcsKWhPrimaireAnnuel := ret;
end;


function TCalculConso.getEmissionCO2EcsAnnuel() : double;
var
  i : integer;
begin
  result := 0;

  for i := 0 to typeEqEcs.nb - 1 do
  begin
    result := result + getEmissionCO2EcsAnnuel(i);
  end;
end;


function TCalculConso.getEmissionCO2EcsAnnuel(rank : integer) : double;
var
  ecs : TEquipementEcs3CL;
  energie : TIzEnergie;
begin
  result := 0;

  ecs := TEquipementEcs3CL(typeEqEcs.getObject(rank));
  energie := typeEnergie.getEnergie(ecs.energie);
  result := (besoinEcsAnnuel * getEqEcsPercent(rank) / 100 * ecs.rdt * getBiblioSystem()
    .biblioEnergieTarif.emissionCO2(energie.fournisseur, energie.typeTarif, energie.nomEnergie, USAGE_ECS_DIRECT));
end;


function TCalculConso.getPrixEcsAnnuel() : double;
var
  i : integer;
begin
  result := 0;
  for i := 0 to typeEqEcs.nb - 1 do
  begin
    result := result + getPrixEcsAnnuel(i);
  end;
end;


function TCalculConso.getPrixEcsAnnuel(rank : integer) : double;
var
  ecs : TEquipementEcs3CL;
  energie : TIzEnergie;
begin
  result := 0;
  ecs := TEquipementEcs3CL(typeEqEcs.getObject(rank));
  energie := typeEnergie.getEnergie(ecs.energie);
  if energie = nil then
    exit;
  result := (besoinEcsAnnuel * getEqEcsPercent(rank) / 100 * ecs.rdt * getBiblioSystem()
    .biblioEnergieTarif.prixEnergie(energie.fournisseur, energie.typeTarif, energie.nomEnergie, USAGE_ECS_DIRECT));
end;


function TCalculConso.checkEcs() : TCalculErreur;
var
  i : integer;
  ecs : TEquipementEcs3CL;
// energie : TIzEnergie;
  totalPercent : double;
begin
  totalPercent := 0;
  checkEcs := CALCUL_ERREUR_AUCUNE;

  if (besoinEcsAnnuel = -1) then
  begin
// showMessage('Il faut renseigner le besoin d''ECS annuel');
    checkEcs := CALCUL_ERREUR_BESOIN;
    exit;
  end;

  for i := 0 to typeEqEcs.nb - 1 do
  begin
    ecs := TEquipementEcs3CL(typeEqEcs.getObject(i));
    result := checkEcs(ecs);
    if (result < CALCUL_ERREUR_AUCUNE) then
      exit;
    totalPercent := totalPercent + getEqEcsPercent(i);
  end;

// on peut aussi n'avoir aucun pourcentage : on utilise aucun �quipement
  if (totalPercent <> 100) and (totalPercent <> 0) then
  begin
    checkEcs := CALCUL_ERREUR_POURCENTAGE;
// showMessage('Le pourcentage cumul� des �quipements d''ECS est de ' + floatToStr(totalPercent));
    exit;
  end;
end;


function TCalculConso.checkEcs(ecs : TEquipementEcs3CL) : TCalculErreur;
var
  energie : TIzEnergie;
begin
  energie := typeEnergie.getEnergie(ecs.energie);

  if (ecs.rdt = 0) and (ecs.systeme <> 'Aucun') then
  begin
    if (ecs.energie = '') or (ecs.energie = '*') then
    begin
      result := CALCUL_ERREUR_ENERGIE_EQUIPEMENT;
// showMessage('Il faut renseigner l''ECS ' + intToStr(i+1) + ' pour l''�nergie ' + ecs.energie);
    end
    else
    begin
      result := CALCUL_ERREUR_EQUIPEMENT;
// showMessage('Il faut renseigner l''ECS ' + intToStr(i+1) );
    end;
    exit;
  end;

  if ((energie = nil) or (energie.typeTarif = CHAINE_VIDE)) and (ecs.systeme <> 'Aucun') then
  begin
    result := CALCUL_ERREUR_ENERGIE;
// showMessage('Il faut renseigner l''�nergie ' + ecs.energie);
    exit;
  end;
  result := CALCUL_ERREUR_AUCUNE;

end;




(* comportement global *)


function TCalculConso.addEnergieAbonnement(nomEnergie : string; var listeNomEnergies : TStringList) : double;
var
  energie : TIzEnergie;
begin
  result := 0;
  energie := typeEnergie.getEnergie(nomEnergie);
  if (energie = nil) or (energie.typeTarif = CHAINE_VIDE) then
  begin
// showMessage('Il faut renseigner l''�nergie ' + chauff.Energie);
    exit;
  end;
  if (listeNomEnergies.indexOf(nomEnergie) = -1) then
  begin
    listeNomEnergies.Add(nomEnergie);
    result := getBiblioSystem.biblioEnergieTarif.prixAbonnement(energie.fournisseur, energie.typeTarif,
      energie.nomEnergie);
  end;
end;


function TCalculConso.getPrixAbonnementAnnuel() : double;
var
  i : integer;
  chauff : TEquipementChauffage3CL;
  ecs : TEquipementEcs3CL;
  clim : TEquipementClimatisation;
  ret : double;
  listeNomEnergies : TStringList;
begin
		// on fait la liste des diff�rentes �nergies de tous les usages, puis on ajoute le prix de tous les abo � cette �nergie
  ret := 0;

  listeNomEnergies := TStringList.Create;

  for i := 0 to typeEqChauffage.nb - 1 do
  begin
    chauff := TEquipementChauffage3CL(typeEqChauffage.getObject(i));
    if (chauff.Chauffage <> '') and (chauff.Chauffage <> 'Aucun') then
    begin
      ret := ret + addEnergieAbonnement(chauff.energie, listeNomEnergies);
    end;
// if chauff.hasChaudiereAppoint() then
// begin
// chauff := chauff.chaudiereAppoint;
// if (chauff.Chauffage <> '') and (chauff.Chauffage <> 'Aucun') then
// begin
// ret := ret + addEnergieAbonnement(chauff.Energie, listeNomEnergies);
// end;
// end;
  end;


  for i := 0 to typeEqEcs.nb - 1 do
  begin
    ecs := TEquipementEcs3CL(typeEqEcs.getObject(i));
    if (ecs.systeme <> '') and (ecs.systeme <> 'Aucun') then
    begin
      ret := ret + addEnergieAbonnement(ecs.energie, listeNomEnergies);
    end;
  end;


//  for i := 0 to typeEqClimatisation.nb - 1 do
//  begin
//    clim := TEquipementClimatisation(typeEqClimatisation.getObject(i));
//    if (clim.Climatisation <> '') and (clim.Climatisation <> 'Aucune') then
//    begin
//      ret := ret + addEnergieAbonnement(clim.energie, listeNomEnergies);
//    end;
//  end;


  result := ret;
end;


function TCalculConso.getVentilationVolH() : double;
var
  vtl : TEquipementVentilation;
  i : integer;
  dbl : double;
begin
  dbl := 0;
  for i := 0 to typeEqVentilation.nb - 1 do
  begin
    vtl := TEquipementVentilation(typeEqVentilation.getObject(i));
    dbl := dbl + vtl.volH * getEqVentilPercent(i);
  end;
  getVentilationVolH := dbl / 100;
end;


procedure TCalculConso.saveToFile(filePath : string);
var
  Liste : TStringList;
begin
  Liste := TStringList.Create;
  saveToList(Liste);
  Liste.saveToFile(filePath);
end;


procedure TCalculConso.loadFromFile(filePath : string);
var
  Liste : TStringList;
begin

  Liste := TStringList.Create;

// ancienne version pour compatibilit�
  if (getSection(filePath, ClassName, Liste)) then
// if (getSection(filePath, firstTagName, liste)) then
  begin
    loadFromList(Liste);
  end;
// prise en compte de la version tagg�e
  if (getSection(filePath, innerTagName, Liste)) then
// if (getSection(filePath, innerTagName, liste)) then
  begin
    loadList(Liste);
  end;
  Liste.Free;
end;


procedure TCalculConso.saveToList(Liste : TStringList);
var
  section : TStringList;
begin

  section := TStringList.Create;

// nouvelle version donc pour remplacer l'ancienne
// innerSaveToList(section);

  innerSaveList(section);

  replaceSectionInList(Liste, stTagName, section);
// replaceSectionInList(liste, innerTagName, section);

  section.Free;
end;


procedure TCalculConso.innerSaveToList(Liste : TStringList);
var
  i : integer;
  chauff : TEquipementChauffage3CL;
  ecs : TEquipementEcs3CL;
  clim : TEquipementClimatisation;
  ventil : TEquipementVentilation;
begin

  typeEnergie.saveToList(Liste);

  Liste.Add(intToStr(typeEqChauffage.nb));
  for i := 0 to typeEqChauffage.nb - 1 do
  begin
    chauff := TEquipementChauffage3CL(typeEqChauffage.getObject(i));
    chauff.saveToList(Liste);
  end;


  Liste.Add(intToStr(typeEqEcs.nb));
  for i := 0 to typeEqEcs.nb - 1 do
  begin
    ecs := TEquipementEcs3CL(typeEqEcs.getObject(i));
    ecs.saveToList(Liste);
  end;

  liste.add('0');
//  Liste.Add(intToStr(typeEqClimatisation.nb));
//  for i := 0 to typeEqClimatisation.nb - 1 do
//  begin
//    clim := TEquipementClimatisation(typeEqClimatisation.getObject(i));
//    clim.saveToList(Liste);
//  end;

  Liste.Add(intToStr(typeEqVentilation.nb));
  for i := 0 to typeEqVentilation.nb - 1 do
  begin
    ventil := TEquipementVentilation(typeEqVentilation.getObject(i));
    ventil.saveToList(Liste);
  end;
end;


procedure TCalculConso.loadFromList(Liste : TStringList);
var
  sectionList : TStringList;
begin
  sectionList := TStringList.Create;
		// special compat' ancienne version
  if getSectionFromList(Liste, stTagName, sectionList) then
  begin
    innerLoadList(sectionList);
  end
  else
  begin
    if getSectionFromList(Liste, innerTagName, sectionList) then
    begin
      innerLoadFromList(sectionList);
    end;
  end;

		// ici on a pas les tags

  sectionList.Free;
end;


procedure TCalculConso.loadFromList(Liste : TSauvegarde);
begin
    // on ne peut plus ouvrir les fichiers de test P+C DPE 2008
    // pas compatible avec les TSauvegarde
  if Liste.ouvreSection(stTagName) then
  begin
    innerLoadList(Liste);
    Liste.FermeSection;
  end
end;


procedure TCalculConso.innerLoadFromList(Liste : TStringList);
var
  nb : integer;
  i : integer;

  chauff : TEquipementChauffage3CL;
  ecs : TEquipementEcs3CL;
  clim : TEquipementClimatisation;
  ventil : TEquipementVentilation;

begin

  typeEnergie.loadFromList(Liste);

  typeEqChauffage.removeAll;
  nb := getInt(removeFirst(Liste));
  for i := 0 to nb - 1 do
  begin
    chauff := TEquipementChauffage3CL.Create;
    chauff.loadFromList(Liste);
    typeEqChauffage.addObject(chauff);
  end;

  typeEqEcs.removeAll;
  nb := getInt(removeFirst(Liste));
  for i := 0 to nb - 1 do
  begin
    ecs := TEquipementEcs3CL.Create;
    ecs.loadFromList(Liste);
    typeEqEcs.addObject(ecs);
  end;

  typeEqVentilation.removeAll;
  nb := getInt(removeFirst(Liste));
  for i := 0 to nb - 1 do
  begin
    ventil := TEquipementVentilation.Create;
    ventil.loadFromList(Liste);
    typeEqVentilation.addObject(ventil);
  end;

end;


procedure TCalculConso.innerLoadList(Liste : TStringList);
var
  errorStdEnergy : boolean;
begin
  inherited innerLoadList(Liste);
  errorStdEnergy := Liste[1] = '<nb>0</nb>';
  typeEnergie.loadList(Liste);

  typeEqChauffage.loadList(Liste);
  typeEqEcs.loadList(Liste);
//  typeEqClimatisation.loadList(Liste);
  typeEqVentilation.loadList(Liste);

end;


procedure TCalculConso.innerLoadList(Liste : TSauvegarde);
begin
  inherited innerLoadList(Liste);

  typeEnergie.loadList(Liste);

  typeEqChauffage.loadList(Liste);
  typeEqEcs.loadList(Liste);
//  typeEqClimatisation.loadList(Liste);
  typeEqVentilation.loadList(Liste);

end;


procedure TCalculConso.innerSaveList(Liste : TStringList);
begin
  inherited innerSaveList(Liste);

  typeEnergie.saveList(Liste);

  typeEqChauffage.saveList(Liste);
  typeEqEcs.saveList(Liste);
//  typeEqClimatisation.saveList(Liste);
  typeEqVentilation.saveList(Liste);
end;


procedure TCalculConso.innerLoadXml(node : IXmlNode);
begin
  typeEnergie.loadParentXml(node);
  typeEqChauffage.loadParentXml(node);
  typeEqEcs.loadParentXml(node);
//  typeEqClimatisation.loadParentXml(node);
  typeEqVentilation.loadParentXml(node);

end;


procedure TCalculConso.innerSaveXml(node : IXmlNode);
begin
  typeEnergie.saveXml(node);
  typeEqChauffage.saveXml(node);
  typeEqEcs.saveXml(node);
//  typeEqClimatisation.saveXml(node);
  typeEqVentilation.saveXml(node);
end;



procedure TCalculConso.setBesoinChauffAnnuel(dbl : double);
begin
  innerBesoinChauffAnnuel := dbl;
end;

function TCalculConso.getBesoinChauffAnnuel() : double;
begin
  result := innerBesoinChauffAnnuel;
end;

procedure TCalculConso.setBesoinChauffPourCorrection(dbl : double);
begin
  innerBesoinChauffPourCorrection := dbl;
end;

function TCalculConso.getBesoinChauffPourCorrection() : double;
begin
  if innerBesoinChauffPourCorrection <> -1 then
    result := innerBesoinChauffAnnuel
  else
    result := innerBesoinChauffPourCorrection;
end;


procedure TCalculConso.setBesoinEcsAnnuel(dbl : double);
begin
  innerBesoinEcsAnnuel := dbl;
end;

function TCalculConso.getBesoinEcsAnnuel() : double;
begin
  result := innerBesoinEcsAnnuel;
end;

procedure TCalculConso.setBesoinClimAnnuel(dbl : double);
begin
  innerBesoinClimAnnuel := dbl;
end;

function TCalculConso.getBesoinClimAnnuel() : double;
begin
  result := innerBesoinClimAnnuel;
end;

(* ******************************************************************
// gestion sp�cifique des pourcentages pour simplifier les calculs
****************************************************************** *)

function TCalculConso.getPercent(aList : TTypePercentObject; aRank : integer) : double;
var
  po : TIzPercentObject;
begin
  po := aList.getPercentObject(aRank);
  if (po = nil) then
  begin
    result := 0; // pour un object qui n'existe pas, on renvoie 0
    exit;
  end;
  result := po.pourCent;
end;

procedure TCalculConso.setPercent(aList : TTypePercentObject; aRank : integer; aPercentage : double);
var
  po : TIzPercentObject;
begin
  po := aList.getPercentObject(aRank);
  if (po = nil) then
  begin
    exit;
  end;
  po.pourCent := aPercentage;
end;


function TCalculConso.getEqChauffagePercent(aRank : integer) : double;
begin
  result := getPercent(typeEqChauffage, aRank);
end;

function TCalculConso.getEqEcsPercent(aRank : integer) : double;
begin
  result := getPercent(typeEqEcs, aRank);
end;

//function TCalculConso.getEqClimPercent(aRank : integer) : double;
//begin
//  result := getPercent(typeEqClimatisation, aRank);
//end;

function TCalculConso.getEqVentilPercent(aRank : integer) : double;
begin
  result := getPercent(typeEqVentilation, aRank);
end;


procedure TCalculConso.setEqChauffagePercent(aRank : integer; aPercentage : double);
begin
  setPercent(typeEqChauffage, aRank, aPercentage);
end;


procedure TCalculConso.setEqEcsPercent(aRank : integer; aPercentage : double);
begin
  setPercent(typeEqChauffage, aRank, aPercentage);
end;


//procedure TCalculConso.setEqClimPercent(aRank : integer; aPercentage : double);
//begin
//  setPercent(typeEqChauffage, aRank, aPercentage);
//end;


procedure TCalculConso.setEqVentilPercent(aRank : integer; aPercentage : double);
begin
  setPercent(typeEqChauffage, aRank, aPercentage);
end;


end.
