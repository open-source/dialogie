unit IzProjet;

interface

uses
  IzObject, IzUtilitaires, IzObjectPersist;

	type TIzProjet = class(TObjectPersist)

		private
			innerMustCompute : boolean;
			innerTypeBatiment : TTypeBatiment;



		protected
			procedure setMustCompute(mustCompute : boolean);virtual;
			function getTypeBatiment() : TTypeBatiment;virtual;
			procedure setTypeBatiment(typeB : TTypeBatiment);virtual;



		public
      const
{$IFDEF APP_VIRVOLT}
      defaultProjectName : string = 'projet Vir''Volt 1';
{$ELSE}
      defaultProjectName : string = 'projet DialogIE 1';
{$ENDIF}
      var
			aSauvegarder : boolean;
			sauve : Boolean;
			nomSauvegarde : String;

			constructor Create();override;

			property aCalculer : boolean write setMustCompute;
			property typeBatiment : TTypeBatiment read getTypeBatiment write setTypeBatiment;

	end;

	var mainIzProjet : TIzProjet;

	function getProjet() : TIzProjet;

implementation


	constructor TIzProjet.Create();
	begin
		inherited create;

		innerTypeBatiment := BATIMENT_Maison;
		aSauvegarder := true;
		sauve := True;
		nomSauvegarde := defaultProjectName;

//		aCalculer := true;//dans la variante maintenant
		innerMustCompute := true;//pour compatibilité
	end;

	procedure TIzProjet.setMustCompute(mustCompute : boolean);
	begin
		innerMustCompute := mustCompute;
	end;

	function TIzProjet.getTypeBatiment() : TTypeBatiment;
	begin
		result := innerTypeBatiment;
	end;

	procedure TIzProjet.setTypeBatiment(typeB : TTypeBatiment);
	begin
  	innerTypeBatiment := typeB;
  end;

//code de l'unité
	function getProjet() : TIzProjet;
	begin
		if (mainIzProjet = nil) then
		begin
			mainIzProjet := TIzProjet.Create;
		end;
		result := mainIzProjet;
	end;


end.
