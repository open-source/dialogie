unit IzBiblioEnergieRepartition3CL;

interface

uses
	IzBiblioData;

	type
		TEnergieRepartition3CL = Record
				Energie,
				Fournisseur,
				TypeTarif,
				Mix,
				Tarif : String;
				ChauffDirect,
				ChauffAccumul,
				ECSDirect,
				ECSAccumul,
				Cuisson,
				AuxChauff,
				AuxVentil,
				lavage,
				froid,
				Bureautique,
				Electro,
				Eclairage,
				clim : Double;
	End;

	type TBiblioEnergieRepartition3CL = class(TBiblioData)
		private
			biblio : array of TEnergieRepartition3CL;
		public
			constructor Create(aFileName : string); override;
      destructor Destroy();override;

			function getItem(rank : integer ) : TEnergieRepartition3CL;
	end;



implementation

uses
	System.Classes,
  System.SysUtils,
  IzUtilitaires;



	Constructor TBiblioEnergieRepartition3CL.Create(aFileName : string);
	Var
		Temp : TStringList;
		i : Integer;
		Ligne : String;
	Begin
		inherited create(aFileName);
		Temp := getStringList();
		SetLength(biblio, nb+1);
		For i := 0 to Temp.Count -1 do
		Begin
			ligne := Temp[i];

			biblio[i].Energie := getItemTab(ligne);
			biblio[i].Fournisseur := getItemTab(ligne);
			biblio[i].TypeTarif := getItemTab(ligne);;
			biblio[i].Mix  := getItemTab(ligne);
			biblio[i].Tarif := getItemTab(ligne);
			biblio[i].ChauffDirect := getDouble(getItemTab(ligne));
			biblio[i].ChauffAccumul := getDouble(getItemTab(ligne));
			biblio[i].ECSDirect := getDouble(getItemTab(ligne));
			biblio[i].ECSAccumul := getDouble(getItemTab(ligne));
			biblio[i].Cuisson := getDouble(getItemTab(ligne));
			biblio[i].AuxChauff := getDouble(getItemTab(ligne));
			biblio[i].AuxVentil := getDouble(getItemTab(ligne));
			biblio[i].Lavage := getDouble(getItemTab(ligne));
			biblio[i].Froid := getDouble(getItemTab(ligne));
			biblio[i].Bureautique := getDouble(getItemTab(ligne));
			biblio[i].Electro := getDouble(getItemTab(ligne));
			biblio[i].Eclairage := getDouble(getItemTab(ligne));
			biblio[i].clim := getDouble(getItemTab(ligne));
		End;
		Temp.free;
end;


destructor TBiblioEnergieRepartition3CL.Destroy();
begin
  SetLength(biblio,0);
  inherited Destroy();
end;


	function TBiblioEnergieRepartition3CL.getItem(rank: Integer) : TEnergieRepartition3CL;
	begin
		getItem := biblio[rank];
	end;

end.
