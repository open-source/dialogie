unit IzBiblioEnergieTarif3CL;

interface

uses
	Classes, IzUtilitaires, IzBiblioData, IzBiblioEnergieRepartition3CL, 
  IzEnergies3CL, IzConstantesConso;


	type
		TEnergieTarif = Record
				Energie,
				Fournisseur,
				TType,
				Tarif,
				Usage,
				Reference,
				miseAjour : String;
				Tarif1,
				Abonnement,
				CO2,
				SO2,
				Nucleaire,
				Uranium,
				Gaznat,
				Petrole,
				Charbon,
				Solaire,
				Biomasse,
				vent,
				Hydraul,
				geothermie,
				energiePrimaire : Double;
		End;

	type TBiblioEnergieTarif3CL = class(TBiblioData)
		private
      ownRepartition : boolean;
			biblio : array of TEnergieTarif;
			innerEnergies : TStringList;
			innerEnergieRepartition : TBiblioEnergieRepartition3CL;
			innerDateEnergie : TDateTime;
			function getEnergieRepartition() : TBiblioEnergieRepartition3CL;
		public


			constructor Create(aFileName : string); override;
			constructor CreateWithLink(aFileName : string ; biblioEnergieRepartition : TBiblioEnergieRepartition3CL);
      destructor Destroy(); override;

			property energieRepartition : TBiblioEnergieRepartition3CL read getEnergieRepartition;// write innerEnergieRepartition;

			function getMenuFournisseur(TypeEnergie : String) : TStringList;
			function getMenuType(TypeEnergie, Fourniss : String) : TStringList;

			Function ressource(ener3 : TIzEnergie; Usage : TTypeUsage ;TypeRessource : TTypeRessource) : Double;overload;
			Function ressource(fournisseur, typeTarif, typeEnergie : string; Usage : TTypeUsage ;TypeRessource : TTypeRessource) : Double;overload;
			Function reference(fournisseur, typeTarif, TypeEnergie : string) : String;
			Function getDateMaj(fournisseur, typeTarif, TypeEnergie : string) : TDateTime;overload;virtual;
      function getDateMajString(fournisseur, typeTarif, typeEnergie: string): string;

			Function prixAbonnement(fournisseur, typeTarif, TypeEnergie : string) : Double;
			Function prixkWh(fournisseur, typeTarif, TypeEnergie : string) : Double;
			Function prixEnergie(fournisseur, typeTarif, TypeEnergie : string ; Usage : TTypeUsage) : Double;

			Function emissionCO2(fournisseur, typeTarif, TypeEnergie : string ; Usage : TTypeUsage) : Double;
			Function emissionNucleaire(fournisseur, typeTarif, TypeEnergie : string ; Usage : TTypeUsage) : Double;
			Function emissionSO2(fournisseur, typeTarif, TypeEnergie : string ; Usage : TTypeUsage) : Double;
//			Function emissionTEP(fournisseur, typeTarif, TypeEnergie : string; Usage : TTypeUsage) : Double;

			function getEnergieIndex(energieName : string) : integer;

			function getEnergie(energieIndex : integer) : string;
			Function getDateMaj() : TDateTime;overload;virtual;

	end;


implementation

uses
  System.UITypes,
	Dialogs,
  SysUtils,
  IzConstantes,
  UtilitairesDialogie;


	Constructor TBiblioEnergieTarif3CL.CreateWithLink(aFileName : string ; biblioEnergieRepartition : TBiblioEnergieRepartition3CL);
	begin
		if(biblioEnergieRepartition <> nil) then
		begin
      ownRepartition := false;
			innerEnergieRepartition := biblioEnergieRepartition;
		end;
		create(aFileName);
	end;


	Constructor TBiblioEnergieTarif3CL.Create(aFileName : string);
	Var
		Temp : TStringList;
		i : Integer;
		Ligne : String;
		chaine : string;
		dateM : TDateTime;
	Begin
		inherited create(aFileName);

		innerDateEnergie := 0;

		innerEnergies := TStringList.Create;

		Temp := getStringList();
		SetLength(biblio, nb+1);
		For i := 0 to Temp.Count - 1 do
		Begin
			ligne := Temp[i];

			biblio[i].Energie := getItemTab(ligne);

			if innerEnergies.IndexOf(biblio[i].Energie) = -1 then
				innerEnergies.Add(biblio[i].Energie);

			biblio[i].Fournisseur := getItemTab(ligne);

			chaine := getItemTab(ligne);
			if (chaine = '') then
				chaine := 'standard';
			biblio[i].TType := chaine;

			biblio[i].Tarif  := getItemTab(ligne);
			biblio[i].Usage := getItemTab(ligne);
			biblio[i].Tarif1 := getDouble(getItemTab(ligne));
			biblio[i].Abonnement := getDouble(getItemTab(ligne));
			biblio[i].Reference := getItemTab(ligne);

			biblio[i].MiseAJour := getItemTab(ligne);
			dateM := getInt(biblio[i].MiseAJour) + CONST_EXCEL_DELPHI_DATE_DIFF;
			if (dateM > innerDateEnergie) then
			begin
				innerDateEnergie := dateM;
			end;

			biblio[i].CO2 := getDouble(getItemTab(ligne));
			biblio[i].SO2 := getDouble(getItemTab(ligne));
			biblio[i].Nucleaire := getDouble(getItemTab(ligne));
			biblio[i].Uranium := getDouble(getItemTab(ligne));
			biblio[i].Gaznat := getDouble(getItemTab(ligne));
			biblio[i].Petrole := getDouble(getItemTab(ligne));
			biblio[i].Charbon := getDouble(getItemTab(ligne));
			biblio[i].Solaire := getDouble(getItemTab(ligne));
			biblio[i].Biomasse := getDouble(getItemTab(ligne));
			biblio[i].Vent := getDouble(getItemTab(ligne));
			biblio[i].Hydraul := getDouble(getItemTab(ligne));
			biblio[i].geothermie := getDouble(getItemTab(ligne));
			biblio[i].energiePrimaire := getDouble(getItemTab(ligne));
		End;
		Temp.free;
	end;



destructor TBiblioEnergieTarif3CL.Destroy();
begin
  SetLength(biblio, 0);
  try
	  innerEnergies.Free;
  finally
  
  end;
  try
    if (ownRepartition) then
	    innerEnergieRepartition.Free;
  finally

  end;
  inherited Destroy();
end;



	function TBiblioEnergieTarif3CL.getEnergieRepartition() : TBiblioEnergieRepartition3CL;
	begin
		if (innerEnergieRepartition = nil)then
		begin
			//cr�ation avec fichier par d�faut
      //revoir l'emplacement des biblioth�ques pour utiliser un dossier standard
			innerEnergieRepartition := TBiblioEnergieRepartition3CL.Create(dialogieLibPath + 'RepartitionDPE.txt');
//			innerEnergieRepartition := TBiblioEnergieRepartition3CL.Create(dialogieLibPath + 'RepartitionDPE.txt');
      ownRepartition := true;
		end;
		getEnergieRepartition := innerEnergieRepartition;
	end;



	Function TBiblioEnergieTarif3CL.getMenuFournisseur(TypeEnergie : String) : TStringList;
	Var
		i : Integer;
		Liste : TStringList;
	Begin
		Liste := TStringList.create;
		For i := 0 to nb-1 do
		Begin
			if (biblio[i].Energie = TypeEnergie) and (liste.IndexOf(biblio[i].Fournisseur) = -1) then
				Liste.add(biblio[i].Fournisseur);
		End;
		getMenuFournisseur := Liste;
	End;


	function TBiblioEnergieTarif3CL.getMenuType(TypeEnergie, Fourniss : String) : TStringList;
	Var
		i : Integer;
		Liste : TStringList;
	Begin
		Liste := TStringList.create;
		For i := 0 to nb -1 do
		Begin
			if (biblio[i].Energie = TypeEnergie) and (Fourniss = biblio[i].Fournisseur) and (liste.IndexOf(biblio[i].TType) = -1) then
				Liste.add(biblio[i].TType);
		End;
		getMenuType := Liste;
	End;


	Function TBiblioEnergieTarif3CL.ressource(ener3 : TIzEnergie; Usage : TTypeUsage ;TypeRessource : TTypeRessource) : Double;
	begin
		result := ressource(ener3.fournisseur, ener3.typeTarif, ener3.nomEnergie, usage, TypeRessource);
	end;

	Function TBiblioEnergieTarif3CL.Ressource(fournisseur, typeTarif, typeEnergie: string ; Usage : TTypeUsage ;TypeRessource : TTypeRessource) : Double;
	Var
		i,j : Integer;
		energieId : integer;
		PourcentageUsage : Double;
		rscTemp : Double;
		repartition : TEnergieRepartition3CL;
	Begin
		ressource := 0;
		energieId := getEnergieIndex(typeEnergie);
		PourcentageUsage := 0;
		If energieId = 0 then//electricite
//	 If energieId = 1 then//electricite
		Begin
		 rscTemp := 0;
		 For i := 0 to energieRepartition.nb-1 do
		 Begin
			repartition := energieRepartition.getItem(i);
			if (repartition.Fournisseur = fournisseur) and
				 (repartition.Energie = typeEnergie) then
			Begin
				Case Usage of
					USAGE_CHAUFFAGE_DIRECT  : PourcentageUsage := repartition.ChauffDirect;
					USAGE_CHAUFFAGE_ACCUMULATION :
						Begin
							PourcentageUsage := repartition.ChauffAccumul;
						End;
					USAGE_ECS_DIRECT  : PourcentageUsage := repartition.ECSDirect;
					USAGE_ECS_ACCUMULATION : PourcentageUsage := repartition.ECSAccumul;
					USAGE_CUISSON  : PourcentageUsage := repartition.Cuisson;
					USAGE_AUX_CHAUFFAGE  : PourcentageUsage := repartition.AuxChauff;
					USAGE_AUX_VENTILATION  : PourcentageUsage := repartition.AuxVentil;
					USAGE_LAVAGE  : PourcentageUsage := repartition.lavage;
					USAGE_FROID  : PourcentageUsage := repartition.Froid;
					USAGE_BUREAUTIQUE  : PourcentageUsage := repartition.Bureautique;
					USAGE_MENAGER  : PourcentageUsage := repartition.Electro;
					USAGE_ECLAIRAGE  : PourcentageUsage := repartition.Eclairage;
					USAGE_CLIMATISATION  : PourcentageUsage := repartition.clim;
					else
					Begin
						messageDlg('Erreur interne : erreur d''usage', mtError, [mbOk], 0);
					End;
				End;
//on passe au suivant puisque ce type d'usage n'entre pas enligne de compte
        if PourcentageUsage = 0 then
          continue;

				For j := 0 to nb-1 do
				Begin
					if (biblio[j].Energie = typeEnergie) and
						 (biblio[j].Fournisseur = fournisseur) and
						 (biblio[j].TType = typeTarif) and
						 (biblio[j].Tarif = repartition.Tarif) and
						 (biblio[j].Usage = repartition.Mix) then
					Case TypeRessource Of
						RESSOURCE_URANIUM :
							rscTemp := rscTemp + (PourcentageUsage*biblio[j].Uranium);
						RESSOURCE_GAZ_NATUREL :
							rscTemp := rscTemp + (PourcentageUsage*biblio[j].Gaznat);
						RESSOURCE_PETROLE :
							rscTemp := rscTemp + (PourcentageUsage*biblio[j].Petrole);
						RESSOURCE_CHARBON :
							rscTemp := rscTemp + (PourcentageUsage*biblio[j].Charbon);
						RESSOURCE_SOLAIRE :
							rscTemp := rscTemp + (PourcentageUsage*biblio[j].Solaire);
						RESSOURCE_BIOMASSE :
							rscTemp := rscTemp + (PourcentageUsage*biblio[j].Biomasse);
						RESSOURCE_VENT :
							rscTemp := rscTemp + (PourcentageUsage*biblio[j].vent);
						RESSOURCE_HYDRAULIQUE :
							rscTemp := rscTemp + (PourcentageUsage*biblio[j].Hydraul);
						RESSOURCE_GEOTHERMIE :
							rscTemp := rscTemp + (PourcentageUsage*biblio[j].Geothermie);
						RESSOURCE_ENERGIE_PRIMAIRE :
							rscTemp := rscTemp + (PourcentageUsage*biblio[j].energiePrimaire);
					End;
				End;
			End;
		End;
		result := rscTemp;
	End
	else if energieId = 8 then//solaire
//	else if energieId = 9 then//solaire
	Begin
		If TypeRessource = RESSOURCE_SOLAIRE then
			result := 1
		else
			result := 0;

	End
	else
	Begin
		For j := 0 to nb-1 do
		Begin
			if (biblio[j].Energie = typeEnergie) and
				 (biblio[j].Fournisseur = fournisseur) and
				 (biblio[j].TType = typeTarif) then
				Case TypeRessource Of
					RESSOURCE_URANIUM :
						result := biblio[j].Uranium;
					RESSOURCE_GAZ_NATUREL :
						result := biblio[j].Gaznat;
					RESSOURCE_PETROLE :
						result := biblio[j].Petrole;
					RESSOURCE_CHARBON :
						result := biblio[j].Charbon;
					RESSOURCE_SOLAIRE :
						result := biblio[j].Solaire;
					RESSOURCE_BIOMASSE :
						result := biblio[j].Biomasse;
					RESSOURCE_VENT :
						result := biblio[j].vent;
					RESSOURCE_HYDRAULIQUE :
						result := biblio[j].Hydraul;
					RESSOURCE_GEOTHERMIE :
						result := biblio[j].Geothermie;
					RESSOURCE_ENERGIE_PRIMAIRE :
						result := biblio[j].energiePrimaire;
				End;
						 //result := (TableEnergie[j].CO2);
				End;
	 End;
End;

Function TBiblioEnergieTarif3CL.PrixAbonnement(fournisseur, typeTarif, typeEnergie : string) : Double;
Var
	 i : Integer;
	 energieId : integer;
Begin
	prixAbonnement := 0;
	energieId := getEnergieIndex(typeEnergie);
	 if energieId <> 8 then
//	 if energieId <> 9 then
	 Begin
		 For i := 0 to nb-1 do
		 Begin
					if (biblio[i].Energie = TypeEnergie) and
						 (biblio[i].Fournisseur = fournisseur) and
						 (biblio[i].TType = typeTarif) then
						 begin
									PrixAbonnement := biblio[i].Abonnement;
									exit;
						 end;
		 End;
	 End;
End;

Function TBiblioEnergieTarif3CL.Reference(fournisseur, typeTarif, typeEnergie : string) : String;
Var
	 i : Integer;
	 Begin

	 if getEnergieIndex(TypeEnergie) = 8 then
//	 if getEnergieIndex(TypeEnergie) = 9 then
	 Begin
				Reference := '';
	 End
	 else
		 For i := 0 to nb-1 do
		 Begin
					if (biblio[i].Energie = TypeEnergie) and
						 (biblio[i].Fournisseur = fournisseur) and
						 (biblio[i].TType = typeTarif) then
						 begin
									Reference := biblio[i].Reference;
									exit;
						 end;
		 End;
End;


Function TBiblioEnergieTarif3CL.getDateMaj(fournisseur, typeTarif, typeEnergie : string) : TDateTime;
Var
	 i : Integer;
   iDate : integer;
   sDate : string;
Begin
	result := Date;
//	if getEnergieIndex(TypeEnergie) <> 8 then
//	if getEnergieIndex(TypeEnergie) <> 9 then
	Begin
		 For i := 0 to nb-1 do
		 Begin
					if (biblio[i].Energie = TypeEnergie) and
						 (biblio[i].Fournisseur = fournisseur) and
						 (biblio[i].TType = typeTarif) then
					Begin
            sDate := trim(biblio[i].miseAjour);
            if sDate <> '' then
            begin
              iDate := getInt(sDate);
              if iDate > 0 then
                result := +CONST_EXCEL_DELPHI_DATE_DIFF
              else
                result := getDate(sDate);
            end;
					End;
		 End;
	End;
End;



Function TBiblioEnergieTarif3CL.getDateMajString(fournisseur, typeTarif, typeEnergie : string) : string;
Var
	 i : Integer;
   iDate : integer;
   sDate : string;
   dDate : TDateTime;
Begin
	result := '';
//	if getEnergieIndex(TypeEnergie) <> 8 then
//	if getEnergieIndex(TypeEnergie) <> 9 then
	Begin
		 For i := 0 to nb-1 do
		 Begin
					if (biblio[i].Energie = TypeEnergie) and
						 (biblio[i].Fournisseur = fournisseur) and
						 (biblio[i].TType = typeTarif) then
					Begin
            sDate := biblio[i].miseAjour;
            if sDate <> '' then
            begin
              iDate := getInt(sDate);
              if iDate > 0 then
                dDate := iDate +CONST_EXCEL_DELPHI_DATE_DIFF
              else
                dDate := getDate(sDate);
              result := DateToStr(dDate);
            end;
            exit;
					End;
		 End;
	End;
End;


Function TBiblioEnergieTarif3CL.PrixkWh(fournisseur, typeTarif, typeEnergie : string) : Double;
Var
	 i : Integer;
	 Maxi : Double;
Begin
	PrixkWh := 0;
	 Maxi := 0;
//	 if getEnergieIndex(TypeEnergie) <> 8 then
//	 if getEnergieIndex(TypeEnergie) <> 9 then
	 Begin
		 For i := 0 to nb-1 do
		 Begin
					if (biblio[i].Energie = TypeEnergie) and
						 (biblio[i].Fournisseur = fournisseur) and
						 (biblio[i].TType = typeTarif) then
					Begin
							 if biblio[i].Tarif1 > Maxi then
									Maxi := biblio[i].Tarif1;
					End;
		 End;
		 PrixkWh := Maxi;
	 End;
End;


	Function TBiblioEnergieTarif3CL.PrixEnergie(fournisseur, typeTarif, typeEnergie : string ; Usage : TTypeUsage) : Double;
	Var
		i,j : Integer;
	 //idEnergie : integer;
   idEnergie : TEnergieIdentificateur;
		PourcentageUsage : Double;
		PrixE : Double;
		repartition : TEnergieRepartition3CL;
	Begin
		//idEnergie := getEnergieIndex(TypeEnergie);
		idEnergie := getEnergieDialogie(TypeEnergie);
		PourcentageUsage := 0;
		result := 0;



//ATTENTION AU RANG DE L'ENERGIE!!!!

		If idEnergie = ENERGIE_ELECTRICITE then//elec
//	 If idEnergie = 0 then
		Begin
			PrixE := 0;
			For i := 0 to energieRepartition.nb-1 do
			Begin
        repartition := energieRepartition.getItem(i);
				if (repartition.Fournisseur = fournisseur) and
					(repartition.Energie = typeEnergie) then
				Begin
					Case Usage of
						USAGE_CHAUFFAGE_DIRECT  : PourcentageUsage := repartition.ChauffDirect;
						USAGE_CHAUFFAGE_ACCUMULATION : PourcentageUsage := repartition.ChauffAccumul;
						USAGE_ECS_DIRECT  : PourcentageUsage := repartition.ECSDirect;
						USAGE_ECS_ACCUMULATION : PourcentageUsage := repartition.ECSAccumul;
						USAGE_CUISSON  : PourcentageUsage := repartition.Cuisson;
						USAGE_AUX_CHAUFFAGE  : PourcentageUsage := repartition.AuxChauff;
						USAGE_AUX_VENTILATION  : PourcentageUsage := repartition.AuxVentil;
						USAGE_LAVAGE  : PourcentageUsage := repartition.lavage;
						USAGE_FROID  : PourcentageUsage := repartition.Froid;
						USAGE_BUREAUTIQUE  : PourcentageUsage := repartition.Bureautique;
						USAGE_MENAGER  : PourcentageUsage := repartition.Electro;
						USAGE_ECLAIRAGE  : PourcentageUsage := repartition.Eclairage;
						USAGE_CLIMATISATION  : PourcentageUsage := repartition.clim;
						else
						begin
							messageDlg('Erreur interne : erreur d''usage', mtError, [mbOk], 0)
						end;
					End;
//formLog.addLog('four ' + fournisseur + ' tarif ' + typeTarif + ' percent ' + floatToStr(PourcentageUsage) + ' usage ' + intToStr(integer(usage)));
					For j := 0 to nb-1 do
					Begin
						if (biblio[j].Energie = typeEnergie) and
							(biblio[j].Fournisseur = fournisseur) and
							(biblio[j].TType = typeTarif) and
							(biblio[j].Tarif = energieRepartition.getItem(i).Tarif) and
							(biblio[j].Usage = energieRepartition.getItem(i).Mix) then
						begin
							PrixE := PrixE + (PourcentageUsage*biblio[j].Tarif1);
						end;
					End;
				End;
			End;
			result := PrixE;
		End
(*
		else if idEnergie = 8 then
		Begin
				result := 0;
		End
*)
		else
		Begin
			For j := 0 to nb-1 do
			Begin
				if (biblio[j].Energie = typeEnergie) and
					(biblio[j].Fournisseur = fournisseur) and
					(biblio[j].TType = typeTarif) then
        begin
				  result := (biblio[j].Tarif1);
          exit;
        end;
			End;
		End;
	End;

Function TBiblioEnergieTarif3CL.EmissionCO2(fournisseur, typeTarif, typeEnergie : string ; Usage : TTypeUsage) : Double;
Var
	 i,j : Integer;
	 //idEnergie : integer;
   idEnergie : TEnergieIdentificateur;
	 PourcentageUsage : Double;
	 PrixE : Double;
Begin
	PourcentageUsage := 0;
	result := 0;
//		idEnergie := getEnergieIndex(TypeEnergie);
		idEnergie := getEnergieDialogie(TypeEnergie);
	 	 if idEnergie = ENERGIE_ELECTRICITE then//elec
//	 	 If idEnergie = 0 then//elec
//	 If idEnergie = 1 then
	 Begin
		 PrixE := 0;
		 For i := 0 to energieRepartition.nb-1 do
		 Begin
					if (energieRepartition.getItem(i).Fournisseur = fournisseur) and
						 (energieRepartition.getItem(i).Energie = typeEnergie) then
					Begin
							 Case Usage of
								 USAGE_CHAUFFAGE_DIRECT  : PourcentageUsage := energieRepartition.getItem(i).ChauffDirect;
								 USAGE_CHAUFFAGE_ACCUMULATION : Begin
												PourcentageUsage := energieRepartition.getItem(i).ChauffAccumul;
											End;
								 USAGE_ECS_DIRECT  : PourcentageUsage := energieRepartition.getItem(i).ECSDirect;
								 USAGE_ECS_ACCUMULATION : PourcentageUsage := energieRepartition.getItem(i).ECSAccumul;
								 USAGE_CUISSON  : PourcentageUsage := energieRepartition.getItem(i).Cuisson;
								 USAGE_AUX_CHAUFFAGE  : PourcentageUsage := energieRepartition.getItem(i).AuxChauff;
								 USAGE_AUX_VENTILATION  : PourcentageUsage := energieRepartition.getItem(i).AuxVentil;
								 USAGE_LAVAGE  : PourcentageUsage := energieRepartition.getItem(i).lavage;
								 USAGE_FROID  : PourcentageUsage := energieRepartition.getItem(i).Froid;
								 USAGE_BUREAUTIQUE  : PourcentageUsage := energieRepartition.getItem(i).Bureautique;
								 USAGE_MENAGER  : PourcentageUsage := energieRepartition.getItem(i).Electro;
								 USAGE_ECLAIRAGE  : PourcentageUsage := energieRepartition.getItem(i).Eclairage;
								 USAGE_CLIMATISATION  : PourcentageUsage := energieRepartition.getItem(i).clim;
								 else
								 begin
									messageDlg('Erreur interne : erreur d''usage', mtError, [mbOk], 0);
								 end;
							 End;
							 For j := 0 to nb-1 do
							 Begin
										if (biblio[j].Energie = typeEnergie) and
											 (biblio[j].Fournisseur = fournisseur) and
											 (biblio[j].TType = typeTarif) and
											 (biblio[j].Tarif = energieRepartition.getItem(i).Tarif) and
											 (biblio[j].Usage = energieRepartition.getItem(i).Mix) then
										PrixE := PrixE + (PourcentageUsage*biblio[j].CO2/1000);
							 End;
					End;
		 End;
		 result := PrixE;
	 End
(*
	 else if idEnergie = 8 then
//	 else if idEnergie = 9 then
	 Begin
				EmissionCO2 := 0;
	 End
*)
	 else
	 Begin
				For j := 0 to nb-1 do
				Begin
						 if (biblio[j].Energie = typeEnergie) and
											 (biblio[j].Fournisseur = fournisseur) and
											 (biblio[j].TType = typeTarif) then
             begin
						  result := (biblio[j].CO2/1000);
              exit;
             end;
				End;
	 End;
End;

Function TBiblioEnergieTarif3CL.EmissionNucleaire(fournisseur, typeTarif, typeEnergie : string ; Usage : TTypeUsage) : Double;
Var
	 i,j : Integer;
	 //idEnergie : integer;
   idEnergie : TEnergieIdentificateur;
	 PourcentageUsage : Double;
	 PrixE : Double;
Begin
	result := 0;
	PourcentageUsage := 0;
    idEnergie := getEnergieDialogie(TypeEnergie);
		//idEnergie := getEnergieIndex(TypeEnergie);
	 if idEnergie = ENERGIE_ELECTRICITE then//elec
//	 If idEnergie = 0 then//elec
//	 If idEnergie = 1 then
	 Begin
		 PrixE := 0;
		 For i := 0 to energieRepartition.nb-1 do
		 Begin
					if (energieRepartition.getItem(i).Fournisseur = fournisseur) and
						 (energieRepartition.getItem(i).Energie = typeEnergie) then
					Begin
							 Case Usage of
								  USAGE_CHAUFFAGE_DIRECT  : PourcentageUsage := energieRepartition.getItem(i).ChauffDirect;
								 USAGE_CHAUFFAGE_ACCUMULATION : Begin
												PourcentageUsage := energieRepartition.getItem(i).ChauffAccumul;
											End;
								 USAGE_ECS_DIRECT  : PourcentageUsage := energieRepartition.getItem(i).ECSDirect;
								 USAGE_ECS_ACCUMULATION : PourcentageUsage := energieRepartition.getItem(i).ECSAccumul;
								 USAGE_CUISSON  : PourcentageUsage := energieRepartition.getItem(i).Cuisson;
								 USAGE_AUX_CHAUFFAGE  : PourcentageUsage := energieRepartition.getItem(i).AuxChauff;
								 USAGE_AUX_VENTILATION  : PourcentageUsage := energieRepartition.getItem(i).AuxVentil;
								 USAGE_LAVAGE  : PourcentageUsage := energieRepartition.getItem(i).lavage;
								 USAGE_FROID  : PourcentageUsage := energieRepartition.getItem(i).Froid;
								 USAGE_BUREAUTIQUE  : PourcentageUsage := energieRepartition.getItem(i).Bureautique;
								 USAGE_MENAGER  : PourcentageUsage := energieRepartition.getItem(i).Electro;
								 USAGE_ECLAIRAGE  : PourcentageUsage := energieRepartition.getItem(i).Eclairage;
								 USAGE_CLIMATISATION  : PourcentageUsage := energieRepartition.getItem(i).clim;
								 else begin
									messageDlg('Erreur interne : erreur d''usage', mtError, [mbOk], 0);
									end;
							 End;
							 For j := 0 to nb-1 do
							 Begin
										if (biblio[j].Energie = typeEnergie) and
											 (biblio[j].Fournisseur = fournisseur) and
											 (biblio[j].TType = typeTarif) and
											 (biblio[j].Tarif = energieRepartition.getItem(i).Tarif) and
											 (biblio[j].Usage = energieRepartition.getItem(i).Mix) then
										PrixE := PrixE + (PourcentageUsage*biblio[j].Nucleaire/1000);
							 End;
					End;
		 End;
		 result := PrixE;
	 End
(*
	 else if idEnergie = 8 then
//	 else if idEnergie = 9 then
	 Begin
				EmissionNucleaire := 0;
	 End
*)
	 else
	 Begin
				For j := 0 to nb-1 do
				Begin
						if (biblio[j].Energie = typeEnergie) and
											 (biblio[j].Fournisseur = fournisseur) and
											 (biblio[j].TType = typeTarif) then
						begin
							result := (PourcentageUsage*biblio[j].Nucleaire/1000);
							exit;
						end;
				End;
	 End;
End;

Function TBiblioEnergieTarif3CL.EmissionSO2(fournisseur, typeTarif, typeEnergie : string ; Usage : TTypeUsage) : Double;
Var
	 i,j : Integer;
	 //idEnergie : integer;
   idEnergie : TEnergieIdentificateur;
	 PourcentageUsage : Double;
	 PrixE : Double;
Begin
	PourcentageUsage := 0;
	result := 0;
    idEnergie := getEnergieDialogie(TypeEnergie);
		//idEnergie := getEnergieIndex(TypeEnergie);
	 If idEnergie = ENERGIE_ELECTRICITE then//elec
//	 If idEnergie = 0 then//elec
//	 If idEnergie = 1 then
	 Begin
		 PrixE := 0;
		 For i := 0 to energieRepartition.nb-1 do
		 Begin
					if (energieRepartition.getItem(i).Fournisseur = fournisseur) and
						 (energieRepartition.getItem(i).Energie = typeEnergie) then
					Begin
							 Case Usage of
									USAGE_CHAUFFAGE_DIRECT  : PourcentageUsage := energieRepartition.getItem(i).ChauffDirect;
								 USAGE_CHAUFFAGE_ACCUMULATION : PourcentageUsage := energieRepartition.getItem(i).ChauffAccumul;
								 USAGE_ECS_DIRECT  : PourcentageUsage := energieRepartition.getItem(i).ECSDirect;
								 USAGE_ECS_ACCUMULATION : PourcentageUsage := energieRepartition.getItem(i).ECSAccumul;
								 USAGE_CUISSON  : PourcentageUsage := energieRepartition.getItem(i).Cuisson;
								 USAGE_AUX_CHAUFFAGE  : PourcentageUsage := energieRepartition.getItem(i).AuxChauff;
								 USAGE_AUX_VENTILATION  : PourcentageUsage := energieRepartition.getItem(i).AuxVentil;
								 USAGE_LAVAGE  : PourcentageUsage := energieRepartition.getItem(i).lavage;
								 USAGE_FROID  : PourcentageUsage := energieRepartition.getItem(i).Froid;
								 USAGE_BUREAUTIQUE  : PourcentageUsage := energieRepartition.getItem(i).Bureautique;
								 USAGE_MENAGER  : PourcentageUsage := energieRepartition.getItem(i).Electro;
								 USAGE_ECLAIRAGE  : PourcentageUsage := energieRepartition.getItem(i).Eclairage;
								 USAGE_CLIMATISATION  : PourcentageUsage := energieRepartition.getItem(i).clim;
								else
									messageDlg('Erreur interne : erreur d''usage', mtError, [mbOk], 0);
							 End;
							 For j := 0 to nb-1 do
							 Begin
										if (biblio[j].Energie = typeEnergie) and
											 (biblio[j].Fournisseur = fournisseur) and
											 (biblio[j].TType = typeTarif) and
											 (biblio[j].Tarif = energieRepartition.getItem(i).Tarif) and
											 (biblio[j].Usage = energieRepartition.getItem(i).Mix) then
										PrixE := PrixE + (PourcentageUsage*biblio[j].SO2/1000);
							 End;
					End;
		 End;
		 result := PrixE;
	 End
(*
	 else if idEnergie = 8 then
//	 else if idEnergie = 9 then
	 Begin
				EmissionSO2 := 0;
	 End
*)
	 else
	 Begin
				For j := 0 to nb-1 do
				Begin
						 if (biblio[j].Energie = typeEnergie) and
											 (biblio[j].Fournisseur = fournisseur) and
											 (biblio[j].TType = typeTarif) then
             begin
						   result := (biblio[j].SO2/1000);
             end;
				End;
	 End;
End;
(*
Function TBiblioEnergieTarif3CL.EmissionTEP(fournisseur, typeTarif, typeEnergie : string ; Usage : TTypeUsage) : Double;
Var
	 i,j : Integer;
	 idEnergie : integer;
	 PourcentageUsage : Double;
	 PrixE : Double;
Begin
	EmissionTEP := 0;
		 idEnergie := getEnergieIndex(TypeEnergie);
	 If idEnergie = 0 then//elec
//	 If idEnergie = 1 then
	 Begin
		 PrixE := 0;
		 For i := 0 to energieRepartition.nb-1 do
		 Begin
				if (energieRepartition.getItem(i).Fournisseur = fournisseur) and
					 (energieRepartition.getItem(i).Energie = typeEnergie) then
				Begin
					Case Usage of
						USAGE_CHAUFFAGE_DIRECT  : PourcentageUsage := energieRepartition.getItem(i).ChauffDirect;
						USAGE_CHAUFFAGE_ACCUMULATION :
							Begin
								PourcentageUsage := energieRepartition.getItem(i).ChauffAccumul;
							End;
						USAGE_ECS_DIRECT  : PourcentageUsage := energieRepartition.getItem(i).ECSDirect;
						USAGE_ECS_ACCUMULATION : PourcentageUsage := energieRepartition.getItem(i).ECSAccumul;
						USAGE_CUISSON  : PourcentageUsage := energieRepartition.getItem(i).Cuisson;
						USAGE_AUX_CHAUFFAGE  : PourcentageUsage := energieRepartition.getItem(i).AuxChauff;
						USAGE_AUX_VENTILATION  : PourcentageUsage := energieRepartition.getItem(i).AuxVentil;
						USAGE_LAVAGE  : PourcentageUsage := energieRepartition.getItem(i).lavage;
						USAGE_FROID  : PourcentageUsage := energieRepartition.getItem(i).Froid;
						USAGE_BUREAUTIQUE  : PourcentageUsage := energieRepartition.getItem(i).Bureautique;
						USAGE_MENAGER  : PourcentageUsage := energieRepartition.getItem(i).Electro;
						USAGE_ECLAIRAGE  : PourcentageUsage := energieRepartition.getItem(i).Eclairage;
						USAGE_CLIMATISATION  : PourcentageUsage := energieRepartition.getItem(i).clim;
						else
						begin
							ShowMessage('Erreur d''usage')
						end;
					End;
					For j := 0 to nb-1 do
					Begin
						if (biblio[j].Energie = typeEnergie) and
							 (biblio[j].Fournisseur = fournisseur) and
							 (biblio[j].TType = typeTarif) and
							 (biblio[j].Tarif = energieRepartition.getItem(i).Tarif) and
							 (biblio[j].Usage = energieRepartition.getItem(i).Mix) then
						//PrixE := PrixE + (PourcentageUsage*TableEnergie[j].TEP);
					End;
				End;
		 End;
		 EmissionTEP := PrixE;
	 End
	 else if idEnergie = 8 then//voir � quoi �a correspond dans les libell�s �nergie
//	 else if idEnergie = 9 then//voir � quoi �a correspond dans les libell�s �nergie
	 Begin
				EmissionTEP := 0;
	 End
	 else
	 Begin
				For j := 0 to nb-1 do
				Begin
						 if (biblio[j].Energie = typeEnergie) and
											 (biblio[j].Fournisseur = fournisseur) and
											 (biblio[j].TType = typeTarif) then
						 //EmissionTEP := (PourcentageUsage*TableEnergie[j].TEP);
				End;
	 End;
End;
*)

	function TBiblioEnergieTarif3CL.getEnergieIndex(energieName : string) : integer;
	Begin
		 getEnergieIndex := innerEnergies.IndexOf(energieName);
	End;

	function TBiblioEnergieTarif3CL.getEnergie(energieIndex : integer) : string;
	begin
		if (energieIndex < 0) or (energieIndex >= innerEnergies.Count) then
			getEnergie := ''
		else
			getEnergie := innerEnergies[energieIndex];
	end;


	function TBiblioEnergieTarif3CL.getDateMaj() : TDateTime;
	begin
		getDateMaj := innerDateEnergie;
	end;

end.
