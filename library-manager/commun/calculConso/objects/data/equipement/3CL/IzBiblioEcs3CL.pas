unit IzBiblioEcs3CL;

interface
uses
	Classes, IzBiblioData;

	type
		TIEcsItem = record
			systeme,
			energie,
			typeE,
			stockage,
			installation,
			ballon,
			veilleuse,
			IEcs,
			CEcs,
			Accu
			:string;
		end;
		TIECS = Array of TIEcsItem;

	type TBiblioEcs3CL = class (TBiblioData)
		public
			biblio : TIECS;
			constructor Create(aFileName : string);override;
      destructor Destroy();override;

			function getSysteme() : TStringList;
			function getEnergie(systeme : String) : TStringList;
			function getTypeEcs(systeme,energie : String) : TStringList;
			function getStockage(systeme,energie,typpe : String) : TStringList;
			function getInstallation(systeme,energie,typpe,stockage : String) : TStringList;
			function getBallon(systeme,energie,typpe,stockage,installation: String) : TStringList;
			function getVeilleuse(systeme,energie,typpe,stockage,installation,ballon : String) : TStringList;

			function getEcs(systeme,energie,typpe,stockage,installation,ballon,veilleuse : String) : TIEcsItem;
	end;

var ECS_VIDE : TIEcsItem;

implementation

uses
	SysUtils, IzUtilitaires;

	Constructor TBiblioEcs3CL.Create(aFileName : string);
	Var
		Temp : TStringList;
		i : integer;
		Ligne : String;
	Begin
		inherited create(aFileName);
		Temp := getStringList();
		SetLength(biblio, nb+1);

		For i := 1 to Temp.Count do
		Begin
			ligne := Temp[i-1];

			//on ne prend pas le premier 'item' de la igne en compte
			getItemTab(ligne);

			biblio[i].systeme := getItemTab(Ligne);
			biblio[i].energie := getItemTab(Ligne);
			biblio[i].typeE := getItemTab(Ligne);
			biblio[i].stockage := getItemTab(Ligne);
			biblio[i].installation := getItemTab(Ligne);
			biblio[i].ballon := getItemTab(Ligne);
			biblio[i].veilleuse := getItemTab(Ligne);
			biblio[i].IEcs := getItemTab(Ligne);
			biblio[i].CEcs := getItemTab(Ligne);
			biblio[i].Accu := getItemTab(Ligne);
		End;

		temp.Free;
	End;


destructor TBiblioEcs3CL.Destroy();
begin
  SetLength(biblio,0);
  inherited Destroy();
end;


Function TBiblioEcs3CL.getSysteme() : TStringList;
Var
	i: Integer;
	Liste : TStringList;
  ecs : TIEcsItem;
Begin
	Liste := TStringList.Create;
	For i := 1 to nb do
	Begin
    ecs := biblio[i];
		addList(liste, ecs.systeme);
	end;
	result := Liste;
End;


Function TBiblioEcs3CL.getEnergie(systeme : String) : TStringList;
Var
	i: Integer;
	Liste : TStringList;
  ecs : TIEcsItem;
Begin
	Liste := TStringList.Create;
	For i := 1 to nb do
	Begin
    ecs := biblio[i];
		if ecs.systeme <> Systeme then
			continue;
		addList(liste, ecs.energie);
	end;
	result := Liste;
End;


Function TBiblioEcs3CL.getTypeEcs(systeme,energie : String) : TStringList;
Var
	i: Integer;
	Liste : TStringList;
  ecs : TIEcsItem;
Begin
	Liste := TStringList.Create;
	For i := 1 to nb do
	Begin
    ecs := biblio[i];
		if ecs.systeme <> Systeme then
			continue;
		if (ecs.energie <> Energie) then
			continue;
		addList(liste, ecs.typeE);
	end;
	result := Liste;
End;


Function TBiblioEcs3CL.getStockage(systeme,energie,typpe : String) : TStringList;
Var
	i: Integer;
	Liste : TStringList;
  ecs : TIEcsItem;
Begin
	Liste := TStringList.Create;
	For i := 1 to nb do
	Begin
    ecs := biblio[i];
		if ecs.systeme <> Systeme then
			continue;
		if (ecs.energie <> Energie) then
			continue;
		if (ecs.typeE <> typpe) then
			continue;
		addList(liste, ecs.stockage);
	end;
	result := Liste;
End;


Function TBiblioEcs3CL.getInstallation(systeme,energie,typpe,stockage : String) : TStringList;
Var
	i: Integer;
	Liste : TStringList;
  ecs : TIEcsItem;
Begin
	Liste := TStringList.Create;
	For i := 1 to nb do
	Begin
    ecs := biblio[i];
		if ecs.systeme <> Systeme then
			continue;
		if (ecs.energie <> Energie) then
			continue;
		if (ecs.typeE <> typpe) then
			continue;
		if (ecs.stockage <> stockage) then
			continue;
		addList(liste, ecs.installation);
	end;
	result := Liste;
End;


Function TBiblioEcs3CL.getBallon(systeme,energie,typpe,stockage,installation : String) : TStringList;
Var
	i: Integer;
	Liste : TStringList;
  ecs : TIEcsItem;
Begin
	Liste := TStringList.Create;
	For i := 1 to nb do
	Begin
    ecs := biblio[i];
		if ecs.systeme <> Systeme then
			continue;
		if (ecs.energie <> Energie) then
			continue;
		if (ecs.typeE <> typpe) then
			continue;
		if (ecs.stockage <> stockage) then
			continue;
		if (ecs.installation <> installation) then
			continue;
		addList(liste, ecs.ballon);
	end;
	result := Liste;
End;


Function TBiblioEcs3CL.getVeilleuse(systeme,energie,typpe,stockage,installation,ballon : String) : TStringList;
Var
	i: Integer;
	Liste : TStringList;
  ecs : TIEcsItem;
Begin
	Liste := TStringList.Create;
	For i := 1 to nb do
	Begin
    ecs := biblio[i];
		if ecs.systeme <> Systeme then
			continue;
		if (ecs.energie <> Energie) then
			continue;
		if (ecs.typeE <> typpe) then
			continue;
		if (ecs.stockage <> stockage) then
			continue;
		if (ecs.installation <> installation) then
			continue;
		if (ecs.ballon <> ballon) then
			continue;
		addList(liste, ecs.veilleuse);
	end;
	result := Liste;
End;

Function TBiblioEcs3CL.getECS(systeme,energie,typpe,stockage,installation,ballon,veilleuse : String) : TIEcsItem;
Var
	 i : Integer;
  ecs : TIEcsItem;
Begin
	result := ECS_VIDE;
	For i := 1 to nb do
	Begin
    ecs := biblio[i];
		if ecs.systeme <> Systeme then
			continue;
		if (ecs.energie <> Energie) then
			continue;
		if (ecs.typeE <> typpe) then
			continue;
		if (ecs.stockage <> stockage) then
			continue;
		if (ecs.installation <> installation) then
			continue;
		if (ecs.ballon <> ballon) then
			continue;
		if (ecs.veilleuse <> veilleuse) then
			continue;
		result := ecs;
		exit;
	End;
End;


initialization

  with ECS_VIDE do
  begin
		systeme := '';
		energie := '';
		typeE := '';
		stockage := '';
		installation := '';
		ballon := '';
		veilleuse := '';
		IEcs := '';
		CEcs := '';
		Accu := '';
	end;


end.
