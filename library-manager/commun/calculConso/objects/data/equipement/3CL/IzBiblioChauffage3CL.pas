unit IzBiblioChauffage3CL;


interface

uses
	Classes, IzBiblioData, IzConstantesConso;
type

		TChauffage = record
			chauffage : string ;
			generateur : string ;
			energie : string ;
			typeC : string ;
			emetteur : string ;
			robinetTh : string ;
			prog : string ;
			ICh : string ;
			IChAux : string ;
			Accu : string ;
			Ref : string ;
			Rdt : string;

			isolationReseau : string;
			option1 : string;
			alphaPCSI : double;

			ichDpe : double;
			progDpe : double;
			Rd : double;
			Re : double;
			Rg : double;
			Rr : double;

			A1 : double;
			A2 : double;
//			corCh : boolean;
//			chaudiereAppoint : boolean;
		end;

		TCChauff = Array of TChauffage;

	type TBiblioChauffage3CL = class(TBiblioData)
		public
			biblio : TCChauff;
			constructor Create(aFileName : string); override;
      destructor Destroy();override;


			function getNomChauffage(filter : TChauffageFilter) : TStringList;
			function getGenerateur(filter : TChauffageFilter ; Chauffage : String) : TStringList;
			function getEnergie(filter : TChauffageFilter ; Chauffage,Generateur : String) : TStringList;
			function getTypeChauffage(filter : TChauffageFilter ; Chauffage,Generateur,Energie : String) : TStringList;
			function getEmetteur(filter : TChauffageFilter ; Chauffage,Generateur,Energie,typeChauffage : string) : TStringList;
			function getRobinet(filter : TChauffageFilter ; Chauffage,Generateur,Energie,typeChauffage,
					emetteur : string) : TStringList;
			function getProgrammateur(filter : TChauffageFilter ; Chauffage,Generateur,Energie,typeChauffage,
					emetteur, robinet : string) : TStringList;
			function getIsolationReseau(filter : TChauffageFilter ; Chauffage,Generateur,Energie,typeChauffage,
					emetteur, robinet, programmateur : string) : TStringList;
			function getOption1(filter : TChauffageFilter ; Chauffage,Generateur,Energie,typeChauffage,
					emetteur, robinet, programmateur, isolationReseau : string) : TStringList;

			function getChauffage(filter : TChauffageFilter ; Chauffage,Generateur,Energie,typeChauffage,
					emetteur, robinet, programmateur, isolationReseau, option1 : string) : TChauffage;

(*
			function Menu(Chauffage,Generateur,Energie,TypeE,Emetteur,Robinet,Programmateur : String; Niveau : Integer) : TStringList;
			function getICh(Chauffage,Generateur,Energie,TypeE,Emetteur,Robinet,Programmateur : String) : String;
			function getAux(Chauffage,Generateur,Energie,TypeE,Emetteur,Robinet,Programmateur : String) : String;
			function getAccumulation(Chauffage,Generateur,Energie,TypeE,Emetteur,Robinet,Programmateur : String) : String;
*)
	end;

	var CHAUFFAGE_VIDE : TChauffage;


implementation

uses
	SysUtils, IzUtilitaires;

	Constructor TBiblioChauffage3CL.Create(aFileName : string);
	Var
		Temp : TStringList;
		cpt : Integer;
    cptItem : integer;
		Ligne : String;
	Begin
		inherited create(aFileName);
		Temp := getStringList();
		SetLength(biblio, nb);


    cpt := 0;
		For cptItem := 0 to nb - 1 do
		Begin
			ligne := Temp[cptItem];

			biblio[cpt].chauffage := getItemTab(Ligne);
			biblio[cpt].generateur := getItemTab(Ligne);
			biblio[cpt].energie := getItemTab(Ligne);
			biblio[cpt].typeC := getItemTab(Ligne);
			biblio[cpt].emetteur := getItemTab(Ligne);
			biblio[cpt].robinetTh := getItemTab(Ligne);
			biblio[cpt].prog := getItemTab(Ligne);
      //on ne prend en compte que les chauffages qui n'ont pas de programmateur
      if getBool(biblio[cpt].prog) and (biblio[cpt].prog <> '*') then
        continue;
			biblio[cpt].ICh := getItemTab(Ligne);
			biblio[cpt].IChAux := getItemTab(Ligne);
			biblio[cpt].Accu := getItemTab(Ligne);
			biblio[cpt].Ref := getItemTab(Ligne);
			biblio[cpt].Rdt := getItemTab(Ligne);

			biblio[cpt].isolationReseau :=getItemTab(Ligne);
			biblio[cpt].option1 := getItemTab(Ligne);

			biblio[cpt].alphaPCSI := getDouble(getItemTab(ligne));
			biblio[cpt].ichDpe := getDouble(getItemTab(Ligne));
			biblio[cpt].progDpe := getDouble(getItemTab(Ligne));
			biblio[cpt].Rd := getDouble(getItemTab(Ligne));
			biblio[cpt].Re := getDouble(getItemTab(Ligne));
			biblio[cpt].Rg := getDouble(getItemTab(Ligne));
			biblio[cpt].Rr := getDouble(getItemTab(Ligne));

			//une colonne pour rien
			getItemTab(ligne);


			biblio[cpt].A1 := getDouble(getItemTab(Ligne));
			biblio[cpt].A2 := getDouble(getItemTab(Ligne));
//			biblio[i].corCh := getBool(getItemTab(Ligne));
//			biblio[i].chaudiereAppoint := getBool(getItemTab(Ligne));
      inc(cpt);
		End;
		Temp.free;
end;


destructor TBiblioChauffage3CL.Destroy();
begin
  SetLength(biblio,0);
  inherited Destroy();
end;


	function TBiblioChauffage3CL.getNomChauffage(filter : TChauffageFilter) : TStringList;
	var
		ret : TStringList;
		cpt : integer;
		chauff : TChauffage;
	begin
		ret := TStringList.Create;

		for cpt := 0 to nb - 1 do
		begin
			chauff := biblio[cpt];
      case filter of
//        CHAUFFAGE_FILTER_CHAUDIERE_APPOINT :
//        begin
//          if not(chauff.chaudiereAppoint) then
//            continue;
//        end;
        CHAUFFAGE_FILTER_SANS_PROGRAMMATEUR :
        begin
          if getBool(chauff.prog) and (chauff.prog <> '*') then
            continue;
        end;
      end;
      addList(ret, chauff.chauffage);
		end;

		result := ret;
	end;


	function TBiblioChauffage3CL.getGenerateur(filter : TChauffageFilter ; Chauffage : String) : TStringList;
	var
		ret : TStringList;
		cpt : integer;
		chauff : TChauffage;
	begin
		ret := TStringList.Create;

		for cpt := 0 to nb - 1 do
		begin
			chauff := biblio[cpt];
      case filter of
//        CHAUFFAGE_FILTER_CHAUDIERE_APPOINT :
//        begin
//          if not(chauff.chaudiereAppoint) then
//            continue;
//        end;
        CHAUFFAGE_FILTER_SANS_PROGRAMMATEUR :
        begin
          if getBool(chauff.prog) and (chauff.prog <> '*') then
            continue;
        end;
      end;

			if (chauff.chauffage <> chauffage) then
        continue;
			addList(ret, chauff.generateur);
		end;

		result := ret;
	end;


	function TBiblioChauffage3CL.getEnergie(filter : TChauffageFilter ; Chauffage,Generateur : String) : TStringList;
	var
		ret : TStringList;
		cpt : integer;
		chauff : TChauffage;
	begin
		ret := TStringList.Create;

		for cpt := 0 to nb - 1 do
		begin
			chauff := biblio[cpt];
      case filter of
//        CHAUFFAGE_FILTER_CHAUDIERE_APPOINT :
//        begin
//          if not(chauff.chaudiereAppoint) then
//            continue;
//        end;
        CHAUFFAGE_FILTER_SANS_PROGRAMMATEUR :
        begin
          if getBool(chauff.prog) and (chauff.prog <> '*') then
            continue;
        end;
      end;

			if chauff.chauffage <> chauffage then
        continue;
			if chauff.generateur <> generateur then
        continue;
			addList(ret, chauff.energie);
		end;

		result := ret;
	end;


	function TBiblioChauffage3CL.getTypeChauffage(filter : TChauffageFilter ; Chauffage,Generateur,Energie : String) : TStringList;
	var
		ret : TStringList;
		cpt : integer;
		chauff : TChauffage;
	begin
		ret := TStringList.Create;

		for cpt := 0 to nb - 1 do
		begin
			chauff := biblio[cpt];
      case filter of
//        CHAUFFAGE_FILTER_CHAUDIERE_APPOINT :
//        begin
//          if not(chauff.chaudiereAppoint) then
//            continue;
//        end;
        CHAUFFAGE_FILTER_SANS_PROGRAMMATEUR :
        begin
          if getBool(chauff.prog) and (chauff.prog <> '*') then
            continue;
        end;
      end;

			if chauff.chauffage <> chauffage then
        continue;
			if chauff.generateur <> generateur then
        continue;
			if chauff.energie <> Energie then
        continue;

			addList(ret, chauff.typeC);
		end;

		result := ret;
	end;


	function TBiblioChauffage3CL.getEmetteur(filter : TChauffageFilter ; Chauffage,Generateur,Energie,typeChauffage : string) : TStringList;
	var
		ret : TStringList;
		cpt : integer;
		chauff : TChauffage;
	begin
		ret := TStringList.Create;

		for cpt := 0 to nb - 1 do
		begin
			chauff := biblio[cpt];
      case filter of
//        CHAUFFAGE_FILTER_CHAUDIERE_APPOINT :
//        begin
//          if not(chauff.chaudiereAppoint) then
//            continue;
//        end;
        CHAUFFAGE_FILTER_SANS_PROGRAMMATEUR :
        begin
          if getBool(chauff.prog) and (chauff.prog <> '*') then
            continue;
        end;
      end;
			if chauff.chauffage <> chauffage then
        continue;
			if chauff.generateur <> generateur then
        continue;
			if chauff.energie <> Energie then
        continue;
			if chauff.typeC <> typeChauffage then
        continue;

			addList(ret, chauff.emetteur);
		end;

		result := ret;
	end;


	function TBiblioChauffage3CL.getRobinet(filter : TChauffageFilter ; Chauffage,Generateur,Energie,typeChauffage,
			emetteur : string) : TStringList;
	var
		ret : TStringList;
		cpt : integer;
		chauff : TChauffage;
	begin
		ret := TStringList.Create;

		for cpt := 0 to nb - 1 do
		begin
			chauff := biblio[cpt];
      case filter of
//        CHAUFFAGE_FILTER_CHAUDIERE_APPOINT :
//        begin
//          if not(chauff.chaudiereAppoint) then
//            continue;
//        end;
        CHAUFFAGE_FILTER_SANS_PROGRAMMATEUR :
        begin
          if getBool(chauff.prog) and (chauff.prog <> '*') then
            continue;
        end;
      end;
			if chauff.chauffage <> chauffage then
        continue;
			if chauff.generateur <> generateur then
        continue;
			if chauff.energie <> Energie then
        continue;
			if chauff.typeC <> typeChauffage then
        continue;
			if chauff.emetteur <> emetteur then
        continue;

			addList(ret, chauff.robinetTh);
		end;

		result := ret;
	end;


	function TBiblioChauffage3CL.getProgrammateur(filter : TChauffageFilter ; Chauffage,Generateur,Energie,typeChauffage,
			emetteur, robinet : string) : TStringList;
	var
		ret : TStringList;
		cpt : integer;
		chauff : TChauffage;
	begin
		ret := TStringList.Create;

		for cpt := 0 to nb - 1 do
		begin
			chauff := biblio[cpt];
      case filter of
//        CHAUFFAGE_FILTER_CHAUDIERE_APPOINT :
//        begin
//          if not(chauff.chaudiereAppoint) then
//            continue;
//        end;
        CHAUFFAGE_FILTER_SANS_PROGRAMMATEUR :
        begin
          if getBool(chauff.prog) and (chauff.prog <> '*') then
            continue;
        end;
      end;
			if chauff.chauffage <> chauffage then
        continue;
			if chauff.generateur <> generateur then
        continue;
			if chauff.energie <> Energie then
        continue;
			if chauff.typeC <> typeChauffage then
        continue;
			if chauff.emetteur <> emetteur then
        continue;
			if chauff.robinetTh <> robinet then
        continue;

			addList(ret, chauff.prog);
		end;

		result := ret;
	end;


	function TBiblioChauffage3CL.getIsolationReseau(filter : TChauffageFilter ; Chauffage,Generateur,Energie,typeChauffage,
			emetteur, robinet, programmateur : string) : TStringList;
	var
		ret : TStringList;
		cpt : integer;
		chauff : TChauffage;
	begin
		ret := TStringList.Create;

		for cpt := 0 to nb - 1 do
		begin
			chauff := biblio[cpt];
      case filter of
//        CHAUFFAGE_FILTER_CHAUDIERE_APPOINT :
//        begin
//          if not(chauff.chaudiereAppoint) then
//            continue;
//        end;
        CHAUFFAGE_FILTER_SANS_PROGRAMMATEUR :
        begin
          if getBool(chauff.prog) and (chauff.prog <> '*') then
            continue;
        end;
      end;
			if chauff.chauffage <> chauffage then
        continue;
			if chauff.generateur <> generateur then
        continue;
			if chauff.energie <> Energie then
        continue;
			if chauff.typeC <> typeChauffage then
        continue;
			if chauff.emetteur <> emetteur then
        continue;
			if chauff.robinetTh <> robinet then
        continue;
			if chauff.prog <> programmateur then
        continue;

			addList(ret, chauff.isolationReseau);
		end;

		result := ret;
	end;


	function TBiblioChauffage3CL.getOption1(filter : TChauffageFilter ; Chauffage,Generateur,Energie,typeChauffage,
			emetteur, robinet, programmateur, isolationReseau : string) : TStringList;
	var
		ret : TStringList;
		cpt : integer;
		chauff : TChauffage;
	begin
		ret := TStringList.Create;

		for cpt := 0 to nb - 1 do
		begin
			chauff := biblio[cpt];
      case filter of
//        CHAUFFAGE_FILTER_CHAUDIERE_APPOINT :
//        begin
//          if not(chauff.chaudiereAppoint) then
//            continue;
//        end;
        CHAUFFAGE_FILTER_SANS_PROGRAMMATEUR :
        begin
          if getBool(chauff.prog) and (chauff.prog <> '*') then
            continue;
        end;
      end;
			if chauff.chauffage <> chauffage then
        continue;
			if chauff.generateur <> generateur then
        continue;
			if chauff.energie <> Energie then
        continue;
			if chauff.typeC <> typeChauffage then
        continue;
			if chauff.emetteur <> emetteur then
        continue;
			if chauff.robinetTh <> robinet then
        continue;
			if chauff.prog <> programmateur then
        continue;
			if chauff.isolationReseau <> isolationReseau then
        continue;

			addList(ret, chauff.option1);
		end;

		result := ret;
	end;




	function TBiblioChauffage3CL.getChauffage(filter : TChauffageFilter ; Chauffage,Generateur,Energie,typeChauffage,
			emetteur, robinet, programmateur, isolationReseau, option1 : string) : TChauffage;
	var
		cpt : integer;
		chauff : TChauffage;
	begin
		result := CHAUFFAGE_VIDE;
		for cpt := 0 to nb - 1 do
		begin
			chauff := biblio[cpt];
      case filter of
//        CHAUFFAGE_FILTER_CHAUDIERE_APPOINT :
//        begin
//          if not(chauff.chaudiereAppoint) then
//            continue;
//        end;
        CHAUFFAGE_FILTER_SANS_PROGRAMMATEUR :
        begin
          if getBool(chauff.prog) and (chauff.prog <> '*') then
            continue;
        end;
      end;
			if chauff.chauffage <> chauffage then
        continue;
			if chauff.generateur <> generateur then
        continue;
			if chauff.energie <> Energie then
        continue;
			if chauff.typeC <> typeChauffage then
        continue;
			if chauff.emetteur <> emetteur then
        continue;
			if chauff.robinetTh <> robinet then
        continue;
			if chauff.prog <> programmateur then
        continue;
			if chauff.isolationReseau <> isolationReseau then
        continue;
			if chauff.option1 <> option1 then
        continue;

			result := chauff;
			exit;
		end;
	end;


initialization;
	CHAUFFAGE_VIDE.chauffage := '';
	CHAUFFAGE_VIDE.generateur := '';
	CHAUFFAGE_VIDE.energie :='';
	CHAUFFAGE_VIDE.typeC :='';
	CHAUFFAGE_VIDE.emetteur :='';
	CHAUFFAGE_VIDE.robinetTh :='';
	CHAUFFAGE_VIDE.prog :='';
	CHAUFFAGE_VIDE.ICh :='';
	CHAUFFAGE_VIDE.IChAux :='';
	CHAUFFAGE_VIDE.Accu :='';
	CHAUFFAGE_VIDE.Ref :='';
	CHAUFFAGE_VIDE.Rdt :='';

	CHAUFFAGE_VIDE.isolationReseau :='';
	CHAUFFAGE_VIDE.option1 :='';
	CHAUFFAGE_VIDE.alphaPCSI := -1;
	CHAUFFAGE_VIDE.ichDpe := -1;
	CHAUFFAGE_VIDE.progDpe := -1;
	CHAUFFAGE_VIDE.Rd := -1;
	CHAUFFAGE_VIDE.Re := -1;
	CHAUFFAGE_VIDE.Rg := -1;
	CHAUFFAGE_VIDE.Rr := -1;
	CHAUFFAGE_VIDE.A1 := -1;
	CHAUFFAGE_VIDE.A2 := -1;
//	CHAUFFAGE_VIDE.corCh := false;
//	CHAUFFAGE_VIDE.chaudiereAppoint := false;



end.
