unit IzBiblioSystem;

interface

uses
	IzBiblioChauffage3CL,
  IzBiblioEcs3CL,
  IzBiblioEnergieTarif3CL,
	IzBiblioEnergieRepartition3CL,
  IzUtilitaires,
	diaParameters;

	type TBiblioFileNames = record
		energieRepartition,
		energieTarif,
		chauffageMaison,
		chauffageAppart,
		ecsMaison,
		ecsAppart,
		climatisation
			: string;
	end;


	type TBiblioSystem = class(TObject)
		private
			innerTypeBatiment : TTypeBatiment;

			procedure checkChauffage(biblioChauffage : TBiblioChauffage3CL);
			procedure checkECS(biblioECS : TBiblioECS3CL);


		public

			biblioChauffageMaison : TBiblioChauffage3CL;
			biblioChauffageAppart : TBiblioChauffage3CL;

			biblioEcsMaison : TBiblioEcs3CL;
			biblioEcsAppart : TBiblioEcs3CL;

			biblioClimatisation : TDiaParams;

			biblioEnergieTarif : TBiblioEnergieTarif3CL;
			biblioEnergieRepartition : TBiblioEnergieRepartition3CL;


			constructor Create(fileNames : TBiblioFileNames);overload;
			destructor Destroy();override;

			function getBiblioChauffage(tBatiment : TTypeBatiment) : TBiblioChauffage3CL;overload;
			function getBiblioChauffage() : TBiblioChauffage3CL;overload;
			function getBiblioEcs(tBatiment : TTypeBatiment) : TBiblioEcs3CL;overload;
			function getBiblioEcs() : TBiblioEcs3CL;overload;

			property typeBatiment : TTypeBatiment write innerTypeBatiment;
	end;

	var biblioSystem : TBiblioSystem;

	function getBiblioSystem() : TBiblioSystem;

	function getDefaultBiblioFileNames():TBiblioFileNames;
	procedure initBiblioSystem();

implementation

uses
  SysUtils, Forms, UtilitairesDialogie;

	constructor TBiblioSystem.Create(fileNames : TBiblioFileNames);
	begin
		inherited create();

    innerTypeBatiment := BATIMENT_Maison;

		//on charge d'abord les �nergies pour ne proposer que les �quipements pour lesquels on a des infos
		biblioEnergieRepartition := TBiblioEnergieRepartition3CL.Create(fileNames.energieRepartition);
		biblioEnergieTarif := TBiblioEnergieTarif3CL.CreateWithLink(fileNames.energieTarif, biblioEnergieRepartition);

		biblioChauffageMaison := TBiblioChauffage3CL.Create(fileNames.chauffageMaison);
		checkChauffage(biblioChauffageMaison);
		biblioChauffageAppart := TBiblioChauffage3CL.Create(fileNames.chauffageAppart);
		checkChauffage(biblioChauffageAppart);

		biblioEcsMaison := TBiblioEcs3CL.Create(fileNames.ecsMaison);
		checkECS(biblioEcsMaison);
		biblioEcsAppart := TBiblioEcs3CL.Create(fileNames.ecsAppart);
		checkECS(biblioEcsAppart);

		biblioClimatisation := TDiaParams.Create();
		biblioClimatisation.load(fileNames.climatisation);
	end;


	destructor TBiblioSystem.Destroy();
	begin
		biblioChauffageMaison.Free;
		biblioChauffageAppart.Free;
		biblioEcsMaison.Free;
		biblioEcsAppart.Free;
		biblioEnergieTarif.Free;
		biblioEnergieRepartition.Free;
    biblioClimatisation.Free;
		inherited destroy();
	end;


	function TBiblioSystem.getBiblioChauffage(tBatiment : TTypeBatiment) : TBiblioChauffage3CL;
	begin
		if (tBatiment = BATIMENT_Maison) then
			getBiblioChauffage := biblioChauffageMaison
		else
			getBiblioChauffage := biblioChauffageAppart;
	end;

	function TBiblioSystem.getBiblioEcs(tBatiment : TTypeBatiment) : TBiblioEcs3CL;
	begin
		if (tBatiment = BATIMENT_Maison) then
			getBiblioEcs := biblioEcsMaison
		else
			getBiblioEcs := biblioEcsAppart;
	end;

	function TBiblioSystem.getBiblioChauffage() : TBiblioChauffage3CL;
	begin
		result := getBiblioChauffage(innerTypeBatiment);
	end;


	function TBiblioSystem.getBiblioEcs() : TBiblioEcs3CL;
	begin
		result := getBiblioEcs(innerTypeBatiment);
	end;


	procedure TBiblioSystem.checkChauffage(biblioChauffage : TBiblioChauffage3CL);
	var
	i: Integer;
	begin
		for i := 0 to biblioChauffage.nb -1 do
		begin
			if (biblioEnergieTarif.getEnergieIndex(biblioChauffage.biblio[i].energie) = -1) and (biblioChauffage.biblio[i].energie <> '*') then
			begin
					biblioChauffage.biblio[i].chauffage := '';
					biblioChauffage.biblio[i].generateur := '';
					biblioChauffage.biblio[i].energie := '';
					biblioChauffage.biblio[i].typeC := '';
					biblioChauffage.biblio[i].typeC := '';
					biblioChauffage.biblio[i].emetteur := '';
					biblioChauffage.biblio[i].robinetTh := '';
					biblioChauffage.biblio[i].prog := '';
					biblioChauffage.biblio[i].ICh := '';
					biblioChauffage.biblio[i].IChAux := '';
					biblioChauffage.biblio[i].Accu := '';
					biblioChauffage.biblio[i].Ref := '';
					biblioChauffage.biblio[i].Rdt := '';
			end;
		end;
	end;


	procedure TBiblioSystem.checkECS(biblioECS : TBiblioECS3CL);
	var
	i: Integer;
//	j: Integer;
	begin
		for i := 0 to biblioECS.nb do
		begin
			if (biblioEnergieTarif.getEnergieIndex(biblioECS.biblio[i].energie) = -1) and (biblioECS.biblio[i].energie <> '*') then
			begin
				biblioECS.biblio[i].systeme := '';
				biblioECS.biblio[i].energie := '';
				biblioECS.biblio[i].typeE := '';
				biblioECS.biblio[i].stockage := '';
				biblioECS.biblio[i].installation := '';
				biblioECS.biblio[i].ballon := '';
				biblioECS.biblio[i].veilleuse := '';
				biblioECS.biblio[i].IEcs := '';
				biblioECS.biblio[i].CEcs := '';
				biblioECS.biblio[i].Accu := '';
			end;
		end;
	end;


	function getBiblioSystem() : TBiblioSystem;
	begin
		if (biblioSystem = nil) then
		begin
			initBiblioSystem();
		end;
		result := biblioSystem;
	end;

	procedure initBiblioSystem();
	var
		fileNames : TBiblioFileNames;
	begin
		fileNames := getDefaultBiblioFileNames();
		if biblioSystem <> nil then
			biblioSystem.Free;
		biblioSystem := TBiblioSystem.Create(fileNames);
	end;

	function getDefaultBiblioFileNames():TBiblioFileNames;
	var
		fileNames : TBiblioFileNames;
    libPath : string;
	begin
    libPath := dialogieLibPath;
		fileNames.chauffageMaison := libPath + 'Chauffage Maison.txt';
		fileNames.chauffageAppart := libPath + 'Chauffage Appart.txt';
		fileNames.ecsMaison := libPath + 'ECS Maison.txt';
		fileNames.ecsAppart := libPath + 'ECS Appart.txt';
		fileNames.climatisation := libPath + 'Climatisation.txt';
    fileNames.energieRepartition := libPath + 'Repartition.txt';
    fileNames.energieTarif := libPath + 'EnergieBase.txt';
		result := fileNames;
	end;

	initialization
		biblioSystem := nil;

	finalization
    freeAndNil(biblioSystem);
end.
