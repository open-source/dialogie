unit IzConstantesConso;


interface

uses
	Classes, Graphics;

const JOURS_OCCUPATION_ANNUEL = 330;

	type TTypeUsage = (
										USAGE_CHAUFFAGE_DIRECT = 1,
										USAGE_CHAUFFAGE_ACCUMULATION = 11,
										USAGE_ECS_DIRECT = 2,
										USAGE_ECS_ACCUMULATION = 12,
										USAGE_CUISSON = 3,
										USAGE_AUX_CHAUFFAGE = 20,
										USAGE_AUX_VENTILATION = 21,
										USAGE_LAVAGE = 22,
										USAGE_FROID = 23,
										USAGE_BUREAUTIQUE = 24,
										USAGE_MENAGER = 25,
										USAGE_ECLAIRAGE = 5,
										USAGE_CLIMATISATION = 6
										);

	type TTypeRessource = (
												RESSOURCE_URANIUM = 1,
												RESSOURCE_GAZ_NATUREL = 2,
												RESSOURCE_PETROLE = 3,
												RESSOURCE_CHARBON = 4,
												RESSOURCE_SOLAIRE = 5,
												RESSOURCE_BIOMASSE = 6,
												RESSOURCE_VENT = 7,
												RESSOURCE_HYDRAULIQUE = 8,
												RESSOURCE_GEOTHERMIE = 9,
												RESSOURCE_ENERGIE_PRIMAIRE = 10,
												RESSOURCE_11 = 11,
												RESSOURCE_12 = 12,
												RESSOURCE_13 = 13,
												RESSOURCE_14 = 14,
												RESSOURCE_15 = 15,
												RESSOURCE_16 = 16,
												RESSOURCE_17 = 17
												);

	type TChauffageFilter = (
												CHAUFFAGE_FILTER_NONE = -1,
//												CHAUFFAGE_FILTER_CHAUDIERE_APPOINT = 0,
												CHAUFFAGE_FILTER_SANS_PROGRAMMATEUR = 1
												);


	type TUtilisationCoeff = record
		libelle : string;
		coeff : double;
	end;

	type TUtilisationCoeffs = class
		private
			class var table : array[0..3] of TUtilisationCoeff;
		public
			class function getCoeff(libelle : string) : double;
			class function getLibelles() : TStrings;
			class function getLibellesPerso() : TStrings;
	end;
	 TMeteo = class
		 Nom,NomFichier : String;
		 Altitude,Lattitude,Longitude : Double;
		 TemperatureSol : Double;
		 GMT : Integer;
	 End;

	 TTableResult = Array[1..9,1..6] of Double;

	 TMensuel = array[1..12,0..10] of double;//valeur pour 11 'appareils'/types d'�nergie pour 12 mois
	 TSemaine = array[1..7,1..24] of Integer;
	 TRessources = Array[1..2, RESSOURCE_URANIUM..RESSOURCE_17] of Double;
	 TEnvironnement = Array[1..3, 1..17] of Double;

	 HYPERDONNEES = record
			sommeRM: double;
			Eclipse, Nucleaire, ApportsI: boolean;
			Pas: integer;
			Mode: char; {S : Simulation SRY, T : simulation TRY }
			Chargement: boolean;
			Meteo: boolean;
			Ete: boolean;
			Hiver: boolean;
			debut: integer;
			Fin: integer;
			HeureSolaire: boolean;
			TimeLong: integer;
			ControleVentilation: boolean;
			ZoneFroide, ZoneTiede, ZoneControlee: integer;
			diff1, diff2: integer;
			TemperatureMax: integer;
			ConvergenceList : TStringList;
	 end;
const

   APPORT_INTERNE_HUMAIN : integer = 70;
	 FichierAide = 'dialogie.chm';

//valeur prise dans la biblioth�que
	 Ventilpardefaut = 1.18;
	 ConsigneDefautJour = 20;
	 ConsigneDefautNuit = 17;
	 ConsigneDefautAbsence = 17;
	 FichierPresence = 'presence.txt';

		ListeFichierDate : Array[1..14] of String = (
    'indiv-chauff.txt',
		'collectif-chauff.txt',
    'CorrZone1.txt',
    'CorrZone2.txt',
		'Cuisine.txt',
    'ECS Appart.txt',
    'ECS Maison.txt',
    'energieBase.txt',//liste et tarif des �nergies
		'modulationEclairage.txt',
    'Repartition.txt',
    'permeabilite26.txt',//permeabilite
    'tau.txt',//taus
		'ventilation Appart.txt',//ventilation appart
    'ventil26.txt');//ventilation maison

		TEP = 11620;

		 EcartStandardPerso = 100000;
		 LettreMat = 'M';
		 LettreElem = 'E';
		 Unite : Array[1..4] of String = (' kWh',' kWh',' W',' W');
		 LabelDegre = ' �C';


		 FichierMateriaux = 'Mat.txt';
		 FichierCompos = 'Compo.txt';
		 FichierFenetres = 'Fen.txt';
		 FichierFenetresNS = 'FenNS.txt';


		 // NOM DES DOSSIERS

		 dossierBiblio = 'Biblioth�ques\';
		 dossierMeteo = 'Fichiers Meteo\';


		 // NOM DES FICHIERS

		 ExportMateriau = 'Materiau';
		 ExportElement = 'Element';
		 ExportEtats = 'Etats';
		 ExportAlbedo = 'Albedo';
		 ExportPlantation = 'Plantation';
		 ExportScenario = 'Scenario';
		 ExportMenuiseries = 'Menuiseries';

		 NomFichierGeneralites = 'generalites.txt';
		 NomFichierAlbedos = 'albedos.txt';
		 NomFichierCompositions = 'Composit.txt';
		 NomFichierEtats = 'etats.txt';
		 NomFichierHuisserie = 'huisseri.txt';
		 NomFichierMasquesInt = 'MasquesInt.txt';
		 NomFichierMateriaux = 'Materiau.txt';
		 NomFichierOccult = 'Occultations.txt';
		 NomFichierScenario = 'Scenarios.txt';
		 NomFichierPieces = 'Pieces.txt';
		 NomFichierParois = 'Parois.txt';
		 NomFichierHyperLink = 'hyperlink.txt';
		 NomFichierVentil = 'sources.txt';
		 NomFichierMasquesLointains = 'masqueloins.txt';
		 NomFichierListeScen = 'ListeScenarios.txt';

		 LabelFichierTitre = 'Titre.txt';

implementation
(*
		class TUtilisationCoeffs
*)
	class function TUtilisationCoeffs.getCoeff(libelle : string) : double;
	var
	i: integer;
	begin
		result := 0;
		for i := 0 to 3 do
		begin
			if (table[i].libelle = libelle) then
			begin
				result := table[i].coeff;
				exit;
			end;
		end;
	end;


	class function TUtilisationCoeffs.getLibelles() : TStrings;
	var
		l : TStringList;
		i : integer;
	begin
		l := TStringList.Create;
		for i := 0 to 2 do
		begin
			l.Add(table[i].libelle);
		end;
		result := l;
	end;

	class function TUtilisationCoeffs.getLibellesPerso() : TStrings;
	var
		l : TStringList;
		i : integer;
	begin
		l := TStringList.Create;
		for i := 0 to 3 do
		begin
			l.Add(table[i].libelle);
		end;
		result := l;
	end;


initialization
	TUtilisationCoeffs.table[0].libelle := 'Important';
	TUtilisationCoeffs.table[0].coeff := 2.0;
	TUtilisationCoeffs.table[1].libelle := 'Normal';
	TUtilisationCoeffs.table[1].coeff := 1.0;
	TUtilisationCoeffs.table[2].libelle := 'Faible';
	TUtilisationCoeffs.table[2].coeff := 0.6;
	TUtilisationCoeffs.table[3].libelle := 'Personnalis�';
//	TUtilisationCoeffs.table[3].coeff := -1.0;
	TUtilisationCoeffs.table[3].coeff := 0;

finalization

end.
