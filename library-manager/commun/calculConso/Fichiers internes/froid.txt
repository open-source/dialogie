Combiné	225 l froid + 75 l congel	A	37.7 W	24	365	330
Combiné	225 l froid + 75 l congel	A+	31.4 W	24	365	275
Combiné	225 l froid + 75 l congel	B	45.7 W	24	365	400
Combiné	225 l froid + 75 l congel	C	57.1 W	24	365	500
Combiné	225 l froid + 75 l congel	Courant	74.2 W	24	365	650
Congélateur	Armoire 100 l	A	22.8 W	24	365	200
Congélateur	Armoire 100 l	B	32.0 W	24	365	280
Congélateur	Armoire 100 l	Courant	40.0 W	24	365	350
Congélateur	Coffre 200 - 250  l	A	25.1 W	24	365	220
Congélateur	Coffre 200 - 250  l	A+	17.1 W	24	365	150
Congélateur	Coffre 200 - 250  l	B	34.2 W	24	365	300
Congélateur	Coffre 200 - 250  l	C	40.0 W	24	365	350
Congélateur	Coffre 200 - 250  l	Courant	57.1 W	24	365	500
Congélateur	Coffre 250 - 300 l	A	37.1 W	24	365	325
Congélateur	Coffre 250 - 300 l	A+	28.5 W	24	365	250
Congélateur	Coffre 250 - 300 l	B	42.8 W	24	365	375
Congélateur	Coffre 250 - 300 l	C	48.5 W	24	365	425
Congélateur	Coffre 250 - 300 l	Courant	70.8 W	24	365	620
Réfrigérateur	Compact 150 l	A	16.0 W	24	365	140
Réfrigérateur	Compact 150 l	A+	12.6 W	24	365	110
Réfrigérateur	Compact 150 l	A++	9.7 W	24	365	85
Réfrigérateur	Compact 150 l	B	21.1 W	24	365	185
Réfrigérateur	Compact 150 l	Courant	25.1 W	24	365	220
