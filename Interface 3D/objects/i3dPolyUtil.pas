unit i3dPolyUtil;

interface

uses
  i3dGeometry,
  classes,
  sysutils;

procedure draw_poly(Room : TRoom; DestListe : TList);
function CheckIntersection(A, B, AA, BB : LTPoint; OnLine : boolean) : boolean;
function PointInOut(P : LTPoint; Points : TList) : boolean;
function PointInOut2(P : LTpoint; points : TList) : Integer;
function Atan2(Dy, Dx : extended) : extended;
function PointSurSegment(Point, p1, p2 : LTPoint) : boolean;

implementation

uses
  Sketch,
  i3dunit3D,
  i3dSketchLevel,
  i3dSketchObject, Winapi.Windows;

procedure draw_poly(Room : TRoom; DestListe : TList);
var
  Liste : TList;
  i, j : integer;
  indice : integer;
  Index1, Index2 : integer;
  OnLine : boolean;
  Croise : boolean;
  Centre : LTPoint;
  PrecCount : integer;
  Sortir : boolean;
  VVolume : double;
  h1, h2, h3, Surface : double;
  scale : double;
  Level : TSketchLevel;
begin
  Index1 := 0;
  Index2 := 0;
  Level := nil;
  for i := 0 to project.sketchObject.Levels.Count - 1 do
  begin
    Level := project.sketchObject.Levels[i];
    if Level.Rooms.IndexOf(Room) <> -1 then
      Break;
  end;
  PrecCount := -1;
  Centre := LTPoint.Create(Point(0, 0));
  Liste := TList.Create;
  indice := 0;

  Liste.add(Room.Points[0]);
  if FormSketch.PageControl1.ActivePage = FormSketch.TabSheet3D then
  begin
    for i := 1 to Room.Points.Count - 2 do
      if not PointSurSegment(Room.Points[i], Room.Points[i - 1], Room.Points[i + 1]) then
        Liste.add(Room.Points[i])
      else
        indice := 0
  end
  else
  begin
    for i := 1 to Room.Points.Count - 2 do
      Liste.add(Room.Points[i]);
  end;

  Sortir := False;
  while (Liste.Count > 2) and not Sortir do
  begin
    Croise := False;
    for j := 0 to Room.Points.Count - 2 do
    begin
      Index1 := indice - 1;
      if Index1 < 0 then
        Index1 := Index1 + Liste.Count;
      Index2 := indice + 1;
      if Index2 >= Liste.Count then
        Index2 := Index2 - Liste.Count;
      OnLine := False;
      Croise := Croise or CheckIntersection(Liste[Index1],
        Liste[Index2], Room.Points[j], Room.Points[j + 1],
        OnLine);
      if (LTPoint(Liste[Index1]).x = LTPoint(Liste[Index2]).x) and
        (LTPoint(Liste[Index1]).y = LTPoint(Liste[Index2]).y) then
        Croise := True;
      Centre.xx := (LTPoint(Liste[Index1]).xx + LTPoint(Liste[Index2]).xx) / 2;
      Centre.yy := (LTPoint(Liste[Index1]).yy + LTPoint(Liste[Index2]).yy) / 2;
      Centre.x := Round(Centre.xx);
      Centre.y := Round(Centre.yy);
      Croise := Croise or not(PointInOut(Centre, Room.Points));
    end;

    if not Croise then
    begin
      PrecCount := -1;
      DestListe.add(Liste[Index1]);
      DestListe.add(Liste[indice]);
      DestListe.add(Liste[Index2]);

      h1 := CorrigeHauteur(Liste[Index1], Liste[indice], Room);
      h2 := CorrigeHauteur(Liste[indice], Liste[Index2], Room);
      h3 := CorrigeHauteur(Liste[Index2], Liste[Index1], Room);
      scale := project.sketchObject.scale;
      Surface := 0;
      Surface := Surface + (LTPoint(Liste[Index1]).xx / scale) *
        (LTPoint(Liste[indice]).yy / scale) - ((LTPoint(Liste[Index1]).yy / scale) *
        (LTPoint(Liste[indice]).xx / scale));
      Surface := Surface + (LTPoint(Liste[indice]).xx / scale) *
        (LTPoint(Liste[Index2]).yy / scale) - ((LTPoint(Liste[indice]).yy / scale) *
        (LTPoint(Liste[Index2]).xx / scale));
      Surface := Surface + (LTPoint(Liste[Index2]).xx / scale) *
        (LTPoint(Liste[Index1]).yy / scale) - ((LTPoint(Liste[Index2]).yy / scale) *
        (LTPoint(Liste[Index1]).xx / scale));
      Surface := Abs(Surface) / 2; // Sqr(Scale);
      VVolume := Surface * ((h1 + h2 + h3) / 3 + Level.Height);
      Room.Volume := Room.Volume + VVolume;

      Liste.Delete(indice);
      inc(indice);
      if indice >= Liste.Count then
        indice := indice - Liste.Count;
    end
    else
    begin
      inc(indice);
      if indice >= Liste.Count then
      begin
        if PrecCount = Liste.Count then
          Sortir := True;
        if PrecCount = -1 then
          PrecCount := 0
        else
          PrecCount := Liste.Count;
        indice := indice - Liste.Count;
      end;
    end;
  end;
  Liste.Free;
  Centre.Free;
end;

function CheckIntersection(A, B, AA, BB : LTPoint; OnLine : boolean) : boolean;
var
  p1, p2, p3, p4, p5, p6, p7, p8, part1, part2, part22, K1, K2, Coeff1,
    Coeff2 : extended;
  TempPoint : LTPoint;
begin
  CheckIntersection := False;
  p3 := (AA.xx - A.xx);
  p1 := (AA.yy - A.yy);
  p5 := (BB.xx - AA.xx);
  p6 := (BB.yy - AA.yy);
  p2 := (B.xx - A.xx);
  p4 := (B.yy - A.yy);
  p8 := (A.xx - AA.xx);
  p7 := (A.yy - AA.yy);

  part1 := ((p1 * p2) - (p3 * p4));
  part2 := ((p4 * p5) - (p2 * p6));
  if Abs(part2) > 0.0000000001 then
    K1 := part1 / part2
  else if part1 = 0 then
    K1 := 0
  else
    K1 := -1;

  part1 := ((p7 * p5) - (p8 * p6));
  part22 := ((p6 * p2) - (p5 * p4));
  if Abs(part22) > 0.0000000001 then
    K2 := part1 / part22
  else if part1 = 0 then
    K2 := 0
  else
    K2 := -1;

  TempPoint := nil;
  if (K1 >= 0) and (K1 <= 1.000000000001) and (K2 > 0) and (K2 < 1) then
  begin
    TempPoint := LTPoint.Create(Point(A.x + Round((B.xx - A.xx) * K2),
      A.y + Round((B.yy - A.yy) * K2)));
    TempPoint.xx := A.xx + (B.xx - A.xx) * K2;
    TempPoint.yy := A.yy + (B.yy - A.yy) * K2;
    TempPoint.x := Round(TempPoint.xx);
    TempPoint.y := Round(TempPoint.yy);
    CheckIntersection := True;
  end
  else if OnLine and (B.xx = AA.xx) and (B.xx = BB.xx) and
    (((B.yy >= AA.yy) and (B.yy <= BB.yy)) or
    ((B.yy >= BB.yy) and (B.yy <= AA.yy))) then
  begin
    TempPoint := LTPoint.Create(Point(B.x, B.y));
    TempPoint.xx := B.xx;
    TempPoint.yy := B.yy;
    TempPoint.x := Round(TempPoint.xx);
    TempPoint.y := Round(TempPoint.yy);
    CheckIntersection := True;
  end
  else if OnLine and (Abs(B.yy - AA.yy) < 0.000001) and
    (Abs(B.yy - BB.yy) < 0.000001) and
    (((B.xx >= AA.xx) and (B.xx <= BB.xx)) or
    ((B.xx >= BB.xx) and (B.xx <= AA.xx))) then
  begin
    TempPoint := LTPoint.Create(Point(B.x, B.y));
    TempPoint.xx := B.xx;
    TempPoint.yy := B.yy;
    TempPoint.x := Round(TempPoint.xx);
    TempPoint.y := Round(TempPoint.yy);
    CheckIntersection := True;
  end
  else if OnLine and (BB.xx <> AA.xx) and (BB.yy <> AA.yy) then
  begin
    Coeff1 := ((B.xx - AA.xx) / (BB.xx - AA.xx));
    Coeff2 := ((B.yy - AA.yy) / (BB.yy - AA.yy));
    if (Coeff1 >= 0) and (Coeff1 <= 1) and
      (Coeff2 >= 0) and (Coeff2 <= 1) and
      (Abs(Coeff1 - Coeff2) < 0.0001) then
    begin
      TempPoint := LTPoint.Create(Point(B.x, B.y));
      TempPoint.xx := B.xx;
      TempPoint.yy := B.yy;
      TempPoint.x := Round(TempPoint.xx);
      TempPoint.y := Round(TempPoint.yy);
      CheckIntersection := True;
    end
  end;
  if TempPoint <> nil then
    TempPoint.Free;
end;



function PointInOut(P : LTPoint; Points : TList) : boolean;
var
  aPolygon: array of TPoint;
  cpt : integer;
  PolyHandle: hRgn;
begin
  result := false;
  if (points = nil) then
    exit;
  if (points.count = 0) then
    exit;
  setLength(aPolygon, points.Count);
  for cpt := 0 to points.Count - 1 do
  begin
    aPolygon[cpt].X := LTPoint(Points[cpt]).x;
    aPolygon[cpt].Y := LTPoint(Points[cpt]).y;
  end;
  PolyHandle := CreatePolygonRgn(aPolygon[0], Length(aPolygon), Winding);
  result     := PtInRegion(PolyHandle,p.X,p.Y);
  DeleteObject(PolyHandle);
end;

(*
function PointInOut(P : LTPoint; Points : TList) : boolean;
var
  i, N0 : integer;
  u, v : double;
  theta, thetai, theta1 : double;
  Sum, Angle, M : double;
begin
  N0 := Points.Count - 1;

  u := LTPoint(Points[0]).x - P.x;
  v := LTPoint(Points[0]).y - P.y;
  theta1 := Atan2(v, u);
  Sum := 0;
  theta := theta1;
  for i := 1 to N0 - 1 do
  begin
    u := LTPoint(Points[i]).x - P.x;
    v := LTPoint(Points[i]).y - P.y;
    thetai := Atan2(v, u);
    Angle := Abs(thetai - theta);
    if Angle > Pi then
      Angle := Angle - (Pi * 2);
    if theta > thetai then
      Angle := -Angle;
    Sum := Sum + Angle;
    theta := thetai;
  end;
  Angle := Abs(theta1 - theta);
  if Angle > Pi then
    Angle := Angle - (Pi * 2);
  if theta > theta1 then
    Angle := -Angle;
  Sum := Sum + Angle;
  M := Abs(Sum) / (Pi * 2);
  result := (M > 0.9);
end;
*)


function PointInOut2(P : LTpoint; points : TList) : Integer;
var
  i, N0 : Integer;
  u, v : Double;
  theta, thetai, theta1 : Double;
  Sum, Angle, M : Double;
  Tol, EPS : Double;
label
  Vingt;
begin
  EPS := 1E-10;
  Tol := 4.0 * EPS * pi;
  N0 := points.count - 1;
  result := -1;
  u := LTpoint(points[0]).X - P.X;
  v := LTpoint(points[0]).Y - P.Y;
  if (u = 0) and (v = 0) then
    goto Vingt;
  if N0 < 2 then
    exit;
  theta1 := Atan2(v, u);
  Sum := 0;
  theta := theta1;
  for i := 1 to N0 - 1 do
  begin
    u := LTpoint(points[i]).X - P.X;
    v := LTpoint(points[i]).Y - P.Y;
    if (u = 0) and (v = 0) then
      goto Vingt;
    thetai := Atan2(v, u);
    Angle := Abs(thetai - theta);
    if (Abs(Angle - pi) < Tol) then
      goto Vingt;
    if Angle > pi then
      Angle := Angle - (pi * 2);
    if theta > thetai then
      Angle := -Angle;
    Sum := Sum + Angle;
    theta := thetai;
  end;
  Angle := Abs(theta1 - theta);
  if (Abs(Angle - pi) < Tol) then
    goto Vingt;
  if Angle > pi then
    Angle := Angle - (pi * 2);
  if theta > theta1 then
    Angle := -Angle;
  Sum := Sum + Angle;
  M := Abs(Sum) / (pi * 2);
  if Abs(M) < Tol then
    exit;
  result := 1;
  exit;
Vingt :
  result := 0;
end;



function Atan2(Dy, Dx : extended) : extended;
var
  TempTan : extended;
begin
  if Dx <> 0 then
  begin
    TempTan := Arctan(Abs(Dy / Dx));
    if Dy < 0 then
      if Dx < 0 then
        result := -Pi + TempTan
      else
        result := -TempTan
    else if Dx < 0 then
      result := Pi - TempTan
    else
      result := TempTan
  end
  else if Dy > 0 then
    result := Pi / 2
  else
    result := -Pi / 2;
end;

function PointSurSegment(Point, p1, p2 : LTPoint) : boolean;
var
  XCross, YCross : double;
  A, B : LTPoint;
  px, py : double;
begin
  result := False;
  A := p1;
  B := p2;
  if (A <> Point) and (B <> Point) and
    ((A.x <> B.x) or (A.y <> B.y)) then
          // Horizontal
    if Abs(A.x - B.x) > Abs(A.y - B.y) then
    begin
      px := Point.x;
      py := Point.y;
      if ((px >= A.x) and (px <= B.x)) or
        ((px >= B.x) and (px <= A.x)) then
      begin
        YCross := (((px - A.x) * B.y) +
          ((B.x - px) * A.y)) / (B.x - A.x);
        if Abs(py - YCross) < 1 then
          result := True;
      end;
    end
          // Vertical
    else if (A.x <> B.x) or (A.y <> B.y) then
    begin
      px := Point.x;
      py := Point.y;
      if ((py >= A.y) and (py <= B.y)) or
        ((py >= B.y) and (py <= A.y)) then
      begin
        XCross := (((py - A.y) * B.x) +
          ((B.y - py) * A.x)) / (B.y - A.y);
        if Abs(px - XCross) < 1 then
          result := True;
      end;
    end
end;

end.
