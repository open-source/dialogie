unit i3dUtil;

interface

uses
  i3dGeometry,
  Vcl.ComCtrls;

type T3DUtil = class

  public
    class var statusBar : TStatusBar;

    class function Angle(P1, P2: LTPoint): Double;
    class function AngleH(P1, P2: LTPoint): Extended;
    class procedure setStatus(s : string ; panelNum : integer);
    class procedure statusRepaint();
end;

var
  ActiveThread: boolean;
  CurrentAction: integer; // Contain the index of the used tool
  ModeUndo: boolean;

  cbRoomSurface : boolean;
  bRoomVolume : boolean;
  bMagneticGrid : boolean;



implementation

uses
  System.Math;


class function T3DUtil.Angle(P1, P2: LTPoint): Double;
Var
  X, Y, Angle1, SegLength: Double;
Begin
  result := 0;
  if P1 <> P2 then
  Begin
    X := P2.X - P1.X;
    Y := P2.Y - P1.Y;
    SegLength := Sqrt(sqr(X) + sqr(Y));
    X := X / SegLength;
    Y := Y / SegLength;
    Angle1 := ArcCos(X);
    if Y < 0 then
      result := -Angle1
    else
      result := Angle1;
  End
End;

class function T3DUtil.AngleH(P1, P2: LTPoint): Extended;
Var
  X, Y, Angle1, SegLength: Extended;
Begin
  result := 0;
  if P1 <> P2 then
  Begin
    X := P2.xx - P1.xx;
    Y := P2.yy - P1.yy;
    SegLength := Sqrt(sqr(X) + sqr(Y));
    X := X / SegLength;
    Y := Y / SegLength;
    Angle1 := ArcCos(X);
    if Y < 0 then
      result := -Angle1
    else
      result := Angle1;
  End
End;



class procedure T3DUtil.setStatus(s: string; panelNum: integer);
begin
  if statusBar = nil then
    exit;
  if not(panelNum < statusBar.Panels.Count) then
    exit;
  statusBar.Panels[panelNum].Text := s;

end;

class procedure T3DUtil.statusRepaint;
begin
  if statusBar = nil then
    exit;
  statusBar.Repaint;
end;

initialization
  ActiveThread := false;
  CurrentAction := 1;
  ModeUndo := False;
  T3DUtil.statusBar := nil;
end.
