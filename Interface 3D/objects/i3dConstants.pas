unit i3dConstants;

interface

Uses
  Classes,
  Windows,
  SysUtils,
  Math,
  Graphics,
  Dialogs,
  forms,
  Controls;

Const

  FileIni = 'app.ini';
  FileMessages = 'messages.txt';
  FichierStation = 'stations.txt';
  // This file contains all the labels of the visualObjects in all languages
  FileObjects = 'objects.txt';

  // This file contains a list of bitmap that represent the different states
  // of the mouse. It appear in the online help
  FileMouse = 'mouselist.bmp';


  // NUMERICAL

  // Contain the number of languages for translation
  NumberOfLanguage = 7;

  // Contain the name of the language for translation
  NamesOfLanguage: Array [1 .. NumberOfLanguage] of String = ('English', 'French', 'Spanish', 'Portugez', 'German', 'Greek', 'Number');

  // Number of pixel of tolerance for a click on a point
  DefaultClickSensibility = 4;

  // Step of the grid
  gridStep = 20;

  // Eraser cursor identifier
  CrEraserCursor = 1;

  // Maximum Number of undo
  MaxUndo = 99;

  // Internal development constant
  DebugMode: Boolean = False;

  MaxOUvertures = 50;

  // TOOLS

Function GetAngle(Dx, Dy: Double): Double;
Function GetAngleH(Dx, Dy: Extended): Extended;


Type
  TpointH = Record
    xx, yy: Extended;
  end;


Var
  // This is a table that contains the name of the different zones
  ZoneOrientation: Array [-2 .. 12] of String;
  ZoneShortOrientation: Array [-2 .. 12] of String;
  ZoneValueOrientation: Array [-2 .. 12] of Double = (0, 0, 0, 0, 0, 180, 135, 225, 90, 270, 45, 315, 0, 0, 0);
  SymboleDecimal: char;
  Langue: Integer = 2;

  i3dDataPath: string;
  libDataPath : string;

implementation

Uses
  i3dTranslation,
  IzApp,
  UtilitairesDialogie;

// *************************************************
// Return the angle of the vector with 0�
// *************************************************

Function GetAngle(Dx, Dy: Double): Double;
Var
  Angle1, SegLength: Double;
Begin
  // Normalisation
  SegLength := Sqrt(sqr(Dx) + sqr(Dy));
  Dx := Dx / SegLength;
  Dy := Dy / SegLength;

  // Calculation of the angle
  Angle1 := ArcCos(Dx);
  if Dy < 0 then
    GetAngle := -Angle1
  else
    GetAngle := Angle1;
End;

// *************************************************
// Return the angle of the vector with 0�
// *************************************************

Function GetAngleH(Dx, Dy: Extended): Extended;
Var
  Angle1, SegLength: Extended;
Begin
  // Normalisation
  SegLength := Sqrt(sqr(Dx) + sqr(Dy));
  Dx := Dx / SegLength;
  Dy := Dy / SegLength;

  // Calculation of the angle
  Angle1 := ArcCos(Dx);
  if Dy < 0 then
    GetAngleH := -Angle1
  else
    GetAngleH := Angle1;
End;


initialization

{$IFDEF DEBUG}
  i3dDataPath := TApp.path + 'data\';
{$ELSE}
  i3dDataPath := dialogieLibPath + (*TApp.commonDataPath + *)TApp.name + '\';
{$ENDIF}

libDataPath := dialogieLibPath;

end.
