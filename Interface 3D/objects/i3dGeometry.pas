unit i3dGeometry;

interface

{$I delphiX\DelphiXcfg.inc}

Uses
  Windows,
  i3dConstants,
  Generics.Collections,
  compositions,
  Menuiseries,
  Classes,
{$IFDEF StandardDX}
  Direct3D, DirectDraw,
{$ELSE}
  DirectX,
{$ENDIF}
IzObjectPersist, Xml.XMLIntf;

Type

  TP3D = record
    x, y, z: double;
  end;

  T3DParoi = class
    p: array of TD3DVertex;
    NbPoints: integer;
    NTexture: integer;
    NTexture2: integer;
    Alpha: boolean;
    Liste: boolean;
    double: boolean;
//    Zone: integer;
    Constructor Create;
    procedure Assign(Pt: Pointer);
  end;

  TPorte = class(TObjectPersist)
  protected
    procedure innerLoadXml(node : IXMLNode); override;
    procedure innerSaveXml(node : IXMLNode); override;
  public
    CompoPorte: TMenuiseries;
    Center: double;
    Hauteur, Largeur: double;
    Constructor Create;override;
  End;

  TFenetre = class(TObjectPersist)
  protected
    procedure innerLoadXml(node : IXMLNode); override;
    procedure innerSaveXml(node : IXMLNode); override;
  public
    CompoFen: TMenuiseries;
    Center: double;
    Hauteur, Largeur: double;
    PositionAbsolue: double;
    Allege: double;
    Retrait: double;
    Constructor Create;override;
  End;

  TDPoint = Record
    x, y: double;
  End;

  LTPoint = class(TObjectPersist)
  protected
    procedure innerLoadXml(node : IXMLNode); override;
    procedure innerSaveXml(node : IXMLNode); override;
  public
    x, y: integer; // coordinate of the point
    xx, yy: Extended; // High Precision coordinate
    Degree: integer; // Number of segments connected
    Perimeter: boolean; // True if the point is on the perimeter
    Group: integer; // Number of the group (if several rooms
    // are connected together they form a group)
//    Pont: String;
//    Psi: double;
    Indeplacable: boolean;
    Constructor Create(p: Tpoint);
    Destructor Free;
    Function Rotated(Origin: LTPoint; Angle: double): Tpoint;
    Function RotatedH(Origin: LTPoint; Angle: Extended): TpointH;
    Function Projected(Origin: LTPoint; Coeff: double): Tpoint;
    Procedure SaveToList(List: TStringList);
    Procedure LoadFromList(List: TStringList; Var Index: integer);
  End;

  LTSegment = Class
    A, B: LTPoint; // The 2 points that delimite the segment
    Perimeter: boolean; // True if the segment is on the perimeter
    Adjacent: boolean; // True if the segment is adjacent to buffer
    IsVirtual: boolean; // True if the segment has been made for zone
    // and don't have to appear on the sketch

    PercentageGlazing: integer; // Percentage of glazing on the wall
    NumberGlazing: integer; // 0=Opaque / 1 Glazing / 2 glazings
    Shadings: integer; // 0=removable / 1=Fixed
    GlazingColor: integer; // 0 : Tinted / 1:Clear
    Orientation: double; // Orientation of the wall (0� is North)
    Position: integer; // Position for sorting
    Composition: TComposition;

    ListePortes : TList<TPorte>;
    ListeFenetres: TList<TFenetre>;
    identificateur, Identificateur2: integer;
    Casquette, Distance: double;
//    NPontHaut, NPontBas: String;
//    PsiHaut, PsiBas: double;
//    Pont: double;
    Nom, NomB: String;
    Indeplacable: boolean;
    Traite: boolean;
    Constructor Create(Point1, Point2: LTPoint);
    Destructor Free;
    Function Size(Scale: double): double;
  End;

  TRoomType = (roomBuffer = 0 , roomAdjacent = 1, roomRoof = 2,  roomObstruction = 12,  roomNormal = -1,  roomExternal = -2,   roomLocal = -3);


  TRoom = Class
    Points: TList;
    HautPoint: TList<double>;
    RoomType: TRoomType;
    //RoomType: integer; // 0-Buffer 1-Adjacent 2-Roof 12-Obstruction -1 normal -2 External -3 local
    Name: String;
//    ZoneNumber: integer;
    Area: double;
    Volume: double;
    Value1: double; // Height1 for Roof / Height for Shading
    // Ventilation type for Buffer
    Value2: double; // Height2 for Roof / BF for Buffer
    Value3: double; // Width for Roof / Shading for Buffer
    Value4: double; // Roof Slope for Roof / Glazing for Buffer
    Value5: double; // Roof Orientation for Roof / Sky Angle for Buffer
    Value6: double; // Type of Roof
    Value7: double; // Window Orientation for Roof
    Value8: double; // Percentage of glazing for Roof
    ID: integer;
    ListePLanchInt: TStringList;
    Normale: TD3DVector;
    CompoFloor, CompoCeiling: TComposition;
    HauteurLocale: double;
    ListeShed: TList;
    Sur, TTau: String;
    Tau: double;
    Constructor Create;
    Destructor Free;
    Function PointOnSegment(Point: LTPoint): boolean;
    Function getSegment(P1, P2: LTPoint; Segments: TList): LTSegment;
  End;

  TPlancher = Class
    PieceBasse, PieceHaute: integer;
    Surface: double;
    Inclinaison, Orientation: double;
    PB, PH: TRoom;
    Correction: double;
    IdB, IdH: integer;
    niveau : integer;
    Constructor Create;
  End;

implementation

uses
  System.SysUtils, XmlUtil, BiblioMenuiseries;



// ********************************************
// CONSTRUCTORS
// ********************************************

Constructor TPlancher.Create;
Begin
  inherited Create;
  PieceBasse := 0;
  PieceHaute := 0;
  Surface := 0;
  Inclinaison := 0;
  Orientation := 0;
  Correction := 1;
  IdB := 0;
  IdH := 0;
  niveau := -1;
End;

Constructor T3DParoi.Create;
Begin
  inherited Create;
  Liste := False;
  double := False;
//  Zone := -1;
end;

procedure T3DParoi.Assign(Pt: Pointer);
var
  i: integer;
begin
  T3DParoi(Pt).NbPoints := NbPoints;
  T3DParoi(Pt).NTexture := NTexture;
  T3DParoi(Pt).Alpha := Alpha;
  For i := 0 to NbPoints - 1 do
    T3DParoi(Pt).p[i] := p[i];
end;

Constructor TPorte.Create;
Begin
  inherited Create();
  innerTagName := 'door';
  CompoPorte := Nil;
  Center := 0;
  Hauteur := 0;
  Largeur := 0;
End;

procedure TPorte.innerLoadXml(node : IXMLNode);
begin
  CompoPorte := bibMenuiseries.Get(TXmlUtil.getTagString(node, 'composition'));
  Center := TXmlUtil.getTagDouble(node, 'center');
  Hauteur := TXmlUtil.getTagDouble(node, 'hauteur');
  Largeur := TXmlUtil.getTagDouble(node, 'largeur');
end;

procedure TPorte.innerSaveXml(node : IXMLNode);
begin
  if CompoPorte <> nil then
    TXmlUtil.setTag(node, 'composition',CompoPorte.nom)
  else
    TXmlUtil.setTag(node, 'composition','');
  TXmlUtil.setTag(node, 'center',Center);
  TXmlUtil.setTag(node, 'hauteur',Hauteur);
  TXmlUtil.setTag(node, 'largeur',Largeur);
end;



Constructor TFenetre.Create;
Begin
  inherited Create();
  innerTagName := 'window';
  CompoFen := Nil;
  Center := 0;
  Hauteur := 0;
  Largeur := 0;
  Allege := 1;
  Retrait := 0;
End;

procedure TFenetre.innerLoadXml(node : IXMLNode);
begin
  CompoFen := bibMenuiseries.Get(TXmlUtil.getTagString(node, 'composition'));

  Center := TXmlUtil.getTagDouble(node, 'center');
  Hauteur := TXmlUtil.getTagDouble(node, 'hauteur');
  Largeur := TXmlUtil.getTagDouble(node, 'largeur');
  Allege := TXmlUtil.getTagDouble(node, 'allege');
  retrait := TXmlUtil.getTagDouble(node, 'retrait');
end;

procedure TFenetre.innerSaveXml(node : IXMLNode);
begin
  if CompoFen <> nil then
    TXmlUtil.setTag(node, 'composition',CompoFen.nom)
  else
    TXmlUtil.setTag(node, 'composition','');
  TXmlUtil.setTag(node, 'center',Center);
  TXmlUtil.setTag(node, 'hauteur',Hauteur);
  TXmlUtil.setTag(node, 'largeur',Largeur);
  TXmlUtil.setTag(node, 'allege',Allege);
  TXmlUtil.setTag(node, 'retrait',retrait);
end;


Constructor TRoom.Create;
Begin
  inherited Create;
  Points := TList.Create;
  HautPoint := TList<double>.Create;
  CompoCeiling := nil;
  CompoFloor := nil;
  RoomType := roomNormal;
  Name := 'Pi�ce';
//  ZoneNumber := 1;
  Value1 := 250;
  Value2 := 250;
  Value3 := 1000;
  Value4 := 0;
  Value5 := 0;
  Value6 := 0;
  Value7 := 0;
  Value8 := 0;
  ListePLanchInt := TStringList.Create;
  Volume := 0;
  HauteurLocale := -1;
  ListeShed := TList.Create;
  Sur := '';
  TTau := '';
  Tau := 0;
End;

Destructor TRoom.Free;
Begin
  ListePLanchInt.Free;
  HautPoint.Clear;
  HautPoint.Free;
  Points.Free;
  inherited Destroy
End;

Constructor LTPoint.Create(p: Tpoint);
begin
  inherited Create;
  innerTagName := 'point';
  x := p.x;
  y := p.y;
  xx := p.x;
  yy := p.y;
  Perimeter := False;
  Degree := 0;
  Group := -1;
//  Pont := '';
//  Psi := -1;
  Indeplacable := False;
end;

Constructor LTSegment.Create(Point1, Point2: LTPoint);
begin
  inherited Create;
  A := Point1;
  B := Point2;
  Perimeter := False;
  PercentageGlazing := 0;
  NumberGlazing := 4;
  Orientation := 0;
  Composition := Nil;
  ListePortes := TList<TPorte>.Create;
  ListeFenetres := TList<TFenetre>.Create;
  Casquette := 0;
  Distance := 0;
//  NPontBas := '';
//  NPontHaut := '';
//  PsiBas := 0.0;
//  PsiHaut := 0.0;
  Indeplacable := False;
end;

// ********************************
// DESTRUCTORS
// ********************************

Destructor LTPoint.Free;
Begin
  inherited Destroy
End;

Destructor LTSegment.Free;
Var
  porte : TPorte;
  fenetre : TFenetre;
Begin
  For porte in ListePortes do
    porte.Free;
  For fenetre in ListeFenetres do
    fenetre.Free;
  ListePortes.Free;
  ListeFenetres.Free;
  inherited Destroy
End;

// *******************************************************
// The function return the rotated position of the point
// by the given angle
// *******************************************************

Function LTPoint.Rotated(Origin: LTPoint; Angle: double): Tpoint;
Var
  DX, DY, OldAngle, SegLength, Newangle: double;
Begin
  DX := x - Origin.x;
  DY := y - Origin.y;
  if (DX = 0) and (DY = 0) then
  Begin
    Rotated.x := x;
    Rotated.y := y
  End
  else
  Begin
    SegLength := Sqrt(Sqr(DX) + Sqr(DY));
    OldAngle := GetAngle(DX, DY);
    Newangle := OldAngle + Angle;
    Rotated.x := Round(Cos(Newangle) * SegLength) + Origin.x;
    Rotated.y := Round(Sin(Newangle) * SegLength) + Origin.y
  End
End;

// *******************************************************
// The function return the rotated position of the point
// by the given angle with a high precision
// *******************************************************

Function LTPoint.RotatedH(Origin: LTPoint; Angle: Extended): TpointH;
Var
  DX, DY, OldAngle, SegLength, Newangle: Extended;
Begin
  DX := xx - Origin.xx;
  DY := yy - Origin.yy;
  if (DX = 0) and (DY = 0) then
  Begin
    RotatedH.xx := xx;
    RotatedH.yy := yy
  End
  else
  Begin
    SegLength := Sqrt(Sqr(DX) + Sqr(DY));
    OldAngle := GetAngleH(DX, DY);
    Newangle := OldAngle + Angle;
    RotatedH.xx := Cos(Newangle) * SegLength + Origin.xx;
    RotatedH.yy := Sin(Newangle) * SegLength + Origin.yy
  End
End;

// *******************************************************
// The function return the projected position of the point
// by the given origin and coefficient
// *******************************************************

Function LTPoint.Projected(Origin: LTPoint; Coeff: double): Tpoint;
Var
  DX, DY: double;
Begin
  DX := (x - Origin.x) * Coeff;
  DY := (y - Origin.y) * Coeff;
  Projected.x := Round(DX) + Origin.x;
  Projected.y := Round(DY) + Origin.y
End;

// *****************************************************************
// This procedure add the characteristics of the point to a list
// *****************************************************************

Procedure LTPoint.SaveToList(List: TStringList);
Begin
  List.add('pont');
  List.add('');
  List.add('0');
  List.add(IntToStr(x));
  List.add(IntToStr(y)); // +'/'+Chaine);
End;

// *****************************************************************
// This procedure set the characteristics of the point from
// the coordinate at index and index+1 position into the list
// Index is incremented after the operation
// *****************************************************************

Procedure LTPoint.LoadFromList(List: TStringList; Var Index: integer);
Var
  Chaine, c2: String;
Begin
  if List[index] = 'pont' then
  Begin
    inc(index);
    inc(index);
    inc(index);
  End;
  x := StrToInt(List[index]);
  inc(Index);
  Chaine := List[index];
  inc(Index);
  if (Pos('/', Chaine) = 0) then
  begin
    y := StrToInt(Chaine);
  end
  else
  begin
    c2 := Copy(Chaine, 1, Pos('/', Chaine) - 1);
    Delete(Chaine, 1, Pos('/', Chaine));
    y := StrToInt(c2);
  end;
End;

procedure LTPoint.innerLoadXml(node : IXMLNode);
begin
  x := TXmlUtil.getAttInt(node, 'x');
  y := TXmlUtil.getAttInt(node, 'y');
end;

procedure LTPoint.innerSaveXml(node : IXMLNode);
begin
  TXmlUtil.setAtt(node, 'x', x);
  TXmlUtil.setAtt(node, 'y', y);
end;




// *******************************************************
// The function return the length of the segment
// taking into account the given scale
// *******************************************************

Function LTSegment.Size(Scale: double): double;
Var
  Ax, Ay, By, Bx: double;
Begin
  Ax := A.x;
  Ay := A.y;
  By := B.y;
  Bx := B.x;
  Size := Round(Sqrt(Sqr(Ax - Bx) + Sqr(Ay - By)) / Scale * 100) / 100
End;

Function TRoom.getSegment(P1, P2: LTPoint; Segments: TList): LTSegment;
Var
  i: integer;
  Seg, TempSeg: LTSegment;
Begin
  TempSeg := nil;
  For i := 0 to Segments.count - 1 do
  Begin
    Seg := LTSegment(Segments[i]);
    if ((Seg.A = P1) and (Seg.B = P2)) or ((Seg.A = P2) and (Seg.B = P1)) then
      TempSeg := Seg
  End;
  if TempSeg = Nil then
  begin
    TempSeg := LTSegment.Create(P1, P2);
    TempSeg.Perimeter := False;
    TempSeg.IsVirtual := True;
    TempSeg.PercentageGlazing := 0;
    TempSeg.NumberGlazing := 2;
  end;
  getSegment := TempSeg;
End;

Function TRoom.PointOnSegment(Point: LTPoint): boolean;
Var
  XCross, YCross: double;
  A, B: LTPoint;
  i: integer;
Begin
  PointOnSegment := False;
  For i := 0 to Points.count - 2 do
  Begin
    A := LTPoint(Points[i]);
    B := LTPoint(Points[i + 1]);
    if (A <> Point) and (B <> Point) and ((A.x <> B.x) or (A.y <> B.y)) then
      // Horizontal
      if Abs(A.x - B.x) > Abs(A.y - B.y) then
      begin
        if ((Point.x >= A.x) and (Point.x <= B.x)) or ((Point.x >= B.x) and (Point.x <= A.x)) then
        begin
          YCross := (((Point.x - A.x) * B.y) + ((B.x - Point.x) * A.y)) div (B.x - A.x);
          if Abs(Point.y - YCross) < 1 then
            PointOnSegment := True;
        end;
      End
      // Vertical
      Else if (A.x <> B.x) or (A.y <> B.y) then
      Begin
        if ((Point.y >= A.y) and (Point.y <= B.y)) or ((Point.y >= B.y) and (Point.y <= A.y)) then
        begin
          XCross := (((Point.y - A.y) * B.x) + ((B.y - Point.y) * A.x)) div (B.y - A.y);
          if Abs(Point.x - XCross) < 1 then
            PointOnSegment := True;
        End;
      End
  End;
End;

end.
