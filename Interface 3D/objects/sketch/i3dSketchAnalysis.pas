unit i3dSketchAnalysis;

interface

Uses
  i3dGeometry,
  Classes,
  Math,
  dialogs,
  windows;


Type
  TCutThread = Class
    Points, segments, Perimeters, Rooms, Zones, VirtualPoints, VirtualSegments: TList;
    PreviousRooms: TList;
    Error: Integer;
    Terminated: Boolean;
    WaitingZones: TList;
    LevHeight: Double;
  Protected
    Procedure PerimeterCalculation;
    Procedure RoomsCalculation(Perimeter, PointsCopy: TList);
    Function Area(PointList: TList): Double;
    Procedure Debug;
    Procedure CheckSegmentsCross;
    Procedure CheckGlobalLinks;
    Procedure RoomsComparison;
    Procedure CheckInside;
    Procedure Warning;
    Procedure CheckShadings;
    function PointInOut(P: LTPoint; Points: TList): Boolean;
    Function Atan2(Dy, Dx: Extended): Extended;
    Function CheckIntersection(A, B, AA, BB: LTPoint; OnLine: Boolean): LTPoint;
    function PointIn(P: LTPoint; Points: TList): Integer;

    Procedure CheckExternalZones;
  Public
    Constructor Create(Po, Se, Pe, Ro, vp, vs: TList; LevelHeight: Double);
    Destructor Free;
    Procedure Execute;
  end;

implementation

Uses
  i3dConstants,
  i3dSketchLevel,
  SysUtils,
  i3dTranslation,
  i3dSketchObject,
  i3dUtil;

function SortOrientation(item1, item2: Pointer): Integer;
var
  diff: Double;
begin
  diff := Abs(LTSegment(item2).orientation) - Abs(LTSegment(item1).orientation);
  if diff < 0 then
    SortOrientation := -1
  else if diff = 0 then
  Begin
    SortOrientation := LTSegment(item1).position - LTSegment(item2).position;
  End
  else
    SortOrientation := 1;
end;

Constructor TCutThread.Create(Po, Se, Pe, Ro, vp, vs: TList; LevelHeight: Double);
Var
  i: Integer;
Begin
  inherited Create;
  Terminated := False;
  LevHeight := LevelHeight;
  Points := Po;
  segments := Se;
  Perimeters := Pe;
  For i := 0 to Perimeters.count - 1 do
    TList(Perimeters[i]).Free;
  Perimeters.Clear;
  Rooms := Ro;
  VirtualPoints := vp;
  VirtualSegments := vs;
  PreviousRooms := TList.Create;
  For i := 0 to Rooms.count - 1 do
    PreviousRooms.add(Rooms[i]);
  Rooms.Clear;
end;

Destructor TCutThread.Free;
Var
  i: Integer;
Begin
  if Error = 0 then
    For i := PreviousRooms.count - 1 downto 0 do
      TRoom(PreviousRooms[i]).Free
    else
    Begin
      For i := Rooms.count - 1 downto 0 do
        TRoom(Rooms[i]).Free;
      Rooms.Clear;
      For i := PreviousRooms.count - 1 downto 0 do
        Rooms.add(PreviousRooms[i]);
    End;
  Case Error Of
    0:
      ;
  End;
  PreviousRooms.Free;
  inherited Destroy;
End;

procedure TCutThread.Execute;
var
  i, j: Integer;
  Seg: LTSegment;
  Room: TRoom;
begin
  For j := 0 to Points.count - 1 do
  Begin
    LTPoint(Points[j]).xx := Round(LTPoint(Points[j]).X);
    LTPoint(Points[j]).yy := Round(LTPoint(Points[j]).Y);
  End;
  // primary verifications
  Repeat
    Error := 0;

    // The number of points must be at least 3
    if Points.count < 3 then
      Error := 1;

    // Initialization of the points
    For i := 0 to Points.count - 1 do
    Begin
      LTPoint(Points[i]).degree := 0;
      LTPoint(Points[i]).Perimeter := False;
    end;
    if Terminated then
      Exit;
    // No point with less that 2 segments are allowed
    // in the same time, initialization of the segments
    For i := 0 to segments.count - 1 do
    Begin
      Seg := LTSegment(segments[i]);
      Seg.Perimeter := False;
      inc(Seg.A.degree);
      inc(Seg.B.degree);
    End;
    if Terminated then
      Exit;
    For i := 0 to Points.count - 1 do
      if (LTPoint(Points[i]).degree < 2) then
        Error := 2;
    if Terminated then
      Exit;

    // check if there is somme crossed segments
    If Error = 0 then
      CheckSegmentsCross;

    // check if the sketch is connex and define groups
    if Error = 0 then
      CheckGlobalLinks;

    if Terminated then
      Exit;

    // Calculate the perimeters of the sketch
    if Error = 0 then
      PerimeterCalculation;
    if Terminated then
      Exit;


    // Check if the a zone is inside another one
    If Error = 0 then
      CheckInside;

    If (Error = -1) then
    Begin
      For i := 0 to Perimeters.count - 1 do
        TList(Perimeters[i]).Free;
      Perimeters.Clear;

      For i := 0 to Rooms.count - 1 do
        TRoom(Rooms[i]).Free;
      Rooms.Clear;

      { For i := Zones.count-1 downto 0 do
        TZone(Zones[i]).Free;
        Zones.Clear; }
    End;

  Until Error <> -1;

  // Check previous rooms with the new rooms and keep the
  // characteristics of previous rooms corresponding to new ones

  If (Error = 0) then
    RoomsComparison;
  if Error = 0 then
    CheckShadings;
  if Error = 0 then
    CheckExternalZones;
  if Terminated then
    Exit;
  // if Error = 0 then
  // ZonesCalculation;
  if Terminated then
    Exit;

  For i := 0 to segments.count - 1 do
  Begin
    Seg := LTSegment(segments[i]);
    if Seg.orientation > 359 then
      Seg.orientation := Seg.orientation - 360
    else if Seg.orientation < 0 then
      Seg.orientation := Seg.orientation + 360;
  end;
  if DebugMode then
    Debug;
  if Error > 1 then
    Warning
  else
    T3DUtil.setStatus('', 1);
  For i := Rooms.count - 1 downto 0 do
  begin
    Room := TRoom(Rooms[i]);
    For j := 0 to Room.Points.count - 1 do
    Begin
      LTPoint(Room.Points[j]).X := Round(LTPoint(Room.Points[j]).xx);
      LTPoint(Room.Points[j]).Y := Round(LTPoint(Room.Points[j]).yy);
    End;
    Room.Area := Area(Room.Points);
  end;
End;


Procedure TCutThread.PerimeterCalculation;
Var
  Perimeter, PointsCopy: TList;
  j, Current: Integer;
  MaxAngle, CAngle, Alpha: Double;
  Seg, MaxSeg: LTSegment;
  NextPoint: LTPoint;
Label Fin;
Begin
  MaxSeg := Nil;
  PointsCopy := TList.Create;
  For j := 0 to segments.count - 1 do
  Begin
    PointsCopy.add(LTSegment(segments[j]).A);
    PointsCopy.add(LTSegment(segments[j]).B);
  end;

  PointsCopy.Sort(SortPoint);

  Repeat
    Perimeter := TList.Create;
    NextPoint := Nil;
    Perimeter.add(PointsCopy[0]);
    PointsCopy.delete(0);
    Current := 0;
    MaxAngle := -10000;
    For j := 0 to segments.count - 1 do
    Begin
      Seg := LTSegment(segments[j]);
      if (Seg.A = Perimeter[Current]) then
      Begin
        CAngle := T3DUtil.Angle(Seg.A, Seg.B);
        Seg.orientation := CAngle / pi * 180 + 180;
        if Seg.orientation > 180 then
          Seg.orientation := Seg.orientation - 360;
        if (CAngle > MaxAngle) then
        Begin
          MaxAngle := CAngle;
          NextPoint := Seg.B;
          MaxSeg := Seg;
        End
      End;
      if (Seg.B = Perimeter[Current]) then
      Begin
        CAngle := T3DUtil.Angle(Seg.B, Seg.A);
        Seg.orientation := CAngle / pi * 180 + 180;
        if Seg.orientation > 180 then
          Seg.orientation := Seg.orientation - 360;
        if (CAngle > MaxAngle) then
        Begin
          MaxAngle := CAngle;
          NextPoint := Seg.A;
          MaxSeg := Seg
        End
      End;
      if Terminated then
        Exit;
    End;
    if NextPoint <> Nil then
    Begin
      Perimeter.add(NextPoint);
      PointsCopy.Remove(NextPoint);
      NextPoint.Perimeter := True;
      MaxSeg.Perimeter := True
    End
    else
    Begin
      Error := 3;
      Goto Fin;
    End;
    inc(Current);
    Repeat
      NextPoint := nil;
      MaxAngle := -10000;
      Alpha := T3DUtil.Angle(Perimeter[Current], Perimeter[Current - 1]);

      For j := 0 to segments.count - 1 do
      Begin
        Seg := LTSegment(segments[j]);
        if (Seg.A = Perimeter[Current]) and (Perimeter[Current - 1] <> Seg.B) then
        Begin
          CAngle := T3DUtil.Angle(Seg.A, Seg.B) - Alpha;
          if CAngle < 0 then
            CAngle := CAngle + (2 * pi);
          Seg.orientation := T3DUtil.Angle(Seg.A, Seg.B) / pi * 180 + 180;
          if Seg.orientation > 180 then
            Seg.orientation := Seg.orientation - 360;
          if (CAngle > MaxAngle) then
          Begin
            MaxAngle := CAngle;
            NextPoint := Seg.B;
            MaxSeg := Seg
          End
        End;
        if (Seg.B = Perimeter[Current]) and (Perimeter[Current - 1] <> Seg.A) then
        Begin
          CAngle := T3DUtil.Angle(Seg.B, Seg.A) - Alpha;
          if CAngle < 0 then
            CAngle := CAngle + (2 * pi);
          Seg.orientation := T3DUtil.Angle(Seg.B, Seg.A) / pi * 180 + 180;
          if Seg.orientation > 180 then
            Seg.orientation := Seg.orientation - 360;
          if (CAngle > MaxAngle) then
          Begin
            MaxAngle := CAngle;
            NextPoint := Seg.A;
            MaxSeg := Seg
          End;
        End;
        if Terminated then
          Exit;
      End;
      if NextPoint <> nil then
      Begin
        Perimeter.add(NextPoint);
        PointsCopy.Remove(NextPoint);
        NextPoint.Perimeter := True;
        MaxSeg.Perimeter := True
      End;
      inc(Current);
    Until (NextPoint = nil) or (NextPoint = Perimeter[0]);
    if NextPoint = Nil then
      Error := 4;
    RoomsCalculation(Perimeter, PointsCopy);
    Perimeters.add(Perimeter);
  until PointsCopy.count = 0;
  PointsCopy.Free;
Fin :
end;

Procedure TCutThread.RoomsCalculation(Perimeter, PointsCopy: TList);
Var
  i, j, Current: Integer;
  MaxAngle, CAngle, Alpha: Double;
  Seg, MaxSeg, FirstSeg: LTSegment;
  NextPoint: LTPoint;
  DoubleSegment: TList;
  Ok: Boolean;
  Room: TRoom;
  PointsAvailable: TList;
  CurrentGroup: Integer;
Begin
  MaxSeg := Nil;
  CurrentGroup := LTPoint(Perimeter[0]).Group;
  PointsAvailable := TList.Create;

  NextPoint := Nil;
  DoubleSegment := TList.Create;
  if Terminated then
    Exit;
  For i := 0 to segments.count - 1 do
    DoubleSegment.add(segments[i]);
  For i := 0 to segments.count - 1 do
    DoubleSegment.add(segments[i]);
  if Terminated then
    Exit;
  For i := 1 to Perimeter.count - 1 do
  Begin
    j := 0;
    Ok := True;
    While (j <= DoubleSegment.count - 1) and Ok do
    Begin
      if ((LTSegment(DoubleSegment[j]).A = Perimeter[i - 1]) and (LTSegment(DoubleSegment[j]).B = Perimeter[i])) or
        ((LTSegment(DoubleSegment[j]).B = Perimeter[i - 1]) and (LTSegment(DoubleSegment[j]).A = Perimeter[i])) then
      begin
        DoubleSegment.delete(j);
        Ok := False
      End;
      inc(j);
      if Terminated then
        Exit;
    End;
  End;
  For j := DoubleSegment.count - 1 downto 0 do
    if LTSegment(DoubleSegment[j]).A.Group <> CurrentGroup then
      DoubleSegment.delete(j);

  if Terminated then
    Exit;
  Points.Sort(SortPoint);
  DoubleSegment.Sort(SortSegment);
  Ok := True;
  While (DoubleSegment.count <> 0) and Ok do
  Begin
    Room := TRoom.Create;
    Rooms.add(Room);
    MaxAngle := -10000;
    PointsAvailable.Clear;
    For i := 0 to DoubleSegment.count - 1 do
    Begin
      PointsAvailable.add(LTSegment(DoubleSegment[i]).A);
      PointsAvailable.add(LTSegment(DoubleSegment[i]).B);
    End;
    PointsAvailable.Sort(SortPoint);

    For i := 0 to DoubleSegment.count - 1 do
    Begin
      Seg := LTSegment(DoubleSegment[i]);
      if (Seg.A = PointsAvailable[0]) then
      Begin
        CAngle := T3DUtil.Angle(Seg.A, Seg.B);
        if (CAngle > MaxAngle) then
        Begin
          MaxAngle := CAngle;
          NextPoint := Seg.B;
          MaxSeg := Seg;
        End
      End;
      if (Seg.B = PointsAvailable[0]) then
      Begin
        CAngle := T3DUtil.Angle(Seg.B, Seg.A);
        if (CAngle > MaxAngle) then
        Begin
          MaxAngle := CAngle;
          NextPoint := Seg.A;
          MaxSeg := Seg;
        End;
      End;
      if Terminated then
        Exit;
    End;
    Seg := MaxSeg;
    FirstSeg := MaxSeg;
    if NextPoint = Seg.B then
    begin
      Room.Points.add(Seg.A);
      PointsCopy.Remove(Seg.A);
      Room.Points.add(Seg.B);
      PointsCopy.Remove(Seg.B);
    end
    else
    begin
      Room.Points.add(Seg.B);
      PointsCopy.Remove(Seg.A);
      Room.Points.add(Seg.A);
      PointsCopy.Remove(Seg.B);
    end;

    Current := 1;
    Repeat
      NextPoint := nil;
      MaxAngle := 10000;
      Alpha := T3DUtil.Angle(Room.Points[Current], Room.Points[Current - 1]);

      For j := 0 to DoubleSegment.count - 1 do
      Begin
        Seg := LTSegment(DoubleSegment[j]);
        if (Seg.A = Room.Points[Current]) and (Room.Points[Current - 1] <> Seg.B) then
        Begin
          CAngle := T3DUtil.Angle(Seg.A, Seg.B) - Alpha;
          if CAngle < 0 then
            CAngle := CAngle + (2 * pi);
          if CAngle > (2 * pi) then
            CAngle := CAngle - (2 * pi);
          if (CAngle < MaxAngle) then
          Begin
            MaxAngle := CAngle;
            NextPoint := Seg.B;
            MaxSeg := Seg;
          End
        End;
        if (Seg.B = Room.Points[Current]) and (Room.Points[Current - 1] <> Seg.A) then
        Begin
          CAngle := T3DUtil.Angle(Seg.B, Seg.A) - Alpha;
          if CAngle < 0 then
            CAngle := CAngle + (2 * pi);
          if CAngle > (2 * pi) then
            CAngle := CAngle - (2 * pi);
          if (CAngle < MaxAngle) then
          Begin
            MaxAngle := CAngle;
            NextPoint := Seg.A;
            MaxSeg := Seg;
          End;
        End;
        if Terminated then
          Exit;
      End;
      if (MaxSeg <> FirstSeg) then
      Begin
        if (NextPoint <> nil) then
        begin
          Room.Points.add(NextPoint);
          PointsCopy.Remove(NextPoint);
          DoubleSegment.Remove(MaxSeg);
        end
        else
        Begin
          Error := 5;
          Ok := False;
        end;
      End;
      inc(Current);
    Until (MaxSeg = FirstSeg) or (NextPoint = Nil) or (Error <> 0);
    DoubleSegment.Remove(FirstSeg);
  End;
  DoubleSegment.Free;
  PointsAvailable.Free;
End;

Function TCutThread.Area(PointList: TList): Double;
Var
  AreaTemp, Scale: Double;
  i: Integer;
Begin
  AreaTemp := 0;
  For i := 0 to PointList.count - 2 do
    AreaTemp := AreaTemp + (LTPoint(PointList[i]).xx * LTPoint(PointList[i + 1]).yy) - (LTPoint(PointList[i]).yy * LTPoint(PointList[i + 1]).xx);
  Scale := project.sketchObject.Scale;
  Area := Abs(AreaTemp) / 2 / sqr(Scale);
end;

procedure TCutThread.Debug;
Begin
End;


Procedure TCutThread.CheckSegmentsCross;
Var
  i, j: Integer;
  Seg1, Seg2: LTSegment;
  k1, k2: Double;
  part1, part2, P1, P2, p3, p4, p5, p6, p7, p8: Double;
Begin
  For i := 0 to segments.count - 2 do
    For j := i + 1 to segments.count - 1 do
    Begin
      Seg1 := LTSegment(segments[i]);
      Seg2 := LTSegment(segments[j]);
      p3 := (Seg1.A.X - Seg2.A.X);
      P1 := (Seg1.A.Y - Seg2.A.Y);
      p5 := (Seg1.B.X - Seg1.A.X);
      p6 := (Seg1.B.Y - Seg1.A.Y);
      P2 := (Seg2.B.X - Seg2.A.X);
      p4 := (Seg2.B.Y - Seg2.A.Y);
      p8 := (Seg2.A.X - Seg1.A.X);
      p7 := (Seg2.A.Y - Seg1.A.Y);
      part1 := ((P1 * P2) - (p3 * p4));
      part2 := ((p4 * p5) - (P2 * p6));
      if part2 <> 0 then
        k1 := part1 / part2
      else if part1 = 0 then
        k1 := 0
      else
        k1 := -1;

      part1 := ((p7 * p5) - (p8 * p6));
      part2 := ((p6 * P2) - (p5 * p4));
      if part2 <> 0 then
        k2 := part1 / part2
      else if part1 = 0 then
        k2 := 0
      else
        k2 := -1;

      if (k1 > 0) and (k1 < 1) and (k2 > 0) and (k2 < 1) then
        Error := 6;
      if Terminated then
        Exit;
    End;
End;

Procedure TCutThread.CheckGlobalLinks;
Var
  Ok, Ok2: Boolean;
  i: Integer;
  Seg: LTSegment;
  count: Integer;
Begin
  For i := 0 to Points.count - 1 do
    LTPoint(Points[i]).Group := -1;
  Ok := True;
  count := 0;
  While Ok do
  Begin
    Ok := False;
    For i := 0 to Points.count - 1 do
      if LTPoint(Points[i]).Group = -1 then
      begin
        LTPoint(Points[i]).Group := count;
        inc(count);
        break
      end;
    Ok2 := True;
    While Ok2 do
    begin
      Ok2 := False;
      For i := 0 to segments.count - 1 do
      begin
        Seg := LTSegment(segments[i]);
        if (Seg.A.Group <> -1) and (Seg.B.Group = -1) then
        begin
          Seg.B.Group := Seg.A.Group;
          Ok2 := True;
          Ok := True;
        end;
        if (Seg.A.Group = -1) and (Seg.B.Group <> -1) then
        begin
          Seg.A.Group := Seg.B.Group;
          Ok2 := True;
          Ok := True;
        end;
      end;
      if Terminated then
        Exit;
    end;
  End;
End;


Procedure TCutThread.RoomsComparison;
Var
  i, j, k: Integer;
  Room1, Room2: TRoom;
  Ok: Boolean;
  dbl : double;
Begin
  For i := 0 to Rooms.count - 1 do
    for j := 0 to PreviousRooms.count - 1 do
    Begin
      Room1 := TRoom(Rooms[i]);
      Room2 := TRoom(PreviousRooms[j]);
      if Room1.Points.count = Room2.Points.count then
      Begin
        Ok := True;
        For k := 0 to Room1.Points.count - 1 do
          Ok := Ok and (Room2.Points.IndexOf(Room1.Points[k]) <> -1);
        if Ok then
        Begin
          Room1.RoomType := Room2.RoomType;
          Room1.Area := Room2.Area;
          Room1.Name := Room2.Name;
//          Room1.ZoneNumber := Room2.ZoneNumber;
          Room1.Value1 := Room2.Value1;
          Room1.Value2 := Room2.Value2;
          Room1.Value3 := Room2.Value3;
          Room1.Value4 := Room2.Value4;
          Room1.Value5 := Room2.Value5;
          Room1.Value6 := Room2.Value6;
          Room1.Value7 := Room2.Value7;
          Room1.Value8 := Room2.Value8;
          Room1.CompoFloor := Room2.CompoFloor;
          Room1.CompoCeiling := Room2.CompoCeiling;
          room1.HautPoint.Clear;
          for dbl in room2.HautPoint do
            room1.HautPoint.Add(dbl);
          Room1.Sur := Room2.Sur;
          Room1.Tau := Room2.Tau;
          Room1.TTau := Room2.TTau;
        End;
      End;
    End;
End;

Procedure TCutThread.Warning;
Begin
  if Error > 1 then
    T3DUtil.setStatus(Labels[23 + Error], 1);
End;

Procedure TCutThread.CheckInside;
Var
  i, j, k, l, C1, C2: Integer;
  Room: TRoom;
  P: LTPoint;
  MinLength, CurrentLength: Double;
  kx, ky: Double;
Begin
  C1 := 0;
  C2 := 0;
  if Error = -1 then
    Error := 0;
  For i := 0 to Rooms.count - 1 do
  Begin
    Room := TRoom(Rooms[i]);
    For j := 0 to Points.count - 1 do
    Begin
      P := LTPoint(Points[j]);
      if Room.Points.IndexOf(P) = -1 then
        if PointInOut(P, Room.Points) then
        Begin
          MinLength := 100000000000000;
          For k := 0 to Points.count - 1 do
            If (LTPoint(Points[k]).Group = P.Group) then
              For l := 0 to Room.Points.count - 1 do
              Begin
                Error := -1;
                kx := LTPoint(Points[k]).X;
                ky := LTPoint(Points[k]).Y;
                CurrentLength := Sqrt(sqr(kx - LTPoint(Room.Points[l]).X) + sqr(ky - LTPoint(Room.Points[l]).Y));
                if CurrentLength < MinLength then
                Begin
                  MinLength := CurrentLength;
                  C1 := k;
                  C2 := l
                End;
              End;
          segments.add(LTSegment.Create(Points[C1], Room.Points[C2]));
          Exit
        End
    End
  End
End;

function TCutThread.PointInOut(P: LTPoint; Points: TList): Boolean;
Var
  i, N0: Integer;
  u, v: Double;
  theta, thetai, theta1: Double;
  Sum, Angle, M: Double;
Begin
  N0 := Points.count - 1;

  u := LTPoint(Points[0]).X - P.X;
  v := LTPoint(Points[0]).Y - P.Y;
  theta1 := Atan2(v, u);
  Sum := 0;
  theta := theta1;
  for i := 1 to N0 - 1 do
  Begin
    u := LTPoint(Points[i]).X - P.X;
    v := LTPoint(Points[i]).Y - P.Y;
    thetai := Atan2(v, u);
    Angle := Abs(thetai - theta);
    if Angle > pi then
      Angle := Angle - (pi * 2);
    if theta > thetai then
      Angle := -Angle;
    Sum := Sum + Angle;
    theta := thetai;
  end;
  Angle := Abs(theta1 - theta);
  if Angle > pi then
    Angle := Angle - (pi * 2);
  if theta > theta1 then
    Angle := -Angle;
  Sum := Sum + Angle;
  M := Abs(Sum) / (pi * 2);
  PointInOut := (M > 0.9);
End;

Function TCutThread.Atan2(Dy, Dx: Extended): Extended;
Var
  TempTan: Extended;
Begin
  if Dx <> 0 then
  Begin
    TempTan := Arctan(Abs(Dy / Dx));
    if Dy < 0 then
      if Dx < 0 then
        Atan2 := -pi + TempTan
      else
        Atan2 := -TempTan
      else if Dx < 0 then
        Atan2 := pi - TempTan
      else
        Atan2 := TempTan
  End
  else if Dy > 0 then
    Atan2 := pi / 2
  else
    Atan2 := -pi / 2;
End;


Procedure TCutThread.CheckShadings;
Var
  Room, Room2: TRoom;
  i, j, ii, jj: Integer;
  Changed: Boolean;
Begin
  Changed := True;
  While Changed do
  begin
    Changed := False;
    For i := 0 to Rooms.count - 1 do
    Begin
      Room := TRoom(Rooms[i]);
      if (Room.RoomType = roomObstruction) then
        For ii := 0 to Rooms.count - 1 do
        Begin
          Room2 := TRoom(Rooms[ii]);
          if (Room2 <> Room) then
            For j := 0 to Room.Points.count - 1 do
            Begin
              for jj := 0 to Room2.Points.count - 1 do
                if (Room.Points[j] = Room2.Points[jj]) and (Room.RoomType <> Room2.RoomType) then
                Begin
                  Room2.RoomType := roomObstruction;
                  Room2.Value1 := Room.Value1;
                  Room2.Name := Room.Name;
//                  Room2.ZoneNumber := Room.ZoneNumber;
                  Changed := True;
                End
            End
        End
    End
  end
End;


function TCutThread.PointIn(P: LTPoint; Points: TList): Integer;
Var
  i, N0: Integer;
  u, v: Extended;
  theta, thetai, theta1: Extended;
  Sum, Angle, M: Extended;
  Tol: Extended;
  Ajout: Boolean;
Label Vingt;
Begin
  if Points[0] <> Points[Points.count - 1] then
  begin
    Ajout := True;
    Points.add(Points[0]);
  end
  else
    Ajout := False;

  // Test if on a line
  N0 := Points.count - 1;
  Tol := 0.0000001;
  PointIn := -1;
  u := LTPoint(Points[0]).xx - P.xx;
  v := LTPoint(Points[0]).yy - P.yy;
  if (u = 0) and (v = 0) then
    goto Vingt;
  if N0 < 2 then
  begin
    if Ajout then
      Points.delete(Points.count - 1);
    Exit
  end;
  theta1 := Atan2(v, u);
  Sum := 0;
  theta := theta1;
  for i := 1 to N0 - 1 do
  Begin
    u := LTPoint(Points[i]).xx - P.xx;
    v := LTPoint(Points[i]).yy - P.yy;
    if (Abs(u) < 0.000000000001) and (Abs(v) < 0.000000000001) then
      goto Vingt;
    thetai := Atan2(v, u);
    Angle := Abs(thetai - theta);
    IF (Abs(Angle - pi) < Tol) then
      goto Vingt;
    if Angle > pi then
      Angle := Angle - (pi * 2);
    if theta > thetai then
      Angle := -Angle;
    Sum := Sum + Angle;
    theta := thetai;
  end;
  Angle := Abs(theta1 - theta);
  if (Abs(Angle - pi) < Tol) then
    goto Vingt;
  if Angle > pi then
    Angle := Angle - (pi * 2);
  if theta > theta1 then
    Angle := -Angle;
  Sum := Sum + Angle;
  M := Abs(Sum) / (pi * 2);
  if Abs(M) < Tol then
    Exit;
  PointIn := 1;
  if Ajout then
    Points.delete(Points.count - 1);
  Exit;
Vingt :
  PointIn := 0;
  if Ajout then
    Points.delete(Points.count - 1);
End;


Function TCutThread.CheckIntersection(A, B, AA, BB: LTPoint; OnLine: Boolean): LTPoint;
Var
  P1, P2, p3, p4, p5, p6, p7, p8, part1, part2, part22, k1, k2, Coeff1, Coeff2: Extended;
  TempPoint: LTPoint;
Begin
  CheckIntersection := Nil;
  p3 := (AA.xx - A.xx);
  P1 := (AA.yy - A.yy);
  p5 := (BB.xx - AA.xx);
  p6 := (BB.yy - AA.yy);
  P2 := (B.xx - A.xx);
  p4 := (B.yy - A.yy);
  p8 := (A.xx - AA.xx);
  p7 := (A.yy - AA.yy);

  part1 := ((P1 * P2) - (p3 * p4));
  part2 := ((p4 * p5) - (P2 * p6));
  if Abs(part2) > 0.0000000001 then
    k1 := part1 / part2
  else if part1 = 0 then
    k1 := 0
  else
    k1 := -1;

  part1 := ((p7 * p5) - (p8 * p6));
  part22 := ((p6 * P2) - (p5 * p4));
  if Abs(part22) > 0.0000000001 then
    k2 := part1 / part22
  else if part1 = 0 then
    k2 := 0
  else
    k2 := -1;

  if (k1 >= 0) and (k1 <= 1.000000000001) and (k2 > 0) and (k2 < 1) then
  Begin
    TempPoint := LTPoint.Create(Point(A.X + Round((B.xx - A.xx) * k2), A.Y + Round((B.yy - A.yy) * k2)));
    TempPoint.xx := A.xx + (B.xx - A.xx) * k2;
    TempPoint.yy := A.yy + (B.yy - A.yy) * k2;
    TempPoint.X := Round(TempPoint.xx);
    TempPoint.Y := Round(TempPoint.yy);
    CheckIntersection := TempPoint;
  End
  else if OnLine and (B.xx = AA.xx) and (B.xx = BB.xx) and (((B.yy >= AA.yy) and (B.yy <= BB.yy)) or ((B.yy >= BB.yy) and (B.yy <= AA.yy))) then
  begin
    TempPoint := LTPoint.Create(Point(B.X, B.Y));
    TempPoint.xx := B.xx;
    TempPoint.yy := B.yy;
    TempPoint.X := Round(TempPoint.xx);
    TempPoint.Y := Round(TempPoint.yy);
    CheckIntersection := TempPoint;
  end
  else if OnLine and (Abs(B.yy - AA.yy) < 0.000001) and (Abs(B.yy - BB.yy) < 0.000001) and
    (((B.xx >= AA.xx) and (B.xx <= BB.xx)) or ((B.xx >= BB.xx) and (B.xx <= AA.xx))) then
  begin
    TempPoint := LTPoint.Create(Point(B.X, B.Y));
    TempPoint.xx := B.xx;
    TempPoint.yy := B.yy;
    TempPoint.X := Round(TempPoint.xx);
    TempPoint.Y := Round(TempPoint.yy);
    CheckIntersection := TempPoint;
  end
  else if OnLine and (BB.xx <> AA.xx) and (BB.yy <> AA.yy) then
  begin
    Coeff1 := ((B.xx - AA.xx) / (BB.xx - AA.xx));
    Coeff2 := ((B.yy - AA.yy) / (BB.yy - AA.yy));
    if (Coeff1 >= 0) and (Coeff1 <= 1) and (Coeff2 >= 0) and (Coeff2 <= 1) and (Abs(Coeff1 - Coeff2) < 0.0001) then
    begin
      TempPoint := LTPoint.Create(Point(B.X, B.Y));
      TempPoint.xx := B.xx;
      TempPoint.yy := B.yy;
      TempPoint.X := Round(TempPoint.xx);
      TempPoint.Y := Round(TempPoint.yy);
      CheckIntersection := TempPoint;
    end
  end;
End;

Procedure TCutThread.CheckExternalZones;
Var
  i, j: Integer;
  Room: TRoom;
  Seg: LTSegment;
Begin
  For i := 0 to Rooms.count - 1 do
  begin
    Room := TRoom(Rooms[i]);
    if Room.RoomType = roomExternal then
    Begin
      For j := 0 to Room.Points.count - 2 do
      Begin
        Seg := Room.GetSegment(Room.Points[j], Room.Points[j + 1], segments);
        Seg.orientation := T3DUtil.Angle(Room.Points[j + 1], Room.Points[j]) / pi * 180 + 180;
        While Seg.orientation > 180 do
          Seg.orientation := Seg.orientation - 360;
        While Seg.orientation < -180 do
          Seg.orientation := Seg.orientation + 360;
        Seg.Perimeter := True;
      End;
    End;
  End;
End;

end.
