unit i3dSketchLevel;

interface

uses
  i3dGeometry,
  classes,
  Windows,
  Controls,
  Graphics,
  dialogs,
  sysutils,
  Math,
  Printers,
  types,
  IzObjectPersist,
  Xml.XMLIntf,
  i3dSketchAnalysis;


type
  TSketchLevel = class(TObjectPersist)
  protected

    procedure innerLoadXml(node : IXMLNode); override;
    procedure innerSaveXml(node : IXMLNode); override;
  public
    points : TList; // List of points
    segments : TList; // List of segment
    Perimeters : TList; // List of perimeters
    Rooms : TList; // List of Rooms
    BufferBitmap : Tbitmap; // Buffer for Double-Buffering to avoid
    // fleaking
    VirtualPoints, VirtualSegments : TList; // Points and segments used by the
    // zoning and not input by the user

    FatherObject : Pointer; // Adress of the SketchObject
    AnalysisThread : TCutThread; // Thread that analyse the project
    Analyzed : Boolean; // True if analysis finished
    Height : Double; // Height of the level in meter
    PorteSelectionnee : Tporte;
    FenetreSelectionnee : TFenetre;
    BackGroundPicture : TPicture;
    NomPicture : string;
    decX, decY : Integer; // Decalage de l'image de fond
    NomFichier : string;
    Toiture : Boolean;

    procedure SaveToList(List : TStringList);
    procedure LoadfromList(List : TStringList; var Index : Integer);


    constructor Create(Father : Pointer);
    destructor Destroy; override;
    function PointCreation(Position : Tpoint) : LTpoint;
    function SegmentCreation(Orig, Dest : LTpoint) : LTSegment;
    procedure RotatePoints(Selection : TList; Lastclick : Tpoint; MouseUpPos : Tpoint);
    procedure ProjectPoints(Selection : TList; Lastclick : Tpoint; MouseUpPos : Tpoint);
    procedure MovePoints(Selection : TList; LastPoint : LTpoint; MouseUpPos : Tpoint);
    procedure CurvePoints(Segment : LTSegment; X, Y : Integer; SBitmap : Tbitmap);

    procedure FlashLine(X, Y : Integer; LastPoint : LTpoint; SBitmap : Tbitmap; GridType : Integer);
    procedure FlashSection(X, Y : Integer; LastPoint : LTpoint; SBitmap : Tbitmap; GridType : Integer);
    procedure FlashPoint(X, Y : Integer; LastPoint : LTpoint; Selection : TList; SBitmap : Tbitmap; GridType : Integer);
    procedure FlashRotate(X, Y : Integer; Lastclick : Tpoint; Selection : TList; SBitmap : Tbitmap; GridType : Integer);
    procedure FlashProject(X, Y : Integer; Lastclick : Tpoint; Selection : TList; SBitmap : Tbitmap;
      GridType : Integer);
    procedure FlashRect(Lastclick : Tpoint; X, Y : Integer; SBitmap : Tbitmap; GridType : Integer);
    procedure FlashRectTool(Lastclick : Tpoint; X, Y : Integer; SBitmap : Tbitmap; GridType : Integer);

    function FlashHeight(X, Y : Integer; SBitmap : Tbitmap; GridType : Integer) : TRoom;
    procedure Refresh(SBitmap : Tbitmap; GridType : Integer; Selection : TList);

    function PointExists(Point : Tpoint) : LTpoint;
    function SegmentExists(Orig, Dest : LTpoint) : LTSegment;
    function PointOnSegment(Poin : LTpoint) : LTSegment;
    function PointOnSegmentVar(Point : LTpoint; Tolerance : Integer) : LTSegment;
    function PointOnMenuiserie(Point : LTpoint; Tolerance : Integer) : LTSegment;
    function PointOnSegment_NoSensibility(Point : LTpoint) : LTSegment;
    function DeletePointsLines(X, Y : Integer) : Boolean;
    procedure CheckCross;
    procedure Analyze;
    function GetAngleLT(P1, P2 : LTpoint) : Double;
    function SetShading(MouseDownPOs : Tpoint) : Boolean;
    procedure SetNone(MouseDownPOs : Tpoint);
    function SetZone(MouseDownPOs : Tpoint) : Boolean;
    function GetZone(MouseDownPOs : Tpoint) : TStringList;
    function SetWall(MouseDownPOs : Tpoint) : Boolean;
    function SetCompo(MouseDownPOs : Tpoint) : Boolean;
    function GetWall(MouseDownPOs : Tpoint) : TStringList;
    procedure FlashCurve(Segment : LTSegment; X, Y : Integer; SBitmap : Tbitmap);
    procedure SetExternalSpace(MouseDownPOs : Tpoint);
    procedure SetLocal(MouseDownPOs : Tpoint);
    procedure Print(GridType, IdLevel : Integer);
    procedure SaveMeta(GridType, IdLevel : Integer ; fileName : string);
    procedure ZoomRect(P1, P2 : Tpoint);
    procedure DrawOnCanvas(Can : TCanvas; GridType : Integer; Selection : TList; Rect : TRect; DrawLegend : Boolean;
      Tx, Ty : Integer; Impress : Boolean);
    procedure CreateDoor(Point : Tpoint);
    procedure FlashDoor(Point : Tpoint; SBitmap : Tbitmap; GridType : Integer);
    procedure FlashMenuiserie(Point : Tpoint; SBitmap : Tbitmap; GridType : Integer);
    procedure CreateWindow(Point : Tpoint);
    procedure FlashWindow(Point : Tpoint; SBitmap : Tbitmap; GridType : Integer);
    procedure EditPorte;
    procedure EditFenetre;
    procedure DeleteDoorWindow;
    procedure Recaler(Center : Tpoint);
    function SetRoof(MouseDownPOs : Tpoint) : Boolean;
  end;

function SortSegment(Item1, Item2 : Pointer) : Integer;
function SortPoint(Item1, Item2 : Pointer) : Integer;

implementation

uses
  IzUtilitaires,
  i3dSketchObject,
  i3dTranslation,
  i3dFormInsertFenetre,
  i3dFormChangeCompo,
  i3dFormInputWall,
  i3dFormChoixTypeLocal,
  i3dFormRoofSettings,
  i3dFormRoomSettings,
  i3dFormInsertPorte,
  BiblioMenuiseries,
  BiblioCompositions,
  XmlUtil,
  i3dUtil,
  i3dConstants,
  i3dPolyUtil;

procedure TSketchLevel.Recaler(Center : Tpoint);
var
  i : Integer;
begin
  for i := 0 to points.count - 1 do
  begin
    LTpoint(points[i]).X := LTpoint(points[i]).X + Center.X;
    LTpoint(points[i]).Y := LTpoint(points[i]).Y + Center.Y;
  end;
end;

function TSketchLevel.FlashHeight(X, Y : Integer; SBitmap : Tbitmap; GridType : Integer) : TRoom;
var
  Room : TRoom;
  i : Integer;
  P, P1, P2 : LTpoint;
  Center : Tpoint;
  Zoom : Integer;
  chaine : string;
begin
  Center := TsketchObject(FatherObject).Center;
  Zoom := TsketchObject(FatherObject).Zoom;
  P := LTpoint.Create(Point(X, Y));
  P.X := X - Center.X;
  P.Y := Y - Center.Y;
  P.xx := P.X;
  P.yy := P.Y;
  Room := nil;
  for i := 0 to Rooms.count - 1 do
    if PointInOut(P, TRoom(Rooms[i]).points) then
      Room := TRoom(Rooms[i]);
  SBitmap.Assign(BufferBitmap);
  FlashHeight := Room;
  if Room <> nil then
  begin
    while Room.points.count > Room.HautPoint.count do
      Room.HautPoint.Add(0.0);
    with SBitmap.Canvas do
    begin
      Pen.Width := 2;
      Pen.color := ClRed;
      Pen.style := psSolid;
      Brush.style := bssolid;
      Brush.color := ClRed;
      for i := 0 to Room.points.count - 2 do
      begin
        P1 := LTpoint(Room.points[i]);
        P2 := LTpoint(Room.points[i + 1]);
        Rectangle((P1.X + Center.X) div Zoom - 2, (P1.Y + Center.Y) div Zoom - 2, (P1.X + Center.X) div Zoom + 2,
          (P1.Y + Center.Y) div Zoom + 2);
        // Seg := Room.getSegment(p1,p2,Segments);
        MoveTo((P1.X + Center.X) div Zoom, (P1.Y + Center.Y) div Zoom);
        LineTo((P2.X + Center.X) div Zoom, (P2.Y + Center.Y) div Zoom);
      end;
      Brush.style := bsClear;
      for i := 0 to Room.points.count - 2 do
      begin
        P1 := LTpoint(Room.points[i]);
        Str(Room.HautPoint[i] : 0 : 2, chaine);
        TextOut((P1.X + Center.X) div Zoom, (P1.Y + Center.Y) div Zoom, Chr(Ord('A') + i) + ' : ' + chaine +
          Labels[32]);
      end;
      Pen.Width := 1;
    end;
  end;
  P.Free;
end;


procedure TSketchLevel.DeleteDoorWindow;
var
  i, j : Integer;
  Seg : LTSegment;
begin
  for i := 0 to segments.count - 1 do
  begin
    Seg := LTSegment(segments[i]);
    if PorteSelectionnee <> nil then
      for j := Seg.ListePortes.count - 1 downto 0 do
      begin
        if Seg.ListePortes[j] = PorteSelectionnee then
        begin
          Seg.ListePortes.Delete(j);
        end
      end;
    if FenetreSelectionnee <> nil then
      for j := Seg.ListeFenetres.count - 1 downto 0 do
      begin
        if Seg.ListeFenetres[j] = FenetreSelectionnee then
        begin
          Seg.ListeFenetres.Delete(j);
        end
      end
  end;
  freeAndNil(PorteSelectionnee);
  freeAndNil(FenetreSelectionnee);
end;

procedure TSketchLevel.EditFenetre;
var
  chaine : string;
  Reponse : Word;
  Erreur, i, j : Integer;
  Seg : LTSegment;
begin
  FormFenetre.BitBtnMove.visible := True;
  FormFenetre.CompoFenetre := FenetreSelectionnee.CompoFen;
  if (FormFenetre.CompoFenetre <> nil) then
    FormFenetre.StaticCompoFen.Caption := FormFenetre.CompoFenetre.nom
  else
    FormFenetre.StaticCompoFen.Caption := Labels[234];
  Str(FenetreSelectionnee.Largeur : 0 : 2, chaine);
  FormFenetre.EditFenW.text := chaine;
  Str(FenetreSelectionnee.Hauteur : 0 : 2, chaine);
  FormFenetre.EditFenH.text := chaine;
  Str(FenetreSelectionnee.Allege : 0 : 2, chaine);
  FormFenetre.EditFenAllege.text := chaine;
  Str(FenetreSelectionnee.Retrait : 0 : 2, chaine);
  FormFenetre.EditRetrait.text := chaine;
  Reponse := FormFenetre.ShowModal;
  if Reponse <> mrCancel then
  begin
    FenetreSelectionnee.CompoFen := FormFenetre.CompoFenetre;
    Val(FormFenetre.EditFenW.text, FenetreSelectionnee.Largeur, Erreur);
    if Erreur <> 0 then
      FenetreSelectionnee.Largeur := 1;
    Val(FormFenetre.EditFenH.text, FenetreSelectionnee.Hauteur, Erreur);
    if Erreur <> 0 then
      FenetreSelectionnee.Hauteur := 1;
    Val(FormFenetre.EditFenAllege.text, FenetreSelectionnee.Allege, Erreur);
    if Erreur <> 0 then
      FenetreSelectionnee.Allege := 1;
    Val(FormFenetre.EditRetrait.text, FenetreSelectionnee.Retrait, Erreur);
    if Erreur <> 0 then
      FenetreSelectionnee.Retrait := 0;
    if Reponse = mrRetry then
    begin
      for i := 0 to segments.count - 1 do
      begin
        Seg := LTSegment(segments[i]);
        for j := Seg.ListeFenetres.count - 1 downto 0 do
          if Seg.ListeFenetres[j] = FenetreSelectionnee then
          begin
            FenetreSelectionnee.Free;
            Seg.ListeFenetres.Delete(j);
            Break;
          end;
        CurrentAction := 20;
      end;
    end;
  end;
end;


procedure TSketchLevel.EditPorte;
var
  chaine : string;
  Reponse : Word;
  Erreur, i, j : Integer;
  Seg : LTSegment;
begin
  FormPorte.BitBtnMove.visible := True;
  FormPorte.CompoPorte := PorteSelectionnee.CompoPorte;
  if (FormPorte.CompoPorte <> nil) then
    FormPorte.StaticCompoDoor.Caption := FormPorte.CompoPorte.nom
  else
    FormPorte.StaticCompoDoor.Caption := Labels[234];
  Str(PorteSelectionnee.Largeur : 0 : 2, chaine);
  FormPorte.EditDoorW.text := chaine;
  Str(PorteSelectionnee.Hauteur : 0 : 2, chaine);
  FormPorte.EditDoorH.text := chaine;
  Reponse := FormPorte.ShowModal;
  if Reponse <> mrCancel then
  begin
    PorteSelectionnee.CompoPorte := FormPorte.CompoPorte;
    Val(FormPorte.EditDoorW.text, PorteSelectionnee.Largeur, Erreur);
    if Erreur <> 0 then
      PorteSelectionnee.Largeur := 1;
    Val(FormPorte.EditDoorH.text, PorteSelectionnee.Hauteur, Erreur);
    if Erreur <> 0 then
      PorteSelectionnee.Hauteur := 1;
    FormPorte.Largeur := PorteSelectionnee.Largeur;
    FormPorte.Hauteur := PorteSelectionnee.Hauteur;
    FormPorte.CompoPorte := PorteSelectionnee.CompoPorte;
    if Reponse = mrRetry then
    begin
      for i := 0 to segments.count - 1 do
      begin
        Seg := LTSegment(segments[i]);
        for j := Seg.ListePortes.count - 1 downto 0 do
          if Seg.ListePortes[j] = PorteSelectionnee then
          begin
            PorteSelectionnee.Free;
            Seg.ListePortes.Delete(j);
            Break;
          end;
        CurrentAction := 19;
      end;
    end;
  end;
end;

// ********************************************************************
// CONSTRUCTOR
// *********************************************************************

constructor TSketchLevel.Create(Father : Pointer);
begin
  inherited Create;
  innerTagName := 'level';
  FatherObject := Father;
  points := TList.Create;
  segments := TList.Create;
  Perimeters := TList.Create;
  Rooms := TList.Create;
  BufferBitmap := Tbitmap.Create;
  BufferBitmap.HandleType := bmDIB;
  BufferBitmap.PixelFormat := pf24bit;
  VirtualPoints := TList.Create;
  VirtualSegments := TList.Create;
  // Height := 3;
  Height := 2.5;
  Analyzed := True;
  AnalysisThread := nil;
  BackGroundPicture := TPicture.Create;
  decX := 0;
  decY := 0;
end;

// ********************************************************************
// DESTRUCTOR
// *********************************************************************

destructor TSketchLevel.Destroy;
var
  i : Integer;
begin
  // Release all points and segments
  for i := segments.count - 1 downto 0 do
    LTSegment(segments[i]).Free;
  for i := points.count - 1 downto 0 do
    LTpoint(points[i]).Free;
  for i := Rooms.count - 1 downto 0 do
    TRoom(Rooms[i]).Free;
  for i := VirtualPoints.count - 1 downto 0 do
    LTpoint(VirtualPoints[i]).Free;
  for i := VirtualSegments.count - 1 downto 0 do
    LTSegment(VirtualSegments[i]).Free;
  Rooms.Free;
  points.Free;
  segments.Free;
  Perimeters.Free;
  BufferBitmap.Free;
  VirtualPoints.Free;
  VirtualSegments.Free;
  BackGroundPicture.Free;
  inherited Destroy;
end;

// ********************************************************************
// Delete points and lines that are at the given coordinates
// *********************************************************************

function TSketchLevel.DeletePointsLines(X, Y : Integer) : Boolean;
var
  Point : Tpoint;
  LPoint : LTpoint;
  i : Integer;
  Seg : LTSegment;
  Modif : Boolean;
  Center : Tpoint;
begin
  Center := TsketchObject(FatherObject).Center;
  Modif := False;
  Point.X := X;
  Point.Y := Y;

  // is there a point at this coordinate ?
  LPoint := PointExists(Point);
  Point.X := Point.X - Center.X;
  Point.Y := Point.Y - Center.Y;
  if (LPoint <> nil) and (not LPoint.Indeplacable) then
  // YES
  begin
    // Delete the point
    Modif := True;
    points.Remove(LPoint);
    // Delete the connected segments
    for i := segments.count - 1 downto 0 do
    begin
      Seg := LTSegment(segments[i]);
      if (Seg.a = LPoint) or (Seg.b = LPoint) then
      begin
        Seg.Free;
        segments.Delete(i);
      end
    end
  end
  else
    // NO : create a virtual point
    LPoint := LTpoint.Create(Point);

  // is the point on a segment ?
  Seg := PointOnSegment(LPoint);
  if (Seg <> nil) and (not Seg.Indeplacable) then
  // YES
  begin
    // Delete Segment
    Modif := True;
    segments.Remove(Seg);
    Seg.Free
  end;
  // release point memory
  LPoint.Free;
  DeletePointsLines := Modif;

  Analyzed := False;
end;


// ********************************************************************
// This method is used by the sort method of the list of points
// if item 1 < item 2 then SortPoint := -1
// if item 2 < item 1 then SortPoint := 1
// if item 1 = item 2 then SortPoint := 0
// *********************************************************************

function SortPoint(Item1, Item2 : Pointer) : Integer;
var
  P1, P2 : LTpoint;
begin
  P1 := LTpoint(Item1);
  P2 := LTpoint(Item2);
  if P1.Y < P2.Y then
    SortPoint := 1
  else if P1.Y > P2.Y then
    SortPoint := -1
  else if P1.X > P2.X then
    SortPoint := 1
  else if P1.X < P2.X then
    SortPoint := -1
  else
    SortPoint := 0;
end;

// ********************************************************************
// This method is used by the sort method of the list of segments
// if item 1 < item 2 then SortSegment := -1
// if item 2 < item 1 then SortSegment := 1
// if item 1 = item 2 then SortSegment := 0
// *********************************************************************

function SortSegment(Item1, Item2 : Pointer) : Integer;
var
  Index1a, Index1b, Index2a, Index2b : Integer;
  Chaine1, Chaine2 : string;
  Lev : TSketchLevel;
begin
  Lev := TSketchLevel(project.SketchObject.Levels[project.SketchObject.CurrentLevel]);
  Index1a := Lev.points.IndexOf(LTSegment(Item1).a);
  Index1b := Lev.points.IndexOf(LTSegment(Item1).b);
  if Index1a < Index1b then
    Chaine1 := IntToStr(Index1a) + IntToStr(Index1b)
  else
    Chaine1 := IntToStr(Index1b) + IntToStr(Index1a);

  Index2a := Lev.points.IndexOf(LTSegment(Item2).a);
  Index2b := Lev.points.IndexOf(LTSegment(Item2).b);
  if Index2a < Index2b then
    Chaine2 := IntToStr(Index2a) + IntToStr(Index2b)
  else
    Chaine2 := IntToStr(Index2b) + IntToStr(Index2a);

  if Chaine1 < Chaine2 then
    SortSegment := -1
  else if Chaine1 > Chaine2 then
    SortSegment := 1
  else
    SortSegment := 0;
end;

// ********************************************************************
// This method create a point at the given coordinate and return it
// if a point already exits, it will be returned instead
// *********************************************************************

function TSketchLevel.PointCreation(Position : Tpoint) : LTpoint;
var
  AddPoint : LTpoint;
  Seg, Seg2 : LTSegment;
  Center : Tpoint;
  i, j : Integer;
  Centre : Double;
  Niveau : Integer;
  SurPerimetre : Boolean;
  Exposit : Tpoint;
  Piece : TRoom;
  Fenetre : TFenetre;
begin
  Exposit := Position;
  Center := TsketchObject(FatherObject).Center;
  // check existance of the point
  AddPoint := PointExists(Position);
  // Absolute position calculation
  Position.X := (Position.X) - Center.X;
  Position.Y := (Position.Y) - Center.Y;
  SurPerimetre := False;
  if AddPoint = nil then
  // the point does not exist
  begin
    // Creation
    AddPoint := LTpoint.Create(Position);
    points.Add(AddPoint);
    // is the point on a segment ?
    Seg := PointOnSegment(AddPoint);
    if Seg <> nil then
    // YES, so cut the segment in two parts
    begin
      for i := 0 to Rooms.count - 1 do
      begin
        Piece := TRoom(Rooms[i]);
        for j := 0 to Piece.points.count - 2 do
          if ((Seg.a = Piece.points[j]) and (Seg.b = Piece.points[j + 1])) or
            ((Seg.b = Piece.points[j]) and (Seg.a = Piece.points[j + 1])) then
            Piece.points.Insert(j + 1, AddPoint);
      end;


      if Seg.Perimeter then
        SurPerimetre := True;
      Seg2 := LTSegment.Create(Seg.a, AddPoint);
      Seg2.Perimeter := Seg.Perimeter;
      Seg2.IsVirtual := Seg.IsVirtual;
      Seg2.PercentageGlazing := Seg.PercentageGlazing; // inutile
      Seg2.NumberGlazing := Seg.NumberGlazing;
      Seg2.GlazingColor := Seg.GlazingColor;
      Seg2.Shadings := Seg.Shadings;
      Seg2.Orientation := Seg.Orientation;

      // Repositionnier les vitrages
      for Fenetre in Seg.ListeFenetres do
      begin
        Seg.Distance := Sqrt(Sqr(Seg.b.X - Seg.a.X) + Sqr(Seg.b.Y - Seg.a.Y));
        Fenetre.PositionAbsolue := Seg.Distance * Fenetre.Center;
      end;
      Seg.a := AddPoint;
      segments.Add(Seg2);

      for i := Seg.ListeFenetres.count - 1 downto 0 do
      begin
        Fenetre := Seg.ListeFenetres[i];
        Seg.Distance := Sqrt(Sqr(Seg.b.X - Seg.a.X) + Sqr(Seg.b.Y - Seg.a.Y));
        Seg2.Distance := Sqrt(Sqr(Seg2.b.X - Seg2.a.X) + Sqr(Seg2.b.Y - Seg2.a.Y));
        Centre := Fenetre.PositionAbsolue / Seg2.Distance;
        if Centre <= 1 then
        begin
          Fenetre.Center := Centre;
          Seg2.ListeFenetres.Add(Seg.ListeFenetres[i]);
          Seg.ListeFenetres.Delete(i);
        end
        else
        begin
          Fenetre.PositionAbsolue := Fenetre.PositionAbsolue - Seg2.Distance;
          Centre := Fenetre.PositionAbsolue / Seg.Distance;
          Fenetre.Center := Centre;
        end;
      end;
    end;
  end;
  PointCreation := AddPoint;
  Analyzed := False;
  if Toiture and SurPerimetre then
  begin
    Niveau := project.SketchObject.CurrentLevel - 1;
    TSketchLevel(project.SketchObject.Levels[Niveau]).PointCreation(Exposit);
  end;
end;

// ********************************************************************
// This method display the instant selection rectangle
// *********************************************************************

procedure TSketchLevel.FlashRect(Lastclick : Tpoint; X, Y : Integer; SBitmap : Tbitmap; GridType : Integer);
var
  Zoom : Integer;
  Center : Tpoint;
begin
  Center := TsketchObject(FatherObject).Center;
  Zoom := TsketchObject(FatherObject).Zoom;
  // calculate absolute coordinate
  X := (X) - Center.X;
  Y := (Y) - Center.Y;
  Lastclick.X := (Lastclick.X) - Center.X;
  Lastclick.Y := (Lastclick.Y) - Center.Y;

  // copy the buffer on the bitmap
  SBitmap.Assign(BufferBitmap);
  with SBitmap.Canvas do
  begin
    // draw the rectangle
    Pen.color := clBlack;
    Pen.style := psDot;
    Brush.style := bsClear;
    Rectangle((Lastclick.X + Center.X) div Zoom, (Lastclick.Y + Center.Y) div Zoom, (X + Center.X) div Zoom,
      (Y + Center.Y) div Zoom);
  end;
end;

// ********************************************************************
// This method display the instant rectangle tool
// *********************************************************************

procedure TSketchLevel.FlashRectTool(Lastclick : Tpoint; X, Y : Integer; SBitmap : Tbitmap; GridType : Integer);
var
  Distance : Double;
  TempString : string;
  Zoom : Integer;
  Center : Tpoint;
begin
  Center := TsketchObject(FatherObject).Center;
  Zoom := TsketchObject(FatherObject).Zoom;
  // Calculate the absolute coordinate
  X := (X) - Center.X;
  Y := (Y) - Center.Y;
  Lastclick.X := (Lastclick.X) - Center.X;
  Lastclick.Y := (Lastclick.Y) - Center.Y;
  // Copy the buffer for the double-Buffering
  SBitmap.Assign(BufferBitmap);
  with SBitmap.Canvas do
  begin
    // Draw the rectangle
    Pen.color := ClRed;
    Pen.style := psDot;
    Brush.style := bsClear;
    Rectangle((Lastclick.X + Center.X) div Zoom, (Lastclick.Y + Center.Y) div Zoom, (X + Center.X) div Zoom,
      (Y + Center.Y) div Zoom);
    if TsketchObject(FatherObject).DisplayLength then
    begin
      Distance := Round(Abs(Lastclick.Y - Y) / project.SketchObject.scale * 100) / 100;
      Str(Distance : 0 : 2, TempString);
      Pen.style := psSolid;
      Brush.style := bsClear;
      Font.Name := 'Arial';
      Font.Size := 8;
      TextOut(2 + (Lastclick.X + Center.X) div Zoom, ((Lastclick.Y + Y) div 2 + Center.Y) div Zoom, TempString + 'm');
      Distance := Round(Abs(Lastclick.X - X) / project.SketchObject.scale * 100) / 100;
      Str(Distance : 0 : 2, TempString);
      Pen.style := psSolid;
      Brush.style := bsClear;
      Font.Name := 'Arial';
      Font.Size := 8;
      TextOut(((Lastclick.X + X) div 2 + Center.X) div Zoom, (Lastclick.Y + Center.Y) div Zoom, TempString + 'm');
    end;
  end;
end;

// ********************************************************************
// This method will display an instant line
// *********************************************************************

procedure TSketchLevel.FlashLine(X, Y : Integer; LastPoint : LTpoint; SBitmap : Tbitmap; GridType : Integer);
var
  Distance : Double;
  TempString : string;
  Zoom : Integer;
  Center : Tpoint;
  xxx, yyy : Double;
begin
  Center := TsketchObject(FatherObject).Center;
  Zoom := TsketchObject(FatherObject).Zoom;
  // Calculate the absolute coordinate
  X := (X) - Center.X;
  Y := (Y) - Center.Y;
  // Copy the buffer for the double-Buffering
  SBitmap.Assign(BufferBitmap);
  with SBitmap.Canvas do
  begin
    // draw the line
    Pen.color := clGreen;
    Pen.style := psDot;
    Brush.style := bsClear;
    MoveTo((LastPoint.X + Center.X) div Zoom, (LastPoint.Y + Center.Y) div Zoom);
    LineTo((X + Center.X) div Zoom, (Y + Center.Y) div Zoom);
    if TsketchObject(FatherObject).DisplayLength then
    begin
      xxx := X;
      yyy := Y;
      Distance := Round(Sqrt(Sqr(LastPoint.X - xxx) + Sqr(LastPoint.Y - yyy)) / project.SketchObject.scale *
        100) / 100;
      Str(Distance : 0 : 2, TempString);
      Pen.style := psSolid;
      Brush.style := bsClear;
      Font.Name := 'Arial';
      Font.Size := 8;
      TextOut(((LastPoint.X + X) div 2 + Center.X) div Zoom, ((LastPoint.Y + Y) div 2 + Center.Y) div Zoom,
        TempString + 'm');
    end;
  end;
end;

// ********************************************************************
// This method will display an instant line
// *********************************************************************

procedure TSketchLevel.FlashSection(X, Y : Integer; LastPoint : LTpoint; SBitmap : Tbitmap; GridType : Integer);
var
  VectorLength : Double;
  VectorX, VectorY : Double;
  Zoom : Integer;
  Center : Tpoint;
begin
  Center := TsketchObject(FatherObject).Center;
  Zoom := TsketchObject(FatherObject).Zoom;
  // Calculate the absolute coordinate
  X := (X) - Center.X;
  Y := (Y) - Center.Y;
  // Copy the buffer for the double-Buffering
  SBitmap.Assign(BufferBitmap);
  with SBitmap.Canvas do
  begin
    // draw the line
    Pen.color := clGreen;
    Pen.style := psDot;
    Brush.style := bsClear;
    MoveTo((LastPoint.X + Center.X) div Zoom, (LastPoint.Y + Center.Y) div Zoom);
    LineTo((X + Center.X) div Zoom, (Y + Center.Y) div Zoom);

    VectorX := (X - LastPoint.X);
    VectorY := (Y - LastPoint.Y);
    VectorLength := Sqrt(Sqr(VectorX) + Sqr(VectorY));
    if VectorLength <> 0 then
    begin
      VectorX := VectorX / VectorLength * 10;
      VectorY := VectorY / VectorLength * 10;
      MoveTo((LastPoint.X + Center.X) div Zoom, (LastPoint.Y + Center.Y) div Zoom);
      LineTo(Round(VectorX + (Center.X + LastPoint.X) / Zoom), Round(VectorY + (Center.Y + LastPoint.Y) / Zoom));
    end;
    VectorX := (Y - LastPoint.Y);
    VectorY := -(X - LastPoint.X);
    VectorLength := Sqrt(Sqr(VectorX) + Sqr(VectorY));
    if VectorLength <> 0 then
    begin
      VectorX := VectorX / VectorLength * 20;
      VectorY := VectorY / VectorLength * 20;
      LineTo(Round(VectorX + (Center.X + LastPoint.X) / Zoom), Round(VectorY + (Center.Y + LastPoint.Y) / Zoom));
    end;
    VectorX := (X - LastPoint.X);
    VectorY := (Y - LastPoint.Y);
    VectorLength := Sqrt(Sqr(VectorX) + Sqr(VectorY));
    if VectorLength <> 0 then
    begin
      VectorX := VectorX / VectorLength * -10;
      VectorY := VectorY / VectorLength * -10;
      LineTo(Round(VectorX + (Center.X + LastPoint.X) / Zoom), Round(VectorY + (Center.Y + LastPoint.Y) / Zoom));
    end;

    VectorX := -(X - LastPoint.X);
    VectorY := -(Y - LastPoint.Y);
    VectorLength := Sqrt(Sqr(VectorX) + Sqr(VectorY));
    if VectorLength <> 0 then
    begin
      VectorX := VectorX / VectorLength * 10;
      VectorY := VectorY / VectorLength * 10;
      MoveTo((X + Center.X) div Zoom, (Y + Center.Y) div Zoom);
      LineTo(Round(VectorX + (Center.X + X) / Zoom), Round(VectorY + (Center.Y + Y) / Zoom));
    end;
    VectorX := (Y - LastPoint.Y);
    VectorY := -(X - LastPoint.X);
    VectorLength := Sqrt(Sqr(VectorX) + Sqr(VectorY));
    if VectorLength <> 0 then
    begin
      VectorX := VectorX / VectorLength * 20;
      VectorY := VectorY / VectorLength * 20;
      LineTo(Round(VectorX + (Center.X + X) / Zoom), Round(VectorY + (Center.Y + Y) / Zoom));
    end;
    VectorX := -(X - LastPoint.X);
    VectorY := -(Y - LastPoint.Y);
    VectorLength := Sqrt(Sqr(VectorX) + Sqr(VectorY));
    if VectorLength <> 0 then
    begin
      VectorX := VectorX / VectorLength * -10;
      VectorY := VectorY / VectorLength * -10;
      LineTo(Round(VectorX + (Center.X + X) / Zoom), Round(VectorY + (Center.Y + Y) / Zoom));
    end;

  end;
end;

// ********************************************************************
// This method display the instant selection points and segments
// *********************************************************************

procedure TSketchLevel.FlashPoint(X, Y : Integer; LastPoint : LTpoint; Selection : TList; SBitmap : Tbitmap;
  GridType : Integer);
var
  Rect : TRect;
  i, j, Index, Index2 : Integer;
  Seg : LTSegment;
  Coordinate : Tpoint;
  Zoom : Integer;
  Center : Tpoint;
  xxx, yyy, Distance : Double;
  TempString : string;
  Dx, Dy : Integer;
  ddx, ddy : Double;
begin
  ddx := 0;
  Center := TsketchObject(FatherObject).Center;
  Zoom := TsketchObject(FatherObject).Zoom;
  // Calculate the absolute coordinates
  Dx := (X - Center.X) - LastPoint.X;
  Dy := (Y - Center.Y) - LastPoint.Y;
  // copy the buffer for the double-buffering
  SBitmap.Assign(BufferBitmap);
  with SBitmap.Canvas do
  begin
    for j := 0 to Selection.count - 1 do
    begin
      // Get one point the selection
      LastPoint := LTpoint(Selection[j]);
      // move it
      Coordinate.X := LastPoint.X + Dx + Center.X;
      Coordinate.Y := LastPoint.Y + Dy + Center.Y;
      // draw it
      Rect.Left := (Coordinate.X) div Zoom - 2;
      Rect.Top := (Coordinate.Y) div Zoom - 2;
      Rect.Right := (Coordinate.X) div Zoom + 2;
      Rect.Bottom := (Coordinate.Y) div Zoom + 2;
      Brush.color := ClRed;
      Pen.style := psDot;
      FillRect(Rect);
    end;
    Pen.color := ClRed;
    Brush.style := bssolid;
    SBitmap.Canvas.Brush.style := bsClear;
    SBitmap.Canvas.Brush.color := clWhite;
    // for each segments
    for i := 0 to segments.count - 1 do
    begin
      Seg := LTSegment(segments[i]);
      index := Selection.IndexOf(Seg.a);
      Index2 := Selection.IndexOf(Seg.b);
      if (index <> -1) or (Index2 <> -1) then
      // if the segment is linked with the moved point
      begin
        // draw the line with the new position of the point
        if index <> -1 then
          MoveTo((Seg.a.X + Dx + Center.X) div Zoom, (Seg.a.Y + Dy + Center.Y) div Zoom)
        else
          MoveTo((Seg.a.X + Center.X) div Zoom, (Seg.a.Y + Center.Y) div Zoom);
        if Index2 <> -1 then
          LineTo((Seg.b.X + Dx + Center.X) div Zoom, (Seg.b.Y + Dy + Center.Y) div Zoom)
        else
          LineTo((Seg.b.X + Center.X) div Zoom, (Seg.b.Y + Center.Y) div Zoom);
        if TsketchObject(FatherObject).DisplayLength then
          // xxx := x;
          // yyy := y;
          ddx := Dx;
        ddy := Dy;
        if index <> -1 then
          Distance := Round(Sqrt(Sqr(Seg.a.X + ddx - Seg.b.X) + Sqr(Seg.a.Y + ddy - Seg.b.Y)) /
            project.SketchObject.scale * 100) / 100
        else
          Distance := Round(Sqrt(Sqr(Seg.a.X - ddx - Seg.b.X) + Sqr(Seg.a.Y - ddy - Seg.b.Y)) /
            project.SketchObject.scale * 100) / 100;
        Str(Distance : 0 : 2, TempString);
        Pen.style := psSolid;
        Brush.style := bsClear;
        Font.Name := 'Arial';
        Font.Size := 8;
        TextOut((((Seg.a.X + Seg.b.X + Dx) div 2 + Center.X) div Zoom),
          (((Seg.a.Y + Seg.b.Y + Dy) div 2 + Center.Y) div Zoom), TempString + 'm');
      end;
    end;
  end;
end;

// ********************************************************************
// This method display the instant selection projection of points and
// Segments
// *********************************************************************

procedure TSketchLevel.FlashProject(X, Y : Integer; Lastclick : Tpoint; Selection : TList; SBitmap : Tbitmap;
  GridType : Integer);
var
  Rect : TRect;
  i, j, Index, Index2 : Integer;
  Seg : LTSegment;
  Coordinate, Coordinate1, Coordinate2 : Tpoint;
  ProjectionCenter, LastPoint : LTpoint;
  ProjectCoeff, Dx, Dy, AverageDistance : Double;
  Zoom : Integer;
  Center : Tpoint;
begin
  Center := TsketchObject(FatherObject).Center;
  Zoom := TsketchObject(FatherObject).Zoom;
  // Calculate absolute coordinate of the projection center
  Lastclick.X := Lastclick.X - Center.X;
  Lastclick.Y := Lastclick.Y - Center.Y;
  ProjectionCenter := LTpoint.Create(Lastclick);

  // Calculate the Average Distance between the projection center
  // and the Average position of the points of the selection (D1)
  AverageDistance := 0;
  for i := 0 to Selection.count - 1 do
  begin
    LastPoint := LTpoint(Selection[i]);
    Dx := LastPoint.X - ProjectionCenter.X;
    Dy := LastPoint.Y - ProjectionCenter.Y;
    AverageDistance := AverageDistance + Sqrt(Sqr(Dx) + Sqr(Dy));
  end;
  if Selection.count <> 0 then
    AverageDistance := AverageDistance / Selection.count
  else
    AverageDistance := 1;

  // Calculate the distance between the mouse and the projection center (D2)
  Dx := (X - Center.X) - ProjectionCenter.X;
  Dy := (Y - Center.Y) - ProjectionCenter.Y;

  // Calculate the projection coefficient (D1/D2)
  if AverageDistance <> 0 then
    ProjectCoeff := Sqrt(Sqr(Dx) + Sqr(Dy)) / AverageDistance
  else
    ProjectCoeff := 1;

  // Copy the buffer for the Double-Buffering
  SBitmap.Assign(BufferBitmap);
  with SBitmap.Canvas do
  begin
    // Draw a cross at the projection center
    Pen.color := clBlack;
    Pen.style := psSolid;
    MoveTo((ProjectionCenter.X + Center.X) div Zoom - 5, (ProjectionCenter.Y + Center.Y) div Zoom);
    LineTo((ProjectionCenter.X + Center.X) div Zoom + 6, (ProjectionCenter.Y + Center.Y) div Zoom);
    MoveTo((ProjectionCenter.X + Center.X) div Zoom, (ProjectionCenter.Y + Center.Y) div Zoom - 5);
    LineTo((ProjectionCenter.X + Center.X) div Zoom, (ProjectionCenter.Y + Center.Y) div Zoom + 6);
    for j := 0 to Selection.count - 1 do
    begin
      // Project anf draw each point of the Selection
      LastPoint := LTpoint(Selection[j]);
      Coordinate := LastPoint.Projected(ProjectionCenter, ProjectCoeff);
      Coordinate.X := Coordinate.X + Center.X;
      Coordinate.Y := Coordinate.Y + Center.Y;
      Rect.Left := (Coordinate.X) div Zoom - 2;
      Rect.Top := (Coordinate.Y) div Zoom - 2;
      Rect.Right := (Coordinate.X) div Zoom + 2;
      Rect.Bottom := (Coordinate.Y) div Zoom + 2;
      Brush.color := ClRed;
      Pen.style := psDot;
      FillRect(Rect);
    end;
    Pen.color := ClRed;
    Brush.style := bssolid;
    SBitmap.Canvas.Brush.style := bsClear;
    SBitmap.Canvas.Brush.color := clWhite;
    for i := 0 to segments.count - 1 do
    begin
      // Project and Draw each segment linked with a projected point
      Seg := LTSegment(segments[i]);
      index := Selection.IndexOf(Seg.a);
      Index2 := Selection.IndexOf(Seg.b);
      if Seg.a <> ProjectionCenter then
        Coordinate1 := Seg.a.Projected(ProjectionCenter, ProjectCoeff)
      else
      begin
        Coordinate1.X := Seg.a.X;
        Coordinate1.Y := Seg.a.Y;
      end;
      if Seg.b <> ProjectionCenter then
        Coordinate2 := Seg.b.Projected(ProjectionCenter, ProjectCoeff)
      else
      begin
        Coordinate2.X := Seg.b.X;
        Coordinate2.Y := Seg.b.Y;
      end;
      if (index <> -1) or (Index2 <> -1) then
      begin
        if index <> -1 then
          MoveTo((Coordinate1.X + Center.X) div Zoom, (Coordinate1.Y + Center.Y) div Zoom)
        else
          MoveTo((Seg.a.X + Center.X) div Zoom, (Seg.a.Y + Center.Y) div Zoom);
        if Index2 <> -1 then
          LineTo((Coordinate2.X + Center.X) div Zoom, (Coordinate2.Y + Center.Y) div Zoom)
        else
          LineTo((Seg.b.X + Center.X) div Zoom, (Seg.b.Y + Center.Y) div Zoom);
      end;
    end;
  end;
  ProjectionCenter.Free;
end;

// ********************************************************************
// This method display the instant selection rotation of points and
// Segments
// *********************************************************************

procedure TSketchLevel.FlashRotate(X, Y : Integer; Lastclick : Tpoint; Selection : TList; SBitmap : Tbitmap;
  GridType : Integer);
var
  Rect : TRect;
  i, Dx, Dy, j, Index, Index2 : Integer;
  Seg : LTSegment;
  Coordinate, Coordinate1, Coordinate2 : Tpoint;
  Angle : Double;
  RotationCenter, LastPoint : LTpoint;
  Zoom : Integer;
  Center : Tpoint;
  chaine : string;
begin
  Center := TsketchObject(FatherObject).Center;
  Zoom := TsketchObject(FatherObject).Zoom;
  // Calculate absolute coordinate of the projection center
  Lastclick.X := Lastclick.X - Center.X;
  Lastclick.Y := Lastclick.Y - Center.Y;
  RotationCenter := LTpoint.Create(Lastclick);

  // Calculate the absolute coordinate of the mouse
  Dx := (X - Center.X) - RotationCenter.X;
  Dy := (Y - Center.Y) - RotationCenter.Y;

  // Calculate Angle of the vector with 0�
  if (Dx = 0) and (Dy = 0) then
    exit;
  Angle := GetAngle(Dx, Dy);

  // Copy buffer for double-buffering
  SBitmap.Assign(BufferBitmap);

  with SBitmap.Canvas do
  begin
    // Draw a cross at the position of the Rotation Center
    Pen.color := clBlack;
    Pen.style := psSolid;
    Brush.style := bssolid;
    Font.style := Font.style + [fsBold];
    Str(Angle / pi * 180 : 0 : 2, chaine);
    TextOut(10, 10, 'Rotation angle : ' + chaine + ' �');
    Font.style := Font.style - [fsBold];
    MoveTo((RotationCenter.X + Center.X) div Zoom - 5, (RotationCenter.Y + Center.Y) div Zoom);
    LineTo((RotationCenter.X + Center.X) div Zoom + 6, (RotationCenter.Y + Center.Y) div Zoom);
    MoveTo((RotationCenter.X + Center.X) div Zoom, (RotationCenter.Y + Center.Y) div Zoom - 5);
    LineTo((RotationCenter.X + Center.X) div Zoom, (RotationCenter.Y + Center.Y) div Zoom + 6);
    for j := 0 to Selection.count - 1 do
    begin
      // Rotate and draw each point of the selection
      LastPoint := LTpoint(Selection[j]);
      Coordinate := LastPoint.Rotated(RotationCenter, Angle);
      Coordinate.X := Coordinate.X + Center.X;
      Coordinate.Y := Coordinate.Y + Center.Y;
      Rect.Left := (Coordinate.X) div Zoom - 2;
      Rect.Top := (Coordinate.Y) div Zoom - 2;
      Rect.Right := (Coordinate.X) div Zoom + 2;
      Rect.Bottom := (Coordinate.Y) div Zoom + 2;
      Brush.color := ClRed;
      Pen.style := psDot;
      FillRect(Rect);
    end;
    Pen.color := ClRed;
    Brush.style := bssolid;
    SBitmap.Canvas.Brush.style := bsClear;
    SBitmap.Canvas.Brush.color := clWhite;
    for i := 0 to segments.count - 1 do
    begin
      // Rotate and draw each segment linked to a rotated point
      Seg := LTSegment(segments[i]);
      index := Selection.IndexOf(Seg.a);
      Index2 := Selection.IndexOf(Seg.b);
      if Seg.a <> RotationCenter then
        Coordinate1 := Seg.a.Rotated(RotationCenter, Angle)
      else
      begin
        Coordinate1.X := Seg.a.X;
        Coordinate1.Y := Seg.a.Y;
      end;
      if Seg.b <> RotationCenter then
        Coordinate2 := Seg.b.Rotated(RotationCenter, Angle)
      else
      begin
        Coordinate2.X := Seg.b.X;
        Coordinate2.Y := Seg.b.Y;
      end;
      if (index <> -1) or (Index2 <> -1) then
      begin
        if index <> -1 then
          MoveTo((Coordinate1.X + Center.X) div Zoom, (Coordinate1.Y + Center.Y) div Zoom)
        else
          MoveTo((Seg.a.X + Center.X) div Zoom, (Seg.a.Y + Center.Y) div Zoom);
        if Index2 <> -1 then
          LineTo((Coordinate2.X + Center.X) div Zoom, (Coordinate2.Y + Center.Y) div Zoom)
        else
          LineTo((Seg.b.X + Center.X) div Zoom, (Seg.b.Y + Center.Y) div Zoom);
      end;
    end;
  end;
  RotationCenter.Free;
end;

// ********************************************************************
// This method draw the complete sketch in the buffer and
// display it
// *********************************************************************

procedure TSketchLevel.Refresh(SBitmap : Tbitmap; GridType : Integer; Selection : TList);
var
  Rect : TRect;
begin
  // Resize the buffer to the bitmap size
  BufferBitmap.Width := SBitmap.Width;
  BufferBitmap.Height := SBitmap.Height;
  BufferBitmap.PixelFormat := pf24bit;
  BufferBitmap.Canvas.Brush.color := clWhite;
  BufferBitmap.Canvas.Brush.style := bssolid;
  Rect.Left := 0;
  Rect.Top := 0;
  Rect.Right := BufferBitmap.Width;
  Rect.Bottom := BufferBitmap.Height;
  BufferBitmap.Canvas.FillRect(Rect);
  DrawOnCanvas(BufferBitmap.Canvas, GridType, Selection, Rect, True, 0, 0, False);
  // Display the number of segments and points in the status bar
  T3DUtil.setStatus(' ' + IntToStr(points.count) + ' Points / ' + IntToStr(segments.count) + ' segments', 0);
  // Copy the buffer into the bitmap to display it now
  // if SBitmap is TBitmap then
  SBitmap.PixelFormat := pf24bit;
  SBitmap.Canvas.Draw(0, 0, BufferBitmap);
  T3DUtil.statusRepaint;
end;

procedure TSketchLevel.DrawOnCanvas(Can : TCanvas; GridType : Integer; Selection : TList; Rect : TRect;
  DrawLegend : Boolean; Tx, Ty : Integer; Impress : Boolean);
var
  Seg : LTSegment;
  x1, y1, x2, y2, kkk : Integer;
  SegSize : string;
  Room : TRoom;
  i, j, z : Integer;
  Lev0 : TSketchLevel;
  PointArray : array of Tpoint;
  Center, P1, P2 : Tpoint;
  Zoom : Integer;
  AverageX, AverageY : Integer;
  Bidon, Bidon2 : string;
  Recto : TRect;
  porte : Tporte;
  VecteurX, VecteurY, VecteurX2, VecteurY2, Longueur, scale : Double;
  LPoint : LTpoint;
  Fenetre : TFenetre;
  Vecteur : Tpoint;
  chaine : string;
begin
  Zoom := TsketchObject(FatherObject).Zoom;
  Center := TsketchObject(FatherObject).Center;
  scale := TsketchObject(FatherObject).scale;
  with Can do
  begin
    // Clear the screen
    Recto.Left := (Center.X + decX) div Zoom;
    Recto.Top := (Center.Y + decY) div Zoom;
    Recto.Right := Recto.Left + (project.CurrentLevel.BackGroundPicture.Width * 64 div Zoom);
    Recto.Bottom := Recto.Top + (project.CurrentLevel.BackGroundPicture.Height * 64 div Zoom);
    if not TsketchObject(FatherObject).SpecialDisplay then
      StretchDraw(Recto, project.CurrentLevel.BackGroundPicture.Bitmap);

    // Draw the gray grid (Grid 1)
    if GridType = 1 then
    begin
      Pen.style := psSolid;
      Pen.color := $00F0F0F0;
      i := 0;
      while i < Rect.Right do
      begin
        MoveTo(i, 0);
        LineTo(i, Rect.Bottom);
        i := i + GridStep;
      end;
      i := 0;
      while i < Rect.Bottom do
      begin
        MoveTo(0, i);
        LineTo(Rect.Right, i);
        i := i + GridStep;
      end;
    end
    // Draw the black points grid (Grid 2)
    else if GridType = 2 then
    begin
      Pen.color := clBlack;
      Pen.style := psSolid;
      i := 0;
      while i < Rect.Right do
      begin
        j := 0;
        while j < Rect.Bottom do
        begin
          MoveTo(i, j);
          LineTo(i, j + 1);
          j := j + GridStep;
        end;
        i := i + GridStep;
      end;
    end;
    Lev0 := TSketchLevel(project.SketchObject.Levels[0]);
    if Lev0.Analyzed and (Lev0 <> Self) then
    begin
      Pen.style := psSolid;
      Pen.color := $00E0E0E0;
      Pen.Width := 3;
      for z := 0 to Lev0.Perimeters.count - 1 do
      begin
        SetLength(PointArray, TList(Lev0.Perimeters[z]).count);
        for i := 0 to TList(Lev0.Perimeters[z]).count - 1 do
        begin
          PointArray[i].X := (LTpoint(TList(Lev0.Perimeters[z])[i]).X + Center.X + Tx) div Zoom;
          PointArray[i].Y := (LTpoint(TList(Lev0.Perimeters[z])[i]).Y + Center.Y + Ty) div Zoom;
        end;
        PolyLine(PointArray);

      end;
      if Lev0.Rooms.count <> 0 then
        for z := 0 to Lev0.Rooms.count - 1 do
          if TRoom(Lev0.Rooms[z]).RoomType = roomObstruction then
          begin
            SetLength(PointArray, TRoom(Lev0.Rooms[z]).points.count);
            AverageX := 0;
            AverageY := 0;
            for i := 0 to TRoom(Lev0.Rooms[z]).points.count - 1 do
            begin
              PointArray[i].X := (LTpoint(TRoom(Lev0.Rooms[z]).points[i]).X + Center.X + Tx) div Zoom;
              PointArray[i].Y := (LTpoint(TRoom(Lev0.Rooms[z]).points[i]).Y + Center.Y + Ty) div Zoom;
              if i <> 0 then
              begin
                AverageX := AverageX + PointArray[i].X;
                AverageY := AverageY + PointArray[i].Y
              end;
            end;
            Brush.color := clLtGray;
            Brush.style := bssolid;
            Pen.color := Brush.color;
            Polygon(PointArray);
            AverageX := AverageX div (TRoom(Lev0.Rooms[z]).points.count - 1);
            AverageY := AverageY div (TRoom(Lev0.Rooms[z]).points.count - 1);
            Str(TRoom(Lev0.Rooms[z]).Value1 : 0 : 2, chaine);
            Bidon := Labels[31] + chaine + Labels[32];

            AverageX := AverageX - TextWidth(Bidon) div 2;
            AverageY := AverageY - TextHeight(Bidon) div 2;
            Brush.style := bsClear;
            Font.Name := 'Arial';
            Font.Size := 8;
            TextOut(AverageX, AverageY, Bidon);
          end
    end;
    Pen.Width := 1;
    // If analyzed colorize the polygons
    if (Analyzed) then
      for i := Rooms.count - 1 downto 0 do
      begin
        Brush.style := bssolid;
        Room := TRoom(Rooms[i]);
        if (Room.RoomType = roomObstruction) then
        begin
          Brush.color := clLtGray;
          Brush.style := bssolid;
        end
        else if (Room.RoomType = roomLocal) then
        begin
          Brush.color := clLtGray;
          Brush.style := bssolid;
        end
        else
        begin
          Brush.color := clSilver;
          if Impress then
          begin
            Brush.style := bssolid;
          end
          else
            Brush.style := bsBDiagonal;
        end;
        if Brush.color = clNone then
          Brush.style := bsClear;
        Pen.color := Brush.color;
        SetLength(PointArray, Room.points.count);
        AverageX := 0;
        AverageY := 0;
        for j := 0 to Room.points.count - 1 do
        begin
          PointArray[j].X := (LTpoint(Room.points[j]).X + Center.X + Tx) div Zoom;
          PointArray[j].Y := (LTpoint(Room.points[j]).Y + Center.Y + Ty) div Zoom;
          if j <> 0 then
          begin
            AverageX := AverageX + PointArray[j].X;
            AverageY := AverageY + PointArray[j].Y
          end;
        end;
        // if FormSketch.CheckBoxZoning.checked then
        Polygon(PointArray);
        if Room.RoomType = roomObstruction then
        begin
          Brush.color := clgray;
          Brush.style := bssolid;
          AverageX := AverageX div (Room.points.count - 1);
          AverageY := AverageY div (Room.points.count - 1);
          Str(Room.Value1 : 0 : 2, chaine);
          Bidon := Labels[31] + chaine + Labels[32];
          AverageX := AverageX - TextWidth(Bidon) div 2;
          AverageY := AverageY - TextHeight(Bidon) div 2;
          Brush.style := bsClear;
          Font.Name := 'Arial';
          Font.Size := 8;
          TextOut(AverageX, AverageY, Bidon);
        end
        else if Room.RoomType = roomLocal then
        begin
          Brush.color := clgray;
          Brush.style := bssolid;
          Font.style := [fsBold];
          AverageX := AverageX div (Room.points.count - 1);
          AverageY := AverageY div (Room.points.count - 1);
          Bidon := Room.sur;
          AverageX := AverageX - TextWidth(Bidon) div 2;
          AverageY := AverageY - TextHeight(Bidon) div 2;
          Brush.style := bsClear;
          Font.Name := 'Arial';
          Font.Size := 8;
          TextOut(AverageX, AverageY - 16, Bidon);

          Bidon := Room.Ttau;
          Brush.style := bsClear;
          Font.Name := 'Arial';
          Font.Size := 8;
          TextOut(AverageX, AverageY, Bidon);

          Str(Room.Tau : 0 : 2, Bidon);
          Brush.style := bsClear;
          Font.Name := 'Arial';
          Font.Size := 8;
          TextOut(AverageX, AverageY + 16, 'Tau : ' + Bidon);

        end;

        AverageX := AverageX div (Room.points.count - 1);
        AverageY := AverageY div (Room.points.count - 1);
        Str(Room.Area : 0 : 2, Bidon);
        Str((Room.Area * Self.Height) : 0 : 2, Bidon2);
        AverageX := AverageX - TextWidth(Bidon + Labels[41]) div 2;
        AverageY := AverageY - TextHeight(Bidon + Labels[41]) div 2;
        Brush.color := clWhite;
        Font.color := clNavy;
        Font.Name := 'Arial';
        Font.Size := 8;
        Font.style := [fsBold];
        // if FormSketch.CheckBoxZoning.checked then
        Brush.style := bssolid;
        // else
        // brush.style := bsClear;
        if (Room.RoomType <> roomObstruction) and (Room.RoomType <> roomLocal) then
        begin
          Font.style := [fsBold];
          // if FormSketch.CheckRoomName.Checked then
          // TextOut(AverageX,AverageY-14,Room.Name);
          if cbRoomSurface then
            TextOut(AverageX, AverageY, Bidon + Labels[41]);
          if bRoomVolume then
            TextOut(AverageX, AverageY + 14, Bidon2 + Labels[243]);
          if TsketchObject(FatherObject).SpecialDisplay then
          begin
            Font.style := [];
            for kkk := 0 to Room.ListePlanchInt.count - 1 do
            begin
              AverageY := AverageY + 12;
              TextOut(AverageX, AverageY, Room.ListePlanchInt[kkk]);
            end;
          end;
        end;
        Font.color := clBlack;
        Font.style := [];
      end;
    Brush.style := bssolid;
    for i := 0 to segments.count - 1 do
    begin
      // Draw each segment
      Seg := LTSegment(segments[i]);
      Pen.style := psSolid;
      if Seg.Perimeter and Analyzed then
        Pen.Width := 2
      else
        Pen.Width := 1;
      if FormChg.visible and (Seg.Composition = FormChg.Compo) then
      begin
        Pen.color := ClRed;
        Pen.Width := 2;
      end
      else
      begin
        Pen.color := clTeal;
      end;
      // else
      // Pen.color := clAqua;
      x1 := (Seg.a.X + Center.X + Tx) div Zoom;
      y1 := (Seg.a.Y + Center.Y + Ty) div Zoom;
      x2 := (Seg.b.X + Center.X + Tx) div Zoom;
      y2 := (Seg.b.Y + Center.Y + Ty) div Zoom;
      MoveTo(x1, y1);
      LineTo(x2, y2);
      for porte in Seg.ListePortes do
      begin
        LPoint := LTpoint.Create(Point(0, 0));
        VecteurX := (Seg.b.X - Seg.a.X);
        VecteurY := (Seg.b.Y - Seg.a.Y);
        LPoint.X := Round((Seg.b.X * porte.Center) + (Seg.a.X * (1 - porte.Center)));
        LPoint.Y := Round((Seg.b.Y * porte.Center) + (Seg.a.Y * (1 - porte.Center)));

        Longueur := Sqrt(Sqr(VecteurX) + Sqr(VecteurY));
        if Longueur <> 0 then
        begin
          VecteurX := VecteurX / Longueur;
          VecteurY := VecteurY / Longueur;
        end
        else
        begin
          VecteurX := 1;
          VecteurY := 1;
        end;
        P1.X := Tx + Round(LPoint.X + porte.Largeur / 2 * scale * VecteurX);
        P1.Y := Ty + Round(LPoint.Y + porte.Largeur / 2 * scale * VecteurY);
        P2.X := Tx + Round(LPoint.X - porte.Largeur / 2 * scale * VecteurX);
        P2.Y := Ty + Round(LPoint.Y - porte.Largeur / 2 * scale * VecteurY);
        Pen.color := clMaroon;
        Pen.Width := 4;
        MoveTo((P1.X + Center.X) div Zoom, (P1.Y + Center.Y) div Zoom);
        LineTo((P2.X + Center.X) div Zoom, (P2.Y + Center.Y) div Zoom);
        LPoint.Free;
      end;
      for Fenetre in Seg.ListeFenetres do
      begin
        LPoint := LTpoint.Create(Point(0, 0));
        VecteurX := (Seg.b.X - Seg.a.X);
        VecteurY := (Seg.b.Y - Seg.a.Y);
        LPoint.X := Round((Seg.b.X * Fenetre.Center) + (Seg.a.X * (1 - Fenetre.Center)));
        LPoint.Y := Round((Seg.b.Y * Fenetre.Center) + (Seg.a.Y * (1 - Fenetre.Center)));

        Longueur := Sqrt(Sqr(VecteurX) + Sqr(VecteurY));
        if Longueur <> 0 then
        begin
          VecteurX := VecteurX / Longueur;
          VecteurY := VecteurY / Longueur;
        end
        else
        begin
          VecteurX := 1;
          VecteurY := 1;
        end;
        P1.X := Tx + Round(LPoint.X + Fenetre.Largeur / 2 * scale * VecteurX);
        P1.Y := Ty + Round(LPoint.Y + Fenetre.Largeur / 2 * scale * VecteurY);
        P2.X := Tx + Round(LPoint.X - Fenetre.Largeur / 2 * scale * VecteurX);
        P2.Y := Ty + Round(LPoint.Y - Fenetre.Largeur / 2 * scale * VecteurY);
        Pen.color := clBlue;
        Pen.Width := 4;
        MoveTo((P1.X + Center.X) div Zoom, (P1.Y + Center.Y) div Zoom);
        LineTo((P2.X + Center.X) div Zoom, (P2.Y + Center.Y) div Zoom);
        LPoint.Free;
      end;
      Pen.Width := 1;
      // Draw the length of the segment if required
      if TsketchObject(FatherObject).DisplayLength then
      begin
        // Vertical segment
        Brush.style := bsClear;
        Font.Name := 'Arial';
        Font.Size := 8;
        if Abs(x1 - x2) > Abs(y1 - y2) then
        begin
          x1 := (x1 + (x2 - x1) div 2);
          y1 := y1 + (y2 - y1) div 2 + 4;
          Str(Seg.Size(TsketchObject(FatherObject).scale) : 0 : 2, SegSize);
          if TsketchObject(FatherObject).SpecialDisplay then
          begin
            Brush.style := bssolid;
            TextOut(x1, y1, Seg.nom + ' ' + Seg.nomB);
            Brush.style := bsClear;
          end
          else
            TextOut(x1, y1, SegSize + ' m');
        end
        else
        // Horizontal Segment
        begin
          x1 := x1 + (x2 - x1) div 2 + 4;
          y1 := y1 + (y2 - y1) div 2;
          Str(Seg.Size(TsketchObject(FatherObject).scale) : 0 : 2, SegSize);
          if TsketchObject(FatherObject).SpecialDisplay then
          begin
            Brush.style := bssolid;
            if Seg.nomB <> '' then
              x1 := x1 - TextWidth(Seg.nom + ' ' + Seg.nomB) div 2;
            TextOut(x1, y1, Seg.nom + ' ' + Seg.nomB);
            Brush.style := bsClear;
          end
          else
            TextOut(x1, y1, SegSize + ' m');
        end;
      end;
    end;

    for i := 0 to points.count - 1 do
    begin
      // Draw each Points
      if Selection.IndexOf(points[i]) = -1 then
        Brush.color := clBlack
      else
        Brush.color := ClRed;
      Rect.Left := (LTpoint(points[i]).X + Center.X + Tx) div Zoom - 2;
      Rect.Top := (LTpoint(points[i]).Y + Center.Y + Ty) div Zoom - 2;
      Rect.Right := (LTpoint(points[i]).X + Center.X + Tx) div Zoom + 2;
      Rect.Bottom := (LTpoint(points[i]).Y + Center.Y + Ty) div Zoom + 2;
      FillRect(Rect);
      if DebugMode then
      begin
        Str(i, SegSize);
        TextOut(Rect.Left, Rect.Top, SegSize);
      end;
    end;
    if DrawLegend then
    begin
      Pen.color := clBlack;
      Pen.Width := 2;
      VecteurY := Cos(TsketchObject(FatherObject).OrientationNord / 180 * pi);
      VecteurX := -Sin(TsketchObject(FatherObject).OrientationNord / 180 * pi);
      MoveTo(Round(BufferBitmap.Width - 60 + VecteurX * 30), Round(50 + VecteurY * 30));
      LineTo(Round(BufferBitmap.Width - 60 - VecteurX * 30), Round(50 - VecteurY * 30));
      Brush.style := bsClear;
      Font.Size := 12;
      Font.style := BufferBitmap.Canvas.Font.style + [fsBold];
      TextOut(Round(BufferBitmap.Width - 60 - VecteurX * 42 - TextWidth('N') / 2),
        Round(50 - VecteurY * 42 - TextHeight('N') / 2), 'N');
      VecteurY2 := Cos(TsketchObject(FatherObject).OrientationNord / 180 * pi - 0.3);
      VecteurX2 := -Sin(TsketchObject(FatherObject).OrientationNord / 180 * pi - 0.3);
      MoveTo(Round(BufferBitmap.Width - 60 - VecteurX * 30), Round(50 - VecteurY * 30));
      LineTo(Round(BufferBitmap.Width - 60 - VecteurX2 * 25), Round(50 - VecteurY2 * 22));
      VecteurY2 := Cos(TsketchObject(FatherObject).OrientationNord / 180 * pi + 0.3);
      VecteurX2 := -Sin(TsketchObject(FatherObject).OrientationNord / 180 * pi + 0.3);
      MoveTo(Round(BufferBitmap.Width - 60 - VecteurX * 30), Round(50 - VecteurY * 30));
      LineTo(Round(BufferBitmap.Width - 60 - VecteurX2 * 25), Round(50 - VecteurY2 * 22));
      Pen.Width := 1;
      // BufferBitmap.Canvas.TextOut(BufferBitmap.Width-106,0,
      // 'N');
      // BufferBitmap.Canvas.Font.style := BufferBitmap.Canvas.Font.style-
      // [fsbold];
      // BufferBitmap.Canvas.Draw(BufferBitmap.Width-130,16,
      // FormSketch.RoseDesVents.Graphic);
    end;
  end;

end;

// ********************************************************************
// This function return the LTPoint that is under the given point
// if the LTpoint does not exist, return Nil
// *********************************************************************

function TSketchLevel.PointExists(Point : Tpoint) : LTpoint;
var
  i : Integer;
  Zoom : Integer;
  Center : Tpoint;
begin
  Center := TsketchObject(FatherObject).Center;
  Zoom := TsketchObject(FatherObject).Zoom;
  // Calculate absolute coordinate
  Point.X := (Point.X) - Center.X;
  Point.Y := (Point.Y) - Center.Y;
  PointExists := nil;
  // compare the coordinate of each LTPoint with the given point
  // using a tolerance (Click sensibility)
  for i := 0 to points.count - 1 do
    if (Abs(LTpoint(points[i]).X - Point.X) < DefaultClickSensibility * Zoom) and
      (Abs(LTpoint(points[i]).Y - Point.Y) < DefaultClickSensibility * Zoom) then
      PointExists := points[i];
end;

// ********************************************************************
// This function create a new segment and return it
// if the segment already exists, the function return the existing one
// if the segment has the same point as origin and destination it return nil
// *********************************************************************

function TSketchLevel.SegmentCreation(Orig, Dest : LTpoint) : LTSegment;
var
  AddSegment, Seg1 : LTSegment;
  NivPrec : TSketchLevel;
  i, j : Integer;
  P1, P2, p3, p4, p5, p6, p7, p8 : Integer;
  Part1, Part2, K1, K2 : Double;
  Croise : Boolean;
  InterSection : LTpoint;
  Center : Tpoint;
begin
  AddSegment := SegmentExists(Orig, Dest);
  if (AddSegment = nil) and (Orig <> Dest) then
  begin
    AddSegment := LTSegment.Create(Orig, Dest);
    segments.Add(AddSegment);
  end;
  SegmentCreation := AddSegment;
  CheckCross;
  Analyzed := False;

  if (AddSegment <> nil) and Toiture then
  begin
    NivPrec := TSketchLevel(project.SketchObject.Levels[project.SketchObject.CurrentLevel - 1]);
    for i := 0 to NivPrec.segments.count - 1 do
    begin
      Croise := False;
      Seg1 := LTSegment(NivPrec.segments[i]);

      p3 := (Seg1.a.X - AddSegment.a.X);
      P1 := (Seg1.a.Y - AddSegment.a.Y);
      p5 := (Seg1.b.X - Seg1.a.X);
      p6 := (Seg1.b.Y - Seg1.a.Y);
      P2 := (AddSegment.b.X - AddSegment.a.X);
      p4 := (AddSegment.b.Y - AddSegment.a.Y);
      p8 := (AddSegment.a.X - Seg1.a.X);
      p7 := (AddSegment.a.Y - Seg1.a.Y);
      Part1 := ((P1 * P2) - (p3 * p4));
      Part2 := ((p4 * p5) - (P2 * p6));
      if Part2 <> 0 then
        K1 := Part1 / Part2
      else if Part1 = 0 then
        K1 := 0
      else
        K1 := -1;

      Part1 := ((p7 * p5) - (p8 * p6));
      Part2 := ((p6 * P2) - (p5 * p4));
      if Part2 <> 0 then
        K2 := Part1 / Part2
      else if Part1 = 0 then
        K2 := 0
      else
        K2 := -1;

      if (K1 > 0) and (K1 < 1) and (K2 > 0) and (K2 < 1) then
        Croise := True;
      if Croise then
      begin
        InterSection := LTpoint.Create(Point(Round(Seg1.b.X * K1 + Seg1.a.X * (1 - K1)),
          Round(Seg1.b.Y * K1 + Seg1.a.Y * (1 - K1))));
        Center := TsketchObject(FatherObject).Center;
        NivPrec.PointCreation(Point(InterSection.X + Center.X, InterSection.Y + Center.Y));
        PointCreation(Point(InterSection.X + Center.X, InterSection.Y + Center.Y));
      end;
    end;
    NivPrec.Analyze;
  end
end;

// ********************************************************************
// This function return the segment that connect the two given points
// if the segment does not exists, it return Nil
// *********************************************************************

function TSketchLevel.SegmentExists(Orig, Dest : LTpoint) : LTSegment;
var
  i : Integer;
  Seg : LTSegment;
begin
  result := nil;
  for i := 0 to segments.count - 1 do
  begin
    Seg := LTSegment(segments[i]);
    if ((Seg.a = Orig) and (Seg.b = Dest))
      or ((Seg.b = Orig) and (Seg.a = Dest))
    then
      exit(Seg);
  end;
end;

// ********************************************************************
// This function return a segment that pass by the given point
// if the segment does not exist it returns Nil
// Segments that connect the given point are not taken into account
// *********************************************************************

function TSketchLevel.PointOnSegment(Poin : LTpoint) : LTSegment;
var
  Seg : LTSegment;
  i : Integer;
  XCross, YCross, P1, P2 : Double;
  Zoom : Integer;
  Point : LTpoint;
begin
  Point := LTpoint.Create(types.Point(Poin.X, Poin.Y));
  Zoom := TsketchObject(FatherObject).Zoom;
  PointOnSegment := nil;
  for i := 0 to segments.count - 1 do
  begin
    Seg := LTSegment(segments[i]);
    if (Seg.a <> Point) and (Seg.b <> Point) and ((Seg.a.X <> Seg.b.X) or (Seg.a.Y <> Seg.b.Y)) then
      // Horizontal
      if Abs(Seg.a.X - Seg.b.X) > Abs(Seg.a.Y - Seg.b.Y) then
      begin
        if ((Point.X >= Seg.a.X) and (Point.X <= Seg.b.X)) or ((Point.X >= Seg.b.X) and (Point.X <= Seg.a.X)) then
        begin
          P1 := (Point.X - Seg.a.X);
          P2 := (Seg.b.X - Point.X);
          YCross := ((P1 * Seg.b.Y) + (P2 * Seg.a.Y)) / (Seg.b.X - Seg.a.X);
          if Abs(Point.Y - YCross) < DefaultClickSensibility * Zoom then
          begin
            Point.Y := Round(YCross);
            PointOnSegment := Seg;
          end;
        end;
      end
      // Vertical
      else if (Seg.a.X <> Seg.b.X) or (Seg.a.Y <> Seg.b.Y) then
      begin
        if ((Point.Y >= Seg.a.Y) and (Point.Y <= Seg.b.Y)) or ((Point.Y >= Seg.b.Y) and (Point.Y <= Seg.a.Y)) then
        begin
          P1 := (Point.Y - Seg.a.Y);
          P2 := (Seg.b.Y - Point.Y);
          XCross := ((P1 * Seg.b.X) + (P2 * Seg.a.X)) / (Seg.b.Y - Seg.a.Y);
          if Abs(Point.X - XCross) < DefaultClickSensibility * Zoom then
          begin
            Point.X := Round(XCross);
            PointOnSegment := Seg;
          end;
        end;
      end
      else
      // Remove bad segments (a=b)
      begin
        segments.Remove(Seg);
        Seg.Free;
      end;

  end;
end;

function TSketchLevel.PointOnMenuiserie(Point : LTpoint; Tolerance : Integer) : LTSegment;
var
  Seg : LTSegment;
  i : Integer;
  XCross, YCross, P1, P2 : Double;
  Zoom : Integer;
begin
  Zoom := TsketchObject(FatherObject).Zoom;
  PointOnMenuiserie := nil;
  for i := 0 to segments.count - 1 do
  begin
    Seg := LTSegment(segments[i]);
    if (Seg.a <> Point) and (Seg.b <> Point) and ((Seg.a.X <> Seg.b.X) or (Seg.a.Y <> Seg.b.Y)) then
      // Horizontal
      if Abs(Seg.a.X - Seg.b.X) > Abs(Seg.a.Y - Seg.b.Y) then
      begin
        if ((Point.X >= Seg.a.X) and (Point.X <= Seg.b.X)) or ((Point.X >= Seg.b.X) and (Point.X <= Seg.a.X)) then
        begin
          P1 := (Point.X - Seg.a.X);
          P2 := (Seg.b.X - Point.X);
          YCross := ((P1 * Seg.b.Y) + (P2 * Seg.a.Y)) / (Seg.b.X - Seg.a.X);
          if Abs(Point.Y - YCross) < Tolerance * Zoom then
            PointOnMenuiserie := Seg;
        end;
      end
      // Vertical
      else if (Seg.a.X <> Seg.b.X) or (Seg.a.Y <> Seg.b.Y) then
      begin
        if ((Point.Y >= Seg.a.Y) and (Point.Y <= Seg.b.Y)) or ((Point.Y >= Seg.b.Y) and (Point.Y <= Seg.a.Y)) then
        begin
          P1 := (Point.Y - Seg.a.Y);
          P2 := (Seg.b.Y - Point.Y);
          XCross := ((P1 * Seg.b.X) + (P2 * Seg.a.X)) / (Seg.b.Y - Seg.a.Y);
          if Abs(Point.X - XCross) < Tolerance * Zoom then
            PointOnMenuiserie := Seg;
        end;
      end
      else
      // Remove bad segments (a=b)
      begin
        segments.Remove(Seg);
        Seg.Free;
      end;

  end;
end;


function TSketchLevel.PointOnSegmentVar(Point : LTpoint; Tolerance : Integer) : LTSegment;
var
  Seg : LTSegment;
  i : Integer;
  XCross, YCross, P1, P2 : Double;
  Zoom : Integer;
begin
  Zoom := TsketchObject(FatherObject).Zoom;
  PointOnSegmentVar := nil;
  for i := 0 to segments.count - 1 do
  begin
    Seg := LTSegment(segments[i]);
    if (Seg.a <> Point) and (Seg.b <> Point) and ((Seg.a.X <> Seg.b.X) or (Seg.a.Y <> Seg.b.Y)) then
      // Horizontal
      if Abs(Seg.a.X - Seg.b.X) > Abs(Seg.a.Y - Seg.b.Y) then
      begin
        if ((Point.X >= Seg.a.X) and (Point.X <= Seg.b.X)) or ((Point.X >= Seg.b.X) and (Point.X <= Seg.a.X)) then
        begin
          P1 := (Point.X - Seg.a.X);
          P2 := (Seg.b.X - Point.X);
          YCross := ((P1 * Seg.b.Y) + (P2 * Seg.a.Y)) / (Seg.b.X - Seg.a.X);
          if Abs(Point.Y - YCross) < Tolerance * Zoom then
            PointOnSegmentVar := Seg;
        end;
      end
      // Vertical
      else if (Seg.a.X <> Seg.b.X) or (Seg.a.Y <> Seg.b.Y) then
      begin
        if ((Point.Y >= Seg.a.Y) and (Point.Y <= Seg.b.Y)) or ((Point.Y >= Seg.b.Y) and (Point.Y <= Seg.a.Y)) then
        begin
          P1 := (Point.Y - Seg.a.Y);
          P2 := (Seg.b.Y - Point.Y);
          XCross := ((P1 * Seg.b.X) + (P2 * Seg.a.X)) / (Seg.b.Y - Seg.a.Y);
          if Abs(Point.X - XCross) < Tolerance * Zoom then
            PointOnSegmentVar := Seg;
        end;
      end
      else
      // Remove bad segments (a=b)
      begin
        segments.Remove(Seg);
        Seg.Free;
      end;

  end;
end;

function TSketchLevel.PointOnSegment_NoSensibility(Point : LTpoint) : LTSegment;
var
  Seg : LTSegment;
  i, XCross, YCross : Integer;
  Part1, Part2 : Int64;
begin
  PointOnSegment_NoSensibility := nil;
  if Point = nil then
    exit;
  for i := 0 to segments.count - 1 do
  begin
    Seg := LTSegment(segments[i]);
    if (Seg.a <> Point) and (Seg.b <> Point) and ((Seg.a.X <> Seg.b.X) or (Seg.a.Y <> Seg.b.Y)) then
      // Horizontal
      if Abs(Seg.a.X - Seg.b.X) > Abs(Seg.a.Y - Seg.b.Y) then
      begin
        if ((Point.X >= Seg.a.X) and (Point.X <= Seg.b.X)) or ((Point.X >= Seg.b.X) and (Point.X <= Seg.a.X)) then
        begin
          Part1 := ((Point.X - Seg.a.X) * Int64(Seg.b.Y));
          Part2 := ((Seg.b.X - Point.X) * Int64(Seg.a.Y));
          Part1 := Part1 + Part2;
          YCross := Part1 div (Seg.b.X - Seg.a.X);
          if Abs(Point.Y - YCross) < 1 then
            PointOnSegment_NoSensibility := Seg;
        end;
      end
      // Vertical
      else if (Seg.a.X <> Seg.b.X) or (Seg.a.Y <> Seg.b.Y) then
      begin
        if ((Point.Y >= Seg.a.Y) and (Point.Y <= Seg.b.Y)) or ((Point.Y >= Seg.b.Y) and (Point.Y <= Seg.a.Y)) then
        begin
          Part1 := ((Point.Y - Seg.a.Y) * Int64(Seg.b.X));
          Part2 := ((Seg.b.Y - Point.Y) * Int64(Seg.a.X));
          Part1 := Part1 + Part2;
          XCross := Part1 div (Seg.b.Y - Seg.a.Y);
          if Abs(Point.X - XCross) < 1 then
            PointOnSegment_NoSensibility := Seg;
        end;
      end
      else
      // Remove bad segments (a=b)
      begin
        segments.Remove(Seg);
        Seg.Free;
      end;

  end;
end;

// **********************************************************************
// This method move the points of the selection to the new location
// **********************************************************************

procedure TSketchLevel.MovePoints(Selection : TList; LastPoint : LTpoint; MouseUpPos : Tpoint);
var
  TempPoint, Point : LTpoint;
  i, j : Integer;
  Seg, Seg2 : LTSegment;
  Dx, Dy : Integer;
  Coordinate : Tpoint;
  Center : Tpoint;
begin
  Center := TsketchObject(FatherObject).Center;
  // Calculate the absolute coordinate of the moving vector
  Dx := (MouseUpPos.X - Center.X) - LastPoint.X;
  Dy := (MouseUpPos.Y - Center.Y) - LastPoint.Y;
  for j := Selection.count - 1 downto 0 do
  begin
    // Calculate the new coordinates for each point of the selection
    Point := LTpoint(Selection[j]);
    Coordinate.X := Point.X + Dx + Center.X;
    Coordinate.Y := Point.Y + Dy + Center.Y;

    // Is there already a point at this position ?
    TempPoint := PointExists(Coordinate);
    if (TempPoint <> nil) and (Selection.IndexOf(TempPoint) <> -1) then
      // YES, but it is a point of the selection so it is not taken into account
      TempPoint := nil;

    Coordinate.X := (Coordinate.X) - Center.X;
    Coordinate.Y := (Coordinate.Y) - Center.Y;
    if (TempPoint <> nil) and (TempPoint <> Point) then
    // YES, we need to join the 2 points into 1.
    begin
      // all the segments connected to the moved point
      // are connected to the existing point
      for i := segments.count - 1 downto 0 do
      begin
        Seg := LTSegment(segments[i]);
        if (Seg.a = Point) then
          Seg.a := TempPoint;
        if (Seg.b = Point) then
          Seg.b := TempPoint;
        // Delete bad segment
        if Seg.a = Seg.b then
        begin
          Seg.Free;
          segments.Delete(i)
        end;
      end;
      // Delete moved point
      points.Remove(Point);
      Selection.Remove(Point);
      // Replace the moved point by the existing point into the selection
      if Selection.IndexOf(TempPoint) = -1 then
        Selection.Add(TempPoint);
      Point.Free;
    end
    else
    // NO
    begin
      // Is there any segments at this position ?
      Point.X := Coordinate.X;
      Point.Y := Coordinate.Y;
      Seg := PointOnSegment(Point);

      if (Seg <> nil) and ((Selection.IndexOf(Seg.a) <> -1) or (Selection.IndexOf(Seg.b) <> -1)) then
        // YES, But it is connected to a point of the selection,
        // So it is not taken into account
        Seg := nil;
      if Seg <> nil then
      // YES, so the point cuts the segment into 2 parts
      begin
        Seg2 := LTSegment.Create(Seg.a, Point);
        Seg.a := Point;
        segments.Add(Seg2);
      end;
    end;
  end;
  // Sort the segments list to delete every segments that are identical
  segments.Sort(SortSegment);
  if segments.count <> 0 then
  begin
    Seg := LTSegment(segments[segments.count - 1]);
    for i := segments.count - 1 downto 1 do
    begin
      Seg2 := LTSegment(segments[i - 1]);
      if ((Seg2.a = Seg.a) and (Seg2.b = Seg.b)) or ((Seg2.a = Seg.b) and (Seg2.b = Seg.a)) then
      // The segment are identicals
      begin
        Seg.Free;
        segments.Delete(i);
      end;
      Seg := Seg2;
    end;
  end;
  // Additionnal verification
  CheckCross;
  Analyzed := False;
end;

// **********************************************************************
// This method apply a projection to the points selection
// **********************************************************************

procedure TSketchLevel.ProjectPoints(Selection : TList; Lastclick : Tpoint; MouseUpPos : Tpoint);
var
  TempPoint, Point, ProjectionCenter : LTpoint;
  i, j : Integer;
  Seg, Seg2 : LTSegment;
  Dx, Dy : Double;
  Coordinate : Tpoint;
  ProjectCoeff, AverageDistance : Double;
  Center : Tpoint;
begin
  Center := TsketchObject(FatherObject).Center;
  // Calculate the absolute coordinate of the projection center
  Lastclick.X := Lastclick.X - Center.X;
  Lastclick.Y := Lastclick.Y - Center.Y;
  ProjectionCenter := LTpoint.Create(Lastclick);

  // Calculate the average distance of the points of the selection
  // with the center of projection (D1)
  AverageDistance := 0;
  for i := 0 to Selection.count - 1 do
  begin
    Point := LTpoint(Selection[i]);
    Dx := Point.X - ProjectionCenter.X;
    Dy := Point.Y - ProjectionCenter.Y;
    AverageDistance := AverageDistance + Sqrt(Sqr(Dx) + Sqr(Dy));
  end;
  if Selection.count <> 0 then
    AverageDistance := AverageDistance / Selection.count
  else
    AverageDistance := 1;

  // Calculate the absolute coordinate of the vector of projection (D2)
  Dx := (MouseUpPos.X - Center.X) - Lastclick.X;
  Dy := (MouseUpPos.Y - Center.Y) - Lastclick.Y;

  // Calculate the projection coefficient (D2/D1)
  if AverageDistance <> 0 then
    ProjectCoeff := Sqrt(Sqr(Dx) + Sqr(Dy)) / AverageDistance
  else
    ProjectCoeff := 1;

  for j := Selection.count - 1 downto 0 do
  begin
    // Projection of each point of the selection
    Point := LTpoint(Selection[j]);
    Coordinate := Point.Projected(ProjectionCenter, ProjectCoeff);
    Coordinate.X := Coordinate.X + Center.X;
    Coordinate.Y := Coordinate.Y + Center.Y;

    // Test if there is already a point at this position
    TempPoint := PointExists(Coordinate);
    if (TempPoint <> nil) and (Selection.IndexOf(TempPoint) <> -1) then
      // YES, but it is a point of the selection, so we don't care
      TempPoint := nil;

    Coordinate.X := (Coordinate.X) - Center.X;
    Coordinate.Y := (Coordinate.Y) - Center.Y;
    if (TempPoint <> nil) and (TempPoint <> Point) then
    // YES, so we have to join the 2 points as 1 point
    begin
      // all the segments connected to the projected point
      // are connected to the existing point
      for i := segments.count - 1 downto 0 do
      begin
        Seg := LTSegment(segments[i]);
        if (Seg.a = Point) then
          Seg.a := TempPoint;
        if (Seg.b = Point) then
          Seg.b := TempPoint;
        // Delete bad segment
        if Seg.a = Seg.b then
        begin
          Seg.Free;
          segments.Delete(i)
        end;
      end;
      // Remove the unsefull point
      points.Remove(Point);
      Selection.Remove(Point);
      Point.Free;
    end
    else
    // NO
    begin
      // Is the point on an existing segment ?
      Point.X := Coordinate.X;
      Point.Y := Coordinate.Y;
      Seg := PointOnSegment(Point);
      if (Seg <> nil) and ((Selection.IndexOf(Seg.a) <> -1) or (Selection.IndexOf(Seg.b) <> -1)) then
        // YES but it is a segment of the selection, so we don't care
        Seg := nil;
      if Seg <> nil then
      // YES, so the point cut it into 2 parts
      begin
        Seg2 := LTSegment.Create(Seg.a, Point);
        Seg.a := Point;
        segments.Add(Seg2);
      end;
    end;
  end;
  // Sort of the segment to test duplicate
  segments.Sort(SortSegment);
  if segments.count <> 0 then
  begin
    Seg := LTSegment(segments[segments.count - 1]);
    for i := segments.count - 1 downto 1 do
    begin
      Seg2 := LTSegment(segments[i - 1]);
      if ((Seg2.a = Seg.a) and (Seg2.b = Seg.b)) or ((Seg2.a = Seg.b) and (Seg2.b = Seg.a)) then
      // this segments are identicals, so delete them
      begin
        Seg.Free;
        segments.Delete(i);
      end;
      Seg := Seg2;
    end
  end;
  // Additional verification
  CheckCross;
  Analyzed := False;
end;

// **********************************************************************
// This method apply a rotation to the points selection
// **********************************************************************

procedure TSketchLevel.RotatePoints(Selection : TList; Lastclick : Tpoint; MouseUpPos : Tpoint);
var
  TempPoint, Point, RotationCenter : LTpoint;
  i, j : Integer;
  Seg, Seg2 : LTSegment;
  Dx, Dy : Integer;
  Coordinate : Tpoint;
  Angle : Double;
  Center : Tpoint;
begin
  Center := TsketchObject(FatherObject).Center;
  // Calculate the absolute rotation center
  Lastclick.X := Lastclick.X - Center.X;
  Lastclick.Y := Lastclick.Y - Center.Y;
  RotationCenter := LTpoint.Create(Lastclick);

  // Calculate the absolute position of the mouse
  Dx := (MouseUpPos.X - Center.X) - Lastclick.X;
  Dy := (MouseUpPos.Y - Center.Y) - Lastclick.Y;

  // Calculate the angle of the vector with 0�
  if (Dx = 0) and (Dy = 0) then
    exit;
  Angle := GetAngle(Dx, Dy);

  for j := Selection.count - 1 downto 0 do
  begin
    // Rotate each point of the selection
    Point := LTpoint(Selection[j]);
    Coordinate := Point.Rotated(RotationCenter, Angle);
    Coordinate.X := Coordinate.X + Center.X;
    Coordinate.Y := Coordinate.Y + Center.Y;

    // Is the point already exists ?
    TempPoint := PointExists(Coordinate);
    if (TempPoint <> nil) and (Selection.IndexOf(TempPoint) <> -1) then
      // YES, but it is a point of the selection, so we don't care
      TempPoint := nil;

    Coordinate.X := (Coordinate.X) - Center.X;
    Coordinate.Y := (Coordinate.Y) - Center.Y;

    if (TempPoint <> nil) and (TempPoint <> Point) then
    // YES, so the points will become one point
    begin
      for i := segments.count - 1 downto 0 do
      begin
        // Each segment connected to the rotated point
        // will be connect to the existing point instead
        Seg := LTSegment(segments[i]);
        if (Seg.a = Point) then
          Seg.a := TempPoint;
        if (Seg.b = Point) then
          Seg.b := TempPoint;

        // Remove bad segment
        if Seg.a = Seg.b then
        begin
          Seg.Free;
          segments.Delete(i)
        end;
      end;
      // Remove the unusefull point
      points.Remove(Point);
      Selection.Remove(Point);
      Point.Free;
    end
    else
    // NO
    begin
      // Is there a segment under the point ?
      Point.X := Coordinate.X;
      Point.Y := Coordinate.Y;
      Seg := PointOnSegment(Point);
      if (Seg <> nil) and ((Selection.IndexOf(Seg.a) <> -1) or (Selection.IndexOf(Seg.b) <> -1)) then
        // YES but it is connected to a point of the selection
        // Do we don't care
        Seg := nil;
      if Seg <> nil then
      // YES, so the point cuts the segment into 2 parts
      begin
        Seg2 := LTSegment.Create(Seg.a, Point);
        Seg.a := Point;
        segments.Add(Seg2);
      end;
    end;
  end;
  // Sort the segment list to easyly find duplicate ones
  segments.Sort(SortSegment);
  if segments.count <> 0 then
  begin
    Seg := LTSegment(segments[segments.count - 1]);
    for i := segments.count - 1 downto 1 do
    begin
      Seg2 := LTSegment(segments[i - 1]);
      if ((Seg2.a = Seg.a) and (Seg2.b = Seg.b)) or ((Seg2.a = Seg.b) and (Seg2.b = Seg.a)) then
      // The segments are identical, so we delete one of them
      begin
        Seg.Free;
        segments.Delete(i);
      end;
      Seg := Seg2;
    end
  end;
  // Addintionnal verification
  CheckCross;
  Analyzed := False;
end;

// **********************************************************************
// This method check all points and segments of the level to find any
// cross between points and lines and to delete any duplicate data
// **********************************************************************

procedure TSketchLevel.CheckCross;
var
  i, j : Integer;
  P1, P2 : LTpoint;
  Seg, Seg2 : LTSegment;
begin
  // Sort the point by position to find duplicated ones
  points.Sort(SortPoint);
  for i := points.count - 1 downto 1 do
  begin
    P1 := LTpoint(points[i]);
    P2 := LTpoint(points[i - 1]);
    if (P1.X = P2.X) and (P1.Y = P2.Y) then
    // the points are identical, so we have to delete one of them
    begin
      for j := segments.count - 1 downto 0 do
      begin
        // each segment connected to the first point
        // is connected to the second one
        Seg := LTSegment(segments[j]);
        if Seg.a = P1 then
          Seg.a := P2;
        if Seg.b = P1 then
          Seg.b := P2;
      end;
      // Delete the first point
      project.SketchObject.Selection.Remove(P1);
      points.Remove(P1);
      P1.Free;
    end;
  end;
  // Sort the segment to easily find duplicates ones
  segments.Sort(SortSegment);
  for i := segments.count - 1 downto 1 do
  begin
    Seg := LTSegment(segments[i]);
    Seg2 := LTSegment(segments[i - 1]);
    if (Seg.a = Seg.b) or ((Seg.a = Seg2.a) and (Seg.b = Seg2.b)) or ((Seg.a = Seg2.b) and (Seg.b = Seg2.a)) then
    // The segment are identicals, so we have to delete one of them
    begin
      segments.Remove(Seg);
      Seg.Free;
    end;
  end;

  // Check if points cross segments
  for i := 0 to points.count - 1 do
  begin
    P1 := LTpoint(points[i]);
    if P1 = nil then
      continue;
    Seg := PointOnSegment_NoSensibility(P1);
    while Seg <> nil do
    begin
      // Each segment that is crossed by the point
      // is cut into 2 parts by the points
      Seg2 := LTSegment.Create(Seg.a, P1);
      Seg.a := P1;
      segments.Add(Seg2);
      Seg := PointOnSegment_NoSensibility(P1);
    end;
  end;

  // Sort the segment to easily find duplicates ones
  segments.Sort(SortSegment);
  for i := segments.count - 1 downto 1 do
  begin
    Seg := LTSegment(segments[i]);
    Seg2 := LTSegment(segments[i - 1]);
    if (Seg.a = Seg.b) or ((Seg.a = Seg2.a) and (Seg.b = Seg2.b)) or ((Seg.a = Seg2.b) and (Seg.b = Seg2.a)) then
    // The segment are identicals, so we have to delete one of them
    begin
      segments.Remove(Seg);
      Seg.Free;
    end;
  end;
end;

procedure TSketchLevel.Analyze;
begin
  ActiveThread := True;
  AnalysisThread := TCutThread.Create(points, segments, Perimeters, Rooms, VirtualPoints, VirtualSegments, Height);
  AnalysisThread.execute;
  project.SketchObject.FinishAnalyze(nil);
  Analyzed := (AnalysisThread.Error = 0);
  AnalysisThread.Free;
end;


function TSketchLevel.GetAngleLT(P1, P2 : LTpoint) : Double;
var
  X, Y, Angle1, Angle2, SegLength : Double;
begin
  X := P2.X - P1.X;
  Y := P2.Y - P1.Y;
  SegLength := Sqrt(Sqr(X) + Sqr(Y));
  X := X / SegLength;
  Y := Y / SegLength;
  Angle1 := ArcCos(X);
  Angle2 := ArcSin(Y);
  if Angle2 <= 0 then
    result := Angle1
  else
    result := (2 * pi) - Angle1;
end;


procedure TSketchLevel.SetExternalSpace(MouseDownPOs : Tpoint);
var
  LastPoint : LTpoint;
  i : Integer;
  Center : Tpoint;
  r : TRoom;
begin
  Center := TsketchObject(FatherObject).Center;
  MouseDownPOs.X := MouseDownPOs.X - Center.X;
  MouseDownPOs.Y := MouseDownPOs.Y - Center.Y;
  LastPoint := LTpoint.Create(MouseDownPOs);
  for i := 0 to Rooms.count - 1 do
  begin
    r := TRoom(Rooms[i]);
    if PointInOut(LastPoint, r.points) and (r.RoomType <> roomExternal) then
    begin
      r.RoomType := roomExternal;
//      r.ZoneNumber := -1;
      r.Value1 := 0;
      r.Value2 := 1;
    end;
  end;
  LastPoint.Free;
end;

procedure TSketchLevel.SetLocal(MouseDownPOs : Tpoint);
var
  LastPoint : LTpoint;
  i : Integer;
  Center : Tpoint;
  r : TRoom;
begin
  Center := TsketchObject(FatherObject).Center;
  MouseDownPOs.X := MouseDownPOs.X - Center.X;
  MouseDownPOs.Y := MouseDownPOs.Y - Center.Y;
  LastPoint := LTpoint.Create(MouseDownPOs);
  for i := 0 to Rooms.count - 1 do
  begin
    r := TRoom(Rooms[i]);
    if PointInOut(LastPoint, r.points) and (r.RoomType <> roomExternal) then
    begin
      FormLocal.setRoom(r);
      if FormLocal.ShowModal = mrOk then
      begin
        r.RoomType := roomLocal;
        r.sur := FormLocal.ComboSur.text;
        r.Ttau := FormLocal.ComboTau.text;
        r.Tau := getDouble(FormLocal.EditTau.text);
      end;
    end;
  end;
  LastPoint.Free;
end;

function TSketchLevel.SetRoof(MouseDownPOs : Tpoint) : Boolean;
var
  Center : Tpoint;
  LastPoint : LTpoint;
  i, j, k : Integer;
  Reponse : Word;
  H1, H2, H3 : Double;
  i1, i2, i3, Erreur : Integer;
  M1, M2, M3 : TP3D;
  a, b, C, D : Double;
  Room : TRoom;
  P : LTpoint;
begin
  Center := TsketchObject(FatherObject).Center;
  SetRoof := False;
  MouseDownPOs.X := MouseDownPOs.X - Center.X;
  MouseDownPOs.Y := MouseDownPOs.Y - Center.Y;
  LastPoint := LTpoint.Create(MouseDownPOs);
  for i := 0 to Rooms.count - 1 do
    if PointInOut(LastPoint, TRoom(Rooms[i]).points) then
    begin
      Room := TRoom(Rooms[i]);
      FormRoof.ComboPoint1.Items.Clear;
      FormRoof.ComboPoint2.Items.Clear;
      FormRoof.ComboPoint3.Items.Clear;
      for j := 0 to points.count - 1 do
      begin
        FormRoof.ComboPoint1.Items.Add(Chr(Ord('A') + j));
        FormRoof.ComboPoint2.Items.Add(Chr(Ord('A') + j));
        FormRoof.ComboPoint3.Items.Add(Chr(Ord('A') + j));
      end;
      FormRoof.BitBtn1.Enabled := False;
      FormRoof.ComboPoint1.itemIndex := -1;
      FormRoof.ComboPoint2.itemIndex := -1;
      FormRoof.ComboPoint3.itemIndex := -1;
      Reponse := FormRoof.ShowModal;
      if Reponse = mrOk then
      begin
        Val(FormRoof.Height1.text, H1, Erreur);
        Val(FormRoof.Height2.text, H2, Erreur);
        Val(FormRoof.Height3.text, H3, Erreur);
        i1 := FormRoof.ComboPoint1.itemIndex;
        i2 := FormRoof.ComboPoint2.itemIndex;
        i3 := FormRoof.ComboPoint3.itemIndex;

        M1.X := LTpoint(Room.points[i1]).X;
        M1.Y := LTpoint(Room.points[i1]).Y;
        M1.z := H1;

        M2.X := LTpoint(Room.points[i2]).X;
        M2.Y := LTpoint(Room.points[i2]).Y;
        M2.z := H2;

        M3.X := LTpoint(Room.points[i3]).X;
        M3.Y := LTpoint(Room.points[i3]).Y;
        M3.z := H3;

        a := M2.Y * (M3.z - M1.z) - M1.Y * M3.z - M2.z * M3.Y + M2.z * M1.Y + M1.z * M3.Y;
        b := -M2.X * M3.z + M2.X * M1.z + M1.X * M3.z + M2.z * M3.X - M2.z * M1.X - M1.z * M3.X;
        C := M2.X * M3.Y - M2.X * M1.Y - M1.X * M3.Y - M2.Y * M3.X + M2.Y * M1.X + M1.Y * M3.X;
        D := -M1.X * a - M1.Y * b - M1.z * C;

        for k := 0 to Room.points.count - 1 do
        begin
          P := LTpoint(Room.points[k]);
          Room.HautPoint[k] := (-a * P.X - b * P.Y - D) / C;
        end;
      end;

    end
end;

function TSketchLevel.SetZone(MouseDownPOs : Tpoint) : Boolean;
var
  LastPoint : LTpoint;
  i : Integer;
  InputString : string;
  Center : Tpoint;
  Reponse : Word;
  r : TRoom;

begin
  Center := TsketchObject(FatherObject).Center;
  SetZone := False;
  InputString := '0';
  MouseDownPOs.X := MouseDownPOs.X - Center.X;
  MouseDownPOs.Y := MouseDownPOs.Y - Center.Y;
  LastPoint := LTpoint.Create(MouseDownPOs);
  for i := 0 to Rooms.count - 1 do
  begin
    r := TRoom(Rooms[i]);
    if PointInOut(LastPoint, r.points) then
    begin
      FormRoom.EditName.text := r.Name;
      FormRoom.Room := r;
      if r.CompoFloor <> nil then
        FormRoom.StaticCompoFloor.Caption := r.CompoFloor.nom
      else
        FormRoom.StaticCompoFloor.Caption := Labels[234];
      if r.RoomType = roomNormal then
        FormRoom.RadioEtudie.Checked := True
      else if r.RoomType = roomLocal then
      begin
        FormRoom.RadioLocal.Checked := True;
        FormRoom.ComboSur.itemIndex := FormRoom.ComboSur.Items.IndexOf(r.sur);
        FormRoom.ComboSurChange(nil);
        FormRoom.ComboTau.itemIndex := FormRoom.ComboTau.Items.IndexOf(r.Ttau);
        if FormRoom.ComboTau.itemIndex = -1 then
          FormRoom.ComboTau.itemIndex := 0;
        FormRoom.ComboTauChange(nil);
      end
      else if r.RoomType = roomExternal then
        FormRoom.RAdioPatio.Checked := True;
      FormRoom.RadioEtudieClick(nil);
      if r.RoomType <> roomObstruction then
      begin
        Reponse := FormRoom.ShowModal;
        if Reponse = mrOk then
        begin
          if FormRoom.RadioEtudie.Checked then
          begin
            r.RoomType := roomNormal;
          end
          else if FormRoom.RAdioPatio.Checked then
          begin
            r.RoomType := roomExternal;
//            r.ZoneNumber := -1;
            r.Value1 := 0;
            r.Value2 := 1;
          end
          else if FormRoom.RadioLocal.Checked then
          begin
            r.RoomType := roomLocal;
            r.sur := FormRoom.ComboSur.text;
            r.Ttau := FormRoom.ComboTau.text;
            r.Tau := getDouble(FormRoom.EditTau.text);
          end;
        end;
      end;
    end;
  end;
  LastPoint.Free;
end;

function TSketchLevel.SetShading(MouseDownPOs : Tpoint) : Boolean;
var
  LastPoint : LTpoint;
  i : Integer;
  InputString : string;
  Value1 : Double;
  StringError : Integer;
  Center : Tpoint;
begin
  Center := TsketchObject(FatherObject).Center;
  SetShading := False;
  InputString := '0';
  StringError := 1;
  MouseDownPOs.X := MouseDownPOs.X - Center.X;
  MouseDownPOs.Y := MouseDownPOs.Y - Center.Y;
  LastPoint := LTpoint.Create(MouseDownPOs);
  for i := 0 to Rooms.count - 1 do
    if PointInOut(LastPoint, TRoom(Rooms[i]).points) then
    begin
      if TRoom(Rooms[i]).RoomType = roomObstruction then
        InputString := FloatToStr(TRoom(Rooms[i]).Value1);
      Value1 := 0;
      while StringError <> 0 do
      begin
        if not InputQuery(Labels[24], Labels[24], InputString) then
          exit;
        SetShading := True;
        Val(InputString, Value1, StringError);
      end;
      TRoom(Rooms[i]).RoomType := roomObstruction;
      TRoom(Rooms[i]).Value1 := Value1;
    end;
  LastPoint.Free;
end;

procedure TSketchLevel.SetNone(MouseDownPOs : Tpoint);
var
  LastPoint : LTpoint;
  i : Integer;
  Center : Tpoint;
begin
  Center := TsketchObject(FatherObject).Center;
  MouseDownPOs.X := MouseDownPOs.X - Center.X;
  MouseDownPOs.Y := MouseDownPOs.Y - Center.Y;
  LastPoint := LTpoint.Create(MouseDownPOs);
  for i := 0 to Rooms.count - 1 do
    if PointInOut(LastPoint, TRoom(Rooms[i]).points) then
    begin
      TRoom(Rooms[i]).RoomType := roomNormal;
    end;
  LastPoint.Free;
end;

function TSketchLevel.SetWall(MouseDownPOs : Tpoint) : Boolean;
var
  LastPoint : LTpoint;
  i, P1, P2 : Integer;
  Seg : LTSegment;
  Reponse : Word;
  TempString : string;
  Value, CAngle : Double;
  Error : Integer;
  isObstructionSeg : Boolean;
  Room : TRoom;
  Center : Tpoint;
begin
  Room := nil;
  Center := TsketchObject(FatherObject).Center;
  SetWall := False;
  MouseDownPOs.X := MouseDownPOs.X - Center.X;
  MouseDownPOs.Y := MouseDownPOs.Y - Center.Y;
  LastPoint := LTpoint.Create(MouseDownPOs);
  Seg := PointOnSegment(LastPoint);

  if Seg = nil then
    exit;
  // isBufferSeg := False;
  isObstructionSeg := False;
  for i := 0 to Rooms.count - 1 do
  begin
    Room := TRoom(Rooms[i]);
    if (Room.RoomType = roomObstruction) and (Room.points.IndexOf(Seg.a) <> -1) and (Room.points.IndexOf(Seg.b) <> -1) and
      ((Abs(Room.points.IndexOf(Seg.a) - Room.points.IndexOf(Seg.b)) = 1) or
      (Abs(Room.points.IndexOf(Seg.a) - Room.points.IndexOf(Seg.b))
      = Room.points.count - 2)) then
    begin
      isObstructionSeg := True;
      Break;
    end;
    if (Room.RoomType = roomBuffer) and (Room.points.IndexOf(Seg.a) <> -1) and (Room.points.IndexOf(Seg.b) <> -1) and
      ((Abs(Room.points.IndexOf(Seg.a) - Room.points.IndexOf(Seg.b)) = 1) or
      (Abs(Room.points.IndexOf(Seg.a) - Room.points.IndexOf(Seg.b))
      = Room.points.count - 2)) then
    begin
      // IsBufferSeg := true;
      Break
    end
  end;
  if Seg.adjacent then
  begin
    P1 := Room.points.IndexOf(Seg.a);
    P2 := Room.points.IndexOf(Seg.b);
    if not(P1 - P2 = -1) or (P1 - P2 = Room.points.count - 2) then
    begin
      CAngle := T3DUtil.Angle(Seg.a, Seg.b);
      Seg.Orientation := CAngle / pi * 180 + 180
    end
    else
    begin
      CAngle := T3DUtil.Angle(Seg.b, Seg.a);
      Seg.Orientation := CAngle / pi * 180 + 180
    end
  end;
  if isObstructionSeg then
    exit;
  begin
    TempString := getString(Seg.Size(project.SketchObject.scale));
    FormInputWall.EditSize.text := TempString;
    TempString := getString(Seg.Orientation - project.SketchObject.OrientationNord, 0);
    FormInputWall.StaticText1.Caption := TempString;
    FormInputWall.Seg := Seg;
    FormInputWall.Level := Self;
    FormInputWall.TempShadings := Seg.Shadings;

    FormInputWall.ShowModal;
    // FormInputWall.FormShow(Self);
    if Seg.Composition = nil then
      FormInputWall.StaticComposition.Caption := Labels[234]
    else
      FormInputWall.StaticComposition.Caption := Seg.Composition.nom;


    Reponse := FormInputWall.ShowModal;
    if Reponse = mrOk then
    begin
      Val(FormInputWall.EditSize.text, Value, Error);
      project.SketchObject.scale := project.SketchObject.scale *
        Seg.Size(project.SketchObject.scale) / Value;
      SetWall := True;
      Seg.Shadings := FormInputWall.TempShadings;
    end
  end;
  LastPoint.Free;
end;

function TSketchLevel.SetCompo(MouseDownPOs : Tpoint) : Boolean;
var
  LastPoint : LTpoint;
  Seg : LTSegment;
  Center : Tpoint;
begin
  result := False;
  Center := TsketchObject(FatherObject).Center;
  MouseDownPOs.X := MouseDownPOs.X - Center.X;
  MouseDownPOs.Y := MouseDownPOs.Y - Center.Y;
  LastPoint := LTpoint.Create(MouseDownPOs);
  Seg := PointOnSegment(LastPoint);
  LastPoint.Free;
  if Seg = nil then
    exit;
  Seg.Composition := FormChg.Compo;
end;

function TSketchLevel.GetWall(MouseDownPOs : Tpoint) : TStringList;
var
  LastPoint : LTpoint;
  i : Integer;
  Seg : LTSegment;
  TempString : string;
  IsBufferSeg, isObstructionSeg : Boolean;
  Room : TRoom;
  Center : Tpoint;
begin
  Room := nil;
  Center := TsketchObject(FatherObject).Center;
  GetWall := TStringList.Create;
  MouseDownPOs.X := MouseDownPOs.X - Center.X;
  MouseDownPOs.Y := MouseDownPOs.Y - Center.Y;
  LastPoint := LTpoint.Create(MouseDownPOs);
  Seg := PointOnSegment(LastPoint);
  if Seg = nil then
    exit;
  IsBufferSeg := False;
  isObstructionSeg := False;
  for i := 0 to Rooms.count - 1 do
  begin
    Room := TRoom(Rooms[i]);
    if (Room.RoomType = roomObstruction) and (Room.points.IndexOf(Seg.a) <> -1) and (Room.points.IndexOf(Seg.b) <> -1) and
      ((Abs(Room.points.IndexOf(Seg.a) - Room.points.IndexOf(Seg.b)) = 1) or
      (Abs(Room.points.IndexOf(Seg.a) - Room.points.IndexOf(Seg.b))
      = Room.points.count - 2)) then
    begin
      isObstructionSeg := True;
      Break;
    end;
    if (Room.RoomType = roomBuffer) and (Room.points.IndexOf(Seg.a) <> -1) and (Room.points.IndexOf(Seg.b) <> -1) and
      ((Abs(Room.points.IndexOf(Seg.a) - Room.points.IndexOf(Seg.b)) = 1) or
      (Abs(Room.points.IndexOf(Seg.a) - Room.points.IndexOf(Seg.b))
      = Room.points.count - 2)) then
    begin
      IsBufferSeg := True;
      Break
    end
  end;
  if isObstructionSeg then
  begin
    GetWall.Add(ZoneOrientation[12]);
    GetWall.Add(Labels[31] + FloatToStr(Room.Value1) + ' m');
    exit;
  end;
  begin
    if IsBufferSeg and Seg.Perimeter then
      GetWall.Add(Labels[48])
    else if IsBufferSeg then
      GetWall.Add(Labels[219])
    else if Seg.Perimeter then
      GetWall.Add(Labels[43])
    else
      GetWall.Add(Labels[58]);

    TempString := getString(Seg.Size(project.SketchObject.scale));
    GetWall.Add(Labels[46] + ' ' + TempString + ' m');
    TempString := getString(Seg.Orientation - project.SketchObject.OrientationNord, 0);
    GetWall.Add(Labels[47] + ' ' + TempString + ' �');
  end;
  if Seg.Composition <> nil then
  begin
    GetWall.Add(Labels[242]);
    GetWall.Add(Seg.Composition.nom);
  end;
  if Seg.Casquette <> 0 then
  begin
    TempString := getString(Seg.Casquette);
    GetWall.Add(Labels[259] + ' ' + TempString + 'm');
    TempString := getString(Seg.Distance);
    GetWall.Add(Labels[260] + ' ' + TempString + 'm');
  end;
  LastPoint.Free;
end;

procedure TSketchLevel.FlashCurve(Segment : LTSegment; X, Y : Integer; SBitmap : Tbitmap);
var
  v1, v2, v3 : TDPoint; // Vectors
  PointList : TList;
  Current : TDPoint; // Point;
  SegCenter : LTpoint;
  i : Integer;
  Zoom : Integer;
  Center : Tpoint;
begin
  Center := TsketchObject(FatherObject).Center;
  Zoom := TsketchObject(FatherObject).Zoom;
  X := X - Center.X;
  Y := Y - Center.Y;
  SegCenter := LTpoint.Create(Point((Segment.a.X + Segment.b.X) div 2, (Segment.a.Y + Segment.b.Y) div 2));

  X := (X - SegCenter.X) * 2 + SegCenter.X;
  Y := (Y - SegCenter.Y) * 2 + SegCenter.Y;
  PointList := TList.Create;
  SBitmap.Assign(BufferBitmap);
  with SBitmap.Canvas do
  begin
    Pen.color := ClRed;
    Brush.color := ClRed;
    Pen.style := psDot;
    Brush.style := bsClear;
    Brush.color := clWhite;

    v1.X := (X - Segment.a.X);
    v1.Y := (Y - Segment.a.Y);
    v2.X := (Segment.b.X - Segment.a.X);
    v2.Y := (Segment.b.Y - Segment.a.Y);
    v3.X := (Segment.b.X - X);
    v3.Y := (Segment.b.Y - Y);

    Current.X := Segment.a.X;
    Current.Y := Segment.a.Y;
    MoveTo(Round(Current.X + Center.X) div Zoom, Round(Current.Y + Center.Y) div Zoom);
    for i := 0 to 3 do
    begin
      Current.X := Current.X + Round((4 - i) * v1.X / 25) + Round(i * v2.X / 25);
      Current.Y := Current.Y + Round((4 - i) * v1.Y / 25) + Round(i * v2.Y / 25);
      LineTo(Round(Current.X + Center.X) div Zoom, Round(Current.Y + Center.Y) div Zoom);
    end;
    Current.X := Segment.b.X;
    Current.Y := Segment.b.Y;
    MoveTo(Round(Current.X + Center.X) div Zoom, Round(Current.Y + Center.Y) div Zoom);
    for i := 0 to 3 do
    begin
      Current.X := Current.X - Round((4 - i) * v3.X / 25) - Round(i * v2.X / 25);
      Current.Y := Current.Y - Round((4 - i) * v3.Y / 25) - Round(i * v2.Y / 25);
      LineTo(Round(Current.X + Center.X) div Zoom, Round(Current.Y + Center.Y) div Zoom);
    end;
  end;
  PointList.Free;
end;

procedure TSketchLevel.CurvePoints(Segment : LTSegment; X, Y : Integer; SBitmap : Tbitmap);
var
  v1, v2, v3 : TDPoint; // Vectors
  PointList : TList;
  Current : TDPoint; // Point;
  i : Integer;
  Previous, Previous2, P, SegCenter : LTpoint;
  TempSeg : LTSegment;
  Center : Tpoint;
begin
  Center := TsketchObject(FatherObject).Center;
  for i := segments.count - 1 downto 0 do
    if segments[i] = Segment then
    begin
      segments.Delete(i);
    end;
  X := X - Center.X;
  Y := Y - Center.Y;
  SegCenter := LTpoint.Create(Point((Segment.a.X + Segment.b.X) div 2, (Segment.a.Y + Segment.b.Y) div 2));
  X := (X - SegCenter.X) * 2 + SegCenter.X;
  Y := (Y - SegCenter.Y) * 2 + SegCenter.Y;

  PointList := TList.Create;
  SBitmap.Assign(BufferBitmap);
  with SBitmap.Canvas do
  begin
    Pen.color := ClRed;
    Brush.color := ClRed;
    Pen.style := psDot;
    Brush.style := bsClear;
    Brush.color := clWhite;

    v1.X := X - Segment.a.X;
    v1.Y := Y - Segment.a.Y;
    v2.X := Segment.b.X - Segment.a.X;
    v2.Y := Segment.b.Y - Segment.a.Y;
    v3.X := Segment.b.X - X;
    v3.Y := Segment.b.Y - Y;

    Current.X := Segment.a.X;
    Current.Y := Segment.a.Y;
    Previous := Segment.a;
    for i := 0 to 3 do
    begin
      Current.X := Current.X + Round((4 - i) * v1.X / 25) + Round(i * v2.X / 25);
      Current.Y := Current.Y + Round((4 - i) * v1.Y / 25) + Round(i * v2.Y / 25);
      P := PointCreation(Point(Round(Current.X + Center.X), Round(Current.Y + Center.Y)));
      TempSeg := SegmentCreation(Previous, P);
      if TempSeg <> nil then
      begin
        TempSeg.PercentageGlazing := Segment.PercentageGlazing;
        TempSeg.NumberGlazing := Segment.NumberGlazing;
        TempSeg.GlazingColor := Segment.GlazingColor;
        TempSeg.Shadings := Segment.Shadings;
      end;
      Previous := P;
    end;
    Current.X := Segment.b.X;
    Current.Y := Segment.b.Y;
    Previous2 := Segment.b;
    Pen.color := clGreen;
    for i := 0 to 3 do
    begin
      Current.X := Current.X - Round((4 - i) * v3.X / 25) - Round(i * v2.X / 25);
      Current.Y := Current.Y - Round((4 - i) * v3.Y / 25) - Round(i * v2.Y / 25);
      P := PointCreation(Point(Round(Current.X + Center.X), Round(Current.Y + Center.Y)));
      TempSeg := SegmentCreation(Previous2, P);
      if TempSeg <> nil then
      begin
        TempSeg.PercentageGlazing := Segment.PercentageGlazing;
        TempSeg.NumberGlazing := Segment.NumberGlazing;
        TempSeg.GlazingColor := Segment.GlazingColor;
        TempSeg.Shadings := Segment.Shadings;
      end;
      Previous2 := P;
    end;
    TempSeg := SegmentCreation(Previous, Previous2);
    if TempSeg <> nil then
    begin
      TempSeg.PercentageGlazing := Segment.PercentageGlazing;
      TempSeg.NumberGlazing := Segment.NumberGlazing;
      TempSeg.GlazingColor := Segment.GlazingColor;
      TempSeg.Shadings := Segment.Shadings
    end;
  end;
  Segment.Free;
  CheckCross;
  PointList.Free;
end;

function TSketchLevel.GetZone(MouseDownPOs : Tpoint) : TStringList;
var
  LastPoint : LTpoint;
  i : Integer;
  Room : TRoom;
  TempString : string;
  Center : Tpoint;
begin
  Center := TsketchObject(FatherObject).Center;
  GetZone := TStringList.Create;
  MouseDownPOs.X := MouseDownPOs.X - Center.X;
  MouseDownPOs.Y := MouseDownPOs.Y - Center.Y;
  LastPoint := LTpoint.Create(MouseDownPOs);
  Room := nil;
  for i := 0 to Rooms.count - 1 do
    if PointInOut(LastPoint, TRoom(Rooms[i]).points) then
      Room := TRoom(Rooms[i]);
  if (Room <> nil) and (Room.RoomType <> roomExternal) then
  begin
    GetZone.Add(Room.Name);
    TempString := getString(Room.Area);
    GetZone.Add(Labels[85] + ' : ' + TempString + ' m2');
    if Room.CompoFloor <> nil then
      GetZone.Add(Labels[261] + ' : ' + Room.CompoFloor.nom);
    if Room.CompoCeiling <> nil then
      GetZone.Add(Labels[262] + ' : ' + Room.CompoCeiling.nom);
  end;
  LastPoint.Free;
end;

procedure TSketchLevel.SaveMeta(GridType, IdLevel : Integer ; fileName : string);
var
  Rect : TRect;
  i : Integer;
  ImageToPrint : TMetaFile;
  MetaCanvas : TMetaFileCanvas;
  MinX, MaxX, MinY, MaxY : Integer;
  P : LTpoint;
  SuperCenter, OldCenter : Tpoint;
  Levv0 : TSketchLevel;
  Zzoom : Integer;
  List : TList;
begin
  Zzoom := TsketchObject(FatherObject).Zoom;
  OldCenter := TsketchObject(FatherObject).Center;
  TsketchObject(FatherObject).Center.X := 10;
  TsketchObject(FatherObject).Center.Y := 10;
  TsketchObject(FatherObject).Zoom := 64;

  Levv0 := TSketchLevel(project.SketchObject.Levels[0]);
  MinX := 1000000000;
  MaxX := -1000000000;
  MinY := 1000000000;
  MaxY := -1000000000;
  ImageToPrint := TMetaFile.Create;
  ImageToPrint.Enhanced := True;
  for i := 0 to Levv0.points.count - 1 do
  begin
    P := LTpoint(Levv0.points[i]);
    if P.X < MinX then
      MinX := P.X;
    if P.X > MaxX then
      MaxX := P.X;
    if P.Y < MinY then
      MinY := P.Y;
    if P.Y > MaxY then
      MaxY := P.Y;
  end;
  SuperCenter.X := -MinX;
  SuperCenter.Y := -MinY;
  TsketchObject(FatherObject).Zoom := (MaxX - MinX) div Round(4500 / 3.68);
  ImageToPrint.Width := (MaxX - MinX) div TsketchObject(FatherObject).Zoom + 20;
  ImageToPrint.Height := (MaxY - MinY) div TsketchObject(FatherObject).Zoom + 20;
  MetaCanvas := TMetaFileCanvas.Create(ImageToPrint, 0);
  Rect.Left := 0;
  Rect.Top := 0;
  Rect.Right := ImageToPrint.Width;
  Rect.Bottom := ImageToPrint.Height;
  List := TList.Create;
  DrawOnCanvas(MetaCanvas, 0, List, Rect, False, -MinX, -MinY, False);
  TsketchObject(FatherObject).Zoom := Zzoom;
  TsketchObject(FatherObject).Center.X := OldCenter.X;
  TsketchObject(FatherObject).Center.Y := OldCenter.Y;
  List.Free;

  MetaCanvas.Free;
  ImageToPrint.SaveToFile(fileName);
  ImageToPrint.Free;
end;

procedure TSketchLevel.Print(GridType, IdLevel : Integer);
var
  Rect : TRect;
  i, z : Integer;
  ImageToPrint : TMetaFile;
  MetaCanvas : TMetaFileCanvas;
  CoeffX, CoeffY : Double;
  MinX, MaxX, MinY, MaxY : Integer;
  P : LTpoint;
  SuperCenter, OldCenter : Tpoint;
  levTemp : TSketchLevel;
  Zzoom : Integer;
  List : TList;
begin
  Zzoom := TsketchObject(FatherObject).Zoom;
  OldCenter := TsketchObject(FatherObject).Center;
  TsketchObject(FatherObject).Center.X := 10;
  TsketchObject(FatherObject).Center.Y := 10;
  MinX := 1000000000;
  MaxX := -1000000000;
  MinY := 1000000000;
  MaxY := -1000000000;
  ImageToPrint := TMetaFile.Create;
  ImageToPrint.Enhanced := True;

  for z := 0 to TsketchObject(FatherObject).Levels.count - 1 do
  begin
    levTemp := TSketchLevel(TsketchObject(FatherObject).Levels[z]);
    for i := 0 to levTemp.points.count - 1 do
    begin
      P := LTpoint(levTemp.points[i]);
      if P.X < MinX then
        MinX := P.X;
      if P.X > MaxX then
        MaxX := P.X;
      if P.Y < MinY then
        MinY := P.Y;
      if P.Y > MaxY then
        MaxY := P.Y;
    end;
  end;
  // TSketchObject(FatherObject).Zoom := 16;
  // if Printer.pageWidth = 0 then Beep;
  TsketchObject(FatherObject).Zoom := (MaxX - MinX) div Round(Printer.pageWidth / 3.68);
  if TsketchObject(FatherObject).Zoom <= 0 then
    TsketchObject(FatherObject).Zoom := 1;
  SuperCenter.X := -MinX;
  SuperCenter.Y := -MinY;
  ImageToPrint.Width := ((MaxX - MinX) div TsketchObject(FatherObject).Zoom + 20);
  ImageToPrint.Height := ((MaxY - MinY) div TsketchObject(FatherObject).Zoom + 20);
  ImageToPrint.Width := ImageToPrint.Width;
  ImageToPrint.Height := ImageToPrint.Height;
  MetaCanvas := TMetaFileCanvas.Create(ImageToPrint, 0);
  Rect.Left := 0;
  Rect.Top := 0;
  Rect.Right := ImageToPrint.Width;
  Rect.Bottom := ImageToPrint.Height;
  List := TList.Create;
  DrawOnCanvas(MetaCanvas, 0, List, Rect, False, -MinX, -MinY, True);
  TsketchObject(FatherObject).Zoom := Zzoom;
  TsketchObject(FatherObject).Center.X := OldCenter.X;
  TsketchObject(FatherObject).Center.Y := OldCenter.Y;
  List.Free;

  MetaCanvas.Free;
  with Printer do
  begin
    if ImageToPrint.Width > ImageToPrint.Height then
    begin
      Printer.Orientation := poLandscape;
    end
    else
      Printer.Orientation := poPortrait;
    CoeffX := pageWidth / ImageToPrint.Width;
    CoeffY := PageHeight / ImageToPrint.Height;
    if CoeffY < CoeffX then
      CoeffX := CoeffY;
    Printer.Title := 'Level' + IntToStr(IdLevel);
    Rect.Left := 10;
    Rect.Top := 10;
    Rect.Right := Round(ImageToPrint.Width * CoeffX) - 10;
    Rect.Bottom := Round(ImageToPrint.Height * CoeffX) - 10;
    BeginDoc;
    Canvas.StretchDraw(Rect, ImageToPrint);
    EndDoc;
    ImageToPrint.Free;
  end;
end;

procedure TSketchLevel.ZoomRect(P1, P2 : Tpoint);
var
  NewCenterX, NewCenterY : Integer;
  FacteurX, FacteurY : Integer;
begin
  if project.SketchObject.Zoom <> 1 then
  begin
    FacteurX := project.SketchObject.Zoom * (Abs(P1.X - P2.X) div project.SketchObject.Zoom)
      div TsketchObject(FatherObject).imageUi.Width;
    FacteurY := project.SketchObject.Zoom * (Abs(P1.Y - P2.Y) div project.SketchObject.Zoom)
      div TsketchObject(FatherObject).imageUi.Height;
    if FacteurY > FacteurX then
      FacteurX := FacteurY;
    if P2.X < P1.X then
      P1.X := P2.X;
    if P2.Y < P1.Y then
      P1.Y := P2.Y;
    NewCenterX := project.SketchObject.Center.X - P1.X;
    NewCenterY := project.SketchObject.Center.Y - P1.Y;
    if bMagneticGrid then
    begin
      NewCenterX := NewCenterX div FacteurX div GridStep * GridStep * FacteurX;
      NewCenterY := NewCenterY div FacteurY div GridStep * GridStep * FacteurY;
    end;
    project.SketchObject.Zoom := FacteurX;
    project.SketchObject.Center.X := NewCenterX;
    project.SketchObject.Center.Y := NewCenterY;
    project.SketchObject.Refresh();
  end;
end;

procedure TSketchLevel.CreateDoor(Point : Tpoint);
var
  Seg : LTSegment;
  LPoint : LTpoint;
  VecteurX, VecteurY, Longueur : Double;
  P1, P2 : Tpoint;
  Center : Tpoint;
  Erreur : Integer;
  scale, Largeur : Double;
  porte : Tporte;
  R1, R2 : Double;
begin
  Val(FormPorte.EditDoorW.text, Largeur, Erreur);
  Largeur := Largeur / 2;
  Center := TsketchObject(FatherObject).Center;
  // Zoom := TSketchObject(FatherObject).Zoom;
  scale := TsketchObject(FatherObject).scale;
  LPoint := LTpoint.Create(Point);
  LPoint.X := LPoint.X - Center.X;
  LPoint.Y := LPoint.Y - Center.Y;
  Seg := PointOnSegmentVar(LPoint, 15);
  if Seg <> nil then
  begin
    if (Seg.Size(scale) < Largeur * 2) then
    begin
      MessageDLG(Labels[246], mtWarning, [mbOk], 0);
      exit;
    end;
    if (Height < FormPorte.Hauteur) then
    begin
      MessageDLG(Labels[247], mtWarning, [mbOk], 0);
      exit;
    end;
    if (Seg.ListePortes.count) + (Seg.ListeFenetres.count) < 100 then
    begin
      // Normalisation du vecteur du segment;
      // Lpoint.x := (Lpoint.x)-Center.x;
      // Lpoint.y := (Lpoint.y)-Center.y;
      VecteurX := (Seg.b.X - Seg.a.X);
      VecteurY := (Seg.b.Y - Seg.a.Y);
      if (Abs(VecteurX) > Abs(VecteurY)) and (VecteurX <> 0) then
        LPoint.Y := Round((LPoint.X - Seg.a.X) * (VecteurY / VecteurX) + Seg.a.Y)
      else if (Abs(VecteurX) <= Abs(VecteurY)) and (VecteurY <> 0) then
        LPoint.X := Round((LPoint.Y - Seg.a.Y) * (VecteurX / VecteurY) + Seg.a.X);

      Longueur := Sqrt(Sqr(VecteurX) + Sqr(VecteurY));

      if Longueur <> 0 then
      begin
        VecteurX := VecteurX / Longueur;
        VecteurY := VecteurY / Longueur;
      end
      else
      begin
        VecteurX := 1;
        VecteurY := 1;
      end;


      P1.X := Round(LPoint.X + Largeur * scale * VecteurX);
      P1.Y := Round(LPoint.Y + Largeur * scale * VecteurY);
      P2.X := Round(LPoint.X - Largeur * scale * VecteurX);
      P2.Y := Round(LPoint.Y - Largeur * scale * VecteurY);

      // Calculate the absolute coordinate

      // D1 := Sqrt(Sqr(p2.x-Seg.A.x)+Sqr(p2.y-Seg.A.y));
      // D2 := Sqrt(Sqr(p1.x-Seg.B.x)+Sqr(p1.y-Seg.B.y));
      // D1 := D1/longueur;
      // D2 := D2/longueur;
      porte := Tporte.Create;
      porte.CompoPorte := FormPorte.CompoPorte;
      porte.Hauteur := FormPorte.Hauteur;
      porte.Largeur := FormPorte.Largeur;
      R1 := LPoint.X - Seg.a.X;
      R2 := LPoint.Y - Seg.a.Y;
      porte.Center := Sqrt(Sqr(R1) + Sqr(R2)) / Longueur;
      // Porte.CoeffD := D2;
      Seg.ListePortes.Add(porte);
    end
    else
      MessageDLG(Labels[244] + IntToStr(MaxOuvertures) + Labels[245], mtWarning, [mbOk], 0);
  end;
end;

procedure TSketchLevel.FlashMenuiserie(Point : Tpoint; SBitmap : Tbitmap; GridType : Integer);
var
  Seg : LTSegment;
  LPoint, LPoint2 : LTpoint;
  VecteurX, VecteurY, Longueur : Double;
  P1, P2 : Tpoint;
  Center : Tpoint;
  Zoom, Erreur, i, j : Integer;
  scale, Largeur : Double;
  D1, D2 : Double;
  C1, C2 : string;
  porte : Tporte;
  Fenetre : TFenetre;
begin
  PorteSelectionnee := nil;
  FenetreSelectionnee := nil;
  Center := TsketchObject(FatherObject).Center;
  Zoom := TsketchObject(FatherObject).Zoom;
  scale := TsketchObject(FatherObject).scale;
  LPoint := LTpoint.Create(Point);
  LPoint.X := LPoint.X - Center.X;
  LPoint.Y := LPoint.Y - Center.Y;
  Seg := PointOnSegmentVar(LPoint, 15);
  SBitmap.Assign(BufferBitmap);
  if Seg <> nil then
  begin
    // Normalisation du vecteur du segment;
    VecteurX := (Seg.b.X - Seg.a.X);
    VecteurY := (Seg.b.Y - Seg.a.Y);
    if (Abs(VecteurX) > Abs(VecteurY)) and (VecteurX <> 0) then
    begin
      LPoint.Y := Round((LPoint.X - Seg.a.X) * (VecteurY / VecteurX) + Seg.a.Y);
      for porte in Seg.ListePortes do
      begin
        LPoint2 := LTpoint.Create(Point);
        VecteurX := (Seg.b.X - Seg.a.X);
        VecteurY := (Seg.b.Y - Seg.a.Y);
        LPoint2.X := Round((Seg.b.X * porte.Center) + (Seg.a.X * (1 - porte.Center)));
        LPoint2.Y := Round((Seg.b.Y * porte.Center) + (Seg.a.Y * (1 - porte.Center)));
        Longueur := Sqrt(Sqr(VecteurX) + Sqr(VecteurY));
        if Longueur <> 0 then
        begin
          VecteurX := VecteurX / Longueur;
          VecteurY := VecteurY / Longueur;
        end
        else
        begin
          VecteurX := 1;
          VecteurY := 1;
        end;
        P1.X := Round(LPoint2.X + porte.Largeur / 2 * scale * VecteurX);
        P1.Y := Round(LPoint2.Y + porte.Largeur / 2 * scale * VecteurY);
        P2.X := Round(LPoint2.X - porte.Largeur / 2 * scale * VecteurX);
        P2.Y := Round(LPoint2.Y - porte.Largeur / 2 * scale * VecteurY);
        if ((P1.X < P2.X) and (LPoint.X >= P1.X) and (LPoint.X <= P2.X)) or ((LPoint.X >= P2.X) and (LPoint.X <= P1.X))
        then
          with SBitmap.Canvas do
          begin
            PorteSelectionnee := porte;
            Pen.color := ClRed;
            Pen.Width := 4;
            MoveTo((P1.X + Center.X) div Zoom, (P1.Y + Center.Y) div Zoom);
            LineTo((P2.X + Center.X) div Zoom, (P2.Y + Center.Y) div Zoom);
            Pen.Width := 1;
          end;
        LPoint2.Free;
      end;
      for Fenetre in Seg.ListeFenetres do
      begin
        LPoint2 := LTpoint.Create(Point);
        VecteurX := (Seg.b.X - Seg.a.X);
        VecteurY := (Seg.b.Y - Seg.a.Y);
        LPoint2.X := Round((Seg.b.X * Fenetre.Center) + (Seg.a.X * (1 - Fenetre.Center)));
        LPoint2.Y := Round((Seg.b.Y * Fenetre.Center) + (Seg.a.Y * (1 - Fenetre.Center)));
        Longueur := Sqrt(Sqr(VecteurX) + Sqr(VecteurY));
        if Longueur <> 0 then
        begin
          VecteurX := VecteurX / Longueur;
          VecteurY := VecteurY / Longueur;
        end
        else
        begin
          VecteurX := 1;
          VecteurY := 1;
        end;
        P1.X := Round(LPoint2.X + Fenetre.Largeur / 2 * scale * VecteurX);
        P1.Y := Round(LPoint2.Y + Fenetre.Largeur / 2 * scale * VecteurY);
        P2.X := Round(LPoint2.X - Fenetre.Largeur / 2 * scale * VecteurX);
        P2.Y := Round(LPoint2.Y - Fenetre.Largeur / 2 * scale * VecteurY);
        if ((P1.X < P2.X) and (LPoint.X >= P1.X) and (LPoint.X <= P2.X)) or ((LPoint.X >= P2.X) and (LPoint.X <= P1.X))
        then
          with SBitmap.Canvas do
          begin
            FenetreSelectionnee := Fenetre;
            Pen.color := ClRed;
            Pen.Width := 4;
            MoveTo((P1.X + Center.X) div Zoom, (P1.Y + Center.Y) div Zoom);
            LineTo((P2.X + Center.X) div Zoom, (P2.Y + Center.Y) div Zoom);
            Pen.Width := 1;
          end;
        LPoint2.Free;
      end;
    end
    else if (Abs(VecteurX) <= Abs(VecteurY)) and (VecteurY <> 0) then
    begin
      LPoint.X := Round((LPoint.Y - Seg.a.Y) * (VecteurX / VecteurY) + Seg.a.X);
      for porte in Seg.ListePortes do
      begin
        LPoint2 := LTpoint.Create(Point);
        VecteurX := (Seg.b.X - Seg.a.X);
        VecteurY := (Seg.b.Y - Seg.a.Y);
        LPoint2.X := Round((Seg.b.X * porte.Center) + (Seg.a.X * (1 - porte.Center)));
        LPoint2.Y := Round((Seg.b.Y * porte.Center) + (Seg.a.Y * (1 - porte.Center)));
        Longueur := Sqrt(Sqr(VecteurX) + Sqr(VecteurY));
        if Longueur <> 0 then
        begin
          VecteurX := VecteurX / Longueur;
          VecteurY := VecteurY / Longueur;
        end
        else
        begin
          VecteurX := 1;
          VecteurY := 1;
        end;
        P1.X := Round(LPoint2.X + porte.Largeur / 2 * scale * VecteurX);
        P1.Y := Round(LPoint2.Y + porte.Largeur / 2 * scale * VecteurY);
        P2.X := Round(LPoint2.X - porte.Largeur / 2 * scale * VecteurX);
        P2.Y := Round(LPoint2.Y - porte.Largeur / 2 * scale * VecteurY);
        if ((P1.Y < P2.Y) and (LPoint.Y >= P1.Y) and (LPoint.Y <= P2.Y)) or ((LPoint.Y >= P2.Y) and (LPoint.Y <= P1.Y))
        then
          with SBitmap.Canvas do
          begin
            PorteSelectionnee := porte;
            Pen.color := ClRed;
            Pen.Width := 4;
            MoveTo((P1.X + Center.X) div Zoom, (P1.Y + Center.Y) div Zoom);
            LineTo((P2.X + Center.X) div Zoom, (P2.Y + Center.Y) div Zoom);
            Pen.Width := 1;
          end;
        LPoint2.Free;
      end;
      for Fenetre in Seg.ListeFenetres do
      begin
        LPoint2 := LTpoint.Create(Point);
        VecteurX := (Seg.b.X - Seg.a.X);
        VecteurY := (Seg.b.Y - Seg.a.Y);
        LPoint2.X := Round((Seg.b.X * Fenetre.Center) + (Seg.a.X * (1 - Fenetre.Center)));
        LPoint2.Y := Round((Seg.b.Y * Fenetre.Center) + (Seg.a.Y * (1 - Fenetre.Center)));
        Longueur := Sqrt(Sqr(VecteurX) + Sqr(VecteurY));
        if Longueur <> 0 then
        begin
          VecteurX := VecteurX / Longueur;
          VecteurY := VecteurY / Longueur;
        end
        else
        begin
          VecteurX := 1;
          VecteurY := 1;
        end;
        P1.X := Round(LPoint2.X + Fenetre.Largeur / 2 * scale * VecteurX);
        P1.Y := Round(LPoint2.Y + Fenetre.Largeur / 2 * scale * VecteurY);
        P2.X := Round(LPoint2.X - Fenetre.Largeur / 2 * scale * VecteurX);
        P2.Y := Round(LPoint2.Y - Fenetre.Largeur / 2 * scale * VecteurY);
        if ((P1.Y < P2.Y) and (LPoint.Y >= P1.Y) and (LPoint.Y <= P2.Y)) or ((LPoint.Y >= P2.Y) and (LPoint.Y <= P1.Y))
        then
          with SBitmap.Canvas do
          begin
            FenetreSelectionnee := Fenetre;
            Pen.color := ClRed;
            Pen.Width := 4;
            MoveTo((P1.X + Center.X) div Zoom, (P1.Y + Center.Y) div Zoom);
            LineTo((P2.X + Center.X) div Zoom, (P2.Y + Center.Y) div Zoom);
            Pen.Width := 1;
          end;
        LPoint2.Free;
      end;
    end;
  end;
end;


procedure TSketchLevel.FlashDoor(Point : Tpoint; SBitmap : Tbitmap; GridType : Integer);
var
  Seg : LTSegment;
  LPoint : LTpoint;
  VecteurX, VecteurY, Longueur : Double;
  P1, P2 : Tpoint;
  Center : Tpoint;
  Zoom, Erreur : Integer;
  scale, Largeur : Double;
  D1, D2 : Double;
  C1, C2 : string;
  R1, R2 : Double;
begin
  Val(FormPorte.EditDoorW.text, Largeur, Erreur);
  Largeur := Largeur / 2;
  Center := TsketchObject(FatherObject).Center;
  Zoom := TsketchObject(FatherObject).Zoom;
  scale := TsketchObject(FatherObject).scale;
  LPoint := LTpoint.Create(Point);
  LPoint.X := LPoint.X - Center.X;
  LPoint.Y := LPoint.Y - Center.Y;
  Seg := PointOnSegmentVar(LPoint, 15);
  SBitmap.Assign(BufferBitmap);
  if Seg <> nil then
  begin
    // Normalisation du vecteur du segment;
    VecteurX := (Seg.b.X - Seg.a.X);
    VecteurY := (Seg.b.Y - Seg.a.Y);
    if (Abs(VecteurX) > Abs(VecteurY)) and (VecteurX <> 0) then
      LPoint.Y := Round((LPoint.X - Seg.a.X) * (VecteurY / VecteurX) + Seg.a.Y)
    else if (Abs(VecteurX) <= Abs(VecteurY)) and (VecteurY <> 0) then
      LPoint.X := Round((LPoint.Y - Seg.a.Y) * (VecteurX / VecteurY) + Seg.a.X);

    Longueur := Sqrt(Sqr(VecteurX) + Sqr(VecteurY));
    if Longueur <> 0 then
    begin
      VecteurX := VecteurX / Longueur;
      VecteurY := VecteurY / Longueur;
    end
    else
    begin
      VecteurX := 1;
      VecteurY := 1;
    end;


    P1.X := Round(LPoint.X + Largeur * scale * VecteurX);
    P1.Y := Round(LPoint.Y + Largeur * scale * VecteurY);
    P2.X := Round(LPoint.X - Largeur * scale * VecteurX);
    P2.Y := Round(LPoint.Y - Largeur * scale * VecteurY);

    // Calculate the absolute coordinate

    with SBitmap.Canvas do
    begin
      Pen.Width := 4;
      Pen.color := ClRed;
      Pen.style := psSolid;
      Brush.style := bsClear;
      MoveTo((P1.X + Center.X) div Zoom, (P1.Y + Center.Y) div Zoom);
      LineTo((P2.X + Center.X) div Zoom, (P2.Y + Center.Y) div Zoom);
      R1 := P2.X - Seg.a.X;
      R2 := P2.Y - Seg.a.Y;
      D1 := Sqrt(Sqr(R1) + Sqr(R2));
      R1 := P1.X - Seg.b.X;
      R2 := P1.Y - Seg.b.Y;
      D2 := Sqrt(Sqr(R1) + Sqr(R2));
      D1 := D1 / scale;
      D2 := D2 / scale;
      C1 := getString(D1);
      C2 := getString(D2);
      TextOut((Seg.a.X + Center.X) div Zoom, (Seg.a.Y + Center.Y) div Zoom, C1 + Labels[32]);
      TextOut((Seg.b.X + Center.X) div Zoom, (Seg.b.Y + Center.Y) div Zoom, C2 + Labels[32]);
      Pen.Width := 1;
    end;
  end;
end;

procedure TSketchLevel.CreateWindow(Point : Tpoint);
var
  Seg : LTSegment;
  LPoint : LTpoint;
  VecteurX, VecteurY, Longueur : Double;
  P1, P2 : Tpoint;
  Center : Tpoint;
  Erreur : Integer;
  scale, Largeur : Double;
  Fenetre : TFenetre;
  R1, R2 : Double;
begin
  Val(FormFenetre.EditFenW.text, Largeur, Erreur);
  Largeur := Largeur / 2;
  Center := TsketchObject(FatherObject).Center;
  scale := TsketchObject(FatherObject).scale;
  LPoint := LTpoint.Create(Point);
  LPoint.X := LPoint.X - Center.X;
  LPoint.Y := LPoint.Y - Center.Y;
  Seg := PointOnSegmentVar(LPoint, 15);
  if Seg <> nil then
  begin
    if (Seg.Size(scale) < Largeur * 2) then
    begin
      MessageDLG(Labels[246], mtWarning, [mbOk], 0);
      exit;
    end;
    if (Height < FormFenetre.Hauteur + FormFenetre.Allege) then
    begin
      MessageDLG(Labels[247], mtWarning, [mbOk], 0);
      exit;
    end;
    if (Seg.ListePortes.count + Seg.ListeFenetres.count) < MaxOuvertures then
    begin
      // Normalisation du vecteur du segment;
      VecteurX := (Seg.b.X - Seg.a.X);
      VecteurY := (Seg.b.Y - Seg.a.Y);
      if (Abs(VecteurX) > Abs(VecteurY)) and (VecteurX <> 0) then
        LPoint.Y := Round((LPoint.X - Seg.a.X) * (VecteurY / VecteurX) + Seg.a.Y)
      else if (Abs(VecteurX) <= Abs(VecteurY)) and (VecteurY <> 0) then
        LPoint.X := Round((LPoint.Y - Seg.a.Y) * (VecteurX / VecteurY) + Seg.a.X);

      Longueur := Sqrt(Sqr(VecteurX) + Sqr(VecteurY));
      if Longueur <> 0 then
      begin
        VecteurX := VecteurX / Longueur;
        VecteurY := VecteurY / Longueur;
      end
      else
      begin
        VecteurX := 1;
        VecteurY := 1;
      end;

      P1.X := Round(LPoint.X + Largeur * scale * VecteurX);
      P1.Y := Round(LPoint.Y + Largeur * scale * VecteurY);
      P2.X := Round(LPoint.X - Largeur * scale * VecteurX);
      P2.Y := Round(LPoint.Y - Largeur * scale * VecteurY);

      // Calculate the absolute coordinate
      Fenetre := TFenetre.Create;
      Fenetre.CompoFen := FormFenetre.CompoFenetre;
      Fenetre.Hauteur := FormFenetre.Hauteur;
      Fenetre.Largeur := FormFenetre.Largeur;
      Fenetre.Allege := FormFenetre.Allege;
      Fenetre.Retrait := FormFenetre.Retrait;
      R1 := LPoint.X - Seg.a.X;
      R2 := LPoint.Y - Seg.a.Y;
      Fenetre.Center := Sqrt(Sqr(R1) + Sqr(R2)) / Longueur;
      // Porte.CoeffD := D2;
      Seg.ListeFenetres.Add(Fenetre);
    end
    else
      MessageDLG(Labels[244] + IntToStr(MaxOuvertures) + Labels[245], mtWarning, [mbOk], 0);
  end;
end;

procedure TSketchLevel.FlashWindow(Point : Tpoint; SBitmap : Tbitmap; GridType : Integer);
var
  Seg : LTSegment;
  LPoint : LTpoint;
  VecteurX, VecteurY, Longueur : Double;
  P1, P2 : Tpoint;
  Center : Tpoint;
  Zoom, Erreur : Integer;
  scale, Largeur : Double;
  D1, D2 : Double;
  C1, C2 : string;
  R1, R2 : Double;
begin
  Val(FormFenetre.EditFenW.text, Largeur, Erreur);
  Largeur := Largeur / 2;
  Center := TsketchObject(FatherObject).Center;
  Zoom := TsketchObject(FatherObject).Zoom;
  scale := TsketchObject(FatherObject).scale;
  LPoint := LTpoint.Create(Point);
  LPoint.X := LPoint.X - Center.X;
  LPoint.Y := LPoint.Y - Center.Y;
  Seg := PointOnSegmentVar(LPoint, 15);
  SBitmap.Assign(BufferBitmap);
  if Seg <> nil then
  begin
    // Normalisation du vecteur du segment;
    VecteurX := (Seg.b.X - Seg.a.X);
    VecteurY := (Seg.b.Y - Seg.a.Y);
    if (Abs(VecteurX) > Abs(VecteurY)) and (VecteurX <> 0) then
      LPoint.Y := Round((LPoint.X - Seg.a.X) * (VecteurY / VecteurX) + Seg.a.Y)
    else if (Abs(VecteurX) <= Abs(VecteurY)) and (VecteurY <> 0) then
      LPoint.X := Round((LPoint.Y - Seg.a.Y) * (VecteurX / VecteurY) + Seg.a.X);

    Longueur := Sqrt(Sqr(VecteurX) + Sqr(VecteurY));
    if Longueur <> 0 then
    begin
      VecteurX := VecteurX / Longueur;
      VecteurY := VecteurY / Longueur;
    end
    else
    begin
      VecteurX := 1;
      VecteurY := 1;
    end;


    P1.X := Round(LPoint.X + Largeur * scale * VecteurX);
    P1.Y := Round(LPoint.Y + Largeur * scale * VecteurY);
    P2.X := Round(LPoint.X - Largeur * scale * VecteurX);
    P2.Y := Round(LPoint.Y - Largeur * scale * VecteurY);

    // Calculate the absolute coordinate

    with SBitmap.Canvas do
    begin
      Pen.Width := 4;
      Pen.color := ClRed;
      Pen.style := psSolid;
      Brush.style := bsClear;
      MoveTo((P1.X + Center.X) div Zoom, (P1.Y + Center.Y) div Zoom);
      LineTo((P2.X + Center.X) div Zoom, (P2.Y + Center.Y) div Zoom);
      R1 := P2.X - Seg.a.X;
      R2 := P2.Y - Seg.a.Y;
      D1 := Sqrt(Sqr(R1) + Sqr(R2));
      R1 := P1.X - Seg.b.X;
      R2 := P1.Y - Seg.b.Y;
      D2 := Sqrt(Sqr(R1) + Sqr(R2));
      D1 := D1 / scale;
      D2 := D2 / scale;
      C1 := getString(D1);
      C2 := getString(D2);
      TextOut((Seg.a.X + Center.X) div Zoom, (Seg.a.Y + Center.Y) div Zoom, C1 + Labels[32]);
      TextOut((Seg.b.X + Center.X) div Zoom, (Seg.b.Y + Center.Y) div Zoom, C2 + Labels[32]);
      Pen.Width := 1;
    end;
  end;
end;



// ********************************************************************
// This method store the characteristics of a level inside a string list
// *********************************************************************

procedure TSketchLevel.SaveToList(List : TStringList);
var
  i, j, NbRoom : Integer;
  Seg : LTSegment;
  Room : TRoom;
  Ok : Boolean;
  TempString, chaine, Chaine2 : string;
  porte : Tporte;
  Fenetre : TFenetre;
begin
  // l'�tage est-il vide?
  if (points.count = 0) and (segments.count = 0) then
    exit;
  Str(Height, chaine);
  if Toiture then
    chaine := chaine + ' ';
  List.Add(chaine);
  // Store all points
  List.Add(IntToStr(points.count));
  for i := 0 to points.count - 1 do
  begin
    LTpoint(points[i]).SaveToList(List);
  end;
  // Store all Segments
  List.Add(IntToStr(segments.count));
  for i := 0 to segments.count - 1 do
  begin
    Seg := LTSegment(segments[i]);
    List.Add(IntToStr(points.IndexOf(Seg.a)));
    List.Add(IntToStr(points.IndexOf(Seg.b)));
    List.Add(IntToStr(Seg.PercentageGlazing));
    List.Add(IntToStr(Seg.NumberGlazing));
    if Seg.Composition <> nil then
      List.Add(Seg.Composition.nom)
    else
      List.Add('');
    List.Add(IntToStr(Seg.Shadings));
    Str(Seg.Orientation, TempString);
    List.Add(TempString);
    List.Add(IntToStr(Seg.ListePortes.count));
    for porte in Seg.ListePortes do
    begin
      if porte.CompoPorte <> nil then
        List.Add(porte.CompoPorte.nom)
      else
        List.Add('');
      Str(porte.Center, chaine);
      List.Add(chaine);
      Str(porte.Hauteur, chaine);
      List.Add(chaine);
      Str(porte.Largeur, chaine);
      List.Add(chaine);
    end;

    List.Add('Pont');
    List.Add('');
    List.Add('0');
    List.Add('P2');
    List.Add('');
    List.Add('0');

    List.Add(IntToStr(Seg.ListeFenetres.count));
    for Fenetre in Seg.ListeFenetres do
    begin
      if Fenetre.CompoFen <> nil then
        List.Add(Fenetre.CompoFen.nom)
      else
        List.Add('');
      Str(Fenetre.Center, chaine);
      List.Add(chaine);
      Str(Fenetre.Hauteur, chaine);
      List.Add(chaine);
      Str(Fenetre.Largeur, chaine);
      Str(Fenetre.Allege, Chaine2);
      List.Add(chaine + '/' + Chaine2);
      List.Add('R');
      Str(Fenetre.Retrait, chaine);
      List.Add(chaine);
    end;
    List.Add('Casquette');
    Str(Seg.Casquette, chaine);
    List.Add(chaine);
    List.Add('d');
    Str(Seg.Distance, chaine);
    List.Add(chaine);
  end;

  // Store All Rooms
  NbRoom := 0;
  for i := 0 to Rooms.count - 1 do
  begin
    Ok := True;
    Room := TRoom(Rooms[i]);
    for j := 0 to Room.points.count - 1 do
      if points.IndexOf(Room.points[j]) = -1 then
        Ok := False;
    if Ok then
      inc(NbRoom);
  end;
  List.Add(IntToStr(NbRoom));
  for i := 0 to Rooms.count - 1 do
  begin
    Room := TRoom(Rooms[i]);
    Ok := True;
    for j := 0 to Room.points.count - 1 do
      if points.IndexOf(Room.points[j]) = -1 then
        Ok := False;
    if Ok then
    begin
      List.Add(IntToStr(Room.points.count));
      for j := 0 to Room.points.count - 1 do
        List.Add(IntToStr(points.IndexOf(Room.points[j])));

      List.Add('Supp');
      List.Add(IntToStr(Room.HautPoint.count));
      for j := 0 to Room.HautPoint.count - 1 do
      begin
        Str(Room.HautPoint[j], TempString);
        List.Add(TempString);
      end;

      List.Add(IntToStr(Integer(Room.RoomType)));
      Str(Room.Area, TempString);
      List.Add(TempString);
      List.Add(Room.Name);

//      Str(Room.ZoneNumber, TempString);
//      List.Add(TempString);
      List.Add('');

      Str(Room.Value1, TempString);
      List.Add(TempString);
      Str(Room.Value2, TempString);
      List.Add(TempString);
      Str(Room.Value3, TempString);
      List.Add(TempString);
      Str(Room.Value4, TempString);
      List.Add(TempString);
      Str(Room.Value5, TempString);
      List.Add(TempString);
      Str(Room.Value6, TempString);
      List.Add(TempString);
      Str(Room.Value7, TempString);
      List.Add(TempString);
      Str(Room.Value8, TempString);
      List.Add(TempString);
      List.Add(Room.sur);
      List.Add(Room.Ttau);
      Str(Room.Tau, TempString);
      List.Add(TempString);

    end;
    List.Add('c');
    if Room.CompoFloor <> nil then
      List.Add(Room.CompoFloor.nom)
    else
      List.Add('');
    if Room.CompoCeiling <> nil then
      List.Add(Room.CompoCeiling.nom)
    else
      List.Add('');
  end;
  List.Add('bck');
  List.Add(NomPicture);
  List.Add(IntToStr(decX));
  List.Add(IntToStr(decY));
end;


// ********************************************************************
// This method load a level from a data string list
// *********************************************************************

procedure TSketchLevel.LoadfromList(List : TStringList; var Index : Integer);
var
  i, j, NbRoom, Error : Integer;
  Seg : LTSegment;
  nbPoint, NbSegment, Erreur, nbportes : Integer;
  P : LTpoint;
  Room : TRoom;
  porte : Tporte;
  Fenetre : TFenetre;
  Reel : Double;
  ch1, ch2, chaine : string;
  Valeur : Integer;
  X, Y, r, g, b, zz : Integer;
  TempPicture : TPicture;
  Pp : PByteArray;
begin
  chaine := List[index];
  Val(chaine, Height, Erreur);
  Toiture := chaine[Length(chaine)] = ' ';
  if Erreur <> 0 then
    Height := 3;
  inc(index);
  // Loading Points
  nbPoint := StrToInt(List[index]);
  inc(index);
  for i := 0 to nbPoint - 1 do
  begin
    P := LTpoint.Create(Point(0, 0));
    points.Add(P);
    P.LoadfromList(List, index);
  end;

  // Loading Segments
  NbSegment := StrToInt(List[index]);
  inc(index);
  for i := 0 to NbSegment - 1 do
  begin
    Seg := LTSegment.Create(nil, nil);
    segments.Add(Seg);
    Seg.a := points[StrToInt(List[index])];
    inc(index);
    Seg.b := points[StrToInt(List[index])];
    inc(index);
    Seg.PercentageGlazing := StrToInt(List[index]);
    inc(index);
    Seg.NumberGlazing := StrToInt(List[index]);
    inc(index);

    Seg.Composition := bibCompositions.Get(List[index]);
    // Seg.GlazingColor := StrToInt(List[Index]);
    inc(index);
    Seg.Shadings := StrToInt(List[index]);
    inc(index);
    Val(List[index], Seg.Orientation, Error);
    inc(index);
    nbportes := StrToInt(List[index]);
    inc(index);
    for j := 1 to nbportes do
    begin
      porte := Tporte.Create;
      porte.CompoPorte := bibMenuiseries.Get(List[index]);
      inc(index);
      Val(List[index], Reel, Erreur);
      inc(index);
      porte.Center := Reel;
      Val(List[index], Reel, Erreur);
      inc(index);
      porte.Hauteur := Reel;
      Val(List[index], Reel, Erreur);
      inc(index);
      porte.Largeur := Reel;
      Seg.ListePortes.Add(porte);
    end;

    if list[index] = 'Pont' then
    begin
      inc(index, 6);
//      List.Add('Pont');
//      List.Add('');
//      List.Add('0');
//      List.Add('P2');
//      List.Add('');
//      List.Add('0');
    end;

    nbportes := StrToInt(List[index]);
    inc(index);
    for j := 1 to nbportes do
    begin
      Fenetre := TFenetre.Create;
      Fenetre.CompoFen := bibMenuiseries.Get(List[index]);
      inc(index);
      Val(List[index], Reel, Erreur);
      inc(index);
      Fenetre.Center := Reel;
      Val(List[index], Reel, Erreur);
      inc(index);
      Fenetre.Hauteur := Reel;
      ch2 := List[index];
      inc(index);
      // pp:= pos('/',ch2);
      if (pos('/', ch2) = 0) then
      begin
        ch1 := ch2;
        ch2 := '1.0';
      end
      else
      begin
        ch1 := Copy(ch2, 1, pos('/', ch2) - 1);
        Delete(ch2, 1, pos('/', ch2));
      end;
      Val(ch1, Reel, Erreur);
      Fenetre.Largeur := Reel;
      Val(ch2, Reel, Erreur);
      Fenetre.Allege := Reel;
      if List[index] = 'R' then
      begin
        inc(index);
        ch1 := List[index];
        inc(index);
        Val(ch1, Reel, Erreur);
        Fenetre.Retrait := Reel;
      end
      else
        Fenetre.Retrait := 0;
      Seg.ListeFenetres.Add(Fenetre);
    end;
    if List[index] = 'Casquette' then
    begin
      inc(index);
      Val(List[index], Seg.Casquette, Erreur);
      inc(index)
    end;
    if List[index] = 'd' then
    begin
      inc(index);
      Val(List[index], Seg.Distance, Erreur);
      inc(index)
    end;
  end;
  // Loading rooms
  if List[index] <> 'bck' then
  begin
    NbRoom := StrToInt(List[index]);
    inc(index);
  end
  else
    NbRoom := 0;
  for i := 0 to NbRoom - 1 do
  begin
    Room := TRoom.Create;
    nbPoint := StrToInt(List[index]);
    inc(index);
    for j := 0 to nbPoint - 1 do
    begin
      Room.points.Add(points[StrToInt(List[index])]);
      inc(index);
    end;
    chaine := List[index];
    inc(index);
    if chaine = 'Supp' then
    begin
      nbPoint := StrToInt(List[index]);
      inc(index);
      for j := 0 to nbPoint - 1 do
      begin
        Val(List[index], Reel, Erreur);
        Room.HautPoint.Add(Reel);
        inc(index);
      end;
      chaine := List[index];
      inc(index);
    end;
    Room.RoomType := TRoomType(StrToInt(chaine));
    Val(List[index], Room.Area, Error);
    inc(index);
    Room.Name := List[index];
    inc(index);
//    Val(List[index], Room.ZoneNumber, Error);
    inc(index);
    Val(List[index], Room.Value1, Error);
    inc(index);
    Val(List[index], Room.Value2, Error);
    inc(index);
    Val(List[index], Room.Value3, Error);
    inc(index);
    Val(List[index], Room.Value4, Error);
    inc(index);
    Val(List[index], Room.Value5, Error);
    inc(index);
    Val(List[index], Room.Value6, Error);
    inc(index);
    Val(List[index], Room.Value7, Error);
    inc(index);
    Val(List[index], Room.Value8, Error);
    inc(index);
    Room.sur := List[index];
    inc(index);
    Room.Ttau := List[index];
    inc(index);
    Val(List[index], Room.Tau, Error);
    inc(index);
    if List[index] = 'c' then
    begin
      inc(index);
      Room.CompoFloor := bibCompositions.Get(List[index]);
      inc(index);
      Room.CompoCeiling := bibCompositions.Get(List[index]);
      inc(index);
    end;
    Rooms.Add(Room);
  end;
  if List[index] = 'bck' then
  begin
    inc(index);
    NomPicture := List[index];
    inc(index);
    decX := StrToInt(List[index]);
    inc(index);
    decY := StrToInt(List[index]);
    inc(index);
    if not ModeUndo and FileExists(NomPicture) then
    begin
      TempPicture := TPicture.Create;
      TempPicture.LoadFromFile(NomPicture);
      BackGroundPicture.Bitmap.Width := TempPicture.Width;
      BackGroundPicture.Bitmap.Height := TempPicture.Height;
      BackGroundPicture.Bitmap.Canvas.Draw(0, 0, TempPicture.graphic);
      BackGroundPicture.Bitmap.PixelFormat := pf24bit;
      for Y := 0 to BackGroundPicture.Height - 1 do
      begin
        Pp := BackGroundPicture.Bitmap.ScanLine[Y];
        for X := 0 to BackGroundPicture.Width - 1 do
        begin
          r := Pp[X * 3];
          g := Pp[X * 3 + 1];
          b := Pp[X * 3 + 2];
          r := 255 - ((255 - r) div 2);
          g := 255 - ((255 - g) div 2);
          b := 255 - ((255 - b) div 2);
          Pp[X * 3] := r;
          Pp[X * 3 + 1] := g;
          Pp[X * 3 + 2] := b;
        end;
      end;
      freeAndNil(TempPicture);
    end;
  end;
  Analyzed := False;
end;

procedure TSketchLevel.innerLoadXml(node : IXMLNode);
var
  i, j, NbRoom, Error : Integer;
  Seg : LTSegment;
  nbPoint, NbSegment, Erreur, nbportes : Integer;
  P : LTpoint;
  Room : TRoom;
  porte : Tporte;
  Fenetre : TFenetre;
  Reel : Double;
  ch1, ch2, chaine : string;
  Valeur : Integer;
  X, Y, r, g, b, zz : Integer;
  TempPicture : TPicture;
  Pp : PByteArray;

  nodeC : IXMLNode;
  child : IXMLNode;
  child2 : IXMLNode;
begin
  Height := TXmlUtil.getTagDouble(node, 'height');
  Toiture := TXmlUtil.getAttBool(node, 'toiture');
  nodeC := TXmlUtil.getChild(node, 'points');
  // load all points
  for i := 0 to nodeC.ChildNodes.count - 1 do
  begin
    P := LTpoint.Create(Point(0, 0));
    points.Add(P);
    P.loadXml(nodeC.ChildNodes[i]);
  end;
  // load all Segments
  nodeC := TXmlUtil.getChild(node, 'segments');
  for i := 0 to nodeC.ChildNodes.count - 1 do
  begin
    child := nodeC.ChildNodes[i];
    Seg := LTSegment.Create(nil, nil);
    segments.Add(Seg);
    Seg.a := points[TXmlUtil.getTagInt(child, 'id-a')];
    Seg.b := points[TXmlUtil.getTagInt(child, 'id-b')];

    Seg.PercentageGlazing := TXmlUtil.getTagInt(child, 'percent-glazing');
    Seg.NumberGlazing := TXmlUtil.getTagInt(child, 'number-glazing');
    Seg.Composition := bibCompositions.Get(TXmlUtil.getTagString(child, 'composition'));

    Seg.Shadings := TXmlUtil.getTagInt(child, 'shadings');
    Seg.Orientation := TXmlUtil.getTagDouble(child, 'orientation');
    child2 := TXmlUtil.getChild(child, 'doors');
    for j := 0 to child2.ChildNodes.count - 1 do
    begin
      porte := Tporte.Create;
      Seg.ListePortes.Add(porte);
      porte.loadXml(child2.ChildNodes[j]);
    end;

    child2 := TXmlUtil.getChild(child, 'windows');
    for j := 0 to child2.ChildNodes.count - 1 do
    begin
      Fenetre := TFenetre.Create;
      Seg.ListeFenetres.Add(Fenetre);
      Fenetre.loadXml(child2.ChildNodes[j]);
    end;
    Seg.Casquette := TXmlUtil.getTagDouble(child, 'casquette');
    Seg.Distance := TXmlUtil.getTagDouble(child, 'distance');
  end;

  // Store All Rooms
  nodeC := TXmlUtil.getChild(node, 'rooms');
  for i := 0 to nodeC.ChildNodes.count - 1 do
  begin
    Room := TRoom.Create;
    Rooms.Add(Room);
    child := nodeC.ChildNodes[i];
    child2 := TXmlUtil.getChild(child, 'points');
    for j := 0 to child2.ChildNodes.count - 1 do
    begin
      Room.points.Add(points[getInt(child2.ChildNodes[j].text)]);
    end;

    child2 := TXmlUtil.getChild(child, 'haut-points');
    for j := 0 to child2.ChildNodes.count - 1 do
    begin
      Room.HautPoint.Add(getDouble(child2.ChildNodes[j].text));
    end;

    Room.RoomType := TRoomType(TXmlUtil.getTagInt(child, 'type'));
    Room.Area := TXmlUtil.getTagDouble(child, 'area');
    Room.Name := TXmlUtil.getTagString(child, 'name');
//    Room.ZoneNumber := TXmlUtil.getTagInt(child, 'zone_number');
    Room.Value1 := TXmlUtil.getTagDouble(child, 'value-1');
    Room.Value2 := TXmlUtil.getTagDouble(child, 'value-2');
    Room.Value3 := TXmlUtil.getTagDouble(child, 'value-3');
    Room.Value4 := TXmlUtil.getTagDouble(child, 'value-4');
    Room.Value5 := TXmlUtil.getTagDouble(child, 'value-5');
    Room.Value6 := TXmlUtil.getTagDouble(child, 'value-6');
    Room.Value7 := TXmlUtil.getTagDouble(child, 'value-7');
    Room.Value8 := TXmlUtil.getTagDouble(child, 'value-8');
    Room.sur := TXmlUtil.getTagString(child, 'sur');
    Room.Ttau := TXmlUtil.getTagString(child, 'ttau');
    Room.tau := TXmlUtil.getTagDouble(child, 'tau');
    Room.CompoFloor := bibCompositions.Get(TXmlUtil.getTagString(child, 'floor-composition'));
    Room.CompoCeiling := bibCompositions.Get(TXmlUtil.getTagString(child, 'ceiling-composition'));
  end;
  NomPicture := TXmlUtil.getTagString(child, 'picture-name');
  decX := TXmlUtil.getTagInt(child, 'dec-x');
  decY := TXmlUtil.getTagInt(child, 'dec-y');
  if (NomPicture <> '') and not(ModeUndo) and FileExists(NomPicture) then
  begin
    TempPicture := TPicture.Create;
    TempPicture.LoadFromFile(NomPicture);
    BackGroundPicture.Bitmap.Width := TempPicture.Width;
    BackGroundPicture.Bitmap.Height := TempPicture.Height;
    BackGroundPicture.Bitmap.Canvas.Draw(0, 0, TempPicture.graphic);
    BackGroundPicture.Bitmap.PixelFormat := pf24bit;
    for Y := 0 to BackGroundPicture.Height - 1 do
    begin
      Pp := BackGroundPicture.Bitmap.ScanLine[Y];
      for X := 0 to BackGroundPicture.Width - 1 do
      begin
        r := Pp[X * 3];
        g := Pp[X * 3 + 1];
        b := Pp[X * 3 + 2];
        r := 255 - ((255 - r) div 2);
        g := 255 - ((255 - g) div 2);
        b := 255 - ((255 - b) div 2);
        Pp[X * 3] := r;
        Pp[X * 3 + 1] := g;
        Pp[X * 3 + 2] := b;
      end;
    end;
    freeAndNil(TempPicture);
  end;
  Analyzed := False;
end;


procedure TSketchLevel.innerSaveXml(node : IXMLNode);
var
  i, j : Integer;
  Seg : LTSegment;
  Room : TRoom;
  Ok : Boolean;
  porte : Tporte;
  Fenetre : TFenetre;

  nodeC : IXMLNode;
  child : IXMLNode;
  child2 : IXMLNode;
begin
  // l'�tage est-il vide?
  if (points.count = 0) and (segments.count = 0) then
    exit;
  TXmlUtil.setTag(node, 'height', Height);
  TXmlUtil.setAtt(node, 'toiture', Toiture);
  nodeC := TXmlUtil.addChild(node, 'points');
  // Store all points
  for i := 0 to points.count - 1 do
  begin
    LTpoint(points[i]).saveXml(nodeC);
  end;
  // Store all Segments
  nodeC := TXmlUtil.addChild(node, 'segments');
  for i := 0 to segments.count - 1 do
  begin
    Seg := LTSegment(segments[i]);
    child := TXmlUtil.addChild(nodeC, 'segment');
    TXmlUtil.setTag(child, 'id-a', points.IndexOf(Seg.a));
    TXmlUtil.setTag(child, 'id-b', points.IndexOf(Seg.b));
    TXmlUtil.setTag(child, 'percent-glazing', Seg.PercentageGlazing);
    TXmlUtil.setTag(child, 'number-glazing', Seg.NumberGlazing);
    if Seg.Composition <> nil then
      TXmlUtil.setTag(child, 'composition', Seg.Composition.nom)
    else
      TXmlUtil.setTag(child, 'composition', '');
    TXmlUtil.setTag(child, 'shadings', Seg.Shadings);
    TXmlUtil.setTag(child, 'orientation', Seg.Orientation);
    child2 := TXmlUtil.addChild(child, 'doors');
    for porte in Seg.ListePortes do
    begin
      porte.saveXml(child2);
    end;

    child2 := TXmlUtil.addChild(child, 'windows');
    for Fenetre in Seg.ListeFenetres do
    begin
      Fenetre.saveXml(child2);
    end;
    TXmlUtil.setTag(child, 'casquette', Seg.Casquette);
    TXmlUtil.setTag(child, 'distance', Seg.Distance);
  end;

  // Store All Rooms
  nodeC := TXmlUtil.addChild(node, 'rooms');
  for i := 0 to Rooms.count - 1 do
  begin
    Room := TRoom(Rooms[i]);
    child := TXmlUtil.addChild(nodeC, 'room');
    Ok := True;
    for j := 0 to Room.points.count - 1 do
    begin
      if points.IndexOf(Room.points[j]) = -1 then
      begin
        Ok := False;
        Break;
      end;
    end;
    if not(Ok) then
      continue;
    child2 := TXmlUtil.addChild(child, 'points');
    for j := 0 to Room.points.count - 1 do
    begin
      TXmlUtil.setTag(child2, 'id', points.IndexOf(Room.points[j]));
    end;

    child2 := TXmlUtil.addChild(child, 'haut-points');
    for j := 0 to Room.HautPoint.count - 1 do
    begin
      TXmlUtil.setTag(child2, 'haut-point', Room.HautPoint[j]);
    end;

    TXmlUtil.setTag(child, 'type', Integer(Room.RoomType));
    TXmlUtil.setTag(child, 'area', Room.Area);
    TXmlUtil.setTag(child, 'name', Room.Name);
//    TXmlUtil.setTag(child, 'zone_number', Room.ZoneNumber);
    TXmlUtil.setTag(child, 'value-1', Room.Value1);
    TXmlUtil.setTag(child, 'value-2', Room.Value2);
    TXmlUtil.setTag(child, 'value-3', Room.Value3);
    TXmlUtil.setTag(child, 'value-4', Room.Value4);
    TXmlUtil.setTag(child, 'value-5', Room.Value5);
    TXmlUtil.setTag(child, 'value-6', Room.Value6);
    TXmlUtil.setTag(child, 'value-7', Room.Value7);
    TXmlUtil.setTag(child, 'value-8', Room.Value8);
    TXmlUtil.setTag(child, 'sur', Room.sur);
    TXmlUtil.setTag(child, 'ttau', Room.Ttau);
    TXmlUtil.setTag(child, 'tau', Room.tau);
    if Room.CompoFloor <> nil then
      TXmlUtil.setTag(child, 'floor-composition', Room.CompoFloor.nom)
    else
      TXmlUtil.setTag(child, 'floor-composition', '');
    if Room.CompoCeiling <> nil then
      TXmlUtil.setTag(child, 'ceiling-composition', Room.CompoCeiling.nom)
    else
      TXmlUtil.setTag(child, 'ceiling-composition', '');
  end;
  TXmlUtil.setTag(child, 'picture-name', NomPicture);
  TXmlUtil.setTag(child, 'dec-x', decX);
  TXmlUtil.setTag(child, 'dec-y', decY);
end;


end.
