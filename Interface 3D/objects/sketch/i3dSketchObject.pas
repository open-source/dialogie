unit i3dSketchObject;

interface

uses
  i3dGeometry,
  classes,
  Windows,
  Controls,
  Graphics,
  sysutils,
  Clipbrd,
  i3dTranslation,
  dialogs,
  forms,
  checklst,
  Xml.XMLIntf,
  IzObjectPersist,
  i3dSketchLevel,
  Vcl.ExtCtrls;

type
  TSketchObject = class(TObjectPersist)
  strict private
    fCurrentLevel: Integer;
  private
    procedure initLevels; // Number of the current level
  protected
    procedure setCurrentLevel(rank: Integer);
    procedure innerLoadXml(node: IXMLNode); override;
    procedure innerSaveXml(node: IXMLNode); override;
  public
    InfoCleared: boolean;
    HelpInfos: TStringList;
    levels: TList; // List of TSketchLevel
    Modified: Boolean; // True if modified since last calculation
    LastPoint: LTPoint; // Last point created
    LastSeg: LTSegment; // Last segment used
    CurrentPoint: LTPoint; // Current created point
    Lastclick: Tpoint; // Last Click position
    GridType: Integer; // 0 = no Grid, 1 = grey square, 2 = black points
    Selection: TList; // Contain the list of points selected
    Scale: Double; // Current scale (Zoom) coefficient
    DisplayLength: Boolean; // Display length of the wall
    Zoom: Integer; // Coefficient of zoom
    Center: Tpoint;
    CaseName: string;
    LastRoom: TRoom;
    Buff1, Buff2: Tpoint;
    Buff3: TMouseButton;
    Buff4: Integer;
    Buff5: Integer;
    Buff6: TshiftState;
    Stored: Boolean;
    Lattitude: Double;
    SpecialDisplay: Boolean;
    OrientationNord: Double;
    imageUi : TImage;

    property currentLevel: Integer read fCurrentLevel write setCurrentLevel;
    constructor Create;override; // Constructor
    destructor Free; // destructor
    procedure Action(MouseDownPos, MouseUpPos: Tpoint; MouseButton: TMouseButton; MouseState: Integer; CurrentAction: Integer; ShiftKey: TshiftState);
    function StoreAction(MouseDownPos, MouseUpPos: Tpoint; MouseButton: TMouseButton; MouseState: Integer; CurrentAction: Integer; ShiftKey: TshiftState): Boolean;
    procedure Refresh();
    procedure MoveMouse(X, Y: Integer; MouseButton: TMouseButton; MouseState, CurrentAction: Integer);
    procedure MoveCenter(X, Y: Integer);
    procedure SelectionCalculation(Lastclick, CurrentClick: Tpoint; Selection: TList);
    procedure Move2(X, Y: Double; Level: Integer);
    procedure InitTool;
    procedure loadFromList(List: TStringList; var Index: Integer);
    procedure SaveToList(List: TStringList);
    procedure LoadFromFile(FileName: string);
    procedure AlignSelection;
    procedure Rotation(Angle: Double);
    procedure FlipHorizontal;
    procedure FlipVertical;
    procedure nextLevel;
    procedure PreviousLevel;
    procedure PhotoProject(Undo: TList; var IndexUndo: Integer);
    procedure CopySelection;
    procedure CutSelection;
    procedure ClearSelection;
    procedure PasteSelection;
    procedure FinishAnalyze(Sender: Tobject);
    procedure SelectAll(SBitmap: TBitmap);
    procedure DisplayInfo(Infos: TStringList; X, Y: Integer);
    procedure MoveCenter2(p1, p2: Tpoint);
    procedure CalculateSum;
    procedure Move(Vecteur: Tpoint; Level: Integer);
    procedure AlignPoints;
    procedure Recaler;
    procedure TraiteToit;
  end;

  TProject = class(TObjectPersist)
  protected
    procedure innerLoadXml(node: IXMLNode); override;
    procedure innerSaveXml(node: IXMLNode); override;
  public
    sketchObject: TSketchObject;
    lastFileName: string; // Last name and path use to save the project
    Saved: Boolean; // True if the project has been saved since the
    // Last modification, False if into saved.
    constructor Create;override; // Constructor
    destructor Destroy; override; // destructor
    procedure ClearSketchObject;
    procedure LoadFromFile(FileName: string);
    procedure SaveToFile(FileName: string);
    function loadXml(FileName: string): boolean;overload;
    procedure saveXml(FileName: string);overload;
    procedure PrintCurrentLevel;
    procedure SaveCurrentLevel(fileName : string);
    procedure Print;
    function currentLevel: TSketchLevel;
  end;


var
  Project: TProject;


implementation

uses
  i3dConstants,
  XmlUtil,
  i3dBuildingData,
  i3dUtil, IzApp;

procedure TSketchObject.TraiteToit;
var
  Toit, NPrec: TSketchLevel;
  p1, p2, ADdpoint: LTPoint;
  i, j, k: Integer;
  Trouve: Boolean;
  Seg, Seg2: LTSegment;
  Centre: Double;
  fenetre: TFenetre;
begin
  // D�terminer les niveaux concern�s
  Toit := nil;
  NPrec := nil;
  for i := 0 to levels.Count - 1 do
  begin
    if TSketchLevel(levels[i]).toiture then
    begin
      Toit := levels[i];
      NPrec := levels[i - 1];
    end;
  end;
  // Tester les points
  for i := 0 to Toit.points.Count - 1 do
  begin
    p1 := LTPoint(Toit.points[i]);
    Trouve := False;
    for j := 0 to NPrec.points.Count - 1 do
    begin
      p2 := LTPoint(NPrec.points[j]);
      if (p1.X = p2.X) and (p1.Y = p2.Y) then
        Trouve := True;
    end;
    if not Trouve then
    begin
      ADdpoint := LTPoint.Create(Point(p1.X, p1.Y));

      Seg := NPrec.PointOnSegment(p1);
      if Seg <> nil then
      begin
        Seg2 := LTSegment.Create(Seg.A, ADdpoint);
        Seg2.Perimeter := Seg.Perimeter;
        Seg2.IsVirtual := Seg.IsVirtual;
        Seg2.PercentageGlazing := Seg.PercentageGlazing; // inutile
        Seg2.NumberGlazing := Seg.NumberGlazing;
        Seg2.GlazingColor := Seg.GlazingColor;
        Seg2.Shadings := Seg.Shadings;
        Seg2.Orientation := Seg.Orientation;

        // Repositionnier les vitrages
        for fenetre in Seg.ListeFenetres do
        begin
          Seg.distance := Sqrt(Sqr(Seg.B.X - Seg.A.X) + Sqr(Seg.B.Y - Seg.A.Y));
          fenetre.PositionAbsolue := Seg.distance * fenetre.Center;
        end;
        Seg.A := ADdpoint;
        NPrec.segments.add(Seg2);

        for k := Seg.ListeFenetres.Count - 1 downto 0 do
        begin
          fenetre := Seg.ListeFenetres[k];
          Seg.distance := Sqrt(Sqr(Seg.B.X - Seg.A.X) + Sqr(Seg.B.Y - Seg.A.Y));
          Seg2.distance := Sqrt(Sqr(Seg2.B.X - Seg2.A.X) + Sqr(Seg2.B.Y - Seg2.A.Y));
          Centre := fenetre.PositionAbsolue / Seg2.distance;
          if Centre <= 1 then
          begin
            fenetre.Center := Centre;
            Seg2.ListeFenetres.add(fenetre);
            Seg.ListeFenetres.DElete(k);
          end
          else
          begin
            fenetre.PositionAbsolue := fenetre.PositionAbsolue - Seg2.distance;
            Centre := fenetre.PositionAbsolue / Seg.distance;
            fenetre.Center := Centre;
          end;
        end;
      end;
    end;
  end;
end;

procedure TSketchObject.Recaler;
var
  i: Integer;
begin
  for i := 0 to levels.Count - 1 do
  begin
    TSketchLevel(levels[i]).Recaler(Center);
  end;
  Center.X := 0;
  Center.Y := 0;
end;

function TProject.currentLevel: TSketchLevel;
begin
  currentLevel := sketchObject.levels[sketchObject.currentLevel];
end;

procedure TSketchObject.Move(Vecteur: Tpoint; Level: Integer);
begin
  if Level = -1 then
    Level := currentLevel;
  with TSketchLevel(levels[Level]) do
  begin
    Vecteur.X := Round(Vecteur.X * Scale);
    Vecteur.Y := Round(Vecteur.Y * Scale);
    DecX := DecX + Vecteur.X;
    DecY := DecY + Vecteur.Y;
  end;
end;

procedure TSketchObject.Move2(X, Y: Double; Level: Integer);
begin
  if Level = -1 then
    Level := currentLevel;
  with TSketchLevel(levels[Level]) do
  begin
    DecX := Round(DecX + X);
    DecY := Round(DecY + Y);
  end;
end;


// *********************************************
// CONSTRUCTOR
// *********************************************

constructor TSketchObject.Create;
begin
  inherited Create;
  innerTagName := 'building';
  InfoCleared := true;
  SpecialDisplay := False;
  HelpInfos := TStringList.Create;
  Modified := True;
  levels := TList.Create;
  Selection := TList.Create;
  GridType := 1;
  Scale := 1280;
  Zoom := 64;
  Center.X := 0;
  Center.Y := 0;
  // Automatic creation of the first level
  currentLevel := levels.add(TSketchLevel.Create(Self));
  CaseName := '';
  DisplayLength := True;
  OrientationNord := 0;
end;

constructor TProject.Create;
begin
  inherited Create;
  Saved := True;
  lastFileName := '';
  sketchObject := TSketchObject.Create;
end;

destructor TProject.Destroy;
begin
  sketchObject.Free;
  inherited Destroy;
end;

procedure TSketchObject.CalculateSum;
begin
end;

procedure TProject.ClearSketchObject;
begin
  sketchObject.Free;
  sketchObject := TSketchObject.Create;
end;

procedure TProject.LoadFromFile(FileName: string);
var
  List: TStringList;
  Index: Integer;

  sExt : string;
begin
  lastFileName := FileName;
  Saved := True;
  sExt := LowerCase(ExtractFileExt(FileName));
  if sExt = internalX3DFileExt then
  begin
    loadXml(fileName);
    exit;
  end;



  List := TStringList.Create;
  List.LoadFromFile(FileName);
  index := 0;
  sketchObject.loadFromList(List, index);
  buildingData.loadFromList(List, index);
  List.Free;
end;

function TProject.loadXml(fileName: string): boolean;
var
  xmlFile: TBasicXMLFile;
begin
  result := false;
  if not (fileExists(fileName)) then
  begin
    exit;
  end;
  xmlFile := TBasicXMLFile.Create(fileName);
  if (xmlFile.RootNode <> nil) then
  begin
    loadXml(xmlFile.RootNode);
    result := true;
  end;
  xmlFile.Free;
end;

procedure TProject.saveXml(fileName: string);
var
  xmlFile: TBasicXMLFile;
begin
  xmlFile := TBasicXMLFile.getEmptyFile(fileName, innerTagName);
  innerSaveXml(xmlFile.rootNode);
  xmlFile.saveToDisk();
  xmlFile.Free;
end;

procedure TProject.SaveToFile(FileName: string);
var
  List: TStringList;
begin
  lastFileName := FileName;
  Saved := True;
  // store the file name for next saves
  List := TStringList.Create;
  sketchObject.SaveToList(List);
  buildingData.SaveToList(List);
  List.SaveToFile(FileName);
  List.Free;
end;

procedure TProject.innerLoadXml(node: IXMLNode);
begin
  sketchObject.loadParentXml(node);
  buildingData.loadParentXml(node);
end;

procedure TProject.innerSaveXml(node: IXMLNode);
begin
  sketchObject.saveXml(node);
  buildingData.saveXml(node);
end;


// *********************************************
// DESTRUCTOR
// *********************************************

destructor TSketchObject.Free;
var
  i: Integer;
begin
  // Release all levels
  for i := levels.Count - 1 downto 0 do
    TSketchLevel(levels[i]).Free;

  levels.Free;
  Selection.Free;
  inherited Destroy;
end;

function TSketchObject.StoreAction(MouseDownPos, MouseUpPos: Tpoint; MouseButton: TMouseButton; MouseState: Integer; CurrentAction: Integer; ShiftKey: TshiftState): Boolean;
begin
  with TSketchLevel(levels[currentLevel]) do
  begin
    if ActiveThread then
    begin
      stored := True;
      Buff1 := MouseDownPos;
      Buff2 := MouseUpPos;
      Buff3 := MouseButton;
      Buff4 := MouseState;
      Buff5 := CurrentAction;
      Buff6 := ShiftKey;
      result := True
    end
    else
      result := False;
  end
end;


// *********************************************
// This method load a project from a filename
// The text file is loaded into a list. Then the
// list is browsed to create the data (Index is the
// current line of the list)
// *********************************************
procedure TSketchObject.LoadFromFile(FileName: string);
var
  List: TStringList; // file data
  Lev: TSketchLevel;
  i: Integer;
  Index: Integer; // Index of the current line of the list
  nbLevel, Error: Integer;
begin
  initLevels;

  // Load file data into a list a strings
  List := TStringList.Create;
  List.LoadFromFile(FileName);

  // Set variables
  index := 0;
  inc(index);
  Val(List[index], Scale, Error);
  inc(index);
  nbLevel := StrToInt(List[index]);
  inc(index);
  currentLevel := StrToInt(List[index]);
  inc(index);

  // the level 0 is always previously created, so load it
  try
    TSketchLevel(levels[0]).loadFromList(List, index);
  except
    on Exception do
      ;
  end;

  // If there is more than 1 level, it is necessary to create them
  // before to load them
  for i := 1 to nbLevel - 1 do
  begin
    Lev := TSketchLevel.Create(Self);
    levels.add(Lev);
    try
      Lev.loadFromList(List, index);
    except
      on Exception do
        ;
    end;

  end;
  List.Free;
end;

procedure TSketchObject.initLevels;
var
  Lev: TSketchLevel;
  i: Integer;
begin
  for i := 0 to levels.Count - 1 do
  begin
    Lev := TSketchLevel(levels[i]);
    Lev.Free;
  end;
  levels.clear;
  currentLevel := levels.add(TSketchLevel.Create(Self));
end;




// *********************************************
// This method load a project from a filename
// The text file is loaded into a list. Then the
// list is browsed to create the data (Index is the
// current line of the list)
// *********************************************

procedure TSketchObject.loadFromList(List: TStringList; var Index: Integer);
var
  Lev: TSketchLevel;
  i: Integer;
  nbLevel, Error: Integer;
begin
  initLevels;

  // Set variables
  // CaseName := List[Index];
  Val(List[index], OrientationNord, Error);
  inc(index);
  Val(List[index], Scale, Error);
  inc(index);
  Val(List[index], Lattitude, Error);
  inc(index);
  Zoom := StrToInt(List[index]);
  inc(index);
  Center.X := StrToInt(List[index]);
  inc(index);
  Center.Y := StrToInt(List[index]);
  inc(index);
  nbLevel := StrToInt(List[index]);
  inc(index);
  currentLevel := StrToInt(List[index]);
  inc(index);
  if List[index] = 'Pa' then
  begin
    inc(index);
    inc(index);
  end;
  DisplayLength := List[index] = '1';
  inc(index);
  if index >= List.Count then
  begin
    exit;
  end;

  // the level 0 is always previously created, so load it
  TSketchLevel(levels[0]).loadFromList(List, index);

  // If there is more than 1 level, it is necessary to create them
  // before to load them
  for i := 1 to nbLevel - 1 do
  begin
    Lev := TSketchLevel.Create(Self);
    levels.add(Lev);
    Lev.loadFromList(List, index);
  end;
end;

procedure TSketchObject.SaveToList(List: TStringList);
var
  i: Integer;
  TempString: string;
  levelCount: Integer;
begin
  // Set variables
  // List.add(CaseName);
  Str(OrientationNord, TempString);
  List.add(TempString);
  Str(Scale, TempString);
  List.add(TempString);
  Str(Lattitude, TempString);
  List.add(TempString);
  List.add(IntToStr(Zoom));
  List.add(IntToStr(Center.X));
  List.add(IntToStr(Center.Y));
  levelCount := 0;
  for i := 0 to levels.Count - 1 do
    if (TSketchLevel(levels[i]).points.Count > 0) and (TSketchLevel(levels[i]).segments.Count > 0) then
      inc(levelCount);

  List.add(IntToStr(levelCount));
  if currentLevel > (levelCount - 1) then
    currentLevel := levelCount - 1;
  List.add(IntToStr(currentLevel));

  List.add('Pa');
  List.add('True');
  if DisplayLength then
    List.add('1')
  else
    List.add('0');
  // set each level
  for i := 0 to levels.Count - 1 do
    TSketchLevel(levels[i]).SaveToList(List);
end;

procedure TSketchObject.innerSaveXml(node: IXMLNode);
var
  i: Integer;
  TempString: string;
  levelCount: Integer;
  nodeC: IXmlNode;
  lev: TSketchLevel;
begin
  TXmlUtil.setTag(node, 'orientation-nord', OrientationNord);
  TXmlUtil.setTag(node, 'scale', Scale);
  TXmlUtil.setTag(node, 'latitude', Lattitude);
  TXmlUtil.setTag(node, 'zoom', Zoom);
  TXmlUtil.setTag(node, 'center-x', Center.X);
  TXmlUtil.setTag(node, 'center-y', Center.Y);

  nodeC := TXmlUtil.addChild(node, 'levels');
  // set each level
  levelCount := 0;
  for i := 0 to levels.Count - 1 do
  begin
    lev := TSketchLevel(levels[i]);
    if (lev.points.count = 0) and (lev.segments.count = 0) then
      continue;
    inc(levelCount);
    lev.saveXml(nodeC);
  end;
  if currentLevel > (levelCount - 1) then
    currentLevel := levelCount - 1;
  TXmlUtil.setAtt(nodeC, 'current', currentLevel);
  TXmlUtil.setAtt(nodeC, 'display-length', DisplayLength);
end;

procedure TSketchObject.innerLoadXml(node: IXMLNode);
var
  Lev: TSketchLevel;
  i: Integer;
  nodeC: IXmlNode;
  child: IXmlNode;
begin
  initLevels;
  OrientationNord := TXmlUtil.getTagDouble(node, 'orientation-nord');
  Scale := TXmlUtil.getTagDouble(node, 'scale');
  Lattitude := TXmlUtil.getTagDouble(node, 'latitude');
  Zoom := TXmlUtil.getTagInt(node, 'zoom');
  Center.X := TXmlUtil.getTagInt(node, 'center-x');
  Center.Y := TXmlUtil.getTagInt(node, 'center-y');
  nodeC := TXmlUtil.getChild(node, 'levels');

  currentLevel := TXmlUtil.getAttInt(nodeC, 'current');
  DisplayLength := TXmlUtil.getAttBool(nodeC, 'display-length');
  for i := 0 to nodeC.ChildNodes.Count - 1 do
  begin
    child := nodeC.ChildNodes[i];
    if i <> 0 then
    begin
      Lev := TSketchLevel.Create(Self);
      levels.add(Lev);
    end
    else
    begin
      lev := TSketchLevel(levels[0]);
    end;
    Lev.loadXml(child);
  end;
end;


// *******************************************************
// This method save a project into the current undo list
// A strings List is filled with the data and then
// saved into text file.
// *******************************************************

procedure TSketchObject.PhotoProject(Undo: TList; var IndexUndo: Integer);
var
  List: TStringList;
  oldLevel: Integer;
begin
  oldLevel := currentLevel;
  if IndexUndo = MaxUndo then
  begin
    Undo.add(Undo[0]);
    Undo.DElete(0);
    dec(IndexUndo);
  end;
  inc(IndexUndo);
  List := TStringList(Undo.Items[IndexUndo]);
  List.clear;
  Self.SaveToList(List);
  Undo.Items[IndexUndo] := List;
  currentLevel := oldLevel;
end;



// *******************************************************
// This method allow to change the position of the center
// of the current level of the sketch
// *******************************************************

procedure TSketchObject.MoveCenter(X, Y: Integer);
var
  p1, p2: Tpoint;
begin
  // define the vector
  p1.X := 0;
  p1.Y := 0;
  p2.X := X;
  p2.Y := Y;
  // Apply the move center
  MoveCenter2(p1, p2);
end;

// *********************************************************
// This method will interpret the differents user's action
// and call the appropriate methods
// *********************************************************

procedure TSketchObject.Action(MouseDownPos, MouseUpPos: Tpoint; MouseButton: TMouseButton; MouseState: Integer; CurrentAction: Integer; ShiftKey: TshiftState);
var
  Point1, POint2: LTPoint;
  popos: Tpoint;
begin
  // A modification is happening, so the project is not saved
  if StoreAction(MouseDownPos, MouseUpPos, MouseButton, MouseState, CurrentAction, ShiftKey) then
    exit;
  Project.Saved := False;
  Modified := True;
  case CurrentAction of

    // Toiture
    25:
      if MouseState = 0 then
      begin
        TSketchLevel(levels[currentLevel]).SetRoof(MouseDownPos);
      end;
    // LINE and POINT tool
        1:
      if MouseState = 1 then
      // when the user press a mouse button, creation of a first point
      begin
        LastPoint := TSketchLevel(levels[currentLevel]).PointCreation(MouseDownPos);
        refresh;
      end
      else if MouseState = 0 then
      begin
        // when the user release the mouse button, creation of the second point
        // and creation of a segment between the two points
        CurrentPoint := TSketchLevel(levels[currentLevel]).PointCreation(MouseUpPos);
        TSketchLevel(levels[currentLevel]).SegmentCreation(LastPoint, CurrentPoint);
        TSketchLevel(levels[currentLevel]).Analyze;
      end;
    // RECTANGLE Tool
        4:
      if MouseState = 1 then
      // when the user press a mouse button, creation of a first point
      begin
        Lastclick := MouseDownPos;
        LastPoint := TSketchLevel(levels[currentLevel]).PointCreation(MouseDownPos);
        refresh;
      end
      else if (MouseState = 0) and (LastPoint <> nil) then
      // when the user release the mouse button, creation of the three
      // other points and creation of 4 segments between the 4 points
      begin
        CurrentPoint := TSketchLevel(levels[currentLevel]).PointCreation(MouseUpPos);
        Lastclick.X := LastPoint.X + Center.X;
        Lastclick.Y := CurrentPoint.Y + Center.Y;
        Point1 := TSketchLevel(levels[currentLevel]).PointCreation(Lastclick);
        Lastclick.X := CurrentPoint.X + Center.X;
        Lastclick.Y := LastPoint.Y + Center.Y;
        POint2 := TSketchLevel(levels[currentLevel]).PointCreation(Lastclick);
        TSketchLevel(levels[currentLevel]).SegmentCreation(LastPoint, Point1);
        TSketchLevel(levels[currentLevel]).SegmentCreation(Point1, CurrentPoint);
        TSketchLevel(levels[currentLevel]).SegmentCreation(CurrentPoint, POint2);
        TSketchLevel(levels[currentLevel]).SegmentCreation(POint2, LastPoint);
        TSketchLevel(levels[currentLevel]).Analyze;
      end;
    // CONTINUOUS LINE
        3:
      if MouseState = 0 then
      begin
        if MouseButton = mbLeft then
          if LastPoint = nil then
          // if left click and it is the first point, then creation of
          // one point
          begin
            LastPoint := TSketchLevel(levels[currentLevel]).PointCreation(MouseDownPos);
            TSketchLevel(levels[currentLevel]).Analyze;
          end
          else
          // if left click and it is NOT the first point, then creation
          // of one point and creation of a segment between the last
          // point and the current point
          begin
            CurrentPoint := TSketchLevel(levels[currentLevel]).PointCreation(MouseDownPos);
            TSketchLevel(levels[currentLevel]).SegmentCreation(LastPoint, CurrentPoint);
            LastPoint := CurrentPoint;
            TSketchLevel(levels[currentLevel]).Analyze;
          end
        else
            // if right click, interrupt the continuous line
          LastPoint := nil;
        TSketchLevel(levels[currentLevel]).Analyze;
      end;
    // ZOOM SELECTION
        17:
      if MouseState = 1 then
        Lastclick := MouseDownPos
      else if MouseState = 0 then
      begin
        // TRAITEMENT ZOOM
        TSketchLevel(levels[currentLevel]).ZoomRect(MouseUpPos, Lastclick)
      end;
    // MOVE and ROTATE Tool
        2:
      if MouseState = 1 then
      begin
        if Selection.Count = 0 then
        // Case : mouse button pressed, Selection Empty
        begin
          LastPoint := TSketchLevel(levels[currentLevel]).PointExists(MouseDownPos);
          if (LastPoint <> nil) and (not LastPoint.Indeplacable) then
            // if the click was on a point, ADD SELECTION
            Selection.add(LastPoint)
          else
          // else BEGIN SELECTION RECTANGLE
          begin
            Lastclick := MouseDownPos;
          end;
        end
        else
        // Case : mouse button pressed, Selection not Empty
        begin
          LastPoint := TSketchLevel(levels[currentLevel]).PointExists(MouseDownPos);
          if (MouseButton = mbRight) then
          // if Right click then keep position (Center of rotation)
          // BEGIN ROTATION
          begin
            Lastclick := MouseDownPos;
            popos := Mouse.CursorPos;
            popos.X := popos.X + 20;
            Mouse.CursorPos := popos;
          end
          else if (LastPoint = nil) or (MouseButton = mbRight) then
          // if no point on click, BEGIN SELECTION RECTANGLE
          begin
            // if Shift key, don't clear selection
            if ShiftKey * [ssShift] <> [ssShift] then
              Selection.clear;
            Lastclick := MouseDownPos;
            refresh;
          end
          else if Selection.IndexOf(LastPoint) = -1 then
          // if click on a point not include in selection
          begin
            // if Shift key, don't clear selection
            if ShiftKey * [ssShift] <> [ssShift] then
              Selection.clear;
            // add it to selection and BEGIN MOVE
            Selection.add(LastPoint);
            refresh;
          end
        end
      end
      else if MouseState = 0 then
      begin
        if (LastPoint = nil) and (MouseButton = mbLeft) then
        // Case : mouse button release, END SELECTION RECTANGLE
        begin
          SelectionCalculation(Lastclick, MouseUpPos, Selection);
          refresh;
        end
        else if (MouseButton = mbLeft) then
        // Case : mouse Left button release , END MOVE
        begin
          TSketchLevel(levels[currentLevel]).MovePoints(Selection, LastPoint, MouseUpPos);
          TSketchLevel(levels[currentLevel]).Analyze;
        end
        else if (MouseButton = mbRight) then
        // Case : mouse Right button release, END ROTATE
        begin
          TSketchLevel(levels[currentLevel]).RotatePoints(Selection, Lastclick, MouseUpPos);
          TSketchLevel(levels[currentLevel]).Analyze;
        end
      end;
    // MOVE CENTER : Keep the mouse position to move center
        10:
      if MouseState = 1 then
        Lastclick := MouseDownPos;
    280:
      if MouseState = 1 then
        Lastclick := MouseDownPos;
    // ERASER tool
          5:
      if (MouseState = 1) then
      // Delete point and lines that are under the cursor
      begin
        if TSketchLevel(levels[currentLevel]).DeletePointsLines(MouseDownPos.X, MouseDownPos.Y) then
        // if some points or line were deleted, refresh sketch
        begin
          refresh;
        end
      end
      else
        TSketchLevel(levels[currentLevel]).Analyze;
    // PROJECTION tool
          6:
      if MouseState = 1 then
      begin
        if Selection.Count = 0 then
        // Case : Mouse button pressed, Selection empty
        begin
          LastPoint := TSketchLevel(levels[currentLevel]).PointExists(MouseDownPos);
          if LastPoint <> nil then
          // if click on a point ADD TO SELECTION / BEGIN MOVE
          begin
            Selection.add(LastPoint);
            refresh;
          end
          else
          begin
            // Else BEGIN SELECTION RECTANGLE
            Lastclick := MouseDownPos;
          end;
        end
        else
        // Case : Mouse button pressed, Selection NOT empty
        begin
          LastPoint := TSketchLevel(levels[currentLevel]).PointExists(MouseDownPos);
          if (MouseButton = mbRight) then
            // if rightClick : BEGIN PROJECTION
            Lastclick := MouseDownPos
          else if (LastPoint = nil) or (MouseButton = mbRight) then
          // if LeftClick, no point : BEGIN SELECTION RECTANGLE
          begin
            if ShiftKey * [ssShift] <> [ssShift] then
              Selection.clear;
            Lastclick := MouseDownPos;
          end
          else if Selection.IndexOf(LastPoint) = -1 then
          // If left Click, point not included in selection
          // ADD SELECTION / BEGIN MOVE
          begin
            if ShiftKey * [ssShift] <> [ssShift] then
              Selection.clear;
            Selection.add(LastPoint);
            refresh;
          end
        end
      end
      else if MouseState = 0 then
      // Case : Mouse button released, Selection empty
      begin
        if (LastPoint = nil) and (MouseButton = mbLeft) then
        // END SELECTION RECTANGLE
        begin
          SelectionCalculation(Lastclick, MouseUpPos, Selection);
          refresh;
        end
        else if (MouseButton = mbLeft) then
        begin
          // END MOVE
          TSketchLevel(levels[currentLevel]).MovePoints(Selection, LastPoint, MouseUpPos);
          TSketchLevel(levels[currentLevel]).Analyze;
        end
        else if (MouseButton = mbRight) then
        begin
          // END PROJECTION
          TSketchLevel(levels[currentLevel]).ProjectPoints(Selection, Lastclick, MouseUpPos);
          TSketchLevel(levels[currentLevel]).Analyze;
        end
      end;
    16:
      if MouseState = 0 then
      begin
        TSketchLevel(levels[currentLevel]).SetExternalSpace(MouseDownPos);
        TSketchLevel(levels[currentLevel]).Analyze;
      end;
    27:
      if MouseState = 0 then
      begin
        TSketchLevel(levels[currentLevel]).SetLocal(MouseDownPos);
        TSketchLevel(levels[currentLevel]).Analyze;
      end;
    7:
      if (MouseState = 0) then
      begin
        if currentLevel = 0 then
        begin
          if TSketchLevel(levels[currentLevel]).SetShading(MouseDownPos) then
            TSketchLevel(levels[currentLevel]).Analyze
          else
            imageUi.Repaint;
        end
        else
          MessageDLG(Labels[39], mtwarning, [mbOk], 0);
      end;
    11:
      if MouseState = 0 then
      begin
        TSketchLevel(levels[currentLevel]).SetNone(MouseDownPos);
        TSketchLevel(levels[currentLevel]).Analyze;
      end;
    12:
      if MouseState = 0 then
      begin
        Point1 := TSketchLevel(levels[currentLevel]).PointExists(MouseDownPos);
        if Point1 <> nil then
        begin
          // TSketchLevel(Levels[CurrentLevel]).SetPontPignon(MouseDownPos,LastRoom);
        end
        else if TSketchLevel(levels[currentLevel]).SetWall(MouseDownPos) then
          TSketchLevel(levels[currentLevel]).Analyze
        else
          imageUi.Repaint;
      end;
    26:
      if MouseState = 0 then
      begin
        if TSketchLevel(levels[currentLevel]).SetCompo(MouseDownPos) then
          TSketchLevel(levels[currentLevel]).Analyze
        else
          imageUi.Repaint;
      end;
    15:
      if MouseState = 1 then
      begin
        LastPoint := LTPoint.Create(MouseDownPos);
        LastPoint.X := LastPoint.X - Center.X;
        LastPoint.Y := LastPoint.Y - Center.Y;
        LastSeg := TSketchLevel(levels[currentLevel]).PointOnSegment(LastPoint);
        LastPoint.Free;
      end
      else if LastSeg <> nil then
      begin
        TSketchLevel(levels[currentLevel]).CurvePoints(LastSeg, MouseUpPos.X, MouseUpPos.Y, imageUi.picture.bitmap);
        TSketchLevel(levels[currentLevel]).Analyze;
      end;
    18:
      if (MouseState = 0) then
      begin
        TSketchLevel(levels[currentLevel]).SetZone(MouseDownPos);
        TSketchLevel(levels[currentLevel]).Analyze
      end;
    // creation porte
        19:
      if MouseState = 0 then
      begin
        TSketchLevel(levels[currentLevel]).CreateDoor(MouseUpPos);
        TSketchLevel(levels[currentLevel]).Analyze;
      end;
    // cr�ation fen�tre
        20:
      if MouseState = 0 then
      begin
        TSketchLevel(levels[currentLevel]).CreateWindow(MouseUpPos);
        TSketchLevel(levels[currentLevel]).Analyze;
      end;
    // edition porte/fen�tre
        21:
      if MouseState = 0 then
      begin
        if TSketchLevel(levels[currentLevel]).PorteSelectionnee <> nil then
        begin
          TSketchLevel(levels[currentLevel]).EditPorte;
          TSketchLevel(levels[currentLevel]).Analyze;
        end
        else if TSketchLevel(levels[currentLevel]).FenetreSelectionnee <> nil then
        begin
          TSketchLevel(levels[currentLevel]).EditFenetre;
          TSketchLevel(levels[currentLevel]).Analyze;
        end
      end;
    // suppression porte/fenetre
        22:
      if MouseState = 0 then
      begin
        TSketchLevel(levels[currentLevel]).DeleteDoorWindow;
        TSketchLevel(levels[currentLevel]).Analyze;
      end;
  end;
end;

// *********************************************************
// This method will interpret the moving of the mouse on
// the sketch.
// *********************************************************

procedure TSketchObject.MoveMouse(X, Y: Integer; MouseButton: TMouseButton; MouseState, CurrentAction: Integer);
var
  TempPoint: Tpoint;
  p: Tpoint;
  sBitmap : TBitmap;
begin
  sBitmap := imageUi.picture.bitmap;
  case CurrentAction of
    // TOITURE
    25:
      begin
        LastRoom := TSketchLevel(levels[currentLevel]).FlashHeight(X, Y, SBitmap, GridType);
        imageUi.Repaint;
      end;


    // LINE AND POINT
        1:
      if MouseState = 1 then
      begin
        TSketchLevel(levels[currentLevel]).FlashLine(X, Y, LastPoint, SBitmap, GridType);
        imageUi.Repaint;
      end;
    // Section Line
        14:
      if MouseState = 1 then
      begin
        TSketchLevel(levels[currentLevel]).FlashSection(X, Y, LastPoint, SBitmap, GridType);
        imageUi.Repaint;
      end;
    // CONTINUOUS LINE
        3:
      if LastPoint <> nil then
      begin
        TSketchLevel(levels[currentLevel]).FlashLine(X, Y, LastPoint, SBitmap, GridType);
        imageUi.Repaint;
      end;
    //
        4:
      if (MouseState = 1) then
      begin
        TSketchLevel(levels[currentLevel]).FlashRectTool(Lastclick, X, Y, SBitmap, GridType);
        imageUi.Repaint
      end;
    // ZOOM SELECTION
        17:
      if (MouseState = 1) and (LastPoint = nil) then
      begin
        TSketchLevel(levels[currentLevel]).FlashRect(Lastclick, X, Y, SBitmap, GridType);
        imageUi.Repaint
      end;
    // MOVE AND ROTATE
        2:
      if (MouseState = 1) and (LastPoint <> nil) and (MouseButton = mbLeft) then
      begin
        // MOVE
        TSketchLevel(levels[currentLevel]).FlashPoint(X, Y, LastPoint, Selection, SBitmap, GridType);
        imageUi.Repaint
      end
      else if (MouseState = 1) and (MouseButton = mbRight) then
      begin
        // ROTATE
        TSketchLevel(levels[currentLevel]).FlashRotate(X, Y, Lastclick, Selection, SBitmap, GridType);
        imageUi.Repaint
      end
      else if (MouseState = 1) and (LastPoint = nil) then
      begin
        // SELECTION RECTANGLE
        TSketchLevel(levels[currentLevel]).FlashRect(Lastclick, X, Y, SBitmap, GridType);
        imageUi.Repaint
      end;
    // MOVE CENTER
        10:
      if (MouseState = 1) then
      begin
        TempPoint.X := X;
        TempPoint.Y := Y;
        MoveCenter2(Lastclick, TempPoint);
        Lastclick := TempPoint;
        refresh;
        imageUi.Repaint;
      end;
    280:
      if (MouseState = 1) then
      begin
        TempPoint.X := X;
        TempPoint.Y := Y;
        Move2(TempPoint.X - Lastclick.X, TempPoint.Y - Lastclick.Y, currentLevel);
        Lastclick := TempPoint;
        refresh;
        imageUi.Repaint;
      end;
    // ERASER
        5:
      if (MouseState = 1) then
        if TSketchLevel(levels[currentLevel]).DeletePointsLines(X, Y) then
        begin
          refresh;
          imageUi.Repaint;
        end;
    // MOVE / PROJECTION
          6:
      if (MouseState = 1) and (LastPoint <> nil) and (MouseButton = mbLeft) then
      begin
        // MOVE
        TSketchLevel(levels[currentLevel]).FlashPoint(X, Y, LastPoint, Selection, SBitmap, GridType);
        imageUi.Repaint
      end
      else if (MouseState = 1) and (MouseButton = mbRight) then
      begin
        // PROJECTION
        TSketchLevel(levels[currentLevel]).FlashProject(X, Y, Lastclick, Selection, SBitmap, GridType);
        imageUi.Repaint
      end
      else if (MouseState = 1) and (LastPoint = nil) then
      begin
        // SELECTION RECTANGLE
        TSketchLevel(levels[currentLevel]).FlashRect(Lastclick, X, Y, SBitmap, GridType);
        imageUi.Repaint
      end;
    12, 26:
      if MouseState = 0 then
      begin
        HelpInfos := TSketchLevel(levels[currentLevel]).GetWall(Point(X, Y));
        if not infoCleared then
        begin
          refresh;
          infoCleared := True;
        end;
        if HelpInfos.Count <> 0 then
        begin
          p := imageUi.ScreenToClient(Mouse.CursorPos);
          DisplayInfo(HelpInfos, p.X, p.Y);
          imageUi.Repaint;
          infoCleared := False;
        end;
        HelpInfos.Free;
      end;
    18:
      if MouseState = 0 then
      begin
        HelpInfos := TSketchLevel(levels[currentLevel]).GetZone(Point(X, Y));
        if not infoCleared then
        begin
          refresh;
          infoCleared := True;
        end;
        if HelpInfos.Count <> 0 then
        begin
          p := imageUi.ScreenToClient(Mouse.CursorPos);
          DisplayInfo(HelpInfos, p.X, p.Y);
          imageUi.Repaint;
          infoCleared := False;
        end;
        HelpInfos.Free;
      end;
    15:
      if (MouseState = 1) and (LastSeg <> nil) then
      begin
        TSketchLevel(levels[currentLevel]).FlashCurve(LastSeg, X, Y, SBitmap);
        imageUi.Repaint
      end;
    19:
      if MouseState = 0 then
      begin
        TSketchLevel(levels[currentLevel]).FlashDoor(Point(X, Y), SBitmap, GridType);
        imageUi.Repaint
      end;
    20:
      if MouseState = 0 then
      begin
        TSketchLevel(levels[currentLevel]).FlashWindow(Point(X, Y), SBitmap, GridType);
        imageUi.Repaint
      end;
    21, 22:
      begin
        TSketchLevel(levels[currentLevel]).FlashMenuiserie(Point(X, Y), SBitmap, GridType);
        imageUi.Repaint
      end;
  end;
  if MouseState = 1 then
  begin
    if Y < 10 then
    begin
      TempPoint.X := Lastclick.X;
      TempPoint.Y := Round(Lastclick.Y + Zoom * 20);
      MoveCenter2(Lastclick, TempPoint);
      Lastclick := TempPoint;
      refresh;
      imageUi.Repaint;
    end
    else if (Y div Zoom > imageUi.height - 10) then
    begin
      TempPoint.X := Lastclick.X;
      TempPoint.Y := Round(Lastclick.Y - Zoom * 20);
      MoveCenter2(Lastclick, TempPoint);
      Lastclick := TempPoint;
      refresh;
      imageUi.Repaint;
    end;
    if X < 10 then
    begin
      TempPoint.X := Round(Lastclick.X + Zoom * 20);
      TempPoint.Y := Lastclick.Y;
      MoveCenter2(Lastclick, TempPoint);
      Lastclick := TempPoint;
      refresh;
      imageUi.Repaint;
    end
    else if (X div Zoom > imageUi.Width - 10) then
    begin
      TempPoint.X := Round(Lastclick.X - Zoom * 20);
      TempPoint.Y := Lastclick.Y;
      MoveCenter2(Lastclick, TempPoint);
      Lastclick := TempPoint;
      refresh;
      imageUi.Repaint;
    end;
  end;
end;

// *********************************************************
// This method will Refresh the bitmap of the sketch with
// the current level
// *********************************************************

procedure TSketchObject.Refresh();
begin
  TSketchLevel(levels[currentLevel]).Refresh(imageUi.Picture.Bitmap, GridType, Selection);
end;

// *********************************************************
// This method calculate the points that are inside the
// selection rectangle and add them to the selection list
// *********************************************************

procedure TSketchObject.SelectionCalculation(Lastclick, CurrentClick: Tpoint; Selection: TList);
var
  i: Integer;
  Minx, MaxX, MinY, MaxY: Integer;
begin
  // Calculate absolute coordinates
  Lastclick.X := Lastclick.X - Center.X;
  Lastclick.Y := Lastclick.Y - Center.Y;
  CurrentClick.X := CurrentClick.X - Center.X;
  CurrentClick.Y := CurrentClick.Y - Center.Y;
  // sort Left/right
  if Lastclick.X > CurrentClick.X then
  begin
    MaxX := Lastclick.X;
    Minx := CurrentClick.X
  end
  else
  begin
    MaxX := CurrentClick.X;
    Minx := Lastclick.X
  end;
  // Sort Up/down
  if Lastclick.Y > CurrentClick.Y then
  begin
    MaxY := Lastclick.Y;
    MinY := CurrentClick.Y
  end
  else
  begin
    MaxY := CurrentClick.Y;
    MinY := Lastclick.Y
  end;
  with TSketchLevel(levels[currentLevel]) do
  begin
    // Test if each point are in the rectangle
    for i := 0 to points.Count - 1 do
      if not LTPoint(points[i]).Indeplacable then
        if (LTPoint(points[i]).X <= MaxX) and (LTPoint(points[i]).X >= Minx) and (LTPoint(points[i]).Y <= MaxY) and (LTPoint(points[i]).Y >= MinY) then
          Selection.add(points[i]);

  end;
end;

procedure TSketchObject.setCurrentLevel(rank: Integer);
begin
  if rank < 0 then
    rank := 0;
  if rank >= levels.Count then
    rank := levels.Count - 1;
  fCurrentLevel := rank;
end;

// *********************************************************
// This method is called each time the user change of tool
// *********************************************************

procedure TSketchObject.InitTool;
begin
  // initialize all click to 0
  LastPoint := nil;
  Selection.clear;
end;

// *********************************************************
// This method is allow to align all the points on the grid
// *********************************************************

procedure TSketchObject.AlignSelection;
var
  p: LTPoint;
  i: Integer;
begin
  // Select current level
  with TSketchLevel(levels[currentLevel]) do
  begin
    // Align each point
    for i := 0 to Selection.Count - 1 do
    begin
      p := LTPoint(Selection[i]);
      p.X := Round((p.X div Zoom) / gridStep) * gridStep * Zoom;
      p.Y := Round((p.Y div Zoom) / gridStep) * gridStep * Zoom;
    end;
    CheckCross;
    Analyzed := False;
  end
end;

// ************************************************************
// This method will apply a 'Flip horizontal' to the selection
// ************************************************************

procedure TSketchObject.FlipHorizontal;
var
  p: LTPoint;
  i: Integer;
  p1: Tpoint;
begin
  // Select the current level
  with TSketchLevel(levels[currentLevel]) do
  begin
    // calculate the X coordinate of the symetry axe
    p1.X := 0;
    for i := 0 to Selection.Count - 1 do
      p1.X := p1.X + LTPoint(Selection[i]).X;
    p1.X := Round(p1.X / Selection.Count);
    // flip each point of the selection around the axe
    for i := 0 to Selection.Count - 1 do
    begin
      p := LTPoint(Selection[i]);
      p.X := p1.X + (p1.X - p.X);
    end;
    // Check new positions of the points
    CheckCross;
    Analyzed := False;
  end;
end;

// ************************************************************
// This method will apply a 'Flip vertical' to the selection
// ************************************************************

procedure TSketchObject.FlipVertical;
var
  p: LTPoint;
  i: Integer;
  p1: Tpoint;
begin
  // Select current level
  with TSketchLevel(levels[currentLevel]) do
  begin
    // calculate the Y coordinate of the symetry axe
    p1.Y := 0;
    for i := 0 to Selection.Count - 1 do
      p1.Y := p1.Y + LTPoint(Selection[i]).Y;
    p1.Y := Round(p1.Y / Selection.Count);
    // flip each point of the selection around the axe
    for i := 0 to Selection.Count - 1 do
    begin
      p := LTPoint(Selection[i]);
      p.Y := p1.Y + (p1.Y - p.Y);
    end;
    // Check new positions of the points
    CheckCross;
    Analyzed := False;
  end;
end;

// ************************************************************
// This method will apply a 'Rotation' of a given angle
// to the selection
// ************************************************************

procedure TSketchObject.Rotation(Angle: Double);
var
  p1, p2: Tpoint;
  i: Integer;
begin
  if Selection.Count = 0 then
    exit;
  // Select current Level
  with TSketchLevel(levels[currentLevel]) do
  begin
    // Calculate the center of rotation
    p1.X := 0;
    p1.Y := 0;
    for i := 0 to Selection.Count - 1 do
    begin
      p1.X := p1.X + LTPoint(Selection[i]).X;
      p1.Y := p1.Y + LTPoint(Selection[i]).Y;
    end;
    p1.X := Round(p1.X / Selection.Count) + Center.X;
    p1.Y := Round(p1.Y / Selection.Count) + Center.Y;

    // Calculate a mouse position with the given angle
    p2.X := Round(p1.X + Cos(Angle / 180 * pi) * 1000);
    p2.Y := Round(p1.Y + Sin(Angle / 180 * pi) * 1000);
    RotatePoints(Selection, p1, p2);
    // Check new point positions
    CheckCross;
    Analyzed := False;
  end;
end;

// ************************************************************
// This method will to change the current level to the next one
// ************************************************************

procedure TSketchObject.nextLevel;
begin
  // if the level do not exist, create it
  if currentLevel >= levels.Count - 1 then
    levels.add(TSketchLevel.Create(Self));
  currentLevel := currentLevel + 1;
end;

// ************************************************************
// This method change the current level to the
// previous one
// ************************************************************

procedure TSketchObject.PreviousLevel;
begin
  currentLevel := currentLevel - 1;
end;

// ************************************************************
// This method copy the selection of points and segments (that
// are connected to selected points) into the clipboard
// ************************************************************

procedure TSketchObject.CopySelection;
var
  i, Index1, Index2: Integer;
  List: TStringList;
  List2: TStringList;
  Seg: LTSegment;
  TempString: string;
begin
  // Create the mini list for the segments
  List2 := TStringList.Create;

  // Create the main list
  List := TStringList.Create;
  List.add(Labels[3]);
  List.add(IntToStr(Selection.Count));
  List.add(Labels[4]);
  // Save each point of the selection
  for i := 0 to Selection.Count - 1 do
    LTPoint(Selection[i]).SaveToList(List);
  //
  with TSketchLevel(levels[currentLevel]) do
    for i := 0 to segments.Count - 1 do
    begin
      Seg := LTSegment(segments[i]);
      Index1 := Selection.IndexOf(Seg.A);
      if Index1 <> -1 then
      begin
        Index2 := Selection.IndexOf(Seg.B);
        if Index2 <> -1 then
        begin
          List2.add(IntToStr(Index1));
          List2.add(IntToStr(Index2));
          List2.add(IntToStr(Seg.PercentageGlazing));
          List2.add(IntToStr(Seg.NumberGlazing));
          List2.add(IntToStr(Seg.GlazingColor));
          List2.add(IntToStr(Seg.Shadings));
          Str(Seg.Orientation, TempString);
          List2.add(TempString);
        end;
      end;
    end;
  List.add(Labels[5]);
  List.add(IntToStr(List2.Count div 7));
  List.add(Labels[6]);
  List.AddStrings(List2);
  ClipBoard.SetTextBuf(List.getText);
  List.Free;
  List2.Free;
end;

// ************************************************************
// This method delete the points of the selection and the segments
// that are connected to selected points
// ************************************************************

procedure TSketchObject.ClearSelection;
var
  i, j: Integer;
  p: LTPoint;
  Lev: TSketchLevel;
  Seg: LTSegment;
begin
  // Set the current level
  Lev := TSketchLevel(levels[currentLevel]);
  // For each point of the selection
  for i := Selection.Count - 1 downto 0 do
  begin
    p := LTPoint(Selection[i]);
    // For each segment of the level
    for j := Lev.segments.Count - 1 downto 0 do
    begin
      Seg := LTSegment(Lev.segments[j]);
      if (Seg.A = p) or (Seg.B = p) then
      // if Point is connected to the segment
      begin
        Lev.segments.DElete(j);
        Seg.Free
      end
    end;
    // Remove point from al lists
    Selection.DElete(i);
    Lev.points.Remove(p);
    p.Free;
  end;
  Lev.Analyzed := False;
end;

// ************************************************************
// This method cut the points of the selection and the segments
// that are connected to selected points.
// It is the same thing than Copy then Clear;
// ************************************************************

procedure TSketchObject.CutSelection;
begin
  CopySelection;
  ClearSelection;
end;

// ************************************************************
// This method paste from the clipboard the previously copied
// data
// ************************************************************

procedure TSketchObject.PasteSelection;
var
  List: TStringList;
  Index, nbpoints, nbSegment: Integer;
  Lev: TSketchLevel;
  Base, i, Error: Integer;
  p: LTPoint;
  Seg: LTSegment;
begin
  Selection.clear;

  // Put the clipboard into the list
  List := TStringList.Create;
  List.SetText(PChar(ClipBoard.asText));
  // set the current level
  Lev := TSketchLevel(levels[currentLevel]);
  index := 1;
  nbpoints := StrToInt(List[index]);
  inc(index);
  inc(index);
  // Build all new points
  Base := Lev.points.Count;
  for i := 0 to nbpoints - 1 do
  begin
    p := LTPoint.Create(Point(0, 0));
    p.loadFromList(List, index);
    Lev.points.add(p);
    Selection.add(p);
  end;
  inc(index);
  nbSegment := StrToInt(List[index]);
  inc(index);
  inc(index);
  // Build all new segments
  for i := 0 to nbSegment - 1 do
  begin
    p := Lev.points[Base + StrToInt(List[index])];
    inc(index);
    Seg := LTSegment.Create(p, Lev.points[Base + StrToInt(List[index])]);
    inc(index);
    Seg.PercentageGlazing := StrToInt(List[index]);
    inc(index);
    Seg.NumberGlazing := StrToInt(List[index]);
    inc(index);
    Seg.GlazingColor := StrToInt(List[index]);
    inc(index);
    Seg.Shadings := StrToInt(List[index]);
    inc(index);
    Val(List[index], Seg.Orientation, Error);
    inc(index);
    Lev.segments.add(Seg);
  end;
  List.Free;
  // Verification of the new points
  Lev.CheckCross;
  Lev.Analyzed := False;
end;

procedure TSketchObject.FinishAnalyze(Sender: Tobject);
begin
  ActiveThread := False;
  TSketchLevel(levels[currentLevel]).Analyzed := True;
  refresh;
  if stored then
  begin
    stored := False;
    Action(Buff1, Buff2, Buff3, Buff4, Buff5, Buff6);
  end;
end;

procedure TSketchObject.SelectAll(SBitmap: TBitmap);
var
  points: TList;
  i: Integer;
begin
  Selection.clear;
  points := TSketchLevel(levels[currentLevel]).points;
  Selection.Capacity := points.Count;
  for i := 0 to points.Count - 1 do
    Selection.add(points[i]);
  refresh;
end;

procedure TSketchObject.DisplayInfo(Infos: TStringList; X, Y: Integer);
var
  TWidth, THeight, THeight2, i: Integer;
  Rect: TRect;
begin
  Y := Y + 22;
  with imageUi.picture.bitmap do
  begin
    TWidth := 0;
    for i := 0 to Infos.Count - 1 do
      if Canvas.TextWidth(Infos[i]) > TWidth then
        TWidth := Canvas.TextWidth(Infos[i]);
    THeight2 := Canvas.TextHeight(Infos[0]);
    THeight := THeight2 * Infos.Count;
    Rect.Left := X;
    Rect.Right := TWidth + X + 6;
    Rect.Top := Y;
    Rect.Bottom := THeight + Y + 6;
    Canvas.Brush.Color := Application.HintColor;
    Canvas.Pen.Color := clBlack;
    Canvas.Pen.Style := psSolid;
    Canvas.RoundRect(Rect.Left, Rect.Top, Rect.Right, Rect.Bottom, 0, 0);
    for i := 0 to Infos.Count - 1 do
    begin
      Canvas.TextOut(Rect.Left + 3, Rect.Top + 3, Infos[i]);
      Rect.Top := Rect.Top + THeight2;
    end;
  end;
end;

procedure TProject.PrintCurrentLevel;
begin
  TSketchLevel(sketchObject.levels[sketchObject.currentLevel]).Print(0, sketchObject.currentLevel);
end;

procedure TProject.SaveCurrentLevel(fileName : string);
begin
  TSketchLevel(sketchObject.levels[sketchObject.currentLevel]).SaveMeta(0, sketchObject.currentLevel, fileName);
end;

procedure TProject.Print;
var
  i: Integer;
  Level: TSketchLevel;
begin
  for i := 0 to sketchObject.levels.Count - 1 do
  begin
    Level := TSketchLevel(sketchObject.levels[i]);
    if (Level <> nil) and (Level.points.Count <> 0) then
      Level.Print(0, i);
  end;
end;

procedure TSketchObject.MoveCenter2(p1, p2: Tpoint);
begin
  Center.X := Center.X + (p2.X - p1.X);
  Center.Y := Center.Y + (p2.Y - p1.Y);
end;

procedure TSketchObject.AlignPoints;
var
  i, j, k: Integer;
  NbDep: Integer;
  Lev1, Lev2: TSketchLevel;
  p1, p2: LTPoint;
  ecx, ecy: Double;
  Tolerance: Double;
  Reponse: string;
  Erreur: Integer;
begin
  NbDep := 0;
  Reponse := '0.2';
  Reponse := inputBox('', Labels[237], Reponse);
  Val(Reponse, Tolerance, Erreur);
  if Erreur = 0 then
  begin
    for i := 0 to levels.Count - 2 do
    begin
      Lev1 := TSketchLevel(levels[i]);
      Lev2 := TSketchLevel(levels[i + 1]);
      for j := 0 to Lev1.points.Count - 1 do
      begin
        for k := 0 to Lev2.points.Count - 1 do
        begin
          p1 := LTPoint(Lev1.points[j]);
          p2 := LTPoint(Lev2.points[k]);
          ecx := abs(p1.X - p2.X) / Scale;
          ecy := abs(p1.Y - p2.Y) / Scale;
          if (ecx <= Tolerance) and (ecy <= Tolerance) and (ecx <> 0) and (ecy <> 0) then
          begin
            p2.X := p1.X;
            p2.Y := p1.Y;
            inc(NbDep)
          end;
        end;
      end;
    end;
    ShowMessage(IntToStr(NbDep) + Labels[238]);
  end
  else
    ShowMessage(Labels[239]);
end;

initialization
  Project := TProject.Create;


end.

