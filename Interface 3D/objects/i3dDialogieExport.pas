unit i3dDialogieExport;

interface

uses
  Classes,
  i3dGeometry,
  sysutils,
  i3dBuildingData,
  i3dUnit3D,
  Math,
  Dialogs,
  comctrls;


type
  TDialogieExport = class
    class procedure save(filePath : string);
    class procedure saveXml(filePath : string);
  end;

implementation

uses
  Sketch,
  i3dFormChoixTypeLocal,
  i3dSketchLevel,
  IzApp,
  XmlUtil,
  Xml.XMLIntf,
  i3dSketchObject;

class procedure TDialogieExport.save(filePath : string);
var
  cptLevel, cptRoom, cpt, cptPoint : Integer;
  level : TSketchLevel;
  Piece : TRoom;
  ListeDialogie : TStringList;
  Chaine : string;
  Ligne : string;
  stringNiv : string;
  stringParoi : string;
  nbParoi : Integer;
  ToitureExiste : Boolean;
  Seg : LTSegment;
  Porte : TPorte;
  Fen : TFenetre;
  Largeur, Hauteur : string;
  p : TParoiTemp;
  hauteurParoi : double;
  Hauteur1, Hauteur2 : double;
  Inclinaison, orientation, longueur : double;
  Correction : double;
  log : TStringList;
  Plancher : TPlancher;
  SousToiture : Integer;
  Page : TTabSheet;
  Ori : Integer;

  largeurParoi : double;
  largeurOuv : double;
  surfParoi : double;
  surfOuv : double;

  idPiece : Integer;
begin
  Page := FormSketch.PageControl1.ActivePage;
  FormSketch.PageControl1.ActivePage := FormSketch.TabSheet3D;
  FormSketch.PageControl1Change(nil);
  MakeBatiment;
  FormSketch.TrackBarLevel.Position := FormSketch.TrackBarLevel.Max;

  FormSketch.PageControl1.ActivePage := FormSketch.TabSheetDonnees;
  FormSketch.PageControl1.Refresh;

  ListeDialogie := TStringList.Create;
  nbParoi := 1;
  buildingData.PrepareShed;
  ToitureExiste := False;
  log := TStringList.Create;
  SousToiture := -1;

  // nettoyage de niveau inutile, affectation de la variable toiture et ...?
  for cptLevel := Project.sketchObject.Levels.Count - 1 downto 0 do
  begin
    level := TSketchLevel(Project.sketchObject.Levels[cptLevel]);
    if level.segments.Count = 0 then
    begin
      level.free;
      Project.sketchObject.Levels.delete(cptLevel);
      continue;
    end;
    if not level.Toiture then
    begin
      for cptRoom := 0 to level.Rooms.Count - 1 do
      begin
        Piece := TRoom(level.Rooms[cptRoom]);
        Piece.Value8 := Piece.Area;
        Piece.Value7 := 0;
        if Piece.RoomType = roomNormal then
          Piece.ID := 1000
        else
          Piece.ID := 0;
      end
    end
    else
    begin
      ToitureExiste := True;
      for cptRoom := 0 to level.Rooms.Count - 1 do
      begin
        Piece := TRoom(level.Rooms[cptRoom]);
        Piece.Value7 := 1;
      end;
      SousToiture := cptLevel - 1;
    end;
  end;
  buildingData.PreparationPlancherIntermediaire(log);
  // v�rification des planchers et pi�ces, PlancherInterm�diaire et toits
  for cpt := 0 to buildingData.ListePlancher.Count - 1 do
  begin
    Plancher := TPlancher(buildingData.ListePlancher[cpt]);
    if Plancher.niveau = 0 then
      stringNiv := 'RDC '
    else
      stringNiv := 'niv ' + intToStr(Plancher.niveau) + ' ';
    if (Plancher.PieceHaute = 1000) then
      Plancher.PH.Value8 := Plancher.PH.Value8 - Plancher.Surface;

    if (Plancher.PieceBasse = 1000) and (Plancher.PieceHaute <> 1000) and
      ((Plancher.PH = nil) or ((Plancher.PH <> nil) and (Plancher.PH.Value7 <> 1))) then
    begin
      if (Plancher.PieceHaute = -3) then
        stringParoi := stringNiv + 'Toiture interm�diaire' + intToStr(nbParoi)
      else
        stringParoi := stringNiv + 'Plafond interm�diaire ' + intToStr(nbParoi);
      Ligne := stringParoi + ',';
      Str(Plancher.Surface : 0 : 2, Chaine);
      Ligne := Ligne + Chaine + ',0,0,';
      if (Plancher.PieceHaute = -3) then
      begin
        if buildingData.CompoRoof = nil then
        begin
          DeleteFile(dialogie3DPath);
          ListeDialogie.free;
          // ListeCompo.Free;
          raise Exception.Create('Vous n''avez pas d�fini la composition du toit');
          Exit;
        end;
        Ligne := Ligne + buildingData.CompoRoof.nom + ',';
      end
      else
      begin
        if buildingData.CompoIntFloor = nil then
        begin
          DeleteFile(dialogie3DPath);
          ListeDialogie.free;
          // ListeCompo.Free;
          raise Exception.Create('Vous n''avez pas d�fini la composition du plancher interm�diaire');
          Exit;
        end;
        Ligne := Ligne + buildingData.CompoIntFloor.nom + ',';
      end;
      if (Plancher.PieceHaute = -3) then
        if buildingData.VentilatedLoft then
          Ligne := Ligne + '1,0,0.9'
        else
          Ligne := Ligne + '0,0,1'
      else
      begin
        FormLocal.setRoom(Plancher.PH);
        Ligne := Ligne + intToStr(FormLocal.ComboSur.ItemIndex) + ',';
        Ligne := Ligne + intToStr(FormLocal.ComboTau.ItemIndex) + ',';
        Str(Plancher.PH.Tau : 0 : 2, Chaine);
        Ligne := Ligne + Chaine;
      end;
      inc(nbParoi);
      ListeDialogie.Add(Ligne);
      ListeDialogie.Add('0');
    end
    else if (Plancher.PieceHaute = 1000) and (Plancher.PieceBasse <> 1000) then
    begin
      stringParoi := stringNiv + 'Plancher interm�diaire ' + intToStr(nbParoi);
      Ligne := stringParoi + ',';
      Str(Plancher.Surface : 0 : 2, Chaine);
      Ligne := Ligne + Chaine + ',180,0,';
      if buildingData.CompoIntFloor = nil then
      begin
        DeleteFile(dialogie3DPath);
        ListeDialogie.free;
        // ListeCompo.Free;
        raise Exception.Create('Vous n''avez pas d�fini la composition du plancher interm�diaire');
        Exit;
      end;
      Ligne := Ligne + buildingData.CompoIntFloor.nom + ',';


      if (Plancher.PieceBasse = -3) then
        Ligne := Ligne + '0,0,1'
      else
      begin
        FormLocal.setRoom(Plancher.PB);
        Ligne := Ligne + intToStr(FormLocal.ComboSur.ItemIndex) + ',';
        Ligne := Ligne + intToStr(FormLocal.ComboTau.ItemIndex) + ',';
        Str(Plancher.PB.Tau : 0 : 2, Chaine);
        Ligne := Ligne + Chaine;
      end;
      inc(nbParoi);
      ListeDialogie.Add(Ligne);
      ListeDialogie.Add('0');
    end
  end;

  // parcours des pi�ces de chaque niveau et v�rifications des parois
  for cptLevel := 0 to Project.sketchObject.Levels.Count - 1 do
  begin
    level := TSketchLevel(Project.sketchObject.Levels[cptLevel]);
    if cptLevel = 0 then
      stringNiv := 'RDC '
    else
      stringNiv := 'niv ' + intToStr(cptLevel) + ' ';
    for cptRoom := 0 to level.Rooms.Count - 1 do
    begin
      Piece := TRoom(level.Rooms[cptRoom]);

      if Piece.RoomType = roomNormal then
      begin
        // Plancher bas
        if cptLevel = 0 then
        begin
          stringParoi := stringNiv + 'Plancher ' + intToStr(nbParoi);
          Ligne := stringParoi + ',';
          Str(Piece.Area : 0 : 2, Chaine);
          Ligne := Ligne + Chaine + ',180,0,';
          if Piece.CompoFloor = nil then
          begin
            if buildingData.CompoFloor = nil then
            begin
              DeleteFile(dialogie3DPath);
              ListeDialogie.free;
              // ListeCompo.Free;
              raise Exception.Create('Vous n''avez pas d�fini la composition du plancher');
              Exit;
            end;
            Ligne := Ligne + buildingData.CompoFloor.nom + ',';
          end
          else
            Ligne := Ligne + Piece.CompoFloor.nom + ',';

          if buildingData.Crawlspace then
            Ligne := Ligne + '4,0,0.9'
          else
            Ligne := Ligne + '3,0,0.15';
          inc(nbParoi);
          ListeDialogie.Add(Ligne);
          ListeDialogie.Add('0');
        end;

        // Plancher suspendu
        if (Piece.Value8 > 0.1) and (cptLevel <> 0) then
        begin
          stringParoi := stringNiv + 'Plancher suspendu ' + intToStr(nbParoi);
          Ligne := stringParoi + ',';
          Str(Piece.Value8 : 0 : 2, Chaine);
          Ligne := Ligne + Chaine + ',180,0,';
          if Piece.CompoFloor = nil then
          begin
            if buildingData.CompoFloor = nil then
            begin
              DeleteFile(dialogie3DPath);
              ListeDialogie.free;
              // ListeCompo.Free;
              raise Exception.Create('Vous n''avez pas d�fini la composition du plancher');
              Exit;
            end;
            Ligne := Ligne + buildingData.CompoFloor.nom + ',';
          end
          else
            Ligne := Ligne + Piece.CompoFloor.nom + ',';

          if buildingData.Crawlspace then
            Ligne := Ligne + '4,0,0.9'
          else
            Ligne := Ligne + '3,0,0.15';
          inc(nbParoi);
          ListeDialogie.Add(Ligne);
          ListeDialogie.Add('0');
        end;


        // Toiture terrasse
        if (not ToitureExiste) and (cptLevel = Project.sketchObject.Levels.Count - 1) then
        begin
          stringParoi := stringNiv + 'Toiture terrasse ' + intToStr(nbParoi);
          Ligne := stringParoi + ',';
          Str(Piece.Area : 0 : 2, Chaine);
          Ligne := Ligne + Chaine + ',0,0,';
          if buildingData.CompoRoof = nil then
          begin
            DeleteFile(dialogie3DPath);
            ListeDialogie.free;
            // ListeCompo.Free;
            raise Exception.Create('Vous n''avez pas d�fini la composition de la toiture');
            Exit;
          end;
          Ligne := Ligne + buildingData.CompoRoof.nom + ',';
          if buildingData.VentilatedLoft then
            Ligne := Ligne + '1,0,0.9'
          else
            Ligne := Ligne + '0,0,1';
          inc(nbParoi);
          ListeDialogie.Add(Ligne);
          ListeDialogie.Add('0');
        end;
        // Sheds
        for cpt := 0 to Piece.ListeShed.Count - 1 do
        begin
          p := TParoiTemp(Piece.ListeShed[cpt]);
          Ligne := stringNiv + 'Shed ' + intToStr(nbParoi) + ',';
          Str(p.Surface : 0 : 2, Chaine);
          Ori := p.orientation - Round(Project.sketchObject.OrientationNord);
          while Ori > 180 do
            Ori := Ori - 360;
          while Ori < -180 do
            Ori := Ori + 360;
          Ligne := Ligne + Chaine + ',90,' + intToStr(Ori) + ',';
          if buildingData.CompoRoof = nil then
          begin
            DeleteFile(dialogie3DPath);
            ListeDialogie.free;
            // ListeCompo.Free;
            raise Exception.Create('Vous n''avez pas d�fini la composition de la toiture');
            Exit;
          end;
          Ligne := Ligne + buildingData.CompoRoof.nom + ',';
          Ligne := Ligne + '0,0,1';
          inc(nbParoi);
          ListeDialogie.Add(Ligne);
          ListeDialogie.Add('0');
        end;

        // Toiture Inclin�e
        if level.Toiture then
        begin
          Inclinaison := ArcCos(Piece.Normale.y) / pi * 180;
          longueur := Sqrt(Sqr(Piece.Normale.x) + Sqr(Piece.Normale.z));
          if longueur <> 0 then
            orientation := ArcSin(Piece.Normale.x / longueur) / pi * 180
          else
            orientation := 0;


          if Piece.Normale.z > 0 then
            orientation := 180 - orientation;
          orientation := 180 + orientation;

          while orientation > 180 do
            orientation := orientation - 360;
          while orientation < -180 do
            orientation := orientation + 360;
          orientation := orientation - Project.sketchObject.OrientationNord;
          Correction := Cos(Inclinaison / 180 * pi);

          if (Inclinaison = 0) or (Inclinaison = 180) then
          begin
            stringParoi := stringNiv + 'Toiture terrasse ' + intToStr(nbParoi);
          end
          else
          begin
            stringParoi := stringNiv + 'Toiture ' + intToStr(nbParoi);
          end;
          Ligne := stringParoi + ',';
          Str(Piece.Area / Correction : 0 : 2, Chaine);
          Ligne := Ligne + Chaine + ',' + intToStr(Round(Inclinaison)) + ',' + intToStr(Round(orientation)) + ',';
          if buildingData.CompoRoof = nil then
          begin
            DeleteFile(dialogie3DPath);
            ListeDialogie.free;
            // ListeCompo.Free;
            raise Exception.Create('Vous n''avez pas d�fini la composition de la toiture');
            Exit;
          end;
          Ligne := Ligne + buildingData.CompoRoof.nom + ',';
          Ligne := Ligne + '0,0,1';
          inc(nbParoi);
          ListeDialogie.Add(Ligne);
          ListeDialogie.Add('0');
        end;
      end;
      // Parois verticales
      if not level.Toiture then
      begin
        for cptPoint := 0 to Piece.Points.Count - 2 do
        begin
          Seg := level.SegmentExists(Piece.Points[cptPoint], Piece.Points[cptPoint + 1]);
          if (Seg = nil) then
          begin
            showMessage('Probl�me de liaison de point');
            Seg := level.SegmentExists(Piece.Points[cptPoint], Piece.Points[cptPoint + 1]); // pour voir ce que �a donne
            continue;
          end;
          // Seg := LTSegment(Lev.Segments[j]);
          if ((Seg.Perimeter) and (Piece.RoomType = roomNormal)) or
            ((not Seg.Perimeter) and (Piece.RoomType = roomLocal)) then
          begin
            if Seg.Perimeter then
              stringParoi := stringNiv + 'Paroi ext�rieure ' + intToStr(nbParoi)
            else
              stringParoi := stringNiv + 'Paroi int�rieure ' + intToStr(nbParoi);
            Ligne := stringParoi + ',';
            Hauteur1 := level.Height + buildingData.CorrigeHauteurP(Seg.a, Seg.b, Piece);
            Hauteur2 := level.Height + buildingData.CorrigeHauteurP(Seg.b, Seg.a, Piece);
            hauteurParoi := (Hauteur1 + Hauteur2) / 2;

            largeurParoi := Seg.Size(Project.sketchObject.scale);
            surfParoi := largeurParoi * hauteurParoi;
            Str(surfParoi : 0 : 2, Chaine);
            Ori := Round(Seg.orientation - Project.sketchObject.OrientationNord + 180);
            while Ori > 180 do
              Ori := Ori - 360;
            while Ori < -180 do
              Ori := Ori + 360;

            Ligne := Ligne + Chaine + ',90,' + intToStr(Round(Ori)) + ',';
            if Piece.RoomType = roomNormal then
            begin
              if Seg.Composition = nil then
              begin
                if buildingData.CompoRoof = nil then
                begin
                  DeleteFile(dialogie3DPath);
                  ListeDialogie.free;
                  // ListeCompo.Free;
                  raise Exception.Create('Vous n''avez pas d�fini la composition des murs ext�rieurs');
                  Exit;
                end;
                Ligne := Ligne + buildingData.CompoExt.nom + ',';
              end
              else
                Ligne := Ligne + Seg.Composition.nom + ',';
            end
            else
            begin
              if Seg.Composition = nil then
              begin
                if buildingData.CompoInt = nil then
                begin
                  DeleteFile(dialogie3DPath);
                  ListeDialogie.free;
                  // ListeCompo.Free;
                  raise Exception.Create('Vous n''avez pas d�fini la composition des murs int�rieurs');
                  Exit;
                end;
                Ligne := Ligne + buildingData.CompoInt.nom + ',';
              end
              else
                Ligne := Ligne + Seg.Composition.nom + ',';
            end;
            // Tau
            if Piece.RoomType = roomLocal then
            begin
              idPiece := FormLocal.ComboSur.Items.IndexOf(Piece.Sur);
              if idPiece = -1 then
                idPiece := 0;
              FormLocal.ComboSur.ItemIndex := idPiece;
              FormLocal.ComboSurChange(nil);
              Ligne := Ligne + intToStr(idPiece) + ',';

              idPiece := FormLocal.ComboTau.Items.IndexOf(Piece.TTau);
              if idPiece = -1 then
                idPiece := 0;
              FormLocal.ComboTau.ItemIndex := idPiece;
              FormLocal.ComboTauChange(nil);

              Ligne := Ligne + intToStr(idPiece) + ',';
              Str(Piece.Tau : 0 : 2, Chaine);
              Ligne := Ligne + Chaine;
            end
            else
              Ligne := Ligne + '0,0,1';


            inc(nbParoi);
            largeurOuv := 0;
            surfOuv := 0;
            ListeDialogie.Add(Ligne);

            // Menuiseries
            ListeDialogie.Add(intToStr(Seg.ListePortes.Count + Seg.Listefenetres.Count));
            // fen�tres
            for Fen in Seg.Listefenetres do
            begin
              Str(Fen.Hauteur : 0 : 2, Hauteur);
              Str(Fen.Largeur : 0 : 2, Largeur);
              largeurOuv := largeurOuv + Fen.Largeur;
              surfOuv := surfOuv + (Fen.Largeur * Fen.Hauteur);
              if largeurOuv > largeurParoi then
              begin
                DeleteFile(dialogie3DPath);
                ListeDialogie.free;
                raise Exception.Create
                  (Format('Paroi "%s" : la largeur des ouvertures (fen�tres %.2fm) est sup�rieure � celle de la paroi (%.2fm)',
                  [stringParoi, largeurOuv, largeurParoi]));
                Exit;
              end;
              if surfOuv > surfParoi then
              begin
                DeleteFile(dialogie3DPath);
                ListeDialogie.free;
                raise Exception.Create
                  (Format('Paroi "%s" : la surface des ouvertures (fen�tres %.2fm�) est sup�rieure � celle de la paroi (h.l %.2fm . %.2fm = %.2fm�)',
                  [stringParoi, surfOuv, hauteurParoi, largeurParoi, surfParoi]));
                Exit;
              end;
              if Fen.CompoFen <> nil then
                Ligne := Fen.CompoFen.nom + ',' + Hauteur + ',' + Largeur
              else
              begin
                if buildingData.CompoWindow = nil then
                begin
                  DeleteFile(dialogie3DPath);
                  ListeDialogie.free;
                  // ListeCompo.Free;
                  raise Exception.Create('Vous n''avez pas d�fini la composition des fen�tres');
                  Exit;
                end;
                Ligne := buildingData.CompoWindow.nom + ',' + Hauteur + ',' + Largeur;
              end;
              ListeDialogie.Add(Ligne);
            end;
            // portes
            for Porte in Seg.ListePortes do
            begin
              Str(Porte.Hauteur : 0 : 2, Hauteur);
              Str(Porte.Largeur : 0 : 2, Largeur);
              largeurOuv := largeurOuv + Porte.Largeur;
              surfOuv := surfOuv + (Porte.Largeur * Porte.Hauteur);
              if largeurOuv > largeurParoi then
              begin
                DeleteFile(dialogie3DPath);
                ListeDialogie.free;
                raise Exception.Create
                  (Format('Paroi "%s" : la largeur des ouvertures (ouvertures %.2fm) est sup�rieure � celle de la paroi (%.2fm)',
                  [stringParoi, largeurOuv, largeurParoi]));
                Exit;
              end;
              if surfOuv > surfParoi then
              begin
                DeleteFile(dialogie3DPath);
                ListeDialogie.free;
                raise Exception.Create
                  (Format('Paroi "%s" : la surface des ouvertures (ouvertures %.2fm�) est sup�rieure � celle de la paroi (h.l %.2fm . %.2fm = %.2fm�)',
                  [stringParoi, surfOuv, hauteurParoi, largeurParoi, surfParoi]));
                Exit;
              end;
              if Porte.CompoPorte <> nil then
                Ligne := Porte.CompoPorte.nom + ',' + Hauteur + ',' + Largeur
              else
              begin
                if buildingData.CompoDoor = nil then
                begin
                  DeleteFile(dialogie3DPath);
                  ListeDialogie.free;
                  // ListeCompo.Free;
                  raise Exception.Create('Vous n''avez pas d�fini la composition des portes');
                  Exit;
                end;
                Ligne := buildingData.CompoDoor.nom + ',' + Hauteur + ',' + Largeur;
              end;
              ListeDialogie.Add(Ligne);
            end;
          end;
        end;
      end;
    end;

  end;
  log.SaveToFile(TApp.tempPath + 'log-export.txt');
  FreeAndNil(log);
  ListeDialogie.insert(0, intToStr(nbParoi - 1));
  ListeDialogie.SaveToFile(filePath);
  ListeDialogie.free;
end;

class procedure TDialogieExport.saveXml(filePath : string);
var
  xmlFile : TBasicXMLFile;
  node : IXmlNode;
  child : IXmlNode;
  child2 : IXmlNode;
  child3 : IXmlNode;

  cptLevel : Integer;
  cptRoom : Integer;
  cpt : Integer;
  cptPoint : Integer;
  level : TSketchLevel;
  Piece : TRoom;

  Chaine : string;

  stringNiv : string;
  stringParoi : string;
  nbParoi : Integer;
  ToitureExiste : Boolean;
  Seg : LTSegment;
  Porte : TPorte;
  Fen : TFenetre;
  p : TParoiTemp;
  hauteurParoi : double;
  Hauteur1 : double;
  Hauteur2 : double;
  Inclinaison : double;
  orientation : double;
  longueur : double;
  Correction : double;
  log : TStringList;
  Plancher : TPlancher;
  SousToiture : Integer;
  Page : TTabSheet;
  Ori : Integer;

  largeurParoi : double;
  largeurOuv : double;
  surfParoi : double;
  surfOuv : double;

  idPiece : Integer;
begin
  xmlFile := TBasicXMLFile.getEmptyFile(filePath, 'dialogie-3d');


  Page := FormSketch.PageControl1.ActivePage;
  FormSketch.PageControl1.ActivePage := FormSketch.TabSheet3D;
  FormSketch.PageControl1Change(nil);
  MakeBatiment;
  FormSketch.TrackBarLevel.Position := FormSketch.TrackBarLevel.Max;

  FormSketch.PageControl1.ActivePage := FormSketch.TabSheetDonnees;
  FormSketch.PageControl1.Refresh;

  nbParoi := 1;
  buildingData.PrepareShed;
  ToitureExiste := False;
  log := TStringList.Create;
  SousToiture := -1;

  // nettoyage de niveau inutile, affectation de la variable toiture et ...?
  for cptLevel := Project.sketchObject.Levels.Count - 1 downto 0 do
  begin
    level := TSketchLevel(Project.sketchObject.Levels[cptLevel]);
    if level.segments.Count = 0 then
    begin
      level.free;
      Project.sketchObject.Levels.delete(cptLevel);
      continue;
    end;
    if not level.Toiture then
    begin
      for cptRoom := 0 to level.Rooms.Count - 1 do
      begin
        Piece := TRoom(level.Rooms[cptRoom]);
        Piece.Value8 := Piece.Area;
        Piece.Value7 := 0;
        if Piece.RoomType = roomNormal then
          Piece.ID := 1000
        else
          Piece.ID := 0;
      end
    end
    else
    begin
      ToitureExiste := True;
      for cptRoom := 0 to level.Rooms.Count - 1 do
      begin
        Piece := TRoom(level.Rooms[cptRoom]);
        Piece.Value7 := 1;
      end;
      SousToiture := cptLevel - 1;
    end;
  end;
  buildingData.PreparationPlancherIntermediaire(log);
  // v�rification des planchers et pi�ces, PlancherInterm�diaire et toits
  node := TXmlUtil.addChild(xmlFile.rootNode, 'parois');
  for cpt := 0 to buildingData.ListePlancher.Count - 1 do
  begin
    Plancher := TPlancher(buildingData.ListePlancher[cpt]);
    if Plancher.niveau = 0 then
      stringNiv := 'RDC '
    else
      stringNiv := 'niv ' + intToStr(Plancher.niveau) + ' ';
    if (Plancher.PieceHaute = 1000) then
      Plancher.PH.Value8 := Plancher.PH.Value8 - Plancher.Surface;

    if (Plancher.PieceBasse = 1000) and (Plancher.PieceHaute <> 1000) and
      ((Plancher.PH = nil) or ((Plancher.PH <> nil) and (Plancher.PH.Value7 <> 1))) then
    begin
      if (Plancher.PieceHaute = -3) then
        stringParoi := stringNiv + 'Toiture interm�diaire' + intToStr(nbParoi)
      else
        stringParoi := stringNiv + 'Plafond interm�diaire ' + intToStr(nbParoi);
      child := TXmlUtil.addChild(node, 'paroi');
      TXmlUtil.setTag(child, 'nom', stringParoi);
      TXmlUtil.setTag(child, 'surface', Plancher.Surface);
      TXmlUtil.setTag(child, 'inclinaison', '0');
      TXmlUtil.setTag(child, 'orientation', '0');
      if (Plancher.PieceHaute = -3) then
      begin
        if buildingData.CompoRoof = nil then
        begin
          xmlFile.free;
          DeleteFile(dialogie3DPath);
          // ListeCompo.Free;
          raise Exception.Create('Vous n''avez pas d�fini la composition du toit');
          Exit;
        end;
        TXmlUtil.setTag(child, 'composition', buildingData.CompoRoof.nom);
      end
      else
      begin
        if buildingData.CompoIntFloor = nil then
        begin
          xmlFile.free;
          DeleteFile(dialogie3DPath);
          // ListeCompo.Free;
          raise Exception.Create('Vous n''avez pas d�fini la composition du plancher interm�diaire');
          Exit;
        end;
        TXmlUtil.setTag(child, 'composition', buildingData.CompoIntFloor.nom);
      end;
      if (Plancher.PieceHaute = -3) then
        if buildingData.VentilatedLoft then
        begin
          TXmlUtil.setTag(child, 'lib-tau-id', '1');
          TXmlUtil.setTag(child, 'tau-id', '0');
          TXmlUtil.setTag(child, 'tau', '0.9');
        end
        else
        begin
          TXmlUtil.setTag(child, 'lib-tau-id', '0');
          TXmlUtil.setTag(child, 'tau-id', '0');
          TXmlUtil.setTag(child, 'tau', '1');
        end
      else
      begin
        FormLocal.setRoom(Plancher.PH);
        TXmlUtil.setTag(child, 'lib-tau-id', FormLocal.ComboSur.ItemIndex);
        TXmlUtil.setTag(child, 'tau-id', FormLocal.ComboTau.ItemIndex);
        TXmlUtil.setTag(child, 'tau', Plancher.PH.Tau);
      end;
      inc(nbParoi);
      TXmlUtil.addChild(child, 'fenetres');
    end
    else if (Plancher.PieceHaute = 1000) and (Plancher.PieceBasse <> 1000) then
    begin
      child := TXmlUtil.addChild(node, 'paroi');
      stringParoi := stringNiv + 'Plancher interm�diaire ' + intToStr(nbParoi);
      TXmlUtil.setTag(child, 'nom', stringParoi);
      TXmlUtil.setTag(child, 'surface', Plancher.Surface);
      TXmlUtil.setTag(child, 'inclinaison', '180');
      TXmlUtil.setTag(child, 'orientation', '0');
      if buildingData.CompoIntFloor = nil then
      begin
        xmlFile.free;
        DeleteFile(dialogie3DPath);
        // ListeCompo.Free;
        raise Exception.Create('Vous n''avez pas d�fini la composition du plancher interm�diaire');
        Exit;
      end;
      TXmlUtil.setTag(child, 'composition', buildingData.CompoIntFloor.nom);


      if (Plancher.PieceBasse = -3) then
      begin
        TXmlUtil.setTag(child, 'lib-tau-id', '0');
        TXmlUtil.setTag(child, 'tau-id', '0');
        TXmlUtil.setTag(child, 'tau', '1');
      end
      else
      begin
        FormLocal.setRoom(Plancher.PB);
        TXmlUtil.setTag(child, 'lib-tau-id', FormLocal.ComboSur.ItemIndex);
        TXmlUtil.setTag(child, 'tau-id', FormLocal.ComboTau.ItemIndex);
        TXmlUtil.setTag(child, 'tau', Plancher.PB.Tau);
      end;
      inc(nbParoi);
      TXmlUtil.addChild(child, 'fenetres');
    end
  end;

  // parcours des pi�ces de chaque niveau et v�rifications des parois
  for cptLevel := 0 to Project.sketchObject.Levels.Count - 1 do
  begin
    level := TSketchLevel(Project.sketchObject.Levels[cptLevel]);
    if cptLevel = 0 then
      stringNiv := 'RDC '
    else
      stringNiv := 'niv ' + intToStr(cptLevel) + ' ';
    for cptRoom := 0 to level.Rooms.Count - 1 do
    begin
      Piece := TRoom(level.Rooms[cptRoom]);

      if Piece.RoomType = roomNormal then
      begin
        // Plancher bas
        if cptLevel = 0 then
        begin
          child := TXmlUtil.addChild(node, 'paroi');
          stringParoi := stringNiv + 'Plancher ' + intToStr(nbParoi);
          TXmlUtil.setTag(child, 'nom', stringParoi);
          TXmlUtil.setTag(child, 'surface', Piece.Area);
          TXmlUtil.setTag(child, 'inclinaison', '180');
          TXmlUtil.setTag(child, 'orientation', '0');
          if Piece.CompoFloor = nil then
          begin
            if buildingData.CompoFloor = nil then
            begin
              xmlFile.free;
              DeleteFile(dialogie3DPath);
              // ListeCompo.Free;
              raise Exception.Create('Vous n''avez pas d�fini la composition du plancher');
              Exit;
            end;
            TXmlUtil.setTag(child, 'composition', buildingData.CompoFloor.nom);
          end
          else
            TXmlUtil.setTag(child, 'composition', Piece.CompoFloor.nom);

          if buildingData.Crawlspace then
          begin
            TXmlUtil.setTag(child, 'lib-tau-id', '4');
            TXmlUtil.setTag(child, 'tau-id', '0');
            TXmlUtil.setTag(child, 'tau', '0.9');
          end
          else
          begin
            TXmlUtil.setTag(child, 'lib-tau-id', '3');
            TXmlUtil.setTag(child, 'tau-id', '0');
            TXmlUtil.setTag(child, 'tau', '0.15');
          end;
          inc(nbParoi);
          TXmlUtil.addChild(child, 'fenetres');
        end;

        // Plancher suspendu
        if (Piece.Value8 > 0.1) and (cptLevel <> 0) then
        begin
          child := TXmlUtil.addChild(node, 'paroi');
          stringParoi := stringNiv + 'Plancher suspendu ' + intToStr(nbParoi);
          TXmlUtil.setTag(child, 'nom', stringParoi);
          TXmlUtil.setTag(child, 'surface', Piece.Value8);
          TXmlUtil.setTag(child, 'inclinaison', '180');
          TXmlUtil.setTag(child, 'orientation', '0');
          if Piece.CompoFloor = nil then
          begin
            if buildingData.CompoFloor = nil then
            begin
              xmlFile.free;
              DeleteFile(dialogie3DPath);
              // ListeCompo.Free;
              raise Exception.Create('Vous n''avez pas d�fini la composition du plancher');
              Exit;
            end;
            TXmlUtil.setTag(child, 'composition', buildingData.CompoFloor.nom);
          end
          else
            TXmlUtil.setTag(child, 'composition', Piece.CompoFloor.nom);

          if buildingData.Crawlspace then
          begin
            TXmlUtil.setTag(child, 'lib-tau-id', '4');
            TXmlUtil.setTag(child, 'tau-id', '0');
            TXmlUtil.setTag(child, 'tau', '0.9');
          end
          else
          begin
            TXmlUtil.setTag(child, 'lib-tau-id', '3');
            TXmlUtil.setTag(child, 'tau-id', '0');
            TXmlUtil.setTag(child, 'tau', '0.15');
          end;
          inc(nbParoi);
          TXmlUtil.addChild(child, 'fenetres');
        end;


        // Toiture terrasse
        if (not ToitureExiste) and (cptLevel = Project.sketchObject.Levels.Count - 1) then
        begin
          child := TXmlUtil.addChild(node, 'paroi');
          stringParoi := stringNiv + 'Toiture terrasse ' + intToStr(nbParoi);
          TXmlUtil.setTag(child, 'nom', stringParoi);
          TXmlUtil.setTag(child, 'surface', Piece.Area);
          TXmlUtil.setTag(child, 'inclinaison', '0');
          TXmlUtil.setTag(child, 'orientation', '0');
          if buildingData.CompoRoof = nil then
          begin
            xmlFile.free;
            DeleteFile(dialogie3DPath);
            // ListeCompo.Free;
            raise Exception.Create('Vous n''avez pas d�fini la composition de la toiture');
            Exit;
          end;
          TXmlUtil.setTag(child, 'composition', buildingData.CompoRoof.nom);
          if buildingData.VentilatedLoft then
          begin
            TXmlUtil.setTag(child, 'lib-tau-id', '1');
            TXmlUtil.setTag(child, 'tau-id', '0');
            TXmlUtil.setTag(child, 'tau', '0.9');
          end
          else
          begin
            TXmlUtil.setTag(child, 'lib-tau-id', '0');
            TXmlUtil.setTag(child, 'tau-id', '0');
            TXmlUtil.setTag(child, 'tau', '1');
          end;
          inc(nbParoi);
          TXmlUtil.addChild(child, 'fenetres');
        end;
        // Sheds
        for cpt := 0 to Piece.ListeShed.Count - 1 do
        begin
          p := TParoiTemp(Piece.ListeShed[cpt]);
          Ori := p.orientation - Round(Project.sketchObject.OrientationNord);
          while Ori > 180 do
            Ori := Ori - 360;
          while Ori < -180 do
            Ori := Ori + 360;
          child := TXmlUtil.addChild(node, 'paroi');
          stringParoi := stringNiv + 'Shed ' + intToStr(nbParoi) + ',';
          TXmlUtil.setTag(child, 'nom', stringParoi);
          TXmlUtil.setTag(child, 'surface', Piece.Area);
          TXmlUtil.setTag(child, 'inclinaison', '90');
          TXmlUtil.setTag(child, 'orientation', Ori);
          if buildingData.CompoRoof = nil then
          begin
            xmlFile.free;
            DeleteFile(dialogie3DPath);
            // ListeCompo.Free;
            raise Exception.Create('Vous n''avez pas d�fini la composition de la toiture');
            Exit;
          end;
          TXmlUtil.setTag(child, 'composition', buildingData.CompoRoof.nom);
          TXmlUtil.setTag(child, 'lib-tau-id', '0');
          TXmlUtil.setTag(child, 'tau-id', '0');
          TXmlUtil.setTag(child, 'tau', '1');
          inc(nbParoi);
          TXmlUtil.addChild(child, 'fenetres');
        end;

        // Toiture Inclin�e
        if level.Toiture then
        begin
          Inclinaison := ArcCos(Piece.Normale.y) / pi * 180;
          longueur := Sqrt(Sqr(Piece.Normale.x) + Sqr(Piece.Normale.z));
          if longueur <> 0 then
            orientation := ArcSin(Piece.Normale.x / longueur) / pi * 180
          else
            orientation := 0;

          if Piece.Normale.z > 0 then
            orientation := 180 - orientation;
          orientation := 180 + orientation;

          while orientation > 180 do
            orientation := orientation - 360;
          while orientation < -180 do
            orientation := orientation + 360;
          orientation := orientation - Project.sketchObject.OrientationNord;
          Correction := Cos(Inclinaison / 180 * pi);

          if (Inclinaison = 0) or (Inclinaison = 180) then
          begin
            stringParoi := stringNiv + 'Toiture terrasse ' + intToStr(nbParoi);
          end
          else
          begin
            stringParoi := stringNiv + 'Toiture ' + intToStr(nbParoi);
          end;
          child := TXmlUtil.addChild(node, 'paroi');
          TXmlUtil.setTag(child, 'nom', stringParoi);
          TXmlUtil.setTag(child, 'surface', Piece.Area / Correction);
          TXmlUtil.setTag(child, 'inclinaison', Round(Inclinaison));
          TXmlUtil.setTag(child, 'orientation', Round(orientation));
          if buildingData.CompoRoof = nil then
          begin
            xmlFile.free;
            DeleteFile(dialogie3DPath);
            // ListeCompo.Free;
            raise Exception.Create('Vous n''avez pas d�fini la composition de la toiture');
            Exit;
          end;
          TXmlUtil.setTag(child, 'composition', buildingData.CompoRoof.nom);
          TXmlUtil.setTag(child, 'lib-tau-id', '0');
          TXmlUtil.setTag(child, 'tau-id', '0');
          TXmlUtil.setTag(child, 'tau', '1');
          inc(nbParoi);
          TXmlUtil.addChild(child, 'fenetres');
        end;
      end;
      // Parois verticales
      if not level.Toiture then
      begin
        for cptPoint := 0 to Piece.Points.Count - 2 do
        begin
          Seg := level.SegmentExists(Piece.Points[cptPoint], Piece.Points[cptPoint + 1]);
          if (Seg = nil) then
          begin
            showMessage('Probl�me de liaison de point');
            Seg := level.SegmentExists(Piece.Points[cptPoint], Piece.Points[cptPoint + 1]); // pour voir ce que �a donne
            continue;
          end;
          // Seg := LTSegment(Lev.Segments[j]);
          if ((Seg.Perimeter) and (Piece.RoomType = roomNormal)) or
            ((not Seg.Perimeter) and (Piece.RoomType = roomLocal)) then
          begin
            if Seg.Perimeter then
              stringParoi := stringNiv + 'Paroi ext�rieure ' + intToStr(nbParoi)
            else
              stringParoi := stringNiv + 'Paroi int�rieure ' + intToStr(nbParoi);
            Hauteur1 := level.Height + buildingData.CorrigeHauteurP(Seg.a, Seg.b, Piece);
            Hauteur2 := level.Height + buildingData.CorrigeHauteurP(Seg.b, Seg.a, Piece);
            hauteurParoi := (Hauteur1 + Hauteur2) / 2;

            largeurParoi := Seg.Size(Project.sketchObject.scale);
            surfParoi := largeurParoi * hauteurParoi;
            Str(surfParoi : 0 : 2, Chaine);
            Ori := Round(Seg.orientation - Project.sketchObject.OrientationNord + 180);
            while Ori > 180 do
              Ori := Ori - 360;
            while Ori < -180 do
              Ori := Ori + 360;

            child := TXmlUtil.addChild(node, 'paroi');
            TXmlUtil.setTag(child, 'nom', stringParoi);
            TXmlUtil.setTag(child, 'surface', surfParoi);
            TXmlUtil.setTag(child, 'inclinaison', '90');
            TXmlUtil.setTag(child, 'orientation', Ori);
            if Piece.RoomType = roomNormal then
            begin
              if Seg.Composition = nil then
              begin
                if buildingData.CompoRoof = nil then
                begin
                  xmlFile.free;
                  DeleteFile(dialogie3DPath);
                  // ListeCompo.Free;
                  raise Exception.Create('Vous n''avez pas d�fini la composition des murs ext�rieurs');
                  Exit;
                end;
                TXmlUtil.setTag(child, 'composition', buildingData.CompoExt.nom);
              end
              else
                TXmlUtil.setTag(child, 'composition', Seg.Composition.nom);
            end
            else
            begin
              if Seg.Composition = nil then
              begin
                if buildingData.CompoInt = nil then
                begin
                  xmlFile.free;
                  DeleteFile(dialogie3DPath);
                  // ListeCompo.Free;
                  raise Exception.Create('Vous n''avez pas d�fini la composition des murs int�rieurs');
                  Exit;
                end;
                TXmlUtil.setTag(child, 'composition', buildingData.CompoInt.nom);
              end
              else
                TXmlUtil.setTag(child, 'composition', Seg.Composition.nom);
            end;
            // Tau
            if Piece.RoomType = roomLocal then
            begin
              idPiece := FormLocal.ComboSur.Items.IndexOf(Piece.Sur);
              if idPiece = -1 then
                idPiece := 0;
              FormLocal.ComboSur.ItemIndex := idPiece;
              FormLocal.ComboSurChange(nil);
              TXmlUtil.setTag(child, 'lib-tau-id', idPiece);

              idPiece := FormLocal.ComboTau.Items.IndexOf(Piece.TTau);
              if idPiece = -1 then
                idPiece := 0;
              FormLocal.ComboTau.ItemIndex := idPiece;
              FormLocal.ComboTauChange(nil);
              TXmlUtil.setTag(child, 'tau-id', idPiece);
              TXmlUtil.setTag(child, 'tau', Piece.Tau);
            end
            else
            begin
              TXmlUtil.setTag(child, 'lib-tau-id', '0');
              TXmlUtil.setTag(child, 'tau-id', '0');
              TXmlUtil.setTag(child, 'tau', '1');
            end;


            inc(nbParoi);
            largeurOuv := 0;
            surfOuv := 0;

            // Menuiseries
            child2 := TXmlUtil.addChild(child, 'fenetres');
            // fen�tres
            for Fen in Seg.Listefenetres do
            begin
              child3 := TXmlUtil.addChild(child2, 'fenetre');
              largeurOuv := largeurOuv + Fen.Largeur;
              surfOuv := surfOuv + (Fen.Largeur * Fen.Hauteur);
              if largeurOuv > largeurParoi then
              begin
                xmlFile.free;
                DeleteFile(dialogie3DPath);
                raise Exception.Create
                  (Format('Paroi "%s" : la largeur des ouvertures (fen�tres %.2fm) est sup�rieure � celle de la paroi (%.2fm)',
                  [stringParoi, largeurOuv, largeurParoi]));
                Exit;
              end;
              if surfOuv > surfParoi then
              begin
                xmlFile.free;
                DeleteFile(dialogie3DPath);
                raise Exception.Create
                  (Format('Paroi "%s" : la surface des ouvertures (fen�tres %.2fm�) est sup�rieure � celle de la paroi (h.l %.2fm . %.2fm = %.2fm�)',
                  [stringParoi, surfOuv, hauteurParoi, largeurParoi, surfParoi]));
                Exit;
              end;
              if Fen.CompoFen <> nil then
              begin
                TXmlUtil.setTag(child3, 'nom', Fen.CompoFen.nom);
                TXmlUtil.setTag(child3, 'hauteur', Fen.Hauteur);
                TXmlUtil.setTag(child3, 'largeur', Fen.Largeur);
              end
              else
              begin
                if buildingData.CompoWindow = nil then
                begin
                  xmlFile.free;
                  DeleteFile(dialogie3DPath);
                  // ListeCompo.Free;
                  raise Exception.Create('Vous n''avez pas d�fini la composition des fen�tres');
                  Exit;
                end;
                TXmlUtil.setTag(child3, 'nom', buildingData.CompoWindow.nom);
                TXmlUtil.setTag(child3, 'hauteur', Fen.Hauteur);
                TXmlUtil.setTag(child3, 'largeur', Fen.Largeur);
              end;
            end;
            // portes
            for Porte in Seg.ListePortes do
            begin
              child3 := TXmlUtil.addChild(child2, 'fenetre');
              largeurOuv := largeurOuv + Porte.Largeur;
              surfOuv := surfOuv + (Porte.Largeur * Porte.Hauteur);
              if largeurOuv > largeurParoi then
              begin
                xmlFile.free;
                DeleteFile(dialogie3DPath);
                raise Exception.Create
                  (Format('Paroi "%s" : la largeur des ouvertures (ouvertures %.2fm) est sup�rieure � celle de la paroi (%.2fm)',
                  [stringParoi, largeurOuv, largeurParoi]));
                Exit;
              end;
              if surfOuv > surfParoi then
              begin
                xmlFile.free;
                DeleteFile(dialogie3DPath);
                raise Exception.Create
                  (Format('Paroi "%s" : la surface des ouvertures (ouvertures %.2fm�) est sup�rieure � celle de la paroi (h.l %.2fm . %.2fm = %.2fm�)',
                  [stringParoi, surfOuv, hauteurParoi, largeurParoi, surfParoi]));
                Exit;
              end;
              if Porte.CompoPorte <> nil then
              begin
                TXmlUtil.setTag(child3, 'nom', Porte.CompoPorte.nom);
                TXmlUtil.setTag(child3, 'hauteur', Porte.Hauteur);
                TXmlUtil.setTag(child3, 'largeur', Porte.Largeur);
              end
              else
              begin
                if buildingData.CompoDoor = nil then
                begin
                  xmlFile.free;
                  DeleteFile(dialogie3DPath);
                  // ListeCompo.Free;
                  raise Exception.Create('Vous n''avez pas d�fini la composition des portes');
                  Exit;
                end;
                TXmlUtil.setTag(child3, 'nom', buildingData.CompoDoor.nom);
                TXmlUtil.setTag(child3, 'hauteur', Porte.Hauteur);
                TXmlUtil.setTag(child3, 'largeur', Porte.Largeur);
              end;
            end;
          end;
        end;
      end;
    end;

  end;
  log.SaveToFile(TApp.tempPath + 'log-export.txt');
  FreeAndNil(log);


  xmlFile.saveToDisk();
  xmlFile.free;
end;

end.
