unit i3dToiture;

interface

Uses
  i3dSketchLevel,
  i3dGeometry,
  Classes,
  windows;

Procedure CreerLimitesToitures(Level: TSketchLevel);
Function PointExiste(Lev: TSketchLevel; p: LTPoint): LTPoint;

implementation

Uses
  Sketch,
  i3dSketchObject;

Function PointExiste(Lev: TSketchLevel; p: LTPoint): LTPoint;
Var
  i: Integer;
  pp: LTPoint;
Begin
  PointExiste := Nil;
  for i := 0 to Lev.points.count - 1 do
  Begin
    pp := LTPoint(Lev.points[i]);
    if (p.x = pp.x) and (p.y = pp.y) then
      PointExiste := pp
  End;
End;

Procedure CreerLimitesToitures(Level: TSketchLevel);
Var
  Lev: TSketchLevel;
  i, j, k: Integer;
  p1, p2: LTPoint;
  S: LTSegment;
  Piece: TRoom;
  Father: TSketchObject;
  Room: TRoom;
  canMove : boolean;
Begin
  Father := TSketchObject(Level.FatherObject);
  // For i := 0 to FormSketch.Project.SketchObject.Levels.count-1 do
  // Begin
  canMove := false;
  Lev := Project.sketchObject.Levels[Father.CurrentLevel - 1];
  For j := 0 to Lev.Rooms.count - 1 do
  Begin
    Piece := TRoom(Lev.Rooms[j]);
    If Piece.RoomType <> roomObstruction then
      canMove := piece.RoomType = roomNormal;
      For k := 0 to Piece.points.count - 2 do
      Begin
        S := Lev.SegmentExists(Piece.points[k], Piece.points[k + 1]);
        if S.Perimeter then
        Begin
          p1 := PointExiste(Level, S.A);
          if p1 = nil then
          Begin
            p1 := Level.PointCreation(Point(S.A.x + Father.Center.x, S.A.y + Father.Center.y));
            p1.Indeplacable := canMove;
          End;
          p2 := PointExiste(Level, S.B);
          if p2 = nil then
          Begin
            p2 := Level.PointCreation(Point(S.B.x + Father.Center.x, S.B.y + Father.Center.y));
            p2.Indeplacable := canMove;
          end;
          S := LTSegment.Create(p1, p2);
          S.Indeplacable := canMove;
          Level.segments.Add(S);
        End;
      End;
  End;
  // end;
  Level.checkCross;
  Level.analyze;
  For i := 0 to Level.Rooms.count - 1 do
  Begin
    Room := TRoom(Level.Rooms[i]);
    While Room.points.count - 1 > Room.HautPoint.count do
      Room.HautPoint.Add(0);
  End;
End;

end.
