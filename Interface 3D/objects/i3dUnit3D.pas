unit i3dUnit3D;

interface

{$I delphiX\DelphiXcfg.inc}

uses
  i3dGeometry,
  i3dSketchLevel,
  Classes,
  SysUtils,
{$IFDEF StandardDX}
  Direct3D, DirectDraw;
{$ELSE}
  DirectX;
{$ENDIF}


procedure MakeBatiment;
procedure MakePorte(Seg: LTSegment; level: TSketchLevel; CumulHauteur, Coeff: double; ListeParoi: TList);
procedure MakeFenetre(Seg: LTSegment; level: TSketchLevel; CumulHauteur, Coeff: double; ListeParoi: TList);
procedure MakeMasque(Seg: LTSegment; level: TSketchLevel; CumulHauteur, Coeff: double; ListeParoi: TList; Sens: boolean);
function CorrigeHauteur(p, p2: LTPoint; Room: TRoom): double;
function CorrigeHauteur2(p, p2: LTPoint; Room: TRoom): double;
function CalculeNormale(V1, V2: TD3DVector): TD3DVector;
function CalculeDecalage(Room: Troom; NLevel: integer): double;

implementation

uses
  Windows,
  i3dSketchObject,
  Sketch,
  i3dPolyUtil,
  D3DUtils;

function CalculeDecalage(Room: Troom; NLevel: integer): double;
var
  i, j, k: integer;
  decalage: double;
  Ok, Ok2: boolean;
  p: LTPoint;
  Room2: TRoom;
  Lev: TSketchLevel;
begin
  CalculeDecalage := 0;
  decalage := 0;
  for j := NLevel - 1 downto 0 do
  begin
    Lev := TSketchLevel(Project.sketchObject.Levels[j]);
    Ok2 := True;
    for i := 0 to Room.Points.Count - 2 do
    begin
      p := LTPoint(Room.Points[i]);
      Ok := False;
      for k := 0 to Lev.Rooms.Count - 1 do
      begin
        Room2 := TRoom(Lev.Rooms[k]);
        Ok := Ok or (PointInOut2(p, Room2.Points) >= 0);
      end;
      Ok2 := Ok2 and Ok;
    end;
    if Ok2 = False then
    begin
      Decalage := decalage - Lev.Height;
      CalculeDecalage := Decalage;
    end
    else
      Break;
  end;
end;

function CalculeNormale(V1, V2: TD3DVector): TD3DVector;
begin
  CalculeNormale.x := -(V1.y * V2.z - V2.y * V1.z);
  CalculeNormale.y := (V1.z * V2.x - V2.z * V1.x);
  CalculeNormale.z := -(V1.x * V2.y - V2.x * V1.y);
  if (V1.z * V2.x - V2.z * V1.x) < 0 then
  begin
    CalculeNormale.x := (V1.y * V2.z - V2.y * V1.z);
    CalculeNormale.y := -(V1.z * V2.x - V2.z * V1.x);
    CalculeNormale.z := (V1.x * V2.y - V2.x * V1.y);
  end;
end;

function CorrigeHauteur(p, p2: LTPoint; Room: TRoom): double;
var
  i, j, k, ijkk: integer;
  Level, LevelToit, TLevel: TSketchLevel;
  NLevel, NLevelToit: integer;
  NbLevel, Erreur: integer;
  ATraiter: boolean;
  Level2: boolean;
  RoomTemp, RRoom: TRoom;
  MaxHauteur, Hauteur: double;
  a, b, c, d: double;
  m1, m2, m3: TP3D;
  Indice: integer;
  PTrouve: boolean;
begin
  CorrigeHauteur := 0;
  MaxHauteur := Maxlongint;
  NbLevel := project.sketchObject.Levels.Count;
  NLevel := 0;
  for i := 0 to NbLevel - 2 do
  begin
    Level := TSketchLevel(project.sketchObject.Levels[i]);
    if Level.Rooms.IndexOf(Room) <> -1 then
      NLevel := i;
  end;
  LevelToit := TsketchLevel(project.sketchObject.Levels[nbLevel - 1]);
  NLevelToit := nbLevel - 1;
  if LevelToit.points.Count = 0 then
  begin
    LevelToit := TsketchLevel(project.sketchObject.Levels[nbLevel - 2]);
    NLevelToit := nbLevel - 2;
  end;
  if LevelToit.Toiture then
  begin
    for j := 0 to LevelToit.Rooms.Count - 1 do
    begin
      RRoom := TRoom(LevelToit.Rooms[j]);
      if RRoom.HautPoint.Count <> RRoom.Points.Count then
      begin
        RRoom.HautPoint.Clear;
        for ijkk := 0 to RRoom.Points.Count - 1 do
          RRoom.HautPoint.Add(0);
      end;
    end;
    ATraiter := True;
    for i := NLevel + 1 to NLevelToit - 1 do
    begin
      TLevel := TSketchLevel(project.sketchObject.Levels[i]);
      for j := 0 to TLevel.Rooms.Count - 1 do
      begin
        RoomTemp := TRoom(TLevel.Rooms[j]);
        if PointInOut2(p, RoomTemp.Points) >= 0 then
          ATraiter := False
      end;
    end;
    if ATraiter then
    begin
      PTrouve := False;
      for j := 0 to LevelToit.Rooms.Count - 1 do
      begin
        RoomTemp := TRoom(LevelToit.Rooms[j]);
        for k := 0 to RoomTemp.Points.Count - 2 do
        begin
          if (abs(LTPoint(RoomTemp.Points[k]).x - p.x) < 0.001) and
            (abs(LTPoint(RoomTemp.Points[k]).y - p.y) < 0.001) and
            (PointInOut2(p2, RoomTemp.Points) >= 0) then
          begin
            hauteur := RoomTemp.HautPoint[k];
            PTrouve := True;
            if Hauteur < MaxHauteur then
              MaxHauteur := Hauteur;
          end;
        end;
      end;
      for j := 0 to LevelToit.Rooms.Count - 1 do
      begin
        RoomTemp := TRoom(LevelToit.Rooms[j]);
        if (PointInOut2(p, RoomTemp.Points) >= 0) and
          (PointInOut2(p2, RoomTemp.Points) >= 0) and not PTrouve then
        begin
          indice := 0;
          a := 0;
          b := 0;
          c := 0;
          d := 0;
          //while (a = 0) and (b = 0) and (c = 0) and (d = 0) do
          while (c = 0) do
          begin
            M1.x := LTPoint(RoomTemp.Points[indice]).x;
            M1.y := LTPoint(RoomTemp.Points[indice]).y;
            m1.z := RoomTemp.HautPoint[indice];

            M2.x := LTPoint(RoomTemp.Points[indice + 1]).x;
            M2.y := LTPoint(RoomTemp.Points[indice + 1]).y;
            m2.z := RoomTemp.HautPoint[indice + 1];

            M3.x := LTPoint(RoomTemp.Points[indice + 2]).x;
            M3.y := LTPoint(RoomTemp.Points[indice + 2]).y;
            m3.z := RoomTemp.HautPoint[indice + 2];

            //Calcule �quation du plan
            A := M2.y * (M3.z - M1.z) - M1.y * M3.z - M2.z * M3.y +
              M2.z * M1.y + M1.z * M3.y;
            B := -M2.x * M3.z + M2.x * M1.z + M1.x * M3.z + M2.z *
              M3.x - M2.z * M1.x - M1.z * M3.x;
            C := M2.x * M3.y - M2.x * M1.y - M1.x * M3.y - M2.y *
              M3.x + M2.y * M1.x + M1.y * M3.x;
            D := -M1.x * A - M1.y * B - M1.z * C;
            inc(indice);
          end;
          Hauteur := (-a * p.x - b * p.y - d) / c;
          if Hauteur < MaxHauteur then
            MaxHauteur := Hauteur;
        end;
      end;
      if MaxHauteur = maxlongint then
        MaxHauteur := 0;
      CorrigeHauteur := MaxHauteur;
    end;
  end;
end;

function CorrigeHauteur2(p, p2: LTPoint; Room: TRoom): double;
var
  i, j, k, ijkk: integer;
  Level, LevelToit, TLevel: TSketchLevel;
  NLevel, NLevelToit: integer;
  NbLevel, Erreur: integer;
  ATraiter: boolean;
  Level2: boolean;
  RoomTemp, RRoom: TRoom;
  MaxHauteur, Hauteur: double;
  a, b, c, d: double;
  m1, m2, m3: TP3D;
  Indice: integer;
  PTrouve: boolean;
begin
  CorrigeHauteur2 := 0;
  MaxHauteur := Maxlongint;
  NbLevel := project.sketchObject.Levels.Count;
  NLevel := 0;
  for i := 0 to NbLevel - 2 do
  begin
    Level := TSketchLevel(project.sketchObject.Levels[i]);
    if Level.Rooms.IndexOf(Room) <> -1 then
      NLevel := i;
  end;
  LevelToit := TsketchLevel(project.sketchObject.Levels[nbLevel - 1]);
  NLevelToit := nbLevel - 1;
  if LevelToit.points.Count = 0 then
  begin
    LevelToit := TsketchLevel(project.sketchObject.Levels[nbLevel - 2]);
    NLevelToit := nbLevel - 2;
  end;
  if LevelToit.Toiture then
  begin
    for j := 0 to LevelToit.Rooms.Count - 1 do
    begin
      RRoom := TRoom(LevelToit.Rooms[j]);
      if RRoom.HautPoint.Count <> RRoom.Points.Count then
      begin
        RRoom.HautPoint.Clear;
        for ijkk := 0 to RRoom.Points.Count - 1 do
          RRoom.HautPoint.Add(0);
      end;
    end;
    ATraiter := True;
    for i := NLevel + 1 to NLevelToit - 1 do
    begin
      TLevel := TSketchLevel(project.sketchObject.Levels[i]);
      for j := 0 to TLevel.Rooms.Count - 1 do
      begin
        RoomTemp := TRoom(TLevel.Rooms[j]);
        if PointInOut2(p, RoomTemp.Points) >= 0 then
          ATraiter := False
      end;
    end;
    if ATraiter then
    begin
      PTrouve := False;
      for j := 0 to LevelToit.Rooms.Count - 1 do
      begin
        RoomTemp := TRoom(LevelToit.Rooms[j]);
        for k := 0 to RoomTemp.Points.Count - 2 do
        begin
          if (abs(LTPoint(RoomTemp.Points[k]).x - p.x) < 0.001) and
            (abs(LTPoint(RoomTemp.Points[k]).y - p.y) < 0.001) and
            (PointInOut2(p2, RoomTemp.Points) >= 0) then
          begin
            hauteur := RoomTemp.HautPoint[k];
            PTrouve := True;
            if Hauteur < MaxHauteur then
              MaxHauteur := Hauteur;
          end;
        end;
      end;
      for j := 0 to LevelToit.Rooms.Count - 1 do
      begin
        RoomTemp := TRoom(LevelToit.Rooms[j]);
        if (PointInOut2(p, RoomTemp.Points) >= 0) and
          (PointInOut2(p2, RoomTemp.Points) >= 0) and not PTrouve then
        begin
          indice := 0;
          a := 0;
          b := 0;
          c := 0;
          d := 0;
          while (a = 0) and (b = 0) and (c = 0) and (d = 0) do
          begin
            M1.x := LTPoint(RoomTemp.Points[indice]).x;
            M1.y := LTPoint(RoomTemp.Points[indice]).y;
            M1.z := RoomTemp.HautPoint[indice];

            M2.x := LTPoint(RoomTemp.Points[indice + 1]).x;
            M2.y := LTPoint(RoomTemp.Points[indice + 1]).y;
            M2.z := RoomTemp.HautPoint[indice + 1];

            M3.x := LTPoint(RoomTemp.Points[indice + 2]).x;
            M3.y := LTPoint(RoomTemp.Points[indice + 2]).y;
            m3.z := RoomTemp.HautPoint[indice + 2];

            //Calcule �quation du plan
            A := M2.y * (M3.z - M1.z) - M1.y * M3.z - M2.z * M3.y +
              M2.z * M1.y + M1.z * M3.y;
            B := -M2.x * M3.z + M2.x * M1.z + M1.x * M3.z + M2.z *
              M3.x - M2.z * M1.x - M1.z * M3.x;
            C := M2.x * M3.y - M2.x * M1.y - M1.x * M3.y - M2.y *
              M3.x + M2.y * M1.x + M1.y * M3.x;
            D := -M1.x * A - M1.y * B - M1.z * C;
            inc(indice);
          end;
          Hauteur := (-a * p.x - b * p.y - d) / c;
          if Hauteur < MaxHauteur then
            MaxHauteur := Hauteur;
        end;
      end;
      if MaxHauteur = maxlongint then
        MaxHauteur := 0;
      CorrigeHauteur2 := MaxHauteur;
    end;
  end;
end;


procedure MakePorte(Seg: LTSegment; level: TSketchLevel;
  CumulHauteur, Coeff: double; ListeParoi: TList);
var
  j: integer;
  LPoint: LTPoint;
  Porte: TPorte;
  VecteurX, VecteurY, Longueur: double;
  p1, p2: Tpoint;
  Scale: double;
  Normale: TD3DVector;
  Par: T3DParoi;
begin
  Lpoint := LTPoint.Create(point(0,0));
  Scale := TSketchObject(Level.fatherObject).Scale;
  for porte in Seg.ListePortes do
  begin
    VecteurX := (Seg.B.x - Seg.A.x);
    VecteurY := (Seg.B.y - Seg.A.y);
    LPoint.x := Round((Seg.B.x * Porte.Center) +
      (Seg.A.x * (1 - Porte.center)));
    LPoint.y := Round((Seg.B.y * Porte.Center) +
      (Seg.A.y * (1 - Porte.center)));

    Longueur := Sqrt(Sqr(VecteurX) + Sqr(VecteurY));
    if longueur <> 0 then
    begin
      VecteurX := VecteurX / Longueur;
      VecteurY := VecteurY / Longueur;
    end
    else
    begin
      VecteurX := 1;
      VecteurY := 1;
    end;
    p1.x := Round(Lpoint.x + Porte.Largeur / 2 * Scale * VecteurX);
    p1.y := Round(LPoint.y + Porte.Largeur / 2 * Scale * VecteurY);
    p2.x := Round(Lpoint.x - Porte.Largeur / 2 * Scale * VecteurX);
    p2.y := Round(LPoint.y - Porte.Largeur / 2 * Scale * VecteurY);

    Normale := MakeD3DVector(p1.y - p2.y, 0, p1.x - p2.x);
    Normale := VectorNormalize(Normale);
    Par := T3DParoi.Create;
    SetLength(Par.p, 4);
    Par.p[0] := MakeD3DVERTEX(MakeD3DVECTOR((p1.x) * Coeff - Normale.x / 100,
      CumulHauteur, - (p1.y) * Coeff - Normale.z / 100), Normale, 0.0, 1.0);
    Par.p[1] := MakeD3DVERTEX(MakeD3DVECTOR(p1.x * Coeff - Normale.x / 100,
      (CumulHauteur + Porte.Hauteur), - p1.y * Coeff - Normale.z / 100),
      Normale, 0.0, 0.0);
    Par.p[2] := MakeD3DVERTEX(MakeD3DVECTOR(p2.x * Coeff - Normale.x / 100,
      CumulHauteur, - p2.y * Coeff - Normale.z / 100), Normale, 1.0, 1.0);
    Par.p[3] := MakeD3DVERTEX(MakeD3DVECTOR(p2.x * Coeff - Normale.x / 100,
      (CumulHauteur + Porte.Hauteur), - p2.y * Coeff - Normale.z / 100),
      Normale, 1.0, 0.0);
    Par.Alpha := False;
    Par.NTexture := 7;
    Par.nbPoints := 4;
    ListeParoi.Add(Par);

    Normale := MakeD3DVector(p2.y - p1.y, 0, p2.x - p1.x);
    Normale := VectorNormalize(Normale);
    Par := T3DParoi.Create;
    SetLength(Par.p, 4);
    Par.p[0] := MakeD3DVERTEX(MakeD3DVECTOR(p1.x * Coeff - Normale.x / 100,
      CumulHauteur, - p1.y * Coeff - Normale.z / 100), Normale, 0.0, 1.0);
    Par.p[1] := MakeD3DVERTEX(MakeD3DVECTOR(p2.x * Coeff - Normale.x / 100,
      CumulHauteur, - p2.y * Coeff - Normale.z / 100), Normale, 1.0, 1.0);
    Par.p[2] := MakeD3DVERTEX(MakeD3DVECTOR(p1.x * Coeff - Normale.x / 100,
      (CumulHauteur + Porte.Hauteur), - p1.y * Coeff - Normale.z / 100),
      Normale, 0.0, 0.0);
    Par.p[3] := MakeD3DVERTEX(MakeD3DVECTOR(p2.x * Coeff - Normale.x / 100,
      (CumulHauteur + Porte.Hauteur), - p2.y * Coeff - Normale.z / 100),
      Normale, 1.0, 0.0);
    Par.Alpha := False;
    Par.NTexture := 7;
    Par.nbPoints := 4;
    ListeParoi.Add(Par);
  end;
  Lpoint.Free;
end;

procedure MakeMasque(Seg: LTSegment; level: TSketchLevel;
  CumulHauteur, Coeff: double; ListeParoi: TList; Sens: boolean);
var
  LPoint: LTPoint;
  VecteurX, VecteurY, Longueur: double;
  p1, p2, p3, p4: Tpoint;
  Scale: double;
  Normale: TD3DVector;
  Par: T3DParoi;
begin
  Lpoint := LTPoint.Create(point(0,0));
  Scale := TSketchObject(Level.fatherObject).Scale;
  VecteurX := (Seg.B.x - Seg.A.x);
  VecteurY := (Seg.B.y - Seg.A.y);

  Longueur := Sqrt(Sqr(VecteurX) + Sqr(VecteurY));
  if longueur <> 0 then
  begin
    VecteurX := VecteurX / Longueur;
    VecteurY := VecteurY / Longueur;
  end
  else
  begin
    VecteurX := 1;
    VecteurY := 1;
  end;
  p1.x := Round(Seg.A.x);
  p1.y := Round(Seg.A.y);
  p3.x := Round(Seg.B.x);
  p3.y := Round(Seg.B.y);

  if Sens then
  begin
    p2.x := Round(Seg.A.x - VecteurY * Seg.Casquette / coeff);
    p2.y := Round(Seg.A.y + VecteurX * Seg.Casquette / coeff);
    p4.x := Round(Seg.B.x - VecteurY * Seg.Casquette / coeff);
    p4.y := Round(Seg.B.y + VecteurX * Seg.Casquette / coeff);
  end
  else
  begin
    p2.x := Round(Seg.A.x + VecteurY * Seg.Casquette / coeff);
    p2.y := Round(Seg.A.y - VecteurX * Seg.Casquette / coeff);
    p4.x := Round(Seg.B.x + VecteurY * Seg.Casquette / coeff);
    p4.y := Round(Seg.B.y - VecteurX * Seg.Casquette / coeff);
  end; 
  Normale := MakeD3DVector(0,1,0);
  Normale := VectorNormalize(Normale);
  Par := T3DParoi.Create;
  SetLength(Par.p, 4);
  Par.p[0] := MakeD3DVERTEX(MakeD3DVECTOR(p1.x * Coeff,
    CumulHauteur + Level.Height - Seg.Distance, - p1.y * Coeff),
    Normale, 0.0, 1.0);
  Par.p[1] := MakeD3DVERTEX(MakeD3DVECTOR(p2.x * Coeff,
    (CumulHauteur + +Level.Height - Seg.Distance), - p2.y * Coeff),
    Normale, 0.0, 0.0);
  Par.p[2] := MakeD3DVERTEX(MakeD3DVECTOR(p3.x * Coeff,
    CumulHauteur + Level.Height - Seg.Distance, - p3.y * Coeff),
    Normale, 1.0, 1.0);
  Par.p[3] := MakeD3DVERTEX(MakeD3DVECTOR(p4.x * Coeff,
    (CumulHauteur + Level.Height - Seg.Distance), - p4.y * Coeff),
    Normale, 1.0, 0.0);
  Par.Alpha := False;
  Par.NTexture := 3;
  Par.nbPoints := 4;
  ListeParoi.Add(Par);

  // Normale := MakeD3DVector(p2.y - p1.y, 0, p2.x - p1.x);
  Normale := MakeD3DVector(0, - 1,0);
  Normale := VectorNormalize(Normale);
  Par := T3DParoi.Create;
  SetLength(Par.p, 4);
  Par.p[0] := MakeD3DVERTEX(MakeD3DVECTOR(p1.x * Coeff,
    CumulHauteur + Level.Height - Seg.Distance, - p1.y * Coeff),
    Normale, 0.0, 1.0);
  Par.p[1] := MakeD3DVERTEX(MakeD3DVECTOR(p3.x * Coeff,
    CumulHauteur + Level.Height - Seg.Distance, - p3.y * Coeff),
    Normale, 1.0, 1.0);
  Par.p[2] := MakeD3DVERTEX(MakeD3DVECTOR(p2.x * Coeff,
    (CumulHauteur + Level.Height - Seg.Distance), - p2.y * Coeff),
    Normale, 0.0, 0.0);
  Par.p[3] := MakeD3DVERTEX(MakeD3DVECTOR(p4.x * Coeff,
    (CumulHauteur + Level.Height - Seg.Distance), - p4.y * Coeff),
    Normale, 1.0, 0.0);
  Par.Alpha := False;
  Par.NTexture := 3;
  Par.nbPoints := 4;
  ListeParoi.Add(Par);
  Lpoint.Free;
end;

procedure MakeFenetre(Seg: LTSegment; level: TSketchLevel;
  CumulHauteur, Coeff: double; ListeParoi: TList);
var
  j: integer;
  LPoint: LTPoint;
  fenetre: Tfenetre;
  VecteurX, VecteurY, Longueur: double;
  p1, p2: Tpoint;
  Scale: double;
  Normale: TD3DVector;
  Par: T3DParoi;
begin
  Lpoint := LTPoint.Create(point(0,0));
  Scale := TSketchObject(Level.fatherObject).Scale;
  for fenetre in Seg.Listefenetres do
  begin
    VecteurX := (Seg.B.x - Seg.A.x);
    VecteurY := (Seg.B.y - Seg.A.y);
    LPoint.x := Round((Seg.B.x * fenetre.Center) +
      (Seg.A.x * (1 - fenetre.center)));
    LPoint.y := Round((Seg.B.y * fenetre.Center) +
      (Seg.A.y * (1 - fenetre.center)));

    Longueur := Sqrt(Sqr(VecteurX) + Sqr(VecteurY));
    if longueur <> 0 then
    begin
      VecteurX := VecteurX / Longueur;
      VecteurY := VecteurY / Longueur;
    end
    else
    begin
      VecteurX := 1;
      VecteurY := 1;
    end;
    p1.x := Round(Lpoint.x + fenetre.Largeur / 2 * Scale * VecteurX);
    p1.y := Round(LPoint.y + fenetre.Largeur / 2 * Scale * VecteurY);
    p2.x := Round(Lpoint.x - fenetre.Largeur / 2 * Scale * VecteurX);
    p2.y := Round(LPoint.y - fenetre.Largeur / 2 * Scale * VecteurY);

    Normale := MakeD3DVector(p1.y - p2.y, 0, p1.x - p2.x);
    Normale := VectorNormalize(Normale);
    Par := T3DParoi.Create;
    SetLength(Par.p, 4);
    if ((Fenetre.Allege + Fenetre.Hauteur) > Level.Height) then
      Fenetre.Allege := Level.Height - Fenetre.Hauteur;
    Par.p[0] := MakeD3DVERTEX(MakeD3DVECTOR((p1.x) * Coeff - Normale.x / 100,
      CumulHauteur + Fenetre.Allege, - (p1.y) * Coeff - Normale.z / 100),
      Normale, 1.0, 0.0);
    Par.p[1] := MakeD3DVERTEX(MakeD3DVECTOR(p1.x * Coeff - Normale.x / 100,
      (CumulHauteur + Fenetre.Allege + fenetre.Hauteur),
      - p1.y * Coeff - Normale.z / 100),
      Normale, 1.0, 1.0);
    Par.p[2] := MakeD3DVERTEX(MakeD3DVECTOR(p2.x * Coeff - Normale.x / 100,
      CumulHauteur + Fenetre.Allege, - p2.y * Coeff - Normale.z / 100),
      Normale, 0.0, 0.0);
    Par.p[3] := MakeD3DVERTEX(MakeD3DVECTOR(p2.x * Coeff - Normale.x / 100,
      (CumulHauteur + Fenetre.Allege + fenetre.Hauteur),
      - p2.y * Coeff - Normale.z / 100),
      Normale, 0.0, 1.0);
    Par.Alpha := False;
    Par.NTexture := 4;
    Par.nbPoints := 4;
    ListeParoi.Add(Par);

    Normale := MakeD3DVector(p2.y - p1.y, 0, p2.x - p1.x);
    Normale := VectorNormalize(Normale);
    Par := T3DParoi.Create;
    SetLength(Par.p, 4);
    Par.p[0] := MakeD3DVERTEX(MakeD3DVECTOR(p1.x * Coeff - Normale.x / 100,
      CumulHauteur + Fenetre.Allege, - p1.y * Coeff - Normale.z / 100),
      Normale, 1.0, 0.0);
    Par.p[1] := MakeD3DVERTEX(MakeD3DVECTOR(p2.x * Coeff - Normale.x / 100,
      CumulHauteur + Fenetre.Allege, - p2.y * Coeff - Normale.z / 100),
      Normale, 0.0, 0.0);
    Par.p[2] := MakeD3DVERTEX(MakeD3DVECTOR(p1.x * Coeff - Normale.x / 100,
      (CumulHauteur + Fenetre.Allege + fenetre.Hauteur),
      - p1.y * Coeff - Normale.z / 100),
      Normale, 1.0, 1.0);
    Par.p[3] := MakeD3DVERTEX(MakeD3DVECTOR(p2.x * Coeff - Normale.x / 100,
      (CumulHauteur + Fenetre.Allege + fenetre.Hauteur),
      - p2.y * Coeff - Normale.z / 100),
      Normale, 0.0, 1.0);
    Par.Alpha := False;
    Par.NTexture := 4;
    Par.nbPoints := 4;
    ListeParoi.Add(Par);
  end;
  LPoint.Free;
end;

procedure MakeBatiment;
var
  n0, n1, n2, n3, n4, n5: TD3DVector;
  Normale, Normale2: TD3DVector;
  Par, Par2, Par3: T3DParoi;
  i, j, l, k, i1, i2, i3, kl: integer;
  Level: TSketchLevel;
  Room: TRoom;
  p, p1, p2: LTPoint;
  pp: TD3DVertex;
  CumulHauteur: double;
  Coeff: double;
  Seg: LTSegment;
  Ok: boolean;
  CTexture: integer;
  LevelAAfficher: integer;
  NbCentre: integer;
  VP1, VP2: TD3DVector;
  VP1bis, VP2bis: TD3DVector;
  Minx, MinY, MaxX, MaxY: double;
  Tempo: TD3DVertex;
  ijk, ijkk, indice: integer;
  ListeTriangle: TList;
  ltp: LTPoint;
  Index, Erreur: integer;
  Reel, Reel2, Reel3: double;
  v1, v2, v3: TD3DVector;
  Liste: TStringList;
  scale: double;
  r1, r2: TRoom;
  Position1, Position2, z: integer;
  Ecart1, Ecart2: double;
begin
  Scale := project.sketchObject.Scale;
  Liste := nil;
  Seg := nil;
  Set8087CW(Default8087CW);
  for i := 0 to project.sketchObject.Levels.Count - 1 do
  begin
    Level := TSketchLevel(project.sketchObject.Levels[i]);
    Level.Analyze;
    for k := 0 to Level.segments.Count - 1 do
    begin
      LTSegment(Level.segments[k]).isVirtual := False;
      LTSegment(Level.segments[k]).adjacent := False;
    end;
    for j := 0 to Level.Rooms.Count - 1 do
    begin
      Room := TRoom(Level.Rooms[j]);
      Room.Volume := 0;
      if Room.HautPoint.Count <> Room.Points.Count then
      begin
        Room.HautPoint.Clear;
        for ijkk := 0 to Room.Points.Count - 1 do
          Room.HautPoint.Add(0);
      end;
      if (Room.RoomType <> roomExternal) then
        for k := 0 to Room.Points.Count - 2 do
        begin
          p := LTPoint(Room.Points[k]);
          p2 := LTPoint(Room.Points[k + 1]);
          Seg := Level.SegmentExists(p, p2);
          if (Seg.A = p) and (Seg.B = p2) then
            Seg.Adjacent := True
          else if (Seg.A = p2) and (Seg.B = p) then
            Seg.Adjacent := False
        end;
    end;
  end;
  NbCentre := 0;
  FormSketch.CentreX := 0;
  FormSketch.CentreY := 0;
  Minx := Maxlongint;
  MinY := Maxlongint;
  MaxX := -Maxlongint;
  MaxY := -Maxlongint;
  Normale := MakeD3DVECTOR(0,1,0);
  for i := 0 to FormSketch.Liste3DParoi.Count - 1 do
  begin
    T3DParoi(FormSketch.Liste3DParoi[i]).Free;
  end;
  FormSketch.Liste3DParoi.Clear;

  CumulHauteur := 0;

  LevelAAfficher := FormSketch.TrackBarLevel.Position;
  if LevelAAfficher = FormSketch.TrackBarLevel.Max then
    dec(LevelAAfficher);
  Coeff := 1 / Round(project.sketchObject.scale);
  for i := 0 to LevelAAfficher - 1 do
  begin
    Level := TSketchLevel(project.sketchObject.Levels[i]);
    for j := 0 to Level.Rooms.Count - 1 do
    begin
      Room := TRoom(Level.Rooms[j]);
      for k := 0 to Level.segments.Count - 1 do
        LTSegment(Level.segments[k]).isVirtual := False;
      if (not level.toiture) then
        for k := 0 to Room.Points.Count - 2 do
        begin
          p := LTPoint(Room.Points[k]);
          FormSketch.CentreX := FormSketch.CentreX + p.x;
          FormSketch.CentreY := FormSketch.CentreY + p.y;
          if p.x < MinX then
            MinX := p.x;
          if p.y < MinY then
            MinY := p.y;
          if p.x > MaxX then
            MaxX := p.x;
          if p.y > MaxY then
            MaxY := p.y;
          Inc(NbCentre);
          p2 := LTPoint(Room.Points[k + 1]);
          Seg := Level.SegmentExists(p, p2);
          if not Seg.isVirtual then
          begin
            Seg.isVirtual := True;
            Normale := MakeD3DVector(p.y - p2.y, 0, p.x - p2.x);
            Normale := VectorNormalize(Normale);

            Par := T3DParoi.Create;
            SetLength(Par.p, 4);
            if (Room.RoomType <> roomObstruction) and (Room.RoomType <> roomLocal) then
            begin
              Par.p[0] := MakeD3DVERTEX(MakeD3DVECTOR(p.x * Coeff,
                CumulHauteur, - p.y * Coeff), Normale, 0.0, 0.0);

              Par.p[1] := MakeD3DVERTEX(MakeD3DVECTOR(p.x * Coeff,
                (CumulHauteur + Level.Height +
                CorrigeHauteur(p, p2, room)), - p.y * Coeff),
                Normale, Level.Height / 3, 0.0);

              vp1 := MakeD3DVECTOR(p2.x * Coeff,
                CumulHauteur, - p2.y * Coeff);
              vp2 := MakeD3DVECTOR(p2.x * Coeff,
                (CumulHauteur + Level.Height +
                CorrigeHauteur(p2, p, room)), - p2.y * Coeff);
              Par.p[2] := MakeD3DVERTEX(vp1, Normale,
                0.0, Seg.Size(1 / Coeff) / 3);
              Par.p[3] := MakeD3DVERTEX(vp2, Normale,
                Level.Height / 3, Seg.Size(1 / Coeff) / 3);
              Par.Alpha := False;
              Par.NTexture := 5;
            end
            else if (Room.RoomType = roomLocal) then
            begin
              Par.p[0] := MakeD3DVERTEX(MakeD3DVECTOR(p.x * Coeff,
                CumulHauteur, - p.y * Coeff), Normale, 0.0, 0.0);

              Par.p[1] := MakeD3DVERTEX(MakeD3DVECTOR(p.x * Coeff,
                (CumulHauteur + Level.Height +
                CorrigeHauteur(p, p2, room)), - p.y * Coeff),
                Normale, Level.Height / 3, 0.0);

              vp1 := MakeD3DVECTOR(p2.x * Coeff,
                CumulHauteur, - p2.y * Coeff);
              vp2 := MakeD3DVECTOR(p2.x * Coeff,
                (CumulHauteur + Level.Height +
                CorrigeHauteur(p2, p, room)), - p2.y * Coeff);
              Par.p[2] := MakeD3DVERTEX(vp1, Normale,
                0.0, Seg.Size(1 / Coeff) / 3);
              Par.p[3] := MakeD3DVERTEX(vp2, Normale,
                Level.Height / 3, Seg.Size(1 / Coeff) / 3);
              Par.Alpha := False;
              Par.NTexture := 3;
            end
            else
            begin
              Par.p[0] := MakeD3DVERTEX(MakeD3DVECTOR(p.x * Coeff,
                0, - p.y * Coeff), Normale, 0.0, 0.0);
              Par.p[1] := MakeD3DVERTEX(MakeD3DVECTOR(p.x * Coeff,
                Room.Value1, - p.y * Coeff), Normale, 1.0, 0.0);
              Par.p[2] := MakeD3DVERTEX(MakeD3DVECTOR(p2.x * Coeff,
                0, - p2.y * Coeff), Normale, 0.0, 1.0);
              Par.p[3] := MakeD3DVERTEX(MakeD3DVECTOR(p2.x * Coeff,
                Room.Value1, - p2.y * Coeff), Normale, 1.0, 1.0);
              Par.NTexture := 3;
              Par.Alpha := False;
            end;
            Par.NbPoints := 4;
            if (Seg.Perimeter) or (Room.RoomType = roomLocal) then
              FormSketch.Liste3DParoi.Add(Par);
            Normale := MakeD3DVector(p2.y - p.y, 0, p2.x - p.x);
            Normale := VectorNormalize(Normale);
            if ((Room.RoomType <> roomObstruction) and Seg.perimeter) or
              (Room.RoomType = roomLocal) then
            begin
              Par3 := T3DParoi.Create;
              SetLength(Par3.p, 4);
              Par3.p[0] := MakeD3DVERTEX(MakeD3DVECTOR(p.x * Coeff,
                CumulHauteur, - p.y * Coeff), Normale, 0.0, 0.0);
              Par3.p[1] := MakeD3DVERTEX(MakeD3DVECTOR(p2.x * Coeff,
                CumulHauteur, - p2.y * Coeff), Normale, 0.0, 1.0);
              vp1Bis := MakeD3DVECTOR(p.x * Coeff,
                (CumulHauteur + Level.Height +
                CorrigeHauteur(p, p2, room)), - p.y * Coeff);
              vp2Bis := MakeD3DVECTOR(p2.x * Coeff,
                (CumulHauteur + Level.Height +
                CorrigeHauteur(p2, p, room)), - p2.y * Coeff);
              Par3.p[2] := MakeD3DVERTEX(vp1bis, Normale, 1.0, 0.0);
              Par3.p[3] := MakeD3DVERTEX(vp2bis, Normale, 1.0, 1.0);
              if (Room.RoomType = roomLocal) then
                Par3.NTexture := 3
              else if Seg.Perimeter then
                Par3.NTexture := 1
              else
                Par3.NTexture := 5;
              Par3.Alpha := False;
              Par3.NbPoints := 4;
              FormSketch.Liste3DParoi.Add(Par3);
              if Seg.Perimeter and (Seg.Casquette <> 0) then
              begin
                if seg.Adjacent then
                  MakeMasque(Seg, Level,
                    CumulHauteur, Coeff, FormSketch.Liste3DParoi, True)
                else
                  MakeMasque(Seg, Level,
                    CumulHauteur, Coeff, FormSketch.Liste3DParoi, False);
              end;
            end
            else if (Seg.Perimeter) or (Room.RoomType = roomLocal) then
            begin
              Par3 := T3DParoi.Create;
              SetLength(Par3.p, 4);
              Par3.p[0] := MakeD3DVERTEX(MakeD3DVECTOR(p.x * Coeff,
                0, - p.y * Coeff), Normale, 0.0, 0.0);
              Par3.p[1] := MakeD3DVERTEX(MakeD3DVECTOR(p2.x * Coeff,
                0, - p2.y * Coeff), Normale, 0.0, 1.0);
              Par3.p[2] := MakeD3DVERTEX(MakeD3DVECTOR(p.x * Coeff,
                Room.Value1, - p.y * Coeff), Normale, 1.0, 0.0);
              Par3.p[3] := MakeD3DVERTEX(MakeD3DVECTOR(p2.x * Coeff,
                Room.Value1, - p2.y * Coeff), Normale, 1.0, 1.0);
              Par3.NTexture := 3;
              Par3.Alpha := False;
              Par3.NbPoints := 4;
              FormSketch.Liste3DParoi.Add(Par3);
            end;
            //ICI
            MakePorte(Seg, Level, CumulHauteur, Coeff,
              FormSketch.Liste3DParoi);
            MakeFenetre(Seg, Level, CumulHauteur,
              Coeff, FormSketch.Liste3DParoi);
          end;
        end;
      if (TRoom(Level.Rooms[j]).RoomType <> roomObstruction) then
      begin
        ListeTriangle := TList.Create;
        Draw_Poly(Room, ListeTriangle);
        Normale := MakeD3DVECTOR(0,1,0);
        indice := 0;
        Par := T3DParoi.Create;
        SetLength(Par.p, 1000);
        while Indice < ListeTriangle.Count do
        begin
          if not Level.Toiture then
          begin
            ltp := LTPoint(ListeTriangle[Indice]);
            Par.p[Indice] := MakeD3DVERTEX(MakeD3DVECTOR(ltp.x * Coeff,
              CumulHauteur + 0.001, - ltp.y * Coeff), Normale,
              (FormSketch.CentreX - ltp.x) *
              Coeff / 4, (FormSketch.CentreY - ltp.y) * Coeff / 4);
            ltp := LTPoint(ListeTriangle[Indice + 1]);
            Par.p[Indice + 1] :=
              MakeD3DVERTEX(MakeD3DVECTOR(ltp.x * Coeff,
              CumulHauteur + 0.001, - ltp.y * Coeff), Normale,
              (FormSketch.CentreX - ltp.x) *
              Coeff / 4, (FormSketch.CentreY - ltp.y) * Coeff / 4);
            ltp := LTPoint(ListeTriangle[Indice + 2]);
            Par.p[Indice + 2] :=
              MakeD3DVERTEX(MakeD3DVECTOR(ltp.x * Coeff,
              CumulHauteur + 0.001, - ltp.y * Coeff), Normale,
              (FormSketch.CentreX - ltp.x) *
              Coeff / 4, (FormSketch.CentreY - ltp.y) * Coeff / 4);
            Indice := Indice + 3;
          end
          else
          begin
            ltp := LTPoint(ListeTriangle[Indice]);
            Index := Room.Points.indexOf(ltp);
            reel := Room.HautPoint[Index];
            Index := Room.Points.indexOf(LTPoint(ListeTriangle[Indice + 1]));
            reel2 := Room.HautPoint[Index];
            Index := Room.Points.indexOf(LTPoint(ListeTriangle[Indice + 2]));
            reel3 := Room.HautPoint[Index];

            v1 := MakeD3DVECTOR(-(ltp.x * Coeff) +
              (LTPoint(ListeTriangle[Indice + 1]).x * Coeff),
              - Reel + Reel2, + ltp.y * Coeff - LTPoint(ListeTriangle[Indice + 1]).y * Coeff);
            v2 := MakeD3DVECTOR(LTPoint(ListeTriangle[Indice + 2]).x *
              Coeff - (LTPoint(ListeTriangle[Indice + 1]).x * Coeff),
              Reel3 - Reel2,
              - LTPoint(ListeTriangle[Indice + 2]).y *
              Coeff + (LTPoint(ListeTriangle[Indice + 1]).y * Coeff));
            Normale := CalculeNormale(v1, v2);
            if not ((normale.x = 0) and (normale.y = 0) and
               (normale.z = 0)) then
            Normale := VectorNormalize(Normale);
            Room.Normale := Normale;
            Par.p[Indice] :=
              MakeD3DVERTEX(MakeD3DVECTOR((ltp.x * Coeff),
              CumulHauteur + 0.001 + Reel + CalculeDecalage(Room, i),
              - ltp.y * Coeff),
              Normale,
              (FormSketch.CentreX - ltp.x) / scale *
              2, (FormSketch.CentreY - ltp.y) / scale * 2);
            ltp := LTPoint(ListeTriangle[Indice + 1]);
            Index := Room.Points.indexOf(ltp);
            reel := Room.HautPoint[Index];
            Par.p[Indice + 1] :=
              MakeD3DVERTEX(MakeD3DVECTOR(ltp.x * Coeff,
              CumulHauteur + 0.001 + Reel + CalculeDecalage(Room, i),
              - ltp.y * Coeff),
              Normale, (FormSketch.CentreX - ltp.x) /
              scale * 2, (FormSketch.CentreY - ltp.y) / scale * 2);
            ltp := LTPoint(ListeTriangle[Indice + 2]);
            Index := Room.Points.indexOf(ltp);
            reel := Room.HautPoint[Index];
            Par.p[Indice + 2] :=
              MakeD3DVERTEX(MakeD3DVECTOR(ltp.x * Coeff,
              CumulHauteur + 0.001 + Reel + CalculeDecalage(Room, i),
              - ltp.y * Coeff),
              Normale,
              (FormSketch.CentreX - ltp.x) /
              scale * 2, (FormSketch.CentreY - ltp.y) / scale * 2);
            Indice := Indice + 3;
          end;
        end;
        Par.NbPoints := Indice;
        Par.Alpha := False;
        par.Liste := True;
        if not Level.Toiture then
        begin
          Par.NTexture := 6;
        end
        else
        begin
          Par.NTexture := 0;
        end;
        FormSketch.Liste3DParoi.Add(par);


        Normale := MakeD3DVECTOR(0, - 1,0);
        indice := 0;
        Par := T3DParoi.Create;
        SetLength(Par.p, 1000);
        while Indice < ListeTriangle.Count do
        begin
          if not Level.Toiture then
          begin
            ltp := LTPoint(ListeTriangle[Indice]);
            Par.p[Indice + 2] :=
              MakeD3DVERTEX(MakeD3DVECTOR(ltp.x * Coeff,
              CumulHauteur + 0.001, - ltp.y * Coeff), Normale,
              (FormSketch.CentreX - ltp.x) * Coeff / 4
              , (FormSketch.CentreY - ltp.y) * Coeff / 4);
            ltp := LTPoint(ListeTriangle[Indice + 1]);
            Par.p[Indice + 1] :=
              MakeD3DVERTEX(MakeD3DVECTOR(ltp.x * Coeff,
              CumulHauteur + 0.001, - ltp.y * Coeff), Normale,
              (FormSketch.CentreX - ltp.x) * Coeff / 4
              , (FormSketch.CentreY - ltp.y) * Coeff / 4);
            ltp := LTPoint(ListeTriangle[Indice + 2]);
            Par.p[Indice + 0] :=
              MakeD3DVERTEX(MakeD3DVECTOR(ltp.x * Coeff,
              CumulHauteur + 0.001, - ltp.y * Coeff), Normale,
              (FormSketch.CentreX - ltp.x) * Coeff / 4
              , (FormSketch.CentreY - ltp.y) * Coeff / 4);
            Indice := Indice + 3;
          end
          else
          begin
            ltp := LTPoint(ListeTriangle[Indice]);
            Index := Room.Points.indexOf(ltp);
            Reel := Room.HautPoint[Index];
            Par.p[Indice + 2] :=
              MakeD3DVERTEX(MakeD3DVECTOR(ltp.x * Coeff,
              CumulHauteur + 0.001 + Reel + CalculeDecalage(Room, i),
              - ltp.y * Coeff),
              Normale, (FormSketch.CentreX - ltp.x) /
              scale, (FormSketch.CentreY - ltp.y) / scale);
            ltp := LTPoint(ListeTriangle[Indice + 1]);
            Index := Room.Points.indexOf(ltp);
            Reel := Room.HautPoint[Index];
            Par.p[Indice + 1] :=
              MakeD3DVERTEX(MakeD3DVECTOR(ltp.x * Coeff,
              CumulHauteur + 0.001 + Reel + CalculeDecalage(Room, i),
              - ltp.y * Coeff),
              Normale,
              (FormSketch.CentreX - ltp.x) /
              scale, (FormSketch.CentreY - ltp.y) / scale);
            ltp := LTPoint(ListeTriangle[Indice + 2]);
            Index := Room.Points.indexOf(ltp);
            Reel := Room.HautPoint[Index];
            Par.p[Indice + 0] :=
              MakeD3DVERTEX(MakeD3DVECTOR(ltp.x * Coeff,
              CumulHauteur + 0.001 + reel + CalculeDecalage(Room, i),
              - ltp.y * Coeff),
              Normale,
              (FormSketch.CentreX - ltp.x) /
              scale, (FormSketch.CentreY - ltp.y) / scale);
            Indice := Indice + 3;
          end
        end;

        Par.NbPoints := Indice;
        Par.Alpha := False;
        par.Liste := True;
        Par.NTexture := 5;
        FormSketch.Liste3DParoi.Add(par);

        if not level.Toiture and (i = LevelAAfficher - 1) and
          (LevelAAfficher <> FormSketch.TrackBarLevel.Position) then
        begin
          Par2 := T3DParoi.Create;
          SetLength(Par2.p, 1000);
          Par.Assign(Par2);
          for kl := 0 to Par.NbPoints - 1 do
          begin
            par2.NTexture := 5;
            par2.p[kl].y := par2.p[kl].y + Level.Height;
          end;
          par2.Liste := True;
          FormSketch.Liste3DParoi.Add(Par2);

          Par2 := T3DParoi.Create;
          SetLength(Par2.p, 1000);
          Par.Assign(Par2);
          for kl := 0 to Par.NbPoints - 1 do
          begin
            par2.NTexture := 5;
            par2.p[kl] := par.p[Par.NbPoints - 1 - kl];
            par2.p[kl].y := par2.p[kl].y + Level.Height;
          end;
          par2.Liste := True;
          FormSketch.Liste3DParoi.Add(Par2);
        end;
        ListeTriangle.Free;
      end;
    end;
    // Rajouter les "sheds'
    if level.Toiture then
      for j := 0 to Level.Rooms.Count - 2 do
      begin
        for k := j + 1 to Level.Rooms.Count - 1 do
        begin
          r1 := TRoom(Level.Rooms[j]);
          r2 := TRoom(Level.Rooms[k]);
          for l := 0 to r1.Points.Count - 2 do
          begin
            p := LTpoint(r1.Points[l]);
            p2 := LTpoint(r1.Points[l + 1]);
            position1 := r2.Points.IndexOf(p);
            position2 := r2.Points.IndexOf(p2);
            if (position1 <> -1) and (Position2 <> -1) and
              ((R1.HautPoint[l] <> R2.HautPoint[Position1]) or
              (R1.HautPoint[l + 1] <> R2.HautPoint[Position2])) then
            begin
              Normale := MakeD3DVector(p2.y - p.y, 0, p2.x - p.x);
              Normale := VectorNormalize(Normale);
              Par := T3DParoi.Create;
              SetLength(Par.p, 4);
              if (R1.RoomType <> roomObstruction) then
              begin
                Reel := R1.HautPoint[l];
                Par.p[0] := MakeD3DVERTEX(MakeD3DVECTOR(p.x * Coeff,
                  CumulHauteur + Reel, - p.y * Coeff),
                  Normale, 0.0, 0.0);
                Reel := R1.HautPoint[l + 1];
                vp1 := MakeD3DVECTOR(p2.x * Coeff,
                  CumulHauteur + Reel, - p2.y * Coeff);

                Reel := R2.HautPoint[Position1];
                Par.p[1] := MakeD3DVERTEX(MakeD3DVECTOR(p.x * Coeff,
                  (CumulHauteur + Reel), - p.y * Coeff),
                  Normale, Level.Height / 3, 0.0);
                Reel := R2.HautPoint[Position2];
                vp2 := MakeD3DVECTOR(p2.x * Coeff,
                  (CumulHauteur + Reel), - p2.y * Coeff);

                Par.p[2] := MakeD3DVERTEX(vp1, Normale,
                  0.0, Seg.Size(1 / Coeff) / 3);
                Par.p[3] := MakeD3DVERTEX(vp2, Normale,
                  Level.Height / 3, Seg.Size(1 / Coeff) / 3);
                Par.Alpha := False;
                Par.NTexture := 5;
              end;
              Par.NbPoints := 4;
              Par.Alpha := False;
              Par.NTexture := 1;
              FormSketch.Liste3DParoi.Add(Par);

              Normale := MakeD3DVector(p.y - p2.y, 0, p.x - p2.x);
              Normale := VectorNormalize(Normale);
              Par := T3DParoi.Create;
              SetLength(Par.p, 4);
              if (Room.RoomType <> roomObstruction) then
              begin
                Reel := R1.HautPoint[l];
                Par.p[1] := MakeD3DVERTEX(MakeD3DVECTOR(p.x * Coeff,
                  CumulHauteur + Reel, - p.y * Coeff),
                  Normale, 0.0, 0.0);
                Reel := R1.HautPoint[l + 1];
                vp1 := MakeD3DVECTOR(p2.x * Coeff,
                  CumulHauteur + Reel, - p2.y * Coeff);

                Reel := R2.HautPoint[Position1];
                Par.p[0] := MakeD3DVERTEX(MakeD3DVECTOR(p.x * Coeff,
                  (CumulHauteur + Reel), - p.y * Coeff),
                  Normale, Level.Height / 3, 0.0);
                Reel := R2.HautPoint[Position2];
                vp2 := MakeD3DVECTOR(p2.x * Coeff,
                  (CumulHauteur + Reel), - p2.y * Coeff);

                Par.p[3] := MakeD3DVERTEX(vp1, Normale,
                  0.0, Seg.Size(1 / Coeff) / 3);
                Par.p[2] := MakeD3DVERTEX(vp2, Normale,
                  Level.Height / 3, Seg.Size(1 / Coeff) / 3);
                Par.Alpha := False;
                Par.NTexture := 5;
              end;
              Par.NbPoints := 4;
              Par.Alpha := False;
              Par.NTexture := 5;
              FormSketch.Liste3DParoi.Add(Par);
            end;
          end;
        end;
      end;
    CumulHauteur := CumulHauteur + Level.Height;
  end;
  if NbCentre <> 0 then
  begin
    FormSketch.CentreX := FormSketch.CentreX / NbCentre * Coeff;
    FormSketch.CentreY := -FormSketch.CentreY / NbCentre * Coeff;
  end;
  if FormSketch.CheckGround.Checked then
  begin
    FormSketch.Sol := T3DParoi.Create;
    SetLength(FormSketch.Sol.p, 4);
    FormSketch.sol.NbPoints := 4;
    MinX := MinX * Coeff - 5;
    MinY := -MinY * Coeff + 5;
    MaxX := MaxX * Coeff + 5;
    MaxY := -MaxY * Coeff - 5;
    Normale := MakeD3DVECTOR(0,1,0);
    FormSketch.Sol.p[3] := MakeD3DVERTEX(MakeD3DVECTOR(MinX, - 0.2,MinY),
      Normale, 1.0, 1.0);
    FormSketch.Sol.p[2] := MakeD3DVERTEX(MakeD3DVECTOR(MinX, - 0.2,MaxY),
      Normale, 0, 1.0);
    FormSketch.Sol.p[1] := MakeD3DVERTEX(MakeD3DVECTOR(MaxX, - 0.2,MinY),
      Normale, 1.0, 0.0);
    FormSketch.Sol.p[0] := MakeD3DVERTEX(MakeD3DVECTOR(MaxX, - 0.2,MaxY),
      Normale, 0.0, 0.0);
    FormSketch.Sol.NTexture := 2;
    FormSketch.Sol.Alpha := False;
    FormSketch.Liste3DParoi.Add(FormSketch.Sol);
  end;
end;

end.
