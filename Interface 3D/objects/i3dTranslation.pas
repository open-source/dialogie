unit i3dTranslation;

interface

//********************************************************************
//
// SOFTWARE : LT For Europe
//
// PROJECT : Interface LT
// UNIT : Translation unit
//
// AUTHORS : Renaud MIKOLASEK
// ORGANIZATION : GEFOSAT
//
// DATE OF CREATION : 26/04/99
// DATE OF MODIFICATION : 26/04/99
//
// COMMENTS : This unit can translate all the objects and the
//            messages of the interface using two text files.
//            The first one contains all the messages in all languages
//            The second contains all the labels of all visible objects
//            on the screen.
//
//*********************************************************************


Uses
  forms,
  classes,
  stdctrls,
  i3dConstants,
  SysUtils,
  Menus,
  comctrls,
  extctrls,Dialogs,
  Controls;

         Procedure ListConstruction;
         Procedure Recurs(comp : Tcomponent; Path : String; List : TStringList);
         Procedure Translate(Language : Integer);
         Function  SearchComponent(ComponentName : String;Var Special : Integer) : TComponent;
         Procedure LoadMessages(Langue : Integer ; filePath : string = '');

Var
   Labels : TStringList;  // This StringList contains the messages in
                          // the selected language
   Countries : TStringList; // This StringList contains the name of countries
                            // in the selected language
   ClimateZones : TStringList; // This StringList contains the name of climateZones
                               // in the selected language

implementation

Uses IzApp;

// ******************************************************************
// This procedure load the messages in the file 'message.txt'
// then create a memory list (Label) of the messages concerned by the language
// INPUT : Language -> (Integer) each number correspond to a language
// OUTPUT : Fill the LABELS array of Strings
// ******************************************************************

Procedure LoadMessages(Langue : Integer ; filePath : string = '');
Var
   List : TStringList;
   TextLine : String;
   Index : Integer;
begin
     // Initialisation and loading of the file
     Labels.Free;
     Labels := TStringList.Create;
     Countries.Free;
     Countries := TStringList.Create;
     ClimateZones.Free;
     ClimateZones := TStringList.Create;
     Labels.Add(' ');
     List := TStringList.Create;

     if filePath = '' then
      filePath := TApp.path +FileMessages;
     List.LoadFromFile(filePath);
     Index := 1;
     if Index < List.count then
        TextLine := List[Index];
     While Index < List.Count do
     begin
          // here is the selection of the lines of the correct language
          Labels.add(List[Index+Langue-1]);
          Index := Index+NumberOfLanguage;
          if Index < List.Count then TextLine := List[Index];
     end;

     List.Free;
end;

// ******************************************************************
// This procedure construct a list of all component labels
// Then the list is saved into the file 'Objects.txt'
// OUTPUT : Create a text file that contains all the labels of components
// ******************************************************************

Procedure ListConstruction;
Var
   List : TStringList;
   compo : TComponent;
   i : Integer;
begin
     List := TStringList.Create;
     List.LoadFromFile(TApp.path+FileObjects);

       For i := 1 to Application.ComponentCount do
        begin
             // Get the main components
             Compo := Application.Components[i-1];
             // Call a recursive procedure to explore all the sub-objects
             recurs(Compo,'Application.',List);
        end;

     // Save the list into the file 'Objects.txt'
     List.SaveTofile(TApp.path+FileObjects);
     List.Free;
end;

// ******************************************************************
// This is a recursive procedure that explore the visible property
// of the component and all its sub-component sent in parameter
// Then the list is saved into the file 'Objects.txt'
// INPUT : Comp -> Component to explore
//         Path -> Object path to the component
//         List -> Contain the text list of all the previous found
//                 labels.
// ******************************************************************

Procedure Recurs(comp : Tcomponent; Path : String; List : TStringList);
Var
   I,j, Index : Integer;
   compo : TComponent;
   Addon,TempString : String;
begin
        begin
         // Add the sub-component name to the component name
         Path := Path+Comp.Name+'.';
         // Get the label of the component
         Addon := '';
         if (Comp is TControl) and (TControl(Comp).ShowHint = True) and
            (TControl(Comp).hint <> '') then
         Begin
             Addon := TControl(Comp).hint;
             Index :=  List.IndexOf(Path+'Hint');
             if (Index = -1) then
             begin
                  Index := List.add(Path+'Hint');
                  List.Insert(Index+1,Addon);
                  For j := 2 to NumberOfLanguage do
                       List.insert(Index+j,'***************  TO TRANSLATE IN '+NamesOfLanguage[j]);
                  //List.Insert(Index+1,'');
                  //List.Insert(Index+2,'');
                  //List.Insert(Index+3,'');
             end;
         End;
         Addon := '';
         if Comp is TButton then
           Addon := TButton(Comp).Caption
         else if Comp is TLabel then
           Addon := TLabel(Comp).Caption
         else if Comp is TForm then
           Addon := TForm(Comp).Caption
         else if Comp is TGroupBox then
           Addon := TGroupBox(Comp).Caption
         else if Comp is TRadioButton then
           Addon := TRadioButton(Comp).Caption
         else if Comp is TTabSheet then
           Addon := TTabSheet(Comp).Caption
         else if Comp is TMenuItem then
           Addon := TMenuItem(Comp).Caption
         else if Comp is TCheckBox then
           Addon := TCheckBox(Comp).Caption
         else if Comp is TRadioGroup then
           Addon := TRadioGroup(Comp).Caption;
         // Normal Case
         if Addon <> '' then
         begin
             Index :=  List.IndexOf(Path+'Caption');
             if (Index = -1) then
             begin
                  // reservation of space in the list for the new component
                  Index := List.add(Path+'Caption');
                  For i := 1 to NumberOfLanguage do
                      List.Insert(Index+i,'');
             end;
             if (List[Index+1] <> Addon) and (List[Index+1] <> '#####') then
             begin
                  // Here filling of the space reserved
                  List[Index+1] := Addon;
                  For i := 2 to NumberOfLanguage do
                     List[Index+i] := '***************  TO TRANSLATE IN '+NamesOfLanguage[i];
             end;
         end;
         // Special case of RadioGroup
         if Comp is TRadioGroup then
         begin
             For i := 1 to TRadioGroup(Comp).items.count do
             begin
                  tempstring := intToStr(i-1);
                  Index :=  List.IndexOf(Path+'items['+TempString+']');
                  if Index = -1 then
                  begin
                       Index := List.add(Path+'items['+TempString+']');
                       For j := 1 to NumberOfLanguage do
                           List.Insert(Index+j,Addon);
                  end;
                  if List[Index+1] <> TRadioGroup(Comp).items[i-1] then
                  begin
                       List[Index+1] := TRadioGroup(Comp).items[i-1];
                       For j := 2 to NumberOfLanguage do
                           List[Index+j] := '***************  TO TRANSLATE IN '+NamesOfLanguage[j];
                  end
             end
         end
         // Special case of ComboBox
         else if Comp is TComboBox then
         begin
             For i := 1 to TComboBox(Comp).items.count do
             begin
                  tempString := intToStr(i-1);
                  Index :=  List.IndexOf(Path+'items['+TempString+']');
                  if Index = -1 then
                  begin
                       Index := List.add(Path+'items['+TempString+']');
                       For j := 1 to NumberOfLanguage do
                           List.Insert(Index+j,Addon);
                  end;
                  if (List[Index+1] <> TComboBox(Comp).items[i-1])
                     and (List[index+1] <> '#####') then
                  begin
                       List[Index+1] := TComboBox(Comp).items[i-1];
                       For j := 2 to NumberOfLanguage do
                           List[Index+j] := '**TO TRANSLATE IN '+NamesOfLanguage[j];
                  end
             end;
         end
         else if Comp is TMenu then

         else
         // Explore all sub-components
         For i := 1 to Comp.ComponentCount do
         begin
             Compo := Comp.Components[i-1];
             recurs(Compo,Path,List);
         end
        end;
end;


// ******************************************************************
// This procedure will change all the component labels using the
// language sent in parameter. Then it will load corresponding messages
// INPUT : Language -> Ordinal of the language
// ******************************************************************

Procedure Translate(Language : Integer);
Var
   List : TStringList;
   i,j,Index : Integer;
   MyComp : TComponent;
   Special : Integer;
begin
     // Loading of the file containing all labels
     List := TStringList.Create;
     List.LoadFromFile(TApp.path+FileObjects);

     // Browse the file to translate component labels
     For i := 1 to (List.Count div (NumberOfLanguage+1)) do
     begin
       Index := (i-1)*(NumberOfLanguage+1);
       if List.Strings[index+Language] <> '#####' then
       begin
          //if List.Strings[Index] = 'Application.FormSketch.Display1.Caption' then
          //   Beep;
          MyComp := SearchComponent(List.Strings[Index],Special);
          //if MyComp is TToolButton then
          //   Beep;
          if Pos('.Hint',List.Strings[Index]) <> 0 then
          begin
             if (MyComp <> nil) then
                TControl(MyComp).Hint := List.Strings[Index+Language]
          end
          else if MyComp is TButton then
           TButton(MyComp).Caption := List.Strings[Index+Language]
          else if MyComp is TLabel then
           TLabel(MyComp).Caption := List.Strings[Index+Language]
          else if MyComp is TForm then
           TForm(MyComp).Caption := List.Strings[Index+Language]
          else if MyComp is TGroupBox then
           TGroupBox(MyComp).Caption := List.Strings[Index+Language]
          else if MyComp is TRadioButton then
           TRadioButton(MyComp).Caption := List.Strings[Index+Language]
          else if MyComp is TTabSheet then
           TTabSheet(MyComp).Caption := List.Strings[Index+Language]
          else if MyComp is TMenuItem then
           TMenuItem(MyComp).Caption := List.Strings[Index+Language]
          else if MyComp is TCheckBox then
           TCheckBox(MyComp).Caption := List.Strings[Index+Language]
          else if MyComp is TComboBox then
          begin
               If special = 0 then TComboBox(MyComp).items.Clear;
               TComboBox(MyComp).Items.add(List.Strings[Index+Language])
          end
          else if MyComp is TRadioGroup then
          begin
               If special = -1 then
               Begin
                    TRadioGroup(MyComp).Caption := List.Strings[Index+Language];
                    TRadioGroup(MyComp).items.Clear
               End
               Else
                   TRadioGroup(MyComp).Items.Add(List.Strings[Index+Language])
          end
          else if MyComp = Nil then
          begin
               List[Index] := '';
               For j := 1 to NumberOfLanguage do
                   List.Insert(Index+j,'');
          end
       End;
     end;

     For i := List.count-1 downto 0 do
         if List[i] = '' then
            List.Delete(i);

     FileSetAttr(PChar(TApp.path+FileObjects),
             FileGetAttr(PChar(TApp.path+FileObjects))
             and not faReadOnly);
     List.saveToFile(TApp.path+FileObjects);
     List.free;
end;

// ******************************************************************
// This function will return a Component by its absolute name.
// The component contains an array of string, Special return the
// index in this array.
// INPUT : ComponentName -> Complete name of the component.
// OUTPUT : Special -> The index in the array if necessary, else -1
// RETURN : The Component required
// ******************************************************************

Function SearchComponent(ComponentName : String;Var Special : Integer) : TComponent;
Var
   TempCompo : TComponent;
   Piece : String;
   Index, StringError : Integer;
begin
     Special := -1;
     Index := Pos('.Caption',ComponentName);
     if Index = 0 then
        Index := Pos('.Hint',ComponentName);
     if Index = 0 then
     begin
          Piece := Copy(ComponentName,Pos('.items[',ComponentName),20);
          Delete(Piece,1,Pos('[',Piece));
          Val (Copy(Piece,1,Pos(']',Piece)-1), special, StringError);
          Index := Pos('.items[',ComponentName);
     end;
     Delete(ComponentName,Index+1,10);
     Delete(ComponentName,1,Pos('.',ComponentName));
     TempCompo := Application;
     While (ComponentName <> '') and (TempCompo <> Nil) do
     begin
          Piece := Copy(ComponentName,1,Pos('.',ComponentName)-1);
          Delete(ComponentName,1,Pos('.',ComponentName));
          TempCompo := TempCompo.FindComponent(Piece);
     end;
     SearchComponent := TempCompo;
end;


end.
 