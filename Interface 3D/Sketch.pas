unit Sketch;

interface

{$I delphiX\DelphiXcfg.inc}

uses
  Forms,
  Windows,
  Messages,

  i3dSketchObject,


  ToolWin,
  ExtDlgs,

  i3dBuildingData,

  Compositions,

  DXDraws,
  DXClass,
  D3DUtils,
  i3dGeometry,
  DXSprite,

  i3dSketchLevel,
  i3dDialogieExport,
  XPMan,
  Dialogs,
  AppEvnts,
  ExtCtrls,
  Menus,

  ImgList,
  Controls,
  ComCtrls,
  StdCtrls,
  Buttons,

  Classes, Graphics, SysUtils,
  IzUiUtil,
{$IFDEF StandardDX}
  Direct3D, DirectDraw;
{$ELSE}
  DirectX, System.ImageList;
{$ENDIF}




type

  TCursorSprite = class(TImageSPrite)

  end;

  TFormSketch = class(TdxForm)

    MainMenu1: TMainMenu;
    Help1: TMenuItem;
    ImageListIcons: TImageList;
    SaveDialogProject: TSaveDialog;
    OpenDialogProject: TOpenDialog;
    PopupMenuTools: TPopupMenu;
    Horizonsymetry1: TMenuItem;
    Flipvertical1: TMenuItem;
    N4: TMenuItem;
    Rotate901: TMenuItem;
    Rotate902: TMenuItem;
    Rotate1801: TMenuItem;
    N5: TMenuItem;
    AligntoGrid1: TMenuItem;
    Edition1: TMenuItem;
    Undolastcommand1: TMenuItem;
    Redo1: TMenuItem;
    N6: TMenuItem;
    Copy1: TMenuItem;
    Cut1: TMenuItem;
    Paste1: TMenuItem;
    Clear1: TMenuItem;
    ControlBarMenu: TControlBar;
    ToolBar2: TToolBar;
    ToolButtonUndo: TToolButton;
    ToolButtonRedo: TToolButton;
    ToolButtonS1: TToolButton;
    ToolButtonCopy: TToolButton;
    ToolButtonCut: TToolButton;
    ToolButtonPaste: TToolButton;
    ToolButtonClear: TToolButton;
    SelectAll1: TMenuItem;
    Timer1: TTimer;
    HelpList: TImageList;
    ToolButtonS3: TToolButton;
    ToolSelectAll: TToolButton;
    Levels1: TMenuItem;
    Duplicatecurrentlevel1: TMenuItem;
    Deletecurrentlevel1: TMenuItem;
    Assistant1: TMenuItem;
    PopupMenuZone: TPopupMenu;
    Defineroomasshading1: TMenuItem;
    Defineroomasexternal1: TMenuItem;
    N7: TMenuItem;
    Undefineroom1: TMenuItem;
    N9: TMenuItem;
    PageControl1: TPageControl;
    TabSheetSketch: TTabSheet;
    GroupBoxViewOptions: TGroupBox;
    SpeedZoomOut: TSpeedButton;
    SpeedZoomIn: TSpeedButton;
    PanelSketchingtools: TPanel;
    SpeedButtonLinePoint: TSpeedButton;
    SpeedButtonClear: TSpeedButton;
    SpeedButtonContinuousLine: TSpeedButton;
    SpeedButtonRectangle: TSpeedButton;
    SpeedButtonMoveProject: TSpeedButton;
    SpeedButtonMoveRotate: TSpeedButton;
    SpeedButtonCenter: TSpeedButton;
    SpeedWall: TSpeedButton;
    SpeedButtonCurve: TSpeedButton;
    SpeedZone: TSpeedButton;
    BitBtnTransform: TBitBtn;
    ImageSketch: TImage;
    StatusBar: TStatusBar;
    ToolBar4: TToolBar;
    ToolButtonHelpIndex: TToolButton;
    ToolButtonContent: TToolButton;
    ToolButtonAssistant: TToolButton;
    PrintDialog1: TPrintDialog;
    Printcompletesketch1: TMenuItem;
    Printcurrentlevelofthesketch1: TMenuItem;
    SAvecurrentlevelasmetafile1: TMenuItem;
    SaveDialogMeta: TSaveDialog;
    Importpicture1: TMenuItem;
    OpenPictureDialog1: TOpenPictureDialog;
    Deletebackgroundpicture1: TMenuItem;
    Userrotate1: TMenuItem;
    ToolButton1: TToolButton;
    N14: TMenuItem;
    TabSheetDonnees: TTabSheet;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    GroupBox3: TGroupBox;
    Label3: TLabel;
    Label7: TLabel;
    StaticCompoExt: TStaticText;
    BitBtn1: TBitBtn;
    Label8: TLabel;
    StaticCompofloor: TStaticText;
    BitBtn2: TBitBtn;
    CheckCrawlSpace: TCheckBox;
    Label10: TLabel;
    StaticCompoRoof: TStaticText;
    BitBtn5: TBitBtn;
    CheckLoft: TCheckBox;
    StaticCompoWindow: TStaticText;
    BitBtn6: TBitBtn;
    StaticCompoDoor: TStaticText;
    BitBtn7: TBitBtn;
    EditWindowW: TEdit;
    EditDoorW: TEdit;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    EditWindowH: TEdit;
    Label14: TLabel;
    EditDoorH: TEdit;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Definecharacteristicsoftheroom1: TMenuItem;
    N10: TMenuItem;
    ApplicationEvents1: TApplicationEvents;
    ImageListMat: TImageList;
    SpeedPorte: TSpeedButton;
    PopupFenetre: TPopupMenu;
    insertadoor1: TMenuItem;
    insertawindow1: TMenuItem;
    N11: TMenuItem;
    Editadoororawindow1: TMenuItem;
    Deleteadoorwindow1: TMenuItem;
    SaveDialogExport: TSaveDialog;
    N13: TMenuItem;
    Definethenorthposition1: TMenuItem;
    Movebackgroundpicture1: TMenuItem;
    TabSheet3D: TTabSheet;
    Panel1: TPanel;
    DXTimer: TDXTimer;
    DXImageList: TDXImageList;
    DXDraw: TDXDraw;
    Timer2: TTimer;
    DXImageList1: TDXImageList;
    RadioGroupDetail: TRadioGroup;
    RadioGroupLight: TRadioGroup;
    SaveDialogbmp: TSaveDialog;
    GroupBox7: TGroupBox;
    TrackBarLevel: TTrackBar;
    RadioGroupMovingMode: TRadioGroup;
    CheckBoxToit: TCheckBox;
    N15: TMenuItem;
    Alignpointsbetweenlevels1: TMenuItem;
    Display1: TMenuItem;
    Grid1: TMenuItem;
    N16: TMenuItem;
    Pptions1: TMenuItem;
    RadioButtonNoGrid: TMenuItem;
    RadioButtonGrid1: TMenuItem;
    RadioButtongrid2: TMenuItem;
    CheckBoxLength: TMenuItem;
    CheckRoomName: TMenuItem;
    CheckBoxArea: TMenuItem;
    CheckMagnetism: TMenuItem;
    N17: TMenuItem;
    EditlevelHeight: TEdit;
    Labelmeter: TLabel;
    LabelHeight: TLabel;
    UpDownLevel: TUpDown;
    LabelCurrentLevel: TLabel;
    ColorDialog1: TColorDialog;
    CheckBoxVolume: TMenuItem;
    Properties1: TMenuItem;
    BitBtn17: TBitBtn;
    BitBtn18: TBitBtn;
    GroupBox8: TGroupBox;
    BitBtn15: TBitBtn;
    BitBtn14: TBitBtn;
    CheckGround: TCheckBox;
    BitBtn16: TBitBtn;
    ShapeColor: TShape;
    SpeedChg: TSpeedButton;
    ButtonRecal: TButton;
    Dfinirlapicecommeunlocalexterne1: TMenuItem;
    Fichier1: TMenuItem;
    Nouveau1: TMenuItem;
    Label2: TLabel;
    StaticCompoIntFloor: TStaticText;
    BitBtn3: TBitBtn;
    Label4: TLabel;
    StaticCompoInt: TStaticText;
    BitBtn4: TBitBtn;
    ToolBar1: TToolBar;
    BitBtn8: TBitBtn;
    XPManifest1: TXPManifest;
    buttonQuit: TButton;
    procedure BitBtn8Click(Sender: TObject);

    procedure FormCreate(Sender: TObject);
    procedure ImageSketchMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: integer);
    procedure ImageSketchMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: integer);
    procedure ManageAction;
    procedure SpeedButtonLinePointClick(Sender: TObject);
    procedure SpeedButtonMoveRotateClick(Sender: TObject);
    procedure SpeedButtonContinuousLineClick(Sender: TObject);
    procedure SpeedButtonRectangleClick(Sender: TObject);
    procedure SpeedButtonClearClick(Sender: TObject);
    procedure SpeedButtonMoveProjectClick(Sender: TObject);
    procedure SpeedButtonCenterClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ImageSketchMouseMove(Sender: TObject; Shift: TShiftState; X, Y: integer);
    procedure RadioButtonNoGrid2Click(Sender: TObject);
    procedure RadioButtonGrid12Click(Sender: TObject);
    procedure RadioButtongrid22Click(Sender: TObject);
    procedure SpeedZoomInClick(Sender: TObject);
    procedure SpeedZoomOutClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure Saveproject2Click(Sender: TObject);
    procedure Saveproject1Click(Sender: TObject);
    procedure Newproject1Click(Sender: TObject);
    procedure Open1Click(Sender: TObject);
    procedure BitBtnTransformClick(Sender: TObject);
    procedure Rotate901Click(Sender: TObject);
    procedure Rotate902Click(Sender: TObject);
    procedure Rotate1801Click(Sender: TObject);
    procedure AligntoGrid1Click(Sender: TObject);
    procedure Horizonsymetry1Click(Sender: TObject);
    procedure CheckBoxLength2Click(Sender: TObject);
    procedure Flipvertical1Click(Sender: TObject);
    procedure UpDownLevelClick(Sender: TObject; Button: TUDBtnType);
    procedure Undolastcommand1Click(Sender: TObject);
    procedure Redo1Click(Sender: TObject);
    procedure Copy1Click(Sender: TObject);
    procedure Cut1Click(Sender: TObject);
    procedure Clear1Click(Sender: TObject);
    procedure Paste1Click(Sender: TObject);
    procedure SelectAll1Click(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure SpeedWallClick(Sender: TObject);
    procedure Quit1Click(Sender: TObject);
    procedure EditlevelHeightChange(Sender: TObject);
    procedure StatusBarDrawPanel(StatusBar: TStatusBar; Panel: TStatusPanel; const Rect: TRect);
    procedure SpeedButtonCurveClick(Sender: TObject);
    procedure Duplicatecurrentlevel1Click(Sender: TObject);
    procedure Deletecurrentlevel1Click(Sender: TObject);
    procedure Assistant1Click(Sender: TObject);
    procedure Defineroomasshading1Click(Sender: TObject);
    procedure Defineroomasabuferspace1Click(Sender: TObject);
    procedure Defineroomasrooflightened1Click(Sender: TObject);
    procedure Defineroomasexternal1Click(Sender: TObject);
    procedure Undefineroom1Click(Sender: TObject);
    procedure SpeedZoneClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
    procedure BitBtnNext1Click(Sender: TObject);
    procedure Printcurrentlevelofthesketch1Click(Sender: TObject);
    procedure Printcompletesketch1Click(Sender: TObject);
    procedure CheckBoxAreaClick(Sender: TObject);
    procedure SAvecurrentlevelasmetafile1Click(Sender: TObject);
    procedure Importpicture1Click(Sender: TObject);
    procedure Deletebackgroundpicture1Click(Sender: TObject);
    procedure Userrotate1Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn5Click(Sender: TObject);
    procedure BitBtn6Click(Sender: TObject);
    procedure BitBtn7Click(Sender: TObject);
    procedure Definecharacteristicsoftheroom1Click(Sender: TObject);
    procedure CheckBoxZoning2Click(Sender: TObject);
    procedure CheckCrawlSpaceClick(Sender: TObject);
    procedure CheckLoftClick(Sender: TObject);
    procedure EditWindowWChange(Sender: TObject);
    procedure EditWindowHChange(Sender: TObject);
    procedure EditDoorWChange(Sender: TObject);
    procedure EditDoorHChange(Sender: TObject);
    procedure ApplicationEvents1Exception(Sender: TObject; E: Exception);
    procedure SpeedPorteClick(Sender: TObject);
    procedure RafraichisInterface;
    procedure insertadoor1Click(Sender: TObject);
    procedure insertawindow1Click(Sender: TObject);
    procedure Editadoororawindow1Click(Sender: TObject);
    procedure Deleteadoorwindow1Click(Sender: TObject);
    procedure Definethenorthposition1Click(Sender: TObject);
    procedure Movebackgroundpicture1Click(Sender: TObject);
    procedure DXDrawInitialize(Sender: TObject);
    procedure DXDrawFinalize(Sender: TObject);
    procedure DXDrawInitializeSurface(Sender: TObject);
    procedure DXTimerTimer(Sender: TObject; LagCount: integer);
    procedure PageControl1Changing(Sender: TObject; var AllowChange: boolean);
    procedure DXDrawMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: integer);
    procedure DXDrawMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: integer);
    procedure Timer2Timer(Sender: TObject);
    procedure TrackBar1Change(Sender: TObject);
    procedure RadioQuickLightClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure RadioGroupDetailClick(Sender: TObject);
    procedure RadioGroupLightClick(Sender: TObject);
    procedure TrackBarLevelChange(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure BitBtn14Click(Sender: TObject);
    procedure CheckBoxToitClick(Sender: TObject);
    procedure Alignpointsbetweenlevels1Click(Sender: TObject);
    procedure RadioButtonNoGridClick(Sender: TObject);
    procedure RadioButtonGrid1Click(Sender: TObject);
    procedure RadioButtongrid2Click(Sender: TObject);
    procedure CheckBoxLengthClick(Sender: TObject);
    procedure CheckRoomNameClick(Sender: TObject);
    procedure Areaofrooms1Click(Sender: TObject);
    procedure CheckMagnetismClick(Sender: TObject);
    procedure RadioGroupMovingModeClick(Sender: TObject);
    procedure CheckBoxZoneClick(Sender: TObject);
    procedure BitBtn15Click(Sender: TObject);
    procedure CheckGroundClick(Sender: TObject);
    procedure BitBtn16Click(Sender: TObject);
    procedure CheckBoxVolumeClick(Sender: TObject);
    procedure Properties1Click(Sender: TObject);
    procedure French1Click(Sender: TObject);
    procedure SpeedChgClick(Sender: TObject);
    procedure ButtonRecalClick(Sender: TObject);
    procedure Dfinirlapicecommeunlocalexterne1Click(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure BitBtn4Click(Sender: TObject);
    procedure FormMouseWheel(Sender: TObject; Shift: TShiftState; WheelDelta: integer; MousePos: TPoint; var Handled: boolean);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure ImageSketchMouseLeave(Sender: TObject);
    procedure buttonQuitClick(Sender: TObject);
  public
    { Public Declaration }
    LanguageUsed: integer; // Index of the language wanted

    MouseDownPos, MouseUpPos: TPoint; // store the position of the mouse
    // during the use of themousebutton
    MouseButton: TMouseButton; // store the button pressed
    MouseState: integer; // Mouse Button State (1 = pressed, 0 = released)
    ShiftKey: TShiftState; // Shift state during click of the user
    Undo: TList; // List of Undo
    IndexUndo: integer; // Position into the undo list
    OldCursorPos: TPoint;
    ZoningDone: boolean;
    HelpDisplayed: boolean;
    RoseDesVents: TPicture;
    // Configuration
    AutoShowHide: boolean;
    Language: integer;
    GoodClick: boolean;
    Liste3DParoi: TList;
    CentreX, CentreY: Double;
    Sol: T3DParoi;
    AncienneAction: integer;
    saveFileName: String;
    PointPrec: TPoint;

    class function getAppVersionLabel: string;
  private
    dxMouseDown : boolean;
    dataSaved : boolean;
    FTexture: array [0 .. 8] of TDirect3DTexture2;
    ActiveMouvement1, ActiveMouvement3: boolean;
    OrientationCam, PositionCamX, PositionCamY, PositionCamZ, InclinaisonCam: Double;
    procedure FrameMovie(Time: Double);

    function exportData() : boolean;
    procedure initTool;
    procedure DisplayHelp(CurrentAction: Integer; Can2: TCanvas);
  end;

var
  FormSketch: TFormSketch;

implementation

{$R *.DFM}
{$R Extend.res}

uses
  i3dUnit3D,
  i3dTranslation,
  i3dFormHelp,
  i3dFormChoixCompo,
  i3dFormChoixFenetre,
  i3dFormInsertPorte,
  i3dFormInsertFenetre,
  i3dFormPositionNord,
  i3dToiture,
  FProgress,
  i3dFormProp,
  i3dFormChangeCompo,
  IzApp,
  IzConstantes,
  i3dConstants,
  UtilitairesDialogie,
  System.Math,
  Vcl.Printers,
  i3dUtil;


procedure TFormSketch.initTool();
begin
  project.sketchObject.InitTool;
  BitBtntransform.Enabled := (Project.SketchObject.Selection.Count <> 0);
  project.sketchObject.Refresh();
end;



// ********************************************************
//
// This procedure happens at the launching of the software
// It is here that the initialisation occurs
//
// ********************************************************


procedure TFormSketch.FormCreate(Sender: TObject);
var
  i: integer;
  TempString: string;
  Recto: TRect;
  Size: TSize;
begin
  Caption := getAppVersionLabel;
  dataSaved := false;
  dxMouseDown := false;

  DXDraw.Initialize;
  OrientationCam := 0.4;
  InclinaisonCam := -0.7;
  PositionCamX := 0;
  PositionCamY := 40;
  PositionCamZ := -50;
  Liste3DParoi := TList.Create;
  GoodClick := False;
  TempString := FloatToStr(0.1);
  SymboleDecimal := TempString[2];

  OpenDialogProject.InitialDir := TApp.path;

  // Creation of the list of Undo
  ZoningDone := true;
  Undo := TList.Create;
  for i := 0 to MaxUndo do
    Undo.Add(TStringList.Create);
  IndexUndo := -1;


  Screen.Cursors[crEraserCursor] := LoadCursor(HInstance, 'Eraser');
  // Creation of the  Project;

  MouseState := 0;

  // Creation and initialization of the Sketch Image
  ImageSketch.Picture := TPicture.Create;
  ImageSketch.Picture.Bitmap.PixelFormat := pf24Bit;
  ImageSketch.Picture.Bitmap.Width := ImageSketch.Width;
  ImageSketch.Picture.Bitmap.Height := ImageSketch.Height;

  Project.sketchObject.imageUi := ImageSketch;
  T3DUtil.statusBar := StatusBar;
  cbRoomSurface := CheckBoxArea.Checked;
  bRoomVolume := CheckBoxVolume.Checked;
  bMagneticGrid := CheckMagnetism.Checked;

  Str(TsketchLevel(Project.sketchObject.Levels[Project.sketchObject.CurrentLevel]).Height: 4: 2, TempString);
  EditlevelHeight.Text := TempString;
  HelpList.FileLoad(rtBitmap, i3dDataPath + FileMouse, $FF00FF);

  Recto.left := 3;
  Recto.Top := 3;
  Recto.Right := 20;
  Recto.Bottom := 20;
  RadioGroupDetail.ItemIndex := 0;
  RadioGroupLight.ItemIndex := 1;
  RadioGroupMovingMode.ItemIndex := 3;
end;

// ***********************************************************
// this procedure happens when the user press a mouse button
// on the sketching tool
// It will align the mouse position to the grid if necessary,
// it will scale the position too, then it call the action
// procedure
// ***********************************************************

procedure TFormSketch.ImageSketchMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: integer);
begin
  If Button = mbMiddle then
  Begin
    AncienneAction := CurrentAction;
    CurrentAction := 10;
  End;

  ZoningDone := true;
  Project.sketchObject.PhotoProject(Undo, IndexUndo);
  if IndexUndo < 8 then
    TStringList(Undo[IndexUndo + 2]).Clear;

  Undolastcommand1.Enabled := true;
  ToolButtonUndo.Enabled := true;
  ToolButtonUndo.down := true;
  ToolButtonUndo.down := False;
  Redo1.Enabled := False;
  ToolButtonRedo.Enabled := False;

  MouseDownPos.X := X;
  MouseDownPos.Y := Y;

  // align to grid (out of 'Delete tool')
  if (bMagneticGrid) and (CurrentAction <> 2) and (CurrentAction <> 8) and (CurrentAction <> 9) and (CurrentAction <> 6) and (CurrentAction <> 11) and
    (CurrentAction <> 26) and (CurrentAction <> 12) and (CurrentAction <> 7) and (CurrentAction <> 15) and (CurrentAction <> 16) and (CurrentAction <> 280) then
  begin
    MouseDownPos.X := Round(MouseDownPos.X / GridStep) * GridStep;
    MouseDownPos.Y := Round(MouseDownPos.Y / GridStep) * GridStep;
  end;

  // Scaling
  MouseDownPos.X := MouseDownPos.X * Project.sketchObject.zoom;
  MouseDownPos.Y := MouseDownPos.Y * Project.sketchObject.zoom;

  // Store state of the mouse
  ShiftKey := Shift;
  MouseButton := Button;
  MouseState := 1;
  ManageAction;
  dxMouseDown := true;
end;

procedure TFormSketch.ImageSketchMouseLeave(Sender: TObject);
begin
  dxMouseDown := false;
end;

// ***********************************************************
// this procedure happens when the user release a mouse button
// on the sketching tool
// It will align the mouse position to the grid if necessary,
// it will scale the position too, then it call the action
// procedure
// ***********************************************************

procedure TFormSketch.ImageSketchMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: integer);
begin
  if not(dxMouseDown) then
    exit;
  dxMouseDown := false;
  If Button = mbMiddle then
  Begin
    CurrentAction := AncienneAction;
    MouseState := 0;
    Exit;
  End;
  ZoningDone := False;
  MouseUpPos.X := X;
  MouseUpPos.Y := Y;

  // align to grid (out of 'Delete tool')
  if (bMagneticGrid) and (CurrentAction <> 20) and (CurrentAction <> 21) and (CurrentAction <> 22) and (CurrentAction <> 8) and (CurrentAction <> 9)
    and (CurrentAction <> 11) and (CurrentAction <> 19) and (CurrentAction <> 26) and (CurrentAction <> 12) and (CurrentAction <> 7) and (CurrentAction <> 16)
    and (CurrentAction <> 280) and not((CurrentAction = 2) and (Button = mbright)) then
  begin
    MouseUpPos.X := Round(MouseUpPos.X / GridStep) * GridStep;
    MouseUpPos.Y := Round(MouseUpPos.Y / GridStep) * GridStep;

  end;

  // Scaling
  MouseUpPos.X := MouseUpPos.X * Project.sketchObject.zoom;
  MouseUpPos.Y := MouseUpPos.Y * Project.sketchObject.zoom;

  // Store state of the mouse
  ShiftKey := Shift;
  MouseButton := Button;
  MouseState := 0;

  ManageAction;
end;

// ***********************************************************
// This procedure call the Action method of the sketch object.
// then, it will enable/disable the 'transform tool' the
// selection is not empty. At the end it will refresh the sketch
// ***********************************************************

procedure TFormSketch.ManageAction;
begin
  Project.sketchObject.Action(MouseDownPos, MouseUpPos, MouseButton, MouseState, CurrentAction, ShiftKey);
  // Enable/Disable transform tool button
  BitBtnTransform.Enabled := (Project.sketchObject.Selection.Count <> 0);
end;

// ***********************************************************
// This procedure is called when the user click on the
// 'Line/Point' tool
// It will set the current action, set the correct cursor
// and then initialize the tool
// ***********************************************************

procedure TFormSketch.SpeedButtonLinePointClick(Sender: TObject);
begin
  CurrentAction := 1;
  ImageSketch.Cursor := CrCross;
  initTool;
  DisplayHelp(CurrentAction, FormHelp.ImageHelp.Canvas);
  HelpDisplayed := true;
end;

// ***********************************************************
// This procedure is called when the user click on the
// 'Move/Rotate' tool
// It will set the current action, set the correct cursor
// and then initialize the tool
// ***********************************************************

procedure TFormSketch.SpeedButtonMoveRotateClick(Sender: TObject);
begin
  CurrentAction := 2;
  ImageSketch.Cursor := CrCross;
  initTool;
  DisplayHelp(CurrentAction, FormHelp.ImageHelp.Canvas);
  HelpDisplayed := true;
end;

// ***********************************************************
// This procedure is called when the user click on the
// 'Continuous line' tool
// It will set the current action, set the correct cursor
// and then initialize the tool
// ***********************************************************

procedure TFormSketch.SpeedButtonContinuousLineClick(Sender: TObject);
begin
  CurrentAction := 3;
  ImageSketch.Cursor := CrCross;
  initTool;
  DisplayHelp(CurrentAction, FormHelp.ImageHelp.Canvas);
  HelpDisplayed := true;
end;

// ***********************************************************
// This procedure is called when the user click on the
// 'Rectangle' tool
// It will set the current action, set the correct cursor
// and then initialize the tool
// ***********************************************************

procedure TFormSketch.SpeedButtonRectangleClick(Sender: TObject);
begin
  CurrentAction := 4;
  ImageSketch.Cursor := CrCross;
  initTool;
  DisplayHelp(CurrentAction, FormHelp.ImageHelp.Canvas);
  HelpDisplayed := true;
end;

// ***********************************************************
// This procedure is called when the user click on the
// 'Eraser' tool
// It will set the current action, set the correct cursor
// and then initialize the tool
// ***********************************************************

procedure TFormSketch.SpeedButtonClearClick(Sender: TObject);
begin
  CurrentAction := 5;
  ImageSketch.Cursor := crEraserCursor;
  initTool;
  DisplayHelp(CurrentAction, FormHelp.ImageHelp.Canvas);
  HelpDisplayed := true;
end;

// ***********************************************************
// This procedure is called when the user click on the
// 'Move/Project' tool
// It will set the current action, set the correct cursor
// and then initialize the tool
// ***********************************************************

procedure TFormSketch.SpeedButtonMoveProjectClick(Sender: TObject);
begin
  CurrentAction := 6;
  ImageSketch.Cursor := CrCross;
  initTool;
  DisplayHelp(CurrentAction, FormHelp.ImageHelp.Canvas);
  HelpDisplayed := true;
end;

// ***********************************************************
// This procedure is called when the user click on the
// 'Center' tool
// It will set the current action, set the correct cursor
// and then initialize the tool
// ***********************************************************

procedure TFormSketch.SpeedButtonCenterClick(Sender: TObject);
begin
  CurrentAction := 10;
  ImageSketch.Cursor := CrSizeAll;
  initTool;
  DisplayHelp(CurrentAction, FormHelp.ImageHelp.Canvas);
  HelpDisplayed := true;
end;

// ***********************************************************
// This procedure is called when the user close the window
// It will release the memory.
// ***********************************************************

procedure TFormSketch.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  try
    Project.Free;
  finally
    Project := nil;
  end;
end;

procedure TFormSketch.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  //code de sortie :
  //0 si on a enregistr� des modifications
  //1 si on a rien enregistr�
  canClose := true;
  if not(dataSaved) then
  begin
    if MessageDLG('La saisie graphique n''a pas �t� enregistr�e, �tes vous s�r de vouloir quitter l''�diteur graphique?', mtconfirmation, mbYesNo, 0, mbYes) = mrNo then
    begin
      CanClose := false;
    end
    else
    begin
      exitCode := 1;
    end;
  end
  else
  begin
    exitCode := 0;
  end;
end;

// ***********************************************************
// This procedure is called when the user move the mouse
// over the sketch.
// It will align the mouse position to grid if needed,
// if will scale the mouse position, then call the
// MouseMove method of the sketchObject
// ***********************************************************

procedure TFormSketch.ImageSketchMouseMove(Sender: TObject; Shift: TShiftState; X, Y: integer);
begin
  if HelpDisplayed and AutoShowHide then
    FormHelp.hide;
  if (project = nil) or (project.sketchObject = nil) then
    exit;
  // Align to Grid
  if bMagneticGrid and (CurrentAction <> 5) and (CurrentAction <> 12) and (CurrentAction <> 26) and (CurrentAction <> 19) and (CurrentAction <> 22)
    and (CurrentAction <> 20) and (CurrentAction <> 21) and not((CurrentAction = 2) and (MouseState = 1) and (MouseButton = mbright)) and not
    (CurrentAction = 7) and not(CurrentAction = 8) and not(CurrentAction = 9) and not(CurrentAction = 16) and not(CurrentAction = 11) and not
    ((CurrentAction = 6) and (MouseState = 1) and (MouseButton = mbright)) and not(CurrentAction = 25) and (CurrentAction <> 280) then
  begin
    X := Round(X / GridStep) * GridStep;
    Y := Round(Y / GridStep) * GridStep;
  end;
  // Scale
  X := X * Project.sketchObject.zoom;
  Y := Y * Project.sketchObject.zoom;
  StatusBar.Panels[1].Text := IntToStr(X - Project.sketchObject.Center.X) + ' / ' + IntToStr(Y - Project.sketchObject.Center.Y);
  Project.sketchObject.MoveMouse(X, Y, MouseButton, MouseState, CurrentAction);
end;

// ***********************************************************
// This procedure is called when the user click on the
// no grid button.
// ***********************************************************

procedure TFormSketch.RadioButtonNoGrid2Click(Sender: TObject);
begin
  Project.sketchObject.GridType := 0;
  Project.sketchObject.Refresh();
end;

// ***********************************************************
// This procedure is called when the user click on the
// Grid 1 button.
// ***********************************************************

procedure TFormSketch.RadioButtonGrid12Click(Sender: TObject);
begin
  Project.sketchObject.GridType := 1;
  Project.sketchObject.Refresh();
end;

// ***********************************************************
// This procedure is called when the user click on the
// no grid2 button.
// ***********************************************************

procedure TFormSketch.RadioButtongrid22Click(Sender: TObject);
begin
  Project.sketchObject.GridType := 2;
  Project.sketchObject.Refresh();
end;

// ***********************************************************
// This procedure is called when the user click on the
// zoom in button
// ***********************************************************

procedure TFormSketch.SpeedZoomInClick(Sender: TObject);
var
  NewCenterX, NewCenterY: integer;
begin
  if Project.sketchObject.zoom <> 1 then
  begin
    Project.sketchObject.zoom := Project.sketchObject.zoom div 2;
    NewCenterX := (-ImageSketch.Width div GridStep) * GridStep * Project.sketchObject.zoom div 2;
    NewCenterY := (-ImageSketch.Height div GridStep) * GridStep * Project.sketchObject.zoom div 2;
    Project.sketchObject.MoveCenter(NewCenterX, NewCenterY);

    NewCenterX := Project.sketchObject.Center.X;
    NewCenterY := Project.sketchObject.Center.Y;
    NewCenterX := Round((NewCenterX div Project.sketchObject.zoom) / GridStep) * GridStep * Project.sketchObject.zoom;
    NewCenterY := Round((NewCenterY div Project.sketchObject.zoom) / GridStep) * GridStep * Project.sketchObject.zoom;
    Project.sketchObject.Center.X := NewCenterX;
    Project.sketchObject.Center.Y := NewCenterY;
    // refresh the screen
    Project.sketchObject.Refresh();
  end;
end;

// ***********************************************************
// This procedure is called when the user click on the
// zoom ou button
// ***********************************************************

procedure TFormSketch.SpeedZoomOutClick(Sender: TObject);
var
  NewCenterX, NewCenterY: integer;
begin
  if Project.sketchObject.zoom > 524288 then
    Exit;
  // Set the new zoom coefficient
  Project.sketchObject.zoom := Project.sketchObject.zoom * 2;
  // Move the center to keep it at the good place
  NewCenterX := (ImageSketch.Width div GridStep * GridStep) * Project.sketchObject.zoom div 4;
  NewCenterY := (ImageSketch.Height div GridStep * GridStep) * Project.sketchObject.zoom div 4;
  Project.sketchObject.MoveCenter(NewCenterX, NewCenterY);

  NewCenterX := Project.sketchObject.Center.X;
  NewCenterY := Project.sketchObject.Center.Y;
  NewCenterX := Round((NewCenterX div Project.sketchObject.zoom) / GridStep) * GridStep * Project.sketchObject.zoom;
  NewCenterY := Round((NewCenterY div Project.sketchObject.zoom) / GridStep) * GridStep * Project.sketchObject.zoom;
  Project.sketchObject.Center.X := NewCenterX;
  Project.sketchObject.Center.Y := NewCenterY;
  // refresh the screen
  Project.sketchObject.Refresh();
end;

// ***********************************************************
// This procedure is called when the user resize the window
// It will resize the bitmap of the sketch and refresh it
// ***********************************************************

procedure TFormSketch.FormResize(Sender: TObject);
begin
  ImageSketch.Picture.Bitmap.Width := ImageSketch.Width;
  ImageSketch.Picture.Bitmap.Height := ImageSketch.Height;
  if (Project <> nil) and (Project.sketchObject <> nil) then
    Project.sketchObject.Refresh();
  DXDraw.SurfaceHeight := DXDraw.Height;
  DXDraw.SurfaceWidth := DXDraw.Width;
  DXDraw.Initialize;
  DXTimerTimer(Sender, 0);
end;

// ***********************************************************
// This procedure is called when the user click on the
// menu 'Save project as...'
// ***********************************************************

procedure TFormSketch.Saveproject2Click(Sender: TObject);
begin
  // Preset the filename to the last filename
  SaveDialogProject.FileName := Project.LastFileName;
  // ask the file name
  if SaveDialogProject.Execute then
  // save Sketch
  begin
    if FileExists(SaveDialogProject.FileName) then
    begin
      FileSetAttr(PChar(SaveDialogProject.FileName), FileGetAttr(PChar(SaveDialogProject.FileName)) and not faReadOnly);
    end;
    Project.SaveToFile(SaveDialogProject.FileName);
  end
end;

// ***********************************************************
// This procedure is called when the user click on the
// menu or the tool button 'Save project'
// ***********************************************************

procedure TFormSketch.Saveproject1Click(Sender: TObject);
begin
  // if a previous filename exists : Save
  if FileExists(Project.LastFileName) then
    FileSetAttr(PChar(Project.LastFileName), FileGetAttr(PChar(Project.LastFileName)) and not faReadOnly);
  if FileExists(Project.LastFileName) then
    Project.SaveToFile(Project.LastFileName)
    // else call the 'Save project as...' procedure
  else
    Saveproject2Click(Sender);
end;

// ***********************************************************
// This procedure is called when the user click on the
// menu or the tool button 'New project'
// It will allow to save the current project before to clear
// it.
// ***********************************************************

procedure TFormSketch.Newproject1Click(Sender: TObject);
begin
  // is the project previously not saved ?
  { if not Project.Saved then
    begin
    // Yes : Ask for user if he want to save, to cancel or to continu
    Reponse := MessageDLG(Labels[1], mtWarning, [mbYes, mbNo, mbCancel], 0);
    if reponse = mrYes then
    Saveproject2Click(Sender)
    else if Reponse = mrCancel then
    Exit;
    end; }
  if MessageDLG('Etes-vous s�r de vouloir effacer le projet en cours de saisie graphique?', mtconfirmation, mbYesNo, 0) = mrYes then
  Begin
    // release the memory object
    freeAndNil(Project);
    // Create a new one
    Project := TProject.Create;
    project.sketchObject.imageUi := ImageSketch;
    freeAndNil(buildingData);
    buildingData := TBuildingData.Create;
    // refresh the sketch
    Project.sketchObject.Refresh();
    RafraichisInterface;
  End;
end;

// ***********************************************************
// This procedure is called when the user click on the
// menu or the tool button 'Open project'
// It will create a new project, ask the file name and then
// open the project.
// ***********************************************************

procedure TFormSketch.RafraichisInterface;
var
  TempString, Chaine: string;
  i: integer;
begin
  CheckBoxLength.Checked := Project.sketchObject.DisplayLength;
  Project.sketchObject.CurrentLevel := 0;
  LabelCurrentLevel.Caption := Labels[2] + ' ' + IntToStr(Project.sketchObject.CurrentLevel);
  Str(TsketchLevel(Project.sketchObject.Levels[Project.sketchObject.CurrentLevel]).Height: 4: 2, TempString);
  EditlevelHeight.Text := TempString;
  UpDownLevel.position := Project.sketchObject.CurrentLevel;
  if buildingData.CompoExt <> nil then
    StaticCompoExt.Caption := buildingData.CompoExt.nom
  else
    StaticCompoExt.Caption := '';
  if buildingData.CompoInt <> nil then
    StaticCompoInt.Caption := buildingData.CompoInt.nom
  else
    StaticCompoInt.Caption := '';
  if buildingData.CompoFloor <> nil then
    StaticCompofloor.Caption := buildingData.CompoFloor.nom
  else
    StaticCompofloor.Caption := '';
  if buildingData.CompoIntFloor <> nil then
    StaticCompoIntFloor.Caption := buildingData.CompoIntFloor.nom
  else
    StaticCompoIntFloor.Caption := '';
  if buildingData.CompoRoof <> nil then
    StaticCompoRoof.Caption := buildingData.CompoRoof.nom
  else
    StaticCompoRoof.Caption := '';
  if buildingData.CompoWindow <> nil then
    StaticCompoWindow.Caption := buildingData.CompoWindow.nom
  else
    StaticCompoWindow.Caption := '';
  if buildingData.CompoDoor <> nil then
    StaticCompoDoor.Caption := buildingData.CompoDoor.nom
  else
    StaticCompoDoor.Caption := '';
  Str(buildingData.WindowWidth: 0: 2, Chaine);
  EditWindowW.Text := Chaine;
  Str(buildingData.WindowHeight: 0: 2, Chaine);
  EditWindowH.Text := Chaine;
  Str(buildingData.DoorWidth: 0: 2, Chaine);
  EditDoorW.Text := Chaine;
  Str(buildingData.DoorHeight: 0: 2, Chaine);
  EditDoorH.Text := Chaine;
  CheckCrawlSpace.Checked := buildingData.CrawlSpace;
  CheckLoft.Checked := buildingData.VentilatedLoft;
  Str(Project.sketchObject.Lattitude: 4: 2, Chaine);
  for i := 1 to Length(Chaine) do
    if Chaine[i] = '.' then
      Chaine[i] := ',';
end;

procedure TFormSketch.Open1Click(Sender: TObject);
var
  // TempString,Chaine : String;
  i: integer;
  MemoAuto: boolean;
begin
  // Call the 'New project' procedure
  SpeedButtonMoveRotate.down := true;
  MemoAuto := AutoShowHide;
  AutoShowHide := False;
  SpeedButtonMoveRotateClick(Sender);
  AutoShowHide := MemoAuto;
  Newproject1Click(Sender);
  // Ask file name
  OpenDialogProject.FileName := '';
  if OpenDialogProject.Execute then
  begin
    // open it
    Project.loadFromFile(OpenDialogProject.FileName);
    // refresh the sketch
    RafraichisInterface;
    ZoningDone := False;
    for i := 0 to Project.sketchObject.Levels.Count - 1 do
      TsketchLevel(Project.sketchObject.Levels[i]).Analyze;
    Project.saved := true;
  end;
end;

// ***********************************************************
// This procedure is called when the user click on the
// 'Transform' tool button.
// It will display the menu containing the differents tools
// ***********************************************************

procedure TFormSketch.BitBtnTransformClick(Sender: TObject);
var
  P: TPoint;
begin
  P := BitBtnTransform.ClientToScreen(Point(BitBtnTransform.Width, 0));
  PopupMenuTools.Popup(P.X, P.Y);
  DisplayHelp(CurrentAction, FormHelp.ImageHelp.Canvas);
  HelpDisplayed := true;
end;

// ***********************************************************
// This procedure is called when the user click on the
// 'Rotate +90�' menu item
// ***********************************************************

procedure TFormSketch.Rotate901Click(Sender: TObject);
begin
  Project.sketchObject.PhotoProject(Undo, IndexUndo);
  Project.sketchObject.Rotation(90);
  TsketchLevel(Project.sketchObject.Levels[Project.sketchObject.CurrentLevel]).Analyze;
end;

// ***********************************************************
// This procedure is called when the user click on the
// 'Rotate -90�' menu item
// ***********************************************************

procedure TFormSketch.Rotate902Click(Sender: TObject);
begin
  Project.sketchObject.PhotoProject(Undo, IndexUndo);
  Project.sketchObject.Rotation(-90);
  TsketchLevel(Project.sketchObject.Levels[Project.sketchObject.CurrentLevel]).Analyze;
end;

// ***********************************************************
// This procedure is called when the user click on the
// 'Rotate +180�' menu item
// ***********************************************************

procedure TFormSketch.Rotate1801Click(Sender: TObject);
begin
  Project.sketchObject.PhotoProject(Undo, IndexUndo);
  Project.sketchObject.Rotation(180);
  TsketchLevel(Project.sketchObject.Levels[Project.sketchObject.CurrentLevel]).Analyze;
end;

// ***********************************************************
// This procedure is called when the user click on the
// 'Align to Grid' menu item
// ***********************************************************

procedure TFormSketch.AligntoGrid1Click(Sender: TObject);
begin
  Project.sketchObject.PhotoProject(Undo, IndexUndo);
  Project.sketchObject.AlignSelection;
  TsketchLevel(Project.sketchObject.Levels[Project.sketchObject.CurrentLevel]).Analyze;
end;

// ***********************************************************
// This procedure is called when the user click on the
// 'Flip horizontal' menu item
// ***********************************************************

procedure TFormSketch.Horizonsymetry1Click(Sender: TObject);
begin
  Project.sketchObject.PhotoProject(Undo, IndexUndo);
  Project.sketchObject.FlipHorizontal;
  TsketchLevel(Project.sketchObject.Levels[Project.sketchObject.CurrentLevel]).Analyze;
end;

// ***********************************************************
// This procedure is called when the user click on the
// 'Flip Vertical' menu item
// ***********************************************************

procedure TFormSketch.Flipvertical1Click(Sender: TObject);
begin
  Project.sketchObject.PhotoProject(Undo, IndexUndo);
  Project.sketchObject.Flipvertical;
  TsketchLevel(Project.sketchObject.Levels[Project.sketchObject.CurrentLevel]).Analyze;
end;

// ***********************************************************
// This procedure is called when the user click on the
// 'Display length of the walls' checkbox.
// ***********************************************************

procedure TFormSketch.CheckBoxLength2Click(Sender: TObject);
begin
  Project.sketchObject.DisplayLength := CheckBoxLength.Checked;
  Project.sketchObject.Refresh();
end;

// ***********************************************************
// This procedure is called when the user click on the
// the arrows to change the current level
// ***********************************************************

procedure TFormSketch.UpDownLevelClick(Sender: TObject; Button: TUDBtnType);
var
  TempString: string;
begin
  // Increase or decrease the current level
  if Button = btNext then
  begin
    if TsketchLevel(Project.sketchObject.Levels[Project.sketchObject.CurrentLevel]).points.Count = 0 then
      exit;
    Project.sketchObject.NextLevel
  end
  else
    Project.sketchObject.PreviousLevel;
  if not TsketchLevel(Project.sketchObject.Levels[Project.sketchObject.CurrentLevel]).Toiture and (CurrentAction = 25) then
    CurrentAction := 12;
  // display the number of the current level
  LabelCurrentLevel.Caption := Labels[2] + ' ' + IntToStr(Project.sketchObject.CurrentLevel);
  Str(TsketchLevel(Project.sketchObject.Levels[Project.sketchObject.CurrentLevel]).Height: 4: 2, TempString);
  EditlevelHeight.Text := TempString;
  // Refresh the sketch
  CheckBoxToit.Checked := TsketchLevel(Project.sketchObject.Levels[Project.sketchObject.CurrentLevel]).Toiture;
  TsketchLevel(Project.sketchObject.Levels[Project.sketchObject.CurrentLevel]).Analyze;
end;

// ***********************************************************
// This procedure undo the last command
// ***********************************************************

procedure TFormSketch.Undolastcommand1Click(Sender: TObject);
var
  TempString: string;
  i, Index: integer;
  Sketcho: TsketchObject;
  currentLevel : integer;
begin
  ModeUndo := true;
  if IndexUndo <> -1 then
  begin
    currentLevel := Project.sketchObject.CurrentLevel;
    Project.sketchObject.PhotoProject(Undo, IndexUndo);
    Sketcho := Project.sketchObject;
    Project.sketchObject := TsketchObject.Create;
    Project.sketchObject.imageUi := imageSketch;
    dec(IndexUndo);
    Index := 0;
    Project.sketchObject.LoadFromList(Undo[IndexUndo], Index);
    for i := 0 to Sketcho.Levels.Count - 1 do
    begin
      if i < Project.sketchObject.Levels.Count then
      begin
        TsketchLevel(Project.sketchObject.Levels[i]).BackGroundPicture.free();
        TsketchLevel(Project.sketchObject.Levels[i]).BackGroundPicture := TsketchLevel(Sketcho.Levels[i]).BackGroundPicture;
        TsketchLevel(Sketcho.Levels[i]).BackGroundPicture := nil;
        TsketchLevel(Project.sketchObject.Levels[i]).decX := TsketchLevel(Sketcho.Levels[i]).decX;
        TsketchLevel(Project.sketchObject.Levels[i]).decY := TsketchLevel(Sketcho.Levels[i]).decY;
      end;
    end;
    dec(IndexUndo);
    Sketcho.Free;
    Project.sketchObject.Refresh();
    Redo1.Enabled := true;
    ToolButtonRedo.Enabled := true;
    ToolButtonRedo.down := true;
    ToolButtonRedo.down := False;
    if IndexUndo = -1 then
    begin
      Undolastcommand1.Enabled := False;
      ToolButtonUndo.Enabled := False;
    end;
    if currentLevel >= Project.sketchObject.Levels.Count then
      Project.sketchObject.CurrentLevel := Project.sketchObject.Levels.Count - 1
    else
      Project.sketchObject.CurrentLevel := currentLevel;

    UpDownLevel.position := Project.sketchObject.CurrentLevel;
    LabelCurrentLevel.Caption := Labels[2] + ' ' + IntToStr(Project.sketchObject.CurrentLevel);
    Str(TsketchLevel(Project.sketchObject.Levels[Project.sketchObject.CurrentLevel]).Height: 4: 2, TempString);
    EditlevelHeight.Text := TempString;
    for i := 0 to Project.sketchObject.Levels.Count - 1 do
      TsketchLevel(Project.sketchObject.Levels[i]).Analyze;
  end;
  ModeUndo := False;
end;

procedure TFormSketch.Redo1Click(Sender: TObject);
var
  TempString: string;
  i, Index: integer;
  Sketcho: TsketchObject;
  currentLevel : integer;
begin
  ModeUndo := true;
  if (IndexUndo < MaxUndo - 1) and (TStringList(Undo[IndexUndo + 2]).Count <> 0) then
  begin
    currentLevel := Project.sketchObject.CurrentLevel;
    Sketcho := Project.sketchObject;
    Project.sketchObject := TsketchObject.Create;
    Project.sketchObject.imageUi := imageSketch;
    inc(IndexUndo);
    Index := 0;
    Project.sketchObject.LoadFromList(Undo[IndexUndo + 1], Index);
    for i := 0 to Sketcho.Levels.Count - 1 do
    begin
      if i < Project.sketchObject.Levels.Count then
      begin
        TsketchLevel(Project.sketchObject.Levels[i]).BackGroundPicture.free();
        TsketchLevel(Project.sketchObject.Levels[i]).BackGroundPicture := TsketchLevel(Sketcho.Levels[i]).BackGroundPicture;
        TsketchLevel(Sketcho.Levels[i]).BackGroundPicture := nil;

        TsketchLevel(Project.sketchObject.Levels[i]).decX := TsketchLevel(Sketcho.Levels[i]).decX;
        TsketchLevel(Project.sketchObject.Levels[i]).decY := TsketchLevel(Sketcho.Levels[i]).decY;
      end;
    end;
    Sketcho.Free;
    Project.sketchObject.Refresh();
    Undolastcommand1.Enabled := true;
    ToolButtonUndo.Enabled := true;
    ToolButtonUndo.down := true;
    ToolButtonUndo.down := False;
    if (IndexUndo = MaxUndo - 1) then
    begin
      Redo1.Enabled := False;
      ToolButtonRedo.Enabled := False;
    end
    else if (IndexUndo < (MaxUndo - 1)) and (TStringList(Undo[IndexUndo + 2]).Count = 0) then
    begin
      Redo1.Enabled := False;
      ToolButtonRedo.Enabled := False;
    end;
    if currentLevel >= Project.sketchObject.Levels.Count then
      Project.sketchObject.CurrentLevel := Project.sketchObject.Levels.Count - 1
    else
      Project.sketchObject.CurrentLevel := currentLevel;

    UpDownLevel.position := Project.sketchObject.CurrentLevel;
    LabelCurrentLevel.Caption := Labels[2] + ' ' + IntToStr(Project.sketchObject.CurrentLevel);
    Str(TsketchLevel(Project.sketchObject.Levels[Project.sketchObject.CurrentLevel]).Height: 4: 2, TempString);
    EditlevelHeight.Text := TempString;
    for i := 0 to Project.sketchObject.Levels.Count - 1 do
      TsketchLevel(Project.sketchObject.Levels[i]).Analyze;
  end;
  ModeUndo := False;
end;

// ***********************************************************
// This procedure call the copy method
// ***********************************************************

procedure TFormSketch.Copy1Click(Sender: TObject);
begin
  Project.sketchObject.CopySelection;
end;

// ***********************************************************
// This procedure call the Cut method
// ***********************************************************

procedure TFormSketch.Cut1Click(Sender: TObject);
begin
  Project.sketchObject.CutSelection;
  TsketchLevel(Project.sketchObject.Levels[Project.sketchObject.CurrentLevel]).Analyze;
end;

// ***********************************************************
// This procedure call the Clear method
// ***********************************************************

procedure TFormSketch.Clear1Click(Sender: TObject);
begin
  Project.sketchObject.ClearSelection;
  TsketchLevel(Project.sketchObject.Levels[Project.sketchObject.CurrentLevel]).Analyze;
end;

// ***********************************************************
// This procedure call the Paste method
// ***********************************************************

procedure TFormSketch.Paste1Click(Sender: TObject);
begin
  Project.sketchObject.PasteSelection;
  TsketchLevel(Project.sketchObject.Levels[Project.sketchObject.CurrentLevel]).Analyze;
end;

procedure TFormSketch.SelectAll1Click(Sender: TObject);
begin
  if (CurrentAction <> 2) and (CurrentAction <> 6) then
  begin
    SpeedButtonMoveRotate.down := true;
    SpeedButtonMoveRotateClick(Sender);
  end;
  Project.sketchObject.SelectAll(ImageSketch.Picture.Bitmap);
  BitBtnTransform.Enabled := true;
end;

procedure TFormSketch.Timer1Timer(Sender: TObject);
begin
  if (mouse.CursorPos.X = OldCursorPos.X) and (mouse.CursorPos.Y = OldCursorPos.Y) and not ZoningDone then
  begin
    TsketchLevel(Project.sketchObject.Levels[Project.sketchObject.CurrentLevel]).Analyze;
    ZoningDone := true;
  end;
  OldCursorPos := mouse.CursorPos;
end;

procedure TFormSketch.SpeedWallClick(Sender: TObject);
begin
  if CheckBoxToit.Checked then
  begin
    CurrentAction := 25;
    ImageSketch.Cursor := crHandPoint;
    initTool;
    DisplayHelp(CurrentAction, FormHelp.ImageHelp.Canvas);
    HelpDisplayed := true;
    Project.sketchObject.InfoCleared := true;
  end
  else
  begin
    CurrentAction := 12;
    ImageSketch.Cursor := crHandPoint;
    initTool;
    DisplayHelp(CurrentAction, FormHelp.ImageHelp.Canvas);
    HelpDisplayed := true;
    Project.sketchObject.InfoCleared := true;
  end;
end;

procedure TFormSketch.Quit1Click(Sender: TObject);
begin
  Close();
end;

procedure TFormSketch.EditlevelHeightChange(Sender: TObject);
var
  Value: Double;
  Error: integer;
begin
  Val(EditlevelHeight.Text, Value, Error);
  if Error = 0 then
  begin
    TsketchLevel(Project.sketchObject.Levels[Project.sketchObject.CurrentLevel]).Height := Value;
    TsketchLevel(Project.sketchObject.Levels[Project.sketchObject.CurrentLevel]).Analyze;
    Project.sketchObject.Refresh();
    Project.sketchObject.Modified := true;
  end;
end;

procedure TFormSketch.StatusBarDrawPanel(StatusBar: TStatusBar; Panel: TStatusPanel; const Rect: TRect);
begin
  StatusBar.Canvas.Font.Color := clmaroon;
  StatusBar.Canvas.TextOut(Rect.left + 2, Rect.Top + 1, StatusBar.Panels[1].Text);
end;

procedure TFormSketch.SpeedButtonCurveClick(Sender: TObject);
begin
  CurrentAction := 15;
  ImageSketch.Cursor := CrCross;
  initTool;
  DisplayHelp(CurrentAction, FormHelp.ImageHelp.Canvas);
  HelpDisplayed := true;
end;

procedure TFormSketch.Duplicatecurrentlevel1Click(Sender: TObject);
var
  List: TStringList;
  Index, Index2: integer;
  Ok: boolean;
  Reponse: Word;
  Level: TsketchLevel;
begin
  Index := 0;
  repeat
    Ok := (Project.sketchObject.Levels.Count <= Index);
    if not Ok then
      Ok := (TsketchLevel(Project.sketchObject.Levels[Index]).points.Count = 0);
    inc(index);
  until Ok;
  Reponse := MessageDLG(Labels[95] + IntToStr(Index - 1) + Labels[96], mtconfirmation, mbOkCancel, 0);
  if Reponse = mrCancel then
    Exit;

  List := TStringList.Create;
  TsketchLevel(Project.sketchObject.Levels[Project.sketchObject.CurrentLevel]).SaveToList(List);
  Index2 := 0;
  if ((Index - 1) >= Project.sketchObject.Levels.Count) then
  begin
    Level := TsketchLevel.Create(Project.sketchObject);
    Project.sketchObject.Levels.Add(Level);
  end
  else
    Level := Project.sketchObject.Levels[Project.sketchObject.Levels.Count - 1];

  Level.LoadFromList(List, Index2);
  Level.Analyze;
  List.Free;
end;

procedure TFormSketch.Deletecurrentlevel1Click(Sender: TObject);
var
  Reponse: integer;
begin
  Reponse := MessageDLG(Labels[97], mtconfirmation, mbOkCancel, 0);
  if Reponse = mrCancel then
    Exit;
  Project.sketchObject.Levels.Delete(Project.sketchObject.CurrentLevel);
  if Project.sketchObject.Levels.Count = 0 then
    Project.sketchObject.Levels.Add(TsketchLevel.Create(Project.sketchObject));
  UpDownLevelClick(nil, btPrev);
end;

procedure TFormSketch.Assistant1Click(Sender: TObject);
begin
  if FormHelp.Visible then
    FormHelp.hide
  else
    FormHelp.Show;
end;

procedure TFormSketch.Defineroomasshading1Click(Sender: TObject);
begin
  CurrentAction := 7;
  ImageSketch.Cursor := crHandPoint;
  initTool;
  DisplayHelp(CurrentAction, FormHelp.ImageHelp.Canvas);
  HelpDisplayed := true;
  SpeedZone.down := true;
end;

procedure TFormSketch.Defineroomasabuferspace1Click(Sender: TObject);
begin
  CurrentAction := 8;
  ImageSketch.Cursor := crHandPoint;
  initTool;
  DisplayHelp(CurrentAction, FormHelp.ImageHelp.Canvas);
  HelpDisplayed := true;
  SpeedZone.down := true;
end;

procedure TFormSketch.Defineroomasrooflightened1Click(Sender: TObject);
begin
  CurrentAction := 9;
  ImageSketch.Cursor := crHandPoint;
  initTool;
  DisplayHelp(CurrentAction, FormHelp.ImageHelp.Canvas);
  SpeedZone.down := true;
end;

procedure TFormSketch.Defineroomasexternal1Click(Sender: TObject);
begin
  CurrentAction := 16;
  ImageSketch.Cursor := crHandPoint;
  initTool;
  DisplayHelp(CurrentAction, FormHelp.ImageHelp.Canvas);
  HelpDisplayed := true;
  SpeedZone.down := true;
end;

procedure TFormSketch.Undefineroom1Click(Sender: TObject);
begin
  CurrentAction := 11;
  ImageSketch.Cursor := crHandPoint;
  initTool;
  DisplayHelp(CurrentAction, FormHelp.ImageHelp.Canvas);
  HelpDisplayed := true;
  SpeedZone.down := true;
end;

procedure TFormSketch.SpeedZoneClick(Sender: TObject);
begin
  CurrentAction := 18;
  ImageSketch.Cursor := crHandPoint;
  initTool;
  DisplayHelp(CurrentAction, FormHelp.ImageHelp.Canvas);
  HelpDisplayed := true;
  SpeedZone.down := true;
end;

procedure TFormSketch.FormShow(Sender: TObject);
var
  i: integer;
begin
  // loading some constants
  ZoneOrientation[-2] := Labels[121];
  for i := -1 to 12 do
    ZoneOrientation[i] := Labels[i + 60];
  for i := -1 to 12 do
    ZoneShortOrientation[i] := Labels[i + 181];
  ZoneShortOrientation[1] := Labels[231];
  Editadoororawindow1Click(nil);
  Definecharacteristicsoftheroom1Click(nil);
  SpeedButtonLinePoint.down := true;
  SpeedButtonLinePointClick(nil);
  DisplayHelp(20, FormHelp.ImageHelp.Canvas);

  saveFileName := '';
  try
    if TApp.launchedByAssociation then
      saveFileName := TApp.launchedFile;
    if (saveFileName <> '') then
    Begin
      if FileExists(saveFileName) then
      begin
        Project.loadFromFile(saveFileName);
        // refresh the sketch
        RafraichisInterface;
        ZoningDone := False;
        if Project.sketchObject.Levels.Count = 0 then
        begin
          PageControl1.ActivePage := TabSheetDonnees;
        end
        else
        begin
          for i := 0 to Project.sketchObject.Levels.Count - 1 do
            TsketchLevel(Project.sketchObject.Levels[i]).Analyze;
          PageControl1.ActivePage := TabSheetSketch;
        end;
        PageControl1Change(nil);
        Project.saved := true;
      end
      else
      begin
        MessageDLG('Impossible de trouver le fichier "' + saveFileName + '"', mtWarning, [mbOk], 0);
      end;
    end
    else
    begin
      RafraichisInterface;
      ZoningDone := False;
      for i := 0 to Project.sketchObject.Levels.Count - 1 do
        TsketchLevel(Project.sketchObject.Levels[i]).Analyze;
      Project.saved := true;
    end;

  Except
    on e : Exception do
      MessageDLG('Fichier projet corrompu : ' + e.Message, mtError, [mbOk], 0);
  end;
end;

procedure TFormSketch.PageControl1Change(Sender: TObject);
Var
  c: TD3DColor;
  isSketchActive : boolean;
begin
  isSketchActive := PageControl1.ActivePage = TabSheetSketch;
  Printcompletesketch1.Enabled := isSketchActive;
  Printcurrentlevelofthesketch1.Enabled := isSketchActive;
  SAvecurrentlevelasmetafile1.Enabled := isSketchActive;
  Undolastcommand1.Enabled := isSketchActive;
  Redo1.Enabled := isSketchActive;
  Copy1.Enabled := isSketchActive;
  Cut1.Enabled := isSketchActive;
  Paste1.Enabled := isSketchActive;
  Clear1.Enabled := isSketchActive;
  SelectAll1.Enabled := isSketchActive;

  ToolButtonUndo.Enabled := isSketchActive;
  ToolButtonRedo.Enabled := isSketchActive;
  ToolButtonCut.Enabled := isSketchActive;
  ToolButtonCopy.Enabled := isSketchActive;
  ToolButtonPaste.Enabled := isSketchActive;
  ToolButtonClear.Enabled := isSketchActive;
  ToolSelectAll.Enabled := isSketchActive;

  Duplicatecurrentlevel1.Enabled := isSketchActive;
  Deletecurrentlevel1.Enabled := isSketchActive;

  if isSketchActive then
  begin
    DisplayHelp(21, FormHelp.ImageHelp.Canvas);
    FormResize(nil);
  end
  else if PageControl1.ActivePage = TabSheet3D then
  begin
    c := ColorToRGB(ColorDialog1.Color);
    c := RGB_MAKE(RGB_GETBLUE(c), RGB_GETGREEN(c), RGB_GETRED(c));
    DXDraw.Color := c;
    if RadioGroupLight.ItemIndex = -1 then
      RadioGroupLight.ItemIndex := 1;
    if RadioGroupDetail.ItemIndex = -1 then
      RadioGroupDetail.ItemIndex := 1;
    if RadioGroupMovingMode.ItemIndex = -1 then
      RadioGroupMovingMode.ItemIndex := 3;
    TrackBarLevel.position := 1;
    TrackBarLevel.Min := 1;
    TrackBarLevel.Max := Project.sketchObject.Levels.Count + 1;
    if TsketchLevel(Project.sketchObject.Levels[TrackBarLevel.Max - 2]).points.Count = 0 then
      TrackBarLevel.Max := TrackBarLevel.Max - 1;
    TrackBarLevel.position := 1;
    DXTimerTimer(Sender, 0);
    Timer2.Enabled := true;
    MakeBatiment;
    BitBtn14Click(Nil);
    RadioGroupLightClick(nil);
  end;
end;

procedure TFormSketch.BitBtnNext1Click(Sender: TObject);
begin
  PageControl1.ActivePage := TabSheetSketch;
  PageControl1Change(nil);
end;


class function TFormSketch.getAppVersionLabel: string;
begin
  result := 'DialogIE : �diteur 3D v' + TApp.version;
end;

procedure TFormSketch.Printcurrentlevelofthesketch1Click(Sender: TObject);
begin
  PrintDialog1.Options := [poPrintToFile];
  PrintDialog1.FromPage := 1;
  PrintDialog1.MinPage := 1;
  PrintDialog1.ToPage := 1;
  PrintDialog1.MaxPage := 1;

  if PrintDialog1.Execute then
  begin
    Project.PrintCurrentLevel;
  end;
end;

procedure TFormSketch.Printcompletesketch1Click(Sender: TObject);
begin
  PrintDialog1.Options := [poPrintToFile];
  PrintDialog1.FromPage := 1;
  PrintDialog1.MinPage := 1;
  PrintDialog1.ToPage := 1;
  PrintDialog1.MaxPage := 1;

  if PrintDialog1.Execute then
  begin
    Project.print;
  end;
end;

procedure TFormSketch.CheckBoxAreaClick(Sender: TObject);
begin
  Project.sketchObject.Refresh();
end;

procedure TFormSketch.SAvecurrentlevelasmetafile1Click(Sender: TObject);
begin
  if FormSketch.SaveDialogMeta.Execute then
  begin
    Project.saveCurrentLevel(FormSketch.SaveDialogMeta.FileName);
  end;
end;

procedure TFormSketch.Importpicture1Click(Sender: TObject);
var
  X, Y, r, g, b: integer;
  TempPicture: TPicture;
  P: PByteArray;
begin
  if OpenPictureDialog1.Execute then
  begin
    Project.CurrentLevel.NomPicture := OpenPictureDialog1.FileName;
    TempPicture := TPicture.Create;
    TempPicture.loadFromFile(OpenPictureDialog1.FileName);
    if not(TempPicture.graphic is TBitmap) then
    begin
      Project.CurrentLevel.BackGroundPicture.Bitmap.Width := TempPicture.Width;
      Project.CurrentLevel.BackGroundPicture.Bitmap.Height := TempPicture.Height;
      Project.CurrentLevel.BackGroundPicture.Bitmap.Canvas.Draw(0, 0, TempPicture.graphic);
    end
    else
      Project.CurrentLevel.BackGroundPicture := TempPicture;
    Project.CurrentLevel.BackGroundPicture.Bitmap.PixelFormat := pf24Bit;
    for Y := 0 to Project.CurrentLevel.BackGroundPicture.Height - 1 do
    begin
      P := Project.CurrentLevel.BackGroundPicture.Bitmap.ScanLine[Y];
      for X := 0 to Project.CurrentLevel.BackGroundPicture.Width - 1 do
      begin
        r := P[X * 3];
        g := P[X * 3 + 1];
        b := P[X * 3 + 2];
        r := 255 - ((255 - r) div 2);
        g := 255 - ((255 - g) div 2);
        b := 255 - ((255 - b) div 2);
        P[X * 3] := r;
        P[X * 3 + 1] := g;
        P[X * 3 + 2] := b;
      end;
    end;
    Project.sketchObject.Refresh();
  end;
end;

procedure TFormSketch.Deletebackgroundpicture1Click(Sender: TObject);
begin
  Project.CurrentLevel.BackGroundPicture.Free;
  Project.CurrentLevel.NomPicture := '';
  Project.CurrentLevel.BackGroundPicture := TPicture.Create;
  Project.sketchObject.Refresh();
end;

procedure TFormSketch.Userrotate1Click(Sender: TObject);
var
  NewString: string;
  Reel: Double;
  Erreur: integer;
begin
  Project.sketchObject.PhotoProject(Undo, IndexUndo);
  NewString := '90';
  if InputQuery('Rotation', 'Angle (en degr�s)', NewString) then
  begin
    Val(NewString, Reel, Erreur);
    if Erreur = 0 then
      Project.sketchObject.Rotation(Reel)
    else
      MessageDLG(Labels[256], mtwarning, [mbOk], 0);
  end;
  TsketchLevel(Project.sketchObject.Levels[Project.sketchObject.CurrentLevel]).Analyze;
end;

procedure TFormSketch.BitBtn1Click(Sender: TObject);
begin
  FormCompo.chooseCompo(buildingData.CompoExt, 'Paroi ext�rieure par d�faut');
  if buildingData.CompoExt <> nil then
  begin
    StaticCompoExt.Caption := buildingData.CompoExt.nom;
  end
  else
  begin
    StaticCompoExt.Caption := '';
  end
end;

procedure TFormSketch.BitBtn2Click(Sender: TObject);
begin
  formCompo.chooseCompo(buildingData.CompoFloor, 'Plancher bas par d�faut');
  if buildingData.CompoFloor <> nil then
  begin
    StaticCompofloor.Caption := buildingData.CompoFloor.nom;
  end
  else
  begin
    StaticCompofloor.Caption := '';
  end
end;

procedure TFormSketch.BitBtn3Click(Sender: TObject);
begin
  formCompo.chooseCompo(buildingData.CompoIntFloor, 'Plancher interm�diaire par d�faut');
  if buildingData.CompoIntFloor <> nil then
  begin
    StaticCompoIntFloor.Caption := buildingData.CompoIntFloor.nom;
  end
  else
  begin
    StaticCompoIntFloor.Caption := '';
  end
end;

procedure TFormSketch.BitBtn4Click(Sender: TObject);
begin
  formCompo.chooseCompo(buildingData.CompoInt, 'Paroi int�rieur par d�faut');
  if buildingData.CompoInt <> nil then
  begin
    StaticCompoInt.Caption := buildingData.CompoInt.nom;
  end
  else
  begin
    StaticCompoInt.Caption := '';
  end
end;

procedure TFormSketch.BitBtn5Click(Sender: TObject);
begin
  formCompo.chooseCompo(buildingData.CompoRoof, 'Plafond toiture par d�faut');
  if buildingData.CompoRoof <> nil then
  begin
    StaticCompoRoof.Caption := buildingData.CompoRoof.nom;
  end
  else
  begin
    StaticCompoRoof.Caption := '';
  end
end;

procedure TFormSketch.BitBtn6Click(Sender: TObject);
begin
  formFen.chooseFen(buildingData.CompoWindow, 'Fen�tre par d�faut');
  if buildingData.CompoWindow <> nil then
  begin
    StaticCompoWindow.Caption := buildingData.CompoWindow.nom;
  end
  else
  begin
    StaticCompoWindow.Caption := '';
  end
end;

procedure TFormSketch.BitBtn7Click(Sender: TObject);
begin
  formFen.chooseFen(buildingData.CompoDoor, 'Porte par d�faut');
  if buildingData.CompoDoor <> nil then
  begin
    StaticCompoDoor.Caption := buildingData.CompoDoor.nom;
  end
  else
  begin
    StaticCompoDoor.Caption := '';
  end
end;

procedure TFormSketch.BitBtn8Click(Sender: TObject);
begin
  dataSaved :=exportData();
  close();
end;

procedure TFormSketch.Definecharacteristicsoftheroom1Click(Sender: TObject);
begin
  CurrentAction := 18;
  ImageSketch.Cursor := crHandPoint;
  initTool;
  DisplayHelp(CurrentAction, FormHelp.ImageHelp.Canvas);
  HelpDisplayed := true;
  SpeedZone.down := true;
end;

procedure TFormSketch.CheckBoxZoning2Click(Sender: TObject);
begin
  Project.sketchObject.Refresh();
end;

procedure TFormSketch.CheckCrawlSpaceClick(Sender: TObject);
begin
  buildingData.CrawlSpace := CheckCrawlSpace.Checked;
end;

procedure TFormSketch.CheckLoftClick(Sender: TObject);
begin
  buildingData.VentilatedLoft := CheckLoft.Checked;
end;

procedure TFormSketch.EditWindowWChange(Sender: TObject);
var
  Erreur: integer;
begin
  Val(EditWindowW.Text, buildingData.WindowWidth, Erreur);
end;

procedure TFormSketch.EditWindowHChange(Sender: TObject);
var
  Erreur: integer;
begin
  Val(EditWindowH.Text, buildingData.WindowHeight, Erreur);
end;

procedure TFormSketch.EditDoorWChange(Sender: TObject);
var
  Erreur: integer;
begin
  Val(EditDoorW.Text, buildingData.DoorWidth, Erreur);
end;

procedure TFormSketch.EditDoorHChange(Sender: TObject);
var
  Erreur: integer;
begin
  Val(EditDoorH.Text, buildingData.DoorHeight, Erreur);
end;

procedure TFormSketch.ApplicationEvents1Exception(Sender: TObject; E: Exception);
begin
  ShowMessage(Sender.ClassName + ' � provoqu� l''erreur suivante : ' + E.message);
end;

procedure TFormSketch.SpeedPorteClick(Sender: TObject);
var
  P: TPoint;
begin
  P := SpeedPorte.ClientToScreen(Point(SpeedPorte.Width, 0));
  PopupFenetre.Popup(P.X, P.Y);

  DisplayHelp(CurrentAction, FormHelp.ImageHelp.Canvas);
  HelpDisplayed := true;
end;

procedure TFormSketch.insertadoor1Click(Sender: TObject);
begin
  CurrentAction := 19;
  ImageSketch.Cursor := CrCross;
  initTool;
  DisplayHelp(CurrentAction, FormHelp.ImageHelp.Canvas);
  HelpDisplayed := true;
  Project.sketchObject.InfoCleared := true;
  if buildingData.CompoDoor <> nil then
    FormPorte.StaticCompoDoor.Caption := Labels[234]
  else
    FormPorte.StaticCompoDoor.Caption := '';
  FormPorte.EditDoorW.Text := EditDoorW.Text;
  FormPorte.EditDoorH.Text := EditDoorH.Text;
  FormPorte.CompoPorte := nil;
  FormPorte.EditDoorHChange(nil);
  FormPorte.BitBtnMove.Visible := False;
  FormPorte.ShowModal;
end;

procedure TFormSketch.insertawindow1Click(Sender: TObject);
begin
  CurrentAction := 20;
  ImageSketch.Cursor := CrCross;
  initTool;
  DisplayHelp(CurrentAction, FormHelp.ImageHelp.Canvas);
  HelpDisplayed := true;
  Project.sketchObject.InfoCleared := true;
  if buildingData.CompoWindow <> nil then
    FormFenetre.StaticCompoFen.Caption := Labels[234]
  else
    FormFenetre.StaticCompoFen.Caption := '';
  FormFenetre.EditFenW.Text := EditWindowW.Text;
  FormFenetre.EditFenH.Text := EditWindowH.Text;
  FormFenetre.EditFenAllege.Text := '1.0';
  FormFenetre.EditRetrait.Text := '0.0';
  FormFenetre.CompoFenetre := nil;
  FormFenetre.EditFenHChange(nil);
  FormFenetre.BitBtnMove.Visible := False;
  FormFenetre.ShowModal;
end;

procedure TFormSketch.Editadoororawindow1Click(Sender: TObject);
begin
  CurrentAction := 21;
  ImageSketch.Cursor := CrCross;
  initTool;
  DisplayHelp(CurrentAction, FormHelp.ImageHelp.Canvas);
  HelpDisplayed := true;
end;

procedure TFormSketch.Deleteadoorwindow1Click(Sender: TObject);
begin
  CurrentAction := 22;
  ImageSketch.Cursor := CrCross;
  initTool;
  DisplayHelp(CurrentAction, FormHelp.ImageHelp.Canvas);
  HelpDisplayed := true;
end;


procedure TFormSketch.Definethenorthposition1Click(Sender: TObject);
begin
  FormNorth.SpinEdit1.Value := Round(Project.sketchObject.OrientationNord);
  if FormNorth.ShowModal = mrOk then
    Project.sketchObject.OrientationNord := FormNorth.SpinEdit1.Value;
  Project.sketchObject.Refresh();
end;

procedure TFormSketch.Movebackgroundpicture1Click(Sender: TObject);
begin
  CurrentAction := 280;
  ImageSketch.Cursor := crHandPoint;
  initTool;
  DisplayHelp(CurrentAction, FormHelp.ImageHelp.Canvas);
  HelpDisplayed := true;
end;

procedure TFormSketch.DXDrawInitialize(Sender: TObject);
var
  i: integer;
begin
  if RadioGroupDetail.ItemIndex = 0 then
    for i := Low(FTexture) to High(FTexture) do
      FTexture[i] := TDirect3DTexture2.Create(DXDraw, DXImageList.Items[i].Picture.graphic, False)
    else
      for i := Low(FTexture) to High(FTexture) do
        FTexture[i] := TDirect3DTexture2.Create(DXDraw, DXImageList1.Items[i].Picture.graphic, False);
end;

procedure TFormSketch.DXDrawFinalize(Sender: TObject);
var
  i: integer;
begin
  for i := Low(FTexture) to High(FTexture) do
  begin
    FTexture[i].Free;
    FTexture[i] := nil;
  end;
end;


procedure TFormSketch.DXDrawInitializeSurface(Sender: TObject);
var
  vp: TD3DViewport7;
  mtrl: TD3DMaterial7;
  matProj: TD3DMatrix;
  _lit: TD3DLight7;
begin
  { Viewport }
  FillChar(vp, sizeof(vp), 0);
  vp.dwX := 0;
  vp.dwY := 0;
  vp.dwWidth := DXDraw.SurfaceWidth;
  vp.dwHeight := DXDraw.SurfaceHeight;
  vp.dvMinZ := 0.0;
  vp.dvMaxZ := 1.0;
  DXDraw.D3DDevice7.SetViewport(vp);

  { Material }
  FillChar(mtrl, sizeof(mtrl), 0);
  mtrl.ambient.r := 0.3;
  mtrl.ambient.g := 0.3;
  mtrl.ambient.b := 0.3;
  mtrl.diffuse.r := 1.0;
  mtrl.diffuse.g := 1.0;
  mtrl.diffuse.b := 1.0;
  mtrl.specular.r := 0.1;
  mtrl.specular.g := 0.1;
  mtrl.specular.b := 0.1;
  mtrl.power := 1;
  if RadioGroupLight.ItemIndex = 0 then
    with _lit do
    begin
      dltType := D3DLIGHT_DIRECTIONAL;
      dcvDiffuse.r := 0.4;
      dcvDiffuse.g := 0.4;
      dcvDiffuse.b := 0.4;
      dvDirection := MakeD3DVECTOR(-1.0, -1.0, 0.0);
      dvDirection := VectorNormalize(dvDirection);
      dvFalloff := 0; // Falloff
      dvAttenuation0 := 0; // Constant attenuation
      dvAttenuation1 := 0.05; // Linear attenuation
      dvAttenuation2 := 0; // Quadratic attenuation}
    end
    else
      with _lit do
      begin
        dltType := D3DLIGHT_POINT;
        dcvDiffuse.a := 1.5;
        dcvDiffuse.r := 1.5;
        dcvDiffuse.g := 1.5;
        dcvDiffuse.b := 1.5;
        dvPosition := MakeD3DVECTOR(CentreX, 30, CentreY);
        dvRange := 1000.0;
        dvFalloff := 0; // Falloff
        dvAttenuation0 := 0; // Constant attenuation
        dvAttenuation1 := 0.05; // Linear attenuation
        dvAttenuation2 := 0; // Quadratic attenuation}
      end;
  DXDraw.D3DDevice7.SetLight(0, _lit);
  DXDraw.D3DDevice7.LightEnable(0, true);

  DXDraw.D3DDevice7.SetMaterial(mtrl);
  DXDraw.D3DDevice7.SetRenderState(D3DRENDERSTATE_AMBIENT, $FFFFFFFF);
  DXDraw.D3DDevice7.SetRenderState(D3DRENDERSTATE_DITHERENABLE, 1);
  DXDraw.D3DDevice7.SetRenderState(D3DRENDERSTATE_SPECULARENABLE, 0);
  DXDraw.D3DDevice7.SetRenderState(D3DRENDERSTATE_Lighting, 1);
  DXDraw.D3DDevice7.SetRenderState(D3DRENDERSTATE_SHADEMODE, Ord(D3DSHADE_GOURAUD));
  DXDraw.D3DDevice7.SetRenderState(D3DRENDERSTATE_TEXTUREPERSPECTIVE, 1);
  DXDraw.D3DDevice7.SetRenderState(D3DRENDERSTATE_FILLMODE, Ord(D3DFILL_SOLID));
  DXDraw.D3DDevice7.SetRenderState(D3DRENDERSTATE_CullMode, Ord(D3DCULL_CW));
  FillChar(matProj, sizeof(matProj), 0);
  matProj._11 := 2.0;
  matProj._22 := 2.0;
  matProj._33 := 1.0;
  matProj._34 := 1.0;
  matProj._43 := -1.0;
  D3DUtil_SetProjectionMatrix(matProj, pi / 4, 1, 1, 1000);
  DXDraw.D3DDevice7.SetTransform(D3DTRANSFORMSTATE_PROJECTION, matProj);

  DXDraw.D3DDevice7.SetTextureStageState(0, D3DTSS_MAGFILTER, Ord(D3DFILTER_LINEAR));
  DXDraw.D3DDevice7.SetTextureStageState(0, D3DTSS_MINFILTER, Ord(D3DFILTER_LINEAR));
end;

procedure TFormSketch.FrameMovie(Time: Double);
var
  matView: TD3DMatrix;
  vFrom, vAt, vWorldUp: TD3DVector;
begin
  // Set the view matrix so that the camera is backed out along the z-axis,
  // and looks down on the cube (rotating along the x-axis by -0.5 radians).
  FillChar(matView, sizeof(matView), 0);
  vWorldUp := MakeD3DVECTOR(0, 1, 0);
  vFrom := MakeD3DVECTOR(PositionCamX, PositionCamY, PositionCamZ);
  vAt := MakeD3DVECTOR(vFrom.X + Sin(OrientationCam) * Cos(InclinaisonCam), vFrom.Y + Sin(InclinaisonCam), vFrom.z + Cos(OrientationCam) * Cos(InclinaisonCam));
  D3DUtil_SetViewMatrix(matView, vFrom, vAt, vWorldUp);
  DXDraw.D3DDevice7.SetTransform(D3DTRANSFORMSTATE_VIEW, matView);
end;

procedure TFormSketch.DXTimerTimer(Sender: TObject; LagCount: integer);
var
  r: TD3DRect;
  i: integer;
  mtrl: TD3DMaterial7;
  P: T3DParoi;
begin
  FillChar(mtrl, sizeof(mtrl), 0);
  mtrl.ambient.r := 0.3;
  mtrl.ambient.g := 0.3;
  mtrl.ambient.b := 0.3;
  mtrl.diffuse.r := 1.0;
  mtrl.diffuse.g := 1.0;
  mtrl.diffuse.b := 1.0;
  mtrl.specular.r := 0.1;
  mtrl.specular.g := 0.1;
  mtrl.specular.b := 0.1;
  mtrl.power := 1;
  DXDraw.D3DDevice7.SetMaterial(mtrl);

  if not DXDraw.CanDraw then
    Exit;

  { Frame Movie }
  FrameMovie(GetTickCount / 1000);

  { Clear Screen }
  r.x1 := 0;
  r.y1 := 0;
  r.x2 := DXDraw.SurfaceWidth;
  r.y2 := DXDraw.SurfaceHeight;
  if DXDraw.ZBuffer <> nil then
    DXDraw.D3DDevice7.Clear(1, @r, D3DCLEAR_TARGET or D3DCLEAR_ZBUFFER, DXDraw.Color, 1, 0)
  else
    DXDraw.D3DDevice7.Clear(1, @r, D3DCLEAR_TARGET, $00FF00, 1, 0);
  asm
     FINIT
  end;
  DXDraw.D3DDevice7.BeginScene;
  DXDraw.D3DDevice7.SetTexture(0, nil);

  for i := 0 to Liste3DParoi.Count - 1 do
  begin
    P := T3DParoi(Liste3DParoi[i]);
    DXDraw.D3DDevice7.SetRenderState(D3DRENDERSTATE_ALPHABLENDENABLE, 0);

    if (RadioGroupDetail.ItemIndex = 0) then
    Begin
      mtrl.ambient.r := 0.3333333333;
      mtrl.ambient.g := 0.3333333333;
      mtrl.ambient.b := 0.3333333333;
      // if p.NTexture = 0 then P.NTexture := 5;
      DXDraw.D3DDevice7.SetTexture(0, FTexture[P.NTexture].Surface.IDDSurface7);
      if P.NTexture = 2 then
      Begin
        mtrl.ambient.r := 0.4 / 3;
        mtrl.ambient.g := 0.8 / 3;
        mtrl.ambient.b := 0.2 / 3;
      End;
    End
    else
    begin
      case P.NTexture of
        0:
          begin
            mtrl.ambient.b := $AC / 255 / 3;
            mtrl.ambient.g := $CD / 255 / 3;
            mtrl.ambient.r := $F2 / 255 / 3;
          end;
        1:
          begin
            mtrl.ambient.b := $84 / 255 / 3;
            mtrl.ambient.g := $C6 / 255 / 3;
            mtrl.ambient.r := $D6 / 255 / 3;
          end;
        2:
          begin
            mtrl.ambient.r := 0.2 / 3;
            mtrl.ambient.g := 0.4 / 3;
            mtrl.ambient.b := 0.1 / 3;
          end;
        3:
          begin
            mtrl.ambient.r := 0.77 / 3;
            mtrl.ambient.g := 0.77 / 3;
            mtrl.ambient.b := 0.77 / 3;
          end;
        4:
          begin
            mtrl.ambient.r := 0 / 3;
            mtrl.ambient.g := 1 / 3;
            mtrl.ambient.b := 1 / 3;
          end;
        5:
          begin
            mtrl.ambient.r := 1 / 3.2;
            mtrl.ambient.g := 1 / 3.2;
            mtrl.ambient.b := 1 / 3.2;
          end;
        6:
          begin
            mtrl.ambient.b := $84 / 255 / 3.2;
            mtrl.ambient.g := $C6 / 255 / 3.2;
            mtrl.ambient.r := $D6 / 255 / 3.2;
          end;
        7:
          begin
            mtrl.ambient.r := 0.8 / 3;
            mtrl.ambient.g := 0.6 / 3;
            mtrl.ambient.b := 0.2 / 3;
          end;
      end;
    end;
    mtrl.diffuse.r := mtrl.ambient.r * 3;
    mtrl.diffuse.g := mtrl.ambient.g * 3;
    mtrl.diffuse.b := mtrl.ambient.b * 3;
    DXDraw.D3DDevice7.SetMaterial(mtrl);
    if Not P.liste then
      DXDraw.D3DDevice7.DrawPrimitive(D3DPT_TRIANGLESTRIP, D3DFVF_VERTEX, P.P[0], P.NbPoints, 0)
    else
      DXDraw.D3DDevice7.DrawPrimitive(D3DPT_TRIANGLELIST, D3DFVF_VERTEX, P.P[0], P.NbPoints, 0);
    if P.Double then
    Begin
      if (RadioGroupDetail.ItemIndex = 0) then
      Begin
        mtrl.ambient.r := 0.3333333333;
        mtrl.ambient.g := 0.3333333333;
        mtrl.ambient.b := 0.3333333333;
        // if p.NTexture = 0 then P.NTexture := 5;
        DXDraw.D3DDevice7.SetTexture(0, FTexture[P.NTexture2].Surface.IDDSurface7);
        if P.NTexture2 = 2 then
        Begin
          mtrl.ambient.r := 0.1 / 3;
          mtrl.ambient.g := 0.2 / 3;
          mtrl.ambient.b := 0.05 / 3;
        End;
      End
      else
      begin
        case P.NTexture2 of
          0:
            begin
              mtrl.ambient.b := $AC / 255 / 3;
              mtrl.ambient.g := $CD / 255 / 3;
              mtrl.ambient.r := $F2 / 255 / 3;
            end;
          1:
            begin
              mtrl.ambient.r := 0.8 / 3;
              mtrl.ambient.g := 0.6 / 3;
              mtrl.ambient.b := 0.2 / 3;
            end;
          2:
            begin
              mtrl.ambient.r := 0.1 / 3;
              mtrl.ambient.g := 0.2 / 3;
              mtrl.ambient.b := 0.05 / 3;
            end;
          3:
            begin
              mtrl.ambient.r := 0.77 / 3;
              mtrl.ambient.g := 0.77 / 3;
              mtrl.ambient.b := 0.77 / 3;
            end;
          4:
            begin
              mtrl.ambient.r := 0 / 3;
              mtrl.ambient.g := 1 / 3;
              mtrl.ambient.b := 1 / 3;
            end;
          5:
            begin
              mtrl.ambient.r := 1 / 3.2;
              mtrl.ambient.g := 1 / 3.2;
              mtrl.ambient.b := 1 / 3.2;
            end;
          6:
            begin
              mtrl.ambient.b := $84 / 255 / 3.2;
              mtrl.ambient.g := $C6 / 255 / 3.2;
              mtrl.ambient.r := $D6 / 255 / 3.2;
            end;
          7:
            begin
              mtrl.ambient.r := 0.51;
              mtrl.ambient.g := 0.2588;
              mtrl.ambient.b := 0.0;
            end;
        end;
      end;
      mtrl.diffuse.r := mtrl.ambient.r * 3;
      mtrl.diffuse.g := mtrl.ambient.g * 3;
      mtrl.diffuse.b := mtrl.ambient.b * 3;
      DXDraw.D3DDevice7.SetMaterial(mtrl);
      DXDraw.D3DDevice7.SetRenderState(D3DRENDERSTATE_CullMode, Ord(D3DCULL_CCW));
      if Not P.liste then
        DXDraw.D3DDevice7.DrawPrimitive(D3DPT_TRIANGLESTRIP, D3DFVF_VERTEX, P.P[0], P.NbPoints, 0)
      else
        DXDraw.D3DDevice7.DrawPrimitive(D3DPT_TRIANGLELIST, D3DFVF_VERTEX, P.P[0], P.NbPoints, 0);
      DXDraw.D3DDevice7.SetRenderState(D3DRENDERSTATE_CullMode, Ord(D3DCULL_CW));
    end;
  end;
  DXDraw.D3DDevice7.EndScene;
  asm
     FINIT
  end;
  DXDraw.Flip;
end;

procedure TFormSketch.PageControl1Changing(Sender: TObject; var AllowChange: boolean);
begin
  if PageControl1.ActivePage = TabSheet3D then
  begin
    DXTimer.Enabled := False;
    Timer2.Enabled := False;
  end
end;

procedure TFormSketch.DXDrawMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: integer);
begin
  ActiveMouvement1 := (Button = mbleft);
  DXTimer.Enabled := true;
end;

procedure TFormSketch.DXDrawMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: integer);
begin
  ActiveMouvement1 := False;
  DXTimer.Enabled := False;
end;

procedure TFormSketch.Timer2Timer(Sender: TObject);
var
  P: TPoint;
  ecX, ecY: Double;
  distance : Double;
  d2 : Double;
  d1 : Double;
  Orientation : Double;
  AngleAlpha: Double;
  AncienSin, NouveauSin: Double;
begin
  P := DXDraw.ScreenToClient(mouse.CursorPos);
  ecX := (P.X - PointPrec.X) * 20;
  ecY := (P.Y - PointPrec.Y) * 20;
  PointPrec := P;
  // ecX := p.X - (DXDraw.Width / 2);
  // ecY := p.Y - (DXDraw.Height / 2);
  if ActiveMouvement1 and (RadioGroupMovingMode.ItemIndex = 0) then
  begin
    // OrientationCam := OrientationCam + ecX / DXDraw.Width * pi / 30;
    OrientationCam := OrientationCam + ecX * pi / 25000;
    PositionCamX := PositionCamX + Sin(OrientationCam) * -ecY / 300;
    PositionCamZ := PositionCamZ + Cos(OrientationCam) * -ecY / 300;
  end
  else if ActiveMouvement1 and (RadioGroupMovingMode.ItemIndex = 1) then
  begin
    OrientationCam := OrientationCam - (ecX) / DXDraw.Width * pi / 80;
    InclinaisonCam := InclinaisonCam + (ecY) / DXDraw.Height * pi / 80;
  end
  else if ActiveMouvement1 and (RadioGroupMovingMode.ItemIndex = 2) then
  begin
    PositionCamZ := PositionCamZ + Sin(OrientationCam) * +ecX / DXDraw.Width * 2;
    PositionCamX := PositionCamX + Cos(OrientationCam) * -ecX / DXDraw.Width * 2;
    PositionCamY := PositionCamY + ecY / DXDraw.Height * 2;
  end
  else if ActiveMouvement1 and (RadioGroupMovingMode.ItemIndex = 4) then
  begin
    PositionCamX := PositionCamX + Sin(OrientationCam) * Cos(InclinaisonCam) * -ecY / 200;
    PositionCamZ := PositionCamZ + Cos(OrientationCam) * Cos(InclinaisonCam) * -ecY / 200;
    PositionCamY := PositionCamY + Sin(InclinaisonCam) * -ecY / 200
  end
  else if ActiveMouvement1 and (RadioGroupMovingMode.ItemIndex = 3) then
  begin
    distance := Sqrt(Sqr(CentreX - PositionCamX) + Sqr(CentreY - PositionCamZ));
    d2 := (CentreY - PositionCamZ) / distance;
    d1 := (PositionCamX - CentreX) / distance;
    Orientation := arcCos(d1);
    if d2 < 0 then
      Orientation := -Orientation;
    Orientation := Orientation + ecX / DXDraw.Width / 6.25;
    d1 := Cos(-Orientation) * distance;
    d2 := Sin(-Orientation) * distance;
    PositionCamX := CentreX + d1;
    PositionCamZ := CentreY + d2;

    distance := Sqrt(Sqr(CentreX - PositionCamX) + Sqr(CentreY - PositionCamZ) + Sqr(0 - PositionCamY));
    AngleAlpha := arcCos(PositionCamY / distance);
    AncienSin := Sin(AngleAlpha);
    AngleAlpha := AngleAlpha - ecY / (DXDraw.Height * 10);
    PositionCamY := Cos(AngleAlpha) * distance;
    NouveauSin := Sin(AngleAlpha);
    PositionCamX := (PositionCamX - CentreX) / AncienSin * NouveauSin + CentreX;
    PositionCamZ := (PositionCamZ - CentreY) / AncienSin * NouveauSin + CentreY;


    // PositionCamY := PositionCamY - ecY / DXDraw.Height*10;
    BitBtn14Click(nil);
  end;
  if (InclinaisonCam < -pi / 2) then
    InclinaisonCam := -pi / 2
  else if (InclinaisonCam > pi / 2) then
    InclinaisonCam := pi / 2
end;


procedure TFormSketch.TrackBar1Change(Sender: TObject);
begin
  ActiveMouvement3 := true;
  Timer2Timer(Sender);
  ActiveMouvement3 := False;
end;

procedure TFormSketch.RadioQuickLightClick(Sender: TObject);
begin
  DXDraw.Initialize;
end;

procedure TFormSketch.Button1Click(Sender: TObject);
var
  ImageToPrint: TBitmap;
  CoeffX, CoeffY: Double;
  Rect: TRect;
  X, Y: integer;
  Couleur: TD3DColor;
  r, g, b: byte;
begin
  if PrintDialog1.Execute then
  Begin
    ImageToPrint := TBitmap.Create;
    ImageToPrint.Width := DXDraw.Width;
    ImageToPrint.Height := DXDraw.Height;
    FormProgress.ProgressBar1.Max := DXDraw.Height;
    FormProgress.ProgressBar1.position := 0;
    FormProgress.Label1.Caption := Labels[248];
    FormProgress.Show;
    FormProgress.Repaint;
    for Y := 0 to DXDraw.Height - 1 do
    Begin
      for X := 0 to DXDraw.Width - 1 do
      begin
        Couleur := DXDraw.Surface.pixels[X, Y];
        b := (Couleur and 31) shl 3;
        Couleur := Couleur shr 5;
        g := (Couleur and 63) shl 2;
        Couleur := Couleur shr 6;
        r := (Couleur and 31) shl 3;
        Couleur := r or (g shl 8) or (b shl 16);
        ImageToPrint.Canvas.pixels[X, Y] := Couleur;
      End;
      // ImageToPrint.canvas.pixels[x, y] := DXDraw.Surface.pixels[x, y];
      FormProgress.ProgressBar1.Stepit
    End;
    FormProgress.hide;
    with Printer do
    begin
      if ImageToPrint.Width > ImageToPrint.Height then
      begin
        Printer.Orientation := poLandscape;
      end
      else
        Printer.Orientation := poPortrait;
      CoeffX := PageWidth / ImageToPrint.Width;
      CoeffY := PageHeight / ImageToPrint.Height;
      if CoeffY < CoeffX then
        CoeffX := CoeffY;
      Printer.Title := '3D';
      Rect.left := 10;
      Rect.Top := 10;
      Rect.Right := Round(ImageToPrint.Width * CoeffX) - 10;
      Rect.Bottom := Round(ImageToPrint.Height * CoeffX) - 10;
      BeginDoc;
      Canvas.StretchDraw(Rect, ImageToPrint);
      EndDoc;
    end;
    ImageToPrint.Free;
  End;
end;

procedure TFormSketch.RadioGroupDetailClick(Sender: TObject);
begin
  DXDrawFinalize(Sender);
  DXDrawInitialize(Sender);
  DXTimerTimer(Sender, 0);
end;

procedure TFormSketch.RadioGroupLightClick(Sender: TObject);
begin
  DXDraw.Initialize;
  DXTimerTimer(Sender, 0);
end;

procedure TFormSketch.TrackBarLevelChange(Sender: TObject);
begin
  MakeBatiment;
  DXTimerTimer(Sender, 0);
end;

procedure TFormSketch.Button2Click(Sender: TObject);
var
  ImageToPrint: TBitmap;
  X, Y: integer;
  Reponse: boolean;
  Couleur: TColor;
  r, g, b: byte;
begin
  Reponse := SaveDialogbmp.Execute;
  if Reponse then
  begin
    ImageToPrint := TBitmap.Create;
    ImageToPrint.Width := DXDraw.Width;
    ImageToPrint.Height := DXDraw.Height;
    ImageToPrint.PixelFormat := pf24Bit;
    // ImageToPrint.Canvas.Draw(0,0,DXDraw.Surface.Canvas);
    FormProgress.ProgressBar1.Max := DXDraw.Height;
    FormProgress.ProgressBar1.position := 0;
    FormProgress.Label1.Caption := Labels[248];
    FormProgress.Show;
    FormProgress.Repaint;
    for Y := 0 to DXDraw.Height - 1 do
    Begin
      for X := 0 to DXDraw.Width - 1 do
      begin
        Couleur := DXDraw.Surface.pixels[X, Y];
        b := (Couleur and 31) shl 3;
        Couleur := Couleur shr 5;
        g := (Couleur and 63) shl 2;
        Couleur := Couleur shr 6;
        r := (Couleur and 31) shl 3;
        Couleur := r or (g shl 8) or (b shl 16);
        ImageToPrint.Canvas.pixels[X, Y] := Couleur;
      end;
      FormProgress.ProgressBar1.Stepit
    End;
    FormProgress.hide;
    ImageToPrint.SaveToFile(SaveDialogbmp.FileName);
    ImageToPrint.Free;
  End;
end;

procedure TFormSketch.buttonQuitClick(Sender: TObject);
begin
  close;
end;

procedure TFormSketch.BitBtn14Click(Sender: TObject);
var
  distance, d2: Double;
begin
  distance := Sqrt(Sqr(PositionCamX - CentreX) + Sqr(PositionCamZ - CentreY));
  distance := PositionCamY / distance;
  InclinaisonCam := -arctan(distance);

  distance := Sqrt(Sqr(PositionCamX - CentreX) + Sqr(PositionCamZ - CentreY));
  d2 := (PositionCamZ - CentreY) / distance;
  distance := (PositionCamX - CentreX) / distance;
  OrientationCam := arcCos(distance);
  if d2 < 0 then
    OrientationCam := -OrientationCam;
  OrientationCam := -OrientationCam - pi / 2;
  DXTimerTimer(Sender, 0);
end;

procedure TFormSketch.CheckBoxToitClick(Sender: TObject);
begin
  CheckRoomName.Checked := not CheckBoxToit.Checked;
  CheckRoomName.Enabled := not CheckBoxToit.Checked;
  CheckBoxArea.Enabled := not CheckBoxToit.Checked;
  cbRoomSurface := not CheckBoxToit.Checked;
  CheckBoxArea.Checked := cbRoomSurface;
  SpeedZone.Enabled := not CheckBoxToit.Checked;
  SpeedPorte.Enabled := not CheckBoxToit.Checked;

  If (Not CheckBoxToit.Checked) and (TsketchLevel(Project.sketchObject.Levels[Project.sketchObject.CurrentLevel]).Toiture = true) then
  Begin
    CheckBoxToit.Checked := true;
    Deletecurrentlevel1Click(Nil);
  End;

  if (TsketchLevel(Project.sketchObject.Levels[Project.sketchObject.CurrentLevel]).Toiture = False) and (CheckBoxToit.Checked) then
  Begin
    if TsketchLevel(Project.sketchObject.Levels[Project.sketchObject.CurrentLevel]).points.Count = 0 then
    Begin
      if Project.sketchObject.CurrentLevel <> 0 then
        CreerLimitesToitures(Project.sketchObject.Levels[Project.sketchObject.CurrentLevel])
      else
      Begin
        Beep;
        CheckBoxToit.Checked := False;
        MessageDLG(Labels[267], mtwarning, [mbOk], 0);
      End
    End
    else
    Begin
      Beep;
      CheckBoxToit.Checked := False;
      MessageDLG(Labels[266], mtwarning, [mbOk], 0);
    End;
  End;
  TsketchLevel(Project.sketchObject.Levels[Project.sketchObject.CurrentLevel]).Toiture := CheckBoxToit.Checked;
end;

procedure TFormSketch.Alignpointsbetweenlevels1Click(Sender: TObject);
begin
  Project.sketchObject.AlignPoints;
  TsketchLevel(Project.sketchObject.Levels[Project.sketchObject.CurrentLevel]).Analyze;
end;

procedure TFormSketch.RadioButtonNoGridClick(Sender: TObject);
begin
  Project.sketchObject.GridType := 0;
  Project.sketchObject.Refresh();
  RadioButtonNoGrid.Checked := true;
  RadioButtonGrid1.Checked := False;
  RadioButtongrid2.Checked := False;
end;

procedure TFormSketch.RadioButtonGrid1Click(Sender: TObject);
begin
  Project.sketchObject.GridType := 1;
  Project.sketchObject.Refresh();
  RadioButtonGrid1.Checked := true;
  RadioButtongrid2.Checked := False;
  RadioButtonNoGrid.Checked := False;
end;

procedure TFormSketch.RadioButtongrid2Click(Sender: TObject);
begin
  Project.sketchObject.GridType := 2;
  Project.sketchObject.Refresh();
  RadioButtongrid2.Checked := true;
  RadioButtonNoGrid.Checked := False;
  RadioButtonGrid1.Checked := False;
end;

procedure TFormSketch.CheckBoxLengthClick(Sender: TObject);
begin
  CheckBoxLength.Checked := not CheckBoxLength.Checked;
  Project.sketchObject.DisplayLength := CheckBoxLength.Checked;
  Project.sketchObject.Refresh();
end;

procedure TFormSketch.CheckRoomNameClick(Sender: TObject);
begin
  CheckRoomName.Checked := not CheckRoomName.Checked;
  Project.sketchObject.Refresh();
end;

procedure TFormSketch.Areaofrooms1Click(Sender: TObject);
begin
  cbRoomSurface := not CheckBoxArea.Checked;
  CheckBoxArea.Checked := cbRoomSurface;
  Project.sketchObject.Refresh();
end;

procedure TFormSketch.CheckMagnetismClick(Sender: TObject);
begin
  bMagneticGrid := not CheckMagnetism.Checked;
  CheckMagnetism.Checked := bMagneticGrid;
end;

procedure TFormSketch.RadioGroupMovingModeClick(Sender: TObject);
begin
  DXTimerTimer(Sender, 0);
end;

procedure TFormSketch.CheckBoxZoneClick(Sender: TObject);
begin
  DXTimerTimer(Sender, 0);
end;

procedure TFormSketch.BitBtn15Click(Sender: TObject);
begin
  RadioGroupMovingMode.ItemIndex := 0;
  PositionCamY := 1.8;
  BitBtn14Click(Sender);
  InclinaisonCam := 0;
  DXTimerTimer(Sender, 0);
end;

procedure TFormSketch.CheckGroundClick(Sender: TObject);
begin
  MakeBatiment;
  DXTimerTimer(Sender, 0);
end;

procedure TFormSketch.BitBtn16Click(Sender: TObject);
Var
  c: TD3DColor;
begin
  if ColorDialog1.Execute then
  Begin
    ShapeColor.Brush.Color := ColorDialog1.Color;
    c := ColorToRGB(ColorDialog1.Color);
    c := RGB_MAKE(RGB_GETBLUE(c), RGB_GETGREEN(c), RGB_GETRED(c));

    DXDraw.Color := c;
    DXTimerTimer(Sender, 0);
  End;

end;


procedure TFormSketch.CheckBoxVolumeClick(Sender: TObject);
begin
  bRoomVolume := not CheckBoxVolume.Checked;
  CheckBoxVolume.Checked := bRoomVolume;
  Project.sketchObject.Refresh();
end;

procedure TFormSketch.Properties1Click(Sender: TObject);
Var
  Chaine, C1, C2: String;
  i, j, k, l, m: integer;
  Total, SousTotal, VTotal, VSousTotal: Double;
  SousTotalParoisExt, SousTotalParoisInt, TotalParoisExt, TotalParoisInt, SousTotalVitrageInt, TotalVitrageInt, SousTotalVitrageExt, TotalVitrageExt,
    SousTotalPorteInt, TotalPOrteInt, SousTotalPorteExt, TotalPorteExt: Double;
  PNord, Psud, PEst, POuest: Double;
  VNord, Vsud, Vest, VOuest: Double;
  TPNord, TPsud, TPEst, TPOuest: Double;
  TVNord, TVsud, TVest, TVOuest: Double;
  Room: TRoom;
  Level: TsketchLevel;
  TotalParoi, EP: Double;
  Seg: LTSegment;
  Comp: TComposition;
  Porte: TPorte;
  Fenetre: TFenetre;
  OriSeg: Double;
begin
  FormProp.RichEdit1.Lines.Clear;
  Total := 0;
  VTotal := 0;
  TotalParoisExt := 0;
  TotalParoisInt := 0;
  TotalVitrageInt := 0;
  TotalVitrageExt := 0;
  TotalPOrteInt := 0;
  TotalPorteExt := 0;
  TPNord := 0;
  TPsud := 0;
  TPEst := 0;
  TPOuest := 0;
  TVNord := 0;
  TVsud := 0;
  TVest := 0;
  TVOuest := 0;
  For i := 0 to Project.sketchObject.Levels.Count - 1 do
  Begin
    PNord := 0;
    Psud := 0;
    PEst := 0;
    POuest := 0;
    VNord := 0;
    Vsud := 0;
    Vest := 0;
    VOuest := 0;
    SousTotal := 0;
    VSousTotal := 0;
    SousTotalParoisExt := 0;
    SousTotalParoisInt := 0;
    SousTotalVitrageInt := 0;
    SousTotalVitrageExt := 0;
    SousTotalPorteInt := 0;
    SousTotalPorteExt := 0;
    Level := TsketchLevel(Project.sketchObject.Levels[i]);
    For j := 0 to Level.Rooms.Count - 1 do
    Begin
      Room := TRoom(Level.Rooms[j]);
      if (Room.RoomType <> roomExternal) and (Room.RoomType <> roomObstruction) then
        SousTotal := SousTotal + Room.Area;
    end;
    TotalParoi := 0;

    For j := 0 to Level.Rooms.Count - 1 do
    Begin
      Room := TRoom(Level.Rooms[j]);
      if (Room.RoomType <> roomExternal) and (Room.RoomType <> roomObstruction) then
      Begin
        For m := 0 to Room.Points.Count - 2 do
        Begin
          Seg := Level.SegmentExists(Room.Points[m], Room.Points[m + 1]);
          If (not Seg.isvirtual) and (not Seg.Perimeter) then
          Begin
            EP := 0;
            if Seg.Composition = Nil then
              Comp := buildingData.CompoInt
            else
              Comp := Seg.Composition;
            if Comp <> Nil then
            Begin
              for k := 1 to Comp.NbMat do
                EP := EP + Comp.Epaisseur[k]
            End
            else
              EP := 0;
            TotalParoi := TotalParoi + (Seg.Size(Project.sketchObject.scale) * EP / 100);
            SousTotalParoisInt := SousTotalParoisInt + Seg.Size(Project.sketchObject.scale) * Level.Height;


            For porte in Seg.ListePortes do
            Begin
              SousTotalParoisInt := SousTotalParoisInt - (Porte.Hauteur * Porte.Largeur);
              SousTotalPorteInt := SousTotalPorteInt + (Porte.Hauteur * Porte.Largeur);
            End;
          End;
          If (not Seg.isvirtual) and Seg.Perimeter then
          Begin
            OriSeg := Seg.Orientation - Project.sketchObject.OrientationNord;
            if OriSeg < 0 then
              OriSeg := OriSeg + 360;
            if OriSeg > 360 then
              OriSeg := OriSeg - 360;

            if (OriSeg >= 45) and (OriSeg < 135) then
            Begin
              PEst := PEst + Seg.Size(Project.sketchObject.scale) * Level.Height;
              TPEst := TPEst + Seg.Size(Project.sketchObject.scale) * Level.Height
            end
            else if (OriSeg >= 135) and (OriSeg < 225) then
            Begin
              Psud := Psud + Seg.Size(Project.sketchObject.scale) * Level.Height;
              TPsud := TPsud + Seg.Size(Project.sketchObject.scale) * Level.Height
            end
            else if (OriSeg >= 225) and (OriSeg < 315) then
            Begin
              POuest := POuest + Seg.Size(Project.sketchObject.scale) * Level.Height;
              TPOuest := TPOuest + Seg.Size(Project.sketchObject.scale) * Level.Height
            end
            else if (OriSeg < 45) or (OriSeg >= 315) then
            Begin
              PNord := PNord + Seg.Size(Project.sketchObject.scale) * Level.Height;
              TPNord := TPNord + Seg.Size(Project.sketchObject.scale) * Level.Height;
            end;

            SousTotalParoisExt := SousTotalParoisExt + Seg.Size(Project.sketchObject.scale) * Level.Height;
            For porte in Seg.ListePortes do
            Begin
              SousTotalParoisExt := SousTotalParoisExt - (Porte.Hauteur * Porte.Largeur);
              SousTotalPorteExt := SousTotalPorteExt + (Porte.Hauteur * Porte.Largeur);
            End;
            For fenetre in Seg.ListeFenetres do
            Begin
              if (OriSeg >= 45) and (OriSeg < 135) then
              Begin
                Vest := Vest + (Fenetre.Hauteur * Fenetre.Largeur);
                TVest := TVest + (Fenetre.Hauteur * Fenetre.Largeur)
              end
              else if (OriSeg >= 135) and (OriSeg < 225) then
              Begin
                Vsud := Vsud + (Fenetre.Hauteur * Fenetre.Largeur);
                TVsud := TVsud + (Fenetre.Hauteur * Fenetre.Largeur)
              end
              else if (OriSeg >= 225) and (OriSeg < 315) then
              Begin
                VOuest := VOuest + (Fenetre.Hauteur * Fenetre.Largeur);
                TVOuest := TVOuest + (Fenetre.Hauteur * Fenetre.Largeur)
              end
              else if (OriSeg < 45) or (OriSeg >= 315) then
              Begin
                VNord := VNord + (Fenetre.Hauteur * Fenetre.Largeur);
                TVNord := TVNord + (Fenetre.Hauteur * Fenetre.Largeur);
              end;

              SousTotalParoisExt := SousTotalParoisExt - (Fenetre.Hauteur * Fenetre.Largeur);
              SousTotalVitrageExt := SousTotalVitrageExt + (Fenetre.Hauteur * Fenetre.Largeur);
            End;
          End;
        end;
      end;
    End;
    SousTotal := SousTotal - TotalParoi;

    FormProp.RichEdit1.Lines.Add('');
    FormProp.RichEdit1.Lines.Add('Niveau ' + IntToStr(i));
    FormProp.RichEdit1.Lines.Add('');

    Str(SousTotal: 0: 2, C1);
    FormProp.RichEdit1.Lines.Add('Surface : ' + C1);
    VSousTotal := SousTotal * Level.Height;
    Str(VSousTotal: 0: 2, C2);
    FormProp.RichEdit1.Lines.Add('Volume : ' + C2);
    Total := Total + SousTotal;
    VTotal := VTotal + VSousTotal;

    Str(SousTotalParoisExt: 0: 2, C1);
    FormProp.RichEdit1.Lines.Add('Surface des parois ext�rieures : ' + C1);
    Str(PNord: 0: 2, C1);
    FormProp.RichEdit1.Lines.Add('   Nord : ' + C1);
    Str(Psud: 0: 2, C1);
    FormProp.RichEdit1.Lines.Add('   Sud : ' + C1);
    Str(PEst: 0: 2, C1);
    FormProp.RichEdit1.Lines.Add('   Est : ' + C1);
    Str(POuest: 0: 2, C1);
    FormProp.RichEdit1.Lines.Add('   Ouest : ' + C1);


    Str(SousTotalParoisInt: 0: 2, C1);
    FormProp.RichEdit1.Lines.Add('Surface des parois int�rieures : ' + C1);
    Str(SousTotalPorteExt: 0: 2, C1);
    FormProp.RichEdit1.Lines.Add('Surface des portes ext�rieures : ' + C1);
    Str(SousTotalPorteInt: 0: 2, C1);
    FormProp.RichEdit1.Lines.Add('Surface des portes int�rieures : ' + C1);
    Str(SousTotalVitrageExt: 0: 2, C1);
    FormProp.RichEdit1.Lines.Add('Surface des vitrages ext�rieurs : ' + C1);
    Str(VNord: 0: 2, C1);
    FormProp.RichEdit1.Lines.Add('   Nord : ' + C1);
    Str(Vsud: 0: 2, C1);
    FormProp.RichEdit1.Lines.Add('   Sud : ' + C1);
    Str(Vest: 0: 2, C1);
    FormProp.RichEdit1.Lines.Add('   Est : ' + C1);
    Str(VOuest: 0: 2, C1);
    FormProp.RichEdit1.Lines.Add('   Ouest : ' + C1);
    // Str(SousTotalVitrageInt:0:2,c1);
    /// FormProp.RichEdit1.Lines.Add('Surface des vitrages int�rieurs : '+c1);

    TotalParoisInt := TotalParoisInt + SousTotalParoisInt;
    TotalParoisExt := TotalParoisExt + SousTotalParoisExt;
    TotalPOrteInt := TotalPOrteInt + SousTotalPorteInt;
    TotalPorteExt := TotalPorteExt + SousTotalPorteExt;
    TotalVitrageInt := TotalVitrageInt + SousTotalVitrageInt;
    TotalVitrageExt := TotalVitrageExt + SousTotalVitrageExt;
  End;
  FormProp.RichEdit1.Lines.Add('');
  FormProp.RichEdit1.Lines.Add('Total pour le b�timent :');
  FormProp.RichEdit1.Lines.Add('');
  Str(Total: 0: 2, C1);
  FormProp.RichEdit1.Lines.Add('Surface du b�timent : ' + C1);
  Str(VTotal: 0: 2, C2);
  FormProp.RichEdit1.Lines.Add('Volume du b�timent : ' + C2);
  Str(TotalParoisExt: 0: 2, C1);
  FormProp.RichEdit1.Lines.Add('Surface des parois ext�rieures : ' + C1);

  Str(TPNord: 0: 2, C1);
  FormProp.RichEdit1.Lines.Add('   Nord : ' + C1);
  Str(TPsud: 0: 2, C1);
  FormProp.RichEdit1.Lines.Add('   Sud : ' + C1);
  Str(TPEst: 0: 2, C1);
  FormProp.RichEdit1.Lines.Add('   Est : ' + C1);
  Str(TPOuest: 0: 2, C1);
  FormProp.RichEdit1.Lines.Add('   Ouest : ' + C1);


  Str(TotalParoisInt: 0: 2, C1);
  FormProp.RichEdit1.Lines.Add('Surface des parois int�rieures : ' + C1);
  Str(TotalPorteExt: 0: 2, C1);
  FormProp.RichEdit1.Lines.Add('Surface des portes ext�rieures : ' + C1);
  Str(TotalPOrteInt: 0: 2, C1);
  FormProp.RichEdit1.Lines.Add('Surface des portes int�rieures : ' + C1);
  Str(TotalVitrageExt: 0: 2, C1);
  FormProp.RichEdit1.Lines.Add('Surface des vitrages ext�rieurs : ' + C1);
  Str(TVNord: 0: 2, C1);
  FormProp.RichEdit1.Lines.Add('   Nord : ' + C1);
  Str(TVsud: 0: 2, C1);
  FormProp.RichEdit1.Lines.Add('   Sud : ' + C1);
  Str(TVest: 0: 2, C1);
  FormProp.RichEdit1.Lines.Add('   Est : ' + C1);
  Str(TVOuest: 0: 2, C1);
  FormProp.RichEdit1.Lines.Add('   Ouest : ' + C1);

  FormProp.ShowModal;

end;

procedure TFormSketch.FormMouseWheel(Sender: TObject; Shift: TShiftState; WheelDelta: integer; MousePos: TPoint; var Handled: boolean);
var
  position: TPoint;
  NewCenterX, NewCenterY, i: integer;
  DX, DY, Delta: Double;
begin
  if (PageControl1.ActivePage <> TabSheet3D) and (Project.sketchObject.zoom <> 1) then
  begin
    position := ImageSketch.ScreenToClient(MousePos);

    DX := position.X * Project.sketchObject.zoom;
    DY := position.Y * Project.sketchObject.zoom;

    DX := DX - Project.sketchObject.Center.X;
    DY := DY - Project.sketchObject.Center.Y;

    // if (WheelDelta  > 120) or (WheelDelta < -120) then
    // Beep;
    For i := 0 to (WheelDelta div 120) do
      Project.sketchObject.zoom := Round(Project.sketchObject.zoom / 1.1);
    For i := (WheelDelta div 120) to 0 do
      Project.sketchObject.zoom := Round(Project.sketchObject.zoom * 1.1);

    NewCenterX := Round((position.X * Project.sketchObject.zoom) - DX);
    NewCenterY := Round((position.Y * Project.sketchObject.zoom) - DY);
    Project.sketchObject.Center.X := NewCenterX;
    Project.sketchObject.Center.Y := NewCenterY;

    Project.sketchObject.Refresh();
  end
  else
  Begin
    Delta := WheelDelta * Sqrt(Sqr(CentreX - PositionCamX) + Sqr(CentreY - PositionCamZ) + Sqr(0 - PositionCamY));
    PositionCamX := PositionCamX + Sin(OrientationCam) * Cos(InclinaisonCam) * Delta / 2000;
    PositionCamZ := PositionCamZ + Cos(OrientationCam) * Cos(InclinaisonCam) * Delta / 2000;
    PositionCamY := PositionCamY + Sin(InclinaisonCam) * Delta / 2000;
    DXTimerTimer(Sender, 0);
  End;
  Handled := true;
end;

function TFormSketch.exportData() : boolean;
var
  sName : string;
  sExt : string;
begin
  try
    //dans tous les cas on exporte au format XML
    if saveFileName <> '' then
    begin
      sExt := ExtractFileExt(saveFileName);
      sName := copy(saveFileName, 1, length(saveFileName) - length(sExt));
    end
    else
    begin
      sName := copy(internalX3DPath, 1, length(internalX3DPath) - length(internalX3DFileExt));;
    end;
    sExt := internalX3DFileExt;

    Project.saveXml(sName + internalX3DFileExt);
    //Project.SaveToFile(sName + internal3DFileExt)
  except
    On e : Exception do
    begin
      MessageDLG('Erreur durant la sauvegarde du projet :' + CR + e.message, mtError, [mbOk], 0);
      exit(false);
    end;
  end;
  try
    TDialogieExport.saveXml(sName + dialogieX3DFileExt);
    //TDialogieExport.save(sName + dialogie3DFileExt);
  except
    on e1 : Exception do
    begin
      MessageDLG('Erreur durant le transfert :' + CR + e1.Message, mtwarning, [mbOk], 0);
      exit(false);
    end;
  End;
  result := true;
end;

procedure TFormSketch.French1Click(Sender: TObject);
Var
  FichierIni: TStringList;
begin
  langue := 2;
  FichierIni := TStringList.Create;
  FichierIni.loadFromFile(i3dDataPath + FileINI);
  FichierIni[3] := IntToStr(langue);
  FichierIni.SaveToFile(i3dDataPath + FileINI);
  FichierIni.Free;
end;

procedure TFormSketch.SpeedChgClick(Sender: TObject);
begin
  if CheckBoxToit.Checked then
  begin
  end
  else
  begin
    CurrentAction := 26;
    ImageSketch.Cursor := crHandPoint;
    initTool;
    DisplayHelp(CurrentAction, FormHelp.ImageHelp.Canvas);
    HelpDisplayed := true;
    Project.sketchObject.InfoCleared := true;
    Formchg.Show;
  end;
end;

procedure TFormSketch.ButtonRecalClick(Sender: TObject);
begin
  Project.sketchObject.Recaler;
end;

procedure TFormSketch.Dfinirlapicecommeunlocalexterne1Click(Sender: TObject);
begin
  CurrentAction := 27;
  ImageSketch.Cursor := crHandPoint;
  initTool;
  DisplayHelp(CurrentAction, FormHelp.ImageHelp.Canvas);
  HelpDisplayed := true;
  SpeedZone.down := true;
end;


// *************************************************
// Display an online Help for each tool
// *************************************************

Procedure TFormSketch.DisplayHelp(CurrentAction: Integer; Can2: TCanvas);
Var
  Temp: Tbitmap;
  Can: TCanvas;
Begin
  Can := FormHelp.ImageHelp.Picture.Bitmap.Canvas;
  StatusBar.Panels[1].text := 'Step 11';
  StatusBar.Repaint;
  Temp := Tbitmap.Create;
  Can.Brush.Style := bsSolid;
  Can.Brush.color := clwhite;
  Can.font.Name := 'Arial';
  Can.font.Size := 8;
  Can.Pen.color := clwhite;
  Can.Rectangle(0, 0, FormHelp.ImageHelp.Width, FormHelp.ImageHelp.Height);
  Can.Pen.color := clblack;
  StatusBar.Panels[1].text := 'Step 12';
  StatusBar.Repaint;
  Case CurrentAction Of
    1:
      Begin
        FormHelp.LabelH.Caption := Labels[104];
        HelpList.GetBitmap(0, Temp);
        Can.Draw(10, 10, Temp);
        Temp.Width := 0;
        Can.TextOut(80, 10, Labels[7]);
        HelpList.GetBitmap(0, Temp);
        Can.Draw(10, 50, Temp);
        Temp.Width := 0;
        HelpList.GetBitmap(4, Temp);
        Can.Draw(30, 46, Temp);
        Temp.Width := 0;
        HelpList.GetBitmap(1, Temp);
        Can.Draw(50, 50, Temp);
        Can.TextOut(80, 50, Labels[8]);
      End;
    3:
      Begin
        FormHelp.LabelH.Caption := Labels[105];
        HelpList.GetBitmap(0, Temp);
        Can.Draw(10, 10, Temp);
        Temp.Width := 0;
        Can.TextOut(80, 10, Labels[9]);
        HelpList.GetBitmap(3, Temp);
        Can.Draw(10, 50, Temp);
        Can.TextOut(80, 50, Labels[10]);
      End;
    4:
      Begin
        FormHelp.LabelH.Caption := Labels[106];
        HelpList.GetBitmap(0, Temp);
        Can.Draw(10, 10, Temp);
        Temp.Width := 0;
        HelpList.GetBitmap(4, Temp);
        Can.Draw(30, 6, Temp);
        Temp.Width := 0;
        HelpList.GetBitmap(1, Temp);
        Can.Draw(50, 10, Temp);
        Can.TextOut(80, 10, Labels[11]);
      End;
    5:
      Begin
        FormHelp.LabelH.Caption := Labels[107];
        HelpList.GetBitmap(0, Temp);
        Can.Draw(10, 10, Temp);
        Temp.Width := 0;
        Can.TextOut(80, 10, Labels[12]);
        HelpList.GetBitmap(0, Temp);
        Can.Draw(10, 50, Temp);
        Temp.Width := 0;
        HelpList.GetBitmap(4, Temp);
        Can.Draw(30, 46, Temp);
        Temp.Width := 0;
        HelpList.GetBitmap(1, Temp);
        Can.Draw(50, 50, Temp);
        Can.TextOut(80, 50, Labels[13]);
      End;
    2:
      Begin
        FormHelp.LabelH.Caption := Labels[108];
        HelpList.GetBitmap(0, Temp);
        Can.Draw(10, 10, Temp);
        Temp.Width := 0;
        Can.TextOut(80, 8, Labels[14]);
        Can.TextOut(80, 28, Labels[15]);

        HelpList.GetBitmap(0, Temp);
        Can.Draw(10, 50, Temp);
        Temp.Width := 0;
        HelpList.GetBitmap(4, Temp);
        Can.Draw(30, 50, Temp);
        Temp.Width := 0;
        HelpList.GetBitmap(1, Temp);
        Can.Draw(50, 50, Temp);
        Temp.Width := 0;
        Can.TextOut(80, 50, Labels[17]);

        HelpList.GetBitmap(2, Temp);
        Can.Draw(10, 90, Temp);
        Temp.Width := 0;
        HelpList.GetBitmap(4, Temp);
        Can.Draw(30, 90, Temp);
        Temp.Width := 0;
        HelpList.GetBitmap(3, Temp);
        Can.Draw(50, 90, Temp);
        Temp.Width := 0;
        Can.TextOut(80, 90, Labels[16]);
        Can.TextOut(80, 110, Labels[99]);
      End;
    6:
      Begin
        FormHelp.LabelH.Caption := Labels[109];
        HelpList.GetBitmap(0, Temp);
        Can.Draw(10, 10, Temp);
        Temp.Width := 0;
        Can.TextOut(80, 8, Labels[14]);
        Can.TextOut(80, 28, Labels[15]);

        HelpList.GetBitmap(0, Temp);
        Can.Draw(10, 50, Temp);
        Temp.Width := 0;
        HelpList.GetBitmap(4, Temp);
        Can.Draw(30, 50, Temp);
        Temp.Width := 0;
        HelpList.GetBitmap(1, Temp);
        Can.Draw(50, 50, Temp);
        Temp.Width := 0;
        Can.TextOut(80, 50, Labels[17]);

        HelpList.GetBitmap(2, Temp);
        Can.Draw(10, 90, Temp);
        Temp.Width := 0;
        HelpList.GetBitmap(4, Temp);
        Can.Draw(30, 90, Temp);
        Temp.Width := 0;
        HelpList.GetBitmap(3, Temp);
        Can.Draw(50, 90, Temp);
        Temp.Width := 0;
        Can.TextOut(80, 90, Labels[18]);
        Can.TextOut(80, 110, Labels[98]);
      End;
    10:
      Begin
        FormHelp.LabelH.Caption := Labels[110];
        HelpList.GetBitmap(0, Temp);
        Can.Draw(10, 10, Temp);
        Temp.Width := 0;
        HelpList.GetBitmap(4, Temp);
        Can.Draw(30, 10, Temp);
        Temp.Width := 0;
        HelpList.GetBitmap(1, Temp);
        Can.Draw(50, 10, Temp);
        Temp.Width := 0;
        Can.TextOut(80, 10, Labels[19]);
      End;
    7:
      Begin
        FormHelp.LabelH.Caption := Labels[111];
        HelpList.GetBitmap(0, Temp);
        Can.Draw(10, 10, Temp);
        Temp.Width := 0;
        Can.TextOut(80, 10, Labels[20]);
      End;
    9:
      Begin
        FormHelp.LabelH.Caption := Labels[112];
        HelpList.GetBitmap(0, Temp);
        Can.Draw(10, 10, Temp);
        Temp.Width := 0;
        Can.TextOut(80, 10, Labels[21]);
      End;
    8:
      Begin
        FormHelp.LabelH.Caption := Labels[113];
        HelpList.GetBitmap(0, Temp);
        Can.Draw(10, 10, Temp);
        Temp.Width := 0;
        Can.TextOut(80, 10, Labels[22]);
      End;
    11:
      Begin
        FormHelp.LabelH.Caption := Labels[114];
        HelpList.GetBitmap(0, Temp);
        Can.Draw(10, 10, Temp);
        Temp.Width := 0;
        Can.TextOut(80, 10, Labels[23]);
      End;
    12:
      Begin
        FormHelp.LabelH.Caption := Labels[115];
        HelpList.GetBitmap(0, Temp);
        Can.Draw(10, 10, Temp);
        Temp.Width := 0;
        Can.TextOut(80, 10, Labels[33]);
      End;
    13:
      Begin
        FormHelp.LabelH.Caption := Labels[116];
        HelpList.GetBitmap(0, Temp);
        Can.Draw(10, 10, Temp);
        Temp.Width := 0;
        HelpList.GetBitmap(4, Temp);
        Can.Draw(30, 6, Temp);
        Temp.Width := 0;
        HelpList.GetBitmap(1, Temp);
        Can.Draw(50, 10, Temp);
        Can.TextOut(80, 10, Labels[42]);
      End;
    15:
      Begin
        FormHelp.LabelH.Caption := Labels[117];
        HelpList.GetBitmap(0, Temp);
        Can.Draw(10, 10, Temp);
        Temp.Width := 0;
        HelpList.GetBitmap(4, Temp);
        Can.Draw(30, 6, Temp);
        Temp.Width := 0;
        HelpList.GetBitmap(1, Temp);
        Can.Draw(50, 10, Temp);
        Can.TextOut(80, 10, Labels[100]);
        Can.TextOut(80, 30, Labels[101]);
      End;
    14:
      Begin
        FormHelp.LabelH.Caption := Labels[118];
        HelpList.GetBitmap(0, Temp);
        Can.Draw(10, 10, Temp);
        Temp.Width := 0;
        HelpList.GetBitmap(4, Temp);
        Can.Draw(30, 6, Temp);
        Temp.Width := 0;
        HelpList.GetBitmap(1, Temp);
        Can.Draw(50, 10, Temp);
        Can.TextOut(80, 10, Labels[102]);
        Can.TextOut(80, 30, Labels[103]);
      End;
    20:
      Begin
        FormHelp.LabelH.Caption := Labels[122];
        Can.TextOut(10, 10, Labels[123]);
        Can.TextOut(10, 30, Labels[124]);
        Can.TextOut(10, 50, Labels[125]);
        Can.TextOut(10, 70, Labels[126]);
        Can.TextOut(10, 90, Labels[127]);
        Can.TextOut(10, 110, Labels[128]);
      End;
    21:
      Begin
        FormHelp.LabelH.Caption := Labels[129];
        Can.TextOut(10, 10, Labels[130]);
      End;
    22:
      Begin
        FormHelp.LabelH.Caption := Labels[131];
        Can.TextOut(10, 10, Labels[132]);
        Can.TextOut(10, 30, Labels[133]);
        Can.TextOut(10, 50, Labels[134]);
        Can.TextOut(10, 70, Labels[135]);
        Can.TextOut(10, 90, Labels[136]);
      End;
//    23:
//      Begin
//        FormHelp.LabelH.Caption := Labels[137];
//        Can.TextOut(10, 10, Labels[138]);
//        Can.TextOut(10, 30, Labels[139]);
//        Can.TextOut(10, 50, Labels[140]);
//      End;
    24:
      Begin
        FormHelp.LabelH.Caption := Labels[209];
        Can.TextOut(10, 10, Labels[210]);
        Can.TextOut(10, 40, Labels[211]);
        Can.TextOut(10, 60, Labels[212]);
      End;
    25:
      Begin
        FormHelp.LabelH.Caption := Labels[208];
        Can.TextOut(10, 10, Labels[132]);
        Can.TextOut(10, 30, Labels[133]);
        Can.TextOut(10, 50, Labels[134]);
        Can.TextOut(10, 70, Labels[135]);
        Can.TextOut(10, 90, Labels[136]);
      End;
    26:
      Begin
        FormHelp.LabelH.Caption := Labels[232];
        Can.TextOut(10, 10, Labels[214]);
        Can.TextOut(10, 40, Labels[215]);
        Can.TextOut(10, 70, Labels[216]);
      End;
  End;
  StatusBar.Panels[1].text := 'Step 13';
  StatusBar.Repaint;
  Temp.Free;
  if AutoShowHide then
    FormHelp.Show;
  StatusBar.Panels[1].text := 'Step 14';
  StatusBar.Repaint;
end;


end.
