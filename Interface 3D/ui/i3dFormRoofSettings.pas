unit i3dFormRoofSettings;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons;

type
  TFormRoof = class(TForm)
    Label1: TLabel;
    ComboPoint1: TComboBox;
    ComboPoint2: TComboBox;
    ComboPoint3: TComboBox;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Height1: TEdit;
    Height2: TEdit;
    Height3: TEdit;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    procedure ComboPoint1Change(Sender: TObject);
  private
    { Déclarations privées }
  public
    { Déclarations publiques }
  end;

var
  FormRoof: TFormRoof;

implementation

{$R *.DFM}

procedure TFormRoof.ComboPoint1Change(Sender: TObject);
Var
   Ok : Boolean;
   Valeur : Double;
   Erreur : Integer;
begin
     Ok := True;
     If ComboPoint1.itemIndex = -1 then
        Ok := False;
     If ComboPoint2.ItemIndex = -1 then
        Ok := False;
     If ComboPoint3.ItemIndex = -1 then
        Ok := False;
     Val(Height1.Text,Valeur,Erreur);
     if Erreur <> 0 then
      Ok := False;
     Val(Height2.Text,Valeur,Erreur);
     if Erreur <> 0 then
      Ok := False;
     Val(Height3.Text,Valeur,Erreur);
     if Erreur <> 0 then
      Ok := False;
     if (ComboPoint1.itemIndex = ComboPoint2.itemIndex) or
        (ComboPoint1.itemIndex = ComboPoint3.itemIndex) or
        (ComboPoint2.itemIndex = ComboPoint3.itemIndex) then
          Ok := False;
     BitBtn1.enabled := Ok;
end;

end.
