unit i3dFormInputWall;

interface

uses
  Windows,
  Messages,
  SysUtils,
  Classes,
  Graphics,
  Controls,
  Forms,
  Dialogs,
  StdCtrls,
  Buttons,
  Math,
  i3dGeometry,
  i3dSketchLevel,
  compositions;

type
  TListeDecl = Array[1..12] of double;
  TTable = Array of TPoint;
    TPoly = class
       Poly : TTable;
    End;

  TFormInputWall = class(TForm)
    GroupBox2: TGroupBox;
    Label2: TLabel;
    EditSize: TEdit;
    Label3: TLabel;
    Label4: TLabel;
    StaticText1: TStaticText;
    Label5: TLabel;
    GroupBox1: TGroupBox;
    BitBtn3: TBitBtn;
    StaticComposition: TStaticText;
    Button1: TBitBtn;
    BitBtn2: TBitBtn;
    BitBtn1: TBitBtn;
    Procedure SunCalculation(Focale : Boolean;Image : TBitmap; Lat,Orientation : Double);
    Procedure ShadingCalculation(Image : TBitmap);
    Function Angle(P1,P2:LTPoint) : Double;
    Function CalculateUHA(Segment : LTSegment;Var Street : Double) : Double;
    procedure B(Sender: TObject);
    procedure Button1Click(Sender: TObject);
  private
    { Déclarations privées}
  public
    Seg : LTSegment;
    Level : TsketchLevel;
    TempShadings : Integer;
    { Déclarations publiques}
  end;

  Const
     Declinaison : TListedecl = (-20.8,-12.7,-1.9,9.9,18.9,23.1,21.3,13.7,3.0,
                               -8.8,-18.4,-23.0);

var
  FormInputWall: TFormInputWall;

implementation

Uses
  i3dConstants,
  i3dTranslation,
  i3dFormChoixCompo,
  i3dSketchObject;

{$R *.DFM}

Procedure TFormInputWall.SunCalculation(Focale : Boolean;Image : TBitmap; Lat,Orientation : Double);
Var
   Azimuth,Hauteur,SinDecl,CosDecl,SinLat,CosLat,HeureSolaire : Double;
   Mois,Heure,NouvX : Integer;
   Temp : Tpoint;
   Tempo : Double;
begin
     Orientation := ORientation
        -project.sketchObject.OrientationNord;

     NouvX := 0;
     Image.canvas.pen.color := clRed;
     HeureSolaire := -180;
     SinDecl := Sin(Declinaison[1]/180*pi);
     CosDecl := Cos(Declinaison[1]/180*pi);
     SinLat := Sin(Lat/180*pi);
     Coslat := cos(Lat/180*pi);
     Hauteur := ArcSin( SinLat*Sindecl + CosLat*CosDecl*Cos(HeureSolaire/180*pi));
     Azimuth := (SinLat*CosDecl*Cos(HeureSolaire/180*pi) - CosLat*sinDecl)
                 / Cos(Hauteur) - (Orientation/180*pi);
     Image.Canvas.MoveTo(Round(Azimuth/pi*180/360*Image.width+(image.width/2)),
                              image.Height-Round(Hauteur/pi*180/90*Image.Height));
     For mois := 1 to 12 do
     begin
      for Heure := 0 to 24 do
      begin
          HeureSolaire := Heure*15-180;
          SinDecl := Sin(Declinaison[Mois]/180*pi);
          CosDecl := Cos(Declinaison[Mois]/180*pi);
          SinLat := Sin(Lat/180*pi);
          Coslat := cos(Lat/180*pi);
          Tempo := SinLat*Sindecl + CosLat*CosDecl*Cos(HeureSolaire/180*pi);
          if tempo < -1 then
             tempo := -1
          else if tempo > 1 then
             tempo := 1;

          Hauteur := ArcSin(Tempo);
          Azimuth := (SinLat*CosDecl*Cos(HeureSolaire/180*pi) - CosLat*sinDecl)
                        / Cos(Hauteur) ;
          if (Azimuth >= -1) and (azimuth <= 1) then
                Azimuth := Arccos(Azimuth)
          else
                Azimuth := Arccos(Round(Azimuth));
          if HeureSolaire < 0 then
                Azimuth := -Abs(Azimuth)
          else
                Azimuth := Abs(Azimuth);

          Azimuth := Azimuth - (Orientation/180*pi);
          if Azimuth > Pi then Azimuth := Azimuth - (2*pi);
          if Azimuth < -Pi then Azimuth := Azimuth + (2*pi);

          if Focale then
          Begin
               if (Image.canvas.PenPos.X
                     -Round(Azimuth/pi*180/360*Image.width+(image.width/2))) > (image.width div 2) then
               begin
                       Image.Canvas.LineTo(Round(Azimuth/pi*180/360*Image.width+(image.width*1.5)),
                              image.Height-Round(Hauteur/pi*180/90*Image.Height));
                       Image.Canvas.MoveTo(Round(Azimuth/pi*180/360*Image.width-(image.width*0.5)),
                              image.Height-Round(Hauteur/pi*180/90*Image.Height));
                       Image.Canvas.LineTo(Round(Azimuth/pi*180/360*Image.width+(image.width/2)),
                              image.Height-Round(Hauteur/pi*180/90*Image.Height))
               end
               else if (Image.canvas.PenPos.X
                       -Round(Azimuth/pi*180/360*Image.width+(image.width/2))) < -(image.width div 2) then
               begin
                       Image.Canvas.LineTo(Round(Azimuth/pi*180/360*Image.width-(image.width*0.5)),
                              image.Height-Round(Hauteur/pi*180/90*Image.Height));
                       Image.Canvas.MoveTo(Round(Azimuth/pi*180/360*Image.width+(image.width*1.5)),
                              image.Height-Round(Hauteur/pi*180/90*Image.Height));
                       Image.Canvas.LineTo(Round(Azimuth/pi*180/360*Image.width+(image.width/2)),
                              image.Height-Round(Hauteur/pi*180/90*Image.Height))
               end
               else if (Heure <> 0) then
                       Image.Canvas.LineTo(Round(Azimuth/pi*180/360*Image.width+(image.width/2)),
                              image.Height-Round(Hauteur/pi*180/90*Image.Height))
               else if (Heure = 0) then
                       Image.Canvas.MoveTo(Round(Azimuth/pi*180/360*Image.width+(image.width/2)),
                              image.Height-Round(Hauteur/pi*180/90*Image.Height));
               If ((Heure = 11) and (mois <= 6)) or ((Heure = 13) and (mois > 6)) then
               Begin
                        Temp := Image.canvas.PenPos;
                       Image.Canvas.Brush.color := clWhite;
                       Image.Canvas.Brush.style := bsClear;
                       Image.Canvas.font.Size := 8;
                       Image.canvas.TextOut(Round(Azimuth/pi*180/360*Image.width+(image.width/2)),
                              image.Height-Round(Hauteur/pi*180/90*Image.Height+7),
                              Labels[Mois+71]);
                       Image.canvas.PenPos := Temp;
               End;
          End
          Else
          Begin
               if (Heure <> 0) then
               Begin
                    NouvX := Round(Azimuth/pi*180/180*Image.width+(image.width/2));
                    If Abs(NouvX - Image.Canvas.PenPos.X) < Image.Width then
                       Image.Canvas.LineTo(NouvX,image.Height-Round(Hauteur/pi*180/90*Image.Height))
                    else
                       Image.Canvas.MoveTo(NouvX,image.Height-Round(Hauteur/pi*180/90*Image.Height))
               end
               else if Heure = 0 then
                       Image.Canvas.MoveTo(Round(Azimuth/pi*180/180*Image.width+(image.width/2)),
                              image.Height-Round(Hauteur/pi*180/90*Image.Height));
          End
      End
     End;
     For mois := 1 to 12 do
     begin
      for Heure := 0 to 24 do
      begin
          HeureSolaire := Heure*15-180;
          SinDecl := Sin(Declinaison[Mois]/180*pi);
          CosDecl := Cos(Declinaison[Mois]/180*pi);
          SinLat := Sin(Lat/180*pi);
          Coslat := cos(Lat/180*pi);
          Tempo := SinLat*Sindecl + CosLat*CosDecl*Cos(HeureSolaire/180*pi);
          if tempo < -1 then
             tempo := -1
          else if tempo > 1 then
             tempo := 1;

          Hauteur := ArcSin(Tempo);
          Azimuth := (SinLat*CosDecl*Cos(HeureSolaire/180*pi) - CosLat*sinDecl)
                        / Cos(Hauteur) ;
          if (Azimuth >= -1) and (azimuth <= 1) then
                Azimuth := Arccos(Azimuth)
          else
                Azimuth := Arccos(Round(Azimuth));
          if HeureSolaire < 0 then
                Azimuth := -Abs(Azimuth)
          else
                Azimuth := Abs(Azimuth);

          Azimuth := Azimuth - (Orientation/180*pi);
          if Azimuth > Pi then Azimuth := Azimuth - (2*pi);
          if Azimuth < -Pi then Azimuth := Azimuth + (2*pi);
               if (Heure <> 0) then
               Begin
                    NouvX := Round(Azimuth/pi*180/180*Image.width+(image.width/2));
                    If Abs(NouvX - Image.Canvas.PenPos.X) < Image.Width then
                    else
                       Image.Canvas.MoveTo(NouvX,image.Height-Round(Hauteur/pi*180/90*Image.Height))
               end
               else if Heure = 0 then
                       Image.Canvas.MoveTo(Round(Azimuth/pi*180/180*Image.width+(image.width/2)),
                              image.Height-Round(Hauteur/pi*180/90*Image.Height));
          If ((Heure = 11) and (mois <= 6)) or ((Heure = 13) and (mois > 6)) then
          Begin
               Temp := Image.canvas.PenPos;
               Image.Canvas.Brush.color := clWhite;
               Image.Canvas.Brush.style := bsClear;
               Image.Canvas.font.Size := 8;
               Image.Canvas.Font.Style := Image.Canvas.Font.Style+[fsBold];
               Image.canvas.TextOut(Round(NouvX),
                              image.Height-Round(Hauteur/pi*180/90*Image.Height+7),
                              '<-- '+Labels[Mois+72]);
               Image.canvas.PenPos := Temp
           End
          End
      End
end;

Function TFormInputWall.CalculateUHA(Segment : LTSegment;
             Var Street : Double):Double;
Var
   i,j,k,X1,X2,Y1,Y2,X3,Y3 : Integer;
   Room : TRoom;
   Center : LTPoint;
   Cangle1,Cangle2,Cangle3,Distance1,Distance2,Distance3,XCenter : Double;
   YCenter,sq1,sq2,Total : Double;
   Poly : Tpoly;
   PolyList : TStringList;
   TempString,TempString1,TempString2 : String;
   Orientation,ShadingHeight,AverageHeight,DistanceD : Double;
   Lev,lev2 : TsketchLevel;
   BitmapCalculation : TBitmap;
   Recto : TRect;
   P : PByteArray;
   StepInterPol,InterPol : Integer;
   VirtualPoint : LTPoint;
   DistanceMinimale,CAngleMinimal : Double;
Begin
     DistanceMinimale := 1000000;
     CAngleMinimal := 1000000;
     VirtualPoint := LTPoint.Create(Point(0,0));
     BitmapCalculation := TBitmap.Create;
     BitmapCalculation.HandleType := bmDIB;
     BitmapCalculation.PixelFormat := pf8bit;
     BitmapCalculation.Width := 120;
     BitmapCalculation.Height := 90;
     Recto.left := 0;
     Recto.Right := 120;
     Recto.Top := 0;
     Recto.Bottom := 90;
     BitmapCalculation.Canvas.Brush.Color := clblack;
     BitmapCalculation.Canvas.Pen.Color := clblack;
     BitmapCalculation.Canvas.FillRect(Recto);
     PolyList := TStringList.Create;

     // Calculate the Center of the wall
     Center := LTPoint.Create(Point(0,0));
     Center.x := (Seg.a.x+Seg.b.x) div 2;
     Center.y := (Seg.a.y+Seg.b.y) div 2;

     // For Each Zone
     For i := 0 to level.Rooms.count-1 do
     Begin
          Room := TRoom(Level.Rooms[i]);
          if Room.RoomType <> roomObstruction then
          // The Room is not an obstruction
             For j := 0 to Room.Points.Count-2 do
             Begin
                if not(((Room.Points[j] = Seg.a) and (Room.Points[j+1] = Seg.b)) or
                     ((Room.Points[j] = Seg.b) and (Room.Points[j+1] = Seg.a))) then
                Begin
                  Orientation := Seg.Orientation-180;
                  if Orientation < -180 then Orientation := Orientation+360;
                  Cangle1 := FormInputWall.Angle(Center,Room.Points[j])-(pi/2);
                  if Cangle1 < -pi then Cangle1 := Cangle1+2*pi;
                  Cangle1 := Cangle1-(Orientation/180*pi);
                  if Cangle1 < -pi then Cangle1 := Cangle1+2*pi;
                  if Cangle1 > pi then Cangle1 := Cangle1-2*pi;
                  Cangle2 := FormInputWall.Angle(Center,Room.Points[j+1])-(pi/2);
                  if Cangle2 < -pi then Cangle2 := Cangle2+2*pi;
                  Cangle2 := CAngle2-(Orientation/180*pi);
                  if Cangle2 < -pi then Cangle2 := Cangle2+2*pi;
                  if Cangle2 > pi then Cangle2 := Cangle2-2*pi;

                  if ((Cangle1 >= -pi/2) and (CAngle1 <= pi/2)) or
                     ((Cangle2 >= -pi/2) and (CAngle2 <= pi/2)) then
                  Begin
                       X1 := (BitmapCalculation.width div 2)
                             +Round(Cangle1/(2*pi/3)*BitmapCalculation.width);
                       xCenter := Center.X;
                       yCenter := Center.Y;
                       Distance1 := Sqrt(Sqr(LTpoint(Room.Points[j]).x-xCenter)+
                          Sqr(LTpoint(Room.Points[j]).y-yCenter))/
                          project.sketchObject.scale;
                       if Room.RoomType = roomObstruction then
                       Begin
                          ShadingHeight := Room.Value1-(Level.Height/2);
                       end
                       else
                       Begin
                            ShadingHeight := 0;
                            With project.sketchObject do
                              For k := 0 to Levels.count-1 do
                              Begin
                                 Lev := TSketchLevel(levels[k]);
                                 if (Lev.points.Count <> 0) then
                                    ShadingHeight := ShadingHeight+Lev.height;
                              End;
                              ShadingHeight := ShadingHeight-(Level.Height/2);
                       End;
                       With project.sketchObject do
                         For k := 0 to CurrentLevel-1 do
                         Begin
                               Lev := TSketchLevel(levels[k]);
                               ShadingHeight := ShadingHeight-Lev.height;
                         End;
                       if ShadingHeight < 0 then shadingHeight := 0;
                       
                       Y1 := BitmapCalculation.height
                           -Round(arctan(ShadingHeight/Distance1)/(pi/2)
                             *BitmapCalculation.Height);
                       X2 := (BitmapCalculation.width div 2)
                               +Round(Cangle2/(2*pi/3)*BitmapCalculation.width);
                       sq1 := Sqr(LTpoint(Room.Points[j+1]).x-xCenter);
                       sq2 := Sqr(LTpoint(Room.Points[j+1]).y-yCenter);
                       Distance2 := Sqrt(sq1+sq2)/project.sketchObject.scale;
                       Y2 := BitmapCalculation.height
                            -Round(arctan(ShadingHeight/Distance2)/(pi/2)
                             *BitmapCalculation.Height);
                       if Abs(Distance1) < DistanceMinimale then
                       Begin
                            CAngleMinimal := Abs(Cangle1);
                            DistanceMinimale := Distance1;
                       End;
                       if Abs(Distance2) < DistanceMinimale then
                       Begin
                            CAngleMinimal := Abs(Cangle2);
                            DistanceMinimale := Distance2;
                       End;
                       Poly := TPoly.Create;
                       StepInterPol := Abs(X1-X2)div 5+1;
                       SetLength(Poly.poly, StepInterPol+3);
                       Poly.Poly[0] := Point(X1,BitmapCalculation.height);
                       Poly.Poly[1] := Point(X1,Y1);
                       Poly.Poly[StepInterPol+1] := Point(X2,Y2);
                       Poly.Poly[StepInterPol+2] := Point(X2,BitmapCalculation.height);
                           Str(Distance1+Distance2:8:2,TempString);
                       For interpol := 1 to StepInterpol-1 do
                       Begin
                            VirtualPoint.x :=  ((LTpoint(Room.Points[j+1]).x*Interpol)
                               + (LTpoint(Room.Points[j]).x*(StepInterpol-Interpol))) div StepInterpol;
                            VirtualPoint.y :=  ((LTpoint(Room.Points[j+1]).y*Interpol)
                               + (LTpoint(Room.Points[j]).y*(StepInterpol-Interpol))) div StepInterpol;
                            CAngle3 := FormInputWall.Angle(Center,VirtualPoint)-(pi/2);
                            if Cangle3 < -pi then Cangle3 := Cangle3+2*pi;
                            Cangle3 := Cangle3-(Orientation/180*pi);
                            if Cangle3 < -pi then Cangle3 := Cangle3+2*pi;
                            if Cangle3 > pi then Cangle3 := Cangle3-2*pi;
                            X3 := (BitmapCalculation.width div 2)+ Round(Cangle3/(2*pi/3)*BitmapCalculation.width);
                            Distance3 := Sqrt(Sqr(VirtualPoint.x-xCenter)+
                               Sqr(VirtualPoint.y-yCenter))/
                               project.sketchObject.scale;
                            Y3 := BitmapCalculation.height-Round(arctan(ShadingHeight/
                                  Distance3)/(pi/2)*BitmapCalculation.Height);
                            Poly.Poly[1+interpol] := Point(X3,Y3);
                            if Abs(Distance3) < DistanceMinimale then
                            Begin
                                 CAngleMinimal := Abs(Cangle3);
                                 DistanceMinimale := Distance3;
                            End;
                       End;
                       For k := 1 to Length(TempString) do
                           if TempString[k] = ' ' then
                              TempString[k] := '0';
                       PolyList.AddObject(TempString,TObject(Poly));
                  End;
                End;
             End;
     end;

     Lev2 := TsketchLevel(project.sketchObject.Levels[0]);
     For i := 0 to lev2.Rooms.count-1 do
     Begin
          Room := TRoom(Lev2.Rooms[i]);
           if Room.RoomType = roomObstruction then
             For j := 0 to Room.Points.Count-2 do
             Begin
                if not(((Room.Points[j] = Seg.a) and (Room.Points[j+1] = Seg.b)) or
                     ((Room.Points[j] = Seg.b) and (Room.Points[j+1] = Seg.a))) then
                Begin
                  Orientation := Seg.Orientation-180;
                  if Orientation < -180 then Orientation := Orientation+360;
                  Cangle1 := FormInputWall.Angle(Center,Room.Points[j])-(pi/2);
                  if Cangle1 < -pi then Cangle1 := Cangle1+2*pi;
                  Cangle1 := Cangle1-(Orientation/180*pi);
                  if Cangle1 < -pi then Cangle1 := Cangle1+2*pi;
                  if Cangle1 > pi then Cangle1 := Cangle1-2*pi;
                  Cangle2 := FormInputWall.Angle(Center,Room.Points[j+1])-(pi/2);
                  if Cangle2 < -pi then Cangle2 := Cangle2+2*pi;
                  CAngle2 := CAngle2-(Orientation/180*pi);
                  if Cangle2 < -pi then Cangle2 := Cangle2+2*pi;
                  if Cangle2 > pi then Cangle2 := Cangle2-2*pi;

                  if ((Cangle1 >= -pi/2) and (CAngle1 <= pi/2)) or
                     ((Cangle2 >= -pi/2) and (CAngle2 <= pi/2)) then
                  Begin
                       X1 := (BitmapCalculation.width div 2)+ Round(Cangle1/(2*pi/3)*BitmapCalculation.width);
                       xCenter := Center.X;
                       yCenter := Center.Y;
                       Distance1 := Sqrt(Sqr(LTpoint(Room.Points[j]).x-xCenter)+
                          Sqr(LTpoint(Room.Points[j]).y-yCenter))/
                          project.sketchObject.scale;
                       if Room.RoomType = roomObstruction then
                       Begin
                          ShadingHeight := Room.Value1-(Lev2.Height/2);
                       end
                       else
                       Begin
                            ShadingHeight := 0;
                            With project.sketchObject do
                              For k := 0 to Levels.count-1 do
                              Begin
                                 Lev := TSketchLevel(levels[k]);
                                 if (Lev.points.Count <> 0) then
                                    ShadingHeight := ShadingHeight+Lev.height;
                              End;
                              ShadingHeight := ShadingHeight-(Lev2.Height/2);
                       End;
                       With project.sketchObject do
                         For k := 0 to CurrentLevel-1 do
                         Begin
                               Lev := TSketchLevel(levels[k]);
                               ShadingHeight := ShadingHeight-Lev.height;
                         End;
                       if ShadingHeight < 0 then shadingHeight := 0;
                       Y1 := BitmapCalculation.height-Round(arctan(ShadingHeight/Distance1)/(pi/2)*BitmapCalculation.Height);
                       X2 := (BitmapCalculation.width div 2)+ Round(Cangle2/(2*pi/3)*BitmapCalculation.width);
                       sq1 := Sqr(LTpoint(Room.Points[j+1]).x-xCenter);
                       sq2 := Sqr(LTpoint(Room.Points[j+1]).y-yCenter);
                       Distance2 := Sqrt(sq1+sq2)/project.sketchObject.scale;
                       if Abs(Distance1) < DistanceMinimale then
                       Begin
                            CAngleMinimal := Abs(Cangle1);
                            DistanceMinimale := Distance1;
                       End;
                       if Abs(Distance2) < DistanceMinimale then
                       Begin
                            CAngleMinimal := Abs(Cangle2);
                            DistanceMinimale := Distance2;
                       End;
                       Y2 := BitmapCalculation.height-Round(arctan(ShadingHeight/Distance2)/(pi/2)*BitmapCalculation.Height);
                       Poly := TPoly.Create;
                       StepInterPol := Abs(X1-X2)div 5+1;
                       SetLength(Poly.poly, StepInterPol+3);
                       Poly.Poly[0] := Point(X1,BitmapCalculation.height);
                       Poly.Poly[1] := Point(X1,Y1);
                       Poly.Poly[StepInterPol+1] := Point(X2,Y2);
                       Poly.Poly[StepInterPol+2] := Point(X2,BitmapCalculation.height);
                       For interpol := 1 to StepInterpol-1 do
                       Begin
                            VirtualPoint.x :=  ((LTpoint(Room.Points[j+1]).x*Interpol)
                               + (LTpoint(Room.Points[j]).x*(StepInterpol-Interpol))) div StepInterpol;
                            VirtualPoint.y :=  ((LTpoint(Room.Points[j+1]).y*Interpol)
                               + (LTpoint(Room.Points[j]).y*(StepInterpol-Interpol))) div StepInterpol;
                            CAngle3 := FormInputWall.Angle(Center,VirtualPoint)-(pi/2);
                            if Cangle3 < -pi then Cangle3 := Cangle3+2*pi;
                            Cangle3 := Cangle3-(Orientation/180*pi);
                            if Cangle3 < -pi then Cangle3 := Cangle3+2*pi;
                            if Cangle3 > pi then Cangle3 := Cangle3-2*pi;
                            X3 := (BitmapCalculation.width div 2)+ Round(Cangle3/(2*pi/3)*BitmapCalculation.width);
                            Distance3 := Sqrt(Sqr(VirtualPoint.x-xCenter)+
                               Sqr(VirtualPoint.y-yCenter))/
                               project.sketchObject.scale;
                            Y3 := BitmapCalculation.height-Round(arctan(ShadingHeight/
                                  Distance3)/(pi/2)*BitmapCalculation.Height);
                            Poly.Poly[1+interpol] := Point(X3,Y3);
                            if Abs(Distance3) < DistanceMinimale then
                            Begin
                                 CAngleMinimal := Abs(Cangle3);
                                 DistanceMinimale := Distance3;
                            End;
                       End;
                           Str(Distance1+Distance2:8:2,TempString);
                       For k := 1 to Length(TempString) do
                           if TempString[k] = ' ' then
                              TempString[k] := '0';
                       PolyList.AddObject(TempString,TObject(Poly));
                  End;
                End;
             End;
     end;
     PolyList.Sort;
     BitmapCalculation.Canvas.Pen.Width := 1;
     BitmapCalculation.Canvas.Pen.color := clwhite;
     BitmapCalculation.Canvas.Brush.Color := clwhite;
     BitmapCalculation.Canvas.Brush.Style := bsSolid;
     For i := PolyList.Count-1 downto 0 do
     Begin
          BitmapCalculation.Canvas.Polygon(TPoly(PolyList.Objects[i]).poly);
          TPoly(PolyList.Objects[i]).Poly := nil;
          TPoly(PolyList.Objects[i]).Free;
     End;
     Total := 0;
     For i := 0 to BitmapCalculation.Height-1 do
     Begin
          P := BitmapCalculation.ScanLine[i];
          for j := 0 to BitmapCalculation.width -1 do
              if P[j] <> 0 then Total := Total+1;
     End;
     Total := Total/(90*120);
     CalculateUHA := Total;
     Center.Free;
     PolyList.Free;
     BitmapCalculation.Free;
     Street := DistanceMinimale / Cos(CangleMinimal);
End;

Procedure TFormInputWall.ShadingCalculation;
Var
   i,j,k,X1,X2,X3,Y1,Y2,Y3,x,y,Reponse,PixelSize,xx : Integer;
   Room : TRoom;
   Segbis : LTSegment;
   Center : LTPoint;
   Cangle1,Cangle2,Cangle3,Distance1,Distance2,Distance3 : Double;
   XCenter,YCenter,sq1,sq2 : Double;
   Poly : Tpoly;
   PolyList : TstringList;
   TempString,TempString1,TempString2 : String;
   Orientation,ShadingHeight : Double;
   Lev,lev2 : TsketchLevel;
   UHA : Double;
   P : PByteArray;
   Recto : TRect;
   InterPol,StepInterPol : Integer;
   VirtualPoint : LTPoint;
   Street : Double;
Begin
     PolyList := TStringList.Create;
     VirtualPoint := LTPoint.Create(Point(0,0));
     // Calculate the Center of the wall
     Center := LTPoint.Create(Point(0,0));
     Center.x := (Seg.a.x+Seg.b.x) div 2;
     Center.y := (Seg.a.y+Seg.b.y) div 2;

     // For Each Zone
     For i := 0 to level.Rooms.count-1 do
     Begin
          Room := TRoom(Level.Rooms[i]);
          if (Room.RoomType <> roomObstruction) and (Room.RoomType <> roomBuffer) then
          // The Room is not an obstruction
             For j := 0 to Room.Points.Count-2 do
             Begin
                if not(((Room.Points[j] = Seg.a) and (Room.Points[j+1] = Seg.b)) or
                     ((Room.Points[j] = Seg.b) and (Room.Points[j+1] = Seg.a))) then
                Begin
                  SegBis := Room.GetSegment(Room.Points[j],Room.Points[j+1],
                    Level.segments);
                  Orientation := Seg.Orientation-180;
                  if Orientation < -180 then Orientation := Orientation+360;
                  Cangle1 := FormInputWall.Angle(Center,Room.Points[j])-(pi/2);
                  if Cangle1 < -pi then Cangle1 := Cangle1+2*pi;
                  Cangle1 := Cangle1-(Orientation/180*pi);
                  if Cangle1 < -pi then Cangle1 := Cangle1+2*pi;
                  if Cangle1 > pi then Cangle1 := Cangle1-2*pi;
                  Cangle2 := FormInputWall.Angle(Center,Room.Points[j+1])-(pi/2);
                  if Cangle2 < -pi then Cangle2 := Cangle2+2*pi;
                  CAngle2 := CAngle2-(Orientation/180*pi);
                  if Cangle2 < -pi then Cangle2 := Cangle2+2*pi;
                  if Cangle2 > pi then Cangle2 := Cangle2-2*pi;

                  if ((Cangle1 >= -pi/2) and (CAngle1 <= pi/2)) or
                     ((Cangle2 >= -pi/2) and (CAngle2 <= pi/2)) then
                  Begin
                       X1 := (Image.width div 2)+ Round(Cangle1/pi*Image.width);
                       xCenter := Center.X;
                       yCenter := Center.Y;
                       Distance1 := Sqrt(Sqr(LTpoint(Room.Points[j]).x-xCenter)+
                          Sqr(LTpoint(Room.Points[j]).y-yCenter))/
                          project.sketchObject.scale;
                       //if SegBis.Perimeter and SegBis.Adjacent then
                       //   ShadingHeight := 0;
                       if Room.RoomType = roomObstruction then
                       Begin
                          ShadingHeight := Room.Value1-(Level.Height/2);
                       end
                       else
                       Begin
                            ShadingHeight := 0;
                            With project.sketchObject do
                              For k := 0 to Levels.count-1 do
                              Begin
                                 Lev := TSketchLevel(levels[k]);
                                 if (Lev.points.Count <> 0) then
                                    ShadingHeight := ShadingHeight+Lev.height;
                              End;
                              ShadingHeight := ShadingHeight-(Level.Height/2);
                       End;
                       With project.sketchObject do
                         For k := 0 to CurrentLevel-1 do
                         Begin
                               Lev := TSketchLevel(levels[k]);
                               ShadingHeight := ShadingHeight-Lev.height;
                         End;
                       if ShadingHeight < 0 then shadingHeight := 0;
                       
                       Y1 := Image.height-Round(arctan(ShadingHeight/Distance1)/(pi/2)*Image.Height);

                       X2 := (Image.width div 2)+ Round(Cangle2/pi*Image.width);
                       sq1 := Sqr(LTpoint(Room.Points[j+1]).x-xCenter);
                       sq2 := Sqr(LTpoint(Room.Points[j+1]).y-yCenter);
                       Distance2 := Sqrt(sq1+sq2)/project.sketchObject.scale;
                       Y2 := Image.height-Round(arctan(ShadingHeight/Distance2)/(pi/2)*Image.Height);

                       Poly := TPoly.Create;
                       StepInterPol := Abs(X1-X2)div 5+1;
                       SetLength(Poly.poly, StepInterPol+3);
                       Poly.Poly[0] := Point(X1,Image.height);
                       Poly.Poly[1] := Point(X1,Y1);
                       Poly.Poly[StepInterpol+1] := Point(X2,Y2);
                       Poly.Poly[StepInterpol+2] := Point(X2,Image.height);
                       For interpol := 1 to StepInterpol-1 do
                       Begin
                            VirtualPoint.x :=  ((LTpoint(Room.Points[j+1]).x*Interpol)
                               + (LTpoint(Room.Points[j]).x*(StepInterpol-Interpol))) div StepInterpol;
                            VirtualPoint.y :=  ((LTpoint(Room.Points[j+1]).y*Interpol)
                               + (LTpoint(Room.Points[j]).y*(StepInterpol-Interpol))) div StepInterpol;
                            CAngle3 := FormInputWall.Angle(Center,VirtualPoint)-(pi/2);
                            if Cangle3 < -pi then Cangle3 := Cangle3+2*pi;
                            Cangle3 := Cangle3-(Orientation/180*pi);
                            if Cangle3 < -pi then Cangle3 := Cangle3+2*pi;
                            if Cangle3 > pi then Cangle3 := Cangle3-2*pi;
                            X3 := (Image.width div 2)+ Round(Cangle3/pi*Image.width);
                            Distance3 := Sqrt(Sqr(VirtualPoint.x-xCenter)+
                               Sqr(VirtualPoint.y-yCenter))/
                               project.sketchObject.scale;
                            Y3 := Image.height-Round(arctan(ShadingHeight/
                                  Distance3)/(pi/2)*Image.Height);
                            Poly.Poly[1+interpol] := Point(X3,Y3);
                       End;
                       Str(Distance1+Distance2:8:2,TempString);
                       For k := 1 to Length(TempString) do
                           if TempString[k] = ' ' then
                              TempString[k] := '0';
                       if ((X1 > 0) and (X1 < Image.Width-1)) or
                          ((X2 > 0) and (X2 < Image.height-1)) then
                       PolyList.AddObject(TempString,TObject(Poly));
                  End;
                End;
             End;
     end;

     Lev2 := TsketchLevel(project.sketchObject.Levels[0]);
     For i := 0 to lev2.Rooms.count-1 do
     Begin
          Room := TRoom(Lev2.Rooms[i]);
           if Room.RoomType = roomObstruction then
             For j := 0 to Room.Points.Count-2 do
             Begin
                if not(((Room.Points[j] = Seg.a) and (Room.Points[j+1] = Seg.b)) or
                     ((Room.Points[j] = Seg.b) and (Room.Points[j+1] = Seg.a))) then
                Begin
                  Orientation := Seg.Orientation-180;
                  if Orientation < -180 then Orientation := Orientation+360;
                  Cangle1 := FormInputWall.Angle(Center,Room.Points[j])-(pi/2);
                  if Cangle1 < -pi then Cangle1 := Cangle1+2*pi;
                  Cangle1 := Cangle1-(Orientation/180*pi);
                  if Cangle1 < -pi then Cangle1 := Cangle1+2*pi;
                  if Cangle1 > pi then Cangle1 := Cangle1-2*pi;
                  Cangle2 := FormInputWall.Angle(Center,Room.Points[j+1])-(pi/2);
                  if Cangle2 < -pi then Cangle2 := Cangle2+2*pi;
                  CAngle2 := CAngle2-(Orientation/180*pi);
                  if Cangle2 < -pi then Cangle2 := Cangle2+2*pi;
                  if Cangle2 > pi then Cangle2 := Cangle2-2*pi;

                  if ((Cangle1 >= -pi/2) and (CAngle1 <= pi/2)) or
                     ((Cangle2 >= -pi/2) and (CAngle2 <= pi/2)) then
                  Begin
                       X1 := (Image.width div 2)+ Round(Cangle1/pi*Image.width);
                       xCenter := Center.X;
                       yCenter := Center.Y;
                       Distance1 := Sqrt(Sqr(LTpoint(Room.Points[j]).x-xCenter)+
                          Sqr(LTpoint(Room.Points[j]).y-yCenter))/
                          project.sketchObject.scale;
                       if Room.RoomType = roomObstruction then
                       Begin
                          ShadingHeight := Room.Value1-(Lev2.Height/2);
                       end
                       else
                       Begin
                            ShadingHeight := 0;
                            With project.sketchObject do
                              For k := 0 to Levels.count-1 do
                              Begin
                                 Lev := TSketchLevel(levels[k]);
                                 if (Lev.points.Count <> 0) then
                                    ShadingHeight := ShadingHeight+Lev.height;
                              End;
                              ShadingHeight := ShadingHeight-(Lev2.Height/2);
                       End;
                       With project.sketchObject do
                         For k := 0 to CurrentLevel-1 do
                         Begin
                               Lev := TSketchLevel(levels[k]);
                               ShadingHeight := ShadingHeight-Lev.height;
                         End;
                       if ShadingHeight < 0 then shadingHeight := 0;
                       
                       Y1 := Image.height-Round(arctan(ShadingHeight/Distance1)/(pi/2)*Image.Height);
                       X2 := (Image.width div 2)+ Round(Cangle2/pi*Image.width);
                       sq1 := Sqr(LTpoint(Room.Points[j+1]).x-xCenter);
                       sq2 := Sqr(LTpoint(Room.Points[j+1]).y-yCenter);
                       Distance2 := Sqrt(sq1+sq2)/project.sketchObject.scale;
                       Y2 := Image.height-Round(arctan(ShadingHeight/Distance2)/(pi/2)*Image.Height);
                       Poly := TPoly.Create;
                       StepInterPol := Abs(X1-X2)div 5+1;
                       SetLength(Poly.poly, StepInterPol+3);
                       Poly.Poly[0] := Point(X1,Image.height);
                       Poly.Poly[1] := Point(X1,Y1);
                       Poly.Poly[StepInterPol+1] := Point(X2,Y2);
                       Poly.Poly[StepInterPol+2] := Point(X2,Image.height);

                       For interpol := 1 to StepInterpol-1 do
                       Begin
                            VirtualPoint.x :=  ((LTpoint(Room.Points[j+1]).x*Interpol)
                               + (LTpoint(Room.Points[j]).x*(StepInterpol-Interpol)))
                                  div StepInterpol;
                            VirtualPoint.y :=  ((LTpoint(Room.Points[j+1]).y*Interpol)
                               + (LTpoint(Room.Points[j]).y*(StepInterpol-Interpol)))
                                  div StepInterpol;
                            CAngle3 := FormInputWall.Angle(Center,VirtualPoint)-(pi/2);
                            if Cangle3 < -pi then Cangle3 := Cangle3+2*pi;
                            Cangle3 := Cangle3-(Orientation/180*pi);
                            if Cangle3 < -pi then Cangle3 := Cangle3+2*pi;
                            if Cangle3 > pi then Cangle3 := Cangle3-2*pi;
                            X3 := (Image.width div 2)+ Round(Cangle3/pi*Image.width);
                            Distance3 := Sqrt(Sqr(VirtualPoint.x-xCenter)+
                               Sqr(VirtualPoint.y-yCenter))/
                               project.sketchObject.scale;
                            Y3 := Image.height-Round(arctan(ShadingHeight/
                                  Distance3)/(pi/2)*Image.Height);
                            Poly.Poly[1+interpol] := Point(X3,Y3);
                       End;

                       Str(Distance1+Distance2:8:2,TempString);
                       For k := 1 to Length(TempString) do
                           if TempString[k] = ' ' then
                              TempString[k] := '0';
                       if ((X1 > 0) and (X1 < Image.Width-1)) or
                          ((X2 > 0) and (X2 < Image.height-1)) then
                       PolyList.AddObject(TempString,TObject(Poly));
                  End;
                End;
             End;
     end;
     PolyList.Sort;
     Image.Canvas.Pen.color := clblack;
     Image.Canvas.Brush.Color := clSilver;
     Image.Canvas.Brush.Style := bsSolid;
     For i := PolyList.Count-1 downto 0 do
     Begin
          Image.Canvas.Polygon(TPoly(PolyList.Objects[i]).poly);
          TPoly(PolyList.Objects[i]).Poly := nil;
          TPoly(PolyList.Objects[i]).Free;
     End;

     Image.Canvas.MoveTo(Image.Width div 6,0);
     Image.Canvas.Pen.Width := 1;
     Image.Canvas.Pen.Style := psdot;
     Image.Canvas.Pen.Color := clBlack;
     Image.Canvas.Brush.Style := bsClear;
     Image.Canvas.LineTo(Image.Width div 6,Image.Height);
     Image.Canvas.MoveTo(Image.Width*5 div 6,0);
     Image.Canvas.LineTo(Image.Width*5 div 6,Image.Height);
     Image.Canvas.Pen.Style := psSolid;

     UHA := CalculateUHA(seg,Street);
     //With Image.Canvas do
     //Begin
     //     Pen.color := CLBlack;
     //     Pen.Width := 3;
     //     MoveTo(0,Round(Image.Height*(1-UHA)));
     //     LineTo(Image.Width,Round(Image.Height*(1-UHA)));
     //      Pen.Width := 1;
     //End;
     Center.Free;
     PolyList.Free;
End;

Function TFormInputWall.Angle(P1,P2 : LTpoint):Double;
Var
   X,Y,Angle1,SegLength : Double;
Begin
     Angle := 0;
     if P1 <> P2 then
     Begin
          X := P2.x-P1.x;
          Y := P2.y-P1.y;
          SegLength := Sqrt(sqr(X)+Sqr(Y));
          X := x / SegLength;
          y := y /SegLength;
          Angle1 := ArcCos(X);
          if Y < 0 then
             Angle := -Angle1
          else
             Angle := Angle1;
     End
End;

procedure TFormInputWall.B(Sender: TObject);
Var
   Reponse : Word;
begin
     //bibCompositions.poseTousDansArbre(FormCompo.TreeCompo,true, true, true);
     FormCompo.BitBtn1.Enabled := False;
     Reponse := FormCompo.ShowModal;
     If Reponse = mrOk then
     Begin
          Seg.Composition := TComposition(FormCompo.TreeCompo.Selected.data);
          StaticComposition.Caption := Seg.Composition.nom;
     End
     else if Reponse = mrIgnore then
     begin
          Seg.Composition := Nil;
          StaticComposition.Caption := Labels[234];
     end
end;

procedure TFormInputWall.Button1Click(Sender: TObject);
begin
     Seg.Composition := Nil;
     StaticComposition.Caption := Labels[234];
end;

end.
