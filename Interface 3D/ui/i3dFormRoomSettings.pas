unit i3dFormRoomSettings;

interface

uses
  Windows,
  Messages,
  SysUtils,
  Classes,
  Graphics,
  Controls,
  Forms,
  Dialogs,
  StdCtrls,
  Buttons,
  i3dGeometry,
  compositions;


type
  TFormRoom = class(TForm)
    GroupBox1: TGroupBox;
    Label1: TLabel;
    EditName: TEdit;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    GroupBox2: TGroupBox;
    BitBtn3: TBitBtn;
    StaticCompoceiling: TStaticText;
    Button1: TButton;
    RadioEtudie: TRadioButton;
    RadioPatio: TRadioButton;
    RadioLocal: TRadioButton;
    Label8: TLabel;
    Label9: TLabel;
    Label2: TLabel;
    ComboSur: TComboBox;
    ComboTau: TComboBox;
    EditTau: TEdit;
    StaticcompoFloor: TStaticText;
    BitBtn4: TBitBtn;
    Label3: TLabel;
    Button2: TBitBtn;
    procedure BitBtn4Click(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure RadioEtudieClick(Sender: TObject);
    procedure ComboSurChange(Sender: TObject);
    procedure ComboTauChange(Sender: TObject);
  private
    { Déclarations privées }
  public
    room: TRoom;
    { Déclarations publiques }
  end;

var
  FormRoom: TFormRoom;

implementation

uses
  i3dTranslation,
  i3dFormChoixCompo,
  i3dFormChoixTypeLocal;
{$R *.DFM}

procedure TFormRoom.BitBtn4Click(Sender: TObject);
Var
  Reponse: Word;
begin
  // bibCompositions.PoseTousDansArbre(FormCompo.TreeCompo,true, true, true);
  FormCompo.BitBtn1.Enabled := False;
  Reponse := FormCompo.ShowModal;
  If Reponse = mrOk then
  Begin
    room.CompoFloor := TComposition(FormCompo.TreeCompo.Selected.data);
    StaticcompoFloor.Caption := room.CompoFloor.nom;
  End
  else if Reponse = mrIgnore then
  begin
    room.CompoFloor := Nil;
    StaticcompoFloor.Caption := Labels[234];
  end
end;

procedure TFormRoom.BitBtn3Click(Sender: TObject);
Var
  Reponse: Word;
begin
  // bibCompositions.PoseTousDansArbre(FormCompo.TreeCompo,true, true, true);
  FormCompo.BitBtn1.Enabled := False;
  Reponse := FormCompo.ShowModal;
  If Reponse = mrOk then
  Begin
    room.CompoCeiling := TComposition(FormCompo.TreeCompo.Selected.data);
    StaticCompoceiling.Caption := room.CompoCeiling.nom;
  End
  else if Reponse = mrIgnore then
  begin
    room.CompoCeiling := Nil;
    StaticCompoceiling.Caption := Labels[234];
  end
end;

procedure TFormRoom.Button2Click(Sender: TObject);
begin
  room.CompoCeiling := Nil;
  StaticcompoFloor.Caption := Labels[234];
end;

procedure TFormRoom.Button1Click(Sender: TObject);
begin
  room.CompoCeiling := Nil;
  StaticCompoceiling.Caption := Labels[234];
end;

procedure TFormRoom.RadioEtudieClick(Sender: TObject);
begin
  Label1.Enabled := RadioEtudie.Checked;
  EditName.Enabled := RadioEtudie.Checked;
  Label8.Enabled := RadioLocal.Checked;
  Label9.Enabled := RadioLocal.Checked;
  Label2.Enabled := RadioLocal.Checked;
  ComboSur.Enabled := RadioLocal.Checked;
  ComboTau.Enabled := RadioLocal.Checked;
  EditTau.Enabled := RadioLocal.Checked;
end;

procedure TFormRoom.ComboSurChange(Sender: TObject);
Var
  i, j: Integer;
  Chaine: String;
  Sur: String;
begin
  Chaine := ComboSur.text;
  if Chaine <> '' then
    For i := 1 to 100 do
    Begin
      if FormLocal.Taus[2, i] = Chaine then
      Begin
        Sur := FormLocal.Taus[1, i];
        j := i + 1;
        ComboTau.items.Clear;
        While (j <= 100) and (FormLocal.Taus[1, j] = '') do
        Begin
          if FormLocal.Taus[3, j] <> '' then
            ComboTau.items.Add(FormLocal.Taus[3, j]);
          inc(j);
        End;
        ComboTau.ItemIndex := 0;
        ComboTauChange(Nil);
(*
        if ComboTau.items.count = 1 then
        Begin
          ComboTau.ItemIndex := 0;
          ComboTauChange(Nil);
        End;
*)
      End;
    End;
end;

procedure TFormRoom.ComboTauChange(Sender: TObject);
Var
  i, erreur: Integer;
  Valeur: Double;
  Chaine, Chaine2: String;
  TTau: String;
  Tau: Double;
begin
  For i := 1 to 100 do
  Begin
    if FormLocal.Taus[1, i] <> '' then
    Begin
      Chaine := FormLocal.Taus[2, i];
      Chaine2 := '';
    End
    else
      Chaine2 := FormLocal.Taus[3, i];
    if (Chaine = ComboSur.text) and (Chaine2 = ComboTau.text) then
    Begin
      if FormLocal.Taus[4, i] <> '' then
      Begin
        TTau := ComboTau.text;
        EditTau.text := FormLocal.Taus[4, i];
        EditTau.color := clbtnface;
        EditTau.readonly := True;
        Val(EditTau.text, Valeur, erreur);
        Tau := Valeur;
      End
      else
      Begin
        TTau := ComboTau.text;
        EditTau.color := clWindow;
        EditTau.readonly := False;
      End;
    End;
  End;
end;

end.
