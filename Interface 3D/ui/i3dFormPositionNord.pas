unit i3dFormPositionNord;

interface

uses
  Windows, Vcl.StdCtrls, Vcl.Buttons, Vcl.Samples.Spin, Vcl.Graphics,
  Vcl.ExtCtrls, Vcl.Controls, System.Classes, Vcl.Forms;

type
  TFormNorth = class(TForm)
    GroupBox1: TGroupBox;
    Label1: TLabel;
    SpinEdit1: TSpinEdit;
    Label2: TLabel;
    Image1: TImage;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    procedure SpinEdit1Change(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Déclarations privées }
  public
    { Déclarations publiques }
  end;

var
  FormNorth: TFormNorth;

implementation

uses
  Sketch,
  i3dSketchObject;

{$R *.DFM}

procedure TFormNorth.SpinEdit1Change(Sender: TObject);
begin
     Project.sketchObject.Refresh();
     FormSketch.ImageSketch.Repaint;
end;

procedure TFormNorth.FormCreate(Sender: TObject);
begin
     // Don't delete the following line, it is needed if you modify
     // the interface. Comment the two previous lines and uncomment this one
     // instead to refresh the component file for translation.
     //ListConstruction;
     //translate(Langue);
end;

end.
