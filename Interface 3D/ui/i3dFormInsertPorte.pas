unit i3dFormInsertPorte;

interface

uses
  Windows,
  Messages,
  SysUtils,
  Classes,
  Graphics,
  Controls,
  Forms,
  Dialogs,
  StdCtrls,
  Buttons,
  Menuiseries;

type
  TFormPorte = class(TForm)
    GroupBox1: TGroupBox;
    Label18: TLabel;
    EditDoorH: TEdit;
    Label14: TLabel;
    Label16: TLabel;
    EditDoorW: TEdit;
    Label12: TLabel;
    BitBtn7: TBitBtn;
    StaticCompoDoor: TStaticText;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    BitBtnMove: TBitBtn;
    BitBtn3: TBitBtn;
    procedure BitBtn7Click(Sender: TObject);
    procedure EditDoorHChange(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
  private
    { Déclarations privées }
  public
    Largeur,Hauteur : Double;
    CompoPorte : TMenuiseries;
    { Déclarations publiques }
  end;

var
  FormPorte: TFormPorte;

implementation

uses
  i3dTranslation,
  i3dFormChoixFenetre;

{$R *.DFM}

procedure TFormPorte.BitBtn7Click(Sender: TObject);
Var
   Reponse : Word;
begin
     //RemplirArbreMenuiseries(FormFen.TreeFen,FormSketch.Pleiades.CategorieFen,
     //    FormSketch.Pleiades.BiblioMenuiseries);
     FormFen.BitBtn1.Enabled := False;
     Reponse := FormFen.ShowModal;
     If Reponse = mrOk then
     Begin
          CompoPorte := TMenuiseries(FormFen.TreeFen.Selected.data);
          StaticCompoDoor.Caption := CompoPorte.nom;
          EditDoorHChange(Nil);
     End
     else if Reponse = mrIgnore then
     begin
          CompoPorte := Nil;
          StaticCompoDoor.Caption := '';
          EditDoorHChange(Nil);
     end
end;

procedure TFormPorte.EditDoorHChange(Sender: TObject);
Var
   Erreur1,Erreur2 : Integer;
begin
     Val(EditDoorH.Text,Hauteur,Erreur1);
     Val(EditDoorW.Text,Largeur,Erreur2);
     BitBtn1.enabled := (Erreur1 = 0) and (Erreur2 = 0) and
          (StaticCompoDoor.Caption <> ''); 
end;

procedure TFormPorte.BitBtn3Click(Sender: TObject);
begin
          CompoPorte := Nil;
          StaticCompoDoor.Caption := Labels[234];
          EditDoorHChange(Nil);
end;

end.
