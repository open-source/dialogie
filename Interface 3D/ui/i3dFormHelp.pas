unit i3dFormHelp;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls;

type
  TFormHelp = class(TForm)
    LabelH: TGroupBox;
    ImageHelp: TImage;
    procedure FormCreate(Sender: TObject);
    procedure ImageHelpClick(Sender: TObject);
  private
    { Déclarations privées }
  public
    { Déclarations publiques }
  end;

var
  FormHelp: TFormHelp;

implementation

uses Sketch;

{$R *.DFM}

procedure TFormHelp.FormCreate(Sender: TObject);
begin
     FormSketch.StatusBar.Panels[1].text := 'Step 1';
     FormSketch.StatusBar.Repaint;
     ImageHelp.Picture.bitmap := TBitmap.Create;
     ImageHelp.Picture.bitmap.pixelFormat := pfDevice;
     FormSketch.StatusBar.Panels[1].text := 'Step 2';
     FormSketch.StatusBar.Repaint;
     ImageHelp.Picture.bitmap.width := ImageHelp.Width;
     FormSketch.StatusBar.Panels[1].text := 'Step 3';
     FormSketch.StatusBar.Repaint;
     ImageHelp.Picture.bitmap.height := ImageHelp.height;
     FormSketch.StatusBar.Panels[1].text := 'Step 4';
     FormSketch.StatusBar.Repaint;
end;

procedure TFormHelp.ImageHelpClick(Sender: TObject);
begin
     FormHelp.Hide;
end;

end.
