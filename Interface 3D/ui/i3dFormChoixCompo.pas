unit i3dFormChoixCompo;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, ComCtrls, Compositions;

type
  TFormCompo = class(TForm)
    Panel1: TPanel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    TreeCompo: TTreeView;
    BitBtn3: TBitBtn;
    labInfo: TLabel;
    procedure TreeCompoClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure TreeCompoDblClick(Sender: TObject);
  private
  public
    procedure chooseCompo(var compo: TComposition; infoString: string = '');
  end;

var
  FormCompo: TFormCompo;

implementation

uses
  BiblioCompositions;

{$R *.DFM}

procedure TFormCompo.FormCreate(Sender: TObject);
begin
  bibCompositions.poseTousDansArbre(TreeCompo,true, true, true);
end;

procedure TFormCompo.TreeCompoClick(Sender: TObject);
var
  selNode : TTreeNode;
begin
  selNode := TreeCompo.Selected;
  BitBtn1.enabled := (selNode <> nil) and (selNode.Parent <> nil);
end;


procedure TFormCompo.TreeCompoDblClick(Sender: TObject);
begin
  TreeCompoClick(sender);
  if BitBtn1.Enabled then
  begin
    ModalResult := mrOk;
    CloseModal;
  end;
end;

procedure TFormCompo.chooseCompo(var compo: TComposition ; infoString: string = '');
var
  Reponse: word;
begin
  //bibCompositions.poseTousDansArbre(FormCompo.TreeCompo, true, true, true);
  BitBtn1.Enabled := False;
  labInfo.visible := infoString <> '';
  labInfo.Caption := infoString;
  Reponse := ShowModal;
  if Reponse = mrOk then
  begin
    compo := TComposition(TreeCompo.Selected.Data);
  end
  else if Reponse = mrIgnore then
  begin
    compo := Nil;
  end
end;


end.
