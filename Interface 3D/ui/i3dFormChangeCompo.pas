unit i3dFormChangeCompo;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, compositions;

type
  TFormChg = class(TForm)
    Label1: TLabel;
    StaticCompoExt: TStaticText;
    BitBtn1: TBitBtn;
    Label2: TLabel;
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
  private
    { Déclarations privées }
  public
    { Déclarations publiques }
    Compo : TComposition;
  end;

var
  FormChg: TFormChg;

implementation

uses i3dFormChoixCompo;

{$R *.DFM}

procedure TFormChg.BitBtn1Click(Sender: TObject);
var
  Reponse: word;
begin
//  bibCompositions.poseTousDansArbre(FormCompo.TreeCompo, true, true, true);
  FormCompo.BitBtn1.Enabled := False;
  Reponse := FormCompo.ShowModal;
  if Reponse = mrOk then
  begin
    Compo := TComposition(FormCompo.TreeCompo.Selected.Data);
    StaticCompoExt.Caption := Compo.nom;
  end
  else if Reponse = mrIgnore then
  begin
    Compo := Nil;
    StaticCompoExt.Caption := '';
  end
end;

procedure TFormChg.BitBtn2Click(Sender: TObject);
begin
     Close;
end;

end.
