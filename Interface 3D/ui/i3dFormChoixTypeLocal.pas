unit i3dFormChoixTypeLocal;

interface

uses
  Windows,
  Messages,
  SysUtils,
  Classes,
  Graphics,
  Controls,
  Forms,
  Dialogs,
  StdCtrls,
  Buttons,
  i3dGeometry;


type
  TFormLocal = class(TForm)
    ComboSur: TComboBox;
    ComboTau: TComboBox;
    Label8: TLabel;
    Label9: TLabel;
    EditTau: TEdit;
    Label1: TLabel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    procedure ComboSurChange(Sender: TObject);
    procedure ComboTauChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);

  private
    { D�clarations priv�es }
  public
    { D�clarations publiques }
    Taus: Array [1 .. 4, 1 .. 100] of String;

    procedure setRoom(r : TRoom);

  end;

var
  FormLocal: TFormLocal;

implementation

uses
  i3dConstants,
  IzConstantes,
  i3dFormRoomSettings,
  UtilitairesDialogie;
{$R *.DFM}

procedure TFormLocal.ComboSurChange(Sender: TObject);
Var
  i, j: Integer;
  Chaine: String;
  Sur: String;
begin
//� remplacer par
//procedure TFormListeFenetres.ComboSurChange(Sender: TObject);

  Chaine := ComboSur.text;
  if Chaine <> '' then
    For i := 1 to 100 do
    Begin
      if Taus[2, i] = Chaine then
      Begin
        Sur := Taus[1, i];
        j := i + 1;
        ComboTau.items.Clear;
        While (j <= 100) and (Taus[1, j] = '') do
        Begin
          if Taus[3, j] <> '' then
            ComboTau.items.Add(Taus[3, j]);
          inc(j);
        End;
        ComboTau.ItemIndex := 0;
        ComboTauChange(Nil);
(*
        if ComboTau.items.count = 1 then
        Begin
          ComboTau.ItemIndex := 0;
          ComboTauChange(Nil);
        End
*)
      End;
    End;
end;

procedure TFormLocal.ComboTauChange(Sender: TObject);
Var
  i, erreur: Integer;
  Valeur: Double;
  Chaine, Chaine2: String;
  TTau: String;
  Tau: Double;
begin
//� remplacer par
//procedure TFormListeFenetres.ComboTauChange(Sender: TObject);

  For i := 1 to 100 do
  Begin
    if Taus[1, i] <> '' then
    Begin
      Chaine := Taus[2, i];
      Chaine2 := '';
    End
    else
      Chaine2 := Taus[3, i];
    if (Chaine = ComboSur.text) and (Chaine2 = ComboTau.text) then
    Begin
      if Taus[4, i] <> '' then
      Begin
        TTau := ComboTau.text;
        EditTau.text := Taus[4, i];
        EditTau.color := clbtnface;
        EditTau.readonly := True;
        Val(EditTau.text, Valeur, erreur);
        Tau := Valeur;
      End
      else
      Begin
        TTau := ComboTau.text;
        EditTau.color := clWindow;
        EditTau.readonly := False;
      End;
    End;
  End;
end;

procedure TFormLocal.FormCreate(Sender: TObject);
Var
  Temp: TStringList;
  i: Integer;
  ligne, Chaine: String;
begin
  Temp := TStringList.Create;
  Temp.LoadFromfile(dialogieLibPath + 'Tau.txt');
  For i := 1 to Temp.count do
  Begin
    ligne := Temp[i - 1];

    Chaine := Copy(ligne, 1, Pos(CHAR_TAB, ligne) - 1);
    Taus[1, i] := Chaine;
    Delete(ligne, 1, Pos(CHAR_TAB, ligne));

    Chaine := Copy(ligne, 1, Pos(CHAR_TAB, ligne) - 1);
    Taus[2, i] := Chaine;
    Delete(ligne, 1, Pos(CHAR_TAB, ligne));
    Delete(ligne, 1, Pos(CHAR_TAB, ligne));

    Chaine := Copy(ligne, 1, Pos(CHAR_TAB, ligne) - 1);
    Taus[3, i] := Chaine;
    Delete(ligne, 1, Pos(CHAR_TAB, ligne));
    Delete(ligne, 1, Pos(CHAR_TAB, ligne));
    Delete(ligne, 1, Pos(CHAR_TAB, ligne));

    Taus[4, i] := ligne;
  End;
  For i := 1 to 100 do
  Begin
    Chaine := Taus[2, i];
    if Chaine <> '' then
    Begin
      ComboSur.items.Add(Chaine);
      FormRoom.ComboSur.items.Add(Chaine);
    End;
  End;
end;

procedure TFormLocal.setRoom(r: TRoom);
begin
  ComboSur.itemIndex := ComboSur.Items.IndexOf(r.sur);
  if ComboSur.itemIndex = -1 then
    ComboSur.itemIndex := 0;
  ComboSurChange(nil);
  ComboTau.itemIndex := ComboTau.Items.IndexOf(r.Ttau);
  if ComboTau.itemIndex = -1 then
    ComboTau.itemIndex := 0;
  ComboTauChange(nil);
end;

end.
