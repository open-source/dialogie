unit i3dFormInsertFenetre;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons,Menuiseries;

type
  TFormFenetre = class(TForm)
    GroupBox1: TGroupBox;
    Label18: TLabel;
    EditFenH: TEdit;
    Label14: TLabel;
    Label16: TLabel;
    EditFenW: TEdit;
    Label12: TLabel;
    BitBtn7: TBitBtn;
    StaticCompoFen: TStaticText;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    BitBtnMove: TBitBtn;
    BitBtn3: TBitBtn;
    Label1: TLabel;
    EditFenAllege: TEdit;
    Label2: TLabel;
    Label3: TLabel;
    EditRetrait: TEdit;
    Label4: TLabel;
    procedure BitBtn7Click(Sender: TObject);
    procedure EditFenHChange(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
  private
    { Déclarations privées }
  public
    Largeur,Hauteur,Allege,Retrait : Double;
    CompoFenetre : TMenuiseries;
    { Déclarations publiques }
  end;

var
  FormFenetre: TFormFenetre;

implementation

uses
  i3dTranslation,
  i3dFormChoixFenetre;

{$R *.DFM}

procedure TFormFenetre.BitBtn7Click(Sender: TObject);
Var
   Reponse : Word;
begin
     //RemplirArbreMenuiseries(FormFen.TreeFen,FormSketch.Pleiades.CategorieFen,
     //    FormSketch.Pleiades.BiblioMenuiseries);
     FormFen.BitBtn1.Enabled := False;
     Reponse := FormFen.ShowModal;
     If Reponse = mrOk then
     Begin
          CompoFenetre := TMenuiseries(FormFen.TreeFen.Selected.data);
          StaticCompoFen.Caption := CompoFenetre.nom;
          EditFenHChange(Nil);
     End
     else if Reponse = mrIgnore then
     begin
          CompoFenetre := Nil;
          StaticCompoFen.Caption := '';
          EditFenHChange(Nil);
     end
end;

procedure TFormFenetre.EditFenHChange(Sender: TObject);
Var
   Erreur1,Erreur2,Erreur3 : Integer;
begin
     Val(EditFenH.Text,Hauteur,Erreur1);
     Val(EditFenW.Text,Largeur,Erreur2);
     Val(EditFenAllege.Text,Allege,Erreur3);
     Val(EditRetrait.Text,Retrait,Erreur3);
     BitBtn1.enabled := (Erreur1 = 0) and (Erreur2 = 0)and (Erreur3 = 0) and
          (StaticCompoFen.Caption <> '');
end;

procedure TFormFenetre.BitBtn3Click(Sender: TObject);
begin
     CompoFenetre := Nil;
     StaticCompoFen.Caption := Labels[234];
     EditFenHChange(Nil);
end;

end.
