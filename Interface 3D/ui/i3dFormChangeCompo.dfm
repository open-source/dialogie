object FormChg: TFormChg
  Left = 192
  Top = 104
  BorderStyle = bsToolWindow
  Caption = 'Changement de composition'
  ClientHeight = 65
  ClientWidth = 365
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 36
    Width = 129
    Height = 25
    AutoSize = False
    Caption = 'Nouvelle composition'
    Layout = tlCenter
  end
  object Label2: TLabel
    Left = 8
    Top = 8
    Width = 305
    Height = 13
    Caption = 'S'#233'lectionnez une composition et cliquez sur les parois '#224' changer'
  end
  object StaticCompoExt: TStaticText
    Left = 152
    Top = 36
    Width = 145
    Height = 25
    Alignment = taCenter
    AutoSize = False
    BorderStyle = sbsSunken
    TabOrder = 0
  end
  object BitBtn1: TBitBtn
    Left = 306
    Top = 36
    Width = 57
    Height = 25
    Caption = 'Selection'
    TabOrder = 1
    OnClick = BitBtn1Click
  end
end
