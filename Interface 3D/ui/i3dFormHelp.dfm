object FormHelp: TFormHelp
  Left = 377
  Top = 274
  BorderStyle = bsToolWindow
  BorderWidth = 2
  Caption = 'Assistant'
  ClientHeight = 149
  ClientWidth = 509
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object LabelH: TGroupBox
    Left = 0
    Top = 0
    Width = 509
    Height = 149
    Align = alClient
    Caption = 'Aide'
    Color = clBtnHighlight
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 0
    object ImageHelp: TImage
      Left = 2
      Top = 18
      Width = 505
      Height = 129
      OnClick = ImageHelpClick
    end
  end
end
