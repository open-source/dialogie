unit i3dFormChoixFenetre;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, ComCtrls, Menuiseries;


type
  TFormFen = class(TForm)
    Panel1: TPanel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    TreeFen: TTreeView;
    BitBtn3: TBitBtn;
    labInfo: TLabel;
    procedure TreeFenClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure TreeFenDblClick(Sender: TObject);
  private
    Procedure InitialiseFenetres;
  public
    procedure chooseFen(var fen: TMenuiseries; infoString: string = '');
  end;

var
  FormFen: TFormFen;

implementation

uses
  BiblioMenuiseries;
{$R *.DFM}

procedure TFormFen.InitialiseFenetres;
begin
  // bibMenuiseries.poseTousDansArbre(TreeFen, true, true, true);

  (*
    Var
    Liste : TStringList;
    i : Integer;
    Fen : TMenuiseries;
    Noeud : TTreeNode;
    Begin
    ListeFen := TList.Create;
    Liste := TStringList.Create;
    Liste.loadFromfile(libDataPath +FichierMenuiseriesS);
    For i := 0 to Liste.count-1 do
    Begin
    Fen := TMenuiseries.Create;
    Fen.Lire(Liste[i],i);
    ListeFen.Add(Fen);
    Fen.Id := ListeFen.Count;
    Noeud := FormFen.TreeFen.Items.addObject(Nil,Fen.Nom,Fen);
    if not(Fen.isDoor) then
    Begin
    if Fen.LectureSeule then
    begin
    Noeud.ImageIndex := 32;
    Noeud.SelectedIndex := 32;
    end
    else
    begin
    Noeud.ImageIndex := 30;
    Noeud.SelectedIndex := 30;
    end
    End
    Else
    Begin
    if Fen.LectureSeule then
    begin
    Noeud.ImageIndex := 106;
    Noeud.SelectedIndex := 107;
    end
    else
    begin
    Noeud.ImageIndex := 104;
    Noeud.SelectedIndex := 105;
    end
    End;
    //ListBoxFen.items.Add(Fen.nom);
    end;
    if FileExists(libDataPath + FichierMenuiseriesNS) then
    Begin
    Liste.loadFromfile(libDataPath + FichierMenuiseriesNS);
    For i := 0 to Liste.count-1 do
    Begin
    Fen := TMenuiseries.Create;
    Fen.Lire(Liste[i],i);
    ListeFen.Add(Fen);
    Fen.Id := ListeFen.Count;
    Noeud := FormFen.TreeFen.Items.addObject(Nil,Fen.Nom,Fen);
    if not(Fen.isDoor) then
    Begin
    if Fen.LectureSeule then
    begin
    Noeud.ImageIndex := 32;
    Noeud.SelectedIndex := 32;
    end
    else
    begin
    Noeud.ImageIndex := 30;
    Noeud.SelectedIndex := 30;
    end
    End
    Else
    Begin
    if Fen.LectureSeule then
    begin
    Noeud.ImageIndex := 106;
    Noeud.SelectedIndex := 107;
    end
    else
    begin
    Noeud.ImageIndex := 104;
    Noeud.SelectedIndex := 105;
    end
    End;
    end;
    End;
  *)
end;

procedure TFormFen.TreeFenClick(Sender: TObject);
var
  selNode : TTreeNode;
begin
  selNode := TreeFen.Selected;
  BitBtn1.enabled := (selNode <> nil) and (selNode.Parent <> nil);
end;

procedure TFormFen.TreeFenDblClick(Sender: TObject);
begin
  TreeFenClick(sender);
  if BitBtn1.Enabled then
  begin
    ModalResult := mrOk;
    CloseModal;
  end;
end;

procedure TFormFen.FormCreate(Sender: TObject);
begin
  // InitialiseFenetres;
  bibMenuiseries.poseTousDansArbre(TreeFen, true, true, true);
end;


procedure TFormFen.chooseFen(var fen: TMenuiseries; infoString: string = '');
var
  Reponse: word;
begin
  BitBtn1.enabled := False;
  labInfo.visible := infoString <> '';
  labInfo.Caption := infoString;
  Reponse := ShowModal;
  if Reponse = mrOk then
  begin
    fen := TMenuiseries(TreeFen.Selected.Data);
  end
  else if Reponse = mrIgnore then
  begin
    fen := Nil;
  end
end;

end.
