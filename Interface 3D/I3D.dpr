program I3D;

uses
  Forms,
  DIB in 'delphiX\DIB.pas',
  DirectX in 'delphiX\DirectX.pas',
  DXClass in 'delphiX\DXClass.pas',
  DXConsts in 'delphiX\DXConsts.pas',
  DXDraws in 'delphiX\DXDraws.pas',
  DXSprite in 'delphiX\DXSprite.pas',
  D3DUtils in 'delphiX\D3DUtils.pas',
  DXRender in 'delphiX\DXRender.pas',
  Sketch in 'Sketch.pas' {FormSketch},
  i3dConstants in 'objects\i3dConstants.pas',
  i3dFormInputWall in 'ui\i3dFormInputWall.pas' {FormInputWall},
  i3dGeometry in 'objects\i3dGeometry.pas',
  i3dSketchObject in 'objects\sketch\i3dSketchObject.pas',
  i3dSketchLevel in 'objects\sketch\i3dSketchLevel.pas',
  i3dSketchAnalysis in 'objects\sketch\i3dSketchAnalysis.pas',
  i3dFormHelp in 'ui\i3dFormHelp.pas' {FormHelp},
  i3dBuildingData in 'objects\i3dBuildingData.pas',
  i3dFormChoixCompo in 'ui\i3dFormChoixCompo.pas' {FormCompo},
  i3dFormChoixFenetre in 'ui\i3dFormChoixFenetre.pas' {FormFen},
  i3dFormRoomSettings in 'ui\i3dFormRoomSettings.pas' {FormRoom},
  i3dFormInsertPorte in 'ui\i3dFormInsertPorte.pas' {FormPorte},
  i3dFormPositionNord in 'ui\i3dFormPositionNord.pas' {FormNorth},
  i3dPolyUtil in 'objects\i3dPolyUtil.pas',
  i3dUnit3D in 'objects\i3dUnit3D.pas',
  i3dToiture in 'objects\i3dToiture.pas',
  i3dFormChangeCompo in 'ui\i3dFormChangeCompo.pas' {FormChg},
  i3dFormRoofSettings in 'ui\i3dFormRoofSettings.pas' {FormRoof},
  i3dDialogieExport in 'objects\i3dDialogieExport.pas',
  IzApp in '..\objects\IzApp.pas',
  UtilitairesDialogie in '..\util\UtilitairesDialogie.pas',
  IzConstantes in '..\util\IzConstantes.pas',
  IzConstantesConso in '..\library-manager\commun\calculConso\util\IzConstantesConso.pas',
  Enregistrement in '..\ui\Enregistrement.pas' {FormEnregistrement},
  BiblioCompositions in '..\library-manager\Objets\Compositions\BiblioCompositions.pas',
  Compositions in '..\library-manager\Objets\Compositions\Compositions.pas',
  ComposantBibliotheque in '..\library-manager\Objets\ComposantBib\ComposantBibliotheque.pas',
  GestionBib in '..\library-manager\Objets\GestionBib\GestionBib.pas',
  gestionSauvegarde in '..\objects\gestionSauvegarde.pas',
  creerSauvegarde in '..\objects\creerSauvegarde.pas',
  ListeGenerique in '..\objects\ListeGenerique.pas',
  Materiaux in '..\library-manager\Objets\Materiaux\Materiaux.pas',
  Elements in '..\library-manager\Objets\Elements\Elements.pas',
  BiblioMateriaux in '..\library-manager\Objets\Materiaux\BiblioMateriaux.pas',
  BiblioElements in '..\library-manager\Objets\Elements\BiblioElements.pas',
  DialogsInternational in '..\util\DialogsInternational.pas',
  Menuiseries in '..\library-manager\Objets\Menuiseries\Menuiseries.pas',
  BiblioMenuiseries in '..\library-manager\Objets\Menuiseries\BiblioMenuiseries.pas',
  IzUtilitaires in '..\util\IzUtilitaires.pas',
  IzFormLog in '..\ui\util\IzFormLog.pas' {FormLog},
  IzObjectPersist in '..\objects\model\IzObjectPersist.pas',
  IzObject in '..\objects\model\IzObject.pas',
  i3dTranslation in 'objects\i3dTranslation.pas',
  XmlUtil in '..\util\XmlUtil.pas',
  i3dUtil in 'objects\i3dUtil.pas',
  i3dFormInsertFenetre in 'ui\i3dFormInsertFenetre.pas' {FormFenetre},
  i3dFormChoixTypeLocal in 'ui\i3dFormChoixTypeLocal.pas' {FormLocal},
  FProgress in '..\ui\util\FProgress.pas' {FormProgress},
  BindStatusCallBack in '..\util\BindStatusCallBack.pas',
  i3dFormProp in 'ui\i3dFormProp.pas' {FormProp},
  IzUiUtil in '..\util\IzUiUtil.pas' {UiUtilForm},
  IzSysUtil in '..\util\IzSysUtil.pas';

{$R *.RES}

begin
  Application.Initialize;
  Application.Title := TFormSketch.getAppVersionLabel;
  Application.CreateForm(TFormSketch, FormSketch);
  Application.CreateForm(TFormInputWall, FormInputWall);
  Application.CreateForm(TFormHelp, FormHelp);
  Application.CreateForm(TFormCompo, FormCompo);
  Application.CreateForm(TFormFen, FormFen);
  Application.CreateForm(TFormRoom, FormRoom);
  Application.CreateForm(TFormPorte, FormPorte);
  Application.CreateForm(TFormChg, FormChg);
  Application.CreateForm(TFormNorth, FormNorth);
  Application.CreateForm(TFormRoof, FormRoof);
  Application.CreateForm(TFormEnregistrement, FormEnregistrement);
  Application.CreateForm(TFormLog, FormLog);
  Application.CreateForm(TFormFenetre, FormFenetre);
  Application.CreateForm(TFormLocal, FormLocal);
  Application.CreateForm(TFormProgress, FormProgress);
  Application.CreateForm(TFormProp, FormProp);
  Application.CreateForm(TUiUtilForm, UiUtilForm);
  Application.Run;
end.
