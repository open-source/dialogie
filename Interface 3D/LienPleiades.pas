unit LienPleiades;

interface

uses
  Classes,
  Windows,
  Forms,
  Compositions,
  i3dFormChoixCompo,
  Menuiseries,
  i3dTranslation,
  Materiaux,
  Elements,
  Sysutils,
  i3dGeometry,
  i3dSketchLevel,
  dialogs,
  Graphics,
  Math,
  Controls,
  IzObjectPersist,
  Xml.XMLIntf;


type
  TP3D = record
    x, y, z: double;
  end;

  TParoiTemp = class
    Surface, Hauteur, Largeur: double;
    Orientation, Inclinaison: integer;
  end;

  TBuildingData = class(TObjectPersist)
    private
      BiblioEtat: TList;
      NumStandard: integer;
      IdParoiAbs: integer;
      IdFen: integer;

      procedure ExporteParois(Room: TRoom; Sauvegarde: TStringList; Log: TStrings);
      procedure DessinePlancher(Can: TCanvas; GridType: integer; Rect: TRect; DrawLegend: boolean; Tx, Ty: integer; Room: TRoom);

      procedure innerLoadXml(node : IXMLNode); override;
      procedure innerSaveXml(node : IXMLNode); override;
    protected

    public

      compoExt, compoInt, compoFloor, compoIntFloor, compoRoof: TComposition;
      CompoWindow, CompoDoor: TMenuiseries;

      WindowWidth, WindowHeight, DoorWidth, DoorHeight: double;
      CrawlSpace, VentilatedLoft: boolean;

      ListePlancher: TList;

      constructor Create;override;
      destructor Destroy;override;
      procedure SaveToList(List: TStringList);
      procedure LoadFromList(List: TStringList; var Index: integer);

      function GetFenParID(ID: integer): TMenuiseries;
      function GetCompositionParID(Identificateur: integer): TComposition;
      function GetMatParID(Identificateur: integer): TMateriau;
      function GetElemParID(Identificateur: integer): TElement;

      procedure PrepareShed;
      procedure PreparationPlancherIntermediaire(Log: TStrings);

      function CorrigeHauteurP(p, p2: LTPoint; Room: TRoom): double;

  end;

var
  buildingData: TBuildingData;


function CompareCompo(Item1, Item2: Pointer): integer;

implementation

uses
  i3dConstants,
  i3dSketchObject,
  i3dFormChoixFenetre,
  IzApp,
  BiblioMenuiseries,
  UtilitairesDialogie,
  IzConstantes,
  IzConstantesConso,
  BiblioMateriaux,
  BiblioCompositions,
  BiblioElements,
  XmlUtil,
  i3dUtil;


function TBuildingData.GetMatParID(Identificateur: integer): TMateriau;
begin
  result := bibMateriaux.get(identificateur);
end;


function TBuildingData.GetCompositionParID(Identificateur: integer): TComposition;
begin
  result := bibCompositions.get(identificateur);
end;

function TBuildingData.GetElemParID(Identificateur: integer): TElement;
begin
  result := bibElements.get(identificateur);
end;

function TBuildingData.GetFenParID(ID: integer): TMenuiseries;
begin
  result := bibMenuiseries.get(id);
end;


function CompareCompo(Item1, Item2: Pointer): integer;
begin
  if TComposition(Item1).Identificateur < TComposition(Item2).Identificateur then
    CompareCompo := -1
  else if TComposition(Item1).Identificateur > TComposition(Item2).Identificateur then
    CompareCompo := 1
  else
    CompareCompo := 0;
end;

function CompareMat(Item1, Item2: Pointer): integer;
begin
  if TMateriau(Item1).Identificateur < TMateriau(Item2).Identificateur then
    CompareMat := -1
  else if TMateriau(Item1).Identificateur > TMateriau(Item2).Identificateur then
    CompareMat := 1
  else
    CompareMat := 0;
end;

function CompareMen(Item1, Item2: Pointer): integer;
begin
  if TMenuiseries(Item1).Identificateur < TMenuiseries(Item2).Identificateur then
    CompareMen := -1
  else if TMenuiseries(Item1).Identificateur > TMenuiseries(Item2).Identificateur then
    CompareMen := 1
  else
    CompareMen := 0;
end;

function CompareElem(Item1, Item2: Pointer): integer;
begin
  if TElement(Item1).Identificateur < TElement(Item2).Identificateur then
    CompareElem := -1
  else if TElement(Item1).Identificateur > TElement(Item2).Identificateur then
    CompareElem := 1
  else
    CompareElem := 0;
end;

procedure TBuildingData.LoadFromList(List: TStringList; var Index: integer);
var
  erreur : integer;
  Reel: double;
begin
  CrawlSpace := (List[Index] = '1');
  inc(index);
  VentilatedLoft := (List[Index] = '1');
  inc(index);
  Val(List[index], NumStandard, Erreur);
  inc(index);
  Val(List[index], WindowWidth, Erreur);
  inc(index);
  Val(List[index], WindowHeight, Erreur);
  inc(index);
  Val(List[index], DoorWidth, Erreur);
  inc(index);
  Val(List[index], DoorHeight, Erreur);
  inc(index);
  if Erreur <> 0 then
    NumStandard := 1;

  // compo paroi ext�rieure
  compoExt := nil;
  if List[Index] <> '' then
  begin
    compoExt := bibCompositions.Get(List[Index]);
    if compoExt = nil then
    begin
      if MessageDLG('Une composition de paroi ext�rieure par d�faut a �t� d�finie dans le projet mais elle n''existe pas en biblioth�que.' + CR +
          'Voulez-vous en s�lectionner une pour �viter tout probl�me?', mtWarning, mbYesNo, 0) = mrYes then
      begin
        formCompo.chooseCompo(compoExt, 'Paroi ext�rieure par d�faut');
      end;
    end;
  end
  else
  begin
    if MessageDLG('Il n''y a pas de composition de paroi ext�rieure par d�faut.' + CR + 'Voulez-vous en s�lectionner une?', mtConfirmation, mbYesNo, 0)
      = mrYes then
    begin
      formCompo.chooseCompo(compoExt, 'Paroi ext�rieure par d�faut');
    end;
  end;
  inc(index);
  // compo paroi int�rieure
  compoInt := nil;
  if List[Index] <> '' then
  begin
    compoInt := bibCompositions.Get(List[Index]);
    if compoInt = nil then
    begin
      if MessageDLG('Une composition de paroi int�rieure par d�faut a �t� d�finie dans le projet mais elle n''existe pas en biblioth�que.' + CR +
          'Voulez-vous en s�lectionner une pour �viter tout probl�me?', mtWarning, mbYesNo, 0) = mrYes then
        formCompo.chooseCompo(compoInt, 'Paroi int�rieure par d�faut');
    end;
  end
  else
  begin
    if MessageDLG('Il n''y a pas de composition de paroi int�rieure par d�faut.' + CR + 'Voulez-vous en s�lectionner une?', mtConfirmation, mbYesNo, 0)
      = mrYes then
    begin
      formCompo.chooseCompo(compoInt, 'Paroi int�rieure par d�faut');
    end;
  end;
  inc(index);
  // compo plancher bas
  compoFloor := nil;
  if List[Index] <> '' then
  begin
    compoFloor := bibCompositions.Get(List[Index]);
    if compoFloor = nil then
    begin
      if MessageDLG('Une composition de plancher bas par d�faut a �t� d�finie dans le projet mais elle n''existe pas en biblioth�que.' + CR +
          'Voulez-vous en s�lectionner une pour �viter tout probl�me?', mtWarning, mbYesNo, 0) = mrYes then
        formCompo.chooseCompo(compoFloor, 'Plancher bas par d�faut');
    end;
  end
  else
  begin
    if MessageDLG('Il n''y a pas de composition de plancher bas par d�faut.' + CR + 'Voulez-vous en s�lectionner une?', mtConfirmation, mbYesNo, 0) = mrYes then
    begin
      formCompo.chooseCompo(compoFloor, 'Plancher bas par d�faut');
    end;
  end;
  inc(index);
  // compo plancher interm�diaire
  compoIntFloor := nil;
  if List[Index] <> '' then
  begin
    compoIntFloor := bibCompositions.Get(List[Index]);
    if compoIntFloor = nil then
    begin
      if MessageDLG('Une composition de plancher interm�diaire par d�faut a �t� d�finie dans le projet mais elle n''existe pas en biblioth�que.' + CR +
          'Voulez-vous en s�lectionner une pour �viter tout probl�me?', mtWarning, mbYesNo, 0) = mrYes then
        formCompo.chooseCompo(compoIntFloor, 'Plancher interm�diaire par d�faut');
    end;
  end
  else
  begin
    if MessageDLG('Il n''y a pas de composition de plancher interm�diaire par d�faut.' + CR + 'Voulez-vous en s�lectionner une?', mtConfirmation, mbYesNo, 0)
      = mrYes then
    begin
      formCompo.chooseCompo(compoIntFloor, 'Plancher interm�diaire par d�faut');
    end;
  end;
  inc(index);
  // compo toiture
  compoRoof := nil;
  if List[Index] <> '' then
  begin
    compoRoof := bibCompositions.Get(List[Index]);
    if compoRoof = nil then
    begin
      if MessageDLG('Une composition de toiture par d�faut a �t� d�finie dans le projet mais elle n''existe pas en biblioth�que.' + CR +
          'Voulez-vous en s�lectionner une pour �viter tout probl�me?', mtWarning, mbYesNo, 0) = mrYes then
        formCompo.chooseCompo(compoRoof, 'Toiture par d�faut');
    end;
  end
  else
  begin
    if MessageDLG('Il n''y a pas de composition de toiture par d�faut.' + CR + 'Voulez-vous en s�lectionner une?', mtConfirmation, mbYesNo, 0) = mrYes then
    begin
      formCompo.chooseCompo(compoRoof, 'Toiture par d�faut');
    end;
  end;
  // compo fen�tre
  inc(index);
  CompoWindow := nil;
  if List[Index] <> '-1' then
  begin
    CompoWindow := BibMenuiseries.Get(list[index]);
    if CompoWindow = nil then
    begin
      if MessageDLG('Une fen�tre par d�faut a �t� d�finie dans le projet mais elle n''existe pas en biblioth�que.' + CR +
          'Voulez-vous en s�lectionner une pour �viter tout probl�me?', mtWarning, mbYesNo, 0) = mrYes then
        formFen.chooseFen(CompoWindow, 'Fen�tre par d�faut');
    end;
  end
  else
  begin
    if MessageDLG('Il n''y a pas de fen�tre par d�faut.' + CR + 'Voulez-vous en s�lectionner une?', mtConfirmation, mbYesNo, 0) = mrYes then
    begin
      formFen.chooseFen(CompoWindow, 'Fen�tre par d�faut');
    end;
  end;
  inc(index);
  // compo porte
  CompoDoor := nil;
  if List[Index] <> '' then
  begin
    CompoDoor := BibMenuiseries.Get(list[index]);
    if CompoDoor = nil then
    begin
      if MessageDLG('Une porte par d�faut a �t� d�finie dans le projet mais elle n''existe pas en biblioth�que.' + CR +
          'Voulez-vous en s�lectionner une pour �viter tout probl�me?', mtWarning, mbYesNo, 0) = mrYes then
        formFen.chooseFen(CompoDoor, 'Porte par d�faut');
    end;
  end
  else
  begin
    if MessageDLG('Il n''y a pas de porte par d�faut.' + CR + 'Voulez-vous en s�lectionner une?', mtConfirmation, mbYesNo, 0) = mrYes then
    begin
      formFen.chooseFen(CompoDoor, 'Porte par d�faut');
    end;
  end;
  // inutile pour dialogie
  inc(index);
  // inutile pour dialogie
  inc(index);
  // inutile pour dialogie
  inc(index);
  // inutile pour dialogie
  inc(index);
  // inutile pour dialogie
  inc(index);

  inc(index);
  // Station := nil;
  if Index < List.Count then
  begin
    inc(Index);
  end;

  if (index < List.Count) and (List[Index] = 'Ponts') then
  begin
    inc(index);
    inc(index);
    inc(index);
    inc(index);
    inc(index);
    inc(index);
    inc(index);
    inc(index);
    inc(index);
    inc(index);
    inc(index);
  end;
  if (index < List.Count) and (List[Index] = 'Ponts menuiseries') then
  begin
    inc(index);
    inc(index);
    inc(index);
    inc(index);
    inc(index);
    inc(index);
  end;
end;

procedure TBuildingData.SaveToList(List: TStringList);
var
  Chaine: string;
begin
  if CrawlSpace then
    List.Add('1')
  else
    List.Add('0');
  if VentilatedLoft then
    List.Add('1')
  else
    List.Add('0');
  Str(NumStandard, Chaine);
  List.Add(Chaine);
  Str(WindowWidth, Chaine);
  List.Add(Chaine);
  Str(WindowHeight, Chaine);
  List.Add(Chaine);
  Str(DoorWidth, Chaine);
  List.Add(Chaine);
  Str(DoorHeight, Chaine);
  List.Add(Chaine);
  if compoExt <> nil then
  begin
    List.Add(compoExt.nom);
  end
  else
    List.Add('');

  if compoInt <> nil then
  begin
    list.Add(compoInt.nom)
  end
  else
    List.Add('');

  if compoFloor <> nil then
  begin
    list.Add(compoFloor.Nom);
  end
  else
    List.Add('');

  if compoIntFloor <> nil then
  begin
    list.Add(compoIntFloor.Nom)
  end
  else
    List.Add('');

  if compoRoof <> nil then
  begin
    list.Add(compoRoof.nom);
  end
  else
    List.Add('');

  if CompoWindow <> nil then
  begin
    list.Add(CompoWindow.Nom);
  end
  else
    List.Add('');

  if CompoDoor <> nil then
  begin
    list.Add(CompoDoor.Nom);
  end
  else
    List.Add('');

  List.Add('-1');

  List.Add('-1');

  List.Add('-1');

  List.Add('-1');

  List.Add('-1');
  List.Add('');

  // PontSol, PontInter, PontToit, PontMurMur, PontMurRefend : TPont;
  List.Add('Ponts');

  List.Add('');
  List.Add('0');

  List.Add('');
  List.Add('0');

  List.Add('');
  List.Add('0');

  List.Add('');
  List.Add('0');

  List.Add('');
  List.Add('0');

  List.Add('Ponts menuiseries');

  List.Add('');
  List.Add('0');

  List.Add('');
  List.Add('0');

  List.Add('');
  list.add('0');
end;




procedure TBuildingData.innerLoadXml(node : IXMLNode);
var
  s : string;
begin
  CrawlSpace := TXmlUtil.getTagBool(node, 'crawl-space');
  VentilatedLoft := TXmlUtil.getTagBool(node, 'ventilated-loft');

  NumStandard := TXmlUtil.getTagInt(node, 'num-standard');
  WindowWidth := TXmlUtil.getTagDouble(node, 'window-width');
  WindowHeight := TXmlUtil.getTagDouble(node, 'window-height');
  DoorWidth := TXmlUtil.getTagDouble(node, 'door-width');
  DoorHeight := TXmlUtil.getTagDouble(node, 'door-height');


  // compo paroi ext�rieure
  compoExt := nil;
  s := TXmlUtil.getTagString(node, 'compo-ext');
  if s <> '' then
  begin
    compoExt := bibCompositions.Get(s);
    if compoExt = nil then
    begin
      if MessageDLG('Une composition de paroi ext�rieure par d�faut a �t� d�finie dans le projet mais elle n''existe pas en biblioth�que.' + CR +
          'Voulez-vous en s�lectionner une pour �viter tout probl�me?', mtWarning, mbYesNo, 0) = mrYes then
      begin
        formCompo.chooseCompo(compoExt, 'Paroi ext�rieure par d�faut');
      end;
    end;
  end
  else
  begin
    if MessageDLG('Il n''y a pas de composition de paroi ext�rieure par d�faut.' + CR + 'Voulez-vous en s�lectionner une?', mtConfirmation, mbYesNo, 0)
      = mrYes then
    begin
      formCompo.chooseCompo(compoExt, 'Paroi ext�rieure par d�faut');
    end;
  end;
  // compo paroi int�rieure
  compoInt := nil;
  s := TXmlUtil.getTagString(node, 'compo-int');
  if s <> '' then
  begin
    compoInt := bibCompositions.Get(s);
    if compoInt = nil then
    begin
      if MessageDLG('Une composition de paroi int�rieure par d�faut a �t� d�finie dans le projet mais elle n''existe pas en biblioth�que.' + CR +
          'Voulez-vous en s�lectionner une pour �viter tout probl�me?', mtWarning, mbYesNo, 0) = mrYes then
        formCompo.chooseCompo(compoInt, 'Paroi int�rieure par d�faut');
    end;
  end
  else
  begin
    if MessageDLG('Il n''y a pas de composition de paroi int�rieure par d�faut.' + CR + 'Voulez-vous en s�lectionner une?', mtConfirmation, mbYesNo, 0)
      = mrYes then
    begin
      formCompo.chooseCompo(compoInt, 'Paroi int�rieure par d�faut');
    end;
  end;
  // compo plancher bas
  compoFloor := nil;
  s := TXmlUtil.getTagString(node, 'compo-floor');
  if s <> '' then
  begin
    compoFloor := bibCompositions.Get(s);
    if compoFloor = nil then
    begin
      if MessageDLG('Une composition de plancher bas par d�faut a �t� d�finie dans le projet mais elle n''existe pas en biblioth�que.' + CR +
          'Voulez-vous en s�lectionner une pour �viter tout probl�me?', mtWarning, mbYesNo, 0) = mrYes then
        formCompo.chooseCompo(compoFloor, 'Plancher bas par d�faut');
    end;
  end
  else
  begin
    if MessageDLG('Il n''y a pas de composition de plancher bas par d�faut.' + CR + 'Voulez-vous en s�lectionner une?', mtConfirmation, mbYesNo, 0) = mrYes then
    begin
      formCompo.chooseCompo(compoFloor, 'Plancher bas par d�faut');
    end;
  end;
  // compo plancher interm�diaire
  compoIntFloor := nil;
  s := TXmlUtil.getTagString(node, 'compo-int-floor');
  if s <> '' then
  begin
    compoIntFloor := bibCompositions.Get(s);
    if compoIntFloor = nil then
    begin
      if MessageDLG('Une composition de plancher interm�diaire par d�faut a �t� d�finie dans le projet mais elle n''existe pas en biblioth�que.' + CR +
          'Voulez-vous en s�lectionner une pour �viter tout probl�me?', mtWarning, mbYesNo, 0) = mrYes then
        formCompo.chooseCompo(compoIntFloor, 'Plancher interm�diaire par d�faut');
    end;
  end
  else
  begin
    if MessageDLG('Il n''y a pas de composition de plancher interm�diaire par d�faut.' + CR + 'Voulez-vous en s�lectionner une?', mtConfirmation, mbYesNo, 0)
      = mrYes then
    begin
      formCompo.chooseCompo(compoIntFloor, 'Plancher interm�diaire par d�faut');
    end;
  end;
  // compo toiture
  compoRoof := nil;
  s := TXmlUtil.getTagString(node, 'compo-roof');
  if s <> '' then
  begin
    compoRoof := bibCompositions.Get(s);
    if compoRoof = nil then
    begin
      if MessageDLG('Une composition de toiture par d�faut a �t� d�finie dans le projet mais elle n''existe pas en biblioth�que.' + CR +
          'Voulez-vous en s�lectionner une pour �viter tout probl�me?', mtWarning, mbYesNo, 0) = mrYes then
        formCompo.chooseCompo(compoRoof, 'Toiture par d�faut');
    end;
  end
  else
  begin
    if MessageDLG('Il n''y a pas de composition de toiture par d�faut.' + CR + 'Voulez-vous en s�lectionner une?', mtConfirmation, mbYesNo, 0) = mrYes then
    begin
      formCompo.chooseCompo(compoRoof, 'Toiture par d�faut');
    end;
  end;
  // compo fen�tre
  CompoWindow := nil;
  s := TXmlUtil.getTagString(node, 'compo-window');
  if s <> '' then
  begin
    CompoWindow := BibMenuiseries.Get(s);
    if CompoWindow = nil then
    begin
      if MessageDLG('Une fen�tre par d�faut a �t� d�finie dans le projet mais elle n''existe pas en biblioth�que.' + CR +
          'Voulez-vous en s�lectionner une pour �viter tout probl�me?', mtWarning, mbYesNo, 0) = mrYes then
        formFen.chooseFen(CompoWindow, 'Fen�tre par d�faut');
    end;
  end
  else
  begin
    if MessageDLG('Il n''y a pas de fen�tre par d�faut.' + CR + 'Voulez-vous en s�lectionner une?', mtConfirmation, mbYesNo, 0) = mrYes then
    begin
      formFen.chooseFen(CompoWindow, 'Fen�tre par d�faut');
    end;
  end;
  // compo porte
  CompoDoor := nil;
  s := TXmlUtil.getTagString(node, 'compo-door');
  if s <> '' then
  begin
    CompoDoor := BibMenuiseries.Get(s);
    if CompoDoor = nil then
    begin
      if MessageDLG('Une porte par d�faut a �t� d�finie dans le projet mais elle n''existe pas en biblioth�que.' + CR +
          'Voulez-vous en s�lectionner une pour �viter tout probl�me?', mtWarning, mbYesNo, 0) = mrYes then
        formFen.chooseFen(CompoDoor, 'Porte par d�faut');
    end;
  end
  else
  begin
    if MessageDLG('Il n''y a pas de porte par d�faut.' + CR + 'Voulez-vous en s�lectionner une?', mtConfirmation, mbYesNo, 0) = mrYes then
    begin
      formFen.chooseFen(CompoDoor, 'Porte par d�faut');
    end;
  end;
end;

procedure TBuildingData.innerSaveXml(node : IXMLNode);
var
  s : string;
begin
  TXmlUtil.setTag(node, 'crawl-space', CrawlSpace);
  TXmlUtil.setTag(node, 'ventilated-loft', VentilatedLoft);

  TXmlUtil.setTag(node, 'num-standard', NumStandard);
  TXmlUtil.setTag(node, 'window-width', WindowWidth);
  TXmlUtil.setTag(node, 'window-height', WindowHeight);
  TXmlUtil.setTag(node, 'door-width', DoorWidth);
  TXmlUtil.setTag(node, 'door-height', DoorHeight);
  if compoExt <> nil then
  begin
    s := compoExt.nom;
  end
  else
    s := '' ;
  TXmlUtil.setTag(node, 'compo-ext', s);

  if compoInt <> nil then
  begin
    s := compoInt.nom
  end
  else
    s := '' ;
  TXmlUtil.setTag(node, 'compo-int', s);

  if compoFloor <> nil then
  begin
    s := compoFloor.Nom;
  end
  else
    s := '' ;
  TXmlUtil.setTag(node, 'compo-floor', s);

  if compoIntFloor <> nil then
  begin
    s := compoIntFloor.Nom
  end
  else
    s := '' ;
  TXmlUtil.setTag(node, 'compo-int-floor', s);

  if compoRoof <> nil then
  begin
    s := compoRoof.nom;
  end
  else
    s := '' ;
  TXmlUtil.setTag(node, 'compo-roof', s);

  if CompoWindow <> nil then
  begin
    s := CompoWindow.Nom;
  end
  else
    s := '' ;
  TXmlUtil.setTag(node, 'compo-window', s);

  if CompoDoor <> nil then
  begin
    s := CompoDoor.Nom;
  end
  else
    s := '' ;
  TXmlUtil.setTag(node, 'compo-door', s);
end;




constructor TBuildingData.Create;
var
  FichierIni: TStringList;
  Erreur: integer;
  chemin : string;
begin
  inherited Create();
  WindowWidth := 1.15;
  WindowHeight := 1;
  DoorWidth := 0.83;
  DoorHeight := 2.04;
  CrawlSpace := False;
  VentilatedLoft := False;
  ListePlancher := TList.Create;
  BiblioEtat := TList.Create;
  FichierIni := TStringList.Create;
  FichierIni.LoadFromFile(i3dDataPath + FileINI);
  Chemin := TApp.path + '..\';

  if FichierIni.Count >= 4 then
    Val(FichierIni[3], Langue, Erreur)
  else
    Langue := 2;
  LoadMessages(Langue, i3dDataPath + 'messages.txt');

	bibCompositions := TBiblioCompositions.Create(dialogieLibPath, TApp.documentPath + dossierBiblio);
  bibMenuiseries := TBiblioMenuiseries.Create(dialogieLibPath, TApp.documentPath + dossierBiblio);
	bibMateriaux := TBiblioMateriaux.Create(dialogieLibPath, TApp.documentPath + dossierBiblio);
	bibElements := TBiblioElements.Create(dialogieLibPath, TApp.documentPath + dossierBiblio);
  FichierIni.Free;
end;

destructor TBuildingData.Destroy;
var
  i: integer;
begin
  bibCompositions.free();
  bibMateriaux.Free;
  BibElements.Free;
  BibMenuiseries.Free;
  BiblioEtat.Free;
  for i := 0 to ListePlancher.Count - 1 do
    Tplancher(ListePlancher[i]).Free;
  ListePlancher.Free;
end;



procedure TBuildingData.PrepareShed;
var
  i, j, k, l, m: integer;
  Level, Toiture: TSketchLevel;
  R1, R2: TRoom;
  p1, p2, p3, p4: LTPoint;
  Position1, Position2: integer;
  h1, h2, h3, h4: double;
  ListePoints: TList;
  ListeHaut1, Listehaut2: TStringList;
  Seg: LTSegment;
  p: TParoiTemp;
  pp1, pp2: LTPoint;
  Cangle: double;
begin
  if project.sketchObject.Levels.Count - 2 < 0 then
    exit;
  Level := TSketchLevel(project.sketchObject.Levels[project.sketchObject.Levels.Count - 2]);
  Toiture := TSketchLevel(project.sketchObject.Levels[project.sketchObject.Levels.Count - 1]);
  for i := 0 to Toiture.segments.Count - 1 do
    LTSegment(Toiture.segments).Traite := False;
  ListePoints := TList.Create;
  ListeHaut1 := TStringList.Create;
  Listehaut2 := TStringList.Create;
  for j := 0 to Level.Rooms.Count - 1 do
  begin
    ListePoints.Clear;
    ListeHaut1.Clear;
    Listehaut2.Clear;
    // for k := j to Level.Rooms.Count - 1 do
    // begin
    R1 := TRoom(Level.Rooms[j]);
    R1.ListeShed.Clear;
    for l := 0 to R1.Points.Count - 3 do
    begin
      h1 := CorrigeHauteurP(R1.Points[l + 1], R1.Points[l], R1);
      h2 := CorrigeHauteurP(R1.Points[l + 1], R1.Points[l + 2], R1);
      if h1 <> h2 then
      begin
        ListePoints.Add(R1.Points[l + 1]);
        ListeHaut1.Add(FloatToStr(h1));
        Listehaut2.Add(FloatToStr(h2));
      end;
    end;
    // end;
    for k := 0 to ListePoints.Count - 2 do
      for l := k + 1 to ListePoints.Count - 1 do
      begin
        pp1 := Nil;
        pp2 := Nil;
        for m := 0 to Toiture.points.Count - 1 do
        begin
          if (LTPoint(ListePoints[k]).x = LTPoint(Toiture.points[m]).x) and (LTPoint(ListePoints[k]).y = LTPoint(Toiture.points[m]).y) then
            pp1 := LTPoint(Toiture.points[m]);
          if (LTPoint(ListePoints[l]).x = LTPoint(Toiture.points[m]).x) and (LTPoint(ListePoints[l]).y = LTPoint(Toiture.points[m]).y) then
            pp2 := LTPoint(Toiture.points[m]);
        end;
        Seg := Toiture.SegmentExists(pp1, pp2);
        if Seg = nil then
          Seg := Toiture.SegmentExists(pp2, pp1);
        if Seg <> nil then
        begin
          p := TParoiTemp.Create;
          p.Largeur := Seg.Size(TSketchObject(Toiture.FatherObject).Scale);
          p.Hauteur := (Abs(StrToFloat(ListeHaut1[k]) - StrToFloat(Listehaut2[k])) + Abs(StrToFloat(ListeHaut1[k]) - StrToFloat(ListeHaut1[k])) / 2);
          p.Surface := p.Largeur * p.Hauteur;
          if ListeHaut1[k] > Listehaut2[k] then
            Cangle := -T3DUtil.Angle(pp2, pp1)
          else
            Cangle := -T3DUtil.Angle(pp1, pp2);
          Seg.Orientation := Cangle / pi * 180 + 180;

          p.Orientation := Round(Seg.Orientation);
          p.Orientation := Round(p.Orientation + 180 - project.sketchObject.OrientationNord);
          while p.Orientation > 180 do
            p.Orientation := p.Orientation - 360;
          while p.Orientation < -180 do
            p.Orientation := p.Orientation + 360;
          p.Inclinaison := 90;
          R1.ListeShed.Add(p);
        end;
      end;
  end;
  ListePoints.Free;
  ListeHaut1.Free;
  Listehaut2.Free;
end;

procedure TBuildingData.ExporteParois(Room: TRoom; Sauvegarde: TStringList; Log: TStrings);
var
  Level, LevelToiture: TSketchLevel;
  Seg: LTSegment;
  i, j: integer;
  RDC, SousToit: boolean;
  NbParoi: integer;
  Chaine: string;
  IdParoi: integer;
  nbLevel: integer;
  Room2: TRoom;
  Seg2: LTSegment;
  Orient: integer;
  Porte: TPorte;
  fenetre: TFenetre;
  Plancher: Tplancher;
  NumLevel, Erreur: integer;
  sensN: boolean;
  PlancherArea: double;
  Hauteur1, Hauteur2: double;
  ToitureExiste, SousToiture: boolean;
  Pont, psi: double;
  So: TSketchObject;
  PT: TParoiTemp;
begin
  So := project.sketchObject;
  Room.ListePLanchInt.Clear;
  Level := nil;
  NumLevel := 0;
  for i := 0 to project.sketchObject.Levels.Count - 1 do
  begin
    NumLevel := i;
    Level := TSketchLevel(project.sketchObject.Levels[i]);
    if (Level.Rooms.IndexOf(Room) <> -1) then
      Break;
  end;
  if Level = nil then
    exit;
  for i := 0 to Level.segments.Count - 1 do
  begin
    LTSegment(Level.segments[i]).Identificateur := NumLevel * 10000 + (i * 2) + 1;
    LTSegment(Level.segments[i]).Identificateur2 := NumLevel * 10000 + (i * 2) + 2;
  end;
  nbLevel := project.sketchObject.Levels.Count - 1;
  if TSketchLevel(project.sketchObject.Levels[nbLevel]).points.Count = 0 then
    Dec(nbLevel);
  ToitureExiste := False;
  if TSketchLevel(project.sketchObject.Levels[nbLevel]).Toiture then
  begin
    ToitureExiste := True;
    Dec(nbLevel);
  end;
  RDC := (project.sketchObject.Levels.IndexOf(Level) = 0);
  SousToit := (project.sketchObject.Levels.IndexOf(Level) = nbLevel);
  IdParoi := 1;
  SousToiture := SousToit and ToitureExiste;
  NbParoi := Room.Points.Count - 1;
  if RDC then
    inc(NbParoi)
  else
  begin
    PlancherArea := 0;
    for i := 0 to ListePlancher.Count - 1 do
      if (Tplancher(ListePlancher[i]).PieceHaute = Room.ID) and (Tplancher(ListePlancher[i]).Surface <> 0) then
      begin
        PlancherArea := PlancherArea + Tplancher(ListePlancher[i]).Surface;
        inc(NbParoi);
      end;
    if ((Room.Area - PlancherArea) >= 1) then
      inc(NbParoi);
  end;
  if SousToit and not SousToiture then
  begin
    inc(NbParoi);
  end
  else
  begin
    for i := 0 to ListePlancher.Count - 1 do
    begin
      if (Tplancher(ListePlancher[i]).PieceBasse = Room.ID) and (Tplancher(ListePlancher[i]).Surface <> 0) then
      begin
        // PlancherArea := PlancherArea + TPlancher(ListePlancher[i]).surface;
        inc(NbParoi);
      end;
    end;
  end;
  NbParoi := NbParoi + Room.ListeShed.Count;
  Str(NbParoi, Chaine);
  Sauvegarde.Add(Chaine);

  // plancher bas
  if RDC then
  begin
    // Nom
    Sauvegarde.Add('Plancher ' + IntToStr(Room.ID) + '/' + IntToStr(IdParoi));
    Log.Add('   * ' + 'Plancher ' + IntToStr(Room.ID) + '/' + IntToStr(IdParoi));
    inc(IdParoi);
    // Surface
    Str(Room.Area, Chaine);
    Sauvegarde.Add(Chaine);
    // Hauteur
    Str(Sqrt(Room.Area), Chaine);
    Sauvegarde.Add(Chaine);
    // Largeur
    Str(Sqrt(Room.Area), Chaine);
    Sauvegarde.Add(Chaine);
    // Pont thermique : A FAIRE UN JOUR !!!!
    Sauvegarde.Add('-1');
    // Sauvegarde.Add('-1');
    // Orientation
    Sauvegarde.Add('0');
    // Inclinaison
    Sauvegarde.Add('180');
    // Exposition au vent : A FAIRE UN JOUR !!!!
    Sauvegarde.Add('1');
    // Contact
    if CrawlSpace then
      Sauvegarde.Add('0')
    else
      Sauvegarde.Add('-2');
    // Composition
    if Room.compoFloor = nil then
    begin
      if compoFloor <> nil then
        Str(compoFloor.Identificateur, Chaine)
      else
        Chaine := '-1';
    end
    else
      Chaine := IntToStr(Room.compoFloor.Identificateur);
    Sauvegarde.Add(Chaine);
    // Etat Int
    Sauvegarde.Add('-1');
    // Etat Ext
    Sauvegarde.Add('-1');
    // Albedos : A FAIRE UN JOUR
    Sauvegarde.Add('-1');
    // Occultations : A FAIRE UN JOUR
    Sauvegarde.Add('-1');
    // Plantations : A FAIRE UN JOUR
    Sauvegarde.Add('-1');
    // Ouvertures
    Sauvegarde.Add('Liste des Ouvertures');
    Sauvegarde.Add('0');
    // Sens Compo
    Sauvegarde.Add('1');
    // Checked
    Sauvegarde.Add('1');
    // Identificateur
    Str(Room.ID + 3000, Chaine);
    inc(IdParoiAbs);
    Sauvegarde.Add(Chaine);
    // Masques proches
    Sauvegarde.Add('0');
  end
  else
  begin
    PlancherArea := 0;
    for i := 0 to ListePlancher.Count - 1 do
    begin
      Plancher := Tplancher(ListePlancher[i]);
      if (Plancher.PieceHaute = Room.ID) and (Plancher.Surface <> 0) then
      begin
        Sauvegarde.Add('Plancher Int.' + IntToStr(Room.ID) + '/' + IntToStr(IdParoi));
        Log.Add('   * ' + 'Plancher Int.' + IntToStr(Room.ID) + '/' + IntToStr(IdParoi));
        Room.ListePLanchInt.Add('Plancher Int.' + IntToStr(Room.ID) + '/' + IntToStr(IdParoi));
        inc(IdParoi);
        Str(Plancher.Surface, Chaine);
        Sauvegarde.Add(Chaine);
        Str(Sqrt(Plancher.Surface), Chaine);
        Sauvegarde.Add(Chaine);
        Str(Sqrt(Plancher.Surface), Chaine);
        Sauvegarde.Add(Chaine);
        Str(-1.0, Chaine);
        Sauvegarde.Add(Chaine);
        Sauvegarde.Add('0');
        Sauvegarde.Add('180');
        Sauvegarde.Add('1');
        Sauvegarde.Add(IntToStr(Plancher.Idh));
        if Plancher.PieceBasse <> -3 then
        begin
          begin
            if compoIntFloor <> nil then
              Str(compoIntFloor.Identificateur, Chaine)
            else
              Chaine := '-1';
          end;
        end
        else
        begin
          begin
            if Room.compoFloor = nil then
            begin
              if compoFloor <> nil then
                Str(compoFloor.Identificateur, Chaine)
              else
                Chaine := '-1';
            end
            else
              Chaine := IntToStr(Room.compoFloor.Identificateur);
          end;
        end;
        Sauvegarde.Add(Chaine);
        Sauvegarde.Add('-1');
        Sauvegarde.Add('-1');


        Sauvegarde.Add('-1');
        Sauvegarde.Add('-1');
        Sauvegarde.Add('-1');
        Sauvegarde.Add('Liste des Ouvertures');
        Sauvegarde.Add('0');
        // Sens Compo  A VERIFIER
        Sauvegarde.Add('0');
        Sauvegarde.Add('1');
        // Identificateur
        Str(Plancher.idb, Chaine);
        inc(IdParoiAbs);
        Sauvegarde.Add(Chaine);
        // Masques proches
        Sauvegarde.Add('0');
        PlancherArea := PlancherArea + Plancher.Surface;
      end;
    end;
    // Plancher sur vide
    if (Room.Area - PlancherArea) >= 1 then
    begin
      Sauvegarde.Add('Plancher suspendu' + IntToStr(Room.ID) + '/' + IntToStr(NbParoi + 1));
      Log.Add('   * ' + 'Plancher suspendu' + IntToStr(Room.ID) + '/' + IntToStr(NbParoi + 1));
      Str((Room.Area - PlancherArea), Chaine);
      Sauvegarde.Add(Chaine);
      Str(Sqrt(Room.Area - PlancherArea), Chaine);
      Sauvegarde.Add(Chaine);
      Str(Sqrt(Room.Area - PlancherArea), Chaine);
      Sauvegarde.Add(Chaine);
      Str(-1.0, Chaine);
      Sauvegarde.Add(Chaine);
      Sauvegarde.Add('0');
      Sauvegarde.Add('180');
      Sauvegarde.Add('1');
      Sauvegarde.Add(IntToStr(-3)); // plancher.idh ?
      begin
        if Room.compoFloor = nil then
        begin
          if compoFloor <> nil then
            Str(compoFloor.Identificateur, Chaine)
          else
            Chaine := '-1';
        end
        else
          Chaine := IntToStr(Room.compoFloor.Identificateur);
      end;
      Sauvegarde.Add(Chaine);

      Sauvegarde.Add('-1');
      Sauvegarde.Add('-1');


      Sauvegarde.Add('-1');
      Sauvegarde.Add('-1');
      Sauvegarde.Add('-1');
      Sauvegarde.Add('Liste des Ouvertures');
      Sauvegarde.Add('0');
      // Sens Compo  A VERIFIER
      Sauvegarde.Add('1');
      Sauvegarde.Add('1');
      // Identificateur
      Str(Room.ID + 3001, Chaine);
      // Inc(IdParoiAbs);
      Sauvegarde.Add(Chaine);
      // Masques proches
      Sauvegarde.Add('0');
    end;
  end;
  // Sous les toits
  if SousToit and not ToitureExiste then
  begin
    // Nom
    if VentilatedLoft then
    begin
      Sauvegarde.Add('Plafond ' + IntToStr(Room.ID) + '/' + IntToStr(IdParoi));
      Log.Add('   * ' + 'Plafond ' + IntToStr(Room.ID) + '/' + IntToStr(IdParoi));
    end
    else
    begin
      Sauvegarde.Add('Toiture ' + IntToStr(Room.ID) + '/' + IntToStr(IdParoi));
      Log.Add('   * ' + 'Toiture ' + IntToStr(Room.ID) + '/' + IntToStr(IdParoi));
    end;
    inc(IdParoi);
    // Surface
    Str(Room.Area, Chaine);
    Sauvegarde.Add(Chaine);
    // Hauteur
    Str(Sqrt(Room.Area), Chaine);
    Sauvegarde.Add(Chaine);
    // Largeur
    Str(Sqrt(Room.Area), Chaine);
    Sauvegarde.Add(Chaine);
    // Pont thermique
    Sauvegarde.Add('-1');
    // Orientation
    Sauvegarde.Add('0');
    // Inclinaison
    Sauvegarde.Add('0');
    // Exposition au vent
    Sauvegarde.Add('1');
    // Contact
    if VentilatedLoft then
      Sauvegarde.Add('-1')
    else
      Sauvegarde.Add('-3');
    // Composition
    if Room.CompoCeiling = nil then
    begin
      if compoRoof <> nil then
        Str(compoRoof.Identificateur, Chaine)
      else
        Chaine := '-1';
    end
    else
      Chaine := IntToStr(Room.CompoCeiling.Identificateur);
    Sauvegarde.Add(Chaine);

    Sauvegarde.Add('-1');
    Sauvegarde.Add('-1');


    // Albedos : A FAIRE UN JOUR
    Sauvegarde.Add('-1');
    // Occultations : A FAIRE UN JOUR
    Sauvegarde.Add('-1');
    // Plantations : A FAIRE UN JOUR
    Sauvegarde.Add('-1');
    // Ouvertures
    Sauvegarde.Add('Liste des Ouvertures');
    Sauvegarde.Add('0');
    // Sens Compo : A FAIRE UN JOUR
    Sauvegarde.Add('1');
    // Checked
    Sauvegarde.Add('1');
    // Identificateur
    Str(Room.ID + 4000, Chaine);
    inc(IdParoiAbs);
    Sauvegarde.Add(Chaine);
    // Masques proches
    Sauvegarde.Add('0');
  end
  else
  begin
    for i := 0 to ListePlancher.Count - 1 do
    begin
      Plancher := Tplancher(ListePlancher[i]);
      if (Plancher.PieceBasse = Room.ID) and (Plancher.Surface <> 0) then
      begin
        if (Plancher.PieceHaute = -3) or SousToiture then
        begin
          Sauvegarde.Add('Toiture ' + IntToStr(Room.ID) + '/' + IntToStr(IdParoi));
          Log.Add('   * ' + 'Toiture ' + IntToStr(Room.ID) + '/' + IntToStr(IdParoi));
        end
        else
        begin
          Sauvegarde.Add('Plafond Int.' + IntToStr(Room.ID) + '/' + IntToStr(IdParoi));
          Log.Add('   * ' + 'Plafond Int.' + IntToStr(Room.ID) + '/' + IntToStr(IdParoi));
        end;
        if not SousToiture then
        begin
          Room.ListePLanchInt.Add('Plafond Int.' + IntToStr(Room.ID) + '/' + IntToStr(IdParoi));
        end;
        inc(IdParoi);
        if SousToiture and (Plancher.Correction <> 0) then
          Str(Plancher.Surface / Plancher.Correction, Chaine)
        else
          Str(Plancher.Surface, Chaine);
        Sauvegarde.Add(Chaine);
        Str(Sqrt(Plancher.Surface), Chaine);
        Sauvegarde.Add(Chaine);
        Str(Sqrt(Plancher.Surface), Chaine);
        Sauvegarde.Add(Chaine);
        Sauvegarde.Add('-1');

        // Orientation
        Sauvegarde.Add(IntToStr(Round(Plancher.Orientation)));
        // inclinaison
        Sauvegarde.Add(IntToStr(Round(Plancher.Inclinaison)));
        Sauvegarde.Add('1');
        if SousToiture then
          Sauvegarde.Add(IntToStr(-3))
        else
        begin
          Sauvegarde.Add(IntToStr(Plancher.idb));
        end;
        if (Plancher.PieceHaute = -3) or SousToiture then
        begin
          if Room.CompoCeiling = nil then
          begin
            if compoRoof <> nil then
              Str(compoRoof.Identificateur, Chaine)
            else
              Chaine := '-1';
          end
          else
            Chaine := IntToStr(Room.CompoCeiling.Identificateur);
        end
        else
        begin
          if Plancher.PH.compoFloor = nil then
          begin
            if compoIntFloor <> nil then
              Str(compoIntFloor.Identificateur, Chaine)
            else
              Chaine := '-1';
          end
          else
            Chaine := IntToStr(Plancher.PH.compoFloor.Identificateur);
        end;
        Sauvegarde.Add(Chaine);
        Sauvegarde.Add('-1');
        Sauvegarde.Add('-1');



        Sauvegarde.Add('-1');
        Sauvegarde.Add('-1');
        Sauvegarde.Add('-1');
        Sauvegarde.Add('Liste des Ouvertures');
        Sauvegarde.Add('0');
        // Sens Compo  A VERIFIER
        Sauvegarde.Add('1');
        Sauvegarde.Add('1');
        // Identificateur
        Str(Plancher.Idh, Chaine);
        inc(IdParoiAbs);
        Sauvegarde.Add(Chaine);
        // Masques proches
        Sauvegarde.Add('0');
      end;
    end;
  end;


  // Liste des sheds
  for i := 0 to Room.ListeShed.Count - 1 do
  begin
    PT := TParoiTemp(Room.ListeShed[i]);
    // Nom
    Sauvegarde.Add('Toiture ' + IntToStr(Room.ID) + '/' + IntToStr(IdParoi));
    Log.Add('   * ' + 'Plafond ' + IntToStr(Room.ID) + '/' + IntToStr(IdParoi));
    inc(IdParoi);
    // Surface
    Str(PT.Surface, Chaine);
    Sauvegarde.Add(Chaine);
    // Hauteur
    Str(PT.Hauteur, Chaine);
    Sauvegarde.Add(Chaine);
    // Largeur
    Str(PT.Largeur, Chaine);
    Sauvegarde.Add(Chaine);
    // Pont thermique
    Sauvegarde.Add('-1');
    // Orientation
    Str(PT.Orientation, Chaine);
    Sauvegarde.Add(Chaine);
    // Inclinaison
    Str(PT.Inclinaison, Chaine);
    Sauvegarde.Add(Chaine);
    // Exposition au vent
    Sauvegarde.Add('1');
    // Contact
    Sauvegarde.Add('-3');
    // Composition
    if Room.CompoCeiling = nil then
    begin
      if compoRoof <> nil then
        Str(compoRoof.Identificateur, Chaine)
      else
        Chaine := '-1';
    end
    else
      Chaine := IntToStr(Room.CompoCeiling.Identificateur);
    Sauvegarde.Add(Chaine);
    Sauvegarde.Add('-1');
    Sauvegarde.Add('-1');


    // Albedos
    Sauvegarde.Add('-1');
    // Occultations
    Sauvegarde.Add('-1');
    // Plantations
    Sauvegarde.Add('-1');
    // Ouvertures
    Sauvegarde.Add('Liste des Ouvertures');
    Sauvegarde.Add('0');
    // Sens Compo
    Sauvegarde.Add('1');
    // Checked
    Sauvegarde.Add('1');
    // Identificateur
    Str(Room.ID + 4000, Chaine);
    inc(IdParoiAbs);
    Sauvegarde.Add(Chaine);
    // Masques proches
    Sauvegarde.Add('0');
  end;

  for i := 0 to Room.Points.Count - 2 do
  begin
    Seg := Room.GetSegment(Room.Points[i], Room.Points[i + 1], Level.segments);
    sensN := (Seg.A = Room.Points[i]);
    // Nom
    if Seg.Nom = '' then
    begin
      Seg.Nom := 'Paroi ' + IntToStr(Room.ID) + '/' + IntToStr(IdParoi);
      Log.Add('   * ' + 'Paroi ' + IntToStr(Room.ID) + '/' + IntToStr(IdParoi));
      Sauvegarde.Add(Seg.Nom);
    end
    else
    begin
      Seg.nomB := 'Paroi ' + IntToStr(Room.ID) + '/' + IntToStr(IdParoi);
      Log.Add('   * ' + 'Paroi ' + IntToStr(Room.ID) + '/' + IntToStr(IdParoi));
      Sauvegarde.Add(Seg.nomB);
    end;
    inc(IdParoi);
    // Hauteur corrig�e si n�cessaire
    Hauteur1 := Level.Height + CorrigeHauteurP(Seg.A, Seg.b, Room);
    Hauteur2 := Level.Height + CorrigeHauteurP(Seg.b, Seg.A, Room);
    Hauteur1 := (Hauteur1 + Hauteur2) / 2;
    // Surface
    Str(Seg.Size(project.sketchObject.Scale) * Hauteur1, Chaine);
    Sauvegarde.Add(Chaine);
    // Hauteur
    Str(Hauteur1, Chaine);
    Sauvegarde.Add(Chaine);
    // Largeur
    Str(Seg.Size(project.sketchObject.Scale), Chaine);
    Sauvegarde.Add(Chaine);

    Sauvegarde.Add('-1');

    // Orientation
    Orient := Round(Seg.Orientation + 180 - project.sketchObject.OrientationNord);
    while Orient > 180 do
      Orient := Orient - 360;
    while Orient < -180 do
      Orient := Orient + 360;
    Str(Orient, Chaine);
    Sauvegarde.Add(Chaine);
    // Inclinaison
    Sauvegarde.Add('90');
    // Exposition au vent
    Sauvegarde.Add('1');
    // Contact
    if Seg.Perimeter then
      Sauvegarde.Add('-3')
    else
    begin
      for j := 0 to Level.Rooms.Count - 1 do
      begin
        Room2 := TRoom(Level.Rooms[j]);
        if Room2.ID <> Room.ID then
          Seg2 := Room2.GetSegment(Seg.A, Seg.b, Level.segments)
        else
          Seg2 := nil;
        if Seg2 <> nil then
        begin
          if sensN then
            Str(Seg.Identificateur2, Chaine)
          else
            Str(Seg.Identificateur, Chaine);
          Sauvegarde.Add(Chaine);
          Break;
        end;
      end;
    end;
    // Composition
    if (Seg.composition = nil) and (Seg.Perimeter) then
    begin
      if compoExt <> nil then
        Str(compoExt.Identificateur, Chaine)
      else
        Chaine := '-1';
    end
    else if (Seg.composition = nil) then
    begin
      if compoInt <> nil then
        Str(compoInt.Identificateur, Chaine)
      else
        Chaine := '-1';
    end
    else
    begin
      if Seg.composition <> nil then
        Str(Seg.composition.Identificateur, Chaine)
      else
        Chaine := '-1'
    end;
    Sauvegarde.Add(Chaine);

    Sauvegarde.Add('-1');
    Sauvegarde.Add('-1');


    Sauvegarde.Add(Chaine);
    // Albedos : A FAIRE UN JOUR
    Sauvegarde.Add('-1');
    // Occultations : A FAIRE UN JOUR
    Sauvegarde.Add('-1');
    // Plantations : A FAIRE UN JOUR
    Sauvegarde.Add('-1');
    // Ouvertures
    Sauvegarde.Add('Liste des Ouvertures');
    Sauvegarde.Add(IntToStr(Seg.ListePortes.Count + Seg.ListeFenetres.Count));
    Log.Add('       ' + IntToStr(Seg.ListePortes.Count + Seg.ListeFenetres.Count) + ' ouvertures');
    for porte in  Seg.ListePortes do
    begin
      Str(Porte.Hauteur, Chaine);
      Sauvegarde.Add(Chaine);
      Str(Porte.Largeur, Chaine);
      Sauvegarde.Add(Chaine);
      if Porte.CompoPorte <> nil then
        Str(Porte.CompoPorte.Identificateur, Chaine)
      else
        Str(CompoDoor.Identificateur, Chaine);
      Sauvegarde.Add(Chaine);
      Str(0, Chaine);
      Sauvegarde.Add(Chaine);
      Chaine := '-1';
      Sauvegarde.Add(Chaine);
    end;
    for fenetre in Seg.ListeFenetres do
    begin
      Str(fenetre.Hauteur, Chaine);
      Sauvegarde.Add(Chaine);
      Str(fenetre.Largeur, Chaine);
      Sauvegarde.Add(Chaine);
      if fenetre.CompoFen <> nil then
        Str(fenetre.CompoFen.Identificateur, Chaine)
      else
        Chaine := '-1';
      // Str(CompoWindow.Identificateur, Chaine);
      Sauvegarde.Add(Chaine);
      // Identificateur Fenetre
      Sauvegarde.Add(IntToStr(IdFen));
      inc(IdFen);
      // Masques int�gr�s
      sauvegarde.Add('-1');
      //ExporteMaquesIntegres(Seg, fenetre, Level, Sauvegarde, Log, Room);
    end;
    // Sens Compo : A FAIRE UN JOUR
    if (Seg.A = Room.Points[i]) or Seg.Perimeter then
      // if sensN
      Sauvegarde.Add('1')
    else
      Sauvegarde.Add('0');
    // Checked
    Sauvegarde.Add('1');
    // Identificateur
    if sensN then
      Str(Seg.Identificateur, Chaine)
    else
      Str(Seg.Identificateur2, Chaine);
    // Str(Room.id*100+i+1,Chaine);
    inc(IdParoiAbs);
    Sauvegarde.Add(Chaine);
    // Masques proches : A FAIRE UN JOUR
    //ExporteMaquesProches(Seg, Level, Sauvegarde, Log, Room, NumLevel);
    Sauvegarde.Add('0');
  end;
end;

procedure TBuildingData.PreparationPlancherIntermediaire;
var
  Level, Levelp: TSketchLevel;
  Room, Room2: TRoom;
  cptLevel, j, k, kk, kl: integer;
  Tampon1, Tampon2: TBitmap;
  Rect: TRect;
  ZZoom: integer;
  OldCenter: TPoint;
  MinX, MinY, MaxX, MaxY: integer;
  p: LTPoint;
  SuperCenter: TPoint;
  Z1, Z2: integer;
  PPP, PPP2: PByteArray;
  SurfaceTotale, SurfaceContact, SurfaceTotale2: integer;
  Surface: double;
  Plancher: Tplancher;
  nbLevel: integer;
  longueur: double;
begin
  Tampon1 := TBitmap.Create;
  Tampon2 := TBitmap.Create;
  Tampon1.PixelFormat := pf8bit;
  Tampon2.PixelFormat := pf8bit;
  Tampon1.Width := 3000;
  Tampon2.Width := 3000;
  Tampon1.Height := 3000;
  Tampon2.Height := 3000;
  Rect.Left := 0;
  Rect.Top := 0;
  Rect.Right := 3000;
  Rect.Bottom := 3000;
  nbLevel := project.sketchObject.Levels.Count - 2;
  // if TsketchLevel(project.SketchObject.Levels[nbLevel+1]).Toiture then
  // dec(NbLevel);
  for cptLevel := 0 to nbLevel do
  begin
    Level := TSketchLevel(project.sketchObject.Levels[cptLevel]);
    Levelp := TSketchLevel(project.sketchObject.Levels[cptLevel + 1]);
    for j := 0 to Level.Rooms.Count - 1 do
    begin
      Room := TRoom(Level.Rooms[j]);
      if (Room.RoomType <> roomObstruction) and (Room.RoomType <> roomExternal) then
      begin
        Log.Add('Calcul Plancher interm�diaire pour la pi�ce ' + Room.Name);
        Tampon1.Canvas.Brush.Color := clblack;
        Tampon1.Canvas.FillRect(Rect);
        ZZoom := TSketchObject(Level.FatherObject).Zoom;
        OldCenter := TSketchObject(Level.FatherObject).Center;
        TSketchObject(Level.FatherObject).Center.x := 10;
        TSketchObject(Level.FatherObject).Center.y := 10;
        MinX := 1000000000;
        MaxX := -1000000000;
        MinY := 1000000000;
        MaxY := -1000000000;
        for k := 0 to Room.Points.Count - 1 do
        begin
          p := LTPoint(Room.Points[k]);
          if p.x < MinX then
            MinX := p.x;
          if p.x > MaxX then
            MaxX := p.x;
          if p.y < MinY then
            MinY := p.y;
          if p.y > MaxY then
            MaxY := p.y;
        end;
        Z1 := (MaxX - MinX) div 2900 + 1;
        Z2 := (MaxY - MinY) div 2900 + 1;
        if Z1 > Z2 then
          TSketchObject(Level.FatherObject).Zoom := Z1
        else
          TSketchObject(Level.FatherObject).Zoom := Z2;

        if TSketchObject(Level.FatherObject).Zoom <= 0 then
          TSketchObject(Level.FatherObject).Zoom := 1;
        SuperCenter.x := -MinX;
        SuperCenter.y := -MinY;
        Rect.Left := 0;
        Rect.Top := 0;
        Rect.Right := Tampon1.Width;
        Rect.Bottom := Tampon1.Height;
        DessinePlancher(Tampon1.Canvas, 0, Rect, False, -MinX + 20, -MinY + 20, Room);
        SurfaceTotale := 0;
        for k := 1 to Tampon1.Height - 2 do
        begin
          PPP := Tampon1.scanline[k];
          for kk := 1 to Tampon1.Width - 2 do
          begin
            if PPP[kk] <> 0 then
              inc(SurfaceTotale);
          end;
        end;
        SurfaceTotale2 := SurfaceTotale;
        for k := 0 to Levelp.Rooms.Count - 1 do
        begin
          Tampon2.Canvas.Brush.Color := clblack;
          Tampon2.Canvas.FillRect(Rect);
          Room2 := TRoom(Levelp.Rooms[k]);
          DessinePlancher(Tampon2.Canvas, 0, Rect, False, -MinX + 20, -MinY + 20, Room2);
          SurfaceContact := 0;
          for kl := 1 to Tampon1.Height - 2 do
          begin
            PPP := Tampon1.scanline[kl];
            PPP2 := Tampon2.scanline[kl];
            for kk := 1 to Tampon1.Width - 2 do
            begin
              // if (ppp[kk] <> 0) and (ppp2[kk] <> 0) then
              if bytebool(PPP[kk]) and bytebool(PPP2[kk]) then
                inc(SurfaceContact);
            end;
          end;
          if Room.Area / SurfaceTotale * SurfaceContact >= 0.1 then
          begin
            SurfaceTotale2 := SurfaceTotale2 - SurfaceContact;
            Surface := Room.Area / SurfaceTotale * SurfaceContact;
            Surface := Round(Surface * 10) / 10;
            Plancher := Tplancher.Create;
            plancher.niveau := cptLevel;
            Plancher.PieceBasse := Room.ID;
            Plancher.PieceHaute := TRoom(Levelp.Rooms[k]).ID;
            Plancher.PB := Room;
            Plancher.PH := Levelp.Rooms[k];
            Plancher.Surface := Surface;
            Plancher.idb := 1000 + ListePlancher.Count;
            Plancher.Idh := 2000 + ListePlancher.Count;
            ListePlancher.Add(Plancher);
            if not Levelp.Toiture then
            begin
              Plancher.Orientation := 0;
              Plancher.Inclinaison := 0;
              Plancher.Correction := 1;
            end
            else
            begin
              Plancher.Inclinaison := ArcCos(Room2.Normale.y) / pi * 180;
              longueur := Sqrt(Sqr(Room2.Normale.x) + Sqr(Room2.Normale.z));
              if longueur <> 0 then
                Plancher.Orientation := ArcSin(Room2.Normale.x / longueur) / pi * 180
              else
                Plancher.Orientation := 0;


              if Room2.Normale.z > 0 then
                Plancher.Orientation := 180 - Plancher.Orientation;
              Plancher.Orientation := 180 + Plancher.Orientation;
              while Plancher.Orientation > 180 do
                Plancher.Orientation := Plancher.Orientation - 360;
              while Plancher.Orientation < -180 do
                Plancher.Orientation := Plancher.Orientation + 360;
              Plancher.Orientation := Plancher.Orientation - project.sketchObject.OrientationNord;
              Plancher.Correction := Cos(Plancher.Inclinaison / 180 * pi);
            end;
          end;
        end;
        if SurfaceTotale2 > 1 then
        begin
          Surface := Room.Area / SurfaceTotale * SurfaceTotale2;
          Surface := Round(Surface * 10) / 10;
          Plancher := Tplancher.Create;
          plancher.niveau := cptLevel;
          Plancher.PieceBasse := Room.ID;
          Plancher.PieceHaute := -3;
          Plancher.Surface := Surface;
          Plancher.idb := -3;
          Plancher.Idh := 2000 + ListePlancher.Count;
          ListePlancher.Add(Plancher);
        end;
        TSketchObject(Level.FatherObject).Zoom := ZZoom;
        TSketchObject(Level.FatherObject).Center.x := OldCenter.x;
        TSketchObject(Level.FatherObject).Center.y := OldCenter.y;
        // Form1.Image1.Picture.Bitmap := Tampon1
      end;
    end;
  end;
  Tampon1.Free;
  Tampon2.Free;
end;

procedure TBuildingData.DessinePlancher(Can: TCanvas; GridType: integer; Rect: TRect; DrawLegend: boolean; Tx, Ty: integer; Room: TRoom);
var
  Zoom, j: integer;
  Center: TPoint;
  PointArray: array of TPoint;
  // AverageX,AverageY : Integer;
begin
  Zoom := project.sketchObject.Zoom;
  Center := project.sketchObject.Center;
  SetLength(PointArray, Room.Points.Count);
  with Can do
  begin
    Pen.Color := clwhite;
    Pen.Width := 0;
    Brush.Color := clwhite;
    for j := 0 to Room.Points.Count - 1 do
    begin
      PointArray[j].x := (LTPoint(Room.Points[j]).x + Center.x + Tx) div Zoom;
      PointArray[j].y := (LTPoint(Room.Points[j]).y + Center.y + Ty) div Zoom;
    end;
    Polygon(PointArray);
  end;
end;

function TBuildingData.CorrigeHauteurP(p, p2: LTPoint; Room: TRoom): double;
var
  i, j, k, ijkk: integer;
  Level, LevelToit, TLevel: TSketchLevel;
  NLevel, NLevelToit: integer;
  nbLevel, Erreur: integer;
  ATraiter: boolean;
  Level2: boolean;
  RoomTemp, RRoom: TRoom;
  MaxHauteur, Hauteur: double;
  A, b, c, d: double;
  m1, m2, m3: TP3D;
  Indice: integer;
  PTrouve: boolean;
begin
  result := 0;
  MaxHauteur := Maxlongint;
  nbLevel := project.sketchObject.Levels.Count;
  NLevel := 0;
  for i := 0 to nbLevel - 2 do
  begin
    Level := TSketchLevel(project.sketchObject.Levels[i]);
    if Level.Rooms.IndexOf(Room) <> -1 then
      NLevel := i;
  end;
  LevelToit := TSketchLevel(project.sketchObject.Levels[nbLevel - 1]);
  NLevelToit := nbLevel - 1;
  if LevelToit.points.Count = 0 then
  begin
    LevelToit := TSketchLevel(project.sketchObject.Levels[nbLevel - 2]);
    NLevelToit := nbLevel - 2;
  end;
  if LevelToit.Toiture then
  begin
    for j := 0 to LevelToit.Rooms.Count - 1 do
    begin
      RRoom := TRoom(LevelToit.Rooms[j]);
      if RRoom.HautPoint.Count <> RRoom.Points.Count then
      begin
        RRoom.HautPoint.Clear;
        for ijkk := 0 to RRoom.Points.Count - 1 do
          RRoom.HautPoint.Add(0);
      end;
    end;
    ATraiter := True;
    for i := NLevel + 1 to NLevelToit - 1 do
    begin
      TLevel := TSketchLevel(project.sketchObject.Levels[i]);
      for j := 0 to TLevel.Rooms.Count - 1 do
      begin
        RoomTemp := TRoom(TLevel.Rooms[j]);
        if TLevel.PointInOut2(p, RoomTemp.Points) >= 0 then
          ATraiter := False
      end;
    end;
    if ATraiter then
    begin
      PTrouve := False;
      for j := 0 to LevelToit.Rooms.Count - 1 do
      begin
        RoomTemp := TRoom(LevelToit.Rooms[j]);
        for k := 0 to RoomTemp.Points.Count - 2 do
        begin
          if (Abs(LTPoint(RoomTemp.Points[k]).x - p.x) < 0.001) and (Abs(LTPoint(RoomTemp.Points[k]).y - p.y) < 0.001) and
            (LevelToit.PointInOut2(p2, RoomTemp.Points) >= 0) then
          begin
            hauteur := RoomTemp.HautPoint[k];
            PTrouve := True;
            // if MaxHauteur <> Maxlongint then
            // Maxhauteur := Maxhauteur;
            if Hauteur < MaxHauteur then
              MaxHauteur := Hauteur;
          end;
        end;
      end;
      for j := 0 to LevelToit.Rooms.Count - 1 do
      begin
        RoomTemp := TRoom(LevelToit.Rooms[j]);
        if (LevelToit.PointInOut2(p, RoomTemp.Points) >= 0) and (LevelToit.PointInOut2(p2, RoomTemp.Points) >= 0) and not PTrouve then
        // if (LevelToit.PointInOut(p, RoomTemp.Points)) and
        // (LevelToit.PointInOut(p2, RoomTemp.Points)) and not PTrouve then
        begin
          Indice := 0;
          A := 0;
          b := 0;
          c := 0;
          d := 0;
          // while (a = 0) and (b = 0) and (c = 0) and (d = 0) do
          While (c = 0) do
          begin
            m1.x := LTPoint(RoomTemp.Points[Indice]).x;
            m1.y := LTPoint(RoomTemp.Points[Indice]).y;
            m1.z := RoomTemp.HautPoint[Indice];

            m2.x := LTPoint(RoomTemp.Points[Indice + 1]).x;
            m2.y := LTPoint(RoomTemp.Points[Indice + 1]).y;
            m2.z := RoomTemp.HautPoint[Indice + 1];

            m3.x := LTPoint(RoomTemp.Points[Indice + 2]).x;
            m3.y := LTPoint(RoomTemp.Points[Indice + 2]).y;
            m3.z := RoomTemp.HautPoint[Indice + 2];

            // Calcule �quation du plan
            A := m2.y * (m3.z - m1.z) - m1.y * m3.z - m2.z * m3.y + m2.z * m1.y + m1.z * m3.y;
            b := -m2.x * m3.z + m2.x * m1.z + m1.x * m3.z + m2.z * m3.x - m2.z * m1.x - m1.z * m3.x;
            c := m2.x * m3.y - m2.x * m1.y - m1.x * m3.y - m2.y * m3.x + m2.y * m1.x + m1.y * m3.x;
            d := -m1.x * A - m1.y * b - m1.z * c;
            inc(Indice);
          end;
          Hauteur := (-A * p.x - b * p.y - d) / c;
          // if Hauteur <> 5 then
          // Hauteur := 5;
          if Hauteur < MaxHauteur then
            MaxHauteur := Hauteur;
        end;
      end;
      if MaxHauteur = Maxlongint then
        MaxHauteur := 0;
      result := MaxHauteur;
    end;
  end;
end;

initialization
  buildingData := TBuildingData.Create;

end.
