program Dialogie;

//  FastMM4 in '_files\package\fastMM\FastMM4.pas',
//  FastMM4Messages in '_files\package\fastMM\FastMM4Messages.pas',

uses
  Forms,
  MidasLib,
  Dialogs,
  Main in 'ui\Main.pas' {FormMain},
  EquipementECSSolaire in 'objects\model\EquipementECSSolaire.pas',
  BiblioMeteo in 'ui\BiblioMeteo.pas' {FormBiblioMeteo},
  Choix in 'ui\Choix.pas' {FormChoix},
  EIE in 'ui\EIE.pas' {FormEIE},
  Enregistrement in 'ui\Enregistrement.pas' {FormEnregistrement},
  Splash in 'ui\Splash.pas' {FormSplash},
  UtilitairesDialogie in 'util\UtilitairesDialogie.pas',
  ListeOuvertures in 'ui\ListeOuvertures.pas' {FormListeFenetres},
  richole in 'richole.pas',
  Impstringgrid in 'Impstringgrid.pas',
  Facture in 'objects\model\Facture.pas',
  HelpID in 'HelpID.pas',
  Projet in 'objects\model\Projet.pas',
  Variante in 'objects\model\Variante.pas',
  Resultat in 'objects\data\Resultat.pas',
  Vitrage in 'objects\model\Vitrage.pas',
  Generalites in 'objects\model\Generalites.pas',
  MenuLavage in 'objects\data\MenuLavage.pas',
  MenuFroid in 'objects\data\MenuFroid.pas',
  MenuMenager in 'objects\data\MenuMenager.pas',
  MenuBureau in 'objects\data\MenuBureau.pas',
  MenuEcsSolaire in 'objects\data\MenuEcsSolaire.pas',
  IzCalculConsoDialogie in 'objects\model\Dialogie\IzCalculConsoDialogie.pas',
  IzBiblioDialogie in 'objects\data\IzBiblioDialogie.pas',
  IzAppareilProcess in 'objects\model\appareil\IzAppareilProcess.pas',
  IzBiblioCoeffG in 'objects\data\IzBiblioCoeffG.pas',
  IzCuisine in 'objects\model\IzCuisine.pas',
  IzBiblioCuisine in 'objects\data\IzBiblioCuisine.pas',
  IzAppareil in 'objects\model\appareil\IzAppareil.pas',
  IzBureautique in 'objects\model\appareil\IzBureautique.pas',
  IzFroid in 'objects\model\appareil\IzFroid.pas',
  IzLavage in 'objects\model\appareil\IzLavage.pas',
  IzMenager in 'objects\model\appareil\IzMenager.pas',
  IzBiblioEclairage in 'objects\data\IzBiblioEclairage.pas',
  IzEclairage in 'objects\model\appareil\IzEclairage.pas',
  IzBiblioDepartement in 'objects\data\IzBiblioDepartement.pas',
  IzBiblioCorrectifZone in 'objects\data\IzBiblioCorrectifZone.pas',
  IzBiblioCoeffMaisonAb in 'objects\data\IzBiblioCoeffMaisonAb.pas',
  IzBiblioCoeffMaisonAmelior in 'objects\data\IzBiblioCoeffMaisonAmelior.pas',
  IzBiblioCoeffMaisonAmeliorTravaux in 'objects\data\IzBiblioCoeffMaisonAmeliorTravaux.pas',
  IzFrameDescAppart in 'ui\IzFrameDescAppart.pas' {FrameDescAppart: TFrame},
  IzFrameDescMaison in 'ui\IzFrameDescMaison.pas' {FrameDescMaison: TFrame},
  IzBiblioCollectifPosition in 'objects\data\IzBiblioCollectifPosition.pas',
  IzFrameSankey in 'ui\deperdition\IzFrameSankey.pas' {FrameSankey: TFrame},
  IzDeperdition in 'objects\model\IzDeperdition.pas',
  IzBiblioTau in 'objects\data\IzBiblioTau.pas',
  IzFrameAppareils in 'ui\appareil\IzFrameAppareils.pas' {FrameAppareils: TFrame},
  IzFrameEclairage in 'ui\appareil\IzFrameEclairage.pas' {FrameEclairage: TFrame},
  InternetUpdate in 'util\InternetUpdate.pas',
  IzFormLog in 'ui\util\IzFormLog.pas' {FormLog},
  FProgress in 'ui\util\FProgress.pas' {FormProgress},
  BindStatusCallBack in 'util\BindStatusCallBack.pas',
  IzFrameVarianteComparatorRelative in 'ui\comparator\IzFrameVarianteComparatorRelative.pas' {FrameVarianteComparatorRelative: TFrame},
  IzFrameVarianteComparatorTabs in 'ui\comparator\IzFrameVarianteComparatorTabs.pas' {FrameVarianteComparatorTabs: TFrame},
  IzFrameVarianteComparatorChart in 'ui\comparator\IzFrameVarianteComparatorChart.pas' {FrameVarianteComparatorChart},
  IzConsigneTemperature in 'objects\model\Dialogie\IzConsigneTemperature.pas',
  IzFrameConsigneTemperature in 'ui\IzFrameConsigneTemperature.pas' {FrameConsigneTemperature: TFrame},
  IzFrameDialogieReportManager in 'ui\report\IzFrameDialogieReportManager.pas' {FrameDialogieReportManager: TFrame},
  IzFrameVarianteReportManager in 'ui\report\IzFrameVarianteReportManager.pas' {FrameVarianteReportManager: TFrame},
  IzFormMap in 'ui\map\IzFormMap.pas' {FormMap},
  Meteo in 'ui\map\Meteo.pas' {FormMeteo},
  PlaceVille in 'ui\map\PlaceVille.pas' {FormPlacement},
  IzFormDescription in 'ui\util\IzFormDescription.pas' {FormDescription},
  BiblioMenuiseries in 'library-manager\Objets\Menuiseries\BiblioMenuiseries.pas',
  CadreMenuiseries in 'library-manager\Objets\Menuiseries\CadreMenuiseries.pas' {FrameMenuiseries: TFrame},
  Menuiseries in 'library-manager\Objets\Menuiseries\Menuiseries.pas',
  SaisieFen in 'library-manager\Objets\Menuiseries\SaisieFen.pas' {FormSaisieFen},
  IzFormMenuiserie in 'ui\menuiserie\IzFormMenuiserie.pas' {FormSaisieMenuiserie},
  IzFrameMenuiserieDialogie in 'ui\menuiserie\IzFrameMenuiserieDialogie.pas' {FrameMenuiserieDialogie: TFrame},
  IzFormBiblioMenuiserieDialogie in 'ui\menuiserie\IzFormBiblioMenuiserieDialogie.pas' {FormBiblioMenuiserieDialogie},
  IzFrameSaisieCoeff in 'ui\saisie\IzFrameSaisieCoeff.pas' {FrameSaisieCoeff: TFrame},
  IzFrameSaisieParoi in 'ui\saisie\IzFrameSaisieParoi.pas' {FrameSaisieParoi: TFrame},
  IzFrameSaisie3D in 'ui\saisie\IzFrameSaisie3D.pas' {FrameSaisie3D: TFrame},
  IzFramePleiades in 'ui\saisie\IzFramePleiades.pas' {FramePleiades: TFrame},
  IzFormUiUtil in 'ui\util\IzFormUiUtil.pas' {FormUiUtil},
  IzTypeEnergie in 'library-manager\commun\calculConso\objects\model\energie\IzTypeEnergie.pas',
  IzEnergies3CL in 'library-manager\commun\calculConso\objects\model\energie\3CL\IzEnergies3CL.pas',
  IzEquipementEcsSolaire3CL in 'library-manager\commun\calculConso\objects\model\equipement\3CL\IzEquipementEcsSolaire3CL.pas',
  IzEquipementEcs3CL in 'library-manager\commun\calculConso\objects\model\equipement\3CL\IzEquipementEcs3CL.pas',
  IzEquipementChauffage3CL in 'library-manager\commun\calculConso\objects\model\equipement\3CL\IzEquipementChauffage3CL.pas',
  IzEquipementChauffage in 'library-manager\commun\calculConso\objects\model\equipement\IzEquipementChauffage.pas',
  IzEquipementEcs in 'library-manager\commun\calculConso\objects\model\equipement\IzEquipementEcs.pas',
  IzEquipementEcsSolaire in 'library-manager\commun\calculConso\objects\model\equipement\IzEquipementEcsSolaire.pas',
  IzEquipementVentilation in 'library-manager\commun\calculConso\objects\model\equipement\IzEquipementVentilation.pas',
  IzEquipementClimatisation in 'library-manager\commun\calculConso\objects\model\equipement\IzEquipementClimatisation.pas',
  IzProjet in 'library-manager\commun\calculConso\objects\model\IzProjet.pas',
  IzCalculConso in 'library-manager\commun\calculConso\objects\model\IzCalculConso.pas',
  IzEnergieAbonnementFrame3CL in 'library-manager\commun\calculConso\ui\energie\3CL\IzEnergieAbonnementFrame3CL.pas' {EnergieAbonnementFrame3CL: TFrame},
  IzEnergiesFrame3CL in 'library-manager\commun\calculConso\ui\energie\3CL\IzEnergiesFrame3CL.pas' {EnergiesFrame3CL: TFrame},
  IzEquipementFrame in 'library-manager\commun\calculConso\ui\equipement\IzEquipementFrame.pas' {EquipementFrame: TFrame},
  IzEquipementsFrame in 'library-manager\commun\calculConso\ui\equipement\IzEquipementsFrame.pas' {EquipementsFrame: TFrame},
  IzEcsSolaireFrame3CL in 'library-manager\commun\calculConso\ui\equipement\3CL\IzEcsSolaireFrame3CL.pas' {FrameECSolaire: TFrame},
  IzEcsFrame3CL in 'library-manager\commun\calculConso\ui\equipement\3CL\IzEcsFrame3CL.pas' {EcsFrame3CL: TFrame},
  IzChauffageFrame3CL in 'library-manager\commun\calculConso\ui\equipement\3CL\IzChauffageFrame3CL.pas' {ChauffageFrame3CL: TFrame},
  IzBiblioSystem in 'library-manager\commun\calculConso\objects\data\IzBiblioSystem.pas',
  IzConstantesConso in 'library-manager\commun\calculConso\util\IzConstantesConso.pas',
  IzBiblioEnergieRepartition3CL in 'library-manager\commun\calculConso\objects\data\energie\3CL\IzBiblioEnergieRepartition3CL.pas',
  IzBiblioEnergieTarif3CL in 'library-manager\commun\calculConso\objects\data\energie\3CL\IzBiblioEnergieTarif3CL.pas',
  IzBiblioEcs3CL in 'library-manager\commun\calculConso\objects\data\equipement\3CL\IzBiblioEcs3CL.pas',
  IzBiblioChauffage3CL in 'library-manager\commun\calculConso\objects\data\equipement\3CL\IzBiblioChauffage3CL.pas',
  IzFrameProjetAjustement in 'ui\analyse\IzFrameProjetAjustement.pas' {FrameProjetAjustement: TFrame},
  IzFrameResultChart in 'ui\chart\IzFrameResultChart.pas' {FrameResultChart: TFrame},
  IzFrameOccupation in 'ui\saisie\IzFrameOccupation.pas' {FrameOccupation: TFrame},
  IzFrameRepartitionChart in 'ui\chart\IzFrameRepartitionChart.pas' {FrameRepartitionChart: TFrame},
  IzFrameSaisieFacture in 'ui\analyse\IzFrameSaisieFacture.pas' {FrameSaisieFacture: TFrame},
  IzFrameAppareilsRatio in 'ui\appareil\IzFrameAppareilsRatio.pas' {FrameAppareilsRatio: TFrame},
  SaisieFactureBois in 'ui\analyse\SaisieFactureBois.pas' {FormFBois},
  SaisieFactureElec in 'ui\analyse\SaisieFactureElec.pas' {FormFElectrique},
  SaisieFactureFuel in 'ui\analyse\SaisieFactureFuel.pas' {FormFFuel},
  SaisieFactureGaz in 'ui\analyse\SaisieFactureGaz.pas' {FormFGaz},
  IzFrameCuisson in 'ui\appareil\IzFrameCuisson.pas' {FrameCuisson: TFrame},
  IzFrameEcsSolaire in 'ui\appareil\IzFrameEcsSolaire.pas' {FrameEcsSolaire: TFrame},
  CalculMeteo in 'objects\CalculMeteo.pas',
  CalculECSSolaire in 'objects\CalculECSSolaire.pas',
  Saisie in 'objects\model\Saisie.pas',
  Occupation in 'objects\model\Occupation.pas',
  objetMeteo in 'objects\objetMeteo.pas',
  Rapport in 'objects\report\Rapport.pas',
  ComposantBibliotheque in 'library-manager\Objets\ComposantBib\ComposantBibliotheque.pas',
  GestionBib in 'library-manager\Objets\GestionBib\GestionBib.pas',
  Elements in 'library-manager\Objets\Elements\Elements.pas',
  BiblioElements in 'library-manager\Objets\Elements\BiblioElements.pas',
  CadreElements in 'library-manager\Objets\Elements\CadreElements.pas' {FrameElement: TFrame},
  Materiaux in 'library-manager\Objets\Materiaux\Materiaux.pas',
  BiblioMateriaux in 'library-manager\Objets\Materiaux\BiblioMateriaux.pas',
  CadreMateriaux in 'library-manager\Objets\Materiaux\CadreMateriaux.pas' {FrameMateriaux: TFrame},
  Compositions in 'library-manager\Objets\Compositions\Compositions.pas',
  BiblioCompositions in 'library-manager\Objets\Compositions\BiblioCompositions.pas',
  CadreCompositions in 'library-manager\Objets\Compositions\CadreCompositions.pas' {FrameCompositions: TFrame},
  IzFrameMateriauxDialogie in 'ui\parois-compo\IzFrameMateriauxDialogie.pas' {FrameMateriauxDialogie: TFrame},
  IzFrameElementDialogie in 'ui\parois-compo\IzFrameElementDialogie.pas' {FrameElementDialogie: TFrame},
  IzFrameCompositionsDialogie in 'ui\parois-compo\IzFrameCompositionsDialogie.pas' {FrameCompositionsDialogie: TFrame},
  IzFrameBiblioParoisDialogie in 'ui\parois-compo\IzFrameBiblioParoisDialogie.pas' {FrameBiblioParoisDialogie: TFrame},
  IzFormBiblioParoisDialogie in 'ui\parois-compo\IzFormBiblioParoisDialogie.pas' {FormBiblioParoisDialogie},
  IzFrameSankeyKwh in 'ui\deperdition\IzFrameSankeyKwh.pas' {FrameSankeyKwh: TFrame},
  DialogsInternational in 'util\DialogsInternational.pas',
  IzCalculConso3CL in 'library-manager\commun\calculConso\objects\model\IzCalculConso3CL.pas',
  IzChauffagesFrame3CL in 'ui\dpe\equipement\IzChauffagesFrame3CL.pas' {ChauffagesFrame3CL: TFrame},
  IzChauffageFrameDialogie in 'ui\dpe\equipement\IzChauffageFrameDialogie.pas' {ChauffageFrameDialogie: TFrame},
  IzChauffageDialogDialogie in 'ui\dpe\equipement\IzChauffageDialogDialogie.pas' {ChauffageDialogDialogie},
  uVistaFuncs in 'ui\util\uVistaFuncs.pas',
  creerSauvegarde in 'objects\creerSauvegarde.pas',
  gestionSauvegarde in 'objects\gestionSauvegarde.pas',
  ListeGenerique in 'objects\ListeGenerique.pas',
  IzReport in 'objects\report\IzReport.pas',
  IzBiblioData in 'objects\data\IzBiblioData.pas',
  IzObject in 'objects\model\IzObject.pas',
  IzObjectPersist in 'objects\model\IzObjectPersist.pas',
  IzObjectUtil in 'objects\model\IzObjectUtil.pas',
  IzTypeObject in 'objects\model\IzTypeObject.pas',
  IzTypeObjectPersist in 'objects\model\IzTypeObjectPersist.pas',
  IzHashtable in 'objects\IzHashtable.pas',
  IzObjectFrame in 'ui\IzObjectFrame.pas' {ObjectFrame: TFrame},
  IzObjectPersistFrame in 'ui\IzObjectPersistFrame.pas' {ObjectPersistFrame: TFrame},
  IzObjectsPersistFrame in 'ui\IzObjectsPersistFrame.pas' {ObjectsPersistFrame: TFrame},
  IzPercentObjectFrame in 'ui\IzPercentObjectFrame.pas' {PercentObjectFrame: TFrame},
  IzPercentObjectsFrame in 'ui\IzPercentObjectsFrame.pas' {PercentObjectsFrame: TFrame},
  IzConstantes in 'util\IzConstantes.pas',
  IzUiUtil in 'util\IzUiUtil.pas' {UiUtilForm},
  IzUtilitaires in 'util\IzUtilitaires.pas',
  IzApp in 'objects\IzApp.pas',
  diaSaisieBase in 'objects\model\Dialogie\diaSaisieBase.pas',
  IzFrameSaisieAmelFenetre in 'ui\saisie\IzFrameSaisieAmelFenetre.pas' {FrameSaisieAmelFenetre: TFrame},
  IzFrameSaisieAmelIsolation in 'ui\saisie\IzFrameSaisieAmelIsolation.pas' {FrameSaisieAmelIsolation: TFrame},
  IzFrameSaisieChoixFenetre in 'ui\saisie\IzFrameSaisieChoixFenetre.pas' {FrameSaisieChoixFenetre: TFrame},
  IzFrameSaisieChoixIsolation in 'ui\saisie\IzFrameSaisieChoixIsolation.pas' {FrameSaisieChoixIsolation: TFrame},
  IzFrameSankeyOffice in 'ui\deperdition\IzFrameSankeyOffice.pas' {FrameSankeyOffice: TFrame},
  IzFrameSankeyAdeme in 'ui\deperdition\IzFrameSankeyAdeme.pas' {FrameSankeyAdeme: TFrame},
  IzFrameSaisieIsolation in 'ui\saisie\IzFrameSaisieIsolation.pas' {FrameSaisieIsolation: TFrame},
  IzFrameSaisieFenetre in 'ui\saisie\IzFrameSaisieFenetre.pas' {FrameSaisieFenetre: TFrame},
  IzFormVarianteInfo in 'ui\IzFormVarianteInfo.pas' {FormVarianteInfo},
  DgFrameProjetInfo in 'ui\DgFrameProjetInfo.pas' {FrameProjectInfo: TFrame},
  RapportMulti in 'objects\report\RapportMulti.pas',
  ekbasereport in 'ekRtf\ekbasereport.pas',
  ekrtf in 'ekRtf\ekrtf.pas',
  conscom in 'ekRtf\conscom.pas',
  ekfunc in 'ekRtf\ekfunc.pas',
  ekrtfstream in 'ekRtf\ekrtfstream.pas',
  XmlUtil in 'util\XmlUtil.pas',
  IzBiblioVentilation26 in 'objects\data\IzBiblioVentilation26.pas',
  IzFramePermeabilite26 in 'ui\appareil\IzFramePermeabilite26.pas' {FramePermeabilite26: TFrame},
  IzEquipVentil26 in 'objects\model\Dialogie\equipement\IzEquipVentil26.pas',
  IzFrameVentil26 in 'ui\appareil\IzFrameVentil26.pas' {FrameVentil26: TFrame},
  diaMeteo in 'objects\model\Dialogie\diaMeteo.pas',
  IzBiblioEclairageModulation in 'objects\data\IzBiblioEclairageModulation.pas',
  diaSaisieDescription in 'objects\model\Dialogie\diaSaisieDescription.pas',
  diaAjustement in 'objects\model\Dialogie\diaAjustement.pas',
  IzFormFileProjectTransform in 'ui\report\IzFormFileProjectTransform.pas' {FormFileProjectTransform},
  IzXslTransform in 'util\IzXslTransform.pas',
  IzFormProjectTransform in 'ui\report\IzFormProjectTransform.pas' {FormProjectTransform},
  IzSysUtil in 'util\IzSysUtil.pas',
  diaFrameAnalyseEco in 'ui\analyse\diaFrameAnalyseEco.pas' {FrameAnalyseEco: TFrame},
  diaAnalyseEco in 'objects\model\analyse\diaAnalyseEco.pas',
  diaFormChooseLabelOrVariante in 'ui\diaFormChooseLabelOrVariante.pas' {FormChooseLabelOrVariante},
  diaSynopticTarget in 'objects\model\analyse\diaSynopticTarget.pas',
  amSplitter in 'ui\util\amSplitter.pas',
  IzFrameHtml in 'ui\util\IzFrameHtml.pas' {FrameHtml: TFrame},
  diaParameters in 'objects\util\diaParameters.pas',
  diaCalculClim in 'objects\model\appareil\diaCalculClim.pas',
  diaFrameClim in 'ui\appareil\diaFrameClim.pas' {FrameClim: TFrame},
  diaParametersUi in 'objects\util\diaParametersUi.pas',
  diaFrameSynoptiqueParoi in 'ui\analyse\diaFrameSynoptiqueParoi.pas' {FrameSynoptiqueParoi: TFrame};

//  PontsIntegres in 'library-manager\Objets\ponts\integres\PontsIntegres.pas',
//  GestionJeuxPontIntegre in 'library-manager\Objets\ponts\jeux\GestionJeuxPontIntegre.pas',
//  GestionJeux in 'library-manager\Objets\ponts\jeux\GestionJeux.pas';

{$R *.RES}


begin
	Application.Initialize;
	Application.Title := TFormMain.getAppVersionLabel;
  Application.HelpFile := TApp.commonDataPath + 'dialogie.chm';
  FormEnregistrement := TFormEnregistrement.Create(Application);
//showMessage('enreg create');
  registerSoft(false);
//showMessage('register');
  FormSplash := TFormSplash.Create(application);
//showMessage('splash');
  Application.CreateForm(TFormMain, FormMain);
  Application.CreateForm(TFormFileProjectTransform, FormFileProjectTransform);
  Application.CreateForm(TFormProjectTransform, FormProjectTransform);
  Application.CreateForm(TFormChooseLabelOrVariante, FormChooseLabelOrVariante);
  //showMessage('main');
  Application.CreateForm(TFormBiblioMeteo, FormBiblioMeteo);
  Application.CreateForm(TFormListeFenetres, FormListeFenetres);
  Application.CreateForm(TFormChoix, FormChoix);
//showMessage('choix');
  Application.CreateForm(TFormEIE, FormEIE);
//showMessage('eie');
  Application.CreateForm(TFormLog, FormLog);
//showMessage('log');
  Application.CreateForm(TFormProgress, FormProgress);
  Application.CreateForm(TFormMeteo, FormMeteo);
  Application.CreateForm(TFormPlacement, FormPlacement);
  Application.CreateForm(TFormDescription, FormDescription);
  Application.CreateForm(TFormSaisieFen, FormSaisieFen);
  Application.CreateForm(TFormSaisieMenuiserie, FormSaisieMenuiserie);
  Application.CreateForm(TFormBiblioMenuiserieDialogie, FormBiblioMenuiserieDialogie);
  Application.CreateForm(TFormUiUtil, FormUiUtil);
  Application.CreateForm(TFormFBois, FormFBois);
  Application.CreateForm(TFormFElectrique, FormFElectrique);
  Application.CreateForm(TFormFFuel, FormFFuel);
  Application.CreateForm(TFormFGaz, FormFGaz);
  Application.CreateForm(TFormBiblioParoisDialogie, FormBiblioParoisDialogie);
  Application.CreateForm(TChauffagesFrame3CL, ChauffagesFrame3CL);
  Application.CreateForm(TChauffageDialogDialogie, ChauffageDialogDialogie);
  Application.CreateForm(TUiUtilForm, UiUtilForm);
  Application.CreateForm(TFormVarianteInfo, FormVarianteInfo);
//showMessage('variante');
  formMain.checkLaunchParam();
//showMessage('check launch');
{$IFDEF APP_VIRVOLT}
	FormMain.setVirVolt;
{$ENDIF}
	Application.Run;



end.
