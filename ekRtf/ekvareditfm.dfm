�
 TEKVAREDITFORM 0�  TPF0TEkVarEditFormEkVarEditFormLeft:TopBorderIcons BorderStylebsDialogCaptionVariableClientHeight� ClientWidth�Color	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrderPositionpoScreenCenterOnShowFormShowPixelsPerInch`
TextHeight TEditEdNameLeft8TopWidth� HeightTabOrder   TStaticTextStaticText1LeftTopWidth HeightCaptionNameTabOrder  TStaticTextStaticText2Left� TopWidth3HeightCaptionDataTypeTabOrder  	TComboBoxDataTypeBoxLeftTopWidthqHeightStylecsDropDownList
ItemHeightItems.StringsStringNumberBooleanDateTime TabOrderOnChangeDataTypeBoxChange  TStaticTextStaticText3LeftTop@WidthHeightCaptionValueTabOrder  TButtonOkBtnLeft� Top� WidthKHeightCaptionOkDefault	TabOrderOnClick
OkBtnClick  TButtonCanceBtnLeft@Top� WidthKHeightCancel	CaptionCancelModalResultTabOrder  	TNotebook	Notebook1Left0Top0WidthaHeighti	PageIndexTabOrderOnPageChangedNotebook1PageChanged TPage Left Top CaptionString TEditEdStringLeftTopWidthQHeightTabOrder    TPage Left Top CaptionNumber TEditEdNumberLeftTopWidthQHeightTabOrder    TPage Left Top CaptionBoolean TRadioGroup	BoolGroupLeftTopWidthQHeight)Columns	ItemIndex Items.StringsTrueFalse TabOrder TabStop	   TPage Left Top CaptionDateTime TDateTimePickerDateTimePicker1LeftTopWidth� HeightCalAlignmentdtaLeftDate      �@Time      �@
DateFormatdfShortDateMode
dmComboBoxKinddtkDate
ParseInputTabOrder   TDateTimePickerDateTimePicker2Left� TopWidth� HeightCalAlignmentdtaLeftDate      %�@Time      %�@
DateFormatdfShortDateModedmUpDownEnabledKinddtkTime
ParseInputTabOrder  	TCheckBoxUseTimeLeft� Top(WidthIHeightCaptionSet timeTabOrderOnClickUseTimeClick     